﻿1
00:00:00,880 --> 00:00:07,480
‫In the last lecture we've seen how to read the entire content of a file as a string using the read

2
00:00:07,480 --> 00:00:09,560
‫method.

3
00:00:09,560 --> 00:00:17,030
‫The read method can also be used to tell Python to read only a number of characters from the file and return

4
00:00:17,120 --> 00:00:21,860
‫that many characters not the entire content of the file.

5
00:00:21,860 --> 00:00:30,500
‫You can do this by entering an integer in between parentheses; let's read only the first five characters

6
00:00:30,740 --> 00:00:31,710
‫of the file.

7
00:00:34,300 --> 00:00:41,950
‫This is the text file I am reading and I'll execute the Python script.

8
00:00:42,110 --> 00:00:47,260
‫We can't see how it read and printed out only the first five characters

9
00:00:51,310 --> 00:01:03,980
‫now let's read and print out another three characters f.read and 3 between parentheses print content

10
00:01:12,230 --> 00:01:20,430
‫We see how it read and printed out the next three characters after the first 5 characters. These three characters.

11
00:01:20,520 --> 00:01:29,390
‫Somehow Python memorized the position where it left inside the file after calling

12
00:01:29,390 --> 00:01:31,940
‫the first read method.

13
00:01:31,940 --> 00:01:36,270
‫This is where the concept of cursor comes into play.

14
00:01:36,440 --> 00:01:42,830
‫The cursor is the position at which you are currently located inside the file.

15
00:01:42,830 --> 00:01:52,500
‫We can get this position by calling the Tell method of any file object print f.tell

16
00:01:58,300 --> 00:02:08,900
‫and we see that the cursor is at position 8. If we want to move to a specific position inside the file

17
00:02:08,900 --> 00:02:11,650
‫we use another method called Seek.

18
00:02:11,660 --> 00:02:18,500
‫This method takes in as argument the number of characters from the beginning of the file at which the

19
00:02:18,500 --> 00:02:27,440
‫cursor will be positioned. Let's move the cursor to position two and then read three more characters

20
00:02:27,500 --> 00:02:39,860
‫f.seek of two. I've moved the cursor at position two.  enter now print f.read of three

21
00:02:39,890 --> 00:02:49,360
‫I am reading three characters;  this is position 2 and these are those three characters that it will read.

22
00:02:49,450 --> 00:02:53,550
‫So STM ; let's execute the script.

23
00:02:57,910 --> 00:03:01,700
‫And we see how it's read and printed out exactly

24
00:03:01,810 --> 00:03:04,180
‫those three characters.

25
00:03:04,180 --> 00:03:12,890
‫So starting from position two if we want to move at the beginning of the file we move the cursor to position

26
00:03:12,890 --> 00:03:18,050
‫zero f.seek of zero.

27
00:03:18,060 --> 00:03:21,720
‫Now the cursor is at the beginning of the file.

28
00:03:23,180 --> 00:03:26,920
‫Bprint f.read of one

29
00:03:27,090 --> 00:03:34,480
‫Let's check it out and it printed out h -  a lower case h.

30
00:03:34,550 --> 00:03:36,740
‫Of course this is the first character.


﻿1
00:00:00,720 --> 00:00:08,400
‫Hello guys and welcome! In order to be proficient in Networking Automation using Python you must know

2
00:00:08,400 --> 00:00:13,090
‫very well how to process text files in Python.

3
00:00:13,440 --> 00:00:20,740
‫Most of the time the integration of networking devices is saved inside the text file.

4
00:00:20,790 --> 00:00:28,490
‫We must know how to read from a file, how to search inside the file or how to write a text file.

5
00:00:28,800 --> 00:00:38,100
‫Let's assume for example that we have to configure many routers, let's say 50 routers. We could for example

6
00:00:38,310 --> 00:00:48,960
‫write the configuration in a file and then from our Python script we can connect to each device, to each

7
00:00:49,050 --> 00:00:58,050
‫router and push the configuration on the router or simply said we can run each command from our file

8
00:00:58,290 --> 00:01:01,650
‫on each networking device.

9
00:01:01,650 --> 00:01:08,910
‫In Python 3 text files contain Unicode characters which is an ASCII superset.

10
00:01:08,910 --> 00:01:17,730
‫There are 137 thousand thirty nine UNICODE chars and only 128 ASCII chars.

11
00:01:17,730 --> 00:01:25,580
‫UNICODE and ASCII charts are of the same for the first 128 charts. In most of the cases

12
00:01:25,620 --> 00:01:33,570
‫UNICODE chars are included in the UTF 8 or in UTF 32.

13
00:01:33,570 --> 00:01:40,110
‫in this section I'll show you many examples on how to work efficient with files.

14
00:01:40,200 --> 00:01:47,720
‫Of course if you feel confident in working with files in Python you can skip this section.


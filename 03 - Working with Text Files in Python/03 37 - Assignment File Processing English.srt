﻿1
00:00:00,570 --> 00:00:08,040
‫Hello guys! Now that you know how to read from and write to a file I want to give you an assignment.

2
00:00:08,220 --> 00:00:16,720
‫We have these text file that contains information about different networking devices that must be our

3
00:00:16,720 --> 00:00:20,860
‫automaticaly configured from a Python script.

4
00:00:20,880 --> 00:00:28,950
‫I want you to read the content of this file in the list, in such a way that each line will be another

5
00:00:28,950 --> 00:00:29,620
‫list.

6
00:00:29,640 --> 00:00:32,840
‫So in fact I want something like this.

7
00:00:34,550 --> 00:00:38,430
‫I want the content of the file in a list,

8
00:00:38,510 --> 00:00:43,240
‫this is the list,  and each line will be another list.

9
00:00:43,250 --> 00:00:45,280
‫This is the first line.

10
00:00:45,410 --> 00:00:48,290
‫This is the second line.

11
00:00:48,470 --> 00:00:52,520
‫This will be the third line and so on.

12
00:00:52,820 --> 00:01:02,330
‫Within each list, each element will be from our file having the colon as a delimiter.

13
00:01:02,330 --> 00:01:12,020
‫So this will be the first item of the first inner least - this one -  the IP address will be the second

14
00:01:12,110 --> 00:01:16,130
‫element of the first inner list.

15
00:01:16,160 --> 00:01:20,300
‫This will be the user name and the password.

16
00:01:20,410 --> 00:01:27,070
‫Then the second list, from my main list, this will be the first element, the IP address

17
00:01:27,070 --> 00:01:31,380
‫the second element, the user name the third element and the password

18
00:01:31,420 --> 00:01:41,160
‫the fourth element and so on as in this example from idle.  This way of parsing

19
00:01:41,160 --> 00:01:47,810
‫this file is very good because we can access each element very easily.

20
00:01:47,830 --> 00:01:57,820
‫We can iterate through this list and we can access the hostname, the IP address and the user name or

21
00:01:57,820 --> 00:02:02,580
‫the password very easily in a programmable way.

22
00:02:02,860 --> 00:02:07,160
‫In the next video I'll show you how would I do it

23
00:02:07,210 --> 00:02:09,570
‫In case you get stuck.

24
00:02:09,670 --> 00:02:10,390
‫Good luck.


﻿1
00:00:01,640 --> 00:00:06,200
‫In the last lecture we've seen how to read CSV files.

2
00:00:06,330 --> 00:00:12,090
‫Now I'm going to show you how to write C S V files in Python.

3
00:00:12,090 --> 00:00:19,230
‫Writing a CSV file is similar to reading a CSV file. Instead of using the reader function we use the

4
00:00:19,230 --> 00:00:22,620
‫writer function of the same CSV module.

5
00:00:22,620 --> 00:00:24,720
‫These function returns a writer

6
00:00:24,730 --> 00:00:31,350
‫object that will be actually used to write rows to the CSV file. Having the writer

7
00:00:31,350 --> 00:00:40,170
‫object we call a method named writerow and we simply paste in a list or a tuple. The items in the list

8
00:00:40,230 --> 00:00:44,800
‫will be converted to columns in the CSV.

9
00:00:44,930 --> 00:00:47,180
‫I want to show you an example.

10
00:00:47,360 --> 00:00:57,590
‫We have a very simple CSV file with three columns or fields: Id, name a city and I want to add another

11
00:00:57,680 --> 00:01:01,370
‫row to these CSV file.

12
00:01:01,570 --> 00:01:06,130
‫First we open the CSV file in append mode

13
00:01:09,290 --> 00:01:10,520
‫the name of the file.

14
00:01:10,550 --> 00:01:17,880
‫people.csv ;  the CSV file and the Python script are in the same directory.

15
00:01:17,880 --> 00:01:28,050
‫The file will be opened in append mode and the name of the object is CSV file.

16
00:01:28,130 --> 00:01:32,400
‫Now I am creating a writer object.

17
00:01:32,500 --> 00:01:41,720
‫writer equals csv.writer  and I pass in the CSV file object as an argument

18
00:01:45,110 --> 00:01:47,360
‫After creating the writer object

19
00:01:47,540 --> 00:01:53,160
‫I will create the list or the tuple that will be added to the CSV file.

20
00:01:53,300 --> 00:02:02,900
‫Let's say CSV data equals in my tuple is 5 Anne and Amsterdam

21
00:02:06,180 --> 00:02:10,860
‫writer.writerow of csv data

22
00:02:16,320 --> 00:02:17,520
‫and that's all.

23
00:02:18,060 --> 00:02:25,550
‫It's pretty straightforward how you can write rows to a CSV file in a typical way.

24
00:02:25,560 --> 00:02:34,110
‫Let's run the script! There is no error and if we check the content of the CSV file we can see how a new

25
00:02:34,110 --> 00:02:35,640
‫row has been added.

26
00:02:39,990 --> 00:02:47,040
‫Of course if you want to write more rows to a CSV file you use a for loop in order to call the

27
00:02:47,040 --> 00:02:51,740
‫right row method many times. Let's see a new example!

28
00:02:52,050 --> 00:02:59,940
‫I'm going to create a new CSV file write to that file the numbers from 1 to 100 in the

29
00:02:59,940 --> 00:03:08,190
‫first field or column, the second field will be the square of these numbers, in the third field

30
00:03:08,280 --> 00:03:15,690
‫we'll put the numbers raised to the power of three and in the last field the same numbers raised to

31
00:03:15,690 --> 00:03:27,960
‫the power of four. So with open, the name of the file , numbers.csv the file will be opened in

32
00:03:27,960 --> 00:03:29,610
‫the write mode.

33
00:03:29,610 --> 00:03:40,180
‫It will create this file as f: writer equals csv.writer of f

34
00:03:44,150 --> 00:03:58,070
‫I'll add a first row, so a header, writer.writerow and I'll be pass in a list

35
00:03:58,070 --> 00:04:01,160
‫so X the first field, X to the power of 2,

36
00:04:01,190 --> 00:04:11,620
‫this is the 2nd field, X to the power of 3 and X raised to the power of 4.

37
00:04:11,630 --> 00:04:18,320
‫Now I'm going to use a for loop for x in range 1 comma 101.

38
00:04:18,530 --> 00:04:22,460
‫I want to add the numbers from 1 to 100.

39
00:04:22,460 --> 00:04:27,290
‫So 1 is included but 101 is not included.

40
00:04:27,320 --> 00:04:41,480
‫so it' excluded, colon and the 4 block of code writer.writerow of and I'll use a list X comma X

41
00:04:41,770 --> 00:04:51,600
‫raised to the power of 2, x raised to the  power of 3 and X raised to the  power of 4.

42
00:04:51,620 --> 00:05:01,600
‫That's all! I am going to run the script. Now in the same directory a new file named numbers.csv has

43
00:05:01,600 --> 00:05:03,400
‫been created.

44
00:05:03,400 --> 00:05:05,670
‫Everything looks just fine.

45
00:05:05,740 --> 00:05:07,720
‫We see the right numbers.

46
00:05:07,720 --> 00:05:13,600
‫The only problem is that it added an extra new line after each row.

47
00:05:13,690 --> 00:05:22,720
‫If we don't want these extra new line we add another argument to the open function. Here where we

48
00:05:22,780 --> 00:05:24,100
‫open the file

49
00:05:24,100 --> 00:05:32,170
‫we add another argument, an optional argument called the new line, equals and an empty string.

50
00:05:32,200 --> 00:05:37,390
‫By default the new line is a backslash n . I run the script again.

51
00:05:38,690 --> 00:05:40,600
‫And I'll open numbers.csv

52
00:05:40,630 --> 00:05:41,510
‫one more time

53
00:05:41,510 --> 00:05:47,840
‫and there is no extra new line after each row.

54
00:05:47,850 --> 00:05:51,240
‫This is how we write CSV files in Python.


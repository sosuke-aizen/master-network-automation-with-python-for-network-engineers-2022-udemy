﻿1
00:00:00,790 --> 00:00:07,180
‫In the previous lectures we've seen different ways to read text files with Python.

2
00:00:07,180 --> 00:00:11,140
‫Now it's time to see how to write to a text file.

3
00:00:11,140 --> 00:00:13,160
‫The process is simple.

4
00:00:13,210 --> 00:00:18,250
‫There is the right method that writes any string to an open file.

5
00:00:18,250 --> 00:00:24,810
‫This method does not add a new line character so a backslash n to the end of the string.

6
00:00:25,030 --> 00:00:26,260
‫Let's see an example.

7
00:00:29,230 --> 00:00:39,340
‫With open the name of the file myfile.txt   this time the file must be opened in write mode.

8
00:00:39,340 --> 00:00:44,070
‫For this w access mode is used.

9
00:00:46,560 --> 00:00:49,430
‫as f myfile object

10
00:00:49,710 --> 00:00:55,130
‫Colon and the with block of code f.write

11
00:00:55,650 --> 00:00:59,260
‫and a string, just a line

12
00:01:00,850 --> 00:01:03,250
‫and I'll execute this script.

13
00:01:05,300 --> 00:01:08,410
‫In the same directory with my Python script

14
00:01:08,480 --> 00:01:11,400
‫a new text file called myfile.txt

15
00:01:11,480 --> 00:01:16,030
‫appeared.

16
00:01:16,110 --> 00:01:18,120
‫This is the text file.

17
00:01:18,150 --> 00:01:26,580
‫Take care that the file will be created if it doesn't exist and overwritten if it already exists.

18
00:01:26,580 --> 00:01:34,440
‫A very common mistake is to overwrite a file by using the W mode instead of opening the file in append

19
00:01:34,440 --> 00:01:39,360
‫the mode which means adding new content to the end of the file.

20
00:01:39,690 --> 00:01:43,300
‫We'll see some examples with append in a short while.

21
00:01:44,190 --> 00:01:54,560
‫Anyway if I write here let's say a b c and I execute the script again myfile.txt has been overwritten.

22
00:01:54,930 --> 00:01:55,860
‫So take care!

23
00:01:59,250 --> 00:02:04,980
‫Let's write the second line to the file.

24
00:02:05,190 --> 00:02:09,750
‫Just a second line and I'll execute the script.

25
00:02:13,030 --> 00:02:20,710
‫We notice that it didn't add a new line so backslash n;  we need to manually include the backslash n

26
00:02:20,800 --> 00:02:21,550
‫in the string

27
00:02:21,550 --> 00:02:25,190
‫we write to the file if we want new lines.

28
00:02:25,330 --> 00:02:28,960
‫So here I should have written backslash n

29
00:02:31,650 --> 00:02:33,110
‫now it looks okay.

30
00:02:35,720 --> 00:02:38,600
‫Let's see a new example.

31
00:02:38,600 --> 00:02:48,060
‫I'll show you how to preserve the existing content by appending to the end of the file; if we don't want

32
00:02:48,090 --> 00:02:49,760
‫to overwrite the file

33
00:02:49,800 --> 00:03:03,880
‫but to append to the end  we must use the 8 mode which stands for append ; so instead of wI use a

34
00:03:03,940 --> 00:03:05,320
‫Let's execute the script

35
00:03:08,680 --> 00:03:14,170
‫and we see how it added the new content at the the end of the file.

36
00:03:14,170 --> 00:03:23,220
‫This is the new content that has been appended; there wasn't a backslash n after the second line.

37
00:03:25,030 --> 00:03:29,530
‫If I want a backslash n and so a new line I must edit manually

38
00:03:38,300 --> 00:03:41,740
‫and these are the lines that have been appended.

39
00:03:43,870 --> 00:03:51,910
‫When using append mode, Python creates the file if it doesn't exist or append to the end of the file

40
00:03:52,210 --> 00:03:54,100
‫if it already exists.

41
00:03:54,100 --> 00:03:57,650
‫Also note that we don't have access to the cursor.

42
00:03:57,730 --> 00:04:02,600
‫It always adds to the end of the file; if I write here

43
00:04:02,740 --> 00:04:06,620
‫Myfile1- this file doesn't exist -

44
00:04:06,640 --> 00:04:08,310
‫it will create the file

45
00:04:08,890 --> 00:04:21,160
‫This is the new file; it starts with a backslash n and then adds ABC, a new line and this string ,

46
00:04:21,250 --> 00:04:27,760
‫exactly as we see here. In the last example of this lecture

47
00:04:27,880 --> 00:04:33,160
‫I'll show you how to open a file for both reading and writing.

48
00:04:33,370 --> 00:04:39,920
‫For this we use the "R+" access mode. Let's see an example.

49
00:04:41,420 --> 00:04:53,260
‫With open the name of the file configuration.txt and I am opening the file for both reading and

50
00:04:53,260 --> 00:04:54,880
‫writing r+ as f

51
00:04:55,030 --> 00:05:11,700
‫and now f. write line added with r+ \n and I'll execute the script.

52
00:05:14,340 --> 00:05:21,110
‫And surprise:  we see how it added the content at the beginning of the file

53
00:05:21,230 --> 00:05:22,570
‫not at the end

54
00:05:25,630 --> 00:05:32,830
‫if we want to add content to a specific position we can use the cursor which is available when using

55
00:05:32,890 --> 00:05:36,900
‫the r+ mode

56
00:05:36,900 --> 00:05:38,370
‫Let's see an example.

57
00:05:38,600 --> 00:05:49,620
‫f.seek let's say 5 I am moving the cursor on position 5 and then f.write

58
00:05:49,710 --> 00:05:50,960
‫Let's say 100

59
00:05:55,870 --> 00:05:57,720
‫and we see how it wrote

60
00:05:57,760 --> 00:06:03,370
‫The value 100 starting from position 5

61
00:06:03,460 --> 00:06:06,090
‫One two three four five

62
00:06:09,190 --> 00:06:18,670
‫As I said earlier we can both read and write to a file when opening the file using  r+ mode.

63
00:06:19,210 --> 00:06:31,280
‫f.seek10 and then print f.read and I am reading three characters starting from position

64
00:06:31,490 --> 00:06:31,820
‫10

65
00:06:37,320 --> 00:06:49,560
‫and these are the characters: a white space, W and I; you should also know that when using the r+ mode

66
00:06:49,650 --> 00:06:51,940
‫the file must already exist.

67
00:06:52,020 --> 00:06:54,180
‫Otherwise you'll get an error.

68
00:06:54,180 --> 00:06:58,170
‫Python doesn't create the file; if I write here

69
00:06:58,170 --> 00:07:01,470
‫Configuration1.txt of course

70
00:07:01,470 --> 00:07:03,240
‫this file doesn't exist.

71
00:07:03,240 --> 00:07:12,450
‫I'll get an error: no such file or directory configuration1.txt  okay.

72
00:07:12,470 --> 00:07:16,730
‫This is basically how we write to text files with Python.


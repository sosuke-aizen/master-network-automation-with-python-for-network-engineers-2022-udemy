﻿1
00:00:00,660 --> 00:00:07,080
‫In this section we'll start a series of videos on working with text files.

2
00:00:07,080 --> 00:00:14,100
‫If you are working in the area of network automation,system administration, data science or machine learning

3
00:00:14,430 --> 00:00:22,500
‫reading and writing files is very very common. In this lecture we'll see how to read text files with

4
00:00:22,500 --> 00:00:26,710
‫Python. In order to read or write files

5
00:00:26,740 --> 00:00:28,900
‫you don't need to import the module.

6
00:00:29,110 --> 00:00:33,390
‫It's handled natively in the language.

7
00:00:33,470 --> 00:00:41,750
‫The first thing you'll need to do is to use Python's built in open function to get the file object.

8
00:00:41,750 --> 00:00:44,150
‫When you use the open function

9
00:00:44,210 --> 00:00:48,180
‫it returns something called a file object.

10
00:00:48,200 --> 00:00:55,520
‫File objects contain methods and attributes that can be used to collect information about the file you opened

11
00:00:55,790 --> 00:00:57,680
‫and to manipulate the file.

12
00:00:59,070 --> 00:01:08,580
‫Let's create a file object simply called F equals open the function that returns the file object

13
00:01:09,180 --> 00:01:12,380
‫and its first argument is the file name.

14
00:01:13,680 --> 00:01:23,060
‫In this example there is a file in the current working directory called configuration.txt

15
00:01:23,360 --> 00:01:29,510
‫Here you should use a valid absolute or relative path according to your operating system.

16
00:01:29,510 --> 00:01:40,030
‫Please note that using an invalid path is a common mistake.

17
00:01:40,220 --> 00:01:47,770
‫Then the second argument which is optional is called Access mode or simply mode and indicates how the

18
00:01:47,770 --> 00:01:49,750
‫file will be opened.

19
00:01:49,840 --> 00:01:57,220
‫The most used modes are: read, write and append. We'll see examples with different modes

20
00:01:57,220 --> 00:02:01,590
‫but for the moment let's open the file in read only mode.

21
00:02:02,590 --> 00:02:04,600
‫This is also the default

22
00:02:04,870 --> 00:02:11,870
‫so if you don't specify a mode it will be opened in read only mode.

23
00:02:12,010 --> 00:02:17,760
‫You should know that in Python a file is categorized as either text or binary

24
00:02:18,010 --> 00:02:21,880
‫and the difference between the two file types is important.

25
00:02:22,820 --> 00:02:31,880
‫Text files are structured as sequence of lines where each line includes a sequence of characters and

26
00:02:31,910 --> 00:02:37,640
‫terminates with a special character called E O L or end of line.

27
00:02:37,640 --> 00:02:46,130
‫This is coded as a back slash n. A binary file is any type of file  that's not a text file. Because

28
00:02:46,190 --> 00:02:53,980
‫of their nature binary files can only be processed by an application that knows or understands

29
00:02:54,260 --> 00:03:04,890
‫the file's structure. We can specify if we work with a text file using the t letter or B if we work

30
00:03:04,980 --> 00:03:15,910
‫with a binary file. Binary files are photos , pdf files, executable files and so on. By default

31
00:03:15,930 --> 00:03:20,040
‫Python considers the file as being a text file.

32
00:03:21,370 --> 00:03:28,280
‫Now having the file opened let's see to access and print out the data within the file.

33
00:03:28,390 --> 00:03:31,900
‫The first method will take a look at is read.

34
00:03:31,900 --> 00:03:42,880
‫It reads and returns the entire content of the file as a string. content equals f .read and print content

35
00:03:43,060 --> 00:03:46,510
‫let's execute the script

36
00:03:49,600 --> 00:03:58,070
‫and we see how it read and printed out the entire content of the file. When we are all done with the

37
00:03:58,070 --> 00:03:58,820
‫file

38
00:03:58,820 --> 00:04:00,630
‫we must close it.

39
00:04:00,650 --> 00:04:04,850
‫There is an attribute of the file object called closed,

40
00:04:04,940 --> 00:04:05,650
‫that is true,

41
00:04:05,660 --> 00:04:11,600
‫if the file is closed and false otherwise.Let's see its value.

42
00:04:11,750 --> 00:04:17,140
‫f.closed and I'll execute the script.

43
00:04:18,290 --> 00:04:21,230
‫And the value of this attribute is false.

44
00:04:21,770 --> 00:04:28,070
‫This means that the file is not closed. In order to close the file

45
00:04:28,070 --> 00:04:37,700
‫I must call the close method f.close and I'll print the value of the closed attribute one more

46
00:04:37,700 --> 00:04:38,050
‫time.

47
00:04:42,170 --> 00:04:49,070
‫We see that after calling the closed method, the closed attribute is true.

48
00:04:49,070 --> 00:04:54,780
‫This means that here the file is already closed.

49
00:04:54,790 --> 00:04:59,160
‫This was the basics of reading files with Python.

50
00:04:59,440 --> 00:05:06,340
‫In the next lectures we'll see many other examples on how to handle files with Python.


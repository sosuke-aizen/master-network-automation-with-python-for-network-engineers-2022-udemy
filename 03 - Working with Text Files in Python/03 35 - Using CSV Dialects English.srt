﻿1
00:00:00,330 --> 00:00:07,530
‫In the last lecture, I've shown you how to read the CSB files where another delimiter is used, for

2
00:00:07,530 --> 00:00:16,710
‫example, we had this file where a colon is used as a delimiter and using some optional arguments based

3
00:00:16,710 --> 00:00:18,360
‫in targeter function.

4
00:00:18,600 --> 00:00:20,040
‫We've read the file.

5
00:00:21,490 --> 00:00:29,830
‫Now, let's take a look at dialects, the Python service module comes with some built-In dialects.

6
00:00:30,160 --> 00:00:38,830
‫A dialect describes the usual properties of CACP files such as the delimiter, the quoting mechanism,

7
00:00:39,070 --> 00:00:42,730
‫the escape character into the line Terminator.

8
00:00:43,720 --> 00:00:50,230
‫As we've seen in the last example, we can perceive these individual parameters as arguments to the

9
00:00:50,230 --> 00:00:58,300
‫reader and the writer functions of the CAC module, or we can group all these properties of these files

10
00:00:58,570 --> 00:01:00,370
‫in custom dialects.

11
00:01:02,390 --> 00:01:10,520
‫To see the existing dialects we call the least dialects function of the sea as we module print sea as

12
00:01:10,520 --> 00:01:12,260
‫we do, not least dialects.

13
00:01:17,050 --> 00:01:25,090
‫And we see there are three different dialects, Excel, Excel, Temp and Unix now will take a look at

14
00:01:25,090 --> 00:01:28,720
‫how to create our own custom with dialects.

15
00:01:30,650 --> 00:01:36,950
‫We have this very same policy as we file that uses a harsh character is a delimiter.

16
00:01:44,540 --> 00:01:51,330
‫First, I'm going to register a dialect since we don't register dialect.

17
00:01:51,350 --> 00:01:56,180
‫The first argument is the name of the dialect in this case hashes.

18
00:01:57,880 --> 00:02:01,030
‫And now the properties of this dialect.

19
00:02:02,450 --> 00:02:05,480
‫That limit, there will be a harsh character.

20
00:02:08,380 --> 00:02:09,910
‫There is no quoting.

21
00:02:11,700 --> 00:02:20,130
‫We don't, quote, underline none, this means no quoting and the line Terminator equals pixelation.

22
00:02:22,120 --> 00:02:30,940
‫The second step is to open the file, the CSB file, so with open the name of the file items dot GSV.

23
00:02:33,650 --> 00:02:40,050
‫Of course, I'll open the file in readonly mode, so R as C as V file.

24
00:02:41,780 --> 00:02:49,850
‫Now I am creating the return object reader equals she has vetoed the return of C we file.

25
00:02:50,850 --> 00:03:00,110
‫And the dialect is specified with the dialect argument in the case guitar method, so dialect equals

26
00:03:00,450 --> 00:03:05,670
‫and the name of my new custom dialect, which is hashes.

27
00:03:07,340 --> 00:03:17,720
‫And I reiterate, is usually over the meter object printing line by line, so for row in Reidar print

28
00:03:18,020 --> 00:03:18,500
‫row.

29
00:03:20,550 --> 00:03:27,600
‫Let's execute the script and we see how it has correctly passed and read the CSV file.

30
00:03:33,570 --> 00:03:39,540
‫Now, if you want to write to this file, you piss in the dialect to the right, their method of the

31
00:03:39,540 --> 00:03:42,750
‫service module, Lexia Chaat example.

32
00:03:44,830 --> 00:03:49,690
‫I'll open the file one more time, this time in append mode.

33
00:03:55,690 --> 00:03:58,330
‫And I am creating the right to object.

34
00:04:02,830 --> 00:04:07,750
‫We thought the writer, the name of the file object in this case, GSV File.

35
00:04:10,730 --> 00:04:14,000
‫And the name of the dialect, which is she's.

36
00:04:16,580 --> 00:04:23,810
‫And I, right or wrong, is usually using the right method, so right there, Dot, right now.

37
00:04:24,980 --> 00:04:32,630
‫And a apple, or at least, let's say at Apple spawn three and one point five.

38
00:04:34,430 --> 00:04:44,270
‫And I'll execute the script now if I check the items NSV, we can see how a new role has been added.

39
00:04:47,140 --> 00:04:49,520
‫This is all about GSV Dialects.

40
00:04:49,810 --> 00:04:50,440
‫Thank you.


﻿1
00:00:00,750 --> 00:00:06,370
‫This lecture is the answer to the assignment given to you in the previous lecture.

2
00:00:06,390 --> 00:00:11,610
‫This is just a recording of me thinking loud and walking through the assignment.

3
00:00:11,670 --> 00:00:17,400
‫Maybe you took another approach and that's fine as long as you get the end result.

4
00:00:17,670 --> 00:00:22,680
‫So here I'll read the content of this file list.

5
00:00:22,860 --> 00:00:27,800
‫So with open the name of the file device.txt

6
00:00:27,990 --> 00:00:29,660
‫this is a relative path

7
00:00:29,730 --> 00:00:31,930
‫The devices.txt file

8
00:00:31,980 --> 00:00:36,120
‫and my Python scripts are in the same directory.

9
00:00:36,120 --> 00:00:41,380
‫These are the files as f and now devices

10
00:00:41,430 --> 00:00:48,920
‫This is my list equals f.read.splitlines

11
00:00:49,020 --> 00:00:56,750
‫f.read will return a string and the splitlines method we'll split that string in a list.

12
00:00:56,760 --> 00:01:05,950
‫This is one of the possibilities I've shown you in a previous lecture on how to read a file in a list.

13
00:01:06,390 --> 00:01:16,640
‫If we want we can print the devices list. As we can see our list contains the lines of my file.

14
00:01:16,710 --> 00:01:18,420
‫This is the first line.

15
00:01:18,480 --> 00:01:20,520
‫This is the second line.

16
00:01:20,700 --> 00:01:22,820
‫The third line and so on.

17
00:01:22,950 --> 00:01:29,880
‫This method is recommended because there is no backslash n. We don't want to have a backslash n

18
00:01:29,910 --> 00:01:38,640
‫by the end of each line. And now I'll process my list, my devices list further. So I'll create

19
00:01:38,730 --> 00:01:41,580
‫a new list  named mylist,

20
00:01:41,580 --> 00:01:48,360
‫this is an empty list, and now I'll iterate through my devices using a for loop.

21
00:01:48,360 --> 00:01:54,980
‫So for item in devices and here I'll use the Split method.

22
00:01:55,020 --> 00:01:57,720
‫So tmp -  this is a temporary variable -

23
00:01:57,720 --> 00:02:00,690
‫This is also a list

24
00:02:00,760 --> 00:02:09,670
‫item.split and here will have a colon as a separator or as a delimiter.

25
00:02:09,840 --> 00:02:11,140
‫What does it mean?

26
00:02:11,220 --> 00:02:14,200
‫It will split this element,

27
00:02:14,250 --> 00:02:20,780
‫this is the item, in a list least having the colon as a separator.

28
00:02:20,890 --> 00:02:26,760
‫So tmp will be a new list and this will be the first element of the list.

29
00:02:26,760 --> 00:02:29,330
‫This will be the second element of the list.

30
00:02:29,340 --> 00:02:36,240
‫The third element of the list and the last element of the list. If we want to be sure we can simply

31
00:02:36,240 --> 00:02:39,390
‫print the tmp list.

32
00:02:39,420 --> 00:02:40,920
‫So let's run the script

33
00:02:43,690 --> 00:02:44,910
‫ok, here.

34
00:02:45,180 --> 00:02:46,690
‫There must be a "t".

35
00:02:46,710 --> 00:02:59,110
‫So split. Let's run the script and we can see the first tmp list, the second list, the third list and so

36
00:02:59,110 --> 00:02:59,670
‫on.

37
00:02:59,740 --> 00:03:06,820
‫Each list contains the elements of a line from my file.

38
00:03:07,180 --> 00:03:11,370
‫And of course I'll add this tmp list,

39
00:03:11,380 --> 00:03:14,320
‫this temporary variable, to my list.

40
00:03:14,320 --> 00:03:27,780
‫So instead print tmp I write mylist.append of tmp . And that's all! Let's print here mylist and everything's

41
00:03:27,790 --> 00:03:28,410
‫fine.

42
00:03:28,460 --> 00:03:30,050
‫So I have a list.

43
00:03:30,080 --> 00:03:38,560
‫This is my list and each element of my list is another list with elements from these file.

44
00:03:38,570 --> 00:03:44,310
‫So the host name ,the IP address, the user and the password.

45
00:03:44,420 --> 00:03:48,820
‫The second list, the third and so on.

46
00:03:51,220 --> 00:03:54,600
‫OK that's answer to the assignment.


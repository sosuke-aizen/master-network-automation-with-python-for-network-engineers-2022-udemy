﻿1
00:00:00,660 --> 00:00:03,900
‫In this lecture we'll take a look at how to read

2
00:00:03,900 --> 00:00:06,080
‫C S V files.

3
00:00:06,150 --> 00:00:09,750
‫For that we'll use a sample CSV file called air travel.csv

4
00:00:09,750 --> 00:00:18,900
‫It contains monthly transatlantic air travel in thousands of passengers for nineteen fifty

5
00:00:18,900 --> 00:00:20,850
‫eight to nineteen sixty.

6
00:00:21,240 --> 00:00:30,930
‫There are three fields: month, year nineteen fifty eight , nineteen fifty nine and the nineteen sixty and

7
00:00:30,960 --> 00:00:35,790
‫twelve records: January through December.

8
00:00:35,790 --> 00:00:45,230
‫There is also an initial header line. In order to parse and read this file will use the reader function

9
00:00:45,380 --> 00:00:51,350
‫of the CSV module. First let's import the CSV module.

10
00:00:51,590 --> 00:00:57,890
‫This is a standard library module so we don't have to install it using Pip.

11
00:00:58,060 --> 00:01:07,220
‫We'll open the file as any other text file, so with open the name of the file my CSV file is in the

12
00:01:07,220 --> 00:01:10,710
‫same directory with my Python script.

13
00:01:11,050 --> 00:01:13,740
‫Take care to use a valid path here.

14
00:01:17,550 --> 00:01:23,880
‫The file will be opened in  read only mode in the name of the file object is f.

15
00:01:23,910 --> 00:01:33,420
‫In order to parse and read the this file will use the reader function of the CSV module.

16
00:01:33,420 --> 00:01:41,810
‫This function returns a reader object which will iterate over lines in the given CSV file

17
00:01:41,810 --> 00:01:48,850
‫Each row read from the CSV file is returned as a list of strings and the no automatic data type

18
00:01:48,850 --> 00:01:58,980
‫conversion is performed;  so reader equals csv.reader of f

19
00:01:59,150 --> 00:02:01,110
‫This is my reader object.

20
00:02:01,340 --> 00:02:04,800
‫Now I'll iterate over this object.

21
00:02:05,800 --> 00:02:09,630
‫For row in reader print row

22
00:02:12,390 --> 00:02:16,160
‫I will simply print the content of the file line by line.

23
00:02:21,180 --> 00:02:28,470
‫And this is the content of my CSV file. If I want to display only the second column

24
00:02:29,650 --> 00:02:30,470
‫here

25
00:02:30,600 --> 00:02:33,210
‫I'll use index 1.

26
00:02:33,360 --> 00:02:39,910
‫This is a list so that the element with index 1 will be in fact the second field.

27
00:02:43,190 --> 00:02:52,910
‫And these are the monthly flights in nineteen fifty eight; the reader object is an iterable

28
00:02:52,970 --> 00:03:00,810
‫so we can skip the header if we'd like using the built in function call next; if I don't want to

29
00:03:00,810 --> 00:03:12,500
‫display the header here, before iterating over the reader object, I write next off reader and it will

30
00:03:12,500 --> 00:03:22,940
‫skip the first line. Now let's calculate what was the busiest month in nineteen fifty eight.

31
00:03:22,940 --> 00:03:23,270
‫For that

32
00:03:23,300 --> 00:03:29,120
‫I'll create a dictionary called year _1958

33
00:03:33,890 --> 00:03:42,200
‫and I'll populate the dictionary this way: the key will be the month and the value - the number of flights in that month.

34
00:03:42,530 --> 00:03:51,910
‫So year 1958 of row of 0.

35
00:03:52,020 --> 00:03:54,810
‫This is the first field, in fact

36
00:03:54,860 --> 00:04:07,260
‫this is the month equals row of 1 so the element with index 1

37
00:04:07,270 --> 00:04:07,900
‫Let's print the dictionary to see its content.

38
00:04:13,520 --> 00:04:16,000
‫And everything looks just fine.

39
00:04:17,290 --> 00:04:21,850
‫Now to calculate the busiest month in 1958

40
00:04:21,850 --> 00:04:22,930
‫I'll do the following:

41
00:04:24,120 --> 00:04:36,890
‫I'll create a new variable called max_1958 equals max of year1958.values

42
00:04:37,250 --> 00:04:47,040
‫I remind you that the values method returns a list that contains the values of the dictionary

43
00:04:47,430 --> 00:04:53,740
‫and using the max function I'll get the maximum value from the dictionary.

44
00:04:54,910 --> 00:04:56,530
‫Let's print its value.

45
00:04:59,880 --> 00:05:07,600
‫And in the busiest month 505 flights took place.

46
00:05:07,670 --> 00:05:11,250
‫Now let's print also the month.

47
00:05:11,250 --> 00:05:18,390
‫For that I'll iterate over that dictionary this way: for K , V in year 1958

48
00:05:18,390 --> 00:05:19,890
‫.items

49
00:05:22,840 --> 00:05:28,490
‫if max_1958 is equal to V.

50
00:05:28,540 --> 00:05:33,200
‫So this is the maximum value - I'll print the message.

51
00:05:33,400 --> 00:05:36,340
‫Busiest month in 1958

52
00:05:39,530 --> 00:05:52,540
‫colon and K the key between a pair of curly braces comma flights and V between a pair of curly braces

53
00:05:55,220 --> 00:06:05,750
‫I'll comment out this line and this line, I want to see only the busiest month. And the busiest month

54
00:06:05,870 --> 00:06:15,700
‫in 1958 was August with this number of flights. We can see here in front of this

55
00:06:15,700 --> 00:06:18,610
‫value a number of white spaces.

56
00:06:18,730 --> 00:06:28,730
‫This is because the CSV file is structured this way. In order to move these white spaces I can use

57
00:06:28,760 --> 00:06:38,450
‫here the strip method, V is a variable that stores a string and strip is a string method

58
00:06:45,890 --> 00:06:52,220
‫and it displayed the number of flights without white spaces in front of the value.

59
00:06:55,170 --> 00:07:02,080
‫in the next lecture I'll show you how to write to see CSV files. See you in just a few seconds.

60
00:07:02,130 --> 00:07:02,630
‫Thank you.


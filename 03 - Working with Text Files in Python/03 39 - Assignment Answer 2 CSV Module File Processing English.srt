﻿1
00:00:00,360 --> 00:00:05,330
‫Another approach to the assignment is to use the CSV module.

2
00:00:05,600 --> 00:00:09,880
‫The CSV module belongs to the Python standard library

3
00:00:09,900 --> 00:00:12,680
‫so there is no need to install it.

4
00:00:12,690 --> 00:00:21,720
‫I'll comment this code and here I'll write my second answer to the assignment.

5
00:00:21,720 --> 00:00:24,310
‫So import csv

6
00:00:24,590 --> 00:00:34,140
‫I am importing the  csv module and now with open devices.txt as f from file and now

7
00:00:34,300 --> 00:00:36,810
‫csv.reader.

8
00:00:36,840 --> 00:00:48,630
‫I am using reader method of csv module F, our file, and delimiter equals colon -  by default CSV uses comma

9
00:00:48,720 --> 00:00:49,820
‫as a delimiter.

10
00:00:49,950 --> 00:00:59,640
‫But in our case the delimiter is a colon character so it's mandatory to use delimiter equals colon

11
00:00:59,640 --> 00:01:06,870
‫between single codes and reader method returns a reder object.

12
00:01:06,870 --> 00:01:17,520
‫So let's write here reader equals csv.reader and for row in reader  print row

13
00:01:18,000 --> 00:01:23,500
‫So I'll iterate through the reader object line by line.

14
00:01:23,810 --> 00:01:26,030
‫And now I run the script.

15
00:01:26,620 --> 00:01:28,890
‫Ok this is the first element.

16
00:01:28,950 --> 00:01:33,680
‫The second elements, so the second row, the third row and so on.

17
00:01:33,780 --> 00:01:43,480
‫As you can see each row is a list and each element from the list is an element from my file.

18
00:01:43,500 --> 00:01:47,400
‫So the host name, the IP address, the user name and the password.

19
00:01:47,670 --> 00:01:55,120
‫If I want to have a list that contains these lists I can create a new list here.

20
00:01:55,120 --> 00:01:58,170
‫so let's say my list equals list.

21
00:01:58,170 --> 00:01:59,800
‫This is an empty list.

22
00:02:00,210 --> 00:02:07,100
‫And here I write mylist.append row

23
00:02:07,320 --> 00:02:17,660
‫So here I am adding the row which is a list in my list and let's print my list.

24
00:02:17,670 --> 00:02:24,780
‫I'm running the script and we can see here the same result as from the previous lecture.

25
00:02:24,780 --> 00:02:28,770
‫This is my second answer to the assignment.


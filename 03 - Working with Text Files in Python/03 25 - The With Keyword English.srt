﻿1
00:00:02,670 --> 00:00:09,360
‫In the last two lectures we've seen how to open a file and how to read the content of the file as a

2
00:00:09,360 --> 00:00:09,860
‫string.

3
00:00:11,400 --> 00:00:19,140
‫You should always remember to close the file by calling the close method after you are done with it.

4
00:00:19,140 --> 00:00:24,770
‫In this video I'll show you another way to open and close files.

5
00:00:24,810 --> 00:00:31,450
‫In fact this is the really commonly used and recommended way to deal with files.

6
00:00:31,590 --> 00:00:36,870
‫I'll comment out of the entire code and do it again in another way.

7
00:00:38,620 --> 00:00:49,270
‫With open the name of the file - we have the same file - configuration.txt -  we open the file in

8
00:00:49,430 --> 00:01:02,580
‫read only mode as file: file.read and after that, outside the with block of code, I write

9
00:01:02,580 --> 00:01:06,420
‫print file.closed.

10
00:01:06,630 --> 00:01:11,340
‫I want to see if the file is opened or closed.

11
00:01:11,350 --> 00:01:15,970
‫Of course if I want to see the content of the file I must printed

12
00:01:21,270 --> 00:01:29,640
‫We notice how it read and printed out the entire content of the file but also closed the file automatically.

13
00:01:30,770 --> 00:01:40,970
‫We see this by printing the value of closed attribute of our file object and this is true. As the conclusion

14
00:01:40,980 --> 00:01:48,120
‫if you are not using the with keyword then you should call f.close to manually closed the file.

15
00:01:48,510 --> 00:01:49,850
‫In our scripts

16
00:01:49,860 --> 00:01:55,500
‫we'll use the keyword and the won't close the file manually.

17
00:01:55,560 --> 00:02:04,320
‫Please note that outside the with block of code and this consists of lines with the same indentation

18
00:02:04,410 --> 00:02:14,380
‫level, outside the file it's already closed. If we try for example to read the file outside the with

19
00:02:14,380 --> 00:02:19,420
‫block of code we'll get an error file.read

20
00:02:23,330 --> 00:02:28,790
‫I /O operation on closed file

21
00:02:28,810 --> 00:02:39,150
‫on line 23 the file is already closed. This being said you should use this structure to read or write files and

22
00:02:39,190 --> 00:02:40,440
‫not this one.


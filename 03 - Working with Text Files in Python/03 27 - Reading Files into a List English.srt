﻿1
00:00:00,780 --> 00:00:06,900
‫In this lecture we'll take a look at how to read a file into a list.

2
00:00:06,900 --> 00:00:14,760
‫In Python the list is the most versatile data structure and working with list is extremely common; in many applications

3
00:00:14,790 --> 00:00:17,300
‫where file processing is required,

4
00:00:17,310 --> 00:00:23,730
‫we'll start by reading the content of the file into a list, process the list and write it back to the

5
00:00:23,730 --> 00:00:24,870
‫file.

6
00:00:24,870 --> 00:00:28,390
‫There are more possibilities to read the file into a list.

7
00:00:28,740 --> 00:00:38,170
‫Let's see the first example: I am opening the file using the with keyword: the name of the file configuration

8
00:00:38,340 --> 00:00:39,640
‫.txt'

9
00:00:39,910 --> 00:00:44,540
‫And I don't specify the mode because I am opening it in

10
00:00:44,540 --> 00:00:46,570
‫read only mode and  that is the default.

11
00:00:46,570 --> 00:00:48,740
‫as file

12
00:00:50,540 --> 00:00:58,250
‫In this example we'll use the same read method we've seen before and the split lines method.

13
00:00:58,670 --> 00:01:06,500
‫The split lines method splits the string at line breaks and returns a list of lines in the string.

14
00:01:07,610 --> 00:01:17,880
‫So my list equals file dot read dot split lines and I'll print out my list

15
00:01:22,060 --> 00:01:30,580
‫and we see the content of the file in a list. Each line of the file is in fact an element of the list

16
00:01:31,880 --> 00:01:37,270
‫Another way to read a file into a list is to use the read the lines method.

17
00:01:37,350 --> 00:01:47,290
‫This is a method of any file object. It reads until end of file and returns a list containing the lines.

18
00:01:47,490 --> 00:01:56,120
‫I open the file again, the file has been closed so I can't use the same object.

19
00:01:56,120 --> 00:01:58,750
‫The file will be opened in read only mode,

20
00:01:58,760 --> 00:02:14,550
‫this is the default, as file and my list equals file dot read lines print my list and I'll execute the

21
00:02:14,550 --> 00:02:15,070
‫script.

22
00:02:16,440 --> 00:02:23,880
‫We notice that at the end of each element we see a backslash n which is a new line.

23
00:02:23,880 --> 00:02:32,250
‫This is one difference between using file dot read dot split lines and using file dot read lines.

24
00:02:34,040 --> 00:02:36,740
‫There is also the read line method.

25
00:02:36,800 --> 00:02:43,540
‫So without an ending s that reads just a line, not the entire file.

26
00:02:43,610 --> 00:02:44,750
‫Let's see an example

27
00:02:48,410 --> 00:02:55,610
‫print file dot read line and I'll call the method one more time.

28
00:03:00,020 --> 00:03:09,180
‫We see how  it read and printed out the first two lines of the file; also notice that a file object

29
00:03:09,240 --> 00:03:10,960
‫is an iterable object.

30
00:03:11,040 --> 00:03:14,970
‫We can iterate over it, line by line using a for loop

31
00:03:18,590 --> 00:03:19,670
‫In this example

32
00:03:19,700 --> 00:03:28,740
‫I write for line in file colon print line and I'll execute the script.

33
00:03:29,980 --> 00:03:33,230
‫And this is my file line by line.

34
00:03:34,840 --> 00:03:39,020
‫Of course the print function adds a backslash n at the end of

35
00:03:39,020 --> 00:03:47,890
‫each printed line. If I don't want a backslash n I can use the end argument equals and

36
00:03:47,890 --> 00:03:48,690
‫an empty string.

37
00:03:53,150 --> 00:03:57,290
‫These were some ways to read a file into a list.


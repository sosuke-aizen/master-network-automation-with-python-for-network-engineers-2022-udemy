﻿1
00:00:00,970 --> 00:00:08,530
‫In this lecture, we'll see how to disengage from Jason into Python objects, and I'll create a new

2
00:00:08,530 --> 00:00:10,000
‫Python script for this.

3
00:00:13,210 --> 00:00:16,390
‫The first step is to import that exists, Morgan.

4
00:00:18,440 --> 00:00:27,730
‫Then I'll open the file in read-only mode with open the name of the file you name is Friends Dodgson

5
00:00:27,730 --> 00:00:29,920
‫and extend the current working directory.

6
00:00:32,950 --> 00:00:42,240
‫And it will be opened in Read-Only mouth and expects to fight any way, these two options are default

7
00:00:42,430 --> 00:00:43,720
‫for the open function.

8
00:00:43,960 --> 00:00:46,150
‫So you can write to them or not.

9
00:00:49,550 --> 00:00:58,970
‫To the serializer or lowed a python object, we use the load function from Jason Mogil so oh B.J. from

10
00:00:58,970 --> 00:01:07,070
‫Object equals Jason Dot lot and it has one argument the file object.

11
00:01:08,060 --> 00:01:12,710
‫And I'm going to point out the type in the value of the object.

12
00:01:18,070 --> 00:01:21,850
‫This is the Jason file, and I'll execute the script.

13
00:01:23,530 --> 00:01:27,970
‫And we've got a ticket with this value in Python.

14
00:01:29,320 --> 00:01:38,950
‫As with damp and damp, there is also load and the load is low to this, she realizes from file and

15
00:01:38,950 --> 00:01:42,910
‫the load as DC realizes from Jason String.

16
00:01:44,250 --> 00:01:49,840
‫To show you this, I'll take this Jason Spring into a string python variable.

17
00:01:50,670 --> 00:01:59,340
‫So Jason String equals and I'll paste the adjacent string enclosed by people code.

18
00:01:59,760 --> 00:02:02,910
‫So, in fact, this is a multiline string.

19
00:02:05,010 --> 00:02:16,050
‫And I want to load the string into a Python dictionary, so object equals Jason Dot lowed s and the

20
00:02:16,050 --> 00:02:17,910
‫argument is that Jason Sprink.

21
00:02:19,970 --> 00:02:24,380
‫And I'm pointing out the type and the value of OPG.

22
00:02:30,520 --> 00:02:41,830
‫I am executing the script and I've got a big variable with this value in the Jason introduction, I

23
00:02:41,830 --> 00:02:46,270
‫said that Jason does not support all Python data types.

24
00:02:47,380 --> 00:02:49,090
‫Let me show you what I meant.

25
00:02:50,200 --> 00:02:58,540
‫Let's serialize the object again, but this time the dictionary values will be of type Oppal, not the

26
00:02:58,540 --> 00:02:58,930
‫least.

27
00:03:01,000 --> 00:03:06,610
‫So I'm replacing square brackets with open and closing parentheses.

28
00:03:11,540 --> 00:03:18,920
‫Now, the values are of type toppled, and I am executing the signalization script again.

29
00:03:22,820 --> 00:03:28,710
‫Now back to the script that disguises the Jason turning into a Python object.

30
00:03:28,940 --> 00:03:31,550
‫I'll comment about these lines.

31
00:03:34,860 --> 00:03:38,180
‫OK, and I'll execute the script again.

32
00:03:42,990 --> 00:03:50,730
‫And we noticed that the Topal pipe was not preserved when loading into a python object.

33
00:03:52,290 --> 00:03:59,730
‫Even though the dictionary values were of Type Topal, when the loathing from the adjacent string,

34
00:03:59,910 --> 00:04:02,400
‫we've got the least type, not topple.

35
00:04:03,740 --> 00:04:09,800
‫This means that the Jason types are not hundred percent compatible with Python types.

36
00:04:10,370 --> 00:04:13,400
‫Jason defines its own data type.

37
00:04:13,400 --> 00:04:20,450
‫According to this table you are seeing right now in this table I have created for you, you will see,

38
00:04:20,450 --> 00:04:26,690
‫for example, that a Python Liste or Topal has X corresponding Jason type name or a.

39
00:04:27,970 --> 00:04:35,230
‫Serializing of pople, or at least produces adjacent urry and the then the serializing, the adjacent

40
00:04:35,230 --> 00:04:37,690
‫urry produces a least, not a.

41
00:04:38,800 --> 00:04:44,070
‫Also note that Python asset type is not serializable at all.

42
00:04:45,480 --> 00:04:53,370
‫So if you want to serialize a set, you should first convert the set to a list and then serialize the

43
00:04:53,370 --> 00:04:55,980
‫list or use Pikul instead.

44
00:04:57,740 --> 00:05:03,620
‫If I add another key value pair and the value is of typeset like these.

45
00:05:07,190 --> 00:05:15,770
‫This is a said and I try to serialize this dictionary using JSON, I'll get another object of types.

46
00:05:15,800 --> 00:05:18,350
‫It is not Jason serializable.

47
00:05:19,570 --> 00:05:24,970
‫In this case, you use Pikul or convert the said Paulist.

48
00:05:29,090 --> 00:05:34,550
‫OK, this is how we load Jason encoded strings into Python objects.

49
00:05:34,730 --> 00:05:35,360
‫Thank you.


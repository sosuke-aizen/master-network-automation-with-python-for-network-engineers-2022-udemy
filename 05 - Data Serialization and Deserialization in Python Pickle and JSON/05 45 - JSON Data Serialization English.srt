﻿1
00:00:01,040 --> 00:00:08,060
‫In the last lecture we've discussed about serializing and the serializing Python objects, using Pikul

2
00:00:08,420 --> 00:00:15,710
‫Mike's time to take a look at another way to serialize the data in this video, we'll use Jason, which

3
00:00:15,710 --> 00:00:22,540
‫comes from JavaScript object annotation and is a lightweight data interchange format.

4
00:00:22,970 --> 00:00:28,910
‫It is easy for humans to read and write in for machines to pass and generate.

5
00:00:29,630 --> 00:00:34,800
‫Jason has become the de facto standard for information exchange.

6
00:00:35,240 --> 00:00:42,320
‫Perhaps you are gathering information through an API or storing your data from Python in a database

7
00:00:42,500 --> 00:00:43,550
‫like Mongo be.

8
00:00:44,510 --> 00:00:52,370
‫So you'll go with Jason if you want interoperability between applications outside of the Python ecosystem

9
00:00:52,610 --> 00:00:55,850
‫or you want a text format to store your data.

10
00:00:56,690 --> 00:01:00,500
‫Also note that Jason is more secure than Pikul.

11
00:01:00,800 --> 00:01:08,480
‫Unlike Pickell destabilisation and trusted Jason does not create an arbitrary code execution vulnerability.

12
00:01:09,620 --> 00:01:18,680
‫With all of that said, it seems that Jason is preferrable over Pikul and the next in most cases, however,

13
00:01:18,810 --> 00:01:20,630
‫there is one major drawback.

14
00:01:21,020 --> 00:01:28,940
‫Jason, by default, can only represent a subset of the Python builtin types and to custom classes,

15
00:01:29,420 --> 00:01:34,040
‫Bikal can represent an extremely large number of python types.

16
00:01:34,910 --> 00:01:43,280
‫So in a nutshell, you use Piccola when you are not interested in interoperability and security is not

17
00:01:43,280 --> 00:01:45,080
‫of any concern to you.

18
00:01:45,320 --> 00:01:53,420
‫And you want to load the same data pipes that have been serialized back into python objects and use

19
00:01:53,420 --> 00:01:54,140
‫JSON.

20
00:01:54,350 --> 00:02:01,850
‫When you need interoperability between applications, you have security concerns and you are not interested

21
00:02:01,850 --> 00:02:04,550
‫in preserving every Python data type.

22
00:02:04,880 --> 00:02:09,750
‫We'll talk more about this later when I'll show you some examples.

23
00:02:10,190 --> 00:02:13,310
‫Let's go coding and dive into Jason.

24
00:02:15,050 --> 00:02:17,450
‫Python supports Jason natively.

25
00:02:18,310 --> 00:02:24,720
‫It comes with a built in module called Jason for encoding and decoding JSON data.

26
00:02:26,180 --> 00:02:29,450
‫And I've already imported packages and module.

27
00:02:30,480 --> 00:02:37,020
‫We'll use the same Python dictionary from the previous lecture and try to encode it in Jason format.

28
00:02:38,470 --> 00:02:47,050
‫The Jason module has two functions for serializing Python objects into JSON format dump, which saves

29
00:02:47,050 --> 00:02:56,710
‫the data to file on disk and dump s, which takes the Python object and returns adjacent encoded string.

30
00:02:57,310 --> 00:02:59,740
‫Let's see the first method dump.

31
00:03:01,300 --> 00:03:10,660
‫The first step is to open a file, a text file in rightmost with open the name of the file, let's say

32
00:03:10,660 --> 00:03:12,520
‫friends Dot Jason.

33
00:03:14,590 --> 00:03:22,990
‫This is a text file, and I'll open the file in rightmost, and the name of the file object is F.

34
00:03:24,500 --> 00:03:28,140
‫And I'm going to call the Tampa function from Jason Mogil.

35
00:03:28,310 --> 00:03:34,850
‫So, Jason, don't dump the first argument is the Python object friends.

36
00:03:36,230 --> 00:03:43,610
‫And the second argument, the final object, if and I am executing the script.

37
00:03:45,640 --> 00:03:46,750
‫Let's check it.

38
00:03:48,510 --> 00:03:55,470
‫We see a new file called Friends Daughter Jason in the current working directory called Data Signalization.

39
00:03:55,800 --> 00:04:03,900
‫And inside that file, we see a strong representation of that dictionary in JSON format.

40
00:04:07,900 --> 00:04:13,820
‫Sometimes we just want a strong representation of the Python object in some format.

41
00:04:14,200 --> 00:04:16,570
‫There is another function called dump.

42
00:04:17,110 --> 00:04:24,640
‫So with an ending as that takes into python object and returns adjacent encoded string.

43
00:04:26,150 --> 00:04:33,310
‫Jason, underline strength, a new variable equals Jason don't dump.

44
00:04:33,360 --> 00:04:37,220
‫Yes, and the argument is the python object.

45
00:04:39,340 --> 00:04:44,710
‫And I'm pointing out the Jason Strong point, Jason String.

46
00:04:46,970 --> 00:04:48,880
‫I will execute the script again.

47
00:04:51,530 --> 00:04:58,640
‫And we see here the string representation of the Python object encoded in JSON format.

48
00:04:59,980 --> 00:05:07,750
‫Remember that at the beginning of this lecture, I said that the Jason is meant to be easily readable

49
00:05:07,840 --> 00:05:08,730
‫by humans.

50
00:05:10,590 --> 00:05:14,200
‫In this example, we have just a very simple dictionary.

51
00:05:14,550 --> 00:05:23,460
‫Imagine what would Bergsten's think look like if we serialize tons of information, it will be all squished

52
00:05:23,460 --> 00:05:25,890
‫together and the not very readable.

53
00:05:26,950 --> 00:05:34,780
‫Luckily, there is another argument called indent that controls the indentation the size of a nested

54
00:05:34,780 --> 00:05:35,620
‫structures.

55
00:05:37,550 --> 00:05:41,030
‫And I'll use another argument called intent.

56
00:05:42,090 --> 00:05:43,590
‫And the value for.

57
00:05:44,780 --> 00:05:48,560
‫This means I'll use force bases for indentation.

58
00:05:50,090 --> 00:05:52,250
‫And I'll execute the script again.

59
00:05:54,410 --> 00:05:55,910
‫And we see the difference.

60
00:05:57,280 --> 00:06:04,420
‫It's more readable, we can use the same argument also for dump s function.

61
00:06:08,790 --> 00:06:10,680
‫And we noticed the difference.

62
00:06:12,230 --> 00:06:17,870
‫OK, this is how we serialize data from Python into Jason String's.

63
00:06:19,350 --> 00:06:26,640
‫In the next lecture, we'll see how to disagree allies or to load Jason String's into Python objects.

64
00:06:27,360 --> 00:06:28,880
‫See you in two seconds.


﻿1
00:00:01,720 --> 00:00:07,870
‫Now that you know how to work with Jason encoded strings, I want to give you an assignment.

2
00:00:08,480 --> 00:00:16,630
‫There is a website at this address, Jason Placeholder, that type code dot com, which is in fact an

3
00:00:16,630 --> 00:00:20,520
‫online rest API for testing and prototyping.

4
00:00:21,160 --> 00:00:27,850
‫You can play with this free online rest API, which is great for learning or testing different libraries

5
00:00:28,060 --> 00:00:29,050
‫like pythons.

6
00:00:29,080 --> 00:00:29,740
‫Jason.

7
00:00:30,870 --> 00:00:39,070
‫What I want you to do is to make a request to the Jason Placeholder API for a to do list.

8
00:00:39,120 --> 00:00:42,000
‫This is the list encoded in Jason.

9
00:00:43,170 --> 00:00:51,570
‫We'll make the request using a Python library called requests, which in fact implements the HTP protocol

10
00:00:51,570 --> 00:00:52,270
‫in Python.

11
00:00:53,130 --> 00:01:00,240
‫This is not a Python standard library and you need to install it using beep or directly in picture if

12
00:01:00,240 --> 00:01:02,130
‫you use a virtual environment.

13
00:01:02,610 --> 00:01:04,830
‫I'll show you how to do it in a minute.

14
00:01:06,090 --> 00:01:14,880
‫After making the API request, I want you to load the Jason serialized data into a Python object, print

15
00:01:14,880 --> 00:01:16,240
‫its type in value.

16
00:01:16,590 --> 00:01:24,960
‫Then you'll see that there is a field in the list of to DOS called implemented, which is equal to false

17
00:01:25,050 --> 00:01:25,650
‫or to.

18
00:01:26,770 --> 00:01:33,610
‫I want you to point out only the completed tattoos, so the tattoos that have completed equals to.

19
00:01:34,560 --> 00:01:41,880
‫OK, that's the assignment I have for you in the next video, I'll show you how would I do it?

20
00:01:41,880 --> 00:01:43,770
‫In fact, we'll do it together.


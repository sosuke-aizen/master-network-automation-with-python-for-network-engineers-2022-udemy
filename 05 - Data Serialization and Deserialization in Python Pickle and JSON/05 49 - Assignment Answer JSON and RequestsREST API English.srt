﻿1
00:00:02,320 --> 00:00:03,250
‫Welcome back.

2
00:00:03,340 --> 00:00:11,110
‫This is the answer to the assignment given to you in the lecture prior to this one, as I said, requests

3
00:00:11,110 --> 00:00:15,050
‫the library is used to make HTP requests to Web servers.

4
00:00:15,730 --> 00:00:20,080
‫This library doesn't come with them and we have to install it.

5
00:00:21,500 --> 00:00:30,830
‫In charm, I go to file settings, search for interpretor, then click on Project Interpretor.

6
00:00:32,460 --> 00:00:38,700
‫The next step is to click on the plus side and I'll search for requests.

7
00:00:40,930 --> 00:00:47,770
‫I've already installed it before, but you should install the package by clicking on install package.

8
00:00:49,250 --> 00:00:56,660
‫Of course, you need a working Internet connection for this because it will download and install requests

9
00:00:56,660 --> 00:00:58,280
‫from a live repository.

10
00:01:01,020 --> 00:01:07,980
‫After installing requests, I'll import the Jason and requests modules import Jason.

11
00:01:08,870 --> 00:01:10,700
‫And import requests.

12
00:01:12,120 --> 00:01:15,130
‫And I'm creating a variable called response.

13
00:01:15,600 --> 00:01:27,540
‫This is the HTP response we get from the server response equals requests that get I'll use the get method

14
00:01:27,720 --> 00:01:29,510
‫of HTP protocol.

15
00:01:30,330 --> 00:01:32,850
‫And the argument is that you are el.

16
00:01:35,160 --> 00:01:37,590
‫And I'll copy paste the address.

17
00:01:43,360 --> 00:01:51,310
‫The Jason module has a function called Lotus, which is the same function we've seen in the last lecture,

18
00:01:51,580 --> 00:01:59,590
‫and can be used to load adjacent encoded string into a python object and I'll create a new variable

19
00:01:59,590 --> 00:02:04,930
‫called Tattooers equals Jason that Lotus.

20
00:02:06,810 --> 00:02:10,440
‫And the argument is the response, that text.

21
00:02:13,560 --> 00:02:20,700
‫The text attribute of response is that Jason encoded string received from the rest API.

22
00:02:23,730 --> 00:02:25,830
‫And I'll print type Annville.

23
00:02:33,750 --> 00:02:35,670
‫I am executing the script.

24
00:02:37,880 --> 00:02:47,540
‫We notice, at least and this is its contents, if we look carefully, we see that it's a list of dictionaries.

25
00:02:48,820 --> 00:02:56,590
‫Now I want to filter down to do the tasks and point out only the tasks that have been completed, we

26
00:02:56,590 --> 00:03:02,920
‫notice that there is a key in the dictionary called completed, which is true or false.

27
00:03:04,700 --> 00:03:10,850
‫And I'm going to iterate over the to do list using a for loop like this for a task.

28
00:03:11,630 --> 00:03:20,630
‫This is a dictionary in tattooers and the testing condition inside the four block of code.

29
00:03:20,900 --> 00:03:24,830
‫If task of and the dictionary is key.

30
00:03:25,040 --> 00:03:29,260
‫So completed equals equals true.

31
00:03:30,350 --> 00:03:33,590
‫If the condition is true, I'll point out the task.

32
00:03:33,770 --> 00:03:35,210
‫So print desk.

33
00:03:37,850 --> 00:03:46,940
‫And I am executing the script again, and we see only the completed tattoos or tasks.

34
00:03:48,740 --> 00:03:52,010
‫OK, this was my solution to the assignment.

35
00:03:52,280 --> 00:03:52,880
‫Thank you.


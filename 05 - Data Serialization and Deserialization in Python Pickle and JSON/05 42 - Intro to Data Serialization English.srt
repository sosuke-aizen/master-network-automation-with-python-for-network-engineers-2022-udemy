﻿1
00:00:00,910 --> 00:00:05,740
‫In this section, we'll take a look at the data serialization in Python.

2
00:00:06,160 --> 00:00:12,570
‫First, let's see what exactly is data signalization and then how to do it the right way.

3
00:00:13,390 --> 00:00:21,310
‫Data signalization is the process of converting Python objects into a format that can be stored, for

4
00:00:21,310 --> 00:00:28,090
‫example, into a file or transmitted, for example, across a network connection, and the then converted

5
00:00:28,090 --> 00:00:33,430
‫back into the same python object or possibly in other environments.

6
00:00:34,270 --> 00:00:36,310
‫OK, this was the definition.

7
00:00:36,310 --> 00:00:40,090
‫But to make it clear, I'll give you a real example.

8
00:00:40,420 --> 00:00:47,800
‫Let's suppose we have a Python dictionary that represents one KeyCorp from a database and we want to

9
00:00:47,800 --> 00:00:56,970
‫save that dictionary to a file on disk and then later load the data saved on file into the same format,

10
00:00:56,980 --> 00:00:58,540
‫the initial dictionary.

11
00:00:59,510 --> 00:01:05,410
‫If you try to save the dictionary, tofile using right built function, you'll get an error.

12
00:01:05,570 --> 00:01:09,950
‫You cannot save a dictionary to a text file, only a string.

13
00:01:10,460 --> 00:01:17,480
‫A dictionary is not a string, even though it has a string representation we see when printing out the

14
00:01:17,480 --> 00:01:18,170
‫dictionary.

15
00:01:18,890 --> 00:01:20,650
‫Let me show you this example.

16
00:01:20,960 --> 00:01:23,540
‫I am creating a dictionary called Friends.

17
00:01:25,540 --> 00:01:34,720
‫The key is of type string and is the name, for example, then, and the value at least, let's say

18
00:01:34,720 --> 00:01:45,100
‫the H20, the city where the friend leaves London and another village, for example, a phone number

19
00:01:45,130 --> 00:01:46,280
‫or something else.

20
00:01:46,690 --> 00:01:56,870
‫This is the first key value pair and now the second Moriya, the key and the value, at least twenty

21
00:01:57,070 --> 00:02:00,140
‫five Madrid in the number.

22
00:02:01,360 --> 00:02:08,350
‫Now I'll try to save the dictionary tofile so with open the name of the file for example.

23
00:02:08,350 --> 00:02:18,760
‫Friends don't text the mouth w I am opening the file in rightmost or in other words I am creating the

24
00:02:18,760 --> 00:02:25,660
‫file and the name of the file object F and then F daughter.

25
00:02:25,660 --> 00:02:28,060
‫Right of friends.

26
00:02:29,640 --> 00:02:31,770
‫And I'm executing the script.

27
00:02:33,400 --> 00:02:43,330
‫And I've got the type air right argument must be star, not the solution to this problem is to encode

28
00:02:43,330 --> 00:02:50,230
‫or serialize the Python object in a format that can be saved to disk and the later are loaded into the

29
00:02:50,230 --> 00:02:51,560
‫same python object.

30
00:02:52,000 --> 00:02:59,580
‫Sometimes it's necessary to have an exchange format or in other words, we want our data to be imported

31
00:02:59,800 --> 00:03:06,490
‫also in other programming languages or applications like, say, we have a networking device that gives

32
00:03:06,490 --> 00:03:11,890
‫us structured information that needs to be loaded into a python object.

33
00:03:13,010 --> 00:03:20,120
‫Now that we've seen what data signalization means in the next lectures will take a look at two ways

34
00:03:20,120 --> 00:03:25,690
‫to encode or serialize data in Python, they are Pikul and Jason.

35
00:03:26,060 --> 00:03:27,980
‫See you in the next lecture.


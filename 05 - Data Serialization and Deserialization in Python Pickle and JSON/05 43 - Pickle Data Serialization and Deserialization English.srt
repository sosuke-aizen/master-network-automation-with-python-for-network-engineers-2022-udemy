﻿1
00:00:01,310 --> 00:00:08,210
‫In the last lecture we've seen, what is data signalization now I'll show you how to encode a python

2
00:00:08,210 --> 00:00:14,480
‫object in a stream of bytes and save those buycks to file for later use.

3
00:00:16,040 --> 00:00:23,090
‫The native data signalization module for Python is called PICKELL, because this python proprietory

4
00:00:23,090 --> 00:00:26,930
‫and that means that you can load or disorderlies the store.

5
00:00:26,930 --> 00:00:28,900
‫The data only back to Python.

6
00:00:29,450 --> 00:00:36,140
‫You cannot exchange the data between different languages or applications like, say, you serialize

7
00:00:36,140 --> 00:00:44,510
‫a python list and the load that object to later in a program developed engo non python programs are

8
00:00:44,510 --> 00:00:48,490
‫not able to reconstruct Picault Python objects.

9
00:00:49,460 --> 00:00:57,530
‫Also, you should know that Pikul is a binary protocol for serializing and the serializing python objects.

10
00:00:57,980 --> 00:01:06,530
‫That means that it converts Python objects into buycks for encoding and then loads the bikes back into

11
00:01:06,530 --> 00:01:08,630
‫python objects when decoding.

12
00:01:09,590 --> 00:01:12,620
‫Besides the interoperability issue.

13
00:01:12,950 --> 00:01:18,740
‫Another drawback of using Pikul is that it is not considered secure.

14
00:01:19,190 --> 00:01:22,160
‫You should unpeaceful only data you trust.

15
00:01:23,210 --> 00:01:31,520
‫It is possible to construct malicious serialized data, which will execute arbitrary code during this

16
00:01:31,520 --> 00:01:32,420
‫realization.

17
00:01:33,400 --> 00:01:41,050
‫Never unpeaceful data that could have come from an untrusted source or that could have been tampered

18
00:01:41,140 --> 00:01:47,460
‫with, consider signing the data if you need to ensure that it has not been tampered with.

19
00:01:48,360 --> 00:01:56,160
‫A safer signalization format is Jason, that may be appropriate if you are processing untrusted data,

20
00:01:56,490 --> 00:02:00,060
‫but about Jason will discuss in the next lecture.

21
00:02:01,780 --> 00:02:07,510
‫Let's go coding and see how to sterilize and the serialize Bayti using Pikul.

22
00:02:09,600 --> 00:02:16,680
‫First, I'll import the pickle module Pickle is a standard library module, so he's built in.

23
00:02:18,120 --> 00:02:27,600
‫The first step is to open the file will write to in binary mode once again, Pikul is a binary protocol.

24
00:02:27,990 --> 00:02:30,390
‫It's working with binary files.

25
00:02:31,710 --> 00:02:42,750
‫So with open the name of the file friends that let's say data from data, this is a binary file, not

26
00:02:42,750 --> 00:02:43,800
‫a text file.

27
00:02:44,640 --> 00:02:45,930
‫And the Moakes.

28
00:02:47,020 --> 00:02:50,860
‫W from right and B from binary.

29
00:02:52,360 --> 00:02:53,710
‫This is very important.

30
00:02:54,580 --> 00:03:03,370
‫I am opening the file in binary mode and the name of the file object, take care of that, it will overwrite

31
00:03:03,370 --> 00:03:08,770
‫the file if it already exists or create it if it doesn't exist.

32
00:03:09,520 --> 00:03:13,160
‫And pickle dot dump.

33
00:03:15,190 --> 00:03:16,600
‫This is the name of the function.

34
00:03:16,900 --> 00:03:23,140
‫The first argument is the python object, in our case, the dictionary friends.

35
00:03:24,010 --> 00:03:27,430
‫And the second argument, the file object if.

36
00:03:28,890 --> 00:03:31,290
‫And I am executing the script.

37
00:03:31,800 --> 00:03:40,050
‫There is no error, and if we look in the current working directory, we notice that a new file appeared

38
00:03:40,620 --> 00:03:42,410
‫Franck's that the 80.

39
00:03:44,140 --> 00:03:49,810
‫If we try to open the file using an editor, we notice a binary file.

40
00:03:52,880 --> 00:03:59,780
‫Let's move further and see how to load or d serialize the data back into a python object.

41
00:04:01,670 --> 00:04:05,210
‫I'm going to open the file this time in red only mode.

42
00:04:09,300 --> 00:04:12,030
‫Don't forget to use be from Binelli.

43
00:04:14,180 --> 00:04:21,500
‫And I'll call the load function from Piccola module, this function will read the Picault representation

44
00:04:21,500 --> 00:04:33,410
‫of an object from the file and return the reconstituted object so big from object equals pixel dot load

45
00:04:33,620 --> 00:04:42,170
‫of F, and I want to print the type and the value of the object type of object.

46
00:04:44,770 --> 00:04:46,360
‫And print object.

47
00:04:47,990 --> 00:04:56,330
‫And I'm running the script and we see types deep and we've got the back the same value.

48
00:04:58,270 --> 00:05:05,890
‫If you want to serialize more objects, you can create a table that contains all objects and serialize

49
00:05:06,010 --> 00:05:08,980
‫that table, something like this.

50
00:05:09,640 --> 00:05:11,110
‫This is Franck's one.

51
00:05:15,230 --> 00:05:16,760
‫This is Franck's to.

52
00:05:26,640 --> 00:05:30,360
‫And the Topal that contains both dictionaries.

53
00:05:31,360 --> 00:05:40,630
‫So friends equals a pair of parentheses, this means topple friends, one, call my friends, too,

54
00:05:42,160 --> 00:05:44,290
‫and I'll execute the script.

55
00:05:45,710 --> 00:05:54,720
‫I have serialised the object and then loaded back the serialized data, so the bike's into a Topal object.

56
00:05:55,520 --> 00:05:59,450
‫OK, this is how you use Pikul note perfetto.

57
00:05:59,480 --> 00:06:07,070
‫There is also a faster alternative called typical, but it doesn't belong to Python Standard Library.

58
00:06:07,280 --> 00:06:09,590
‫You should install it if you want to use it.

59
00:06:10,960 --> 00:06:17,140
‫In the next lecture, we'll discuss about Jason, which is another way to serialize data.

60
00:06:18,310 --> 00:06:20,490
‫Thank you and see you in a second.


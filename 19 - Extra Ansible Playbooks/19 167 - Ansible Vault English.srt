﻿1
00:00:00,700 --> 00:00:08,020
‫In a lecture we take a look at the Vault which is a simple feature that allows keeping sensitive data

2
00:00:08,020 --> 00:00:15,940
‫such as passwords or keys in encrypted files or either of them as plain text in our play books to use

3
00:00:15,940 --> 00:00:23,830
‫this feature a command line called ansible vault is used to create it or view encrypted files and a

4
00:00:23,830 --> 00:00:32,470
‫command line option named OSC vault pass or vault pass sort of file he's used the vault feature can

5
00:00:32,470 --> 00:00:35,410
‫encrypt any data used by unseeable.

6
00:00:35,410 --> 00:00:44,100
‫This can include the files variabilis or behavioral inventory part on meters used in your play books.

7
00:00:44,110 --> 00:00:49,340
‫Let's see an example on how to set up and use and Sebald vault.

8
00:00:49,840 --> 00:00:59,050
‫We have the same topology we've used in other lectures and the playbook that configures the OSPF routing

9
00:00:59,050 --> 00:01:02,200
‫protocol on these routers.

10
00:01:02,620 --> 00:01:07,950
‫At this moment we have a plain text password in our inventory file.

11
00:01:08,260 --> 00:01:10,120
‫This is our inventory file.

12
00:01:10,450 --> 00:01:16,290
‫And here we can see the plain text password by them or Cisco.

13
00:01:16,330 --> 00:01:24,070
‫I don't want any more to have clear text passwords and I want to use ansible vault in order to encrypt

14
00:01:24,220 --> 00:01:27,500
‫these sensitive information first.

15
00:01:27,520 --> 00:01:31,130
‫Let's run this playbook to check if it works.

16
00:01:40,270 --> 00:01:41,000
‫OK.

17
00:01:41,100 --> 00:01:46,070
‫The playbook is working in order to set up ansible.

18
00:01:46,170 --> 00:01:50,100
‫The first step is to create an encrypted vault fight.

19
00:01:50,190 --> 00:01:57,740
‫We have to be ansible vault to command and using the Create option will create the vault.

20
00:01:58,850 --> 00:02:03,190
‫And it Sebald vault create and now the name of the file.

21
00:02:03,230 --> 00:02:07,220
‫Let's say Valter Dorothy Hamill is asking for a password.

22
00:02:07,250 --> 00:02:10,860
‫I'm going to use a simple pass or just for our lab.

23
00:02:10,870 --> 00:02:15,260
‫One two three four.

24
00:02:15,440 --> 00:02:20,420
‫And the file has been opened using the vi editor.

25
00:02:20,600 --> 00:02:29,440
‫If you want to use another at the door like the cordon nano you must modify the a global variable.

26
00:02:29,450 --> 00:02:33,080
‫In this case VI is just fine for me.

27
00:02:33,080 --> 00:02:40,770
‫And here we use our one become password call on MySpace and now the password.

28
00:02:41,090 --> 00:02:44,430
‫In this case is Cisco.

29
00:02:44,620 --> 00:02:53,530
‫Then for the second to get their head out or to become Bosshard and the password is Python and so on

30
00:02:54,650 --> 00:02:58,570
‫router 1 become password and get out there to become password.

31
00:02:58,650 --> 00:03:02,080
‫Our variables in not predefined.

32
00:03:02,090 --> 00:03:06,750
‫Of course I can use a passport for an entire group.

33
00:03:06,890 --> 00:03:14,270
‫In this case I have a group called routers become password call on Cisco.

34
00:03:14,300 --> 00:03:17,990
‫So this is the password for the entire group.

35
00:03:18,290 --> 00:03:25,710
‫In this case all that out of my group have the same password but I could have another password for a

36
00:03:25,710 --> 00:03:31,310
‫hard outer one and the password would have overwritten the password.

37
00:03:31,490 --> 00:03:33,620
‫So the group password.

38
00:03:33,910 --> 00:03:36,710
‫Now I'm saving the fight.

39
00:03:36,810 --> 00:03:40,830
‫The vault bought the all fine is now an encrypted file.

40
00:03:41,040 --> 00:03:47,130
‫So if I want to display its content we can see encrypted information.

41
00:03:47,430 --> 00:03:56,100
‫Other options of ansible vault come and our view and now my vote YEM will file.

42
00:03:56,820 --> 00:04:02,900
‫It's asking for the password one two three four and it has displayed its content.

43
00:04:03,090 --> 00:04:11,760
‫Then if I want to use an either password or to change its password I use here or he keep asking for

44
00:04:11,910 --> 00:04:12,960
‫the password.

45
00:04:12,960 --> 00:04:14,450
‫One two three four.

46
00:04:14,460 --> 00:04:18,770
‫This is the old password new password.

47
00:04:18,810 --> 00:04:20,800
‫Lets say the best.

48
00:04:21,210 --> 00:04:22,300
‫And one more time.

49
00:04:22,470 --> 00:04:25,220
‫And the password has been changed.

50
00:04:25,290 --> 00:04:33,470
‫If I want to get the file I use the edit option the best.

51
00:04:33,750 --> 00:04:37,920
‫And the file has been opened for editing.

52
00:04:37,920 --> 00:04:44,550
‫Now I must edit the inventory file in order to use the encrypted data

53
00:04:47,910 --> 00:04:56,160
‫for the first router I have and will become password and the name of my variable from the will to fight

54
00:04:56,980 --> 00:04:57,730
‫here.

55
00:04:57,870 --> 00:05:00,140
‫Either right and will become password

56
00:05:02,750 --> 00:05:07,990
‫equals and all between double cokes and Babul curly braces.

57
00:05:08,090 --> 00:05:10,160
‫The name of the variable.

58
00:05:10,490 --> 00:05:17,800
‫This is a good job to template to valuable in the name of the valuable was out there one become boss

59
00:05:17,810 --> 00:05:18,160
‫worked

60
00:05:22,310 --> 00:05:24,090
‫the same for the second but after

61
00:05:31,800 --> 00:05:35,370
‫and also here for the entire group.

62
00:05:35,370 --> 00:05:41,660
‫So for the other doubters don't forget to use double quotes to they are mandatory here.

63
00:05:41,910 --> 00:05:44,070
‫So routers become password

64
00:05:49,290 --> 00:05:51,260
‫OK and that's all.

65
00:05:51,390 --> 00:05:52,400
‫One more time.

66
00:05:52,470 --> 00:05:57,300
‫These are variableness from the encrypted file.

67
00:06:07,210 --> 00:06:07,870
‫OK.

68
00:06:07,950 --> 00:06:09,750
‫These are the variables.

69
00:06:10,600 --> 00:06:19,770
‫And I'm going to execute the playbook against my routers and Sibbald playbook minus I and the inventory

70
00:06:19,770 --> 00:06:20,380
‫file.

71
00:06:23,170 --> 00:06:27,930
‫The name of the playbook file.

72
00:06:28,190 --> 00:06:31,400
‫And then we have minus minus Eske Volt.

73
00:06:34,220 --> 00:06:36,060
‫Minus e at.

74
00:06:36,290 --> 00:06:43,520
‫And a valid pop to the voile to find x in the same directory so dot slash Voyles Dorothy Hamill

75
00:06:50,250 --> 00:06:57,430
‫is asking for the password the best and the playbook is executing.

76
00:06:59,540 --> 00:07:04,750
‫And we can see how the commands have been executed on my routers.

77
00:07:05,070 --> 00:07:11,970
‫OK in this case only on that outer one but I could modify the playbook to execute the the

78
00:07:12,020 --> 00:07:22,920
‫comments of that configure OSPF on all routers.

79
00:07:22,950 --> 00:07:26,310
‫So here I write that out there's the name of the group

80
00:07:31,400 --> 00:07:32,910
‫and it's working.

81
00:07:33,260 --> 00:07:38,840
‫Now we don't use any more plain text password X for our devices.

82
00:07:38,840 --> 00:07:47,210
‫All passwords are encrypted in this vault fine and we are using a single master password to decrypt

83
00:07:47,600 --> 00:07:49,330
‫the vault file.

84
00:07:49,400 --> 00:07:52,750
‫This is how you should set up and see Boulevard.

85
00:07:52,940 --> 00:07:57,770
‫And that this is the secure way to use passwords with as simple.


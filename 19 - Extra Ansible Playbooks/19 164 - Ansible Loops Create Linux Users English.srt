﻿1
00:00:00,330 --> 00:00:08,430
‫This lecture is all about how to use loops in Playbook's, sometimes you'll want to do many things in

2
00:00:08,430 --> 00:00:16,140
‫one task, such as create a lot of users, install a lot of packages, or repeat a single step until

3
00:00:16,140 --> 00:00:18,030
‫a certain result is reached.

4
00:00:18,990 --> 00:00:25,290
‫In this example, I want to show you a simple playbook that uses a loop.

5
00:00:26,100 --> 00:00:31,260
‫My playbook will create some users on a Linux server.

6
00:00:32,130 --> 00:00:35,250
‫This is the IP of my Linux server.

7
00:00:35,490 --> 00:00:41,000
‫It will become Hogwood and it will use the S.H. connection.

8
00:00:41,580 --> 00:00:43,770
‫Here I am creating a task.

9
00:00:43,770 --> 00:00:46,830
‫Its name is at the users using a loop.

10
00:00:47,460 --> 00:00:52,130
‫I am going to use the user module and then name.

11
00:00:52,170 --> 00:00:59,000
‫This is the name of my new user that will be created and here I'll use a template.

12
00:00:59,460 --> 00:01:09,030
‫So between double clicks and double square brackets, I have a predefined variable called item state

13
00:01:09,240 --> 00:01:09,900
‫present.

14
00:01:10,200 --> 00:01:18,600
‫This means that Ansible will ensure that the user is present on that Linux distribution.

15
00:01:18,870 --> 00:01:27,180
‫If the user already exists, it will do nothing and my user will belong to the pseudo group.

16
00:01:27,180 --> 00:01:30,420
‫So it will be an admin user here.

17
00:01:30,430 --> 00:01:38,400
‫Now, using the loop keyboard, I write something like this loop and here's a list of users like, say,

18
00:01:38,430 --> 00:01:41,310
‫you on YouTube and you three.

19
00:01:41,730 --> 00:01:44,690
‫And next, all this is my playbook.

20
00:01:45,060 --> 00:01:52,410
‫In fact, it will iterate over this list and will execute these instructions.

21
00:01:52,410 --> 00:01:58,140
‫So it will create the user in the pseudo group for each element of my list.

22
00:01:58,140 --> 00:02:02,880
‫So for each user, I also have to modify the inventory file.

23
00:02:03,180 --> 00:02:11,250
‫Here, I'll add a new group name servers and the IP address of my server, or let's write a set of Iran

24
00:02:11,460 --> 00:02:14,880
‫and then Ansible host and IP address.

25
00:02:16,960 --> 00:02:27,460
‫And then I'll add a section service call on verse here, I'm going to specify the credentials Ansible

26
00:02:27,460 --> 00:02:31,150
‫will use in order to connect to my Linux server.

27
00:02:32,330 --> 00:02:42,450
‫So accessible user equals Andre and Cebull S.H. Plus equals best one, two, three, four.

28
00:02:42,920 --> 00:02:45,320
‫Never use such a password.

29
00:02:45,330 --> 00:02:47,900
‫You must always use a strong start.

30
00:02:48,140 --> 00:02:50,690
‫This is just for our example here.

31
00:02:51,290 --> 00:02:56,540
‫Ansible become equal and Ansible become a method.

32
00:02:56,540 --> 00:02:59,750
‫Pseudo Ansible become passe.

33
00:03:00,020 --> 00:03:01,940
‫The same user password.

34
00:03:05,780 --> 00:03:11,700
‫And that's all now on my sensible control machine here.

35
00:03:11,750 --> 00:03:18,680
‫I'm going to paste the content of my e-mail file in a file called Linux Lupe's.

36
00:03:30,270 --> 00:03:37,740
‫And I'll also add these sections in the inventory file of my ansible control machine.

37
00:03:45,170 --> 00:03:48,260
‫And now I'm going to run the playbook.

38
00:03:52,250 --> 00:03:53,810
‫The playbook is running.

39
00:03:56,480 --> 00:04:04,850
‫OK, and I have this warning, it skipped any host, so in the inventory file here.

40
00:04:07,130 --> 00:04:14,330
‫If I'm using the server one name in the playbook, I must also use server one.

41
00:04:14,630 --> 00:04:16,910
‫So I'm going to modify the playbook.

42
00:04:19,940 --> 00:04:24,800
‫Here, instead of the IP address of my host, I'll write the server one.

43
00:04:28,200 --> 00:04:30,150
‫And now I learned the playbook one more time.

44
00:04:32,160 --> 00:04:39,690
‫The playbook is running, there is no error, and you can see how it changed the server, when we see

45
00:04:39,720 --> 00:04:45,870
‫lines in Oregon yellow, we know that something has been changed.

46
00:04:46,200 --> 00:04:49,160
‫When the lines are in red, there is an error.

47
00:04:49,380 --> 00:04:52,200
‫And when we see green, there is no change.

48
00:04:52,590 --> 00:04:58,810
‫For example, the users already exist now on that Linux server.

49
00:04:58,830 --> 00:05:01,650
‫Let's see what users have been created.

50
00:05:05,010 --> 00:05:09,630
‫And we can see here you on YouTube and you three users.

51
00:05:16,600 --> 00:05:24,580
‫If I want to delete some users using a loop here instead of Prezant, I write absent.

52
00:05:28,270 --> 00:05:36,400
‫And by running the playbook, the playbook is executing and it's ready, if I run it one more time,

53
00:05:36,400 --> 00:05:37,930
‫it will modify nothing.

54
00:05:37,930 --> 00:05:42,820
‫So everything is ingrained and changed equals zero.

55
00:05:44,350 --> 00:05:51,760
‫And on my Linux server, there is no name called Yuan or you two or you three.

56
00:05:52,540 --> 00:05:56,230
‫Let's take another look here in our playbook.

57
00:05:57,180 --> 00:05:59,300
‫And I want to show you something else.

58
00:06:00,590 --> 00:06:10,190
‫Instead of using this list of users, I can use a viable I can define a variable using the vast keyword,

59
00:06:10,730 --> 00:06:11,670
‫something like this.

60
00:06:12,080 --> 00:06:19,040
‫So here I write Vargus and my variable is named new users.

61
00:06:19,220 --> 00:06:21,380
‫You want you to.

62
00:06:21,590 --> 00:06:22,580
‫And you three.

63
00:06:26,350 --> 00:06:29,590
‫And here I'm going to use my new variable.

64
00:06:32,160 --> 00:06:42,600
‫Something like these new users and here I write present, so I want to create these users.

65
00:06:44,850 --> 00:06:48,750
‫I'm saving the file and I am running the playbook one more time.

66
00:06:52,330 --> 00:06:56,860
‫And there is no error and the users have been created.

67
00:07:01,010 --> 00:07:07,310
‫Another variation is when we have here a dictionary, for example, I have something like this.

68
00:07:09,730 --> 00:07:17,820
‫Name Alexei Testiest or one coma groups, Alexei Souto.

69
00:07:21,880 --> 00:07:24,940
‫And then name based user to.

70
00:07:26,900 --> 00:07:32,330
‫And groups like, say, will or would like say Will.

71
00:07:35,240 --> 00:07:44,610
‫These groups must exist, and here I obtain the value of the name field of my variable.

72
00:07:44,840 --> 00:07:49,120
‫So this means item, which is predefined dot name.

73
00:07:49,340 --> 00:07:55,460
‫And here, instead of the hard coded value of the pseudo group, I have something like this.

74
00:07:58,640 --> 00:08:04,680
‫Item that groups where groups is this value from here.

75
00:08:05,510 --> 00:08:12,370
‫So the key of my dictionary and that's all, of course, we don't need this variable here.

76
00:08:12,590 --> 00:08:14,960
‫I am commenting about the slyness.

77
00:08:19,820 --> 00:08:23,180
‫I'm saving the playbook, and I learned it one more time.

78
00:08:30,490 --> 00:08:38,830
‫And it seems we have an error, we have an error with our YAML file, Yamal errors are the most frequent

79
00:08:38,830 --> 00:08:39,290
‫ones.

80
00:08:39,400 --> 00:08:40,930
‫So let's see what it says.

81
00:08:41,830 --> 00:08:46,740
‫OK, so there appears to be a tab at the start of the line.

82
00:08:47,230 --> 00:08:50,320
‫He does not use steps for formatting.

83
00:08:50,560 --> 00:08:56,560
‫So I think by mistake I used the tab keyword instead of space's.

84
00:08:58,110 --> 00:09:02,790
‫This is a very simple problem, so here I am going to use space's.

85
00:09:09,870 --> 00:09:15,720
‫OK, now that's better, I'm saving the file and Theiler on it one more time.

86
00:09:18,070 --> 00:09:23,500
‫And there is no air, we can see how it has created the users.

87
00:09:33,350 --> 00:09:43,880
‫So this is how we use loops in Ansible, if you see an older playbook, so before Ansible 2.5 instead

88
00:09:43,880 --> 00:09:48,810
‫of the loop keyboard, you could see here with items.

89
00:09:49,460 --> 00:09:50,360
‫This is the same.

90
00:09:50,390 --> 00:09:52,250
‫This is the old version.

91
00:09:59,840 --> 00:10:06,440
‫OK, there is no error, I would recommend you to use the loop keyboard because this is the new method

92
00:10:06,440 --> 00:10:08,810
‫of working with loops in Ansible.


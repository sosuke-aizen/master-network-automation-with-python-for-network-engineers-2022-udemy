﻿1
00:00:00,730 --> 00:00:06,380
‫In the previous lecture, we've seen what our behavioural inventory parameters.

2
00:00:06,750 --> 00:00:11,810
‫Now I want to show you how to use these parameters in Playbook's.

3
00:00:12,730 --> 00:00:14,070
‫Let's take an example.

4
00:00:14,380 --> 00:00:22,960
‫We have this playbook that executes a privileged show comment on Cisco iOS here.

5
00:00:23,140 --> 00:00:31,550
‫I'm using the QR at the vase and I am setting the parameters ansible user you want.

6
00:00:31,630 --> 00:00:34,510
‫This is the user that is going to connect.

7
00:00:34,510 --> 00:00:40,870
‫Using SSL to the remote node in a simple SSL Express is his password.

8
00:00:41,470 --> 00:00:50,920
‫Ansible Compass is the enable password and Ansible Network OS is OS because we are using the network

9
00:00:50,930 --> 00:00:56,500
‫CLIA method and we are connecting to a Cisco iOS out there.

10
00:00:57,160 --> 00:01:02,070
‫I've already used become yes and become method enabled.

11
00:01:02,800 --> 00:01:12,130
‫These are ansible directives and these are ansible behavioral inventory parameters, a special kind

12
00:01:12,130 --> 00:01:13,300
‫of variables.

13
00:01:14,580 --> 00:01:25,470
‫If I want, I could comment about these two lines in here, I could write and become yes and Ansible

14
00:01:25,800 --> 00:01:27,570
‫become a method enable.

15
00:01:31,450 --> 00:01:40,930
‫If this is the same, I've changed the ansible directives with two ansible parameters, the result will

16
00:01:40,930 --> 00:01:41,620
‫be the same.

17
00:01:42,600 --> 00:01:45,820
‫Let's run this playbook to see how it works.

18
00:01:46,200 --> 00:01:49,500
‫I am selecting the Playbook copy.

19
00:01:51,490 --> 00:01:58,420
‫And here on the export control machine, I am going to paste the content in a new file.

20
00:02:05,010 --> 00:02:14,640
‫I'm saving the file and now I'm going to run the playbook ansible playbook minus, I hope I'm going

21
00:02:14,640 --> 00:02:20,340
‫to use the hosts from the current working directory and the name of my playbook.

22
00:02:20,520 --> 00:02:24,510
‫I recommend behavior, inventory parameters.

23
00:02:26,720 --> 00:02:28,790
‫And the playbook is executing.

24
00:02:31,640 --> 00:02:39,500
‫And we can see the output of the show on come in another possibility to use the behavioral inventory

25
00:02:39,500 --> 00:02:44,390
‫parameters used to use them in the inventory file.

26
00:02:44,870 --> 00:02:54,410
‫This way we have the advantage that we can use different parameters or parameters with different values

27
00:02:54,620 --> 00:02:56,170
‫for each host.

28
00:02:56,990 --> 00:03:02,650
‫I'm going to comment about these lines and this is my inventory file.

29
00:03:02,930 --> 00:03:06,170
‫I'm going to save as hosts one.

30
00:03:09,620 --> 00:03:19,940
‫OK, and here I'm going to modify the inventory file in order to use the behavioral inventory parameters.

31
00:03:21,320 --> 00:03:27,990
‫And simple user equals you one here in the inventory file.

32
00:03:28,110 --> 00:03:30,690
‫We use the equal sign.

33
00:03:31,600 --> 00:03:40,310
‫But in the YAML File will use Kolon to separate the name of the Parmeter from X value.

34
00:03:40,840 --> 00:03:45,370
‫So here we have call on and here we have equal.

35
00:03:46,650 --> 00:04:00,990
‫And then a sensible port equals 22, this is anyway the default and SSX pass equals C score is not recommended

36
00:04:00,990 --> 00:04:03,540
‫to write works in Phylis.

37
00:04:03,960 --> 00:04:07,230
‫The recommended way is to use ansible Volks.

38
00:04:07,770 --> 00:04:12,120
‫But for this example, I'm going to use passwords in this file.

39
00:04:12,390 --> 00:04:14,010
‫This is just one example.

40
00:04:14,220 --> 00:04:16,320
‫It's not an inventory file.

41
00:04:16,470 --> 00:04:21,870
‫Often in production network, then Ansible become best.

42
00:04:22,200 --> 00:04:27,690
‫Cisco Ansible Network OS equals iOS.

43
00:04:28,650 --> 00:04:34,080
‫Then we have Ansible become method enable and Ansible become.

44
00:04:34,080 --> 00:04:34,590
‫Yes.

45
00:04:35,700 --> 00:04:45,300
‫I write them here and people become method enable here, I use the equals sign and Ansible become yes.

46
00:04:49,220 --> 00:04:55,880
‫OK, all parameters from here are written here in the inventory file.

47
00:04:58,690 --> 00:05:04,030
‫And I'll copy paste the parameters for each out of my group.

48
00:05:07,640 --> 00:05:17,950
‫And now I'm going to copy the text and paste it on the ansible control machine here using the video,

49
00:05:18,230 --> 00:05:22,490
‫I am creating a new file, a new inventory file named Swan.

50
00:05:22,970 --> 00:05:31,820
‫And here I'll paste the content of the file and I'm saving the new inventory file.

51
00:05:34,370 --> 00:05:41,450
‫Now, I'm going to copy paste the content of the Playbook YAML file from the Windows recording machine

52
00:05:41,630 --> 00:05:44,420
‫to the Ansible control machine.

53
00:05:50,120 --> 00:05:58,130
‫This is my playbook file, my playbook YAML file, I'm saving it, and now I'm going to run the playbook

54
00:05:58,430 --> 00:06:04,160
‫and simple playbook minus I hosts one enter the name of the playbook.

55
00:06:07,950 --> 00:06:09,960
‫And the playbook is executing.

56
00:06:12,010 --> 00:06:19,180
‫And we can see the output of the show run comment, so everything worked is expected.

57
00:06:20,510 --> 00:06:30,500
‫Another way to specify these variables is to use here something like this or autres call on Vargus.

58
00:06:31,990 --> 00:06:41,860
‫And now I'm going to cut and paste the behavioral inventory parameters from the hosts section to the

59
00:06:41,860 --> 00:06:43,240
‫vast section.

60
00:06:45,130 --> 00:06:50,620
‫And I'll delete these variables from all other hosts.

61
00:07:04,000 --> 00:07:08,510
‫OK, this is another way to specify variables.

62
00:07:09,350 --> 00:07:19,720
‫I've used the name of the group called On and then the keyword VARs and after that, my inventory behavioral

63
00:07:19,720 --> 00:07:20,590
‫parameters.

64
00:07:21,580 --> 00:07:27,350
‫Of course, they are the same for each other out of my group.

65
00:07:27,840 --> 00:07:36,430
‫Now, I'll copy paste the content of this file to the inventory file of the Ansible control machine.

66
00:07:43,140 --> 00:07:46,400
‫And I'm going to run the playbook one more time.

67
00:07:49,540 --> 00:07:50,730
‫And it worked.

68
00:07:52,180 --> 00:07:59,920
‫Now, if I want to use another value for a specific device, for example, the second Raptor has another

69
00:08:00,040 --> 00:08:08,240
‫enabled password, then I'll use that parameter here where I define its IP address.

70
00:08:08,920 --> 00:08:13,920
‫So let's say Ansible become best Cisco.

71
00:08:14,230 --> 00:08:18,730
‫And here I'll modify it to like, say, Python.

72
00:08:19,390 --> 00:08:22,750
‫And now I am going to connect to these scouter.

73
00:08:23,080 --> 00:08:25,750
‫This is a console to that device.

74
00:08:28,880 --> 00:08:34,220
‫OK, this is the device, and here I'll modify the enable password.

75
00:08:35,400 --> 00:08:37,320
‫And I'll write Python.

76
00:08:40,760 --> 00:08:48,890
‫OK, this is the Nable passport, so this value will overwrite this global value from here.

77
00:08:49,310 --> 00:08:54,800
‫Now let's modify the inventory file from the ansible control machine.

78
00:09:06,600 --> 00:09:15,480
‫And I'll run this playbook on router one and also on the second quarter, so they're out there with

79
00:09:15,480 --> 00:09:22,650
‫the IP address of one hundred ninety to one hundred sixty eight, one hundred twenty two dot 11.

80
00:09:22,980 --> 00:09:27,210
‫So this playbook will be run on these two routers.

81
00:09:33,150 --> 00:09:36,820
‫And the playbook has been executed without error.

82
00:09:37,500 --> 00:09:42,930
‫We can see how we've used another password for the second router.

83
00:09:43,110 --> 00:09:46,980
‫So this variable has been overwritten here.

84
00:09:47,530 --> 00:09:53,070
‫OK, so it's only valid for the sake of all other reporters.

85
00:09:53,220 --> 00:09:55,300
‫Have this enable password?

86
00:09:56,160 --> 00:10:06,380
‫One last thing I want to show you is that we can set a global variable for each device of every group.

87
00:10:06,990 --> 00:10:14,310
‫For example, here I have two groups, the servers and the routers group, and I want to use the NFC

88
00:10:14,340 --> 00:10:19,110
‫Bellport 22, both for the routers and for the servers.

89
00:10:19,620 --> 00:10:21,990
‫For that I can write something like this.

90
00:10:23,210 --> 00:10:35,990
‫All on and here, Ansible Port 22, these variable will be valid for each device defined in my inventory

91
00:10:35,990 --> 00:10:36,520
‫file.

92
00:10:36,650 --> 00:10:40,760
‫No member of the group, it belongs to the Vics.

93
00:10:40,760 --> 00:10:49,460
‫How we use behavioral inventory parameters in inventory files, in Playbook, Yamal files.

94
00:10:50,200 --> 00:10:50,750
‫Thank you.


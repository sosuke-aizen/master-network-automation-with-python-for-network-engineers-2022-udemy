﻿1
00:00:00,300 --> 00:00:06,740
‫Another useful option off with the IOC config module is the backup option.

2
00:00:06,990 --> 00:00:13,700
‫This argument will cause the module to create a full backup of the current running config of the remote

3
00:00:13,710 --> 00:00:16,820
‫device before any changes are made.

4
00:00:17,460 --> 00:00:23,800
‫The backup file is written to the backup folder of the Playbook directory.

5
00:00:24,120 --> 00:00:32,850
‫So if the directory that contains the Playbook YAML file, if that directory doesn't exist, Ansible

6
00:00:32,970 --> 00:00:33,910
‫will create it.

7
00:00:34,890 --> 00:00:41,950
‫Let's take the previous example and modify it in order to pick up the running config.

8
00:00:42,510 --> 00:00:46,830
‫So here I'll write backup running config.

9
00:00:47,890 --> 00:00:57,640
‫I am executing the playbook against all our outrace and I'll delete these lines and the single argument

10
00:00:57,640 --> 00:01:03,840
‫I use is back up call on, yes, this is my sensible control machine.

11
00:01:03,850 --> 00:01:08,080
‫And here I'll create a new playbook with this content.

12
00:01:17,920 --> 00:01:22,540
‫In the current working directory, there is no directory called backup.

13
00:01:23,510 --> 00:01:25,370
‫And now let's run the playbook.

14
00:01:34,640 --> 00:01:38,630
‫The playbook is running, and shortly we'll see the result.

15
00:01:40,940 --> 00:01:49,280
‫We can see how a new directory called Backup appeared, and inside these directory, we can find five

16
00:01:49,280 --> 00:01:50,910
‫configuration files.

17
00:01:51,170 --> 00:01:58,370
‫In fact, these are the running config of my five routers of these routers.

18
00:02:00,510 --> 00:02:03,120
‫Let's see what's inside such a file.

19
00:02:04,750 --> 00:02:13,030
‫Let's take the running config file of our out there one, and we can see that inside that file we find

20
00:02:13,030 --> 00:02:16,420
‫the running config of the first router.

21
00:02:17,980 --> 00:02:23,800
‫This is a very simple and easy way to back up our autres configuration.


﻿1
00:00:00,700 --> 00:00:07,600
‫In this lecture We'll see how to communicate with Cisco Networking devices using ansible.

2
00:00:07,670 --> 00:00:10,590
‫Lexi Watt the modulus does unseeable offer.

3
00:00:10,760 --> 00:00:22,700
‫So I am looking for a network more doorless and I'm searching for iOS we can see modules for many vendors

4
00:00:23,030 --> 00:00:28,530
‫for The Voice and The These are the models for Cisco.

5
00:00:28,600 --> 00:00:34,810
‫I guess OK there are many models but there are two models.

6
00:00:34,850 --> 00:00:43,640
‫We are going to talk about and that they are Aiwass command and iOS config I O S comment since arbitrary

7
00:00:43,640 --> 00:00:45,830
‫comments to an I O S.

8
00:00:45,970 --> 00:00:50,460
‫And entry tuchis the results are read from that device.

9
00:00:50,490 --> 00:00:57,920
‫Take care that the small deal does not support the running command in configuration mode so we don't

10
00:00:57,920 --> 00:00:59,940
‫use this module.

11
00:00:59,960 --> 00:01:08,980
‫The iOS command module for configuring devices we use this module only for a running show comments.

12
00:01:09,050 --> 00:01:16,920
‫If we want to configure a device or a Cisco device we use the IRS config module.

13
00:01:17,030 --> 00:01:20,950
‫I'll show you examples with both modules.

14
00:01:20,960 --> 00:01:26,990
‫Now let's see a play book that uses the IRS command module.

15
00:01:27,140 --> 00:01:32,460
‫I've already created the play book and I want to explain it to you.

16
00:01:32,480 --> 00:01:37,580
‫This is a play book with a single play the name of the play book is ironic.

17
00:01:37,580 --> 00:01:47,260
‫Show comments on C Squier's the play will be run against or out there on Route 31 is the name of the

18
00:01:47,260 --> 00:01:49,870
‫device from the inventory file.

19
00:01:49,950 --> 00:01:53,450
‫Let's see what the content of our inventory file.

20
00:01:54,570 --> 00:01:56,920
‫Here we can see a third one.

21
00:01:57,420 --> 00:02:06,870
‫If I want to execute the play against all authors I can simply write here routers.

22
00:02:06,950 --> 00:02:18,410
‫Then we are using the connect CLID Lexi Watts's the simple documentation about networks like we use

23
00:02:18,510 --> 00:02:27,110
‫net supply to run a command on a network appliance this connection plug in provide a connection to remote

24
00:02:27,110 --> 00:02:36,740
‫devices over S-sh and implements CLID shell the connection plugin is typically used by network devices

25
00:02:36,830 --> 00:02:41,950
‫for sending and receiving C-A like Manics to network devices.

26
00:02:41,960 --> 00:02:44,450
‫That's exactly what we need.

27
00:02:44,810 --> 00:02:50,440
‫Let's go back to our playbook and here I've defined two tasks.

28
00:02:50,510 --> 00:02:54,590
‫The first task he's named are on multiple comments on the SkyWest.

29
00:02:54,590 --> 00:03:03,680
‫Note I am using the I O S command module and then I have a list of comments at the moment.

30
00:03:03,680 --> 00:03:06,720
‫I have only one command in the list.

31
00:03:06,920 --> 00:03:13,280
‫I am registering the output in a variable and I am printing the output.

32
00:03:13,280 --> 00:03:16,760
‫I've already explained in a previous lecture.

33
00:03:16,940 --> 00:03:25,150
‫How does the registar work Lex simply ran the play book first the Lex being out there on

34
00:03:28,630 --> 00:03:29,860
‫being works OK.

35
00:03:30,040 --> 00:03:37,410
‫So any simple play book minus I whisks know the name of the human file.

36
00:03:37,770 --> 00:03:43,420
‫I recommend that camel minus you use a good name.

37
00:03:43,580 --> 00:03:47,670
‫Connecting to the same server to that outer.

38
00:03:47,710 --> 00:03:52,100
‫So you wan and minus to ask for the password.

39
00:03:52,340 --> 00:03:57,580
‫I am entering the password and we've gotten there.

40
00:03:57,640 --> 00:04:01,970
‫This error was on purpose because this is a common one.

41
00:04:02,110 --> 00:04:09,910
‫So if you got that this error unable to automatically determine host network OS you can solve it in

42
00:04:09,910 --> 00:04:12,160
‫the following way.

43
00:04:12,160 --> 00:04:14,290
‫Let's open the hosts file.

44
00:04:14,350 --> 00:04:20,590
‫The inventory file and here we can also have a viable.

45
00:04:20,860 --> 00:04:28,990
‫So I'll create a viable for our terse group and that means the name of the group or authors call on

46
00:04:29,610 --> 00:04:31,200
‫Vares.

47
00:04:31,310 --> 00:04:42,910
‫And here I write ansible network OS equals I Os I am saving the file and I'm running the playbook one

48
00:04:42,910 --> 00:04:43,410
‫more time

49
00:04:48,390 --> 00:04:50,050
‫and there is no error.

50
00:04:50,130 --> 00:04:55,630
‫We can see the output of the show version command display here at the console.

51
00:04:57,430 --> 00:05:04,580
‫If we want to play exec more we can simply add the comments here to this list.

52
00:05:05,620 --> 00:05:12,670
‫I'm going to open the file on the ansible control machine one more time.

53
00:05:12,850 --> 00:05:19,320
‫I am connected using S-sh to Linux machine that honest and simple.

54
00:05:19,600 --> 00:05:21,100
‫This is the Linux machine.

55
00:05:21,100 --> 00:05:28,920
‫And here I am opening the new modified so I press on the line command Latium one.

56
00:05:29,170 --> 00:05:35,050
‫And here I'll add another Come and show IP interface.

57
00:05:35,140 --> 00:05:42,920
‫But if I'm saving the file indictor on the playbook one more time.

58
00:05:43,080 --> 00:05:47,510
‫The playbook is running and sharply when you see the output.

59
00:05:47,670 --> 00:05:50,760
‫We can see the output from both comments.

60
00:05:50,760 --> 00:05:53,690
‫The shell version command and the interface.

61
00:05:53,700 --> 00:05:56,690
‫Brief comment in the next lecture.

62
00:05:56,790 --> 00:06:01,890
‫I'll show you how to save the output of a command into a fight.

63
00:06:01,890 --> 00:06:03,660
‫See you in a few seconds.


﻿1
00:00:00,580 --> 00:00:09,880
‫In this lecture We'll talk about the parents argument of the IRS config module the parents option uniquely

2
00:00:09,940 --> 00:00:18,040
‫identifies the Configuration section the command should be check against the parents argument is omitted

3
00:00:18,490 --> 00:00:23,090
‫the comments are checked against the global configuration section.

4
00:00:23,420 --> 00:00:31,540
‫Let's take the playbook file from the previous lecture and modify it in order to configure the OSPF

5
00:00:31,560 --> 00:00:35,220
‫routing protocol on each router.

6
00:00:35,500 --> 00:00:39,720
‫The playbook will be run against all routers.

7
00:00:39,740 --> 00:00:45,160
‫This is my inventory file and there is a group called the routers.

8
00:00:45,320 --> 00:00:58,310
‫The name of the task will be configure or OSPF and the comments will be network 0 0 0 0 0 0 0 0 0 area

9
00:00:58,610 --> 00:00:59,520
‫0.

10
00:00:59,960 --> 00:01:08,270
‫This command will start OSPF on each interface of the device for the backbone area.

11
00:01:08,500 --> 00:01:10,210
‫Let's say this twice.

12
00:01:10,280 --> 00:01:15,240
‫This is the administrative these days let's say 50.

13
00:01:15,410 --> 00:01:24,710
‫And also LICs send an other command and fix the default information originated so it will announce a

14
00:01:24,710 --> 00:01:37,090
‫default route default information originated and the most important part is that the swan bearings are

15
00:01:37,120 --> 00:01:42,050
‫outer OSPF one what does it mean.

16
00:01:42,050 --> 00:01:51,770
‫This means that these comments will be checked against this section and if they don't exist and Cybil

17
00:01:52,130 --> 00:02:02,890
‫will execute these three comments of course in this section and this all on the ansible control machine.

18
00:02:02,960 --> 00:02:07,030
‫I'll copy paste my new playbook in a new email file.

19
00:02:17,560 --> 00:02:19,610
‫This is the inventory file.

20
00:02:19,750 --> 00:02:28,530
‫It's the same file from the previous lecture and now I exit cute the playbook before executing the play

21
00:02:28,530 --> 00:02:28,830
‫book.

22
00:02:28,840 --> 00:02:35,640
‫I want to check the or I would think table and there are outing protocols that run on her out there

23
00:02:35,650 --> 00:02:41,070
‫on show IP protocols.

24
00:02:41,100 --> 00:02:43,870
‫There is no URL protocol.

25
00:02:43,950 --> 00:02:45,730
‫Running an IP.

26
00:02:45,860 --> 00:02:51,570
‫Are out there is no OSPF are out and Siebold playbook minus.

27
00:02:51,690 --> 00:02:56,550
‫I inventory in the AM ONE file from the same working directory

28
00:02:59,280 --> 00:03:01,150
‫and the playbook is executing

29
00:03:08,790 --> 00:03:16,110
‫we can see how the playbook changed the routers and we can see the command acts that have been executed

30
00:03:16,170 --> 00:03:17,620
‫on each device.

31
00:03:17,670 --> 00:03:26,860
‫For example these are the that have been executed on or out R5 back to my or out there on

32
00:03:27,230 --> 00:03:30,710
‫my executive or on Section router.

33
00:03:30,860 --> 00:03:41,550
‫We can see the comics that have been executed by ansible And of course share IP protocols shows us that

34
00:03:41,610 --> 00:03:50,390
‫OSPF one is running and I peer out we can see out in the routing table.

35
00:03:50,870 --> 00:03:55,320
‫Sure IP OSPF neighbor and can see the neighbors.

36
00:03:55,530 --> 00:04:01,440
‫This means that the OSPF routing protocol has been configured also.

37
00:04:01,470 --> 00:04:11,630
‫On the other routers This is how we execute only in a specific configuration section on a

38
00:04:11,640 --> 00:04:12,380
‫Cisco.

39
00:04:12,410 --> 00:04:13,850
‫I always know it's.


﻿1
00:00:00,440 --> 00:00:08,390
‫In the last lecture, we've seen how to run multiple comments on Cisco routers using the iOS command

2
00:00:08,390 --> 00:00:08,850
‫module.

3
00:00:09,140 --> 00:00:14,640
‫Now I want to show you how to save the output of those comments to a file.

4
00:00:14,840 --> 00:00:18,440
‫I'll modify the YAML file from the previous lecture.

5
00:00:18,440 --> 00:00:22,330
‫In the following way, I'll delete this line.

6
00:00:22,340 --> 00:00:26,180
‫I'm going to save only the output of the short version comment.

7
00:00:26,390 --> 00:00:29,300
‫I'll register like, say, my config.

8
00:00:30,230 --> 00:00:34,100
‫This is the variable that holds the output of the task.

9
00:00:35,520 --> 00:00:42,330
‫I'll delete this line because I don't want to print at the console, I want to save to a file.

10
00:00:43,370 --> 00:00:50,270
‫And I'll create here another task, its name is Save Output to a file on disk.

11
00:00:55,000 --> 00:00:59,300
‫And this task will use the copy module, so copy.

12
00:00:59,560 --> 00:01:04,220
‫And here we have content and we have something interesting.

13
00:01:04,630 --> 00:01:07,370
‫This is called Ginge to Template.

14
00:01:07,720 --> 00:01:19,000
‫So here between double clicks and curly braces, we have my config dot standard output, a study out

15
00:01:19,240 --> 00:01:20,450
‫of zero.

16
00:01:21,220 --> 00:01:23,170
‫This is my output.

17
00:01:23,800 --> 00:01:26,710
‫My config is the registered variable.

18
00:01:28,830 --> 00:01:33,700
‫And then we have the best from destination and the path to my file.

19
00:01:33,960 --> 00:01:42,910
‫So let's say here, slash home, slash Andre lab and Ansible and now we have another Ginge to template.

20
00:01:43,500 --> 00:01:47,670
‫So between Carly Brace's, we have to curly braces.

21
00:01:48,710 --> 00:01:49,430
‫Oh, sorry.

22
00:01:49,460 --> 00:01:52,730
‫Also here, we must have too curly braces.

23
00:01:53,090 --> 00:01:57,290
‫This is the syntax of Ginge to template language.

24
00:01:57,860 --> 00:02:10,010
‫OK, and here at this we have between two curly braces, inventory, underline hostname dot DST inventory.

25
00:02:10,010 --> 00:02:18,830
‫Hostname is like a built-In variable that holds the name from the inventory file is a conclusion.

26
00:02:19,010 --> 00:02:28,820
‫Inventory hostname will be in fact Routhier one or this IP address or this IP address, depending on

27
00:02:28,820 --> 00:02:31,580
‫the host this playbook runs against.

28
00:02:33,490 --> 00:02:41,920
‫Here, it's not necessary to have these spaces, if I put spaces here, they will be in the file name

29
00:02:41,920 --> 00:02:45,550
‫and the name of the file will start with a space.

30
00:02:45,550 --> 00:02:50,050
‫So without spaces here, good links are on the playbook.

31
00:02:50,410 --> 00:02:52,480
‫Ansible playbook.

32
00:02:52,930 --> 00:02:56,970
‫Minus, I hosts the name of the Yamal file.

33
00:02:57,010 --> 00:03:02,710
‫I recommend the HTML minus you yuan minus K.

34
00:03:07,190 --> 00:03:09,710
‫The play is running, the first task is running.

35
00:03:11,350 --> 00:03:14,360
‫And the playbook has been executed.

36
00:03:14,800 --> 00:03:22,570
‫Now, if I least the current working directory, I see text files with the same name as the hosts from

37
00:03:22,570 --> 00:03:23,860
‫the inventory file.

38
00:03:25,210 --> 00:03:31,750
‫So this is the short version coming out of the first quarter, the second quarter and so on, if I'm

39
00:03:31,750 --> 00:03:38,770
‫displaying the content of the first file, we can see the output of the show version command executed

40
00:03:38,770 --> 00:03:39,910
‫on the first quarter.

41
00:03:40,630 --> 00:03:42,850
‫OK, thanks all for the moment.

42
00:03:43,220 --> 00:03:49,510
‫Now you know how to save to file information retargeted by Ansible Modulus.


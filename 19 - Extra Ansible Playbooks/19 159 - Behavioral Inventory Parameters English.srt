﻿1
00:00:00,510 --> 00:00:09,230
‫Ansible uses variables to enable more flexibility in Playbook's and rolls, the following variables

2
00:00:09,390 --> 00:00:16,980
‫called behavioral inventory parameters, control how Ansible interacts with remote hosts.

3
00:00:17,640 --> 00:00:25,260
‫We have many behavioral inventory parameters, but now I'll present you the most important ones.

4
00:00:25,920 --> 00:00:35,430
‫Ansible host indicates the name of the host Ansible will connect to Ansible part specifies the SSA export

5
00:00:35,440 --> 00:00:36,000
‫number.

6
00:00:36,000 --> 00:00:40,220
‫If not using the default 22, then we have ansible user.

7
00:00:40,650 --> 00:00:50,090
‫This is the default SSX user name to use Ansible SSX pass indicates the SSX password to use.

8
00:00:50,370 --> 00:00:54,300
‫Then we have Ansible SSX private key file.

9
00:00:54,840 --> 00:00:58,680
‫This is the private key file used by SSX.

10
00:00:58,740 --> 00:01:05,190
‫If we have more private keys or we don't want to use the SSL agent.

11
00:01:06,100 --> 00:01:14,410
‫INAUDIBLE become, which is equivalent to ingestible Suto or inaudible to allow us to force privilege

12
00:01:14,410 --> 00:01:15,310
‫escalation.

13
00:01:16,210 --> 00:01:25,450
‫Then there is any become method which allows to set privilege escalation method like, for example,

14
00:01:25,450 --> 00:01:26,170
‫enable.

15
00:01:26,990 --> 00:01:36,890
‫Ansible become yuzu, which is equivalent to a simple pseudo user or ansible, so user allows to set

16
00:01:36,890 --> 00:01:44,600
‫the user you become through privilege, escalation, ansible become best, which is equivalent to a

17
00:01:44,600 --> 00:01:51,710
‫simple pseudo pece or ansible soopers allows you to set the privilege escalation password.

18
00:01:52,530 --> 00:02:01,280
‫Then we have Ansible Network OS, which is new in Ansible 2.5 index used with network zeolite or net

19
00:02:01,280 --> 00:02:08,060
‫conf in order to indicate what operating system Ansible is going to connect to.

20
00:02:08,750 --> 00:02:14,090
‫These are the most important, the behavioral inventory parameters.

21
00:02:14,420 --> 00:02:23,200
‫And in the next lecture we'll modify our ansible playbooks in order to use these variables.

22
00:02:23,540 --> 00:02:25,730
‫See you in just a few seconds.


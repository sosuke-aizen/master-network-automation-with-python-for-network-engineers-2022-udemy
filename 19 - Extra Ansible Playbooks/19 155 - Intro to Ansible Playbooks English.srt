﻿1
00:00:00,640 --> 00:00:07,630
‫In this lecture, we'll start talking about sensible Playbook's Playbook's our Antibalas configuration,

2
00:00:07,750 --> 00:00:10,120
‫deployment and orchestration language.

3
00:00:10,360 --> 00:00:18,340
‫They can describe a policy you want your remote servers to enforce or a set of steps in a general I.T.

4
00:00:18,340 --> 00:00:19,000
‫process.

5
00:00:19,390 --> 00:00:26,170
‫If ansible modules are the tools in your workshop, playbooks are your instruction manuals and your

6
00:00:26,170 --> 00:00:28,990
‫inventory file is your raw material.

7
00:00:29,770 --> 00:00:34,120
‫Playbooks are used to run a single or multiple tasks.

8
00:00:34,390 --> 00:00:40,960
‫They offer advanced functionality that can't be obtained with ansible hoc comments.

9
00:00:41,410 --> 00:00:46,030
‫Playbooks are well suited to deploying complex applications.

10
00:00:46,510 --> 00:00:50,290
‫Playbooks are Yamal configuration files for tasks.

11
00:00:50,740 --> 00:00:59,320
‫A playbook is a Yamal file that contains one or more place, place are dictionaries and are unordered.

12
00:00:59,680 --> 00:01:02,950
‫Each play contains one or more tasks.

13
00:01:02,950 --> 00:01:06,880
‫A task is a single action that will be executed.

14
00:01:07,300 --> 00:01:11,890
‫Tasks are represented is leasebacks, so or other matters.

15
00:01:12,880 --> 00:01:16,960
‫We use the Ansible Playbook Command to run a playbook.

16
00:01:17,900 --> 00:01:27,320
‫Now, let's see, an example will run unsensible ad hoc comment, and then we'll transform that comment

17
00:01:27,440 --> 00:01:28,510
‫into a playbook.

18
00:01:28,760 --> 00:01:36,710
‫We have the same topology we've used before and using the model, I am going to run show IP BGP neighbors

19
00:01:36,860 --> 00:01:38,210
‫on each router.

20
00:01:38,510 --> 00:01:41,600
‫I'm going to create an ansible ad hoc command.

21
00:01:41,750 --> 00:01:51,230
‫So Ansible minus I hosts, I'm going to use the inventory file from the current working directory routers.

22
00:01:51,500 --> 00:01:59,480
‫This is a group I'm going to run the command on each router of my group and then we have em all.

23
00:01:59,480 --> 00:02:04,270
‫We are using the role model and the argument is the command.

24
00:02:04,730 --> 00:02:07,640
‫So show IP BGP neighbors.

25
00:02:09,130 --> 00:02:15,050
‫I am connecting, using you one username and asking for the password.

26
00:02:15,790 --> 00:02:17,950
‫Now I am entering the password.

27
00:02:19,000 --> 00:02:20,530
‫And I'm waiting.

28
00:02:23,680 --> 00:02:30,370
‫After just a few seconds, Ansible has displayed the output of the command at the console.

29
00:02:31,370 --> 00:02:39,020
‫OK, here we can see the output of that comment that has been executed on each router.

30
00:02:39,380 --> 00:02:43,760
‫Now let's transform this ad hoc comment in a playbook.

31
00:02:44,210 --> 00:02:48,860
‫I've already created the playbook and the A YAML file.

32
00:02:49,040 --> 00:02:53,620
‫And now I want to explain to you what is all about here.

33
00:02:53,720 --> 00:02:57,230
‫We have a single play called Show BGP Neighbors.

34
00:02:57,530 --> 00:02:59,210
‫This is the name of the play.

35
00:02:59,730 --> 00:03:06,530
‫And Ansible will run the play on all routers in our topology here instead of routers.

36
00:03:06,530 --> 00:03:12,430
‫So of course I can write a single IP address or a hostname from my inventory file.

37
00:03:12,890 --> 00:03:19,550
‫We are not going to gather facts about devices and then we have a list of tasks.

38
00:03:19,940 --> 00:03:21,740
‫We have two tasks.

39
00:03:22,020 --> 00:03:31,330
‫The first task is called BGP Neighbors excusing the role model and executing the show IP BGP neighbors

40
00:03:31,340 --> 00:03:31,880
‫come comment.

41
00:03:32,720 --> 00:03:36,530
‫Then we have a register and this is very important.

42
00:03:36,800 --> 00:03:39,080
‫Ansible registers are used.

43
00:03:39,080 --> 00:03:45,740
‫When you want to capture the output of a single task into a variable, you can then use the value of

44
00:03:45,740 --> 00:03:52,340
‫these registers for different scenarios like this, playing at the console, conditional statement,

45
00:03:52,490 --> 00:03:54,050
‫logging and so on.

46
00:03:54,710 --> 00:04:00,110
‫In this case, the variable will contain the value target by the task.

47
00:04:00,380 --> 00:04:08,060
‫Each registered variable will be valid on the remote host where the task was run for the rest of the

48
00:04:08,060 --> 00:04:09,230
‫playbook execution.

49
00:04:09,560 --> 00:04:17,570
‫The exact content of the registered variable can vary widely depending on the type of task.

50
00:04:17,930 --> 00:04:24,810
‫For example, a shell module will include standard output and standard error output from the command

51
00:04:24,920 --> 00:04:26,740
‫you're on in the register.

52
00:04:26,750 --> 00:04:34,070
‫The variable, while the stop module, which retrieves file or file system status, will provide the

53
00:04:34,070 --> 00:04:40,640
‫details of the file that is best to the task, then we have another task.

54
00:04:40,940 --> 00:04:46,000
‫Its name is printing output and using the debug module.

55
00:04:46,310 --> 00:04:53,930
‫This module brings statements during execution printing the output at the console.

56
00:04:55,390 --> 00:05:04,410
‫CTT out underlined lines is a predefined return value and will return the standard output split line-by-line.

57
00:05:05,020 --> 00:05:09,910
‫So this is a building viable or a building return code.

58
00:05:10,330 --> 00:05:12,590
‫Now, let's run this playbook.

59
00:05:12,880 --> 00:05:22,750
‫I'm going to use the Ansible playbook comment minus I post the inventory file from the current directory.

60
00:05:23,620 --> 00:05:26,380
‫Then we have the name of the playbook.

61
00:05:26,570 --> 00:05:29,950
‫So show BGP Neighbors Dot Yamal.

62
00:05:31,220 --> 00:05:39,730
‫Both Phylis, the Yamal playbook file in the host's inventory file are in the same directory index,

63
00:05:39,800 --> 00:05:41,990
‫the current working directory.

64
00:05:44,180 --> 00:05:48,080
‫And then minus you, you want minus K.

65
00:05:50,450 --> 00:05:57,890
‫I am entering the password and we can see how Ansible is running the playbook, and this is the output

66
00:05:58,160 --> 00:06:01,880
‫we can see here the output of my playbook.

67
00:06:04,070 --> 00:06:10,970
‫This is the name of the playbook shall BGP neighbors, then we have the task, the first task, BGP

68
00:06:11,000 --> 00:06:18,070
‫neighbors next from here, then the second task pointing outward, and this is the output.

69
00:06:18,260 --> 00:06:21,890
‫So the standard output printed line by line.

70
00:06:23,110 --> 00:06:31,120
‫This is a very simple playbook, but using complex playbooks, Ansible is capable of deploying, automating

71
00:06:31,120 --> 00:06:39,310
‫or configuring very complex topologies, maybe you wonder why do we have to create such a playbook?

72
00:06:39,310 --> 00:06:44,570
‫So YAML file with a strict syntax just for a single comment?

73
00:06:44,710 --> 00:06:52,450
‫Maybe it would be easier if we are connecting to third autres and simply running these comments or this

74
00:06:52,450 --> 00:06:53,680
‫comment in this case.

75
00:06:54,630 --> 00:07:02,790
‫If we have are out there or if we have two daughters, maybe it doesn't work to create a playbook that

76
00:07:02,790 --> 00:07:04,740
‫executes the Shiite neighbors.

77
00:07:05,100 --> 00:07:12,360
‫But let's suppose we have a very big network, a service provider or something like this with hundreds

78
00:07:12,360 --> 00:07:13,380
‫of reporters.

79
00:07:13,380 --> 00:07:19,080
‫And in that case, it's not easy to run comments on each device.

80
00:07:19,080 --> 00:07:26,940
‫But using Ansible and Playbook's, we can run governments and reconfigure hundreds of routers in just

81
00:07:26,940 --> 00:07:27,870
‫a few seconds.

82
00:07:29,480 --> 00:07:37,260
‫Maybe you noticed that it took a very short time for Ansible to run the ship BGP neighbors in on the

83
00:07:37,520 --> 00:07:46,010
‫outer vics because by default, Ansible forks five processes that execute the tasks.

84
00:07:46,580 --> 00:07:53,480
‫You can use the minus F option and specify another number of parallel processes to use.

85
00:07:54,020 --> 00:08:02,120
‫This was our first lecture about Playbook's, but in the following lectures, I'll show you many other

86
00:08:02,120 --> 00:08:05,140
‫examples with Ansible Playbook's.

87
00:08:05,480 --> 00:08:07,640
‫See you in just a few seconds.


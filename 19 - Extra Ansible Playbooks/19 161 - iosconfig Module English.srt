﻿1
00:00:00,410 --> 00:00:07,460
‫In this lecture We'll take a look at how to configure Cisco IOS devices with ansible.

2
00:00:07,500 --> 00:00:11,100
‫There is a module called Iowas config.

3
00:00:11,100 --> 00:00:16,710
‫It is used to manage Cisco IOS configuration sections.

4
00:00:16,800 --> 00:00:22,930
‫I've created a very simple playbook that uses Irus config module.

5
00:00:23,130 --> 00:00:32,670
‫This playbook uses the same topology with five routers and uses the iris config module in order to configure

6
00:00:32,910 --> 00:00:34,150
‫devices.

7
00:00:34,590 --> 00:00:36,990
‫Let's take a look at this playbook.

8
00:00:37,260 --> 00:00:42,000
‫This playbook will use an inventory file like this one.

9
00:00:42,180 --> 00:00:51,440
‫I have a group called routers with five routers and stable host is the IP address of my router.

10
00:00:51,750 --> 00:00:59,560
‫And then we have Vargus section that defines pardon me thirst for my group.

11
00:00:59,670 --> 00:01:08,460
‫There is an simple network OS Parminter ansible user and a simple S-sh pass ansible become unseeable

12
00:01:08,460 --> 00:01:12,540
‫become method and ansible become best.

13
00:01:12,900 --> 00:01:21,260
‫We've talked a lot in a previous lecture about these behavioral inventory Baaraat meters.

14
00:01:21,270 --> 00:01:29,050
‫Please note that they're out there to use another enable password and the password is Python.

15
00:01:29,250 --> 00:01:36,230
‫All other authors use an enable password with the value of Cisco.

16
00:01:36,240 --> 00:01:42,370
‫Now I want to explain to you in detail what does this playbook do.

17
00:01:42,420 --> 00:01:54,240
‫The first task uses the I O S config module and the since these comments lines is used to send

18
00:01:54,720 --> 00:02:00,650
‫to the devices and that the commands will be sent in this order.

19
00:02:00,690 --> 00:02:10,200
‫Here we have a viable in fact this is going to templating viable and its value is the inventory hostname.

20
00:02:10,250 --> 00:02:17,840
‫In fact it will change the outer hostname to the value from the inventory file.

21
00:02:18,000 --> 00:02:21,270
‫This will be the host name of the first router.

22
00:02:21,450 --> 00:02:26,010
‫This will be the hostname of the second router and so on.

23
00:02:26,250 --> 00:02:31,620
‫After that it will modify the DNS server to 8.8.

24
00:02:31,630 --> 00:02:33,160
‫But they 2.8.

25
00:02:33,330 --> 00:02:37,880
‫This is the public DNS server of Google.

26
00:02:38,010 --> 00:02:46,260
‫Then it took these cables the HTP server and enables the be secure server.

27
00:02:46,460 --> 00:02:57,870
‫So HTP as another important option is save when you save when will save the running config to start

28
00:02:57,860 --> 00:02:59,040
‫up config.

29
00:02:59,240 --> 00:03:07,850
‫Only if there is any modification by default it doesn't save the running config to startup config back

30
00:03:08,120 --> 00:03:15,800
‫if it's not this is a difference between the startup config and the running config and Siebel will save

31
00:03:15,950 --> 00:03:19,240
‫the running config to startup config.

32
00:03:19,490 --> 00:03:25,750
‫Then we are A-G stir the output in a variable and using another task.

33
00:03:25,760 --> 00:03:30,350
‫I'll print that variable and the console.

34
00:03:30,440 --> 00:03:38,840
‫So this is a very simple play book with a single play and two basic tasks that come figuris to see score

35
00:03:38,860 --> 00:03:42,400
‫outers her out one and the router to.

36
00:03:42,520 --> 00:03:47,030
‫Now let's move to the control machine here.

37
00:03:48,750 --> 00:03:56,520
‫This is the inventory file that will be used and this is the playbook file that you will file that will

38
00:03:56,520 --> 00:03:58,140
‫be executed.

39
00:03:58,140 --> 00:04:00,870
‫In fact this is the playbook file.

40
00:04:00,870 --> 00:04:02,920
‫I've just explained to you.

41
00:04:03,450 --> 00:04:13,650
‫And now let's run the play book and see ball play book minus I inventory and from the same working directory

42
00:04:13,770 --> 00:04:24,170
‫will execute IRS basic config Latium will file our play book fight before running the playbook.

43
00:04:24,260 --> 00:04:26,850
‫Let's connect with the first helper in the run.

44
00:04:26,990 --> 00:04:38,600
‫Sure running config include HTP we can see that the HTP server is enabled and the secure HTP server

45
00:04:38,690 --> 00:04:42,080
‫is disabled and now either on the playbook

46
00:04:46,440 --> 00:04:49,090
‫the playbook has been executed.

47
00:04:49,170 --> 00:04:57,630
‫We can see if the output we can see how or out or two has been modified and BKC the comings that have

48
00:04:57,630 --> 00:04:59,130
‫been executed.

49
00:04:59,520 --> 00:05:08,090
‫And if I click one more time on her outer one we can see how eaks hostname changed from R1 to or out

50
00:05:08,090 --> 00:05:08,690
‫are on.

51
00:05:08,790 --> 00:05:18,160
‫And also we can see how the HTP server has been disabled and thus secure HTP server has been enabled.

52
00:05:18,480 --> 00:05:28,740
‫And of course the configuration has been saved to a startup config.

53
00:05:28,750 --> 00:05:34,060
‫In fact the save win option saved the configuration.

54
00:05:34,060 --> 00:05:42,280
‫This is how we use the I O S config module to configure Cisco IOS devices.


﻿1
00:00:01,070 --> 00:00:09,500
‫Now we'll continue developing our ansible playbook that configures this multi vendor topology, in fact,

2
00:00:09,590 --> 00:00:11,840
‫I've already created the playbook.

3
00:00:12,080 --> 00:00:14,090
‫This is a multiple playbook.

4
00:00:14,100 --> 00:00:15,140
‫There are two plays.

5
00:00:15,540 --> 00:00:20,800
‫This is the first play that configures the first artist to switch.

6
00:00:20,810 --> 00:00:22,910
‫This play has more tasks.

7
00:00:22,910 --> 00:00:30,500
‫And we have another play, the second play that only configures the Cisco iOS router.

8
00:00:31,010 --> 00:00:35,860
‫I am going to explain to you in detail what this playbook does.

9
00:00:36,620 --> 00:00:46,580
‫The first task configures a new user on August uSwitch six, the spawning three to Rapide Pavillon spanning

10
00:00:46,580 --> 00:00:50,030
‫three protocol and creates two Vlaams.

11
00:00:50,480 --> 00:00:55,790
‫I've already explained to you in a previous lecture what it does.

12
00:00:56,030 --> 00:00:57,920
‫Then we have another task.

13
00:00:58,490 --> 00:01:00,590
‫This task uses a loop.

14
00:01:01,160 --> 00:01:07,670
‫In fact, here we are setting the VLAN interfaces or the access parts.

15
00:01:08,570 --> 00:01:18,530
‫We are using the iOS config module and in the interface section, we are running these comments for

16
00:01:18,740 --> 00:01:21,290
‫each interface of my list.

17
00:01:21,500 --> 00:01:28,100
‫This is at least a list of interfaces in our script will iterate over this list.

18
00:01:28,550 --> 00:01:36,320
‫First, it will configure the Ethernet through the interface, then the second interface and so on.

19
00:01:37,570 --> 00:01:49,150
‫Here I got this value if the three using item dot interface where the interface is the key of this dictionary,

20
00:01:49,960 --> 00:01:56,870
‫Vilan is another key here where I've allocated the interface to Valeant.

21
00:01:56,870 --> 00:02:02,680
‫Then I used this value, then using item dot vilan.

22
00:02:03,070 --> 00:02:04,940
‫So Vilan is the key.

23
00:02:05,230 --> 00:02:10,510
‫This is the item and this is the first key and the second key.

24
00:02:10,840 --> 00:02:14,650
‫And we are accessing the value of the keys using their names.

25
00:02:15,160 --> 00:02:20,410
‫I've already explained to you how to use loops in a previous lecture.

26
00:02:20,710 --> 00:02:27,130
‫I am also setting forth the security with a maximum of four Mac addresses.

27
00:02:27,310 --> 00:02:33,370
‫If more than four Mac addresses appear, it will shut that interface.

28
00:02:34,340 --> 00:02:43,220
‫All these companies will be executed after the no shot comment I registered at the output and printed

29
00:02:43,220 --> 00:02:44,150
‫at the console.

30
00:02:45,330 --> 00:02:53,700
‫Then in another task, I am setting the trunks, I am using another loop in our example.

31
00:02:53,750 --> 00:03:01,800
‫There is only one trunk, the Ethernet to interface, but I am using a loop because maybe I will extend

32
00:03:02,100 --> 00:03:03,150
‫this lab.

33
00:03:03,150 --> 00:03:07,280
‫So maybe we put here another switch and reconfigure another trunk.

34
00:03:07,290 --> 00:03:11,260
‫And in that case, it would be better to use a loop.

35
00:03:12,060 --> 00:03:20,150
‫So we are looping over this list with only one item we are getting here, the interface.

36
00:03:20,160 --> 00:03:29,280
‫So the key of the dictionary, because we are configuring that section and we are setting that interface

37
00:03:29,280 --> 00:03:37,040
‫is a trunk port and both villans, Valentin and Vilan 20 are allowed on that trunk.

38
00:03:37,830 --> 00:03:42,170
‫We are starting the output and printing it at the console.

39
00:03:42,510 --> 00:03:51,450
‫Then we are setting the switch to virtual interfaces or as VI using another loop, we iterate over a

40
00:03:51,450 --> 00:03:53,460
‫list of interfaces.

41
00:03:53,610 --> 00:03:59,820
‫And for each interface of this list we are setting its IP address.

42
00:04:00,060 --> 00:04:05,510
‫We are getting the IP address by using the dictionary key.

43
00:04:05,700 --> 00:04:12,330
‫So item dot IP means in fact the value of this key.

44
00:04:13,260 --> 00:04:18,810
‫Then we are configuring the OS routing protocol of course in another task.

45
00:04:19,810 --> 00:04:28,930
‫I am using the section out there, SPF one, and I am executing these three comments and this is our

46
00:04:28,930 --> 00:04:33,010
‫first play, then I've created another play.

47
00:04:33,010 --> 00:04:39,250
‫The second play, this play configures SPF on the Cisco router.

48
00:04:39,730 --> 00:04:42,520
‫This is the name of they're out there from the inventory file.

49
00:04:42,610 --> 00:04:44,080
‫I'll show you in a moment.

50
00:04:44,080 --> 00:04:46,900
‫How does the inventory file look like?

51
00:04:47,290 --> 00:04:56,470
‫And we have a single task here that configures or EPF In fact, these three Cumming's are executed in

52
00:04:56,470 --> 00:05:01,270
‫this section and I am printing the output and this is all.

53
00:05:02,450 --> 00:05:10,190
‫We are going to execute the playbook simply by writing a simple playbook, minus I and the name of the

54
00:05:10,190 --> 00:05:13,550
‫inventory file and the name of the YAML file.

55
00:05:13,850 --> 00:05:17,860
‫Now, let's see, how did I modify the inventory file?

56
00:05:18,170 --> 00:05:23,520
‫So I've created here another group called Cisco with just one router.

57
00:05:24,200 --> 00:05:25,370
‫These are from here.

58
00:05:25,490 --> 00:05:33,290
‫This is the IP address of they're out there and I've specified its IP address using this variable ansible

59
00:05:33,290 --> 00:05:34,810
‫underline host.

60
00:05:35,480 --> 00:05:46,010
‫Then we have some variables for this section network OS, IOW, the user, the password, the enable

61
00:05:46,010 --> 00:05:48,830
‫method and the enable password.

62
00:05:49,880 --> 00:05:58,460
‫Let's go now to the ansible control machine, I've already copied these two files, the Yamal file in

63
00:05:58,460 --> 00:06:00,590
‫the inventory file to this machine.

64
00:06:02,370 --> 00:06:07,260
‫This is the inventory file and this is the YAML file.

65
00:06:08,440 --> 00:06:14,260
‫The Yamal file I've just explained to you, and now let's execute it.

66
00:06:18,610 --> 00:06:24,010
‫The playbook is executing this is the first play, the first task.

67
00:06:26,580 --> 00:06:28,260
‫These are the comments.

68
00:06:29,700 --> 00:06:30,840
‫The second task.

69
00:06:31,920 --> 00:06:39,690
‫It configures Vilan interfaces, it allocates the interfaces to their villans.

70
00:06:44,210 --> 00:06:46,670
‫OK, these are the comments.

71
00:06:48,430 --> 00:06:50,110
‫The next task.

72
00:06:51,160 --> 00:06:55,960
‫Setting the trunk and now setting the switch to virtual interfaces.

73
00:06:56,940 --> 00:07:04,230
‫And now it's configuring the SPF routing protocol on the start layer three switch.

74
00:07:06,660 --> 00:07:14,160
‫These are the comments and this is the second play configuring Cisco iOS devices.

75
00:07:14,460 --> 00:07:20,390
‫This is the task configure or SBF and printing at the console.

76
00:07:20,640 --> 00:07:22,890
‫And these are the comix.

77
00:07:23,520 --> 00:07:26,490
‫We have no error and very, very good.

78
00:07:27,510 --> 00:07:36,750
‫OK, this was another example, a more complex one that contains multiple plays in a single playbook,

79
00:07:37,140 --> 00:07:40,250
‫and each play has multiple tasks.

80
00:07:41,010 --> 00:07:48,750
‫We are configuring villans switch to virtual interfaces, trunk's and routing protocols in a multi vendor

81
00:07:48,750 --> 00:07:53,520
‫environment composed of our star and Cisco devices.


﻿1
00:00:01,280 --> 00:00:11,990
‫Ansible 2.5 introduced two new connection pipes network, Seelie and the Net Bryanna Ansible 2.5 using

2
00:00:11,990 --> 00:00:17,420
‫networking modules required the connection type to be set to local.

3
00:00:17,990 --> 00:00:26,450
‫Using a local connection means that the playbook executes the python or ansible module locally and then

4
00:00:26,450 --> 00:00:30,680
‫connects to a networking platform to perform tasks.

5
00:00:31,220 --> 00:00:38,930
‫This was sufficient, but different than how most new networking ansible modules functioned.

6
00:00:39,650 --> 00:00:48,800
‫In general, ansible modules for Linux servers are executed on the remote host compared to being executed

7
00:00:48,800 --> 00:00:51,650
‫locally on the ansible control node.

8
00:00:52,760 --> 00:01:01,880
‫Even if many networking platforms can execute Python code, the vast majority, including Cisco, iOS

9
00:01:02,060 --> 00:01:07,640
‫or iOS, requires the CLIA or an API.

10
00:01:07,790 --> 00:01:12,020
‫Is that the only means for interacting with the device?

11
00:01:12,800 --> 00:01:14,150
‫Let's take a look here.

12
00:01:14,790 --> 00:01:23,690
‫If we are interacting with networking devices like routers or switches, the Python code is executed

13
00:01:23,690 --> 00:01:26,120
‫locally on the control node.

14
00:01:26,450 --> 00:01:35,330
‫If we are interacting with Linux hosts, the Python code is copied and executed on the Linux hosts.

15
00:01:36,380 --> 00:01:44,780
‫Network module's support, an additional parameter called a provider used for passing of credentials,

16
00:01:45,350 --> 00:01:54,170
‫a network automation playbook would run locally, uses ansible inventory, but then uses the provider

17
00:01:54,320 --> 00:02:00,560
‫on a task by task basis to authenticate and connect to each networking platform.

18
00:02:00,770 --> 00:02:08,120
‫This differs from a Linux focused playbook that would initially look into devices using credentials

19
00:02:08,120 --> 00:02:16,370
‫in the inventory itself or pass through the command line and then execute the python code on their remote

20
00:02:16,370 --> 00:02:16,910
‫system.

21
00:02:17,240 --> 00:02:25,070
‫Starting with Ansible 2.5 connection network zeolite endnote connection local is the preferred method

22
00:02:25,190 --> 00:02:28,250
‫of interacting with the networking devices.

23
00:02:28,730 --> 00:02:38,180
‫Networks like Conexion plugin provides a connection to remote devices over the S.H. and implements a

24
00:02:38,180 --> 00:02:39,110
‫CLIA shell.

25
00:02:39,470 --> 00:02:47,450
‫This connection plug in is typically used by network devices for sending and receiving Seelie commands

26
00:02:47,450 --> 00:02:49,070
‫to network devices.

27
00:02:49,460 --> 00:02:55,790
‫This allows Playbook's to look, feel and operate just like they do on Linux hosts.

28
00:02:56,180 --> 00:03:03,470
‫Now let's refer to some examples and see what are the differences between playbooks that use the old

29
00:03:03,470 --> 00:03:11,070
‫connection, local method and playbooks that use the new connection network KELLYE method in this example.

30
00:03:11,120 --> 00:03:19,820
‫I used the connection local and using the iOS command module, I am running to comix the short version

31
00:03:19,940 --> 00:03:23,710
‫comment and the show IP interface brief comment.

32
00:03:24,110 --> 00:03:27,960
‫These comments don't require privileged access.

33
00:03:28,430 --> 00:03:30,210
‫Now let's run this playbook.

34
00:03:30,830 --> 00:03:34,400
‫I am connected to the ansible control node.

35
00:03:34,820 --> 00:03:40,820
‫This is my hosts file and I'm going to run the playbook ansible playbook.

36
00:03:43,390 --> 00:03:50,350
‫I'm using the inventory file from the car and talking like actory and now the name of my playbook,

37
00:03:50,400 --> 00:03:55,490
‫I comment on darlyne local dot Yamal minus you.

38
00:03:55,770 --> 00:03:57,220
‫You want minus K.

39
00:04:01,260 --> 00:04:04,410
‫And it has executed the playbook.

40
00:04:05,380 --> 00:04:12,430
‫If we want to execute privileged criminals, we must use a variable in this example.

41
00:04:12,460 --> 00:04:20,970
‫I'm also using the old connection, local method, but I am creating a variable using the keyword VARs.

42
00:04:21,970 --> 00:04:26,740
‫This is a playbook keyboard and creates a dictionary of variables.

43
00:04:28,090 --> 00:04:36,610
‫My dictionary is named Log-in, and I am using the login variable, the login dictionary is a provider

44
00:04:36,730 --> 00:04:39,740
‫for the iOS command module.

45
00:04:40,630 --> 00:04:48,700
‫In fact, I'm specifying the username that is going to connect using a stage, his password, the enabled

46
00:04:48,700 --> 00:04:53,380
‫passport, and that it must enter the enable mode.

47
00:04:54,620 --> 00:05:04,970
‫And here I'm running on enable comment or a privileged comment, in fact, Ansible will enter the enable

48
00:05:04,970 --> 00:05:09,200
‫mode and will run this comment leaks from this playbook.

49
00:05:16,000 --> 00:05:23,950
‫In this case is not necessary to specify the username because the username and password are specified

50
00:05:24,100 --> 00:05:25,730
‫inside my playbook.

51
00:05:26,200 --> 00:05:28,660
‫I'm just executing the playbook.

52
00:05:31,700 --> 00:05:34,050
‫And the playbook has been executed.

53
00:05:34,340 --> 00:05:39,890
‫We can see here the output of the privileged Cheron comment.

54
00:05:40,900 --> 00:05:47,620
‫Now, let's see how can we transform this playbook that uses the old connectional local method to a

55
00:05:47,620 --> 00:05:52,360
‫playbook that uses the new connection network KELLYE method?

56
00:05:53,240 --> 00:06:01,490
‫One more time, when working with sensible networking devices, we must use networks like this way,

57
00:06:01,520 --> 00:06:08,510
‫ansible tricks, the remote node is a networking device with a limited execution environment.

58
00:06:08,750 --> 00:06:16,430
‫Without this setting, Ansible would attempt to use SSX to connect to a very remote node and execute

59
00:06:16,430 --> 00:06:24,110
‫the Python script on the remote device, which would fail because Python generally isn't available on

60
00:06:24,110 --> 00:06:25,540
‫networking devices.

61
00:06:26,570 --> 00:06:35,390
‫So here I'm using connections to actually become yes and become method enable, this means it will enter

62
00:06:35,420 --> 00:06:45,740
‫the enablement, then I am using the vast keywords and I'm specifying the user, the S6 pass, the compass.

63
00:06:45,770 --> 00:06:47,420
‫This is the enable password.

64
00:06:47,690 --> 00:06:49,910
‫We could have here another password.

65
00:06:50,120 --> 00:06:54,770
‫And also we must specify the network operating system.

66
00:06:54,780 --> 00:06:58,280
‫In this case, we have a Cisco iOS router.

67
00:06:58,850 --> 00:07:03,020
‫These variables, ansible user and policies pass.

68
00:07:03,110 --> 00:07:04,700
‫Ansible become passe.

69
00:07:04,700 --> 00:07:12,800
‫Ansible network OS are predefined and are called behavioral inventory parameters.

70
00:07:14,820 --> 00:07:18,300
‫This means we can't use other names here.

71
00:07:18,570 --> 00:07:25,140
‫The names I used here are amendatory and now I'm going to run the playbook.

72
00:07:26,250 --> 00:07:35,280
‫In fact, I'm using just another e-mail file, so iris command network, CLIA privileges, dot e-mail.

73
00:07:38,940 --> 00:07:45,870
‫And it has executed successfully, we can see the output of the show on comment.

74
00:07:46,830 --> 00:07:48,100
‫It's a conclusion.

75
00:07:48,150 --> 00:07:57,120
‫This is how you transform old playbook that uses the connection and local method to a new playbook that

76
00:07:57,120 --> 00:08:00,810
‫uses the connection network zeolite method.


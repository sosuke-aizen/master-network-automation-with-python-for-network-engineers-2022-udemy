﻿1
00:00:04,800 --> 00:00:12,360
‫In this lecture, we'll take a look at how to automate devices from different vendors, not Cisco with

2
00:00:12,360 --> 00:00:17,870
‫Ansible, and we'll take a look at Augusta in a previous lecture.

3
00:00:17,910 --> 00:00:24,000
‫I've shown you how to configure and run our veto's in Jenness three.

4
00:00:24,630 --> 00:00:31,460
‫If you don't have to configure ingenius three, you can refer to that previous lecture.

5
00:00:32,010 --> 00:00:40,110
‫We have these topology with two or three star switches, and I have a direct connection from my ansible

6
00:00:40,110 --> 00:00:43,050
‫control machine to those switches.

7
00:00:44,040 --> 00:00:53,790
‫There are several options how artists use can be automated artist ideas can be configured in total using

8
00:00:53,790 --> 00:01:04,470
‫three main approaches Jason's scripts through HTP, CLIA, using essayistic and open config using Netcom

9
00:01:04,800 --> 00:01:15,960
‫or RESCAN in this lecture will focus on the second option, which is Kellye using SSX regarding Ansible

10
00:01:15,960 --> 00:01:17,110
‫and Arista.

11
00:01:17,310 --> 00:01:25,050
‫There are plenty of modules developed to configure various aspects of our status and they can be good

12
00:01:25,050 --> 00:01:27,680
‫when you deploy a single new customers.

13
00:01:28,080 --> 00:01:36,780
‫But in this lecture will use a single module called iOS config, which in a nutshell accepts all the

14
00:01:36,780 --> 00:01:38,490
‫configuration comments.

15
00:01:39,240 --> 00:01:42,750
‫So will use iOS underline config.

16
00:01:44,340 --> 00:01:54,300
‫This is the model and this is eeks help its documentation, we start from a previous YAML file that

17
00:01:54,300 --> 00:01:59,040
‫we've used for configuring a Cisco iOS device.

18
00:02:00,100 --> 00:02:04,030
‫So I'll save as artist to iOS config.

19
00:02:07,860 --> 00:02:17,250
‫OK, this is our e-mail file first, we'll modify the inventory file here, I let the new section called

20
00:02:17,760 --> 00:02:17,950
‫the.

21
00:02:22,430 --> 00:02:31,070
‫There are two switches in my topology, so I'm going to add them to the inventory file Augustan with

22
00:02:31,070 --> 00:02:35,450
‫this IP address and start with this IP address.

23
00:02:35,600 --> 00:02:44,150
‫So here I'll write down Ansible host equals and now IP address.

24
00:02:44,540 --> 00:02:48,830
‫This is the IP address Ansible is going to connect to.

25
00:02:57,080 --> 00:03:06,200
‫I remind you that Ansible host is a predefined variable called behavioral inventory parameter, and

26
00:03:06,200 --> 00:03:10,250
‫in fact, it's the IP address of the managed node.

27
00:03:10,640 --> 00:03:16,380
‫Of course, we must have a direct SSX connection to this IP address.

28
00:03:16,790 --> 00:03:17,700
‫Let's check it.

29
00:03:18,230 --> 00:03:20,230
‫This is my ansible control machine.

30
00:03:20,450 --> 00:03:23,840
‫And from here I'll ping the star switch.

31
00:03:33,550 --> 00:03:37,310
‫The username is admin and the password is.

32
00:03:39,620 --> 00:03:42,230
‫OK, we are connected to our.

33
00:03:43,900 --> 00:03:52,270
‫Now back to our Playbook file, our YAML file, I'll start modifying it, so I'm going to configure

34
00:03:52,390 --> 00:03:54,250
‫the group called Hoggy Star.

35
00:03:55,160 --> 00:04:03,860
‫The name of the playbook is configuring our devices, our staff, Vios devices, the name of the task

36
00:04:03,860 --> 00:04:06,470
‫is basic configuration.

37
00:04:06,710 --> 00:04:12,870
‫And instead of Iooss config, I'm going to use E OS config.

38
00:04:13,190 --> 00:04:18,270
‫This is the star module in the module documentation here.

39
00:04:18,500 --> 00:04:21,380
‫There is also a save when parameter.

40
00:04:25,890 --> 00:04:34,410
‫Saverin means that will save the running conflict to start up config if the running config has been

41
00:04:34,410 --> 00:04:35,100
‫modified.

42
00:04:36,240 --> 00:04:39,840
‫And then I look at here three comments.

43
00:04:42,350 --> 00:04:51,200
‫First, I'm going to create a new user name like, say, user name you one or roll network admin secret

44
00:04:51,440 --> 00:04:52,060
‫artist.

45
00:04:52,790 --> 00:05:02,050
‫And the second comment is spanning three mod rapid p vesty and let's create the villains.

46
00:05:02,090 --> 00:05:09,220
‫So in this topology, there are two villains, villain Pan and the Villain Twente, of course, excepting

47
00:05:09,230 --> 00:05:10,750
‫the default villain one.

48
00:05:11,390 --> 00:05:13,400
‫So let's create the two villains.

49
00:05:13,610 --> 00:05:17,000
‫Villain then villain 20.

50
00:05:17,950 --> 00:05:27,160
‫These are configuration comments and I don't modify anything else, I register the output in a variable

51
00:05:27,370 --> 00:05:32,500
‫called simply output, and I am printing that variable at the console.

52
00:05:34,040 --> 00:05:41,990
‫Now, this is a console connection to the past week, and I want to see if there are Vilan then and

53
00:05:41,990 --> 00:05:44,540
‫Vilan 20 configured Chauvelin.

54
00:05:46,280 --> 00:05:51,890
‫There is no Vilan Ten or a villain 20 Suranne include username.

55
00:05:54,720 --> 00:05:59,250
‫There is no you one username here, we have username.

56
00:06:04,270 --> 00:06:12,910
‫And this all, I'll copy paste the Playbook file in a Yemen file on the ansible control machine.

57
00:06:15,530 --> 00:06:16,330
‫So here.

58
00:06:24,600 --> 00:06:27,500
‫And the same for the inventory file.

59
00:06:35,710 --> 00:06:44,560
‫In the inventory file, I must also specify other arguments that are needed by Ansible to connect to

60
00:06:44,560 --> 00:06:52,050
‫and configure those switches in the VCR, the user name, the SSX password and so on.

61
00:06:52,390 --> 00:06:56,620
‫So I'll simply copy this section from Cisco.

62
00:07:00,530 --> 00:07:02,180
‫And I'll paste it here.

63
00:07:05,540 --> 00:07:10,370
‫Instead of outbursts, I have added to this is my section.

64
00:07:12,720 --> 00:07:23,910
‫Instead of Ansible network I os, I have E os, this is the operating system, the name is admin, the

65
00:07:23,910 --> 00:07:28,080
‫password, these are the star and the become password.

66
00:07:28,590 --> 00:07:32,610
‫This is the enable password is also Arkestra.

67
00:07:33,930 --> 00:07:42,810
‫And that's all I'm saving the file and I'm going to execute the playbook against the two switches.

68
00:07:43,950 --> 00:07:52,170
‫And a simple playbook minus eye inventory, I am using the inventory file from the current working directory

69
00:07:52,380 --> 00:08:00,090
‫and now the name of my playbook, start US config dot Yemen and I'm going to run the playbook.

70
00:08:00,090 --> 00:08:02,070
‫The playbook is executing.

71
00:08:06,220 --> 00:08:12,910
‫And we can see how Ansible changed the configuration of our two switches here.

72
00:08:13,090 --> 00:08:16,900
‫We can see the comments that have been executed.

73
00:08:17,140 --> 00:08:25,660
‫We can see how it created the villains, how the spending three mode has been changed to Rapid Peevski,

74
00:08:25,840 --> 00:08:29,000
‫how the user name has been created and so on.

75
00:08:29,740 --> 00:08:32,430
‫Now back to our ARISTOS week.

76
00:08:32,800 --> 00:08:34,690
‫Let's check it one more time.

77
00:08:35,650 --> 00:08:38,280
‫There is a user name called Yuan.

78
00:08:38,620 --> 00:08:41,200
‫It has been created by Ansible.

79
00:08:41,380 --> 00:08:46,810
‫And we have two new villains, Valentin and Vilan 20.

80
00:08:47,470 --> 00:08:56,430
‫So we can see that configuring devices from different vendors is very simple and the Yamal file is similar.

81
00:08:56,770 --> 00:09:02,080
‫We just change the module and use the options of that module.

82
00:09:02,860 --> 00:09:07,330
‫We can see all options in the ansible documentation.

83
00:09:07,900 --> 00:09:16,480
‫In the next lecture, I'll show you how to use loops and how to create multiple tasks in a playbook

84
00:09:16,480 --> 00:09:20,820
‫to configure other aspects of our topology.

85
00:09:21,100 --> 00:09:29,620
‫So in fact, we will configure the strict virtual interface and the the OS PDF routing protocol.

86
00:09:29,920 --> 00:09:31,990
‫See you in just a few seconds.


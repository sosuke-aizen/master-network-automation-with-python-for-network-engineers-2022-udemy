﻿1
00:00:00,420 --> 00:00:07,650
‫In this lecture, we'll take a look at Yamal if you are already familiar with Yamal files.

2
00:00:07,830 --> 00:00:15,540
‫Feel free to skip this lecture and head over to the next one if you haven't worked with YAML files in

3
00:00:15,540 --> 00:00:16,140
‫the past.

4
00:00:16,320 --> 00:00:19,080
‫I would highly recommend going through this lecture.

5
00:00:19,590 --> 00:00:25,710
‫Understanding Gamwell is crucial for being proficient in writing and using Ansible.

6
00:00:25,710 --> 00:00:31,520
‫Playbook's Yamal is an acronym that stands for Yamal Ain't Markup Language.

7
00:00:31,800 --> 00:00:37,110
‫We are interested in Yamal because ansible playbooks are written in Yemen.

8
00:00:38,040 --> 00:00:47,370
‫Yemen is a data structure format similar to XML and Jason here you can see the same information formatted

9
00:00:47,670 --> 00:00:50,970
‫in Yamal, Jason and XML.

10
00:00:52,000 --> 00:00:58,360
‫Please take a minute and focus on how each format looks like.

11
00:01:09,710 --> 00:01:13,640
‫Now, let's see what is the structure of a YAML file.

12
00:01:13,940 --> 00:01:17,030
‫This is an example of a YAML file.

13
00:01:17,910 --> 00:01:27,510
‫A YAML file starts with three dashes or three minus characters, a line that starts with a hash sign

14
00:01:27,630 --> 00:01:28,560
‫is a comment.

15
00:01:28,800 --> 00:01:30,930
‫This is a comment outline.

16
00:01:31,440 --> 00:01:33,870
‫Then we have key value pairs.

17
00:01:34,140 --> 00:01:40,770
‫The simplest form of representing data in Yamal is is a key value pair.

18
00:01:41,010 --> 00:01:48,400
‫Here we have three key value pairs name Colon Space and John H.

19
00:01:48,810 --> 00:01:50,460
‫The second key colon.

20
00:01:50,970 --> 00:01:55,330
‫Then we have a space and the value location Calon.

21
00:01:55,890 --> 00:01:59,160
‫Again, we have a space and London.

22
00:02:00,130 --> 00:02:07,780
‫Then we can have a list, for example, OS from operating system, and then we have Linux Windows and

23
00:02:07,780 --> 00:02:14,200
‫make another list programming and we have Java, Python and C++.

24
00:02:14,560 --> 00:02:18,160
‫You must know that Yamal has a very strict syntax.

25
00:02:18,520 --> 00:02:26,620
‫There are online Yamal validators in the you can try some such validators to see how it works.

26
00:02:27,520 --> 00:02:32,670
‫I'll copy this code and I'll post it here in this validator.

27
00:02:33,520 --> 00:02:39,490
‫OK, I'll click on Validate and I can see that this is a valid Yamal.

28
00:02:39,910 --> 00:02:49,870
‫But now if I erase this base from here, so after the minus sign and before the Java word and I press

29
00:02:49,870 --> 00:02:56,940
‫on validate, it says unable to pass, this isn't a valid YAML file.

30
00:02:57,970 --> 00:03:01,480
‫The same happens if I use here a tab.

31
00:03:02,370 --> 00:03:08,890
‫This isn't a valid Yamal format because I can't define a sequence in mapping.

32
00:03:09,360 --> 00:03:17,100
‫So when you are defining, at least you're right, each element of the list using a minus sign a space

33
00:03:17,100 --> 00:03:18,480
‫and then the value.

34
00:03:19,500 --> 00:03:29,040
‫At the end of the Yamal file, we have three DOT characters, this indicates that the YAML file ended,

35
00:03:29,730 --> 00:03:31,470
‫then we have dictionaries.

36
00:03:32,040 --> 00:03:40,830
‫I have here a dictionary, USA population, Kolon space and the value location, column space and the

37
00:03:40,830 --> 00:03:41,330
‫value.

38
00:03:41,580 --> 00:03:44,060
‫These are key value pairs.

39
00:03:44,610 --> 00:03:48,510
‫OK, and this space, as I said before, is mandatory.

40
00:03:49,020 --> 00:03:51,240
‫This is another dictionary.

41
00:03:52,280 --> 00:03:55,880
‫And if I try to validate these dictionaries.

42
00:03:57,740 --> 00:03:58,600
‫Let's go here.

43
00:04:00,170 --> 00:04:07,040
‫We can see that this is a valid YAML file, but if I erase these two spaces.

44
00:04:10,350 --> 00:04:15,850
‫OK, it validates, but this isn't a valid dictionary anymore.

45
00:04:15,900 --> 00:04:18,110
‫It understands something else.

46
00:04:19,270 --> 00:04:27,310
‫So the elements must be all indented with two spaces, exactly like in Python.

47
00:04:28,210 --> 00:04:34,510
‫These elements belong to the same dictionary because they use the same indentation.

48
00:04:34,900 --> 00:04:37,030
‫Two spaces before them.

49
00:04:37,420 --> 00:04:44,610
‫Using the same number of spaces means that they are properties of the same dictionary.

50
00:04:45,510 --> 00:04:52,920
‫Then we can have a list of dictionaries, every list element is a different dictionary.

51
00:04:53,280 --> 00:04:55,260
‫This is the first list element.

52
00:04:55,710 --> 00:04:57,840
‫This is the second list element.

53
00:04:58,200 --> 00:05:03,550
‫And you can see here key value pairs, type car color, red and so on.

54
00:05:03,750 --> 00:05:06,470
‫This is a list and this is the dictionary.

55
00:05:06,720 --> 00:05:09,410
‫I have a dictionary inside the list.

56
00:05:09,990 --> 00:05:20,610
‫You must also know that there are online converters that convert from Yamal to Jason or from Jason to

57
00:05:20,610 --> 00:05:23,880
‫Yamal, from Yamal to SML and so on.

58
00:05:24,090 --> 00:05:32,250
‫For example, I have this list and I want to see what is the representation in Jason.

59
00:05:33,520 --> 00:05:43,420
‫OK, this is the Jason Furman, if I want this YAML file represented in XML, I convert it to XML.

60
00:05:44,410 --> 00:05:50,470
‫This is the XML file and this is the CSFI file.

61
00:05:51,660 --> 00:05:54,890
‫Of course, this is a valid YAML file.

62
00:05:56,380 --> 00:06:03,200
‫It's enough for the moment about Yamal, we'll talk more about Yamal in the next lecture's where we'll

63
00:06:03,200 --> 00:06:05,630
‫take a look at Ansible Playbook's.


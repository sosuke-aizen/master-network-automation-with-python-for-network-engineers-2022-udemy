﻿1
00:00:00,640 --> 00:00:04,330
‫In this section will start talking about functions.

2
00:00:05,230 --> 00:00:12,300
‫This is a core topic in Python and in any other programming, language functions are everywhere.

3
00:00:13,090 --> 00:00:21,430
‫We've been using tons of built-In functions like print Len sorted in the methods for string's leaks,

4
00:00:21,430 --> 00:00:23,110
‫topolice or dictionaries.

5
00:00:23,320 --> 00:00:28,660
‫These methods are, in fact special type of functions attached to objects.

6
00:00:29,050 --> 00:00:33,100
‫Now we are going to see how to define our own functions.

7
00:00:34,240 --> 00:00:39,880
‫First, let's see what is a function and why do we need functions in our application?

8
00:00:40,420 --> 00:00:47,130
‫Well, we use functions to organize our code in blocks, then can be later reused.

9
00:00:47,560 --> 00:00:55,570
‫This is useful because we want better readability for our code, modularity and also timesaving when

10
00:00:55,570 --> 00:00:57,470
‫designing and running code.

11
00:00:57,940 --> 00:01:01,750
‫This is a key part of becoming an effective programmer.

12
00:01:02,590 --> 00:01:10,210
‫Functions allow us to create blocks of code that can be easily executed many times without needing to

13
00:01:10,210 --> 00:01:13,990
‫constantly rewrite or copy paste that block of code.

14
00:01:14,500 --> 00:01:22,270
‫Instead, we create a function that executes that block of code and after that we call the function.

15
00:01:23,130 --> 00:01:29,230
‫There is a term called TRI, which is an acronym for Don't Repeat Yourself functions.

16
00:01:29,400 --> 00:01:31,160
‫Keep our code dry.

17
00:01:31,590 --> 00:01:35,960
‫This is opposite to what an acronym for, right?

18
00:01:35,970 --> 00:01:38,150
‫Everything twice and fix.

19
00:01:38,160 --> 00:01:39,060
‫Not good at all.

20
00:01:39,780 --> 00:01:44,240
‫Let's explore the syntax of creating our own functions.

21
00:01:44,850 --> 00:01:54,000
‫A function is defined using the D.F. keyword from define, followed by the name of the function, a

22
00:01:54,000 --> 00:02:02,910
‫pair of parentheses and then a colon after the colon on the next row will have the block of code of

23
00:02:02,910 --> 00:02:03,720
‫this function.

24
00:02:04,230 --> 00:02:08,220
‫The block of code must be indented one level to the right.

25
00:02:08,820 --> 00:02:10,770
‫This isn't something new for you.

26
00:02:11,250 --> 00:02:15,950
‫It's exactly what we did with for or while St..

27
00:02:15,960 --> 00:02:23,010
‫Mike's X recommended to use the lowercase letters and does nakase for the function name.

28
00:02:23,250 --> 00:02:27,110
‫And that means two separate words using underscores.

29
00:02:28,050 --> 00:02:35,490
‫The first part of a function body is recommended to be a string, which is a string enclosed in people

30
00:02:35,490 --> 00:02:41,610
‫quotes, and it explains what the function does and not how it does.

31
00:02:41,970 --> 00:02:46,410
‫The string basically creates the help section of the function.

32
00:02:46,710 --> 00:02:51,720
‫Doch string is not mandatory but is recommended after that string.

33
00:02:51,870 --> 00:02:54,750
‫We have whatever code we want to execute.

34
00:02:55,730 --> 00:03:04,310
‫As I said earlier, functions are usable blocks of code after defining a function, we can call that

35
00:03:04,310 --> 00:03:12,290
‫function whenever we need to run the code inside it in order to call a function, you need to type in

36
00:03:12,620 --> 00:03:15,980
‫that function name followed by parentheses.

37
00:03:15,980 --> 00:03:17,120
‫And that's all.

38
00:03:17,900 --> 00:03:25,130
‫If we just defined the function and not call it the block of code inside, the function will never be

39
00:03:25,130 --> 00:03:25,940
‫executed.

40
00:03:26,570 --> 00:03:28,190
‫Let's see some examples.

41
00:03:30,290 --> 00:03:33,290
‫I'm going to spike charm the Python ID.

42
00:03:34,430 --> 00:03:41,090
‫Think about the keyboard, I am defining a new function called my underlying function.

43
00:03:41,330 --> 00:03:42,950
‫This is the snake case.

44
00:03:44,260 --> 00:03:52,900
‫Then I have Prentice's a colon and the function block of code follows, first I'll have a talk string

45
00:03:52,930 --> 00:03:54,330
‫or the function help.

46
00:03:54,700 --> 00:03:58,590
‫This is a string enclosed between people, quote.

47
00:04:00,160 --> 00:04:03,580
‫This is my first function, it brings out a string.

48
00:04:09,490 --> 00:04:17,200
‫And the function will have only one instruction, and to fix the print function, it will print out

49
00:04:17,200 --> 00:04:17,850
‫a string.

50
00:04:18,130 --> 00:04:19,750
‫Hello, Python World.

51
00:04:24,320 --> 00:04:31,670
‫I have defined the function and now I'll call it, I am writing the name of the function followed by

52
00:04:31,670 --> 00:04:38,040
‫parentheses, and that's all here on line number seven, I am calling the function.

53
00:04:38,300 --> 00:04:40,550
‫Now I'm going to run the script.

54
00:04:41,860 --> 00:04:51,030
‫We can see here how our function printed out hello, python world, if I don't call the function, this

55
00:04:51,100 --> 00:04:56,080
‫script does nothing and that means no instruction is executed.

56
00:04:59,650 --> 00:05:04,840
‫The function block of code is executed only when calling the function.

57
00:05:05,700 --> 00:05:09,750
‫Of course, I can call the function as many times as I want.

58
00:05:15,790 --> 00:05:24,310
‫The above function has a string, the help of the function we can access that help using the help builtin

59
00:05:24,310 --> 00:05:32,200
‫function or using the name of the function that and undertook or underline, underline, underline,

60
00:05:32,200 --> 00:05:33,840
‫underline leks it.

61
00:05:33,850 --> 00:05:38,560
‫So help my function here without parentheses.

62
00:05:38,560 --> 00:05:42,150
‫So I'll bessin only the name of the function.

63
00:05:42,640 --> 00:05:44,020
‫I am going to run the script.

64
00:05:45,410 --> 00:05:49,730
‫And this is the function help or the dogs think.

65
00:05:50,890 --> 00:05:59,500
‫Another possibility is preened and as an argument, I'll pass in the name of the function without parentheses,

66
00:05:59,830 --> 00:06:01,620
‫dot, dot, dot org.

67
00:06:02,810 --> 00:06:03,370
‫Thickset.

68
00:06:06,540 --> 00:06:08,660
‫And this is the dog's drink.

69
00:06:09,520 --> 00:06:16,920
‫Now, it should be noted that functions can take in parameters, they are written inside the parentheses.

70
00:06:17,530 --> 00:06:21,990
‫If there is more than one parameter, they are separated by commas.

71
00:06:23,190 --> 00:06:32,640
‫Let's modify our function to take a parameter so I have a parameter called the name, and here in the

72
00:06:32,640 --> 00:06:36,440
‫print function I'm going to concatenate some strings.

73
00:06:37,230 --> 00:06:43,980
‫So plus in between single clicks, my name is plus name.

74
00:06:44,400 --> 00:06:46,830
‫Name is the function parameter.

75
00:06:47,860 --> 00:06:54,640
‫Whenever we are going to call the function, we must pass an argument to the function and that argument

76
00:06:54,790 --> 00:07:01,900
‫will be for the best to the code inside the function, if I call the function without an argument,

77
00:07:01,930 --> 00:07:03,000
‫I'll get an error.

78
00:07:03,670 --> 00:07:07,700
‫So I am executing the script and I've got them there.

79
00:07:07,780 --> 00:07:14,860
‫A type error, my function missing one required positional argument, and the name is name.

80
00:07:15,340 --> 00:07:18,760
‫So here I must base in a string.

81
00:07:19,620 --> 00:07:24,590
‫And now I can execute the script without getting an error.

82
00:07:25,990 --> 00:07:28,800
‫The function pointed out, hello, Python.

83
00:07:29,350 --> 00:07:30,670
‫My name is Andre.

84
00:07:32,750 --> 00:07:41,300
‫Let's take a look at two terms used in this context, arguments and parameters in Python, many people

85
00:07:41,300 --> 00:07:45,470
‫use them interchangeable and maybe there is no such a big deal.

86
00:07:45,710 --> 00:07:49,780
‫But you should know that there is a slight difference between them.

87
00:07:49,790 --> 00:07:58,430
‫Two, when we create a function like, say, my function of a comma, B, in this context, A and B

88
00:07:58,430 --> 00:08:01,220
‫are called functions parameters.

89
00:08:01,520 --> 00:08:04,250
‫They are variables local to the function.

90
00:08:05,180 --> 00:08:14,690
‫When we call the function like, say, X equals five and Y equals a string, and we are basing in X

91
00:08:14,690 --> 00:08:21,080
‫and Y to the function, they are called arguments, functions, arguments.

92
00:08:22,220 --> 00:08:27,500
‫So A and B, our parameters and X and Y are arguments.

93
00:08:28,700 --> 00:08:33,020
‫This is just semantics, and it's OK if you mix up these terms.

94
00:08:33,320 --> 00:08:36,620
‫I just wanted to make this distinction clear.

95
00:08:38,070 --> 00:08:46,530
‫Now back to our example, if the function has more arguments, the order we use them when basing the

96
00:08:46,530 --> 00:08:48,450
‫arguments is important.

97
00:08:48,840 --> 00:08:54,420
‫Lexia an example, I'm going to create a new function, lix, a function one.

98
00:08:54,900 --> 00:08:57,900
‫This function has two parameters.

99
00:08:58,830 --> 00:09:00,390
‫X, comma, Y.

100
00:09:02,360 --> 00:09:08,510
‫And here inside the function, I'm just going to print the value of each parameter.

101
00:09:09,690 --> 00:09:20,160
‫So print F from F string literal in here, first argument, colonics between curly braces.

102
00:09:23,220 --> 00:09:25,890
‫And print second argument, why?

103
00:09:31,390 --> 00:09:36,850
‫And I'm going to call the function so function off like, say, Python.

104
00:09:37,240 --> 00:09:42,130
‫This is the first argument and fifty five, the second argument.

105
00:09:43,110 --> 00:09:44,250
‫And I ran the script.

106
00:09:46,030 --> 00:09:55,030
‫We can see first argument by then second argument, fifty five, the first parameter X was mapped to

107
00:09:55,030 --> 00:10:00,760
‫Python because that's the first argument and Y was mapped to fifty five.

108
00:10:01,570 --> 00:10:10,570
‫If I use fifty five for the first argument, that value will be mapped to X and Python two I.

109
00:10:14,120 --> 00:10:18,620
‫OK, first argument, fifty five, second argument, Python.


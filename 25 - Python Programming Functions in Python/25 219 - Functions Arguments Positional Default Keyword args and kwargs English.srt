﻿1
00:00:00,690 --> 00:00:07,890
‫In this lecture, we'll dive deeper into Function's arguments in the last lecture, we've seen a type

2
00:00:07,890 --> 00:00:13,440
‫of functions argument and the vexed depositional argument, in fact, in Python.

3
00:00:13,470 --> 00:00:16,080
‫There are five types of function arguments.

4
00:00:16,350 --> 00:00:26,790
‫These are positional arguments, default arguments, key arguments, star arcs and star star KW arcs

5
00:00:26,790 --> 00:00:28,110
‫or quarks.

6
00:00:29,090 --> 00:00:31,070
‫Let's take them one by one.

7
00:00:31,890 --> 00:00:40,290
‫The most common way of assigning arguments to parameters is via the order in which they are best or

8
00:00:40,320 --> 00:00:41,280
‫their position.

9
00:00:42,290 --> 00:00:50,510
‫Just a quick example, if you define a function like, say, my function one with two parameters, A

10
00:00:50,540 --> 00:00:57,500
‫and B, when we call my function using five and ten is the arguments.

11
00:00:57,590 --> 00:01:05,300
‫The first argument, five will correspond to the first parameter A and the second argument then will

12
00:01:05,300 --> 00:01:14,120
‫correspond to the second parameter B inside the function A equals five and B equals then.

13
00:01:15,600 --> 00:01:16,730
‫Let's run the script.

14
00:01:17,890 --> 00:01:21,670
‫We can see how A is five and B is 10.

15
00:01:22,670 --> 00:01:30,320
‫If we call my function and we pass in 10 and five, the same thing happens just to that.

16
00:01:30,320 --> 00:01:37,730
‫In this case, inside the function A, which is the first parameter equals ten, because then it's the

17
00:01:37,730 --> 00:01:42,620
‫first argument and B equals five being the second argument.

18
00:01:44,050 --> 00:01:52,930
‫This is what positional arguments mean, it basically looks at the position of the argument and assigns

19
00:01:52,930 --> 00:02:01,330
‫it to the same parameter at the same position, if we call the function with a different number of arguments,

20
00:02:01,630 --> 00:02:03,340
‫will get a type error.

21
00:02:04,350 --> 00:02:08,550
‫For example, I'll call my function with only one argument.

22
00:02:12,560 --> 00:02:19,880
‫The same thing happens if I call my function with like, say, three arguments, I'll get the same error.

23
00:02:23,810 --> 00:02:31,910
‫Now, let's take a look at another type of argument, the default argument, oppositional argument can

24
00:02:31,910 --> 00:02:38,960
‫be made optional by specifying a default value for the corresponding parameter when defining the function.

25
00:02:40,030 --> 00:02:47,620
‫Let's take a look at an example, we'll modify this function and will make the second argument optional

26
00:02:47,620 --> 00:02:51,250
‫by giving a default value for the corresponding parameter.

27
00:02:51,970 --> 00:02:55,450
‫So here, I'll write B equals 20.

28
00:02:57,460 --> 00:03:05,380
‫How do we call this function, we call it in the usual way with two arguments, or we can all meet the

29
00:03:05,380 --> 00:03:07,350
‫second argument if we want.

30
00:03:07,720 --> 00:03:15,490
‫In that case, it doesn't throw an exception, but the second parameter will have the default value,

31
00:03:15,490 --> 00:03:16,750
‫which is 20.

32
00:03:17,690 --> 00:03:25,970
‫If I ran the script, I'll get the same result, but if I meet the second argument, which is five,

33
00:03:26,120 --> 00:03:33,290
‫it won't throw an exception because by default, the second argument is 20.

34
00:03:35,720 --> 00:03:45,020
‫We can see how A, Esten and B, E20, now let's consider a case where we have a function with more

35
00:03:45,020 --> 00:03:48,920
‫than two arguments and we want to make one of them optional.

36
00:03:49,100 --> 00:03:52,300
‫And that optional argument is not the last one.

37
00:03:52,580 --> 00:03:58,520
‫Let's consider this function with three parameters, A, B and C.

38
00:03:59,750 --> 00:04:09,530
‫And B is by default, then we specify A default value for B, but not for C, how can we call this function

39
00:04:09,560 --> 00:04:18,010
‫without passing in the second argument, something like my function, one of five Cormie 30.

40
00:04:18,680 --> 00:04:23,270
‫How could it assign these values to the function parameter?

41
00:04:24,530 --> 00:04:33,950
‫Eight equals five, that's clear, but 30 will be assigned to B or C in this case, Python doesn't know

42
00:04:33,950 --> 00:04:36,290
‫and there will be a syntax error.

43
00:04:41,030 --> 00:04:48,920
‫The goal here is that if we define a positional parameter with a default value, then every positional

44
00:04:48,920 --> 00:04:52,910
‫parameter after it must also have a default value.

45
00:04:53,450 --> 00:05:00,890
‫So if we give a default value to be, then we must also specify a default value for AC.

46
00:05:02,410 --> 00:05:04,380
‫C equals nine.

47
00:05:05,540 --> 00:05:14,660
‫Now, there will be no error, A equals five, B equals 30 and C equals nine.

48
00:05:15,220 --> 00:05:17,590
‫Let's point out the value of C.

49
00:05:19,530 --> 00:05:20,550
‫Around the script.

50
00:05:24,360 --> 00:05:32,310
‫Of course, I can admit meet the second argument in this case, A, is five BSN, the default value

51
00:05:32,310 --> 00:05:35,250
‫NC is nine X default value.

52
00:05:36,600 --> 00:05:40,380
‫But I am also free to type in all arguments.

53
00:05:43,890 --> 00:05:51,030
‫Now, let's see what happens if we want to specify the first and the third argument, but omit the second

54
00:05:51,030 --> 00:05:59,100
‫argument in our example, we want to specify values for A and C, but let B to take its default value

55
00:05:59,700 --> 00:06:00,390
‫for that.

56
00:06:00,600 --> 00:06:04,890
‫They are another type of argument called keyword argument.

57
00:06:05,550 --> 00:06:14,640
‫Keyword arguments also called named arguments basically allow us to ignore the order in which we entered

58
00:06:14,640 --> 00:06:21,120
‫the parameter in the function definition or even to keep some of them when calling the function.

59
00:06:22,120 --> 00:06:30,070
‫Using keyword arguments means you can specify the name of each parameter and assign it a corresponding

60
00:06:30,070 --> 00:06:32,710
‫value during the function called.

61
00:06:34,210 --> 00:06:42,340
‫I'll erase the default value of the functions, parameters, and when calling the function I write is

62
00:06:42,340 --> 00:06:50,040
‫arguments, B equals five, A equals six and C equals seven.

63
00:06:50,980 --> 00:06:52,030
‫Let's run the script.

64
00:06:53,270 --> 00:06:58,880
‫We can see how A is six, B, E five and C is seven.

65
00:06:59,360 --> 00:07:06,890
‫These are keyword arguments and their position when calling the function doesn't matter any more.

66
00:07:07,750 --> 00:07:15,400
‫A function with keyword arguments is defined in the same way is a function with positional arguments.

67
00:07:15,650 --> 00:07:18,320
‫There is no difference when defining the function.

68
00:07:18,670 --> 00:07:21,910
‫The difference is only when calling the function.

69
00:07:22,810 --> 00:07:30,280
‫Python also allows us to mix positional and keyword arguments, but there is a rule when doing this

70
00:07:30,280 --> 00:07:36,780
‫and the text to first specify the positional arguments and then the keyword arguments.

71
00:07:37,120 --> 00:07:39,730
‫So we use a keyword argument.

72
00:07:40,000 --> 00:07:43,990
‫All arguments thereafter must be accurate, too.

73
00:07:44,650 --> 00:07:49,150
‫If we don't follow, the sole python will return a syntax.

74
00:07:49,150 --> 00:07:55,030
‫Sarah, let's try to call the function in this way my function.

75
00:07:55,030 --> 00:08:00,040
‫One of six C equals one because nine.

76
00:08:04,450 --> 00:08:05,330
‫This is OK.

77
00:08:05,440 --> 00:08:16,450
‫There is no error, but if I write a equals six, then one and C equals nine, I'll get an error.

78
00:08:17,500 --> 00:08:26,620
‫This because a positional argument is not allowed to follow a court argument, so this positional argument,

79
00:08:26,620 --> 00:08:32,680
‫which is one, is not allowed to be after a keyword argument, which is a.

80
00:08:33,700 --> 00:08:41,890
‫So if I use a keyword argument for the first argument of the function, all arguments that follow must

81
00:08:41,890 --> 00:08:42,820
‫be quiet.

82
00:08:43,480 --> 00:08:45,880
‫So here I must right.

83
00:08:45,880 --> 00:08:49,480
‫B equals one and there is no error.

84
00:08:51,820 --> 00:08:59,530
‫Let's see another example, the first argument is six B equals one and nine.

85
00:09:00,040 --> 00:09:01,710
‫This is also an error.

86
00:09:02,230 --> 00:09:09,790
‫If the second argument is a keyword argument, all arguments that follow must be of type.

87
00:09:09,790 --> 00:09:14,080
‫Kiet I must try to see equals nine and Viks, ok.

88
00:09:15,970 --> 00:09:22,330
‫But what if you don't know how many parameters you are going to need in a function and you want to specify

89
00:09:22,330 --> 00:09:26,390
‫a variable number of parameters inside a function definition?

90
00:09:26,590 --> 00:09:34,420
‫For example, we define a function that calculates the sum of two, three, four or any numbers.

91
00:09:34,430 --> 00:09:40,750
‫In this case, we don't know exactly how many arguments will be passed in when calling the function.

92
00:09:41,580 --> 00:09:50,340
‫The Python solution for this is Stathakis, where Arkes is, in fact, a couple of potential additional

93
00:09:50,340 --> 00:09:51,030
‫arguments.

94
00:09:51,450 --> 00:09:58,500
‫This table is initial empty and this means we don't get an error if we don't specify any additional

95
00:09:58,500 --> 00:10:02,160
‫argument when calling the function lexia an example.

96
00:10:04,470 --> 00:10:13,230
‫I am going to define a new function, D.F., if one the first parameter is a in the second parameter

97
00:10:13,230 --> 00:10:14,940
‫is star arcs.

98
00:10:17,450 --> 00:10:21,680
‫You can use any name, but it's recommended to use ARC's.

99
00:10:23,460 --> 00:10:29,280
‫In the function body, I use the brain function to print the value of the arguments.

100
00:10:30,400 --> 00:10:33,640
‫A colon and a between curly braces.

101
00:10:33,970 --> 00:10:39,970
‫This is a F string literal and print f ARC's.

102
00:10:40,330 --> 00:10:42,070
‫Colon ARC's.

103
00:10:43,410 --> 00:10:52,770
‫And I'll also calculate the sum I'll create a temporary variable called S equals a plus some of Arkes

104
00:10:53,040 --> 00:10:58,800
‫I am using the sum built in function because ARC's is in fact, a tuple.

105
00:11:02,310 --> 00:11:07,230
‫And print some colon is between curly braces.

106
00:11:08,840 --> 00:11:17,840
‫I can run this function with a variable number of arguments, let's try it if one of five I am calling

107
00:11:17,840 --> 00:11:20,360
‫the function with only one argument.

108
00:11:22,220 --> 00:11:30,440
‫We can see how a is five, this is the first argument, and the Topal, which is Stathakis, is empty,

109
00:11:31,370 --> 00:11:35,840
‫but if I want, I can pass in two arguments five.

110
00:11:36,110 --> 00:11:36,830
‫KomaI four.

111
00:11:40,550 --> 00:11:47,210
‫A means five, Antarctic's is a table with only one element in the sum is nine.

112
00:11:47,660 --> 00:11:51,920
‫If I want to put in more arguments, there is no problem.

113
00:11:52,130 --> 00:11:54,110
‫It will work like a charm.

114
00:11:57,920 --> 00:12:06,890
‫We see how a is five and Arkes is a Tupolev that contains is elements of the arguments that follow after

115
00:12:06,890 --> 00:12:11,480
‫the first argument, which is five and the sum is thirty eight.

116
00:12:13,810 --> 00:12:24,460
‫The last type of argument is star star KW, ARC's or Quark's, if you need a variable number of parameters,

117
00:12:24,460 --> 00:12:30,010
‫but you want to use keyword arguments instead of positional arguments, when calling the function,

118
00:12:30,220 --> 00:12:36,430
‫you can use a double asterisk, followed by KW arcs instead of arcs.

119
00:12:37,750 --> 00:12:45,970
‫Quark's, thanks for keyword arguments, and it builds a dictionary of key value pairs, Lexia, an

120
00:12:45,970 --> 00:12:58,780
‫example, I'll create a new function D.F., F2 and this function has one parameter star star k w ARC's.

121
00:13:00,050 --> 00:13:06,110
‫The first line of the function body will point out the value of Quark's.

122
00:13:09,110 --> 00:13:18,290
‫Quarks being a dictionary, I can test if a key exists and if the key exists inside the dictionary,

123
00:13:18,470 --> 00:13:19,980
‫I can print Hicksville.

124
00:13:20,390 --> 00:13:25,850
‫So let's say if name, this is a dictionary key in quarks.

125
00:13:28,420 --> 00:13:39,280
‫Then print your name is in between curly braces, Quark's of in between double quotes, I've already

126
00:13:39,280 --> 00:13:44,470
‫used single quotes for the F string literal I write name.

127
00:13:46,270 --> 00:13:58,900
‫And I'll call the function this way if two off and now keyword arguments name equals John X equals 40

128
00:13:59,260 --> 00:14:02,200
‫and location equals London.

129
00:14:03,520 --> 00:14:08,050
‫Now, I'll run the script to see what's inside the Quark's argument.

130
00:14:10,400 --> 00:14:14,640
‫And this is the output of my effort to function.

131
00:14:14,900 --> 00:14:18,350
‫We can see how it created a dictionary.

132
00:14:18,950 --> 00:14:26,210
‫The keys are the name of the arguments and their values are their associated values.

133
00:14:26,360 --> 00:14:35,480
‫So the value that corresponds to the key called the name is John 40, corresponds to age and London

134
00:14:35,690 --> 00:14:36,410
‫location.

135
00:14:37,450 --> 00:14:45,100
‫After that, because the name is a key in this dictionary, it printed out your names and the value

136
00:14:45,100 --> 00:14:48,700
‫associated with the name Key, which is John.

137
00:14:49,540 --> 00:14:56,890
‫This type of argument is often used when working with different external modules or libraries.

138
00:14:57,670 --> 00:15:03,550
‫Also, remember that the names ARC's and Quarks are just conventions.

139
00:15:03,850 --> 00:15:11,920
‫You can use any name instead as long as you use the asterisk for positional arguments and double asterisk

140
00:15:11,920 --> 00:15:13,570
‫for keyword arguments.

141
00:15:14,050 --> 00:15:16,720
‫These asterisks are mandatory.

142
00:15:17,530 --> 00:15:23,770
‫If I want to write X instead of quarks, there is no problem.

143
00:15:23,920 --> 00:15:25,240
‫I'll get no error.

144
00:15:28,770 --> 00:15:30,960
‫Anyway, this is not recommended.


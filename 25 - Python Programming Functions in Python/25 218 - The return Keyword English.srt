﻿1
00:00:01,310 --> 00:00:05,180
‫Now let's take a look at a very important topic:

2
00:00:05,300 --> 00:00:07,020
‫the return keyword.

3
00:00:07,160 --> 00:00:13,680
‫We use the return keyword to send back the result of the function instead of printing it out.

4
00:00:13,700 --> 00:00:19,420
‫Return allows us to assign the output of the function to a new variable.

5
00:00:19,550 --> 00:00:24,170
‫In fact we've already used many times the return statement.

6
00:00:24,350 --> 00:00:31,870
‫For example let's create the list with three items L1 equals [1,23].

7
00:00:31,880 --> 00:00:42,830
‫This is a list. And then length equals len(11) . The len function that I've just called doesn't print a

8
00:00:42,830 --> 00:00:50,510
‫value but returns the value which is then captured in the length variable.

9
00:00:50,650 --> 00:00:54,530
‫This is just an example of a function that returns a value.

10
00:00:55,370 --> 00:00:57,370
‫Let's see another example.

11
00:00:57,380 --> 00:01:01,170
‫I'm going to create a function called sum1.

12
00:01:01,180 --> 00:01:09,970
‫This function takes two parameters A and B and inside the function I'm going to create a new variable

13
00:01:10,030 --> 00:01:14,950
‫a temporary variable called Sum equals A plus B.

14
00:01:14,950 --> 00:01:22,260
‫Of course this function calculates the sum of two numbers and I am going to print the sum, so print (f' Sum: {sum }')

15
00:01:22,450 --> 00:01:31,390
‫this is my function and I am going to call the function

16
00:01:32,320 --> 00:01:43,010
‫I'll pass in to arguments, let's say, (3,6) When I run the script it will display

17
00:01:43,100 --> 00:01:44,310
‫sum (9)

18
00:01:44,420 --> 00:01:49,520
‫Now let's create another function that returns the sum of two numbers.

19
00:01:49,910 --> 00:02:02,050
‫So Def some 2 A come out b some equals A plus B and using the return keyword I'll return the sum .

20
00:02:02,050 --> 00:02:10,950
‫If I run the function with two arguments the function won't display any message but it will return a value

21
00:02:11,790 --> 00:02:18,870
‫if I run the script it seems that the second function doesn't do a thing ; that's because the return

22
00:02:18,870 --> 00:02:26,910
‫value is somehow lost. If I don't want to lose the return value I must capture that value in a variable

23
00:02:27,180 --> 00:02:34,450
‫let's say x equals and my function and then I can print that value

24
00:02:37,390 --> 00:02:46,710
‫I'll run the script again; we can see how the second function returned a value, that value has been

25
00:02:46,710 --> 00:02:56,710
‫captured in a new variable called S and on line number 12 I've printed out the sum; That's the difference

26
00:02:56,740 --> 00:03:06,270
‫between a function that prints out a message and a function that returns a value;  a function that doesn't

27
00:03:06,270 --> 00:03:15,960
‫return explicitly, returns implicitly none . This means that the first function called sum 1 returns

28
00:03:16,080 --> 00:03:28,070
‫implicitly none. So if I write here let's say return value equals sum1(3,6) and after

29
00:03:28,150 --> 00:03:30,710
‫print  return value

30
00:03:34,000 --> 00:03:38,010
‫We can see how the return value was none.

31
00:03:38,080 --> 00:03:46,750
‫So any function returns something: the value of the return statement or none if it doesn't return explicitly.

32
00:03:47,350 --> 00:03:56,710
‫You should also know that the return statement excits a function , the return statement is the last statement

33
00:03:56,800 --> 00:04:04,300
‫executed when calling the function;  if there is any other instruction after the return statement it

34
00:04:04,300 --> 00:04:07,350
‫will never be executed.  Let's see an example

35
00:04:12,200 --> 00:04:22,520
‫after the return the statement I'm going to print a message, let's say  message after the return

36
00:04:22,520 --> 00:04:25,940
‫and another message before the return statement

37
00:04:34,220 --> 00:04:42,800
‫Let's run the script! We can see how it printed out message before the return statement but it didn't

38
00:04:42,930 --> 00:04:46,490
‫print out message after the return statement.

39
00:04:46,580 --> 00:04:50,100
‫That's because the return statement exits

40
00:04:50,120 --> 00:04:51,330
‫he function.

41
00:04:51,560 --> 00:05:00,660
‫This is the last instruction of my function; the instructions after the return will never be executed.

42
00:05:01,890 --> 00:05:07,050
‫You should also note that the function can return more values.

43
00:05:07,050 --> 00:05:16,300
‫In that case it returns a tuple.  I'll modify the function to return both the sum of a and b

44
00:05:16,720 --> 00:05:23,610
‫and the product of a multiplied by B; So sum and product

45
00:05:26,220 --> 00:05:37,390
‫product equals a multiplied by B return sum comma product. If the function returns to values

46
00:05:37,390 --> 00:05:41,880
‫I must capture both values

47
00:05:41,910 --> 00:05:47,620
‫So here let's say s comma P , and the name of the function tis sum and product.

48
00:05:53,980 --> 00:05:56,200
‫Now I'll execute the script.

49
00:05:58,550 --> 00:06:02,010
‫The script printed out sum is 6.

50
00:06:02,120 --> 00:06:03,820
‫Product is 5.

51
00:06:05,420 --> 00:06:07,290
‫Here inside the function

52
00:06:07,340 --> 00:06:14,260
‫it's not necessarily to calculate the result of these operations in a variable

53
00:06:14,390 --> 00:06:17,170
‫and after that return that variable.

54
00:06:17,300 --> 00:06:19,790
‫We can return it directly.

55
00:06:19,820 --> 00:06:24,680
‫I'm going to comment out this code and I'll simply write return.

56
00:06:24,830 --> 00:06:30,320
‫A plus B comma A multiplied by B X the same.


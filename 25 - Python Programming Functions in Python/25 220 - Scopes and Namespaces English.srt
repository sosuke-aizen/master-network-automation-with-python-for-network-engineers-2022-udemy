﻿1
00:00:00,450 --> 00:00:07,710
‫In this lecture, we'll take a deep look at Scope's and name species in Python, unnamed space, as

2
00:00:07,890 --> 00:00:14,490
‫the name implies, is a space holding some names and by names I'm referring to variables, functions

3
00:00:14,490 --> 00:00:16,020
‫or classes we define.

4
00:00:16,800 --> 00:00:23,640
‫We can imagine unnamed space is a container or even a table that contains the names we define.

5
00:00:24,180 --> 00:00:30,420
‫This way we can have the same name or a variable defined in different name spaces.

6
00:00:31,640 --> 00:00:40,070
‫A name or variable can exist only in a specific part of our code, the portion of code where the name

7
00:00:40,070 --> 00:00:48,680
‫exists is called the scope of that name or variable into the binding between the variable in the object

8
00:00:48,680 --> 00:00:51,650
‫or value is stored in a namespace.

9
00:00:52,400 --> 00:00:55,100
‫Each scope has its own namespace.

10
00:00:55,340 --> 00:01:00,690
‫So Scope's and the name spaces go hand in hand in Python.

11
00:01:00,950 --> 00:01:04,460
‫There are three types of name spaces or scope's.

12
00:01:05,420 --> 00:01:10,030
‫The built in namespace contains python builtin functions.

13
00:01:10,400 --> 00:01:14,720
‫We've used a lot of built-In functions till this point.

14
00:01:15,080 --> 00:01:20,540
‫Print Lenn sorted range marks are all built in functions.

15
00:01:20,690 --> 00:01:24,440
‫They are available to us whenever we want to use them.

16
00:01:25,610 --> 00:01:34,370
‫The global order module namespace contains all variables, functions and classes we define in our scripts,

17
00:01:35,180 --> 00:01:38,060
‫and then there is the local namespace.

18
00:01:38,330 --> 00:01:43,310
‫It contains only names defined inside our own functions.

19
00:01:44,900 --> 00:01:49,440
‫Let's see examples here in this script.

20
00:01:49,460 --> 00:01:58,130
‫I've used the built in namespace, in fact, I've used three built-In functions, Topal range in print,

21
00:01:59,210 --> 00:02:04,130
‫and I've also created a global scope, the variable called Daewon.

22
00:02:08,220 --> 00:02:17,220
‫The builtin names are available across all files or modules or for a Python application, and the global

23
00:02:17,220 --> 00:02:25,680
‫names can be used anywhere inside our module, including inside any function, but not outside the current

24
00:02:25,680 --> 00:02:28,440
‫file where they have been defined.

25
00:02:30,080 --> 00:02:33,820
‫Now, if I try print X, I'll get an error.

26
00:02:36,620 --> 00:02:46,790
‫A name er Python will look for X in the global automotive name space, it doesn't find it and then it

27
00:02:46,790 --> 00:02:54,530
‫will look for it in the built in namespace where it cannot find the event and returns a runtime error.

28
00:02:56,380 --> 00:02:59,800
‫But if I do, X equals then.

29
00:03:00,720 --> 00:03:04,410
‫And they ran the script one more time, I won't get an error.

30
00:03:05,890 --> 00:03:13,930
‫The script brings the value of X without their X because the name X has been created in the global namespace.

31
00:03:14,710 --> 00:03:17,170
‫So X is a global variable.

32
00:03:17,860 --> 00:03:23,980
‫We can use it anywhere inside this script, but not in other scripts.

33
00:03:26,160 --> 00:03:31,650
‫Lexy, another example, I'll define a function, my func.

34
00:03:33,670 --> 00:03:38,110
‫And inside the function, I am printing the value of X.

35
00:03:42,820 --> 00:03:44,770
‫Now I am calling the function.

36
00:03:50,440 --> 00:03:59,140
‫The script printed X inside the function, calling them when calling the function, the Python interpreter

37
00:03:59,140 --> 00:04:01,840
‫is looking for a local variable called X.

38
00:04:02,260 --> 00:04:06,270
‫A local variable means defined inside the function.

39
00:04:06,610 --> 00:04:15,100
‫If it doesn't find it, and that's the case here, it will search for X in the global scope there it

40
00:04:15,100 --> 00:04:20,460
‫finds the name and uses it to print X value inside of the function.

41
00:04:21,220 --> 00:04:26,290
‫If there is no X variable defined, the needs are in the local namespace.

42
00:04:26,290 --> 00:04:31,210
‫Some inside the function, not global nor in the building namespace.

43
00:04:31,210 --> 00:04:32,770
‫It will return an error.

44
00:04:34,910 --> 00:04:38,180
‫We can see how it returned a name air.

45
00:04:40,680 --> 00:04:49,140
‫We can see from this example how scopes are misted, the local scope is nested inside the global scope,

46
00:04:49,350 --> 00:04:52,980
‫which is nested inside the builtin scope.

47
00:04:53,430 --> 00:05:00,870
‫If you reference a variable name inside the scope and Python does not find it in that Scope's namespace,

48
00:05:01,050 --> 00:05:05,610
‫it will look for it in an inclosing scope's namespace.

49
00:05:06,090 --> 00:05:13,800
‫It searches up the chain of all inclosing, scop name spaces, lexia mouther example.

50
00:05:14,890 --> 00:05:24,160
‫Inside the function I write, X equals five, when we create functions, we can create variable names

51
00:05:24,160 --> 00:05:29,680
‫inside those functions, using assignment's variable as defined inside.

52
00:05:29,680 --> 00:05:33,340
‫The function are not created until the function is called.

53
00:05:33,700 --> 00:05:39,250
‫Every time the function is called, a new scope is created in variables defined inside.

54
00:05:39,250 --> 00:05:41,800
‫The function are assigned to that scope.

55
00:05:42,130 --> 00:05:46,420
‫When the function finishes draining, the scope is gone to.

56
00:05:47,760 --> 00:05:55,500
‫Now, in this example, inside the function, a new local variable called X is created when calling

57
00:05:55,500 --> 00:06:02,720
‫the function, it finds a variable called X in the functions local namespace and uses it.

58
00:06:02,730 --> 00:06:11,170
‫That variable exists only inside the function or in the function, local namespace, outside the function.

59
00:06:11,220 --> 00:06:15,180
‫There is another X variable that stores another value.

60
00:06:16,280 --> 00:06:20,420
‫Here are the right print X outside the function.

61
00:06:24,000 --> 00:06:25,380
‫And I learned the script.

62
00:06:27,450 --> 00:06:36,000
‫We can see how it works inside the function stauss value five and six, outside the function stauss

63
00:06:36,000 --> 00:06:36,420
‫value.

64
00:06:36,580 --> 00:06:43,120
‫Then this is a local variable inside the function and outside the function.

65
00:06:43,140 --> 00:06:46,290
‫I have another variable, a global one.

66
00:06:47,880 --> 00:06:56,370
‫Another example is when we want to use or to import a global name into a function namespace, for that,

67
00:06:56,400 --> 00:07:04,020
‫we use the global keyword for that name we want to use before any other statement that refers to the

68
00:07:04,020 --> 00:07:05,280
‫global variable.

69
00:07:07,080 --> 00:07:16,260
‫I'll create another global variable called a equals then and then new function, my func one in the

70
00:07:16,260 --> 00:07:18,210
‫function has one parameter.

71
00:07:19,220 --> 00:07:24,020
‫Let's say B and inside the function, I have global A.

72
00:07:24,910 --> 00:07:27,220
‫A plus equals B.

73
00:07:28,220 --> 00:07:32,510
‫And I'll print the value of a inside the function.

74
00:07:40,610 --> 00:07:50,300
‫Now I am going to call my function, so my function and I'll pass a value is argument like, say, seven

75
00:07:50,840 --> 00:07:59,540
‫after the function call, I want to print the value of a print, a outside the function.

76
00:08:04,160 --> 00:08:05,780
‫And I'll run the script.

77
00:08:10,560 --> 00:08:18,000
‫In this example, inside the function, we are importing and using the global a variable, and that

78
00:08:18,000 --> 00:08:25,800
‫means that the result of the operations done on a inside the function is available also outside the

79
00:08:25,800 --> 00:08:26,320
‫function.

80
00:08:27,150 --> 00:08:38,460
‫We can see how A equals 17 inside of the function and A also equals 17 outside the function of X because

81
00:08:38,460 --> 00:08:40,520
‫A is a global variable.

82
00:08:41,130 --> 00:08:48,750
‫So inside the function I am using the global variable python doesn't create a local one.

83
00:08:50,090 --> 00:09:01,430
‫Lexia, another example, D.F., my funk to end inside the function I write print X and X equals eight.

84
00:09:03,260 --> 00:09:07,310
‫And now I'll call the function, so my functor.

85
00:09:09,930 --> 00:09:15,450
‫When writing the script, I'll get an error Unbound local editor.

86
00:09:16,820 --> 00:09:26,900
‫Local variable X referenced before Assignment Y is that whenever a python sees an assignment operation

87
00:09:26,900 --> 00:09:35,300
‫inside a function, it interprets that variable is local and this happens at compile time.

88
00:09:35,600 --> 00:09:39,220
‫In our example, Python says, OK, here is an assignment.

89
00:09:39,320 --> 00:09:43,160
‫So I am going to create a local variable, Caltex.

90
00:09:44,280 --> 00:09:52,230
‫In this case, the local variable X that will be created, mosques or chateaux, the global variable

91
00:09:52,230 --> 00:09:53,760
‫X defined earlier.

92
00:09:55,390 --> 00:10:03,490
‫When wanting to paint the value of X, it returns an error because the global variable is masked and

93
00:10:03,490 --> 00:10:06,250
‫the local variable hasn't been created yet.

94
00:10:06,460 --> 00:10:10,100
‫In fact, we are using a variable before creating it.

95
00:10:10,840 --> 00:10:17,930
‫It's the same like writing print, like, say, C and then C equals six.

96
00:10:18,710 --> 00:10:26,560
‫Here I am using a variable called C before creating that variable and vaccinator.

97
00:10:29,370 --> 00:10:36,630
‫It's the conclusion if there are variables that are referenced inside the function but are not assigned

98
00:10:36,630 --> 00:10:44,460
‫a value anywhere in the function, that variables will not be local and Python will look for them in

99
00:10:44,460 --> 00:10:46,890
‫the enclosing scope at runtime.

100
00:10:51,970 --> 00:11:00,340
‫That's because there is no assignment inside the function and it will consider X a global variable,

101
00:11:00,340 --> 00:11:01,490
‫not a local one.

102
00:11:01,510 --> 00:11:07,810
‫And there is such a variable in the global scope here, X equals 10.

103
00:11:13,120 --> 00:11:19,840
‫In the last example from this lecture, I'll show you how a name can be mistakenly shadowed or masked,

104
00:11:20,740 --> 00:11:25,420
‫I am going to write print Lenn off and hear a string A.B.C..

105
00:11:25,940 --> 00:11:34,600
‫After that, I am going to define a new function called Lenn of X and inside the line function I write

106
00:11:34,600 --> 00:11:35,650
‫print X.

107
00:11:37,460 --> 00:11:41,540
‫Now, I'll call the land function and by passing a string.

108
00:11:42,590 --> 00:11:44,000
‫And I learned the script.

109
00:11:45,670 --> 00:11:49,210
‫We see how the builtin land function has been shattered.

110
00:11:50,280 --> 00:11:57,900
‫In fact, Python considered the land function defined here in the global namespace and not the land

111
00:11:57,900 --> 00:12:01,230
‫function defined in the built-In namespace.

112
00:12:02,590 --> 00:12:10,030
‫When I called the land function, it pointed out this thing based in his argument and the not to the

113
00:12:10,030 --> 00:12:13,300
‫number of characters of that string.

114
00:12:14,350 --> 00:12:20,350
‫This is a common error, so you should never use names that are already defined.

115
00:12:21,790 --> 00:12:28,240
‫What can I do now, I can delete the name that shadows the builtin name.

116
00:12:28,420 --> 00:12:36,130
‫So tell Lynn in this case I delete the Lenn function that has been defined in the script.

117
00:12:36,400 --> 00:12:41,600
‫After that, I can run the line function and it will return the correct value.

118
00:12:41,920 --> 00:12:46,120
‫So let's say print lenhoff and a random string.

119
00:12:49,610 --> 00:12:54,620
‫We can see how it returned the number of characters in this string.

120
00:12:55,250 --> 00:12:59,640
‫I have called the original the builtin Lynn function.

121
00:13:00,380 --> 00:13:02,510
‫All right, thanks for watching it.

122
00:13:02,660 --> 00:13:04,840
‫And I'll see you in the next video.


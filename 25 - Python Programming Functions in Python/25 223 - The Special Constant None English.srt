﻿1
00:00:01,100 --> 00:00:07,850
‫In this lecture, we'll take a look at a built built-In constant called the none none is likely to enforce,

2
00:00:07,850 --> 00:00:09,940
‫but it means something else.

3
00:00:09,950 --> 00:00:12,620
‫And it's not an object of the pool class.

4
00:00:13,040 --> 00:00:15,410
‫It's an object of the nanny type.

5
00:00:15,410 --> 00:00:19,670
‫And that there is only one instance of nanny type, which is none.

6
00:00:20,200 --> 00:00:27,830
‫Nannies frequently used to represent the absence of a value, the nothing mess and assignments to none

7
00:00:27,830 --> 00:00:30,290
‫are illegal and a syntax.

8
00:00:31,430 --> 00:00:37,940
‫If you have worked with other programming languages, none is Python version of nonorganic.

9
00:00:38,720 --> 00:00:40,760
‫Now let's see some examples.

10
00:00:41,330 --> 00:00:43,280
‫Boulle of non.

11
00:00:45,200 --> 00:00:50,660
‫Returns false, so the truthiness value of money is false.

12
00:00:52,070 --> 00:00:57,980
‫Keep in mind that none is always written with an uppercase letter and.

13
00:00:59,460 --> 00:01:03,120
‫Of any firefight, none equals false.

14
00:01:04,450 --> 00:01:05,320
‫I'll get the answer.

15
00:01:06,410 --> 00:01:08,750
‫I cannot assign a value to none.

16
00:01:10,430 --> 00:01:11,720
‫Type of non.

17
00:01:13,450 --> 00:01:15,730
‫And it returned Monotype.

18
00:01:17,170 --> 00:01:25,150
‫Money is frequently used when default arguments are not passed to a function lexia, an example, I

19
00:01:25,150 --> 00:01:32,050
‫create a function called F one that has one parameter and the parameter is by default, none.

20
00:01:32,500 --> 00:01:35,920
‫So this is a default argument with value?

21
00:01:36,130 --> 00:01:36,550
‫None.

22
00:01:37,830 --> 00:01:47,880
‫If A this means if a bull of A equals two, so if I call the function with an argument, then I'll print

23
00:01:47,880 --> 00:01:49,200
‫the value of a.

24
00:01:55,360 --> 00:01:55,900
‫Ls.

25
00:01:56,800 --> 00:02:00,670
‫Print function called Without Arguments.

26
00:02:05,640 --> 00:02:08,880
‫And I'll also print the value of a.

27
00:02:14,620 --> 00:02:22,840
‫OK, is my function now, I can call my function with an argument, let's say if one and I'll pass four

28
00:02:23,050 --> 00:02:24,640
‫is the function argument.

29
00:02:25,720 --> 00:02:32,260
‫And it printed a e for I can pass any argument here.

30
00:02:34,130 --> 00:02:43,370
‫But I can also call the function without any argument, if one without arguments and it didn't return

31
00:02:43,370 --> 00:02:47,660
‫an error, it simply executed the else block of code.

32
00:02:49,450 --> 00:02:56,530
‫So keep in mind that none is frequently used when positional arguments are not passed to a function.

33
00:02:57,990 --> 00:03:04,530
‫Of course, all about the man for the moment, but I'm sure you'll see many times in Python applications.


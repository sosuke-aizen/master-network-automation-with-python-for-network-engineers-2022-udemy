﻿1
00:00:00,680 --> 00:00:04,980
‫In the previous lectures we've seen how to define functions.

2
00:00:05,000 --> 00:00:12,980
‫We use the DEF keyword, the function name, an optional list of parameters enclosed by parentheses,s colon

3
00:00:13,340 --> 00:00:17,460
‫and then the function body indented by four spaces.

4
00:00:17,480 --> 00:00:25,840
‫Then there is the  return keyword that returns a value and exists the function. Lambda expressions are

5
00:00:25,840 --> 00:00:27,820
‫another way to create functions.

6
00:00:27,930 --> 00:00:33,940
‫They are called anonymous functions because they don't have a name like a function has and they

7
00:00:33,940 --> 00:00:37,040
‫are a single line of logical code.

8
00:00:37,090 --> 00:00:43,870
‫There is the lambda keyword which creates a lambda expression and we can think of the lambda keyword

9
00:00:44,110 --> 00:00:47,560
‫as of the DEF keyword from functions.

10
00:00:47,560 --> 00:00:56,200
‫Then there is an optional parameter list, the parameters are not enclosed by parentheses. After that comes

11
00:00:56,320 --> 00:01:03,820
‫a colon and this is mandatory and the valid expression that is evaluated and returned when the lumbar

12
00:01:03,820 --> 00:01:04,990
‫is called.

13
00:01:05,110 --> 00:01:12,850
‫You can think of  the expression as the body of the function;  lambda expressions can be assigned

14
00:01:12,880 --> 00:01:19,220
‫to variables or past as arguments to another function;  Let's see examples.

15
00:01:20,200 --> 00:01:32,110
‫First I'll create a normal, a classical function;let's say DE F square of x and the function returns X

16
00:01:32,170 --> 00:01:33,120
‫squared.

17
00:01:33,190 --> 00:01:38,010
‫So return x to the power of two.

18
00:01:38,250 --> 00:01:47,630
‫Now I create a lambda expression that is identical to this function so lambda x This is the parameter

19
00:01:48,320 --> 00:01:54,070
‫colon and now that the expression that  will be evaluated and returned.

20
00:01:54,410 --> 00:02:03,770
‫So X double star to;  this is the Lambda expression; the lambda expression is just like a function.

21
00:02:03,850 --> 00:02:13,600
‫In fact if we print the type of the expression we'll get a function so print type of and the expression

22
00:02:15,980 --> 00:02:19,440
‫we can see that X type is function.

23
00:02:19,640 --> 00:02:28,720
‫As I said earlier we can assign the value returned by the lambda expression to a variable; I'll write

24
00:02:28,800 --> 00:02:37,580
‫sq equals and the lambda expression; the lambda expression and the function are identical.

25
00:02:37,700 --> 00:02:41,140
‫How can we use the lambda expression?

26
00:02:41,260 --> 00:02:52,330
‫For example I can write print sq of 5; 5 will be assigned to the lambda parameter which is x

27
00:02:52,330 --> 00:03:03,820
‫When I run the script it will print twenty five so five squared; Let's see other examples: s equals lambda x comma

28
00:03:03,840 --> 00:03:16,110
‫y colon x plus y; these returns the sum of two numbers print s of five comma seven.

29
00:03:16,140 --> 00:03:26,630
‫When I run the script it will print twelve; five has been mapped to X and seven to y ; the body of a

30
00:03:26,630 --> 00:03:29,610
‫lambda is limited to a single expression.

31
00:03:29,690 --> 00:03:39,420
‫We cannot do assignments inside lambda expressions lambda x colon X equals seven.

32
00:03:39,420 --> 00:03:43,930
‫This is an error because assignements are not allowed.

33
00:03:44,190 --> 00:03:46,050
‫And here I have an assignment

34
00:03:48,780 --> 00:03:57,240
‫can't assign to Lambda; all rules we've seen at the functions arguments are applicable to lambdas

35
00:03:57,290 --> 00:03:57,920
‫too.

36
00:03:58,090 --> 00:04:05,440
‫We can have default arguments, keyword arguments, start arcs or star star quarks.

37
00:04:05,440 --> 00:04:12,050
‫I'll modify this lambda expression and I'll make X default to 1.

38
00:04:12,130 --> 00:04:18,250
‫In this case the parameter called X has a default value equal to 1.

39
00:04:18,400 --> 00:04:26,550
‫I can call the lambda expression like this:sq and than an argument it will return 1.

40
00:04:26,770 --> 00:04:28,120
‫So 1 squared

41
00:04:31,210 --> 00:04:33,660
‫OK,  I must comment out this line

42
00:04:38,360 --> 00:04:46,910
‫Another common use case for lambdas is when we pass a function as an argument to another function.

43
00:04:46,910 --> 00:04:49,230
‫Let's see an example

44
00:04:49,250 --> 00:04:58,810
‫def my_function x and func; the second parameter is also a function.

45
00:04:58,820 --> 00:05:07,470
‫Here I return func of X; let's see how I can call the function.

46
00:05:07,490 --> 00:05:12,620
‫y this is the return value equals my function of let's say 5.

47
00:05:13,350 --> 00:05:17,840
‫And now for the second parameter I'll create a lambda expression.

48
00:05:17,860 --> 00:05:32,890
‫So lambda x colon X star start 2 so x squared; it will return 5 squared; if I print why it will print

49
00:05:33,200 --> 00:05:41,930
‫25; this is an example of a function that takes another function as an argument.


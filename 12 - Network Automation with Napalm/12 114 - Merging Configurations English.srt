﻿1
00:00:00,520 --> 00:00:05,410
‫Napalm offers two ways in dealing with device configuration.

2
00:00:05,620 --> 00:00:12,970
‫It is possible to either replace the old configuration or merge with the existing configuration.

3
00:00:13,360 --> 00:00:21,070
‫In the last lecture, I've shown you how to replace the old configuration of our outer solar running

4
00:00:21,070 --> 00:00:25,780
‫config using the load replace candidate method.

5
00:00:26,140 --> 00:00:31,620
‫Now I'm going to show you how to merge with the existing configuration.

6
00:00:32,080 --> 00:00:36,230
‫We are going to use the load the March candidate method.

7
00:00:36,370 --> 00:00:38,310
‫In fact, it's very simple.

8
00:00:38,650 --> 00:00:45,250
‫We have one out there that already has a configuration and we want to merge the current router configuration

9
00:00:45,250 --> 00:00:50,370
‫with another configuration from within a file in this example.

10
00:00:50,410 --> 00:00:57,940
‫I have this text file and here I create an extended ACL.

11
00:00:58,240 --> 00:01:09,550
‫So on extended access list, this access list denies or drops any telnet packet that is coming to our

12
00:01:09,550 --> 00:01:12,850
‫right with our first Ethernet to zero interface.

13
00:01:13,450 --> 00:01:15,910
‫All of our packets are allowed.

14
00:01:16,360 --> 00:01:25,300
‫I'm going to add this configuration to that out there by calling the load marked candidate method for

15
00:01:25,300 --> 00:01:26,020
‫this hleb.

16
00:01:26,170 --> 00:01:33,040
‫I am using a physical device, a physical score out there and it has another IP address.

17
00:01:33,320 --> 00:01:39,270
‫If you are using the template from our previous lecture, take care of it here.

18
00:01:39,280 --> 00:01:44,320
‫I've changed the IP address, so you must have the correct IP address.

19
00:01:44,830 --> 00:01:54,100
‫OK, and here after opening the iOS object, I write Iris don't load mar candidate.

20
00:01:54,790 --> 00:01:59,170
‫And the argument of this method is our file.

21
00:01:59,320 --> 00:02:07,010
‫Our ACL dot text file is in the same current working directory, otherwise you must use a valid path.

22
00:02:07,180 --> 00:02:09,930
‫So here I write ACL dot.

23
00:02:11,530 --> 00:02:20,710
‫After that I want to see or I want to get the differences between my configuration, my router configuration

24
00:02:20,830 --> 00:02:21,880
‫and this file.

25
00:02:22,030 --> 00:02:26,040
‫Or in other words, I want to see what will be modified.

26
00:02:26,740 --> 00:02:31,080
‫I write def equals I don't compare config.

27
00:02:31,540 --> 00:02:34,210
‫Let's test our script until this point.

28
00:02:34,240 --> 00:02:39,370
‫So here I'm going to print the default variable and I'm running the script.

29
00:02:43,230 --> 00:02:52,950
‫And this is the output, so in fact, it will add these comments, we can see that it's the same content

30
00:02:53,040 --> 00:02:54,510
‫as in this file.

31
00:02:55,160 --> 00:02:58,420
‫And now let's continue developing our script.

32
00:02:59,070 --> 00:03:01,420
‫I check if there is any difference.

33
00:03:01,800 --> 00:03:09,720
‫So if Lenn of the greater than zero and if there is any difference, I print the difference.

34
00:03:10,050 --> 00:03:18,710
‫After the user is seeing the difference, the user can choose if he wants to commit or not the changes.

35
00:03:19,320 --> 00:03:21,140
‫So let's create another variable.

36
00:03:21,150 --> 00:03:23,910
‫Alexei A.R. equals input of.

37
00:03:24,870 --> 00:03:34,950
‫And now a question that will be asked, so climate changes and the user should answer with yes or no.

38
00:03:35,430 --> 00:03:41,940
‫And now if the answer is yes, I am going to commit the changes.

39
00:03:42,180 --> 00:03:49,950
‫So here I write the message, commit changes, and I am calling the committee config method.

40
00:03:49,950 --> 00:03:56,930
‫So I don't commit config and the parentheses and print, none else.

41
00:03:57,270 --> 00:04:06,660
‫So if the user answered, no, I am printing here, no changes required and I am going to discard the

42
00:04:06,660 --> 00:04:07,500
‫configuration.

43
00:04:07,510 --> 00:04:15,520
‫So I am calling the discard config method and all before writing the script.

44
00:04:15,660 --> 00:04:21,780
‫Let's see if there is any ox's least configured on our outer somewhere out there.

45
00:04:21,840 --> 00:04:22,430
‫All right.

46
00:04:22,440 --> 00:04:27,000
‫So IP access list and there is no axis list.

47
00:04:27,360 --> 00:04:30,900
‫Also, I want to check if telnet is working.

48
00:04:30,900 --> 00:04:36,150
‫So Telnet into the IP address of my daughter and you can see that Telnet works.

49
00:04:36,390 --> 00:04:38,130
‫And now Beşiktaş Python script.

50
00:04:38,140 --> 00:04:39,780
‫I am going to run the script.

51
00:04:41,340 --> 00:04:49,440
‫The script is running, and shortly it will display our deferrable, so the comments that will be added

52
00:04:49,440 --> 00:04:51,150
‫to the outas configuration.

53
00:04:51,700 --> 00:05:00,950
‫These are the comments and asking if I want to commit the changes and I must answer yes or no excuse.

54
00:05:00,990 --> 00:05:06,590
‫Yes, I want to commit the changes can make changes, OK?

55
00:05:07,380 --> 00:05:11,880
‫It takes some time, but after a few seconds it will display.

56
00:05:11,880 --> 00:05:12,300
‫Done.

57
00:05:16,640 --> 00:05:24,050
‫And it's done the script finished its execution, now let's go to our out there.

58
00:05:25,610 --> 00:05:28,070
‫This is a kind of soul connection to my daughter.

59
00:05:28,310 --> 00:05:32,260
‫And here I want to display all access lists.

60
00:05:32,300 --> 00:05:34,900
‫So show IP access lists.

61
00:05:36,020 --> 00:05:42,560
‫And they can see our list of the axis list that has been added by napalm.

62
00:05:44,380 --> 00:05:54,190
‫Of course, if I try to connect to the Internet, the packets will be dropped telnet, then dot, dot,

63
00:05:54,340 --> 00:05:59,980
‫dot one and we can see that it doesn't connect to the router.

64
00:06:00,310 --> 00:06:07,090
‫And here, if I leased the access list one more time, I can see that I have six Maxi's.

65
00:06:07,090 --> 00:06:15,790
‫So six packets matched against the school, the school that drops incoming telnet Bishkek's.

66
00:06:17,950 --> 00:06:25,690
‫Here, I've used a simple text file, but I can create these config files by passing values through

67
00:06:25,690 --> 00:06:34,960
‫a templating language such as Ginge to in this way we can automate the configuration of many devices

68
00:06:34,960 --> 00:06:39,170
‫and the configuration could be specific to each device.

69
00:06:40,240 --> 00:06:48,670
‫We can say a lot about generating configuration files, using templates, but I won't go into any detail

70
00:06:48,670 --> 00:06:49,150
‫here.


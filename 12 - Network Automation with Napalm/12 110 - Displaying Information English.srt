﻿1
00:00:01,020 --> 00:00:07,980
‫In this lecture, I'll show you how to process or how to display the information retrieved by napalm.

2
00:00:08,160 --> 00:00:11,650
‫I took the example from the previous lecture.

3
00:00:11,820 --> 00:00:20,130
‫The example I've shown you on how to connect to a device using napalm, and I'll continue developing

4
00:00:20,130 --> 00:00:20,370
‫it.

5
00:00:21,570 --> 00:00:29,220
‫A salary cap from the previous lecture here, I've imported the Gap network driver from the library.

6
00:00:29,370 --> 00:00:32,920
‫I've chosen a device type a support device.

7
00:00:32,970 --> 00:00:34,230
‫This is Esquires.

8
00:00:34,770 --> 00:00:40,620
‫After that, by calling the driver a function, I am creating an object.

9
00:00:40,920 --> 00:00:48,240
‫The driver function has some arguments, the IP address of the device, the user and the password used

10
00:00:48,240 --> 00:00:55,510
‫for SSX connection and an optional ARC's parameter, which is the enable password.

11
00:00:55,590 --> 00:01:04,220
‫After that, I've opened the Iooss object and after I'm done with it, I close the object.

12
00:01:04,440 --> 00:01:08,550
‫And here in between we can have our python code.

13
00:01:08,790 --> 00:01:12,930
‫So in this example, I want to get the ARP table.

14
00:01:13,180 --> 00:01:21,470
‫So I have this topology with threequarters and I want the ARP table back to our code here.

15
00:01:21,750 --> 00:01:22,320
‫All right.

16
00:01:22,320 --> 00:01:28,770
‫Output equals iris dot and now a method offered to buy this object.

17
00:01:29,070 --> 00:01:31,470
‫And there is get our paper.

18
00:01:32,460 --> 00:01:41,070
‫Get our table returns, a list of dictionaries for each entry in the ARP table.

19
00:01:41,100 --> 00:01:44,820
‫There is a dictionary and we have more entries.

20
00:01:44,820 --> 00:01:50,160
‫So we need a list of dictionaries of entries for that.

21
00:01:50,280 --> 00:01:58,470
‫I can display the content of this variable in this way for item in output.

22
00:02:00,550 --> 00:02:03,990
‫Print item, and I'll run the script.

23
00:02:08,120 --> 00:02:12,200
‫And you can see here the dictionaries of my list.

24
00:02:14,290 --> 00:02:23,250
‫This is the first entry in our table, the second entry and the last the third entry we can see here

25
00:02:23,470 --> 00:02:25,510
‫key value pairs.

26
00:02:25,540 --> 00:02:34,610
‫So Interface you segment zero zero Mac and the Mac address IP and the IP address and the H.

27
00:02:35,500 --> 00:02:43,710
‫Another possibility to display the output is to use the JSON module here.

28
00:02:43,870 --> 00:02:47,830
‫I'm importing the JSON module, import JSON.

29
00:02:49,610 --> 00:02:57,800
‫And here I can have something like this, let's say dump equals Jason dot dumps.

30
00:03:00,630 --> 00:03:04,280
‫Jason Dump's will return a string.

31
00:03:04,320 --> 00:03:11,590
‫It takes this list of dictionaries as an argument output and it returns a string.

32
00:03:11,940 --> 00:03:17,270
‫So here I want to print the content of my dump variable.

33
00:03:17,580 --> 00:03:18,600
‫I ran the script.

34
00:03:22,880 --> 00:03:26,510
‫And this is my dump variable.

35
00:03:26,570 --> 00:03:27,830
‫This is a stink.

36
00:03:28,310 --> 00:03:37,080
‫OK, and we can save this thing to a file or we can display the string as we want here.

37
00:03:37,250 --> 00:03:39,680
‫We can't have two other arguments.

38
00:03:40,340 --> 00:03:43,040
‫We can have a of key argument.

39
00:03:43,940 --> 00:03:47,330
‫This means it will be sought by the key.

40
00:03:47,480 --> 00:03:55,440
‫The key of my dictionary equals tool, and I want to invent the information.

41
00:03:55,550 --> 00:04:03,370
‫So let's say intent equals and now the number of spaces used for indentation.

42
00:04:03,590 --> 00:04:04,880
‫So let's say four.

43
00:04:05,180 --> 00:04:06,680
‫And I ran the script.

44
00:04:10,220 --> 00:04:15,050
‫And you can see here a very nice way of displaying the information.

45
00:04:16,940 --> 00:04:22,880
‫This information can be easily saved to a file and can be easily read.

46
00:04:23,810 --> 00:04:31,430
‫Of course, this is a string, so if I want to save the information to a file, I do write with Open,

47
00:04:32,270 --> 00:04:34,410
‫let's say Harpe the text.

48
00:04:35,730 --> 00:04:41,810
‫I'm opening the file in right mode as if if that's right.

49
00:04:43,120 --> 00:04:47,970
‫Dump and I don't want to print this value anymore.

50
00:04:51,930 --> 00:04:53,690
‫And I am running the script.

51
00:04:59,150 --> 00:05:06,620
‫OK, it's ready and here in the same directory with my Python script in the current working directory,

52
00:05:07,100 --> 00:05:16,700
‫a new file named after Texte appeared in this file contains my ARP table in a nice format.

53
00:05:17,170 --> 00:05:24,470
‫So, in fact, this is how we display and save information retrieved by napalm.


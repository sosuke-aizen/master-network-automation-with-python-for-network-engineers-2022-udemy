﻿1
00:00:02,010 --> 00:00:08,160
‫Hello and welcome back in this section, we'll take a look at Napalm.

2
00:00:08,550 --> 00:00:17,010
‫Napalm is an acronym for Network Automation in programmability abstraction layer with multi vendor support.

3
00:00:17,310 --> 00:00:25,950
‫Napalm is written in Python and is a vendor neutral cross platform open source project that provides

4
00:00:25,950 --> 00:00:34,650
‫Ununified API, a standard application programming interface, or a set of functions to interact with

5
00:00:34,650 --> 00:00:38,100
‫different networking device operating systems.

6
00:00:39,090 --> 00:00:48,000
‫Networking vendors have developed their own API for device configuration, napalmed supports the following

7
00:00:48,000 --> 00:00:52,850
‫vendors we can see at least at this address at the CSIRO.

8
00:00:53,070 --> 00:01:06,990
‫So it supports our star iOS Johnny Birgeneau s Cisco iOS S.R. 40 OS from Fortinet, Cisco, iOS and

9
00:01:06,990 --> 00:01:07,620
‫so on.

10
00:01:08,680 --> 00:01:18,040
‫For Cisco, iOS Napalm uses Net Namiko because there is no API available, Ned Mieko is a prerequisite

11
00:01:18,040 --> 00:01:19,690
‫for running napalmed.

12
00:01:20,260 --> 00:01:26,180
‫The interaction with the device is done independently of its type.

13
00:01:26,560 --> 00:01:29,830
‫We just have to specify network driver.

14
00:01:29,980 --> 00:01:38,410
‫This is a library for each supported operating system and the napalmed will use a third party vendor.

15
00:01:38,440 --> 00:01:46,420
‫API Napalm is hiding the low level programming and is offering us only a very high level interface to

16
00:01:46,420 --> 00:01:48,670
‫interact with networking devices.

17
00:01:49,300 --> 00:01:56,500
‫Napalmed makes it easy to develop powerful python scripts that no matter of the device they are interacting

18
00:01:56,500 --> 00:02:02,710
‫with using ununified method, retrieve data or manipulate conflicts.

19
00:02:03,160 --> 00:02:07,420
‫For example, we can use the get BGP neighbors.

20
00:02:08,620 --> 00:02:16,030
‫This is a function, Wanapum, function in order to get the same information about BGP, no matter if

21
00:02:16,030 --> 00:02:23,950
‫we are interacting with a Cisco Iris Exer, a Juna's, an artist, Iris and so on.

22
00:02:24,730 --> 00:02:32,800
‫There are many functions available that can be used to automate many configuration tasks no matter of

23
00:02:32,800 --> 00:02:34,570
‫network or device.

24
00:02:35,330 --> 00:02:44,050
‫Then we must know that the napalmed already works with the most popular automation frameworks like Ansible

25
00:02:44,200 --> 00:02:46,180
‫s or Steck storm.

26
00:02:46,840 --> 00:02:52,960
‫In the next lecture's I'll show you many examples using napalm.


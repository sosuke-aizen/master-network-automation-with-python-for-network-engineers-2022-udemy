﻿1
00:00:00,450 --> 00:00:07,950
‫In the previous lectures, I've shown you how to modify device configuration using napalm, in fact,

2
00:00:07,950 --> 00:00:14,550
‫using the two methods it offers low to replace candidate and load the marked candidate.

3
00:00:15,060 --> 00:00:24,990
‫Napalm also offers the possibility to roll back the configuration or to return to our last configuration

4
00:00:24,990 --> 00:00:27,500
‫or before the last commit.

5
00:00:28,230 --> 00:00:33,630
‫There is a function named rollback that can be called, for example, on these outer.

6
00:00:33,660 --> 00:00:42,930
‫If I want to display X flash memory, we can see that here there is a rollback underlined config file.

7
00:00:43,260 --> 00:00:47,100
‫In fact, this is the old running config.

8
00:00:47,130 --> 00:00:54,570
‫So before the last comit, let's suppose I'm not satisfied with these changes.

9
00:00:54,930 --> 00:00:59,820
‫The ACL doesn't work as expected and I want to roll back.

10
00:01:00,390 --> 00:01:01,290
‫What can I do?

11
00:01:01,890 --> 00:01:12,720
‫So first I am going to command these lines of code and here I write on a server equals input of her

12
00:01:12,780 --> 00:01:13,800
‫all back.

13
00:01:14,310 --> 00:01:15,350
‫Yes or no.

14
00:01:16,620 --> 00:01:26,670
‫And now if answer equals yes, print rolling back and I am calling the roll back method so iOS don't

15
00:01:26,670 --> 00:01:27,780
‫roll back.

16
00:01:28,960 --> 00:01:34,810
‫It will automatically revert the changes in here print done.

17
00:01:36,870 --> 00:01:39,360
‫And I am going to run this script.

18
00:01:43,850 --> 00:01:50,900
‫This is the question it's asking, and they are going to right here, of course, yes, rolling back.

19
00:01:51,830 --> 00:01:59,920
‫We can see here how the script is connecting to the device and done let's run IP access list.

20
00:02:00,440 --> 00:02:08,870
‫And we can see there is no access list because the configuration has been reverted to our last running

21
00:02:08,870 --> 00:02:16,790
‫config or to the running config before the last comit before adding these comments.

22
00:02:17,000 --> 00:02:24,200
‫This is very important because sometimes a new configuration could cause problems and we can use the

23
00:02:24,200 --> 00:02:28,700
‫rollback method to revert to the last working running config.

24
00:02:28,730 --> 00:02:29,590
‫That's all.

25
00:02:29,660 --> 00:02:30,320
‫Thank you.


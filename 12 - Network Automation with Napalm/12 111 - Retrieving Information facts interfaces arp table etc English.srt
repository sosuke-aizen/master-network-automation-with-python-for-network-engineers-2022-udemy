﻿1
00:00:00,840 --> 00:00:08,970
‫Now I want to show you what information can be retrieved from devices of different vendors using napalm.

2
00:00:09,540 --> 00:00:13,100
‫We have the same topology from the previous example.

3
00:00:13,110 --> 00:00:20,030
‫So we have these three outliers in the I want to retrieve information using napalm.

4
00:00:20,340 --> 00:00:29,190
‫They are C-store outbursts, but they could be aboutus from any other vendor because napalm is a vendor

5
00:00:29,220 --> 00:00:31,710
‫neutral here in this script.

6
00:00:31,710 --> 00:00:40,560
‫After importing the GEP network, Priefer and creating a driver object for Scios, I've connected to

7
00:00:40,560 --> 00:00:41,370
‫the device.

8
00:00:41,400 --> 00:00:48,570
‫So this is the IP address they use are the password and the enable secret is an optional ARC's parameter.

9
00:00:49,940 --> 00:00:57,350
‫After I've opened the Irish connection here, I'll write my code, so first let's create a variable

10
00:00:57,350 --> 00:00:58,150
‫named output.

11
00:00:58,400 --> 00:01:01,340
‫I don't get facts.

12
00:01:01,700 --> 00:01:10,450
‫You must know that methods that return information about devices start with get.

13
00:01:10,790 --> 00:01:16,700
‫So we have get facts, get our people, get BGP neighbors and so on.

14
00:01:16,950 --> 00:01:20,750
‫I'm going to show you many examples now.

15
00:01:20,750 --> 00:01:29,240
‫I want to convert the output to a string using the Jason module so that JSON module must be imported

16
00:01:29,240 --> 00:01:34,280
‫and laterite dump equals JSON dot dump's take care.

17
00:01:34,290 --> 00:01:43,520
‫There is also a dump method but here we use the dumps with S at the end and here we have our output.

18
00:01:43,940 --> 00:01:53,960
‫Output is a list of dictionaries and I'm going to use Saugerties equals two and indent equals four and

19
00:01:53,960 --> 00:01:58,550
‫I'm going to print the temp variable and I run the script.

20
00:02:03,590 --> 00:02:13,280
‫We can see here the information this is the output converted to a string, so the get fix method, which

21
00:02:13,290 --> 00:02:20,840
‫targets the fully qualified domain name, the host name, the list of interfaces, what model we have

22
00:02:21,050 --> 00:02:27,030
‫the uptime watch, the vendor, the Ionis version and so on.

23
00:02:28,010 --> 00:02:29,660
‫Now let's see another example.

24
00:02:30,140 --> 00:02:33,230
‫We can also retrieve the ARB table.

25
00:02:33,230 --> 00:02:44,830
‫So output equal size dot get our people and I'll copy paste these two lines and here I'll comment them

26
00:02:45,140 --> 00:02:45,620
‫OK.

27
00:02:45,770 --> 00:02:46,820
‫And I'll run the script.

28
00:02:48,050 --> 00:02:53,180
‫And the script will display information about the ARP table.

29
00:02:54,520 --> 00:03:04,150
‫This is the information about our table, in fact, each dictionary is an entry in the ARP table.

30
00:03:04,390 --> 00:03:08,910
‫Now let's see information about device interfaces.

31
00:03:09,580 --> 00:03:10,630
‫So here.

32
00:03:11,110 --> 00:03:19,210
‫All right, output equals I don't get interfaces and I'll copy paste these two lines.

33
00:03:24,330 --> 00:03:31,900
‫I've commented these lines because I don't want to see a lot of output here in the picture console.

34
00:03:32,760 --> 00:03:37,710
‫Let's see what information about interfaces will my script display.

35
00:03:38,750 --> 00:03:46,940
‫The script is running, and in a few seconds we'll see the information, the get interfaces method with

36
00:03:47,240 --> 00:03:53,300
‫the information about each device interface if the interface is up.

37
00:03:53,630 --> 00:04:00,770
‫So if that zero is up, going zero, one is not up, it's down.

38
00:04:00,770 --> 00:04:01,750
‫We can see it is up.

39
00:04:01,760 --> 00:04:02,710
‫Equal is false.

40
00:04:02,720 --> 00:04:04,910
‫We can see its Mac address.

41
00:04:06,020 --> 00:04:13,160
‫We can also retrieve all the information about interfaces, for example, we can retrieve the interface

42
00:04:13,160 --> 00:04:21,290
‫IP or the interface counters, so output equals ours, don't get interface counters.

43
00:04:24,620 --> 00:04:27,020
‫And I'm executing the script.

44
00:04:30,870 --> 00:04:38,180
‫These are the interfaces, counters, so how many packets have been transmitted or received, Anderson,

45
00:04:38,310 --> 00:04:45,290
‫so, for example, we have if they going to zero zero interface, this is the interface they up.

46
00:04:45,300 --> 00:04:48,270
‫So this is the interface from here.

47
00:04:48,300 --> 00:04:54,060
‫And we can see that it received six hundred thirteen broadcast packets.

48
00:04:54,660 --> 00:05:02,670
‫It received this number of octets or buycks, this number of unicast packets and so on.

49
00:05:03,450 --> 00:05:12,150
‫If we want to get the eyepiece of the interfaces we can use here, the get interface IP method.

50
00:05:20,830 --> 00:05:27,520
‫In a short while, we'll see information about the IP addresses of my device.

51
00:05:29,630 --> 00:05:37,310
‫There are two interfaces that have an IP address configured at zero zero, and this is the IP address

52
00:05:37,340 --> 00:05:45,470
‫with a prefix length of twenty four, that means a slash twenty four and a Lubeck zero interface with

53
00:05:45,470 --> 00:05:48,980
‫this IP address I've already configured.

54
00:05:49,340 --> 00:05:52,010
‫BGP is a routing protocol.

55
00:05:52,020 --> 00:06:00,110
‫In fact, I have configured an internal BGP session between these three autres.

56
00:06:00,440 --> 00:06:03,180
‫Let's find information about BGP neighbors.

57
00:06:03,470 --> 00:06:07,970
‫There is a method named Get BGP Neighbors.

58
00:06:10,090 --> 00:06:18,620
‫So here, instead of get interfaces IP I right, get big zippy neighbors and I am running the script

59
00:06:18,640 --> 00:06:21,310
‫till then, I'll execute this command here.

60
00:06:21,320 --> 00:06:29,560
‫So show IP BGP neighbors and we can see hear information about the neighbors, the same information

61
00:06:29,560 --> 00:06:35,180
‫we can also see here displayed by our napalm script.

62
00:06:35,620 --> 00:06:37,180
‫This is very important.

63
00:06:37,180 --> 00:06:45,760
‫If we want to check if all neighbors are up and if we have a session, a session with those neighbors

64
00:06:46,030 --> 00:06:51,150
‫in, the last thing I want to show you is how to display information about the users.

65
00:06:52,210 --> 00:06:55,480
‫So there is a method called GEP users.

66
00:06:57,210 --> 00:07:04,600
‫And after running the script, we can see that there is only one user, this is the user with this password.

67
00:07:05,010 --> 00:07:06,500
‫This is a hash.

68
00:07:07,200 --> 00:07:12,960
‫This is how we retrieve information using napalm from networking devices.

69
00:07:13,410 --> 00:07:17,730
‫Of course, there are also other methods that retrieve information.

70
00:07:17,850 --> 00:07:28,830
‫We can see them by just typing here, get in the picture or our I.D. will display all methods of this

71
00:07:28,830 --> 00:07:31,980
‫object that start with get.

72
00:07:32,190 --> 00:07:38,580
‫So we have here get our table, get the interfaces, get BGP config, get environment, get federal

73
00:07:38,580 --> 00:07:44,790
‫policy, get A.P. servers, network instances and so on.


﻿1
00:00:00,300 --> 00:00:07,080
‫In this video, I'll show you how to test the connectivity between networking devices from Python scripts

2
00:00:07,080 --> 00:00:08,160
‫using napalm.

3
00:00:08,520 --> 00:00:13,680
‫So I want to check the connectivity between, for example, out the front end router three.

4
00:00:14,010 --> 00:00:20,820
‫I don't want to check the connectivity, the IP connectivity between the machine that executes the Python

5
00:00:20,820 --> 00:00:23,760
‫script, for example, my laptop and a device.

6
00:00:24,030 --> 00:00:27,000
‫I want to check the connectivity between devices.

7
00:00:27,510 --> 00:00:30,960
‫Normally, I should connect to one of those out there.

8
00:00:30,970 --> 00:00:37,530
‫For example, these are out there and I'll ping another router, let's say 10.1, dot, dot.

9
00:00:37,680 --> 00:00:42,420
‫Then this is router one and we can see that being works.

10
00:00:42,540 --> 00:00:46,700
‫So we can say that we have a layer of free connectivity.

11
00:00:47,100 --> 00:00:50,160
‫We can also execute an extended ping.

12
00:00:50,160 --> 00:00:54,800
‫We can specify more parameters being enter.

13
00:00:54,810 --> 00:00:56,820
‫And now the protocol is IP.

14
00:00:57,210 --> 00:01:00,270
‫The target IP address is 10 dot, dot, dot, dot, dot.

15
00:01:00,930 --> 00:01:07,440
‫Then I want to send then packets the data from size like say four hundred.

16
00:01:07,620 --> 00:01:12,750
‫We can stress a firewall to check our quality of service implementation.

17
00:01:12,990 --> 00:01:20,670
‫The time outis want a second and there is no extended comment and this is the result of our being the

18
00:01:20,670 --> 00:01:23,340
‫same thing we can do with napalmed.

19
00:01:24,060 --> 00:01:26,270
‫Back to our Python script here.

20
00:01:26,400 --> 00:01:28,890
‫I'll use the ping method.

21
00:01:29,010 --> 00:01:32,160
‫So output equals I was dot being.

22
00:01:32,670 --> 00:01:40,190
‫If I simply want to send a standard ping I just write here the destination IP address.

23
00:01:40,200 --> 00:01:43,740
‫So let's say 10.1 dot one, dot 20.

24
00:01:44,490 --> 00:01:52,450
‫OK, this is the output and I want to print the output is a string being equals.

25
00:01:52,470 --> 00:02:02,190
‫Jason, data dumps output sort of keys to indent for and print being and I'll run the script.

26
00:02:07,930 --> 00:02:15,670
‫Shortly, we'll see the result of our ping between the two devices, and here is the result, packet

27
00:02:15,670 --> 00:02:22,880
‫loss zero probes sent five, so it sent five packets and this is the result.

28
00:02:23,020 --> 00:02:32,530
‫This is the result of the first ICMP request, the second ICMP, a request we can see here, some statistics

29
00:02:33,010 --> 00:02:38,860
‫around trip time, average, around tripti, max mean and so on.

30
00:02:39,190 --> 00:02:47,530
‫Of course, if we are sending to an unreachable IP address, let's say here 66, there is no such a

31
00:02:47,530 --> 00:02:53,170
‫device and that around the script, the result is as we expect it to be.

32
00:02:56,300 --> 00:03:00,800
‫OK, Becky, close five probes sent five.

33
00:03:01,620 --> 00:03:10,960
‫Now, let's see, how can we use an extended being for that will use key arguments, so here destination.

34
00:03:11,010 --> 00:03:15,720
‫This is the name of the argument then how many packets do I want to send?

35
00:03:16,110 --> 00:03:18,990
‫Let's say count equals two, only two packets.

36
00:03:19,560 --> 00:03:26,010
‫And another useful parameter is source like say one, dot, dot, dot, dot one.

37
00:03:26,190 --> 00:03:29,540
‫I want to explain to you more about this option here.

38
00:03:29,880 --> 00:03:31,740
‫Back to our topology.

39
00:03:31,950 --> 00:03:37,920
‫By default, Cisco is using the IP address of the outgoing interface.

40
00:03:38,160 --> 00:03:45,900
‫So if this router one has, let's say, 10 interfaces and the hipping from router one, router three,

41
00:03:46,230 --> 00:03:53,520
‫the source IP address will be the IP address of the interface named E zero zero, because this is the

42
00:03:53,520 --> 00:03:54,960
‫outgoing interface.

43
00:03:55,590 --> 00:04:03,240
‫But there are moments when I want to use the IP address of another interface as the source IP, for

44
00:04:03,240 --> 00:04:03,720
‫example.

45
00:04:03,720 --> 00:04:07,680
‫I want to use the IP address of the Lubeck interface.

46
00:04:07,950 --> 00:04:13,530
‫If I run a sharp interface brief on another one, we can see that we have a Lubeck interface.

47
00:04:13,680 --> 00:04:22,290
‫I want this IP address to be the source IP from my ICMP packet because for example, I want to check

48
00:04:22,290 --> 00:04:26,910
‫if Rutherford B. Hayes or out back to router one.

49
00:04:26,920 --> 00:04:35,400
‫So I want to check a routing protocol if I can ping router three using this IP address 10.1 to 2010,

50
00:04:35,490 --> 00:04:43,230
‫but I can't think router three using the IP address of Lubeck, we can assume that there is no return

51
00:04:43,350 --> 00:04:43,890
‫route.

52
00:04:43,890 --> 00:04:50,050
‫So Router three doesn't have a route back to the Lubeck interface of router one.

53
00:04:50,100 --> 00:04:52,540
‫There are times when this is useful.

54
00:04:53,070 --> 00:04:54,780
‫Back to our Python script here.

55
00:04:55,110 --> 00:04:59,520
‫I'll run the script that uses an extended ping.

56
00:05:02,810 --> 00:05:10,190
‫OK, this is the result, this is an unreachable IP, in fact, there is no device that has this IP

57
00:05:10,190 --> 00:05:14,740
‫address and there's because we have two lost packets.

58
00:05:14,900 --> 00:05:17,240
‫But if I write here, say.

59
00:05:18,190 --> 00:05:26,350
‫Twenty, the IP address of router two and I ran the script one more time, will see that we sent two

60
00:05:26,350 --> 00:05:34,120
‫packets and we received back two packets packet loss, zero probes sent to this is the first packet

61
00:05:34,120 --> 00:05:35,860
‫and this is the second picked.

62
00:05:36,570 --> 00:05:43,840
‫This is how we test the connectivity between networking devices from Python scripts using napalm.


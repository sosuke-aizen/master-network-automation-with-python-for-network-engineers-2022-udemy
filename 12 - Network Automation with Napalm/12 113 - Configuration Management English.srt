﻿1
00:00:00,270 --> 00:00:08,670
‫In this lecture, I'll show you how to manage devices configuration using napalm, in fact, napalm

2
00:00:08,670 --> 00:00:16,020
‫can send the configuration from a file to a device and it uses SICP.

3
00:00:16,170 --> 00:00:20,970
‫So it's very important that SICP is working correctly.

4
00:00:21,780 --> 00:00:30,990
‫I would advise you to test that out there is prepared for SICP and there is a lecture for this, a previous

5
00:00:30,990 --> 00:00:38,280
‫lecture or better, you can test our Net Mieko SICP script to see that it works.

6
00:00:38,280 --> 00:00:44,850
‫And only after you are sure that recipe works, you can start this example.

7
00:00:45,920 --> 00:00:52,030
‫In this topology, I have her out there and I want to send some comments from a file to the.

8
00:00:53,240 --> 00:01:01,700
‫In fact, my Python script will check to see if there are any differences between the running config

9
00:01:01,700 --> 00:01:11,750
‫of the router and my file and will submit the differences, or in other words, it will replace the

10
00:01:11,750 --> 00:01:16,730
‫running configuration of the outer with the configuration from my file.

11
00:01:17,690 --> 00:01:21,090
‫Let's see how it works on this out there.

12
00:01:21,230 --> 00:01:31,520
‫There is the routing protocol enabled, so show IP protocols and we can see that there is the protocol

13
00:01:31,520 --> 00:01:32,150
‫enabled.

14
00:01:32,570 --> 00:01:33,650
‫What will I do?

15
00:01:34,730 --> 00:01:43,490
‫I run the terminal length zero comment, I want to see the entire output on the screen and now I run

16
00:01:43,490 --> 00:01:44,990
‫the show, run in.

17
00:01:46,450 --> 00:01:52,840
‫This is the configuration, I'll copy the configuration to a file without these two lines.

18
00:01:52,960 --> 00:01:56,170
‫These two lines don't belong to the configuration.

19
00:01:56,750 --> 00:02:00,560
‫Let's create a file like, say, config dot.

20
00:02:02,310 --> 00:02:10,860
‫And the inside this file, I'll paste the router configuration, the outer configuration and our back

21
00:02:10,860 --> 00:02:15,450
‫to the router, I want to disable the reporting protocol.

22
00:02:16,320 --> 00:02:18,660
‫Konforti now or out there.

23
00:02:20,130 --> 00:02:28,290
‫There is no routing protocol running on the device, so show IP protocols, returns, no routing protocol

24
00:02:28,680 --> 00:02:36,360
‫and of course if I want to show Iran, include Reep, there is no output and now I want to send the

25
00:02:36,360 --> 00:02:39,270
‫configuration from this file to my daughter.

26
00:02:40,500 --> 00:02:45,750
‫OK, the name of the file is only conflict dot text without dot by.

27
00:02:48,060 --> 00:02:48,850
‫That's better.

28
00:02:48,900 --> 00:02:55,850
‫So here, let's see how it works first on the out there, the archive command should be executed.

29
00:02:56,190 --> 00:03:06,260
‫That means archive in our path and the path to the storage component in this case is disk zero.

30
00:03:06,420 --> 00:03:10,690
‫Maybe you have Flesche or Discon or slot zero or something like that.

31
00:03:11,010 --> 00:03:12,900
‫This is necessary for napalmed.

32
00:03:13,140 --> 00:03:16,980
‫Otherwise we could get an error on archive error.

33
00:03:17,400 --> 00:03:21,710
‫That being said, let's return here and I'll write something like this.

34
00:03:22,140 --> 00:03:32,910
‫I don't load replace candidate and the argument is filename equals and my config file in this case config

35
00:03:33,150 --> 00:03:33,970
‫dot texte.

36
00:03:34,170 --> 00:03:36,490
‫Here you must use a valid path.

37
00:03:37,200 --> 00:03:43,500
‫In fact, it will check if there is any difference between the configuration from this file and the

38
00:03:43,500 --> 00:03:45,320
‫routers arraigning config.

39
00:03:45,450 --> 00:03:51,120
‫And if there is any difference, it will modify the router accordingly.

40
00:03:52,020 --> 00:03:55,940
‫OK, let's right here live from difference equals.

41
00:03:55,950 --> 00:04:05,220
‫I don't compare config, compare config means that it will compare the configurations and will display

42
00:04:05,220 --> 00:04:07,650
‫or it will return the output.

43
00:04:07,650 --> 00:04:11,900
‫Andile, display that output and I'll run the script.

44
00:04:11,940 --> 00:04:17,190
‫Of course we can imagine what will be displayed on the screen.

45
00:04:17,640 --> 00:04:25,710
‫Here is Lautaro and network then dot dot dot dot dot zero in this file but not on my daughter.

46
00:04:27,980 --> 00:04:30,380
‫The script is running, it takes a while.

47
00:04:31,520 --> 00:04:35,780
‫We must wait, and this is the difference.

48
00:04:35,810 --> 00:04:43,670
‫These comments belong to my file, but not to the doubters configuration, which is why we have this

49
00:04:43,670 --> 00:04:44,810
‫plus sign here.

50
00:04:44,840 --> 00:04:47,120
‫I want to show you something here.

51
00:04:47,480 --> 00:04:50,080
‫If I ran the comment, dear, this is zero.

52
00:04:50,390 --> 00:04:54,170
‫We can see that there is a file named Candidate Config Dhoti.

53
00:04:55,370 --> 00:05:03,650
‫This file has been created by my napalm script and this file will replace the running config over there.

54
00:05:03,650 --> 00:05:07,550
‫Out there, of course, if I choose to commit the changes.

55
00:05:07,820 --> 00:05:11,960
‫So here, I'll write something like this if Lenn of def.

56
00:05:12,110 --> 00:05:15,740
‫So there is a difference between the configurations.

57
00:05:15,890 --> 00:05:22,590
‫I print the difference and commit changes and I am committing the changes.

58
00:05:22,610 --> 00:05:29,120
‫So in fact I am modifying the routers running config so I commit config and print.

59
00:05:29,360 --> 00:05:31,330
‫Done Hayles print.

60
00:05:31,610 --> 00:05:39,080
‫No changes required if there is no difference between my configuration file and the routers running

61
00:05:39,080 --> 00:05:46,620
‫config and I am discarding the changes so IO is not discard config voxel.

62
00:05:46,760 --> 00:05:48,170
‫Now I'm running the script.

63
00:05:50,190 --> 00:05:56,970
‫We can see here the difference comi changes this message from here, now it's committing the changes.

64
00:05:58,090 --> 00:06:06,610
‫If I are on the desk zero, Colin, I see here another file named Roll Back underlined config dot texte,

65
00:06:07,440 --> 00:06:15,970
‫it is possible to roll back the configuration or to return to the previous configuration before our

66
00:06:16,210 --> 00:06:17,490
‫committee operation.

67
00:06:17,890 --> 00:06:20,490
‫That's why it's safe here.

68
00:06:20,530 --> 00:06:22,270
‫The old running config.

69
00:06:22,270 --> 00:06:25,610
‫In fact, this is the old running config.

70
00:06:26,230 --> 00:06:27,520
‫This is very important.

71
00:06:27,740 --> 00:06:30,060
‫There is a function name to roll back.

72
00:06:30,220 --> 00:06:40,330
‫So if I want to return to the old configuration, I write ionis dot roll back and it's done OK.

73
00:06:40,480 --> 00:06:48,010
‫Now if we check the out show IP protocols, we can see there is the report routing protocol enabled

74
00:06:48,010 --> 00:06:51,880
‫before we had no routing protocol on this router.

75
00:06:52,090 --> 00:06:56,230
‫OK, Sharon, we can see here router and the network.

76
00:06:56,230 --> 00:06:59,470
‫These are the lines that have been added.

77
00:07:00,070 --> 00:07:03,820
‫This can be very useful when we have many devices.

78
00:07:03,970 --> 00:07:11,290
‫The configuration files are locally saved and we want to push the configuration to all devices.


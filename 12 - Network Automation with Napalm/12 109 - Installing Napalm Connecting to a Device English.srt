﻿1
00:00:00,500 --> 00:00:08,420
‫In this lecture, we'll continue our talk about napalm, napalm doesn't belong to the standard Python

2
00:00:08,420 --> 00:00:10,910
‫Library, so we must install it.

3
00:00:11,300 --> 00:00:13,880
‫We can install napalm using beep.

4
00:00:14,340 --> 00:00:25,370
‫So from the command prompt, I can run pipe, install napalm or from bicarb we can go to settings then

5
00:00:25,370 --> 00:00:26,360
‫interpretor.

6
00:00:30,930 --> 00:00:35,520
‫And here, by pressing the plus side, we can search for Napranum.

7
00:00:37,570 --> 00:00:45,250
‫OK, this is napalm, Nasdaq automation and programmability abstraction layer with multi vendor support

8
00:00:45,610 --> 00:00:47,140
‫and install package.

9
00:00:55,510 --> 00:00:58,990
‫And napalm has been successfully installed.

10
00:01:01,860 --> 00:01:08,430
‫Now that we've installed napalm, I want to show you how to connect to a networking device.

11
00:01:09,480 --> 00:01:15,170
‫First, we must import the GEP network driver from napalm library.

12
00:01:15,360 --> 00:01:20,340
‫So from napalm import get network driver.

13
00:01:20,890 --> 00:01:27,960
‫Then we create the driver object like say, driver equals get network driver.

14
00:01:28,200 --> 00:01:37,080
‫And now we must specify the driver or the type of the device net is connecting to.

15
00:01:37,800 --> 00:01:46,710
‫These can be Johnny Paşa device, a Cisco iOS, a Cisco, Iris, S.R. Fortinet and so on.

16
00:01:47,400 --> 00:01:53,970
‫We can see a list with all devices McParland supports here at this link.

17
00:01:55,000 --> 00:02:06,730
‫In our case, we are going to use Cisco iOS, so I'll write here iOS by choosing iOS, Napalm will automatically

18
00:02:06,730 --> 00:02:13,060
‫select the correct driver or the set of functions for Cisco iOS.

19
00:02:13,360 --> 00:02:21,510
‫After that, we create another object like, say, iOS equals the driver of A..

20
00:02:21,640 --> 00:02:25,770
‫The first argument is the IP address of my device.

21
00:02:26,350 --> 00:02:35,070
‫I have this topology with freakouts, so I'll write the IP address of one of these devices.

22
00:02:35,290 --> 00:02:39,220
‫So let's say 10 dot one data.

23
00:02:39,220 --> 00:02:44,980
‫And then the second argument is the user name in this case.

24
00:02:45,220 --> 00:02:52,000
‫And the third argument is the password napalmed will use the net.

25
00:02:52,510 --> 00:02:55,860
‫So in fact, it will use SSX.

26
00:02:56,140 --> 00:03:03,730
‫This is the user and the password for SSX, for SSX authentication and the password.

27
00:03:03,730 --> 00:03:10,450
‫These Cisco, if we don't want to have this sort of coded here, we can use the get pass module.

28
00:03:10,960 --> 00:03:20,170
‫And if we are using Cisco, there is also required to specify the enable password, the password that

29
00:03:20,560 --> 00:03:21,730
‫it asks.

30
00:03:21,880 --> 00:03:29,200
‫When we run the enable comment for that, I am going to create another variable like, say, optional

31
00:03:29,230 --> 00:03:33,380
‫ARGs and this variable is of type dictionary.

32
00:03:34,180 --> 00:03:41,620
‫The key is secret and the value is our enable passata in this case is Cisco.

33
00:03:41,950 --> 00:03:44,920
‫So Cisco is the enable password.

34
00:03:46,290 --> 00:03:57,240
‫And here I'll pass this variable is a final argument so optional ARC's equals optional ARC's.

35
00:03:57,780 --> 00:04:05,130
‫Now that I've created an iOS object, I open it so iOS don't open.

36
00:04:07,190 --> 00:04:13,320
‫Now the connection is open and we can communicate with our device.

37
00:04:13,730 --> 00:04:21,470
‫I can send commands, I can retrieve data, or I can gather facts about this device.

38
00:04:21,800 --> 00:04:28,210
‫After finishing with this device, it's necessary to close the connection.

39
00:04:28,550 --> 00:04:31,610
‫So here I was not close.

40
00:04:31,760 --> 00:04:40,640
‫And in between here will be our napalm code, for example, just to check that it works, let's bring

41
00:04:40,790 --> 00:04:43,760
‫all the methods that it offers.

42
00:04:44,510 --> 00:04:47,030
‫Print Dear of Iris.

43
00:04:48,090 --> 00:04:49,470
‫And I ran the script.

44
00:04:54,470 --> 00:05:03,400
‫The script is running and it displayed the attributes and the methods associated with this device.

45
00:05:03,680 --> 00:05:10,610
‫For example, you can see here a config replace connection tests, compare config, get output table,

46
00:05:11,000 --> 00:05:14,180
‫get BGP neighbors and so on.

47
00:05:14,300 --> 00:05:18,190
‫We can execute these methods on this device.

48
00:05:18,410 --> 00:05:22,940
‫And of course we can execute the same method no matter of the device.

49
00:05:23,330 --> 00:05:27,920
‫If he or I have, let's say John OS, nothing would change.

50
00:05:29,160 --> 00:05:35,610
‫Of course, maybe the Jarndyce doesn't have one enabled passport or it has another optional argument,

51
00:05:35,610 --> 00:05:43,800
‫but it will use the same methods in the next lectures, I'll show you how to configure different networking

52
00:05:43,800 --> 00:05:46,140
‫devices using napalm.

53
00:05:46,740 --> 00:05:48,560
‫See you in a few seconds.


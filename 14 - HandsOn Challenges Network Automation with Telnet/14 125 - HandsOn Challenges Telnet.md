

 **More information about the challenges:**

  * Solve these challenges only after watching all the videos in the section, in the order that I’ve provided.

  * For Cisco automation tasks I’d recommend you to use [this topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN) in GNS3 or create a similar one. [Import the GNS3 Project.](https://drive.google.com/open?id=1gqx8pGamn02D0dzOpbd3FKIPOhRJZBgz)

  * If your solution is not correct, then try to understand the error messages, watch the video again, rewrite the solution, and test it again. Repeat this step until you get the correct solution.

  *  **Save** the solution in a file for future reference or recap.

  

 **Challenge #1**

Create a Python script that connects to a Cisco Router using Telnet and
executes the **show users** command in order to display the logged-in users.

Print out the output of the command.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=19vztX5pj_4_Ih--dYsvXD8EVD1bMl9rM).

  

 **Challenge #2**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=19vztX5pj_4_Ih--dYsvXD8EVD1bMl9rM)
so that it will prompt the user for its password without echoing (use
_getpass_ module). Run the script in the terminal (you can not run it in
PyCharm).

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1Pc6ykYgrVBJYIAlnF9fTD-J--GiWBmf8).

  

 **Challenge #3**

Create a Python script that connects to a Cisco Router using Telnet, enters
the enable mode, and then executes the **show run** command. Save the output
to a file.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1S7FRKexFD2U86g7OANcDgCdT_ilq7Prm).

\--

 **Challenge #4**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1HA8FrWStkGaAapjnp7kEayUwiY8ftvkT)
that executes **show ip interface brief** on a remote Cisco Router using
Telnet and displays the output.

However the script it’s hanging (nothing happens when it’s run).

Your task is to troubleshoot the issue and solve it so that it displays the
output of the command.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1g-wGCklHoKjFZxIQAV_uE8EXCZpyH44f).

  

 **Challenge #5**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1IFUzJ3ySRZFm63AOkejSQrrDWsJZdzeI)
that executes **show ip interface brief** on a remote Cisco Router using
Telnet and displays the output.

However, instead of the normal output of the command (a string), it displays
some sort of gibberish.

Your task is to troubleshoot the issue and solve it so that it displays the
entire output correctly.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1quAf6rnJHsH1JiNRb6JMVvnypf88ntMG).

  

 **Challenge #6**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1Vc2D0m32b4xYnxcvdNIlZ_KGnXsH4Wx-)
that executes **show ip interface brief** on a remote Cisco Router using
Telnet and displays the output.

However, the output of the command can’t be displayed as there is an error in
the script. The script doesn’t run, rather it hangs

Your task is to troubleshoot the issue and solve it so that it works as
expected.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1uJOLGiFEgOgnAH9m8HfXZ-aGmQgCUYg4).

  

 **Challenge #7**

Create a Python script that connects to a Cisco Router using Telnet and
executes a list of commands. The commands are saved in a Python list.

An example of a list with commands to execute:

 _# the second element (cisco) is the enable command_

 _commands = [_ ** _'enable'_** _,_ ** _'cisco'_** _,_ ** _'conf t'_** _,_ **
_'username admin1 secret cisco'_** _,_ ** _'access-list 1 permit any'_** _,_
** _'end'_** _,_ ** _'terminal length 0'_** _,_ ** _'sh run | i user'_** _]_

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1U9T3ZZGJKqfbS_IhinInZiufPiCP6Jpi).

  

 **Challenge #8**

Create a Python script that connects to a Cisco Router using Telnet and
executes all the commands from a text file.

An example of a [text file with
commands.](https://drive.google.com/open?id=1Ct_t5TDeR47qYi4E38YSHPmB5z15qoKS)

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1wkokX1RYo_bvhQtjlHQyED-iKou-nqgT).

  

 **Challenge #9**

Consider the [custom Telnet
Class](https://drive.google.com/open?id=1F4QF-z85TDXbB1hAFarJxV6lbrQV7Mcz)
developed in the course.

Add a new method called **send_from_file()** that receives as an argument a
text file with commands and sends all the commands to the remote device to be
executed.

An example of a [text file with
commands.](https://drive.google.com/open?id=1D_oLY2cSQYble05-AwLmwFfXvoMUwag3)

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1DZr7sITgJN2D9_tC444TFjktEEsvEhA7).

  

 **Challenge #10**

Consider a [topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-
eCZHkeUChiS-VbN) with multiple devices like [this
one.](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

For each device in the topology, you have a Python dictionary that stores the
Telnet connection information (IP, username, password) **but also a filename**
that contains the commands to be sent to that device.

 **Example:**

 _r1 = {_ ** _'host'_** _:_ ** _'192.168.122.10'_** _,_ ** _'username'_** _:_
** _'u1'_** _,_ ** _'password'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_
** _'_**[ **
_ospf.txt_**](https://drive.google.com/open?id=1BPPOMLpMuq_4Dk7iE05xPAAuRm6iR_5z)
** _'_** _}_

 _r2 = {_ ** _'host'_** _:_ ** _'192.168.122.20'_** _,_ ** _'username'_** _:_
** _'u1'_** _,_ ** _'password'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_
** _'_**[ **
_eigrp.txt_**](https://drive.google.com/open?id=1pDIVWzQ-5U1SwvDw7YAwH_h8tO6K1R6U)
** _'_** _}_

 _r3 = {_ ** _'host'_** _:_ ** _'192.168.122.30'_** _,_ ** _'username'_** _:_
** _'u1'_** _,_ ** _'password'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_
** _'_**[ **
_router3.conf_**](https://drive.google.com/open?id=1ElFHmTV7-pj0aOtWoU9jyZqaE2FXiV_g)
** _'_** _}_

 _…._

Create a Python script that connects to each device using Telnet and executes
the commands from the file (which is the value of the dictionary _config_
key).

Use the [Telnet
Class](https://drive.google.com/open?id=1F4QF-z85TDXbB1hAFarJxV6lbrQV7Mcz)
that was developed in the course or create the entire Python script from
scratch.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1BA5J9epbRWnx3tzZpcs6uBRIrAlvrO36).


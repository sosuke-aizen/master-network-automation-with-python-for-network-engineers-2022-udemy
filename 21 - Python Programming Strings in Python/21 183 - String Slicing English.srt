﻿1
00:00:00,680 --> 00:00:08,840
‫In the previous lecture, we've seen how a string is indexed and how to use indexes, an important remark

2
00:00:08,840 --> 00:00:12,080
‫is that the python strings can not be changed.

3
00:00:12,270 --> 00:00:13,730
‫They are immutable.

4
00:00:14,150 --> 00:00:17,420
‫If you need a different string, you should create a new one.

5
00:00:17,890 --> 00:00:24,170
‫Therefore, assigning to an indexed position in the string results in an error.

6
00:00:24,980 --> 00:00:34,550
‫I have this variable that starts a string movie of zero will return B and now I'll try to change that

7
00:00:34,550 --> 00:00:35,110
‫character.

8
00:00:35,390 --> 00:00:41,540
‫So movie of zero equals Aleksei simply X.

9
00:00:42,750 --> 00:00:49,650
‫And I've got the type error is the art object does not support item assignment.

10
00:00:50,520 --> 00:00:53,340
‫This is what immutability means.

11
00:00:53,790 --> 00:00:57,330
‫We cannot change an immutable variable.

12
00:00:58,280 --> 00:01:06,920
‫In addition to indexing string's support, also slicing while indexing is used to obtain individual

13
00:01:07,070 --> 00:01:13,130
‫characters, slicing allows you to obtain a substring or a part of a string.

14
00:01:13,430 --> 00:01:15,860
‫It allows you to extract data.

15
00:01:16,370 --> 00:01:23,660
‫Slicing relies on indexing and work only with sequence types like strings or leaks.

16
00:01:24,560 --> 00:01:26,420
‫Let's try some examples.

17
00:01:26,960 --> 00:01:31,160
‫Movie of zero and two.

18
00:01:32,230 --> 00:01:34,840
‫This is how slicing works.

19
00:01:36,260 --> 00:01:46,900
‫It will return characters from position zero, so from the end, that's included two two and excluded.

20
00:01:47,150 --> 00:01:55,010
‫So in fact will return the character from position zero and the character from position one because

21
00:01:55,010 --> 00:01:58,160
‫the character from position two is excluded.

22
00:01:58,370 --> 00:01:59,890
‫Let's try another example.

23
00:02:00,230 --> 00:02:03,580
‫Movie of two, Callon five.

24
00:02:04,340 --> 00:02:10,990
‫This will return characters from position to included two to position five excluded.

25
00:02:11,690 --> 00:02:13,660
‫So e space energy.

26
00:02:14,330 --> 00:02:20,600
‫Note how the start is always included and the end always excluded.

27
00:02:22,630 --> 00:02:31,900
‫Slice indexes have some defaults and a meeting forced index defaults to zero and an all meeting second

28
00:02:31,900 --> 00:02:41,800
‫index defaults to the size of the string being sliced in this case movie of Callon, two main characters

29
00:02:41,800 --> 00:02:47,440
‫from the beginning to the position to the VIX excluded by default.

30
00:02:47,440 --> 00:02:53,350
‫The first index is zero now movies of four colon.

31
00:02:53,350 --> 00:02:56,490
‫And I don't write the second index anymore.

32
00:02:57,130 --> 00:03:05,350
‫The second index defaults to the size of the string being sliced will return from index four till the

33
00:03:05,350 --> 00:03:06,540
‫end of the string.

34
00:03:07,180 --> 00:03:16,330
‫This means that if I write the movie of Collen and let's say a number, any number four plus movie of

35
00:03:16,600 --> 00:03:22,180
‫the same number four call on this will return the entire string.

36
00:03:22,510 --> 00:03:26,010
‫And this is no matter of the value I am using here.

37
00:03:26,020 --> 00:03:33,730
‫If I write here too and here too, I'll get the entire string the same if I write here like say eight

38
00:03:34,240 --> 00:03:37,600
‫and here eight x the entire string.

39
00:03:38,050 --> 00:03:46,120
‫Another interesting example is a movie of minus two Callon and the closing square brackets.

40
00:03:46,700 --> 00:03:55,360
‫It returned e are so characters from the second last included to the end.

41
00:03:56,600 --> 00:04:03,400
‫This is minus one, this is minus two, so this is the character included till the end.

42
00:04:04,280 --> 00:04:11,540
‫We've seen in the previous lecture that attempting to use an index that is too large will result in

43
00:04:11,540 --> 00:04:11,970
‫an error.

44
00:04:12,130 --> 00:04:17,350
‫However, out of range slice indexes are handled without error.

45
00:04:17,690 --> 00:04:26,060
‫So moving off and here I'll write a very large number will return on their bot movie off and let's say

46
00:04:26,060 --> 00:04:30,880
‫for Callon and a very large number won't return an error.

47
00:04:31,280 --> 00:04:40,850
‫It will be taken from the character at position five to the end of my string and movie of, let's say,

48
00:04:41,210 --> 00:04:41,900
‫90.

49
00:04:42,170 --> 00:04:52,550
‫Kolon will return an empty string so we can use out of boundary indexes with slicing slices.

50
00:04:52,550 --> 00:05:00,890
‫Also support a third argument, the step of value when not specified the step value defaults to one

51
00:05:01,370 --> 00:05:12,210
‫Lexia example movie of the zero colon, then colon to two is the third argument will return from index

52
00:05:12,210 --> 00:05:17,570
‫is zero included to index then excluded insteps of two.

53
00:05:20,520 --> 00:05:33,990
‫S t e g d a and so on, this being said, movie of colon, colon minus one will in fact return a new

54
00:05:33,990 --> 00:05:37,950
‫string with X characters in the reverse order.

55
00:05:39,380 --> 00:05:46,430
‫The last character from the initialised thing is the first character in the new string, the second

56
00:05:46,430 --> 00:05:56,210
‫last character is the second and so on Vex because Colon, Colon or he turns the entire string but uses

57
00:05:56,210 --> 00:05:57,880
‫a step of minus one.

58
00:05:58,100 --> 00:06:06,450
‫So one element from the end to the beginning of the string, that's all about string slicing.

59
00:06:06,590 --> 00:06:08,300
‫See you in the next lecture.


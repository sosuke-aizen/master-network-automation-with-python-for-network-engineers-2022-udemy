﻿1
00:00:01,370 --> 00:00:08,760
‫Almost any Python application brings out some kind of output that is in most cases available is a string

2
00:00:08,780 --> 00:00:09,420
‫variable.

3
00:00:09,860 --> 00:00:14,330
‫I want to show you one more time in a nutshell, what are the three main possibilities?

4
00:00:14,330 --> 00:00:21,830
‫We have to print strings and to format and print other variables that are not strings like, say, Flock's

5
00:00:21,830 --> 00:00:22,520
‫or inks.

6
00:00:23,210 --> 00:00:30,280
‫The recommended Pythonic Way is to use if string literals, which have been introduced in Python three,

7
00:00:30,290 --> 00:00:30,890
‫not six.

8
00:00:31,760 --> 00:00:38,030
‫Unfortunately, if you are using an older version of Python, you cannot use f string literals.

9
00:00:38,630 --> 00:00:39,920
‫Lexia an example.

10
00:00:40,040 --> 00:00:45,360
‫I'll use bicarb, but you are free to test it in Python Idel if you prefer.

11
00:00:46,130 --> 00:00:49,370
‫I'll create two variables A and B.

12
00:00:50,910 --> 00:00:59,310
‫A stauss afloat and B stores and Intacta and our print F and the pair of single quotes.

13
00:00:59,990 --> 00:01:11,730
‫This is an F string literal A, E and A between a pair of curly braces and B's and the B variable enclosed

14
00:01:11,730 --> 00:01:18,960
‫by a pair of curly braces a times B is a starboy.

15
00:01:21,240 --> 00:01:23,370
‫Now, I will run the script.

16
00:01:25,150 --> 00:01:33,910
‫And we can see how it printed out the desired string, if I want to display the result of A times B

17
00:01:34,120 --> 00:01:43,260
‫with a specific number of decimals, I can write inside the pair of curly braces, colon, dot and let's

18
00:01:43,270 --> 00:01:49,360
‫say three, if I am displaying it as a float with three decimals.

19
00:01:50,600 --> 00:01:52,910
‫Now, let's see the format method.

20
00:01:54,060 --> 00:02:07,170
‫A ease and a pair of curly braces, BS and another pair of curly braces and A times B is and the last

21
00:02:07,170 --> 00:02:11,370
‫the third pair of curly braces dot format.

22
00:02:11,830 --> 00:02:20,280
‫Now, the first argument, which is a the second argument is B and the third argument is A multiplied

23
00:02:20,280 --> 00:02:20,760
‫by B.

24
00:02:24,290 --> 00:02:29,990
‫And this is the output, it's the same output, of course, I can write here.

25
00:02:30,250 --> 00:02:38,210
‫Zero, this is the first argument won the second argument and to the third argument.

26
00:02:39,120 --> 00:02:47,730
‫If I want to set the number of decimals it displays here, I write Colen Dot for F.

27
00:02:48,980 --> 00:02:54,020
‫So the number will be displayed as a float with four decimals.

28
00:02:57,380 --> 00:03:04,940
‫OK, now I'll show you the last possibility we have to format and display strings, but this is not

29
00:03:04,940 --> 00:03:12,440
‫recommended because we don't have the flexibility given by if string literally or format method.

30
00:03:13,280 --> 00:03:21,170
‫Anyway, in this course, if I just want to simply print the value of a variable, I will also use this

31
00:03:21,170 --> 00:03:21,770
‫method.

32
00:03:22,080 --> 00:03:33,200
‫I'll simply write print and between single or double quotes, a is comma, a comma, then another string

33
00:03:33,200 --> 00:03:35,990
‫enclosed by single or double quotes.

34
00:03:36,410 --> 00:03:49,760
‫So B is comma be comma and the last string and A time B is comma A multiplied by B.

35
00:03:52,290 --> 00:04:00,240
‫And we've got the same result anyway, just remember that you should use if String Clitterhouse when

36
00:04:00,240 --> 00:04:03,900
‫possible, if not the format method.


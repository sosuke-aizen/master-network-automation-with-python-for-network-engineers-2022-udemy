﻿1
00:00:00,590 --> 00:00:06,290
‫In this lecture, we'll take a look at typecasting or converting data types.

2
00:00:06,920 --> 00:00:14,720
‫We'll see how we can turn an integer into a float, a float into an integer, a string into a float

3
00:00:14,720 --> 00:00:16,160
‫or into an integer.

4
00:00:16,550 --> 00:00:18,280
‫This is straightforward.

5
00:00:18,290 --> 00:00:26,120
‫In fact, you can explicitly convert the variables by using the name of the built in type is a function.

6
00:00:27,210 --> 00:00:29,430
‫Let me show you some examples.

7
00:00:30,550 --> 00:00:39,850
‫I'm creating a variable called A that stauss an integer, a variable called B that stauss afloat, and

8
00:00:39,850 --> 00:00:43,830
‫a variable called C that starts a string four.

9
00:00:43,840 --> 00:00:46,770
‫For that, I'm going to use the input function.

10
00:00:46,900 --> 00:00:49,960
‫So input and ther any value.

11
00:00:51,990 --> 00:00:54,930
‫And I'm entering one point five.

12
00:00:56,970 --> 00:01:06,930
‫A star son, Texas, to be stauss afloat and see stars a string, not a float.

13
00:01:07,890 --> 00:01:15,200
‫Now let's see what happens if I try to float off a it displayed a float value.

14
00:01:15,540 --> 00:01:22,230
‫In fact, it converted the value stored by a which is an integer to float.

15
00:01:23,760 --> 00:01:27,730
‫A hasn't been modified, a remained an integer.

16
00:01:28,050 --> 00:01:33,490
‫The float function only returned or created a new float object.

17
00:01:33,780 --> 00:01:37,160
‫So if I want, I can take that object in another variable.

18
00:01:37,170 --> 00:01:51,120
‫Let's say a equals float away and the B B equals Int of B, so B B stauss an integer.

19
00:01:53,120 --> 00:02:02,420
‫The same happens for string variables so I can try to underline ain't equals Int of C.

20
00:02:07,230 --> 00:02:14,450
‫And here we've got an error, it couldn't convert the one point five string to an integer.

21
00:02:15,180 --> 00:02:20,690
‫If I want to convert it to an integer, I must first converted to a float.

22
00:02:21,030 --> 00:02:26,610
‫So let's try to see underlying float equals float of C.

23
00:02:28,310 --> 00:02:37,900
‫See stories, a string value, one point five, a string between single quotes and see underlying float

24
00:02:38,080 --> 00:02:39,790
‫stories of float value.

25
00:02:40,860 --> 00:02:52,960
‫And now I can see underlying end equals in of the underline float and C underline int stauss an interval.

26
00:02:53,610 --> 00:02:56,490
‫I could also have written something like this.

27
00:02:58,330 --> 00:03:05,830
‫See, underlying it equals int of float off C, it's the same.

28
00:03:07,790 --> 00:03:15,320
‫Of course, we can't always convert strength to numbers if I have something like this, let's say name

29
00:03:15,470 --> 00:03:25,390
‫equals a and I want end of name or float off name, I've got an error.

30
00:03:25,580 --> 00:03:30,440
‫It cannot convert a to an integer or to a float.

31
00:03:33,790 --> 00:03:42,580
‫If we want to perform arithmetic operations with values stored in variables created using the input

32
00:03:42,580 --> 00:03:47,060
‫function, we must first cast them to a numeric value.

33
00:03:47,410 --> 00:03:48,660
‫Let's try an example.

34
00:03:49,030 --> 00:03:55,510
‫Let's try to convert miles to kilometers so miles equals input.

35
00:03:56,290 --> 00:03:58,720
‫Enter the number of miles.

36
00:04:00,820 --> 00:04:10,510
‫Let's say then, of course, Miles stauss a string, not an integer or a float, and now kilometers

37
00:04:10,510 --> 00:04:17,830
‫equals Miles Pym's one point six zero nine three four.

38
00:04:17,980 --> 00:04:19,500
‫And I'll get on there.

39
00:04:19,720 --> 00:04:21,610
‫I've got a pipe error.

40
00:04:22,030 --> 00:04:24,770
‫I cannot multiply numbers.

41
00:04:24,880 --> 00:04:27,720
‫This is a number by strings.

42
00:04:27,790 --> 00:04:28,820
‫This is a string.

43
00:04:29,290 --> 00:04:41,050
‫So if I want to calculate the number of kilometers, I must write something like this int of miles multiplied

44
00:04:41,050 --> 00:04:44,530
‫by one point six zero nine three four.

45
00:04:46,620 --> 00:04:55,980
‫And I've got the right answer, I could also do something like this here, instead of using only the

46
00:04:55,980 --> 00:05:03,210
‫input function, I could simply use float off and then the input function.

47
00:05:03,870 --> 00:05:12,840
‫And now the Mila's variable stauss afloat five point four type of miles.

48
00:05:13,910 --> 00:05:23,630
‫And it's afloat and now kilometers equals miles multiplied by one point six zero nine three four.

49
00:05:25,050 --> 00:05:26,700
‫And the fix much better.

50
00:05:28,210 --> 00:05:36,000
‫I take this opportunity to show you why you should never use Python reserved words as variable names,

51
00:05:36,370 --> 00:05:47,270
‫let's create a variable called a star equals ABC and another variable called C equals seven.

52
00:05:47,590 --> 00:05:51,450
‫And now I want to cast C to a string.

53
00:05:51,490 --> 00:05:56,690
‫We cast a numeric value to a string using the SDR function.

54
00:05:57,220 --> 00:05:59,230
‫This line of code is correct.

55
00:05:59,260 --> 00:06:00,130
‫There is no error.

56
00:06:00,370 --> 00:06:09,820
‫But I've got an error viks because I have overwritten the keyword called SDR and this is not permitted

57
00:06:09,940 --> 00:06:10,470
‫here.

58
00:06:10,600 --> 00:06:16,090
‫I should have used a one or another name for the string.


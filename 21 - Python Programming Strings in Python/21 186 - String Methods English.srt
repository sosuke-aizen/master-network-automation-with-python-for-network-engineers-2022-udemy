﻿1
00:00:00,980 --> 00:00:08,270
‫Now that we are all done with string slicing and indexing, let's continue on by discussing about some

2
00:00:08,270 --> 00:00:16,490
‫builtin string methods, objects in Python, no matter of a store, integers, floats, leaks or strings

3
00:00:16,610 --> 00:00:18,330
‫have built-In methods.

4
00:00:18,350 --> 00:00:23,550
‫These methods are essentially functions attached to that object.

5
00:00:24,200 --> 00:00:30,650
‫Later on, we'll see how can we create our own functions and our own methods.

6
00:00:31,500 --> 00:00:37,850
‫Now, I need to do a little bit of explaining, just to be sure that you understand that functions and

7
00:00:37,850 --> 00:00:41,680
‫methods are not exactly the same thing in Python.

8
00:00:42,710 --> 00:00:46,760
‫We've already used some built-In functions of this point.

9
00:00:47,700 --> 00:00:56,550
‫The print function or katelin function, for example, these functions are already defined in Python

10
00:00:56,550 --> 00:01:00,710
‫core and we are just using them by calling the function.

11
00:01:01,440 --> 00:01:09,840
‫When I say calling or executing the function, I mean using its name, parentheses and optionally some

12
00:01:09,840 --> 00:01:14,580
‫arguments written between parentheses with comma between them.

13
00:01:16,640 --> 00:01:21,950
‫You can see a list with all Python built in functions at this address.

14
00:01:23,420 --> 00:01:31,000
‫When we have a function and we attach that function to an object, let's say a string object, that

15
00:01:31,020 --> 00:01:34,650
‫function becomes a method, a string method.

16
00:01:35,000 --> 00:01:39,260
‫These methods are also built in or predefined.

17
00:01:40,520 --> 00:01:48,620
‫Working with strings is very common, so let's see some very useful methods first, let's create a variable

18
00:01:48,620 --> 00:01:55,220
‫that starts a string, let's say simply X equals I learned Python programming.

19
00:01:57,110 --> 00:02:05,540
‫If we want to see all strange methods, we write a string literal or a variable that starts a string

20
00:02:05,870 --> 00:02:16,790
‫and a dot, for example, X dot and now the auto completion function of Idel or bicarb will display

21
00:02:16,790 --> 00:02:18,750
‫all available methods.

22
00:02:19,190 --> 00:02:23,240
‫This is possible for any other type of object.

23
00:02:23,720 --> 00:02:28,370
‫If I have, let's say L1, this is a list and I write L1 DOT.

24
00:02:28,850 --> 00:02:33,830
‫The auto completion feature is displaying all liste menfolks.

25
00:02:36,530 --> 00:02:45,390
‫We can also use the deer builtin function to display all methods of a string so deer and we perceive

26
00:02:45,440 --> 00:02:56,300
‫of a type of object as an argument, and it has displayed all string methoxy deer in it and it has displayed

27
00:02:56,510 --> 00:03:04,820
‫all integer methods, the list and it has displayed all alist methods and so on.

28
00:03:06,020 --> 00:03:15,170
‫If we want to see a short help of a method, we use the built in help function, so help and between

29
00:03:15,170 --> 00:03:23,060
‫parentheses, the type of the object SDR dot and now the name of the method, let's say find.

30
00:03:23,980 --> 00:03:28,390
‫And it has displayed a short help of the find method.

31
00:03:28,990 --> 00:03:37,510
‫This is how we call the method the string dot, the name of the method, and these are the arguments.

32
00:03:38,110 --> 00:03:42,600
‫What is between square brackets are optional arguments.

33
00:03:43,180 --> 00:03:50,620
‫So we have a mandatory argument and then two optional arguments, a start and an end.

34
00:03:51,040 --> 00:03:53,260
‫And this is a short summary.

35
00:03:54,340 --> 00:03:57,950
‫The first method will take a look at these opper.

36
00:03:58,300 --> 00:04:03,610
‫This method will be a new string with all later capitalized.

37
00:04:04,840 --> 00:04:14,470
‫So X dot doorstopper, the opening and the closing parentheses, similarly, the lour method returns

38
00:04:14,470 --> 00:04:20,080
‫a new string only with lowercase letters X dot lower.

39
00:04:22,490 --> 00:04:30,110
‫Keep in mind that all strange methods written and new string and don't modify the original string,

40
00:04:30,470 --> 00:04:35,960
‫a string is an immutable variable and cannot be modified.

41
00:04:36,200 --> 00:04:43,510
‫Even if we've just applied the law and the operative methods, the initial string is still the same.

42
00:04:43,790 --> 00:04:45,800
‫No changes have been made.

43
00:04:46,800 --> 00:04:55,980
‫Another simple method is strip, it eliminates all white spaces from the start and the end of a string,

44
00:04:56,790 --> 00:04:57,840
‫let's see an example.

45
00:04:58,170 --> 00:05:02,070
‫Let's say you have one string that contains an IP address.

46
00:05:07,960 --> 00:05:17,380
‫This is my strength, but I have two white spaces before the IP address and the three spaces after the

47
00:05:17,380 --> 00:05:27,160
‫IP address and our IP dot and strip, and it returned the string, the IP address without any spaces

48
00:05:27,160 --> 00:05:30,760
‫at the beginning and at the end of the string.

49
00:05:31,810 --> 00:05:40,120
‫The strip method can remove also other characters from the beginning and the end of the string, let's

50
00:05:40,120 --> 00:05:49,270
‫create another string, let's say it's Darwan equals and here are two dollar signs and an IP address,

51
00:05:49,660 --> 00:05:54,430
‫something like this, and three dollar signs characters.

52
00:05:55,970 --> 00:06:04,910
‫This is my strength, if I call the strip Method Isti one dot strip, it will return the same string

53
00:06:05,420 --> 00:06:08,720
‫because it removes only white spaces.

54
00:06:09,080 --> 00:06:18,080
‫But if I want to remove another character, I call the function with an optional argument so strong

55
00:06:18,080 --> 00:06:26,420
‫that strip off and here between single or double keryx, the character I want to remove from the beginning

56
00:06:26,450 --> 00:06:28,760
‫and from the end of the string.

57
00:06:29,690 --> 00:06:37,670
‫And it returned the string without dollar signs at the beginning and at the end of the string, keep

58
00:06:37,670 --> 00:06:40,670
‫in mind that these can be useful sometimes.

59
00:06:42,540 --> 00:06:51,420
‫Another useful method is replace it replaces a character or a substring with another character or substring.

60
00:06:53,070 --> 00:06:57,530
‫So let's say IP equals IP dot strip.

61
00:06:58,980 --> 00:07:07,650
‫I've stripped all white spaces, characters, and now I want to replace the tooth signed with another

62
00:07:07,650 --> 00:07:08,370
‫character.

63
00:07:09,560 --> 00:07:19,030
‫I don't replace the first argument is the substring I want to replace and I want to replace the DOT

64
00:07:19,220 --> 00:07:25,550
‫character, I'm replacing it with a colon and that's the new string.

65
00:07:26,150 --> 00:07:30,110
‫Of course, the original string hasn't been modified.

66
00:07:30,260 --> 00:07:32,900
‫It has just returned a new string.

67
00:07:34,250 --> 00:07:35,810
‫This is another example.

68
00:07:39,180 --> 00:07:46,830
‫Now, let's take a look at the split method, it returns a list from a string having a separator, which

69
00:07:46,830 --> 00:07:49,840
‫is by default a whitespace or a tab.

70
00:07:50,250 --> 00:07:57,370
‫I have this string, let's say a star one equals and this is a part of a Linux command.

71
00:07:57,690 --> 00:08:04,230
‫So this is the name of an interface, the encapsulation and smok address.

72
00:08:04,830 --> 00:08:07,590
‫And I write Astron that split.

73
00:08:10,070 --> 00:08:17,130
‫We can see how it has split the string using the white space or the TARP as a separator.

74
00:08:17,450 --> 00:08:21,620
‫In fact, it returned at least with five elements.

75
00:08:21,980 --> 00:08:23,330
‫This is the first element.

76
00:08:23,510 --> 00:08:30,710
‫The second element, the third element, the fourth element and the last element.

77
00:08:32,050 --> 00:08:41,920
‫Of course, here I can use any separator I want if I want to split the string using the colon here I

78
00:08:41,920 --> 00:08:49,060
‫am, passing in the colon is an argument and it will split based on this character.

79
00:08:50,750 --> 00:08:59,330
‫OK, this is the first element, the second element, the third element and so on.

80
00:09:00,450 --> 00:09:08,760
‫Another very useful method is join, it is somehow like the opposite of split, if we have a sequence

81
00:09:08,760 --> 00:09:16,260
‫like a string or at least we can join each element of that sequence in another, a new string, having

82
00:09:16,260 --> 00:09:17,260
‫a separate their.

83
00:09:18,430 --> 00:09:26,890
‫I create another variable, Mac equals and now a Mac address, I'll copy paste this value from here.

84
00:09:28,920 --> 00:09:33,480
‫And I do my those split and Colen is the argument.

85
00:09:34,530 --> 00:09:36,180
‫So here I have a split.

86
00:09:39,620 --> 00:09:48,320
‫This is the least I can take the value of, at least in a variable, so let's say L1 equals MC dot split.

87
00:09:49,960 --> 00:09:57,460
‫And now I'm going to call the joint method first, I have a separator, let's say the new separator

88
00:09:57,490 --> 00:09:58,510
‫is DOT.

89
00:10:00,040 --> 00:10:02,260
‫That joint of Alan.

90
00:10:03,240 --> 00:10:07,050
‫And it returned a string from early.

91
00:10:08,990 --> 00:10:13,670
‫Of course, instead of the DOT character, I can have an exclamation mark.

92
00:10:15,390 --> 00:10:24,240
‫And it has joined the elements of this list, using the exclamation mark is a delimiter or as a separate

93
00:10:24,240 --> 00:10:24,470
‫the.

94
00:10:26,090 --> 00:10:34,170
‫And finally, if we want to test, if a substring belongs to a string, we use the in operator.

95
00:10:34,340 --> 00:10:37,250
‫This is a boolean operator that returns.

96
00:10:37,250 --> 00:10:38,480
‫True or false?

97
00:10:39,830 --> 00:10:44,030
‫For example, Python in X and the to.

98
00:10:45,490 --> 00:10:48,820
‫If I write Python with DeBelin here.

99
00:10:49,870 --> 00:10:57,640
‫It will return false because Python returned with double in at the end, doesn't belong to the string.

100
00:10:57,670 --> 00:10:59,060
‫It's not a substring.

101
00:10:59,620 --> 00:11:02,710
‫We also have the not in operator.

102
00:11:02,710 --> 00:11:06,880
‫So Python written with DoubleLine, not in X..

103
00:11:07,300 --> 00:11:16,180
‫It will return true, but Python with a single end at the end, not in X will return false.

104
00:11:17,050 --> 00:11:21,730
‫These are only a few string methods that are used more frequently.

105
00:11:22,720 --> 00:11:29,620
‫You can find a comprehensive list of string methods at the link I've attached to this lecture.


﻿1
00:00:00,640 --> 00:00:08,530
‫Hey, guys, now we'll start talking about strings, a string is an order, the sequence of characters,

2
00:00:08,830 --> 00:00:16,810
‫it can contain any Unicode character, meaning letters, numbers, white spaces, special characters

3
00:00:16,810 --> 00:00:21,630
‫like the question mark, the dollar sign or even Chinese characters.

4
00:00:21,850 --> 00:00:25,350
‫In fact, Unicode is an ASCII superset.

5
00:00:25,670 --> 00:00:30,340
‫They are more than one hundred thirty thousand Unicode characters.

6
00:00:31,240 --> 00:00:35,080
‫Now let's create the variable that stauss a string value.

7
00:00:35,290 --> 00:00:38,680
‫For that, we can use single or double codes.

8
00:00:39,960 --> 00:00:45,810
‫My Darwan equals and between single quotes I write, I learn python.

9
00:00:47,250 --> 00:00:57,480
‫This is a strange variable or an object of type SDR, I can also use double codes like, say, my ETR

10
00:00:57,480 --> 00:01:00,350
‫two equals I learn Python.

11
00:01:00,750 --> 00:01:04,920
‫There is no difference between using single or double codes.

12
00:01:06,170 --> 00:01:11,840
‫What is important to you is to try to remain consistent within your own code.

13
00:01:12,050 --> 00:01:15,740
‫For example, I use a mainly single code.

14
00:01:16,910 --> 00:01:25,070
‫If we want to display a string at the console, we use the built in print function in Python Eitel,

15
00:01:25,280 --> 00:01:33,110
‫when we write the name of the variable, it automatically displays eat, putting bicarb or another ID

16
00:01:33,260 --> 00:01:39,740
‫we can display only using the print function if I write the name of the variable.

17
00:01:39,750 --> 00:01:44,270
‫So my on it will automatically display its content.

18
00:01:44,780 --> 00:01:51,020
‫Of course I can also use the print function in bicarb or another ID.

19
00:01:51,170 --> 00:01:57,320
‫We must use the print function if we want to display the content of a string variable.

20
00:02:06,040 --> 00:02:13,180
‫Now, I'll show you how to use cukes inside coats, let's say I want a viable hello equals.

21
00:02:13,390 --> 00:02:14,110
‫Hi there.

22
00:02:14,140 --> 00:02:14,800
‫I'm Andre.

23
00:02:20,910 --> 00:02:30,680
‫This is an error, I've got an invalid syntax viks, because I can't use Cotes inside codes or at least

24
00:02:30,690 --> 00:02:33,360
‫single codes inside single codes.

25
00:02:34,690 --> 00:02:38,510
‫There are two possibilities to have codes inside codes.

26
00:02:39,070 --> 00:02:46,490
‫We use a backslash before the single code character or we define our string using double codes.

27
00:02:46,870 --> 00:02:54,640
‫In fact, we can use single codes inside double codes and double codes inside single codes.

28
00:02:55,700 --> 00:02:59,870
‫So I can have something like this, a backslash character here.

29
00:03:01,990 --> 00:03:02,890
‫Ampex, OK?

30
00:03:04,320 --> 00:03:10,050
‫Or another possibility is to use double Cokes instead of a single Coke.

31
00:03:11,840 --> 00:03:16,380
‫And inside the double Cokes, I can use single Cokes without problem.

32
00:03:17,060 --> 00:03:25,460
‫This is another possibility, or we can use double Cokes inside the single Cokes, something like this.

33
00:03:32,800 --> 00:03:40,990
‫There are also triple Cokes that are used mainly for creating dock's drinks, which is a way of documenting

34
00:03:40,990 --> 00:03:46,630
‫classes and functions, we'll see later how to create and use those drinks.

35
00:03:47,080 --> 00:03:55,000
‫Triple Cokes can also be used to comment out more lines if we don't want to use a hash sign for each

36
00:03:55,000 --> 00:04:03,340
‫line anyway, Pepé recommends using a hash sign for each comment line and not using people.

37
00:04:04,390 --> 00:04:14,230
‫Let's try an example here in Pike, Shaam, let's say A equals six, B equals seven, and I'll print

38
00:04:14,230 --> 00:04:16,420
‫A plus B equals.

39
00:04:16,750 --> 00:04:25,690
‫And after the ending single code, I use a comma and A plus B, I'm running the script.

40
00:04:26,740 --> 00:04:36,880
‫It displayed A plus B equals 13, and now I want to comment about these lines, the recommended way

41
00:04:36,880 --> 00:04:39,790
‫is to use a hash character for each line.

42
00:04:39,940 --> 00:04:41,860
‫So this is the recommended way.

43
00:04:43,050 --> 00:04:51,690
‫But if I don't like using a harsh character for each line, I can simply enclose my comment out text

44
00:04:51,930 --> 00:05:00,330
‫or instructions between people, coax the one to three and hear at the end of the commented lines.

45
00:05:00,540 --> 00:05:02,130
‫One more time, people.

46
00:05:03,300 --> 00:05:04,170
‫And here I write.

47
00:05:04,170 --> 00:05:06,440
‫Print something here.

48
00:05:07,270 --> 00:05:15,910
‫And I'm running the script, we can see it has only displayed something here because these lines are

49
00:05:15,910 --> 00:05:16,870
‫commented out.

50
00:05:18,440 --> 00:05:26,690
‫And lastly, we use triple quotes if we want to enter a string on multiple lines, let's create this

51
00:05:26,690 --> 00:05:30,350
‫thing here as one equals one, two, three.

52
00:05:30,890 --> 00:05:32,780
‫And now I learn.

53
00:05:33,290 --> 00:05:37,790
‫Enter Python, enter Java and go.

54
00:05:38,770 --> 00:05:40,840
‫One, two, three, enter.

55
00:05:42,050 --> 00:05:49,190
‫This is a string, if I write is one impressive, the enter key, we can see our string.

56
00:05:50,280 --> 00:05:51,420
‫Print is one.

57
00:05:53,110 --> 00:05:59,620
‫This is my string on multiple lines, but it seems we have something strange here.

58
00:05:59,890 --> 00:06:02,530
‫What are these backslash and.

59
00:06:04,200 --> 00:06:13,080
‫Backslash M is called an escape sequence and is an ASCII code for a new line that is being created,

60
00:06:13,080 --> 00:06:18,200
‫a pivot point in the string is in fact the enter key.

61
00:06:18,240 --> 00:06:20,340
‫We've pressed a pivot point.

62
00:06:21,240 --> 00:06:28,230
‫The print function interprets and displays backslash Leshan is a new line of X Y.

63
00:06:28,260 --> 00:06:37,340
‫We see our string on multiple lines when we print it using the print function like the other examples.

64
00:06:37,830 --> 00:06:42,570
‫Hello equals hello backslash n World.

65
00:06:44,650 --> 00:06:52,240
‫And hello, we can see the pixelation, but when I printed the content of the string using the print

66
00:06:52,240 --> 00:06:53,680
‫function, so print.

67
00:06:53,680 --> 00:06:54,190
‫Hello?

68
00:06:55,330 --> 00:07:04,570
‫I'll seek a new line here, a single backslash character is used to escape characters that otherwise

69
00:07:04,570 --> 00:07:12,710
‫have a special meaning to Python, such as a new line backslash itself or the, quote, character.

70
00:07:13,450 --> 00:07:18,790
‫So, for example, let's suppose I want a variable called the info.

71
00:07:19,300 --> 00:07:26,950
‫And between single codes, I want something like this backslash and means a new line.

72
00:07:28,230 --> 00:07:30,690
‫Enter and I'll print info.

73
00:07:32,170 --> 00:07:41,830
‫And there is no backslash and because print interpreted backslash end is a new line, so as an enter

74
00:07:41,830 --> 00:07:46,630
‫key price here, how can I display backslash?

75
00:07:46,660 --> 00:07:53,270
‫And literally for that I use a backslash character is an escape.

76
00:07:53,290 --> 00:07:57,130
‫So here I write one more backslash character.

77
00:07:57,430 --> 00:08:04,990
‫It means it will cancel the special meaning of the character that follows in this case backslash and

78
00:08:05,020 --> 00:08:07,120
‫won't have a special meaning.

79
00:08:07,930 --> 00:08:09,630
‫And I'll print info.

80
00:08:12,190 --> 00:08:14,380
‫Backslash and means a new line.

81
00:08:15,380 --> 00:08:23,300
‫I can also have something like this, so pretend I'm Andre, as we've seen earlier, this is an error,

82
00:08:23,420 --> 00:08:30,200
‫but I can escape this single code using a backslash and now everything's fine.

83
00:08:31,140 --> 00:08:33,200
‫It will work like a charm.

84
00:08:34,820 --> 00:08:42,320
‫So remember, backslash cancels the special meaning of the character that follows.

85
00:08:43,600 --> 00:08:51,100
‫That's all for the moment about the basics, and in the next lecture, we'll see how can we get the

86
00:08:51,100 --> 00:08:54,190
‫user input in Python applications?


﻿1
00:00:00,510 --> 00:00:07,500
‫The vast majority of applications work with strings, there are several ways to format a given string

2
00:00:07,500 --> 00:00:15,060
‫into a nicer output in Python, the new way of formatting strings, which is new in Python three dot

3
00:00:15,060 --> 00:00:23,040
‫six, is to use the formatted string literal or F strings, which is a string literal that is prefixed

4
00:00:23,040 --> 00:00:26,970
‫with the upper case or lower case letter F.

5
00:00:28,090 --> 00:00:36,130
‫These strings may contain replacement Feliks, which are expressions delimited by pairs of curly braces,

6
00:00:37,000 --> 00:00:44,980
‫while other string literalists always have a constant value, formatted strings are really expressions

7
00:00:44,980 --> 00:00:46,780
‫evaluated at runtime.

8
00:00:47,410 --> 00:00:52,420
‫The part of the string outside curly braces is treated literally.

9
00:00:52,810 --> 00:01:00,250
‫A single opening curly brace marks a replacement field, which starts with a python expression.

10
00:01:00,610 --> 00:01:02,200
‫Between curly braces.

11
00:01:02,210 --> 00:01:10,030
‫There can be arithmetic expressions, a conversion field like casting from a string to an integer or

12
00:01:10,060 --> 00:01:18,910
‫a format specified that is appended using a call on the replacement field ends with a closing curly

13
00:01:18,910 --> 00:01:19,400
‫bracket.

14
00:01:20,020 --> 00:01:21,700
‫Let's see an example.

15
00:01:22,960 --> 00:01:28,510
‫I'll define a float variable, so price equals three point five.

16
00:01:29,850 --> 00:01:30,990
‫And quantity.

17
00:01:32,030 --> 00:01:41,060
‫Equals, let's say, two point eight an hour, if this means that a string literal follows and between

18
00:01:41,060 --> 00:01:42,760
‫single or double clicks.

19
00:01:43,280 --> 00:01:47,900
‫All right, the price is now a pair of curly braces.

20
00:01:48,410 --> 00:01:52,780
‫And between curly braces, I enter the name of my variable.

21
00:01:52,790 --> 00:01:53,480
‫So price.

22
00:01:54,750 --> 00:01:59,160
‫And it displayed the price is three point five.

23
00:01:59,970 --> 00:02:03,210
‫Another example would be total value.

24
00:02:05,280 --> 00:02:14,340
‫And between curly braces, I right price multiplied by quantity, we can have arithmetic expressions

25
00:02:14,340 --> 00:02:15,870
‫between curly braces.

26
00:02:18,570 --> 00:02:21,090
‫And it displayed the total value.

27
00:02:22,190 --> 00:02:31,760
‫We can also cast insight if things like, say, price equals and here, let's write four point two.

28
00:02:32,130 --> 00:02:36,440
‫This is a string because I've written between single quotes.

29
00:02:39,080 --> 00:02:49,220
‫If price is a string, I must cast it to a float so he our float off price and it calculated and displayed

30
00:02:49,400 --> 00:02:50,820
‫a numeric value.

31
00:02:51,680 --> 00:02:59,510
‫Another way of formatting strings is to use the format, the method, but better let's see an example.

32
00:03:00,230 --> 00:03:03,970
‫So Miles equals twenty three point two.

33
00:03:04,130 --> 00:03:05,600
‫And this is a string.

34
00:03:06,470 --> 00:03:15,110
‫The value in miles is a pair of curly brackets that format and minus between parentheses.

35
00:03:16,290 --> 00:03:25,110
‫And it has displayed the correct value, the format or template string contains replacement Felix surrounded

36
00:03:25,110 --> 00:03:26,300
‫by curly braces.

37
00:03:26,640 --> 00:03:34,150
‫Anything that is not contained in braces is considered the literal text, which is copied unchanged

38
00:03:34,150 --> 00:03:34,980
‫to the output.

39
00:03:36,060 --> 00:03:42,030
‫If I use an ID like charm, I must point to the expression in order to display it.

40
00:03:43,340 --> 00:03:50,930
‫Or I can take the output in a variable in a string variable like, say, as equals.

41
00:03:52,870 --> 00:04:01,270
‫The pair of curly braces inside the string template, this is the string template has been mapped with

42
00:04:01,270 --> 00:04:04,990
‫the value inside the parentheses of the format, the method.

43
00:04:05,970 --> 00:04:10,230
‫We can also have more pairs of curly braces like these.

44
00:04:11,640 --> 00:04:21,390
‫The value the first pair of curly braces in kilometers is and the second pair of curly braces.

45
00:04:23,160 --> 00:04:30,000
‫And now for more Miles, and the second argument, these are wireless times, one daughter, six zero

46
00:04:30,000 --> 00:04:31,200
‫nine three four.

47
00:04:32,900 --> 00:04:40,610
‫Again, each pair of curly braces inside the string template has been mapped with each value inside

48
00:04:40,610 --> 00:04:43,340
‫the parentheses of the format method.

49
00:04:44,240 --> 00:04:52,370
‫This pair of curly braces has been mapped with this variable or with the value stored in these variable.

50
00:04:52,610 --> 00:04:57,890
‫And this bear has been mapped with the result of this expression.

51
00:04:58,340 --> 00:05:07,720
‫The curly braces are just placeholders for the arguments to be placed inside the curly brace bear.

52
00:05:07,760 --> 00:05:11,110
‫We can use positional arguments starting from zero.

53
00:05:11,480 --> 00:05:15,430
‫So the same example could be written in this way.

54
00:05:15,800 --> 00:05:20,420
‫Here, I'll write zero and here I write one.

55
00:05:21,390 --> 00:05:27,140
‫This is the argument from position zero and this is the argument from position one.

56
00:05:27,590 --> 00:05:29,660
‫And I've got the same result.

57
00:05:29,840 --> 00:05:36,080
‫As I said, the curly braces are just placeholders for the arguments to be placed.

58
00:05:36,590 --> 00:05:45,860
‫In the above example, zero between curly braces is a placeholder for miles and argument one or one

59
00:05:45,860 --> 00:05:52,430
‫between curly braces is a placeholder for the result of the arithmetic expression.

60
00:05:53,210 --> 00:05:59,840
‫This expression from here we can also format the representation of numbers.

61
00:06:00,140 --> 00:06:04,760
‫Let me show you how I'll execute this instruction one more time.

62
00:06:05,030 --> 00:06:16,160
‫But here I'll use a colon dot and let's say two if and it displayed this value instead of this value

63
00:06:16,370 --> 00:06:26,180
‫with this large number of decimal points, look at the second curly brace pair after one, which is

64
00:06:26,180 --> 00:06:27,590
‫the positional argument.

65
00:06:27,590 --> 00:06:33,380
‫I've used a column F specifies the format is dealing with.

66
00:06:33,380 --> 00:06:34,670
‫And the VIX a float?

67
00:06:34,670 --> 00:06:35,150
‫No.

68
00:06:36,070 --> 00:06:42,910
‫If not correctly specified, it will give you an error and the value after the DOT character indicates

69
00:06:42,910 --> 00:06:50,980
‫the number of decimal points it will display if I write here four, it will display it with four decimals.

70
00:06:53,110 --> 00:06:56,720
‫It's not mandatory to have the arguments in this order.

71
00:06:57,250 --> 00:07:00,250
‫I can have here one and here zero.

72
00:07:03,090 --> 00:07:13,260
‫This argument has been met with this value and this argument zero has been met with Miles, of course,

73
00:07:13,270 --> 00:07:14,460
‫this is not correct.

74
00:07:14,700 --> 00:07:16,740
‫I just wanted to make a point.

75
00:07:16,980 --> 00:07:21,420
‫I'll give you one last example just to be sure that you have understood.

76
00:07:23,820 --> 00:07:24,390
‫Zero.

77
00:07:25,680 --> 00:07:26,250
‫Zero.

78
00:07:27,700 --> 00:07:39,940
‫One, three and two, I have five placeholders format five five multiplied by two, five multiplied

79
00:07:39,940 --> 00:07:42,070
‫by three and one hundred.

80
00:07:44,350 --> 00:07:52,540
‫As you can see, this is the first argument and has been put here and here, the second argument or

81
00:07:52,540 --> 00:07:54,760
‫the argument with index one.

82
00:07:56,620 --> 00:07:57,340
‫Next here.

83
00:07:58,280 --> 00:07:59,750
‫Argument three.

84
00:08:01,040 --> 00:08:05,540
‫This is one hundred and hear argument with index to.

85
00:08:06,600 --> 00:08:09,990
‫So five times three and the next 15.


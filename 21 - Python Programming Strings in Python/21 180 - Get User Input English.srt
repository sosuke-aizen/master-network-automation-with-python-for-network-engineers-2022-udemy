﻿1
00:00:00,770 --> 00:00:06,980
‫Now that we are all done with things, basics, let's take a look at how to get user input.

2
00:00:07,370 --> 00:00:10,790
‫There are hardly any programs without any input.

3
00:00:10,970 --> 00:00:14,570
‫In most cases, the input comes from the keyboard.

4
00:00:14,930 --> 00:00:18,650
‫Let me show you a simple and common example.

5
00:00:19,840 --> 00:00:28,680
‫Here I am connected using SSX to a Linux machine, and I want to uninstall an application form of it,

6
00:00:28,690 --> 00:00:40,990
‫I'm going to use the Apte package manager of Ubuntu Sudo Apte Remove and map, by the way, Apte and

7
00:00:40,990 --> 00:00:43,960
‫API to get our Python applications.

8
00:00:47,840 --> 00:00:58,660
‫And here the application is asking for the user input, I must enter why from yes or no from now?

9
00:00:58,970 --> 00:01:06,890
‫This is the point where the application is asking for the user input for the purpose of getting user

10
00:01:06,890 --> 00:01:07,310
‫input.

11
00:01:07,340 --> 00:01:12,110
‫Python provides a predefined building function named input.

12
00:01:12,350 --> 00:01:19,850
‫Input has an optional argument, which is the prompt think when the input function is called the program

13
00:01:19,850 --> 00:01:28,520
‫flow will be stopped until the user has given an input and has ended the input with the return or enter

14
00:01:28,520 --> 00:01:28,820
‫key.

15
00:01:29,760 --> 00:01:34,590
‫The optional argument, which is the prompt, will be printed on the screen.

16
00:01:35,190 --> 00:01:37,130
‫Let's see some examples.

17
00:01:46,530 --> 00:01:58,140
‫So name equals input and then print your name is on comma name and I'm running the script, as we can

18
00:01:58,140 --> 00:02:06,390
‫see, the script displayed the string, so the string printed on the first line and then it stopped

19
00:02:06,570 --> 00:02:09,300
‫and is waiting for my input.

20
00:02:09,510 --> 00:02:13,170
‫So now I'll write here like, say, Andre.

21
00:02:15,450 --> 00:02:20,060
‫And it has displayed your name is Andre, what happened?

22
00:02:21,150 --> 00:02:29,220
‫The thing I've entered has been stored in the name variable, and after that the print function displayed.

23
00:02:29,370 --> 00:02:35,850
‫Your name is Colin String concatenated with the value of the name variable.

24
00:02:36,450 --> 00:02:40,410
‫Now, let's try to write here something like this.

25
00:02:40,750 --> 00:02:42,750
‫Enter your name.

26
00:02:44,080 --> 00:02:47,650
‫This is the argument of the input function.

27
00:02:47,680 --> 00:02:54,490
‫In fact, when executing the instructions on line number two, so the input function, it will display

28
00:02:54,490 --> 00:02:57,380
‫as prompt at the console, enter your name.

29
00:02:57,550 --> 00:02:59,050
‫It has displayed the string.

30
00:02:59,050 --> 00:02:59,850
‫Enter your name.

31
00:03:00,460 --> 00:03:05,000
‫I am entering on three and it has displayed my name.

32
00:03:05,710 --> 00:03:12,170
‫I also want to see what is the type of the value stored by the name variable.

33
00:03:12,430 --> 00:03:19,270
‫So print and like say pipe, colon comma type of name.

34
00:03:20,280 --> 00:03:24,720
‫Lexy was the type starp in this variable one more time, Andre.

35
00:03:25,970 --> 00:03:30,710
‫And they can see each type is SDR so exhausting.

36
00:03:31,160 --> 00:03:40,640
‫Now let's see what happens if we try to perform arithmetic operations with variables created using the

37
00:03:40,640 --> 00:03:44,300
‫input function, I'll command out these lines.

38
00:03:44,690 --> 00:03:52,130
‫I am pressing control and forward slash and bicarb commented about all lines.

39
00:03:53,320 --> 00:04:01,930
‫And here, I'll try something else like, say, price equals input into the price and then quantity

40
00:04:01,930 --> 00:04:14,260
‫equals input into the quantity and I'll print and then total value equals price, Pym's quantity and

41
00:04:14,260 --> 00:04:17,290
‫now print total value.

42
00:04:17,530 --> 00:04:19,780
‫Colen Colma total value.

43
00:04:21,230 --> 00:04:29,960
‫And I'm writing the script and the price, let's say five, enter the quantity, like, say two, and

44
00:04:29,960 --> 00:04:30,970
‫I've gotten there.

45
00:04:31,130 --> 00:04:31,910
‫But why?

46
00:04:32,270 --> 00:04:35,180
‫Because everything looks just fine.

47
00:04:37,230 --> 00:04:46,350
‫I've got this error due to the fact that I've tried to multiply two things, this is a string and this

48
00:04:46,350 --> 00:04:47,080
‫is the string.

49
00:04:47,340 --> 00:04:48,680
‫It doesn't matter.

50
00:04:48,960 --> 00:04:51,310
‫I've entered an integer.

51
00:04:51,310 --> 00:04:54,540
‫I value it, considers it a string.

52
00:04:54,870 --> 00:05:04,990
‫If I want to perform this arithmetic operation, I must convert or cast my string to an integer or to

53
00:05:04,990 --> 00:05:06,180
‫a the variable.

54
00:05:07,370 --> 00:05:18,410
‫So here in front of price, all right, kind of price and here the same kind of quantity, and now there

55
00:05:18,410 --> 00:05:21,500
‫will be no Adara when I read the script.

56
00:05:23,900 --> 00:05:34,640
‫Enterprise five and quantity two, total value then, OK, here it multiplied to integers.

57
00:05:35,660 --> 00:05:41,930
‫We'll talk more about the casting or convecting data types in the next lecture.

58
00:05:42,080 --> 00:05:44,030
‫See you in just a few seconds.


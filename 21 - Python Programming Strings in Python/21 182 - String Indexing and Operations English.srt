﻿1
00:00:01,040 --> 00:00:02,790
‫Hello, guys, and welcome back.

2
00:00:02,960 --> 00:00:09,860
‫In this lecture, we'll continue our discussion about the strings of string is an order of the sequence

3
00:00:09,860 --> 00:00:10,760
‫of characters.

4
00:00:11,000 --> 00:00:16,370
‫We can refer to any item in the sequence by using index number.

5
00:00:16,670 --> 00:00:24,470
‫So we have a concept of the first element of the string, the second element, the third element and

6
00:00:24,470 --> 00:00:25,090
‫so on.

7
00:00:25,400 --> 00:00:32,600
‫This is also valid for other sequence types, like lists in Python, like also in other programming

8
00:00:32,600 --> 00:00:36,010
‫languages such as C, C++ or Java.

9
00:00:36,170 --> 00:00:40,040
‫We start indexing from number zero, not one.

10
00:00:40,370 --> 00:00:47,530
‫This means that the first string element when counting from left to right has the index is zero.

11
00:00:47,900 --> 00:00:56,570
‫The second element has its corresponding index one, the third element index two and so on.

12
00:00:57,170 --> 00:00:58,730
‫Let's see an example.

13
00:00:59,360 --> 00:01:06,260
‫I create the string variable called movie and now when I want to access a character from a specific

14
00:01:06,260 --> 00:01:13,520
‫index, I use the name of the string variable in the index number enclosed by square brackets, for

15
00:01:13,520 --> 00:01:24,830
‫example, movie of zero returns t because T has index zero, then movie of index one of course will

16
00:01:24,830 --> 00:01:29,030
‫return H because H has index one.

17
00:01:30,030 --> 00:01:38,490
‫When counting backwards from right to left, the first index will be minus one, so I can have here

18
00:01:38,760 --> 00:01:43,030
‫movie off minus one and it will return.

19
00:01:43,100 --> 00:01:47,100
‫Ah, this is when looking from right to left.

20
00:01:47,910 --> 00:01:52,110
‫Movie of minus two will be E and so on.

21
00:01:52,590 --> 00:01:55,480
‫Movie of minus five will be a.

22
00:01:56,640 --> 00:02:00,720
‫Now what happens if we enter an invalid index.

23
00:02:01,170 --> 00:02:03,960
‫Let me show you what I mean with an example.

24
00:02:04,230 --> 00:02:08,100
‫I create another variable called the name equals John.

25
00:02:08,640 --> 00:02:16,140
‫We can count visually that the string referenced by the name Variable has four characters with the last

26
00:02:16,140 --> 00:02:18,300
‫character at Index three.

27
00:02:19,550 --> 00:02:27,820
‫Zero, one, two and three, and I'll access the character from Index for or.

28
00:02:28,450 --> 00:02:29,730
‫What happens then?

29
00:02:30,090 --> 00:02:32,220
‫So name of Lexington.

30
00:02:33,010 --> 00:02:41,350
‫And the answer is, I've got the letter, a string index out of range error, you should know that you

31
00:02:41,350 --> 00:02:49,210
‫can avoid such situations by using another built in function and fixed the line function e three times

32
00:02:49,240 --> 00:02:51,590
‫the number of characters in a string.

33
00:02:51,940 --> 00:02:58,510
‫So if you have a very long string, you'll find out what's the maximum index you can use by using this

34
00:02:58,510 --> 00:02:59,050
‫function.

35
00:02:59,530 --> 00:03:01,360
‫Let's try Lenhoff name.

36
00:03:02,490 --> 00:03:12,650
‫And it returned before because the name variable has four characters length of movie and it returned

37
00:03:12,790 --> 00:03:16,560
‫13, we have here 13 characters.

38
00:03:17,630 --> 00:03:25,280
‫Now that we know how a sequence such as a string or a list is indexed will move farther to basic string

39
00:03:25,280 --> 00:03:33,530
‫operations, which are concatenation and repetition, we use the plus sign to concatenate or join two

40
00:03:33,560 --> 00:03:39,230
‫or more strings in a new string and the star operator to repeat a string.

41
00:03:40,710 --> 00:03:46,950
‫Let's create another variable called director equals Francis Ford Coppola.

42
00:03:50,950 --> 00:03:59,170
‫And our movie, plus this is the concatenation operator and between single or double quotes, a space

43
00:03:59,170 --> 00:04:01,840
‫here was directed by.

44
00:04:03,920 --> 00:04:06,980
‫Another space plus director.

45
00:04:13,090 --> 00:04:19,690
‫And I've obtained a new string, The Godfather was directed by Francis Ford Coppola.

46
00:04:20,050 --> 00:04:23,410
‫Of course, we can get that value in a new variable.

47
00:04:24,310 --> 00:04:32,650
‫Like, say, movie one equals and that expression in our movie, one has this value.

48
00:04:33,790 --> 00:04:39,800
‫If we want to concatenate a string and a float or an integer, we'll get an error.

49
00:04:39,940 --> 00:04:42,400
‫We can only concatenate strings.

50
00:04:44,820 --> 00:04:46,410
‫Product notebook.

51
00:04:47,720 --> 00:04:54,870
‫And price equals ninety point five in our product plus price.

52
00:04:55,220 --> 00:05:02,080
‫So I want to concatenate and display notebook Kahlan and the price and I've got an error.

53
00:05:02,600 --> 00:05:08,060
‫I can only concatenate strings to strings, not to float's.

54
00:05:08,960 --> 00:05:19,830
‫This is a type error, we can solve this error by casting a float to a string for that I SDR of price.

55
00:05:20,390 --> 00:05:23,000
‫Now I'm concatenating to string's.

56
00:05:26,220 --> 00:05:28,870
‫It didn't modify the price variable.

57
00:05:29,100 --> 00:05:31,740
‫It has just returned a string.

58
00:05:33,890 --> 00:05:38,180
‫So the content of the price variable is still afloat.

59
00:05:38,930 --> 00:05:40,340
‫Now let's try repetition.

60
00:05:42,080 --> 00:05:49,630
‫We use the star operator, so movie star five and it returned a new string.

61
00:05:50,510 --> 00:05:58,910
‫Of course, I can also have something like this movie, plus a space here, everything between parentheses

62
00:05:59,870 --> 00:06:00,710
‫times five.

63
00:06:01,160 --> 00:06:02,840
‫And I've got this value.

64
00:06:03,380 --> 00:06:07,810
‫So I've repeated this string five times.

65
00:06:08,420 --> 00:06:09,830
‫Let's try the last example.

66
00:06:09,830 --> 00:06:14,480
‫Let's say price equals input and here price.

67
00:06:16,090 --> 00:06:22,900
‫And our price, six point seven and then price times.

68
00:06:24,000 --> 00:06:29,740
‫Four, and this is the value, and this is not what we expected.

69
00:06:30,570 --> 00:06:39,000
‫In fact, price is a variable that starts a spring and I've repeated this string four times.

70
00:06:39,450 --> 00:06:48,240
‫If I want to perform an arithmetic operation, I must cast the string value returned by the input function

71
00:06:48,240 --> 00:06:54,260
‫to offload something like this float of price times for.

72
00:06:54,510 --> 00:06:57,240
‫And this is the value I've expected.

73
00:06:57,990 --> 00:07:04,320
‫Thank you all for the moment about the string indexing and the basic string operations.


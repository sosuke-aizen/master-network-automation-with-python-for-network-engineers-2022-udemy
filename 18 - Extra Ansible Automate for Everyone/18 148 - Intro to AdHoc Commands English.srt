﻿1
00:00:00,770 --> 00:00:08,870
‫In this lecture, we'll discuss about adhoc comments, what they are and how to use them, an ad hoc

2
00:00:08,870 --> 00:00:15,860
‫comment is something that you might type to do something really quick but don't want to save for later.

3
00:00:16,160 --> 00:00:20,030
‫It's one time task that you run and forget about it.

4
00:00:20,930 --> 00:00:27,830
‫This is a good place to start to understand the basics of what Ansible can do prior to learning the

5
00:00:27,830 --> 00:00:29,240
‫Playbook's language.

6
00:00:29,750 --> 00:00:38,150
‫The two power of Ansible lies in Playbook's playbooks are used for big deployment, orchestration or

7
00:00:38,150 --> 00:00:39,470
‫system configuration.

8
00:00:40,070 --> 00:00:49,550
‫So why would you use ad hoc tasks versus Playbook's ad hoc commands can be used to do quick and simple

9
00:00:49,550 --> 00:00:54,980
‫things that you might not necessarily want to write a full playbook for.

10
00:00:55,710 --> 00:01:02,210
‫If you want to check the logs, if a process is running or you want to install a package on a list of

11
00:01:02,210 --> 00:01:08,900
‫servers, it's simpler and easier to execute an ad hoc comment in a single line.

12
00:01:09,020 --> 00:01:18,640
‫Instead of writing a playbook for this task, we use the Ansible Command to run an ad hoc task and the

13
00:01:18,680 --> 00:01:21,920
‫Ansible Playbook Command to run a playbook.

14
00:01:22,430 --> 00:01:28,250
‫Both ad hoc comments and playbooks use modules to perform different tasks.

15
00:01:28,670 --> 00:01:33,380
‫Modules are uniques of code that do the actual work.

16
00:01:33,380 --> 00:01:40,460
‫In Ansible, they are what GICs executed in each attack, command or playbook task.

17
00:01:41,360 --> 00:01:43,400
‫Here we have two examples.

18
00:01:43,400 --> 00:01:51,710
‫In the first example, Ansible will run an ad hoc command on the server with the IP address of one nine

19
00:01:51,710 --> 00:01:57,980
‫to one hundred sixty eight dollars to one hundred twenty one using the shell module.

20
00:01:58,190 --> 00:02:03,260
‫And the argument of the module is the you name minus eight come in.

21
00:02:03,710 --> 00:02:10,820
‫This command displays useful information like for example, the version of the Linux kernel the system

22
00:02:10,820 --> 00:02:11,360
‫uses.

23
00:02:11,600 --> 00:02:21,440
‫Ansible will connect to the SSX server using Andre Yuzu and it will ask for the password, so it will

24
00:02:21,440 --> 00:02:22,670
‫prompt for the password.

25
00:02:22,820 --> 00:02:32,270
‫In the second example, Ansible is using the module that executes a comment on the remote SSX server

26
00:02:32,660 --> 00:02:36,920
‫and that command is show version we are connecting.

27
00:02:36,920 --> 00:02:44,660
‫Using on username is prompting for the password and then we are looking in the output for the software

28
00:02:44,660 --> 00:02:45,170
‫string.

29
00:02:45,680 --> 00:02:48,340
‫So we have a pipe and the grep command.

30
00:02:48,480 --> 00:02:53,540
‫In fact, I want to see what is the version of the iOS, the outer runs.

31
00:02:54,290 --> 00:03:01,280
‫In the next lecture, I'll show you some live examples with Ansible ad hoc comments.

32
00:03:01,580 --> 00:03:03,590
‫See you in just a few seconds.


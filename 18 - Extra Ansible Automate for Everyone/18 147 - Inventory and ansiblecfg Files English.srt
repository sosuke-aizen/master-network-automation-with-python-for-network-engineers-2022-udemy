﻿1
00:00:00,430 --> 00:00:09,280
‫In this lecture, we'll talk about an inventory file and configuration file, Ansible uses an inventory

2
00:00:09,310 --> 00:00:13,960
‫or at least to know about devices under management.

3
00:00:14,500 --> 00:00:23,080
‫In this lab, I am connected from my Windows recording machine to the Linux machine where I've installed

4
00:00:23,080 --> 00:00:23,770
‫Ansible.

5
00:00:24,370 --> 00:00:33,030
‫The default inventory file for Ansible is in ATC, Ansible and its name is Husks.

6
00:00:33,820 --> 00:00:37,270
‫This is the inventory file of Ansible.

7
00:00:37,510 --> 00:00:48,620
‫Here we must write the IP addresses or the domain names of devices and servers under Ansible management.

8
00:00:49,000 --> 00:00:51,550
‫We can also goup them if we want.

9
00:00:52,330 --> 00:00:53,350
‫By default.

10
00:00:53,560 --> 00:00:56,890
‫The entire file is commented out.

11
00:00:57,610 --> 00:01:07,270
‫I have these Jeunesse three topology with five score outs and two Linux servers and I am going to connect

12
00:01:07,390 --> 00:01:10,060
‫with Ansible from this cloud.

13
00:01:10,540 --> 00:01:20,320
‫These are the IP addresses of my devices and servers and I'm going to show you how to modify the inventory

14
00:01:20,320 --> 00:01:26,140
‫file of Ansible in order to be able to manage these devices.

15
00:01:26,890 --> 00:01:31,410
‫In order to be able to edit the inventory file.

16
00:01:31,450 --> 00:01:39,190
‫We must the OpenNet using Sudo and the preferred editor in this case, I am using VI.

17
00:01:39,400 --> 00:01:44,850
‫You need the root permissions in order to edit the inventory file.

18
00:01:45,250 --> 00:01:52,870
‫If I want to create a group, I'll write the name of my group between square brackets and then I'll

19
00:01:52,870 --> 00:01:56,770
‫write the IP addresses of my routers.

20
00:01:57,040 --> 00:02:02,830
‫So 192 168 dot one two two dot ten.

21
00:02:03,190 --> 00:02:04,150
‫One hundred ninety two.

22
00:02:04,150 --> 00:02:10,840
‫One hundred sixty eight one hundred twenty two dot eleven and so on.

23
00:02:17,110 --> 00:02:21,970
‫And I'm going to create another group called Surfers.

24
00:02:30,150 --> 00:02:32,650
‫It's not mandatory to have groups.

25
00:02:33,000 --> 00:02:42,390
‫I can also have individual IP addresses or domain names, but if I have groups, I can manage all servers

26
00:02:42,390 --> 00:02:43,350
‫in the same way.

27
00:02:43,440 --> 00:02:53,580
‫For example, I could run a single comment on the SRS or I can install a package on these two servers.

28
00:02:53,900 --> 00:02:58,170
‫Now, if we have this inventory file, let's test it.

29
00:02:59,090 --> 00:03:07,640
‫If you want to list all devices from the inventory file that are on ansible management, you simply

30
00:03:07,640 --> 00:03:12,110
‫write Ansible minus, minus the least hosts all.

31
00:03:13,330 --> 00:03:24,130
‫And we can see there are eight hosts on ansible management, so in the inventory file now to test these

32
00:03:24,130 --> 00:03:30,440
‫hosts, I'll use an Apple command and control module first.

33
00:03:30,610 --> 00:03:33,760
‫Let's see what does the role model do?

34
00:03:34,540 --> 00:03:39,160
‫It executes a low down and dirty SSX command.

35
00:03:39,460 --> 00:03:46,510
‫As it says in the documentation, this module must be used in a few cases.

36
00:03:47,350 --> 00:03:53,430
‫A common case is installing Python on a system without python installed by default.

37
00:03:53,800 --> 00:04:01,510
‫And another case is when speaking to any device such as their outbursts that do not have any python

38
00:04:01,510 --> 00:04:02,200
‫installed.

39
00:04:02,710 --> 00:04:12,600
‫In our case, our Cisco routers don't have Python installed, so this is an appropriate way to use their

40
00:04:12,670 --> 00:04:13,250
‫module.

41
00:04:13,750 --> 00:04:20,550
‫We must also know that this module is also supported for Windows as targets.

42
00:04:20,980 --> 00:04:28,010
‫So back to our console, Aileron Ansible, the name of the device from the inventory file.

43
00:04:28,390 --> 00:04:31,600
‫So let's say one hundred ninety to one hundred sixty eight.

44
00:04:31,840 --> 00:04:34,300
‫One hundred twenty two then.

45
00:04:36,290 --> 00:04:46,730
‫Minus M from module four, minus A, and then comes the argument, which is in fact the comment that

46
00:04:46,730 --> 00:04:50,240
‫will be executed on the remote device.

47
00:04:50,270 --> 00:05:00,120
‫So on the managed node using S6 in this case, Lex are on the show version and then we have minus you.

48
00:05:00,470 --> 00:05:08,820
‫This is the user used to authenticate using SSX Yuan and minus K.

49
00:05:08,990 --> 00:05:11,840
‫This means it will ask for the password.

50
00:05:13,980 --> 00:05:15,660
‫The password is Cisco.

51
00:05:19,230 --> 00:05:24,780
‫And it has displayed the output of show version comment.

52
00:05:26,000 --> 00:05:34,970
‫You must also know that even if you have a direct connectivity with this device, it's mandatory for

53
00:05:35,000 --> 00:05:39,230
‫each name or IP address to be in the inventory file.

54
00:05:39,500 --> 00:05:45,470
‫So, for example, if I go and the delete that IP address from my inventory file.

55
00:05:51,650 --> 00:05:55,100
‫And here, I'll comment about this line.

56
00:05:56,570 --> 00:06:01,040
‫I'm saving the file and I'm going to run this comment one more time.

57
00:06:03,530 --> 00:06:11,450
‫I got an error, even if nothing changed, it says ignoring and then the IP address of my device, because

58
00:06:11,480 --> 00:06:17,660
‫this doesn't belong to my inventory file, so everything must be there.

59
00:06:17,720 --> 00:06:25,640
‫Every IP address or name of a device that we manage with Ansible, we can also use names.

60
00:06:25,910 --> 00:06:32,720
‫If we can translate those names to IP addresses, let's become rude and in hosts.

61
00:06:32,900 --> 00:06:37,010
‫This is because I don't have a DNS server configured at this moment.

62
00:06:37,370 --> 00:06:44,770
‫I write one hundred ninety two, one hundred sixty eight one hundred twenty two dot and its name is

63
00:06:44,790 --> 00:06:45,950
‫the outer one.

64
00:06:46,220 --> 00:06:47,510
‫I'm saving the file.

65
00:06:47,720 --> 00:06:49,700
‫I test it if it works.

66
00:06:50,000 --> 00:06:57,410
‫So being out there on OC, it translates the name without the one to the correct IP address.

67
00:06:57,590 --> 00:07:01,550
‫And now I'm going to ATC Ansible hosts.

68
00:07:03,760 --> 00:07:09,520
‫And here, instead of the IP address of my first router, I write about one.

69
00:07:11,990 --> 00:07:17,960
‫And I'm saving the file now, I'm going to run the ansible command one more time.

70
00:07:22,340 --> 00:07:25,940
‫OK, ignoring now hosts makes.

71
00:07:27,290 --> 00:07:32,240
‫And now, instead of the appearance of my daughter, Ayla right here, her out there on.

72
00:07:38,040 --> 00:07:45,240
‫OK, I got this error, using an SSX password instead of a key is not possible because I just keep checking

73
00:07:45,240 --> 00:07:46,140
‫is enabled.

74
00:07:46,440 --> 00:07:54,870
‫So first time when we are connecting using SSX to our server, the client wants to identify that server

75
00:07:55,140 --> 00:08:01,410
‫in a host key file and the that host key file doesn't exist.

76
00:08:01,560 --> 00:08:02,190
‫For that.

77
00:08:02,190 --> 00:08:06,390
‫I'll go and edit the ansible dot CFC file.

78
00:08:11,380 --> 00:08:20,080
‫And here I'm looking for key and we have lost key checking falls and I'm going to comment this line,

79
00:08:20,320 --> 00:08:25,480
‫so I'll comment this to disable SSX key checking, OK?

80
00:08:27,140 --> 00:08:30,230
‫And now I'm going to run the command one more time.

81
00:08:33,560 --> 00:08:42,170
‫And it worked, we can see the output of the show version come in, we can run any comment we want,

82
00:08:42,170 --> 00:08:46,100
‫of course, if we want here, shall I be interface brief?

83
00:08:47,190 --> 00:08:48,270
‫There is no problem.

84
00:08:53,370 --> 00:09:02,400
‫We can see the output of that comment and of course, we can hear the entire group, so let's say rooters.

85
00:09:04,130 --> 00:09:10,520
‫And is going to run this comment on each device of this group.

86
00:09:13,350 --> 00:09:21,120
‫And we can see the output of the show IP interface, brief comment that has been executed on each other

87
00:09:21,120 --> 00:09:29,730
‫out there of my group, something very interesting is that by default, Ansible uses multi threading

88
00:09:29,730 --> 00:09:34,200
‫and it executes these comments in parallel.

89
00:09:34,560 --> 00:09:36,240
‫Ansible, its very fast.

90
00:09:36,750 --> 00:09:42,110
‫Of course, we can execute comments on our Linux servers.

91
00:09:42,600 --> 00:09:47,700
‫Let's try to execute a comment on that Linux server on the first Linux server.

92
00:09:47,950 --> 00:09:54,510
‫So the comment like say I have config, I want to see if the IP address and the Mac address of the server.

93
00:09:55,560 --> 00:10:00,000
‫Instead of outbursts, I have here Linux servers.

94
00:10:02,750 --> 00:10:05,720
‫And here the user name is Andre.

95
00:10:09,090 --> 00:10:11,410
‫OK, there is no such a group.

96
00:10:11,430 --> 00:10:19,260
‫So one more time in D.C. and Cebull hosts and we have here servers.

97
00:10:20,050 --> 00:10:24,540
‫So here instead of Linux servers, I have only servers.

98
00:10:28,140 --> 00:10:33,210
‫And Ansible ran the command on each Linux server.

99
00:10:34,420 --> 00:10:42,160
‫This is the output of the IAF config command executed on a Linux server.

100
00:10:43,320 --> 00:10:51,150
‫The last thing I'll tell you is that if you want to use another inventory file, you can use the minus

101
00:10:51,180 --> 00:10:52,380
‫eye option.

102
00:10:53,460 --> 00:11:02,490
‫LexCorp is the hosts file from the stipple directory, Etsy and Sippel hosts in the current working

103
00:11:02,490 --> 00:11:05,400
‫directory in my current working directory.

104
00:11:05,430 --> 00:11:07,470
‫There is a file named Hosts.

105
00:11:11,040 --> 00:11:17,720
‫It's the same fight, but if I want to use the file from the current directory here, I can have minus

106
00:11:17,730 --> 00:11:20,870
‫I and a valid path to my file.

107
00:11:21,060 --> 00:11:24,630
‫In this case, in the current directory, I have a file named hosts.

108
00:11:26,390 --> 00:11:29,690
‫And it will use that inventory file.


﻿1
00:00:00,660 --> 00:00:09,720
‫Now, I'll show you some useful modules for Ansible ad hoc mode, the general syntax for Ansible ad

2
00:00:09,720 --> 00:00:11,460
‫hoc mode is Ansible.

3
00:00:11,800 --> 00:00:21,390
‫Then comes the host or the list of hosts we are configuring, then minus M in the name of the module,

4
00:00:21,930 --> 00:00:30,660
‫minus AI and the module arguments minus you and the username that will be connecting to the stage server

5
00:00:30,660 --> 00:00:31,560
‫minus K.

6
00:00:31,740 --> 00:00:41,910
‫The lowercase letter K means that it will ask for the password and minus K, the uppercase letter K

7
00:00:42,120 --> 00:00:46,180
‫means it will ask for the pseudo password if we want to execute.

8
00:00:46,180 --> 00:00:54,210
‫The command is root and minus minus become means that it will become another user by default.

9
00:00:54,360 --> 00:00:58,710
‫Who would like to see the first module, the command module.

10
00:00:58,920 --> 00:01:03,130
‫This is the default module for ansible ad hoc mode.

11
00:01:03,780 --> 00:01:05,190
‫I have this topology.

12
00:01:05,700 --> 00:01:13,350
‫We've used this topology also in a previous lecture, we have five, six quarters and two Ubuntu servers

13
00:01:13,530 --> 00:01:21,430
‫and I am going to run some comments using the ansible ad hoc mode from my ansible control machine.

14
00:01:21,720 --> 00:01:27,600
‫This is the ansible control machine and I'm going to run a command on an open to server.

15
00:01:28,020 --> 00:01:31,680
‫In the current working directory, there is a hosts file.

16
00:01:33,380 --> 00:01:42,260
‫And there we have the Cisco routers and two servers, I have also created two groups, the routers and

17
00:01:42,260 --> 00:01:50,360
‫the servers group, the default and configuration file ezine slash ATC, slash Ansible.

18
00:01:50,480 --> 00:01:53,300
‫And the name is Ansible Dot Sevgi.

19
00:01:54,050 --> 00:01:59,030
‫And here I've changed only host Keek shaking equals false.

20
00:01:59,360 --> 00:02:03,730
‫Now let's try to run a comment on 012 server Ansible.

21
00:02:03,980 --> 00:02:07,270
‫Now the name of my server or its IP address.

22
00:02:07,310 --> 00:02:10,580
‫It must belong to the hosts or inventory file.

23
00:02:10,670 --> 00:02:20,030
‫And if I don't specify an inventory file using minus I option, Ansible will automatically use the inventory

24
00:02:20,030 --> 00:02:23,180
‫file from slash ATC slash Ansible.

25
00:02:23,390 --> 00:02:28,660
‫There is a file name hosts minus M the module command.

26
00:02:28,970 --> 00:02:30,050
‫This is the default.

27
00:02:30,410 --> 00:02:39,060
‫It's not mandatory to write the command module minus a this is the argument over the command module.

28
00:02:39,380 --> 00:02:41,300
‫So in fact this is the command.

29
00:02:42,020 --> 00:02:50,210
‫For example, Lix are on the WHO command to see who is connected on that remote server minus you.

30
00:02:50,210 --> 00:02:56,990
‫Andre, this is the user that will be authenticating and draining the command on the remote server in

31
00:02:56,990 --> 00:03:01,160
‫minus K means that it will ask for the user password.

32
00:03:03,230 --> 00:03:04,410
‫I enter the password.

33
00:03:04,940 --> 00:03:12,930
‫This is the output of the who come in, let's look in the ansible documentation at the command module.

34
00:03:13,610 --> 00:03:18,140
‫So the given command will be executed on all selected nodes.

35
00:03:18,440 --> 00:03:21,200
‫It will not be processed through the shell.

36
00:03:21,300 --> 00:03:28,550
‫So variables like home and operations like redirection, piping and so on will not work.

37
00:03:29,000 --> 00:03:34,410
‫If we want to use redirection or piping, we must use the shell module.

38
00:03:34,880 --> 00:03:36,800
‫So, for example, let's return here.

39
00:03:37,970 --> 00:03:44,690
‫And instead of the WHO comment, let's right here Diffie from disk free minus eight.

40
00:03:44,870 --> 00:03:50,630
‫So, in fact, I want to see how much empty space do I have on my partitions?

41
00:03:54,580 --> 00:04:02,230
‫And this is the output and now I want to try or get traction here, so I want to redirect the output

42
00:04:02,230 --> 00:04:06,340
‫of the comment in a file like, say, space dot texte.

43
00:04:09,240 --> 00:04:16,560
‫And it didn't work, I can't use the right direction with the command module, if I want to use rejection,

44
00:04:17,010 --> 00:04:20,700
‫I must use another module and fix the shell module.

45
00:04:25,530 --> 00:04:34,450
‫OK, there is no error, and if I go on that server here, I can see the space dot, the file inside

46
00:04:34,450 --> 00:04:39,180
‫the space, dot the file, I can find the output of my comment.

47
00:04:40,090 --> 00:04:47,080
‫So you must know that you can use the shell module when you want to run Cummings through a shell, if

48
00:04:47,080 --> 00:04:53,410
‫you are using Windows targets, you must use the win underlined shell module instead.

49
00:04:54,580 --> 00:05:01,720
‫Now, I want to show you some examples using the role model, the role model executes a lot down and

50
00:05:01,720 --> 00:05:02,110
‫dirty.

51
00:05:02,110 --> 00:05:08,790
‫SSX comment exposed when a very mild note doesn't have Python installed.

52
00:05:08,980 --> 00:05:14,860
‫For example, we have Siwiec or Outr or another device without Python.

53
00:05:15,190 --> 00:05:21,010
‫Arguments given to role are Iran directly through the configured remote shell.

54
00:05:22,050 --> 00:05:26,080
‫There are module is also supported for Windows targets.

55
00:05:26,700 --> 00:05:27,960
‫Let's try some examples.

56
00:05:29,090 --> 00:05:36,290
‫I want to run the show version, comment on these five rooters, and I want to see what version of Ionis

57
00:05:36,500 --> 00:05:37,190
‫do they run.

58
00:05:38,180 --> 00:05:45,530
‫So here I have a group named Roberts and I'm going to write and Cebull Roberts either on the comment

59
00:05:45,560 --> 00:05:48,580
‫on each chapter minus M or.

60
00:05:49,690 --> 00:05:58,750
‫Minus eight, say, and this is the command show version minus you, you one and minus K e to ask for

61
00:05:58,750 --> 00:06:01,150
‫the password, I am entering the password.

62
00:06:03,640 --> 00:06:10,710
‫And I can see the output of the show version command that has been executed on each other.

63
00:06:11,650 --> 00:06:17,980
‫Now, if I want to see what is the version of the iOS, I can grab for iOS software.

64
00:06:19,900 --> 00:06:23,490
‫Pipe Greb software fix enough.

65
00:06:27,790 --> 00:06:36,880
‫And this is the IRS version, of course, we can attract to a file Lexia outearns, underline IRS,

66
00:06:36,880 --> 00:06:37,270
‫dot the.

67
00:06:41,130 --> 00:06:46,820
‫And in the end, talking back victory, there is a file name throughout the U.S., the.

68
00:06:49,390 --> 00:06:55,980
‫Of course, I can use the role model to Ron Cummings, also on Linux or Windows machines.

69
00:06:56,560 --> 00:07:04,990
‫So here instead of routers like STARIGHT servers, I am going to run a comment on each server of my

70
00:07:04,990 --> 00:07:05,340
‫group.

71
00:07:06,420 --> 00:07:15,570
‫In the coming days, I if config, the user name is Andre, and I want to see the Mac address of each

72
00:07:15,570 --> 00:07:20,640
‫server electron document first without grep to see how does it look like.

73
00:07:22,980 --> 00:07:25,980
‫OK, so I grew up using these string.

74
00:07:32,510 --> 00:07:39,240
‫And it has displayed the mock addresses of the interfaces of my service.

75
00:07:40,160 --> 00:07:47,120
‫In conclusion, when you want to run a single command using Ansible on a remote node, you can use the

76
00:07:47,120 --> 00:07:51,560
‫command module, the shell module or the arrow module.

77
00:07:52,160 --> 00:07:59,540
‫One last thing you must know is that if you are using the command module, Python must be installed

78
00:07:59,540 --> 00:08:00,800
‫on the remote host.

79
00:08:01,910 --> 00:08:10,220
‫In the next lectures, I'll show you more examples using Ansible artwork, comics and other modules.


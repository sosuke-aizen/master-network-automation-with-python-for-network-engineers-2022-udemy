﻿1
00:00:00,420 --> 00:00:07,110
‫Now, let's suppose you have a very large number of servers and you want to update a specific package

2
00:00:07,110 --> 00:00:14,880
‫on each of them, or you want to install a specific package on each server for that, you can use the

3
00:00:14,880 --> 00:00:16,350
‫APTE module.

4
00:00:16,650 --> 00:00:24,060
‫Or if you are using garroted based distribution like S.O.S or Fedora, you can use the youm module.

5
00:00:25,160 --> 00:00:26,320
‫Let's see how he talks.

6
00:00:26,660 --> 00:00:36,380
‫I want to install the Ingenix Sarovar or Neach Ubuntu machine at this moment there is no Ingenix installed,

7
00:00:36,680 --> 00:00:41,730
‫neither on a set of aruan nor on a server to work on a server to.

8
00:00:41,780 --> 00:00:48,050
‫There is a package named Ingenix Common, but there is no Ingenix server installed.

9
00:00:48,320 --> 00:00:51,140
‫So Bechtol Ansible control machine.

10
00:00:51,420 --> 00:00:53,810
‫I write Ansible Mianus.

11
00:00:53,810 --> 00:01:04,040
‫I hosts from the current talking back actory the inventory file servers my servers group minus M Apte.

12
00:01:04,550 --> 00:01:10,160
‫This is the module minus A and now this is the module argument.

13
00:01:11,420 --> 00:01:16,970
‫Name equals Ingenix, state equals present.

14
00:01:18,310 --> 00:01:24,460
‫This means it will check if that package is installed, if it is installed.

15
00:01:24,760 --> 00:01:25,990
‫It does nothing.

16
00:01:26,140 --> 00:01:33,760
‫But if it isn't installed, it will install the package and update cash equal to.

17
00:01:35,330 --> 00:01:41,120
‫Then minus you, Andre, minus key, and I'm going to run the comment.

18
00:01:42,820 --> 00:01:50,770
‫As you can see, I am connecting and running the comment on the server is an unprivileged user, so

19
00:01:50,770 --> 00:01:53,680
‫I suppose I'll get the leg legs tested.

20
00:01:56,600 --> 00:02:04,220
‫As I imagined, I've gotten there, failed to look a pity for exclusive operation, this is because

21
00:02:04,220 --> 00:02:07,710
‫only you can install a package on Linux.

22
00:02:07,910 --> 00:02:09,260
‫What must I do?

23
00:02:09,380 --> 00:02:13,070
‫I must use the minus minus become option.

24
00:02:13,800 --> 00:02:15,890
‫This means it will run.

25
00:02:15,920 --> 00:02:25,490
‫The operation is good and minus K, it's also important because minus K using the uppercase letter indicates

26
00:02:25,550 --> 00:02:28,280
‫that the pseudo password will be asked.

27
00:02:30,130 --> 00:02:37,210
‫This is the user password and then the pseudo password default to a password, in this case it's the

28
00:02:37,210 --> 00:02:38,050
‫same password.

29
00:02:38,230 --> 00:02:41,020
‫So I'm going to press on the enter key.

30
00:02:44,200 --> 00:02:53,020
‫And has connected to the servers and we can see here the output of the Apte install command.

31
00:02:54,040 --> 00:02:59,980
‫This comment takes some time, but at the end it has installed Ingenix.

32
00:03:01,640 --> 00:03:03,410
‫If I run the comment one more time.

33
00:03:06,030 --> 00:03:14,670
‫It does nothing, it checks if there is already a package named Ingenix installed and if there is such

34
00:03:14,670 --> 00:03:18,190
‫a packet, it doesn't install it one more time.

35
00:03:18,210 --> 00:03:25,410
‫We can see here the return status is success and changed equals false.

36
00:03:27,060 --> 00:03:35,390
‫Now back to my servers, let's see if Ingenix is installed and we can see that both engineers and Ingenix

37
00:03:35,400 --> 00:03:41,190
‫core are installed and the same here on the other server.

38
00:03:46,720 --> 00:03:47,230
‫OK.

39
00:03:48,150 --> 00:03:55,560
‫And we can see that it has installed Ingenix, there are also other packages installed, but it doesn't

40
00:03:55,560 --> 00:04:01,650
‫matter maybe these packages or dependencies for other applications.

41
00:04:02,460 --> 00:04:09,300
‫Another possibility to Ron Cummings on a Linux machine is root is to modify the software files.

42
00:04:09,660 --> 00:04:20,590
‫So in ETSI Soothsayer's at Suto group here, I'll add now be a SSW D from no password.

43
00:04:20,610 --> 00:04:25,430
‫This means that the pseudo password won't be asked anymore.

44
00:04:26,740 --> 00:04:35,710
‫So any user that belongs to the SUDO group can run comments as without having to enter the pseudo password.

45
00:04:36,790 --> 00:04:45,790
‫I've saved the file and now Sue Andre, I became Andre and I ran a comment with Sudo and it won't ask

46
00:04:45,790 --> 00:04:46,660
‫for the password.

47
00:04:47,200 --> 00:04:56,230
‫So for example, Cat Etsi Shadow, the shadow file, contains the user's passwords and can be read only

48
00:04:56,230 --> 00:04:56,740
‫by code.

49
00:04:57,040 --> 00:05:00,160
‫And we can see that it didn't ask for the password.

50
00:05:00,820 --> 00:05:05,170
‫Now, if I want to execute the comment is route from Ansible.

51
00:05:05,470 --> 00:05:14,740
‫I don't have to specify here the uppercase letter K Vex because Andrei User can't run commands with

52
00:05:14,740 --> 00:05:18,010
‫Sudo without entering the pseudo password.

53
00:05:19,200 --> 00:05:26,190
‫Let's install another package, let's say a seat to this is another website of our.

54
00:05:30,370 --> 00:05:39,340
‫And we can see how Ansible could install a Bakhshi to server on this machine, but got an error when

55
00:05:39,340 --> 00:05:49,450
‫it tried to install it on the other server because I didn't modify the ITC Sudworth files on this server

56
00:05:49,630 --> 00:05:52,050
‫and a pseudo password is required.

57
00:05:52,690 --> 00:05:59,950
‫So I must specify the uppercase K or in Soudas file from ETSI directory.

58
00:06:00,070 --> 00:06:04,420
‫I must specify no SS W.T..

59
00:06:05,080 --> 00:06:11,740
‫Also, keep in mind that if you want to automate a configuration task from a current job, you must

60
00:06:11,740 --> 00:06:20,290
‫configure S.H. public key authentication because the user vic's connecting to the essayistic server

61
00:06:20,530 --> 00:06:22,930
‫don't have to enter his passport.

62
00:06:23,230 --> 00:06:34,110
‫And you must also add no password in the ATC software file and Ansible won't ask for the Souto password.

63
00:06:34,990 --> 00:06:37,750
‫You must modify this line here.

64
00:06:40,190 --> 00:06:44,570
‫For the studio group now, PJCIS, W.D., call on all.

65
00:06:45,790 --> 00:06:53,680
‫Now, let's see how can we start or stop a service on a group of servers for that, we are going to

66
00:06:53,680 --> 00:06:57,000
‫use the service module first.

67
00:06:57,310 --> 00:07:04,720
‫Let's see if Ingenix is running system Sitel status Ingenix.

68
00:07:07,240 --> 00:07:18,100
‫And we can see that Ingenix isn't running so inactive or dead, and on the other side of our status,

69
00:07:19,300 --> 00:07:26,190
‫engineers and engineers is not running inactive or dead.

70
00:07:26,560 --> 00:07:29,740
‫And now back to our ansible control machine.

71
00:07:29,920 --> 00:07:36,150
‫Let's run an ad hoc comment that starts Ingenix on each server.

72
00:07:37,410 --> 00:07:43,020
‫And Paul, I'll use the same inventory file from the working directory service.

73
00:07:43,410 --> 00:07:47,130
‫This is my group minus AM service.

74
00:07:47,610 --> 00:07:58,320
‫I am using the service module and the argument is name equals Ingenix state equals started.

75
00:07:59,010 --> 00:08:02,850
‫It will verify that Ingenix has started.

76
00:08:03,660 --> 00:08:06,840
‫If it has already started, it doesn't do a thing.

77
00:08:06,990 --> 00:08:12,900
‫But if it hasn't started at the ansible, ad hoc comment will start it.

78
00:08:13,920 --> 00:08:24,120
‫And then minus you, the user that is authenticating to the SSX server, Andre, minus K, it will ask

79
00:08:24,120 --> 00:08:32,880
‫for his passport minus minus become it will become good and minus uppercase K. It will ask for the pseudo

80
00:08:32,880 --> 00:08:33,540
‫passport.

81
00:08:33,930 --> 00:08:40,860
‫You must use both the uppercase and lowercase K. letters and enter.

82
00:08:42,640 --> 00:08:50,410
‫Now I'm entering my password, the pseudo password is the same if I go here and around the command system,

83
00:08:50,770 --> 00:08:52,690
‫I'll start to say, Ingenix, one more time.

84
00:08:52,960 --> 00:08:59,370
‫We can see that Ingenix is running the same on the other server.

85
00:08:59,680 --> 00:09:05,720
‫So my ansible ad hoc comment started Ingenix on both servers.

86
00:09:06,520 --> 00:09:12,010
‫Now let's see, how can we stop the Ingenix process here instead of started?

87
00:09:12,220 --> 00:09:14,770
‫I have state equals stopped.

88
00:09:18,570 --> 00:09:28,500
‫Let's check it on the sacrifice, Ingenix is inactive, and here it should also be in the inactive state.

89
00:09:30,260 --> 00:09:33,260
‫And that's exactly what I expected.

90
00:09:33,920 --> 00:09:43,370
‫That's enough for sensible, adhoc comments in the following lectures will start talking about Yamal

91
00:09:43,640 --> 00:09:45,770
‫and Sensible Playbook's.


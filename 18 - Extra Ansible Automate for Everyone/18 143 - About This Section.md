This section on Ansible as well as the next one **are offered as a BONUS** and
limited support is offered.

In my opinion Ansible is a very complex topic and deserves its own dedicated
course.

But I just wanted to show you another approach to Network Automation using a
well known framework like Ansible.


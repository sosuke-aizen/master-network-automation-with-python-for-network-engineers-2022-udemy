﻿1
00:00:00,700 --> 00:00:08,130
‫Let's take a look at other ansible modules, the setup module gathers facts about remote hosts.

2
00:00:08,620 --> 00:00:15,700
‫This module is automatically called by Playbook's Together Useful Variables about remote hosts that

3
00:00:15,700 --> 00:00:17,100
‫can be used in Playbook's.

4
00:00:17,530 --> 00:00:22,900
‫We can also use the setup module directly using an ad hoc come in.

5
00:00:23,350 --> 00:00:24,490
‫Let's see an example.

6
00:00:25,950 --> 00:00:34,420
‫And Cebull minus I hosts it will use the inventory file from the current working directory service.

7
00:00:34,530 --> 00:00:41,010
‫I want to retrieve information about all servers minus M setup.

8
00:00:46,830 --> 00:00:47,310
‫OK.

9
00:00:48,470 --> 00:00:52,940
‫I must also use minus you, Andre, minus K.

10
00:00:57,460 --> 00:01:01,870
‫And you can see here a lot of information about my service.

11
00:01:06,080 --> 00:01:12,800
‫There is too much information, so a good idea would be to redirect the output in a file like, say,

12
00:01:12,800 --> 00:01:15,500
‫servers in Photoed DE.

13
00:01:17,640 --> 00:01:25,830
‫And I'll open the file using the less viewer indigency of the information we can see of the IP v4 address,

14
00:01:26,010 --> 00:01:36,530
‫we can see if the IP address, we see information about API or more the architecture because date Bewes

15
00:01:36,540 --> 00:01:41,700
‫version, a tool box, Moshin, both the image and so on.

16
00:01:41,940 --> 00:01:44,390
‫A lot of useful information.

17
00:01:45,780 --> 00:01:53,680
‫The final module is used to create or remove files, seemliness or directories using the file module.

18
00:01:53,910 --> 00:01:58,950
‫We can also change file attributes like file permissions or file owner.

19
00:01:59,160 --> 00:02:01,180
‫Let's try some examples.

20
00:02:01,620 --> 00:02:06,060
‫I want to show you how to create a file on a remote note.

21
00:02:06,270 --> 00:02:16,050
‫So Ansible minus I hosts it uses the inventory file from the current working directory, the IP address

22
00:02:16,050 --> 00:02:24,510
‫of my remote host minus M file and now the argument minus A and here I have path equals and this is

23
00:02:24,510 --> 00:02:27,030
‫the path to the file that will be created.

24
00:02:27,220 --> 00:02:28,830
‫So home Andre.

25
00:02:28,830 --> 00:02:33,660
‫And let's say New York State equals touch.

26
00:02:33,840 --> 00:02:39,000
‫This means the file will be created minus you and minus K.

27
00:02:42,730 --> 00:02:50,690
‫OK, changed equals true, and the destination file has appeared on the remote server and these are

28
00:02:50,690 --> 00:02:52,710
‫other information about my file.

29
00:02:53,320 --> 00:02:58,480
‫If I go here and list the current working directory, I can see the new data file.

30
00:02:59,500 --> 00:03:07,050
‫Now, let's see how can we remove a file from a remote node instead of state Michael's touch here,

31
00:03:07,300 --> 00:03:07,540
‫right?

32
00:03:07,540 --> 00:03:09,310
‫The state equals absent.

33
00:03:10,510 --> 00:03:13,810
‫So the file must be absent on the remote note.

34
00:03:16,510 --> 00:03:19,930
‫Now I go here and there is no new dot file.

35
00:03:22,050 --> 00:03:29,430
‫If I want to create the file, having some specific permissions, I can write what he equals and like,

36
00:03:29,430 --> 00:03:31,230
‫say, four zero zero.

37
00:03:39,060 --> 00:03:47,400
‫And we can see that the new file has the four zero zero permissions, if you want to create a directory

38
00:03:47,550 --> 00:03:51,090
‫here, I have a directory like, say, my dear.

39
00:03:52,650 --> 00:03:54,960
‫And state equals directory.

40
00:03:57,080 --> 00:04:00,440
‫And let's say mode seven five five.

41
00:04:06,110 --> 00:04:09,530
‫And a directory appeared on the remote north.

42
00:04:11,770 --> 00:04:20,380
‫The copy module is used to copy files or directories from the ansible control machine to remote nodes,

43
00:04:20,680 --> 00:04:25,750
‫and we can also copy files and directories only on remote notes.

44
00:04:26,620 --> 00:04:27,720
‫Let's try an example.

45
00:04:27,970 --> 00:04:34,140
‫I'm going to copy routers, underline I don't text on each remote server.

46
00:04:34,450 --> 00:04:36,540
‫So Ansible servers.

47
00:04:36,670 --> 00:04:39,980
‫This is my group minus M copy minus.

48
00:04:40,390 --> 00:04:46,570
‫And now the argument source equals I can use an absolute or relative path.

49
00:04:46,570 --> 00:04:48,180
‫I prefer an absolute path.

50
00:04:48,190 --> 00:04:50,930
‫So home Andre Hleb Ansible.

51
00:04:50,950 --> 00:05:02,380
‫Her outbursts underline io s dot the and this equals home Andre let's say only routers dot text minus

52
00:05:02,380 --> 00:05:04,300
‫you Andre minus K.

53
00:05:07,690 --> 00:05:16,630
‫At this moment, there is no file named Robert Durst here on this server or here on this server looks

54
00:05:16,630 --> 00:05:18,430
‫around at the NSC command.

55
00:05:22,770 --> 00:05:32,190
‫And the file has been copied, we can see the rooters dot the file here on the server and also here

56
00:05:32,310 --> 00:05:33,420
‫on this server.

57
00:05:34,590 --> 00:05:40,950
‫Of course, if I run the comment one more time and people won't copy the file one more time.

58
00:05:42,290 --> 00:05:49,970
‫It checks if the file already exists on the destination and it doesn't copy it any more on the remote

59
00:05:49,970 --> 00:05:56,360
‫machine we can see here, changed equals false evicts enough for the moment.

60
00:05:56,360 --> 00:06:04,670
‫In the next lecture, I'll show you how to run companies that need administrative access or Google access.

61
00:06:04,820 --> 00:06:06,860
‫See you in just a few seconds.


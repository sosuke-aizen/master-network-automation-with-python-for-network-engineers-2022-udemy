﻿1
00:00:00,780 --> 00:00:08,460
‫In this section, we'll take a look at Ansible, Ansible is a configuration management system and it's

2
00:00:08,460 --> 00:00:09,600
‫written in Python.

3
00:00:09,900 --> 00:00:15,290
‫There are also other configuration management tools like Poppit Chef or S Steck.

4
00:00:15,540 --> 00:00:21,990
‫We use Ansible to deploy applications, configure and troubleshoot stack of our social networking devices

5
00:00:22,140 --> 00:00:23,940
‫in the most efficient way.

6
00:00:24,360 --> 00:00:30,580
‫Ansible is open source under GPL license in ansible architecture.

7
00:00:30,630 --> 00:00:38,700
‫There is a control node and the many, many notes any Linux or Mac computer that has Python installed

8
00:00:38,700 --> 00:00:40,170
‫can be a control node.

9
00:00:40,440 --> 00:00:45,180
‫Windows operating system is not supported as a control node.

10
00:00:45,180 --> 00:00:46,310
‫At this moment.

11
00:00:46,560 --> 00:00:50,820
‫We can have one or more control nodes or machines.

12
00:00:51,210 --> 00:00:58,110
‫Managed nodes are the network devices, clients or servers we manage with Ansible.

13
00:00:58,470 --> 00:01:02,550
‫Many notes are also sometimes called hosts.

14
00:01:03,000 --> 00:01:06,690
‫Ansible is not installed on many notes.

15
00:01:07,170 --> 00:01:14,490
‫In contrast to other management and automation systems, it uses an agent less architecture.

16
00:01:14,790 --> 00:01:23,220
‫No application is installed on the managed nodes and no deman process or agent is running this way.

17
00:01:23,400 --> 00:01:28,980
‫There is a minimal overhead when using Ansible on the controlling machine.

18
00:01:28,980 --> 00:01:33,810
‫Ansible consumes resources only when configuring devices.

19
00:01:34,110 --> 00:01:40,710
‫There is no demand or background process that is running when Ansible doesn't communicate with other

20
00:01:40,710 --> 00:01:41,460
‫devices.

21
00:01:41,760 --> 00:01:49,680
‫Ansible uses SSL in order to retrieve information or to send commands to the managed notes.

22
00:01:50,160 --> 00:01:59,640
‫Any system that can be configured using SSX can also be configured using Ansible if managing windows.

23
00:01:59,640 --> 00:02:04,230
‫It uses native power shell remote support instead of S.H..

24
00:02:04,740 --> 00:02:09,720
‫In the next lecture, we'll take a look at Ansible components.

25
00:02:10,290 --> 00:02:12,090
‫See you in a few seconds.


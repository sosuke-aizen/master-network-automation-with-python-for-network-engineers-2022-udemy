﻿1
00:00:00,510 --> 00:00:10,050
‫Now, Lexi, what are the components of ansible inventory is a list of managed nodes or an inventory

2
00:00:10,050 --> 00:00:13,340
‫file is also sometimes called a host.

3
00:00:13,350 --> 00:00:21,660
‫While the inventory can specify information like I.P. address, hostname or domain name for each managing

4
00:00:21,660 --> 00:00:30,470
‫note, an inventory can also organize managed nodes, creating and the nesting groups for easier scaling.

5
00:00:30,810 --> 00:00:39,720
‫It also can contain variables and can be formatted as an IMF file, which is the default or is an YAML

6
00:00:39,720 --> 00:00:40,200
‫file.

7
00:00:41,500 --> 00:00:48,100
‫Here in Ansible documentation, we can see examples of inventory files.

8
00:00:48,910 --> 00:00:55,150
‫This is, for example, an inventory or host file formatted as I and I.

9
00:00:55,990 --> 00:01:00,170
‫This list of servers will be managed by Ansible.

10
00:01:00,610 --> 00:01:04,600
‫A Yamal version of this file would look like this.

11
00:01:05,230 --> 00:01:10,060
‫We'll talk more about Yamal in a following section.

12
00:01:11,150 --> 00:01:20,630
‫Then we have modules, modules are the units of code Ansible executes each module has a particular use

13
00:01:20,630 --> 00:01:29,240
‫from administering users on a specific type of database to managing vilan interfaces on a specific type

14
00:01:29,240 --> 00:01:36,770
‫of network device or to installing, configuring and starting a specific server, like, for example,

15
00:01:36,910 --> 00:01:38,520
‫the Apache Web server.

16
00:01:38,840 --> 00:01:44,930
‫You can't run a single module with a desk or on several different modules in a playbook.

17
00:01:45,740 --> 00:01:54,050
‫Even if Ansible is written in Python, you can develop modules also in other scripting languages like

18
00:01:54,050 --> 00:01:54,450
‫Ruby.

19
00:01:55,160 --> 00:02:04,250
‫There are many ansible modules for working with databases, files, identities, messaging, networking

20
00:02:04,250 --> 00:02:05,960
‫protocols and so on.

21
00:02:06,140 --> 00:02:10,370
‫Will discuss more about modules in the next lecture's.

22
00:02:11,850 --> 00:02:20,040
‫Then we have tasks, tasks are units of action in Arcibel, you can execute a single task once with

23
00:02:20,040 --> 00:02:24,060
‫an ad hoc comment or multiple tasks in a playbook.

24
00:02:24,510 --> 00:02:28,020
‫Playbooks are ordered lists of tasks.

25
00:02:28,320 --> 00:02:32,280
‫We can run those tasks in that order repeatedly.

26
00:02:32,730 --> 00:02:39,510
‫Playbooks are written in Yamal and are easy to read, write, share and understand.

27
00:02:39,720 --> 00:02:45,480
‫Yamal is an acronym that stands for Yamal and Markup Language.

28
00:02:45,870 --> 00:02:49,030
‫Let's take a look at an example of Playbook.

29
00:02:49,500 --> 00:02:53,370
‫This is an example of playbook written in Yamal.

30
00:02:53,670 --> 00:02:59,850
‫This playbook configures IP addresses, ox's leaks and so on.

31
00:03:00,420 --> 00:03:05,100
‫Will discuss more about playbooks in the following lecture.


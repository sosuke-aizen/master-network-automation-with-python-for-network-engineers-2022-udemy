﻿1
00:00:00,530 --> 00:00:05,630
‫In this lecture, I'll show you how to install Ansible on the control machine.

2
00:00:05,930 --> 00:00:13,130
‫As I said in another previous lecture, we don't have to install Ansible on the managed notes.

3
00:00:13,130 --> 00:00:17,200
‫We install Ansible only on the control machine.

4
00:00:17,330 --> 00:00:21,320
‫You can install Ansible on Linux or on Mac.

5
00:00:21,350 --> 00:00:26,900
‫At this moment, Ansible doesn't run on Windows as a control machine.

6
00:00:27,260 --> 00:00:29,150
‫There are more possibilities.

7
00:00:29,420 --> 00:00:31,820
‫One of them is to use beep.

8
00:00:32,150 --> 00:00:35,870
‫We use beep on Mac or on Linux.

9
00:00:37,350 --> 00:00:45,630
‫I am connected using S.H. on an open to server, and here I'll install Ansible using people through

10
00:00:45,720 --> 00:00:48,240
‫the beep install Ansible.

11
00:00:48,510 --> 00:00:53,310
‫The user that runs the command must be able to.

12
00:00:53,310 --> 00:01:00,150
‫Ron Cummings is route with Sudo so that the user must belong to the SUDO group.

13
00:01:05,660 --> 00:01:09,140
‫It's downloading Ansible and installing it.

14
00:01:15,960 --> 00:01:18,840
‫OK, Ansible has been installed.

15
00:01:19,260 --> 00:01:28,950
‫We can check its version of Byronic Ansible minus minus version, or we can run Ansible playbook minus

16
00:01:28,950 --> 00:01:30,270
‫minus version.

17
00:01:32,490 --> 00:01:40,470
‫Now, I'm going to uninstall Ansible because I want to show you also how to install it using Apte,

18
00:01:40,860 --> 00:01:43,830
‫so the package manager of Ubuntu.

19
00:01:44,870 --> 00:01:50,210
‫So the beep uninstall and the name of the package.

20
00:01:50,390 --> 00:01:55,410
‫This is how we uninstall a package that has been installed with people.

21
00:01:58,660 --> 00:02:01,390
‫Yes, and it's removing Ansible.

22
00:02:03,520 --> 00:02:10,090
‫It has successfully uninstalled Ansible, and if we write Ansible, we got an error.

23
00:02:12,290 --> 00:02:22,130
‫If we are using Ubuntu, we can use the PPA, this PPA is administered by the ansible company, so it's

24
00:02:22,130 --> 00:02:22,970
‫up to date.

25
00:02:23,480 --> 00:02:31,760
‫This is a very good way of installing Ansible because it will update Ansible when we update our operating

26
00:02:31,760 --> 00:02:35,210
‫system from the ansible documentation.

27
00:02:35,390 --> 00:02:37,910
‫I'm going to run these comments.

28
00:02:39,480 --> 00:02:42,140
‫First, we want to get update.

29
00:02:45,740 --> 00:02:48,110
‫I'll copy paste this line.

30
00:02:52,740 --> 00:02:56,330
‫Then I'll add the PPA repository.

31
00:02:59,920 --> 00:03:07,720
‫OK, it has also imported the public key of the corresponding private key used for signing the package

32
00:03:08,200 --> 00:03:10,150
‫and now a bid to get update.

33
00:03:13,740 --> 00:03:22,020
‫OK, I must use Sudo so pseudo AP to get above update, otherwise I get the permission denied error

34
00:03:22,320 --> 00:03:26,820
‫and so AP to get install Ansible.

35
00:03:36,700 --> 00:03:39,880
‫And Ansible has been installed on Ubuntu.

36
00:03:40,270 --> 00:03:45,460
‫Now let's check its version by running Ansible minus minus version.

37
00:03:49,300 --> 00:03:52,630
‫And sensible playbook, minus minus version.

38
00:03:55,060 --> 00:04:03,940
‫And we've installed Ansible successfully, of course, you install it either by using beep or if you

39
00:04:03,940 --> 00:04:10,090
‫are using Ubuntu, you can install it using AP Target and the PDPA Repository.


﻿1
00:00:00,570 --> 00:00:01,390
‫Welcome back.

2
00:00:02,010 --> 00:00:09,090
‫In this example, I want to show you how to run multiple clients in parallel and process the results

3
00:00:09,300 --> 00:00:11,640
‫when all of them have completed.

4
00:00:12,570 --> 00:00:21,720
‫Imagine that you have many servers and you want to automate their configuration concurrently using async

5
00:00:21,720 --> 00:00:21,900
‫I.

6
00:00:21,900 --> 00:00:23,970
‫O and async essayistic.

7
00:00:24,980 --> 00:00:26,240
‫I'm creating a new script.

8
00:00:37,550 --> 00:00:45,110
‫The first thing I'm going to define will connect and authenticate using its message to the message server

9
00:00:45,290 --> 00:00:46,340
‫and run a comment.

10
00:00:47,730 --> 00:00:54,870
‫Async D.F. on client and the arguments are hosted.

11
00:00:55,820 --> 00:01:07,350
‫This is the IP address of the remotest Dhiman username, password and command and async with async SSX

12
00:01:07,980 --> 00:01:14,430
‫that connect up and the arguments exactly like in the first script.

13
00:01:14,790 --> 00:01:17,460
‫I can even copy and paste this part.

14
00:01:21,000 --> 00:01:33,780
‫And return a connection that run off, come in, perfect, this captain will run this comment on this

15
00:01:33,780 --> 00:01:34,140
‫host.

16
00:01:35,280 --> 00:01:42,600
‫Now I'm defining the top level competition, which will be the async application starting point, this

17
00:01:42,620 --> 00:01:50,310
‫KOTIN will take a list of hosts and call or await the unclimbed KOTIN for each host.

18
00:01:51,340 --> 00:01:56,080
‫So async D.F. run multiple clients.

19
00:01:58,980 --> 00:02:01,110
‫And the single argument hosts.

20
00:02:02,340 --> 00:02:11,160
‫One remark in the previous hazing, your example is the name of the top level Kotin was mine, but mine

21
00:02:11,160 --> 00:02:14,610
‫is not accurate and I can name it any way I wish.

22
00:02:16,120 --> 00:02:26,530
‫This Kotin will spawn a task for each SSX connection, so I'm creating an empty list for the tasks like,

23
00:02:26,600 --> 00:02:30,600
‫say, tasks equals empty list.

24
00:02:31,510 --> 00:02:38,980
‫I'm iterating over the hosts, calling their on client routine and adding the newly created task to

25
00:02:38,980 --> 00:02:39,520
‫the list.

26
00:02:40,790 --> 00:02:48,920
‫So for hosting hosts, desk equals one client and the arguments.

27
00:02:50,130 --> 00:02:57,660
‫By the way, a host in the host's list is the dictionary that has the host, the username, the password,

28
00:02:57,810 --> 00:02:59,760
‫and the comment is Keith.

29
00:03:00,810 --> 00:03:07,530
‫You'll see in a minute how it looks like, so if it's a dictionary, I can get the value of a specific

30
00:03:07,530 --> 00:03:09,540
‫key like this host.

31
00:03:10,110 --> 00:03:17,580
‫This is the name of the dictionary, which is an element of the list of another key, which is also

32
00:03:17,610 --> 00:03:18,050
‫host.

33
00:03:18,360 --> 00:03:23,070
‫This is the first argument then host of username.

34
00:03:24,200 --> 00:03:25,760
‫Host of Passport.

35
00:03:28,620 --> 00:03:30,330
‫And host of comment.

36
00:03:32,890 --> 00:03:36,490
‫I'm upending the new task to the tasks at least.

37
00:03:37,350 --> 00:03:40,230
‫So tasks that a band of task.

38
00:03:42,300 --> 00:03:50,520
‫Outside the loop, I'm signaling the cargo things to run as soon as possible by giving the tasks so

39
00:03:50,530 --> 00:03:57,330
‫results equals 08, a scenario that gather.

40
00:04:00,960 --> 00:04:10,080
‫Oh, star desks in Dili, the second argument, written exceptions equals two, I wanted to raise an

41
00:04:10,080 --> 00:04:11,940
‫exception if it's the case.

42
00:04:13,810 --> 00:04:21,730
‫By default, this is false async, I ordered, gather and aggregate the list of return to values.

43
00:04:22,210 --> 00:04:25,720
‫So I'm going to iterate over these return values like these.

44
00:04:27,700 --> 00:04:36,700
‫For result in results and if the result was an exception, then I pointed out if he's instance.

45
00:04:37,720 --> 00:04:40,030
‫Result and exception.

46
00:04:40,570 --> 00:04:42,310
‫So if it was an exception.

47
00:04:43,470 --> 00:04:47,130
‫This is an error at runtime, then print.

48
00:04:48,590 --> 00:04:52,820
‫If think and they say something like this Basque.

49
00:04:54,400 --> 00:05:02,700
‫I this is the number of the task, and I'll need another variable, a temporary variable called I I

50
00:05:02,710 --> 00:05:07,510
‫equals zero and in the for loop I'm incrementing it.

51
00:05:08,770 --> 00:05:11,460
‫So the number of the task failed.

52
00:05:12,820 --> 00:05:15,190
‫It was an exception, so anti-American.

53
00:05:17,340 --> 00:05:26,610
‫And the exception is the result ayliffe, a result that makes it status.

54
00:05:28,640 --> 00:05:30,140
‫He's not equal to zero.

55
00:05:30,560 --> 00:05:38,510
‫That means there was an error on Linux, each comment, his return status, and if it's zero, it means

56
00:05:38,570 --> 00:05:39,440
‫there was no error.

57
00:05:40,400 --> 00:05:43,480
‫So different from zero Maysara.

58
00:05:45,490 --> 00:06:00,370
‫I'm printing the task number and the exit status, so print f string task I exited with status result

59
00:06:00,370 --> 00:06:02,920
‫that exit underline status.

60
00:06:04,740 --> 00:06:09,420
‫And I'm also pointing out the result that is that the doubler.

61
00:06:17,300 --> 00:06:23,240
‫And the springs for the case where neither an exception nor an error water raised.

62
00:06:24,420 --> 00:06:25,250
‫So print.

63
00:06:26,890 --> 00:06:30,070
‫Task I succeeded.

64
00:06:33,820 --> 00:06:36,390
‫And print result, that is the dot.

65
00:06:42,670 --> 00:06:44,980
‫And I'm also pointing out 50.

66
00:06:46,300 --> 00:06:49,300
‫I want to see each iteration clearly.

67
00:06:51,430 --> 00:06:58,070
‫Now it's time to call a that one, which is the entrance point of the async app.

68
00:06:58,600 --> 00:07:04,690
‫But before doing that, I'll create a list of dictionaries for each host on which I run.

69
00:07:04,690 --> 00:07:08,170
‫Cumming's using SSX, so hosts.

70
00:07:09,330 --> 00:07:16,950
‫And XLE of dictionaries, the first dictionary in the list is host Kolon and the IP address.

71
00:07:17,950 --> 00:07:20,620
‫This is the IP address of the Linux VM.

72
00:07:21,830 --> 00:07:27,440
‫192 one hundred sixty eight zero and 50.

73
00:07:28,840 --> 00:07:31,300
‫User name student.

74
00:07:34,740 --> 00:07:35,580
‫Password.

75
00:07:36,640 --> 00:07:37,360
‫Student.

76
00:07:40,780 --> 00:07:41,650
‫And commend.

77
00:07:43,770 --> 00:07:46,350
‫A random Linux comment, I have config.

78
00:07:48,080 --> 00:07:52,850
‫This is the first dictionary or the first host, the second one.

79
00:07:53,750 --> 00:08:01,280
‫OK, I have a single Linux VM, so I'm going to spawn a necessary task that tries to comment on the

80
00:08:01,280 --> 00:08:01,910
‫same host.

81
00:08:02,270 --> 00:08:08,240
‫But if you have more hosts, you can use their corresponding IP address.

82
00:08:09,780 --> 00:08:12,780
‫In my case, I'm copying this line.

83
00:08:14,270 --> 00:08:20,120
‫And pasting it below, and I'm just changing the comment, who?

84
00:08:21,650 --> 00:08:23,300
‫And your name minus.

85
00:08:25,210 --> 00:08:33,210
‫And finally, a scenario that the run off run multiple clients of hosts.

86
00:08:34,690 --> 00:08:37,390
‫The list of dictionaries, let's run it.

87
00:08:43,890 --> 00:08:51,480
‫And we see how it successfully connected and authenticated to the open SSX demon and Trenda Cumming's

88
00:08:51,630 --> 00:08:52,830
‫asynchronously.

89
00:08:56,540 --> 00:08:59,960
‫If there is an error, you'll see it like this.

90
00:09:02,890 --> 00:09:10,960
‫The last task exited with status one hundred twenty seven and their name is Commander Not Found.

91
00:09:12,230 --> 00:09:18,110
‫That's how you run a multiple six connections on multiple servers asynchronously.


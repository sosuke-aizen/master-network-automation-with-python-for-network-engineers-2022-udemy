﻿1
00:00:00,810 --> 00:00:08,670
‫Welcome back, Async Chayo is a concurrent programming design or paradigm like threading or multiprocessing,

2
00:00:09,060 --> 00:00:15,050
‫it's language agnostic, which means there are implementations in multiple programming languages.

3
00:00:15,540 --> 00:00:18,720
‫It's not something specific to Python async.

4
00:00:18,720 --> 00:00:23,430
‫I o is a style of concurrent programming, but is not parallelism.

5
00:00:23,650 --> 00:00:25,500
‫It's closer to threading.

6
00:00:25,920 --> 00:00:32,760
‫Instead of spawning frags, we say that we found desks and async.

7
00:00:32,760 --> 00:00:41,790
‫IO is the Python standard library that provides support for implementing a scenario using async await

8
00:00:41,790 --> 00:00:42,600
‫syntax.

9
00:00:43,530 --> 00:00:49,890
‫It's a perfect fit to write concurrent code for ironbound and high level structured network code.

10
00:00:50,400 --> 00:00:54,990
‫If the code is CPU bound, consider using multiprocessing instead.

11
00:00:55,350 --> 00:00:59,220
‫Async I o and multiprocessing can be used together.

12
00:00:59,640 --> 00:01:06,750
‫Async IO avoids some of the potential issues that you might otherwise encounter with the traditional

13
00:01:06,750 --> 00:01:07,700
‫threading design.

14
00:01:08,370 --> 00:01:13,020
‫One of the key terms used in async i o apps is called routine.

15
00:01:13,490 --> 00:01:19,410
‫Coatings short for cooperative subroutines are similar to generators.

16
00:01:19,780 --> 00:01:22,680
‫They are in fact specialized to generate their functions.

17
00:01:22,900 --> 00:01:24,960
‫They can be paused and resumed.

18
00:01:25,320 --> 00:01:31,260
‫However, generators generate various are producers and qualities.

19
00:01:31,260 --> 00:01:33,960
‫Consumer values are consumers.

20
00:01:34,620 --> 00:01:41,670
‫A quality is a function that can suspend its execution before reaching return, and it can indirectly

21
00:01:41,670 --> 00:01:45,660
‫pass control to another KOTIN for some time.

22
00:01:47,480 --> 00:01:55,400
‫Let's move to buy time and see some practical examples, I also want to say that async Cayo in Python

23
00:01:55,400 --> 00:01:56,280
‫is not easy.

24
00:01:56,600 --> 00:02:03,460
‫Prepare yourself to invest some time and resources in these concepts to deeply understand them.

25
00:02:04,920 --> 00:02:13,110
‫I'm importing the Python standard library called Async Io that provides support for implementing concurrent

26
00:02:13,140 --> 00:02:18,180
‫code using the async await syntax import async IO.

27
00:02:19,740 --> 00:02:24,840
‫I'm also imparting the time module I needed in this example.

28
00:02:26,370 --> 00:02:29,940
‫Let's start with the classical Synchronoss code.

29
00:02:33,690 --> 00:02:40,050
‫So, D.F., I'm defining a function, seeing underlying if a synchronous function, a normal function.

30
00:02:41,170 --> 00:02:45,130
‫And inside the function, I'm printing one.

31
00:02:46,970 --> 00:02:49,770
‫And equals and an empty string.

32
00:02:49,910 --> 00:02:54,110
‫I do not want a backslash and so on your line after the string one.

33
00:02:56,580 --> 00:03:01,300
‫I'm simulating an expensive task, like working with an external resource.

34
00:03:01,590 --> 00:03:09,360
‫I wanted to wait for one second, so time that sleep of one and then print to.

35
00:03:11,530 --> 00:03:13,150
‫And equally empty string.

36
00:03:14,890 --> 00:03:16,320
‫That's my Synchronoss function.

37
00:03:17,110 --> 00:03:25,300
‫Next, I'll define a routine for the async code, KWARTIN is just a function defined using the async

38
00:03:25,300 --> 00:03:25,870
‫keyword.

39
00:03:28,680 --> 00:03:30,600
‫So async D.F..

40
00:03:31,770 --> 00:03:33,250
‫Async underline if.

41
00:03:36,010 --> 00:03:37,450
‫Print one.

42
00:03:41,360 --> 00:03:50,260
‫And I'm sleeping for one second, instead of calling time that sleep, I'll call another function from

43
00:03:50,270 --> 00:03:57,980
‫the AC module and I'll await a scenario that sleep of one.

44
00:03:59,150 --> 00:04:08,210
‫The async I o that sleep kotin, it's useful for simulating blocking Io inside the Kotin, we are not

45
00:04:08,210 --> 00:04:15,500
‫allowed to call a simple function like that sleep, but we are allowed only to call another Kotin and

46
00:04:15,500 --> 00:04:19,600
‫async Io that sleep is according to color quality.

47
00:04:19,760 --> 00:04:21,020
‫You must await it.

48
00:04:21,770 --> 00:04:26,060
‫We don't just call it like a normal function to do that.

49
00:04:26,210 --> 00:04:29,630
‫We use the wait quiet before the function call.

50
00:04:30,320 --> 00:04:38,900
‫If a call has an wait on it, then you know that it can be a place where your task will be suspended.

51
00:04:39,410 --> 00:04:46,910
‫This is a key concept of async applications and I want to emphasize it with a short example.

52
00:04:47,940 --> 00:04:51,690
‫I'm defining a new function async D.F. G.

53
00:04:54,050 --> 00:05:06,890
‫And inside, gee, I write a weight if and if he's another Cockerton, I think the F F and pass.

54
00:05:07,340 --> 00:05:17,780
‫So an empty function when Python encounters a weight, if it calls F in suspense of the execution of

55
00:05:17,780 --> 00:05:20,460
‫G until F witness.

56
00:05:20,810 --> 00:05:29,370
‫In the meantime, it something else run X like say pause here and come back to G when if he's ready.

57
00:05:30,170 --> 00:05:34,880
‫Note that you can only use a weight in the body of coatis.

58
00:05:36,190 --> 00:05:43,870
‫In fact, the eight cure at best, is functional control back to the event loop, or in other words,

59
00:05:43,870 --> 00:05:48,940
‫it suspends the execution of the surrounding community in this case of.

60
00:05:58,780 --> 00:06:04,250
‫OK, this was just a short example to make it clear, I'll comment it out.

61
00:06:06,070 --> 00:06:08,710
‫Let's return to our asynchronous function.

62
00:06:10,320 --> 00:06:15,630
‫After sleeping for one second, I'll print to print the.

63
00:06:16,950 --> 00:06:25,740
‫And and equally empty string, this asynchronous function implements the same functionality as the synchronous

64
00:06:25,740 --> 00:06:25,980
‫one.

65
00:06:27,080 --> 00:06:34,310
‫Let's recap, we have two functions, a classical one in the quarantine, inside the quarantine, we

66
00:06:34,310 --> 00:06:37,490
‫call or wait another quarantine.

67
00:06:38,760 --> 00:06:46,350
‫In async EPP's, there is the concept of async, a starting point, a.k.a. top level chromatin.

68
00:06:47,950 --> 00:06:54,580
‫So with the application, starting point is another Kotin, this is usually a coating called the main.

69
00:06:56,920 --> 00:07:05,770
‫So async D.F. Main Eakes of the async application starting point and inside this top level quittin,

70
00:07:06,040 --> 00:07:12,340
‫I'll define a task which is besides quoting another away table object.

71
00:07:13,510 --> 00:07:22,000
‫Note that there are three equatable objects, these tasks and futures I've already talked about goatees

72
00:07:22,270 --> 00:07:29,950
‫and an async future is a low level object that acts as a placeholder for data that hasn't been calculated

73
00:07:29,950 --> 00:07:30,910
‫or fixed yet.

74
00:07:31,810 --> 00:07:38,910
‫It's used mainly by framework and library developers, starting with Python 3.0.

75
00:07:39,340 --> 00:07:42,550
‫You don't need to create a future object directly.

76
00:07:43,740 --> 00:07:52,290
‫And that is the task, what is a task, an async, your task is used to several committees concurrently.

77
00:07:52,710 --> 00:08:00,280
‫It's an object that wraps a courtin, providing methods to control its execution and query its status.

78
00:08:00,660 --> 00:08:03,150
‫A task might be created with async.

79
00:08:03,150 --> 00:08:07,910
‫I would not create task or async IO that Chiavari methods.

80
00:08:08,250 --> 00:08:17,130
‫Let's create a list of tasks which consists of three calls to our routine called async underlinings.

81
00:08:17,580 --> 00:08:21,000
‫So tasks he calls list.

82
00:08:21,450 --> 00:08:26,130
‫And inside the list I'll call the asynchronous function three times.

83
00:08:27,390 --> 00:08:33,600
‫So these calls are police elements, so async underline if.

84
00:08:35,750 --> 00:08:42,090
‫One call, I think, underline if the second call and the last call.

85
00:08:43,970 --> 00:08:46,850
‫We could get a similar result by doing this.

86
00:08:48,500 --> 00:08:58,460
‫So instead of three calls to the same function, you could write async, underline F for underline in

87
00:08:58,460 --> 00:08:59,710
‫the range of three.

88
00:09:00,380 --> 00:09:02,510
‫In fact, this is a list comprehension.

89
00:09:04,880 --> 00:09:13,070
‫So these two lines are equivalent, high comment out this one, and I'm signaling the thing to run as

90
00:09:13,070 --> 00:09:20,600
‫soon as possible by giving the tasks like this await a scenario that gather.

91
00:09:21,940 --> 00:09:26,500
‫And his argument so far and the last name, tussocks.

92
00:09:28,750 --> 00:09:36,910
‫This is the top level quarantine or the application starting point, the entrance point of any async

93
00:09:36,910 --> 00:09:37,060
‫I.

94
00:09:37,060 --> 00:09:41,030
‫O program is async biodata run and a quarantine.

95
00:09:41,050 --> 00:09:44,680
‫His argument where the quarantine is the top level quarantine.

96
00:09:45,340 --> 00:09:45,730
‫So.

97
00:09:46,910 --> 00:09:50,270
‫A scenario that the run of main.

98
00:09:52,400 --> 00:10:00,170
‫Async Biodata Run was introduced in Python, VEDAT seven and calling it creates an event loop and the

99
00:10:00,240 --> 00:10:02,550
‫cockatiel will run on it.

100
00:10:02,990 --> 00:10:07,730
‫The event loop is the core of any async cayo application.

101
00:10:08,830 --> 00:10:18,610
‫It's the one that runs asynchronous tasks, performs network operations or runs subprocesses.

102
00:10:20,040 --> 00:10:25,650
‫OK, voxel the next, our asynchronous application, I'm running the program.

103
00:10:30,550 --> 00:10:38,530
‫And it pointed out one one one three times, one and three times to highlight the space here, it's

104
00:10:38,530 --> 00:10:39,760
‫better this way.

105
00:10:40,150 --> 00:10:41,350
‫You will see it clear.

106
00:10:45,080 --> 00:10:45,660
‫Perfect.

107
00:10:46,160 --> 00:10:53,900
‫Now, let's add the Synchronoss code, I want to compare the Synchronoss code to the asynchronous one.

108
00:10:54,830 --> 00:10:57,620
‫So I'll call this function three times.

109
00:10:59,860 --> 00:11:06,790
‫So far, underline in the range of three, Colin Sync underline underlinings.

110
00:11:07,840 --> 00:11:13,450
‫And I'll point out a new line here, a backslash, and I'm writing the script again.

111
00:11:19,090 --> 00:11:26,680
‫This is the difference, the asynchronous code printed out one three times and two, three times and

112
00:11:26,680 --> 00:11:31,200
‫the synchronous one printed out one, two, one, two, one, two.

113
00:11:31,630 --> 00:11:38,920
‫Makes a difference when the program encounters async scenario that sleep, the cockatiel is posed in

114
00:11:38,920 --> 00:11:42,190
‫another one is started async biodata.

115
00:11:42,190 --> 00:11:51,130
‫Sleep doesn't block the async underline if kotin didn't wait for async io that slipcover thing to finish

116
00:11:51,430 --> 00:11:53,890
‫before starting a new one.

117
00:11:55,880 --> 00:11:58,160
‫Let's measure the execution time.

118
00:11:59,500 --> 00:12:06,670
‫Lexi, how long does it take to run the asynchronous code so start equals time that time?

119
00:12:07,300 --> 00:12:15,370
‫The current time is a timestamp and end after the asynchronous code has finished its execution time

120
00:12:15,370 --> 00:12:22,570
‫that time and print and string literal execution time async.

121
00:12:26,750 --> 00:12:28,520
‫And miners start.

122
00:12:31,080 --> 00:12:33,390
‫And the same for the Synchronoss code.

123
00:12:35,950 --> 00:12:37,690
‫I'll copy paste the code.

124
00:12:51,800 --> 00:12:53,600
‫I'm executing the script again.

125
00:12:59,070 --> 00:12:59,910
‫Very nice.

126
00:13:01,030 --> 00:13:10,390
‫The asynchronous code took one second and the synchronous one three seconds, it took three times longer.

127
00:13:11,660 --> 00:13:19,340
‫OK, that's all for the moment, and I know it wasn't easy at all, async COYO is rather difficult and

128
00:13:19,340 --> 00:13:27,530
‫it's built around concepts such as Karate's equatable, Apple's event loops or fut. at first sight.

129
00:13:27,770 --> 00:13:30,820
‫Even the terminology can be intimidating.

130
00:13:31,190 --> 00:13:38,360
‫I can honestly tell you that I spent some time studying and exercising it to deeply understand how they

131
00:13:38,360 --> 00:13:39,110
‫work together.


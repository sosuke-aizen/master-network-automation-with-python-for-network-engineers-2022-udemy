﻿1
00:00:00,860 --> 00:00:07,460
‫When it comes to ethics and the network automation, Bahrami is the Python library of choice in most

2
00:00:07,460 --> 00:00:08,060
‫cases.

3
00:00:08,780 --> 00:00:15,980
‫The problem is it doesn't support this scenario, but only trading, which is not good for scaling concurrency

4
00:00:16,010 --> 00:00:18,710
‫of network IO async.

5
00:00:18,720 --> 00:00:26,500
‫SSX is a Python library which provides an asynchronous client and server implementation of the Sage

6
00:00:26,600 --> 00:00:30,710
‫Vita protocol on top of the async IO framework.

7
00:00:31,800 --> 00:00:41,460
‫He has full support for his message to SFP and SICP client and server functions and allows multiple

8
00:00:41,460 --> 00:00:45,320
‫simultaneous sessions on a single SSX connection.

9
00:00:46,530 --> 00:00:51,000
‫You can read more about it's official documentation at this address.

10
00:00:52,690 --> 00:01:00,170
‫For this lab, we need a Linux OS, the pricing of VM or is a main OS on another machine.

11
00:01:00,730 --> 00:01:09,050
‫We also need SSX access from Windows We a Python scripts run to the Linux VM before starting coding.

12
00:01:09,070 --> 00:01:15,220
‫I advise you to check the network and SSX connection between Windows and Linux.

13
00:01:16,690 --> 00:01:19,160
‫I'm running Linux Mint in a box.

14
00:01:19,510 --> 00:01:22,300
‫The network is put into breeched mode.

15
00:01:24,750 --> 00:01:26,280
‫You can check this as well.

16
00:01:27,610 --> 00:01:32,320
‫And I'll check the network connection, let's see its IP address.

17
00:01:33,620 --> 00:01:35,750
‫And from Windows, I'll bring it.

18
00:01:41,820 --> 00:01:42,780
‫Bing is asking.

19
00:01:43,650 --> 00:01:46,800
‫And I'll also check SSX using Patti.

20
00:01:56,780 --> 00:01:58,910
‫S.A.G. is working as well.

21
00:02:02,640 --> 00:02:10,080
‫By the way, if you don't have a Linux VM, I'll attach my VM is an overlay file is a resource for this

22
00:02:10,080 --> 00:02:13,620
‫lecture you can quickly imported in a virtual box.

23
00:02:14,740 --> 00:02:21,670
‫In this script, I'm going to connect and authenticate to the Linux VM using async S.H. and TransAm

24
00:02:21,670 --> 00:02:22,210
‫Cumming's.

25
00:02:23,510 --> 00:02:28,940
‫Async, as they say, is not a Python standard library, so I have to install it file.

26
00:02:29,880 --> 00:02:30,720
‫Settings.

27
00:02:31,970 --> 00:02:32,900
‫Interpretor.

28
00:02:35,290 --> 00:02:39,430
‫Project interpreter and I'm searching for the module.

29
00:02:42,840 --> 00:02:46,620
‫I'm installing the package async SSX.

30
00:02:48,490 --> 00:02:51,070
‫And it was installed successfully.

31
00:02:54,060 --> 00:02:59,160
‫I'm importing a single and a 66.

32
00:03:01,960 --> 00:03:13,480
‫I'm defining a Kotin called Connecter on async def, connect and run and eeks arguments are host.

33
00:03:13,490 --> 00:03:19,600
‫This will be the IP address of the demon username password.

34
00:03:20,450 --> 00:03:24,220
‫They will be used for authentication and the comment.

35
00:03:24,370 --> 00:03:29,920
‫I'm going to run on the server to authenticate to the remote SSX daemon.

36
00:03:30,100 --> 00:03:36,910
‫I have to call the async SSX that connect the method with an async with statement.

37
00:03:38,220 --> 00:03:40,470
‫So I think with.

38
00:03:41,700 --> 00:03:44,370
‫Async is suspect that connect.

39
00:03:46,710 --> 00:03:49,980
‫And the arguments host equals host.

40
00:03:51,640 --> 00:04:01,510
‫One woman here, host on the left side of the equals sign is the method argument and host on the right

41
00:04:01,510 --> 00:04:05,530
‫hand side of the equals sign is my function's argument.

42
00:04:05,650 --> 00:04:08,770
‫If I wish, I can write it here and here.

43
00:04:08,770 --> 00:04:09,640
‫It is the same.

44
00:04:13,730 --> 00:04:19,160
‫Username equals username, password equals password.

45
00:04:20,140 --> 00:04:29,260
‫And now hosts equals none, I do not want to use the noun hosts file, if you want to verify the identity

46
00:04:29,260 --> 00:04:39,430
‫of the server, you can use the noun hosts file a connection, and I run a single command to run a comment

47
00:04:39,550 --> 00:04:42,930
‫on the remote device on the remote VM.

48
00:04:43,330 --> 00:04:46,750
‫I'll call the random method of the connection object.

49
00:04:47,900 --> 00:04:57,950
‫This is a routine, so in order to call it, I have to await it, await connection that run and the

50
00:04:57,950 --> 00:05:00,350
‫command the functions argument.

51
00:05:01,440 --> 00:05:06,690
‫I'll take the return value in a variable like, say, the result equals.

52
00:05:07,880 --> 00:05:09,350
‫And I'm returning it.

53
00:05:09,890 --> 00:05:11,170
‫Return result.

54
00:05:12,880 --> 00:05:13,270
‫Great.

55
00:05:13,710 --> 00:05:14,770
‫This is the function.

56
00:05:15,780 --> 00:05:24,360
‫Let's see how he talks, I'm defining a variable command equals and the Linux command, for example,

57
00:05:24,360 --> 00:05:29,370
‫I have configured it, will display information about the network interfaces.

58
00:05:30,530 --> 00:05:41,750
‫And the result equals a single doctor on the application starting point, it starts the async event

59
00:05:41,750 --> 00:05:42,110
‫loop.

60
00:05:42,990 --> 00:05:45,870
‫And here, the method connecting Ron.

61
00:05:48,220 --> 00:05:49,450
‫And the article makes.

62
00:05:51,390 --> 00:05:58,110
‫Let's say one hundred ninety to one hundred sixty eight, that's zero.

63
00:05:58,140 --> 00:06:09,420
‫That 50, the Linux IP address, the username student and the password is the same student and comment.

64
00:06:10,540 --> 00:06:18,140
‫The result variable is not the type estar or string, it has more components like standard output and

65
00:06:18,140 --> 00:06:20,170
‫the standard bearer of the comment.

66
00:06:21,290 --> 00:06:27,650
‫So print and an F string like suggested the out call on backslash N.

67
00:06:29,700 --> 00:06:40,010
‫And between curly braces result, that is the diet is the diet is an attribute of the result object.

68
00:06:41,250 --> 00:06:43,860
‫And the same for the doubler.

69
00:06:50,520 --> 00:06:51,570
‫I'm writing the script.

70
00:06:56,330 --> 00:06:56,870
‫Great.

71
00:06:57,700 --> 00:07:03,250
‫See how it has authenticated to the remotest demon and run the command.

72
00:07:04,250 --> 00:07:11,450
‫This is the output, the standard output of the command, and there is no error, of course, if there

73
00:07:11,450 --> 00:07:16,930
‫is an error like this, this is a nonexisting comment on Linux and I run the script again.

74
00:07:17,180 --> 00:07:26,600
‫I see the error here, comment not found or if there is a permission denied there like that, it is

75
00:07:26,630 --> 00:07:27,140
‫a shadow.

76
00:07:35,060 --> 00:07:43,010
‫Let's go ahead and see how to run multiple comments, not only one, so right here, Cummings, instead

77
00:07:43,010 --> 00:07:47,450
‫of comment in time, commenting out of these two lines of code.

78
00:07:50,390 --> 00:07:58,070
‫Run multiple comments, this will be a list or a couple of comments, so I'll iterate over it.

79
00:07:58,530 --> 00:08:02,450
‫So for CMD Incomings.

80
00:08:04,610 --> 00:08:07,430
‫Colin and the four block of code.

81
00:08:08,480 --> 00:08:16,510
‫Result equals 08 connections that run the same method of CMT.

82
00:08:17,760 --> 00:08:25,190
‫And because there will be more results, we have more coming to run, I led the result into a list.

83
00:08:26,420 --> 00:08:33,770
‫I'm creating an empty list, let's say, results and results that append.

84
00:08:37,710 --> 00:08:38,170
‫Result.

85
00:08:41,400 --> 00:08:45,960
‫And outside the loop, I'm returning Driesell XLE.

86
00:08:51,720 --> 00:08:57,240
‫Now, instead of a single comment, I need a list with multiple comix.

87
00:08:59,200 --> 00:09:04,570
‫So X equals and the least charitable acceptable.

88
00:09:06,360 --> 00:09:13,560
‫This is the first comment I have config, the second commander who minus a.

89
00:09:15,480 --> 00:09:17,500
‫And the last comment, the third one.

90
00:09:17,910 --> 00:09:19,030
‫Your name minus.

91
00:09:20,100 --> 00:09:22,260
‫You can choose whatever comment you like.

92
00:09:23,470 --> 00:09:30,940
‫The name of the argument is Cumming's, and here results now the results will be released.

93
00:09:31,180 --> 00:09:35,530
‫So I have to iterate over the list and take out each result.

94
00:09:36,740 --> 00:09:46,370
‫So for result in results, Colleen, and I'm indenting these two lines of code like this.

95
00:09:47,480 --> 00:09:56,240
‫And to see it better, I pointed out 50 hashas epidemic like this, like on the script.

96
00:09:59,180 --> 00:09:59,750
‫Perfect.

97
00:10:01,290 --> 00:10:09,660
‫This is the first command standard output and standardbearer the second command in the last one.

98
00:10:10,140 --> 00:10:14,730
‫And of course, if there is an error, we'll see the error like this.

99
00:10:15,000 --> 00:10:15,780
‫This is an error.

100
00:10:16,880 --> 00:10:19,190
‫That comment does not exist on Linux.

101
00:10:22,900 --> 00:10:23,540
‫Thank you.

102
00:10:23,740 --> 00:10:30,100
‫That's all you now have the knowledge to use a stage in asynchronous network applications.


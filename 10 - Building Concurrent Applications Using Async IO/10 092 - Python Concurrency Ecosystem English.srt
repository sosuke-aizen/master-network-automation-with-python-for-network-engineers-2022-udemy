﻿1
00:00:00,890 --> 00:00:04,510
‫Let's take a look at the Python concurrency ecosystem.

2
00:00:05,480 --> 00:00:13,610
‫Parallelism consists of performing multiple operations at the same time, and it implies multicore SPUs.

3
00:00:14,540 --> 00:00:21,230
‫This is good for our CPU bound operations, like performing computations with numbers, image processing

4
00:00:21,230 --> 00:00:25,820
‫or encryption, not that concurrency does not imply parallelism.

5
00:00:26,390 --> 00:00:30,100
‫We can have concurrency even with a single course.

6
00:00:30,110 --> 00:00:39,410
‫If you just think of all the CPUs that were released before 2005 when the first commercial multicore

7
00:00:39,420 --> 00:00:47,510
‫CPU was made available to the public, the standard the multiprocessing module implements parallelism

8
00:00:47,510 --> 00:00:48,260
‫in Python.

9
00:00:49,330 --> 00:00:51,460
‫Let's take a look at threading.

10
00:00:52,090 --> 00:01:00,340
‫It's a concurrent execution model whereby multiple threads take turns executing tasks, multiple threads

11
00:01:00,340 --> 00:01:02,440
‫run inside a single process.

12
00:01:02,950 --> 00:01:10,570
‫Note that threads do not run in parallel execution does not occur simultaneously on multiple physical

13
00:01:10,570 --> 00:01:11,040
‫course.

14
00:01:11,740 --> 00:01:19,870
‫This is good for Ironbound tasks which imply some waiting time, like working with an external web server

15
00:01:19,990 --> 00:01:25,450
‫or a database, configuring networking devices, or even working with Phylis.

16
00:01:25,900 --> 00:01:30,910
‫The Python program should wait for these external resources to be ready.

17
00:01:31,780 --> 00:01:37,780
‫The primary downside to Python upgrading our memory safety and race conditions.

18
00:01:38,230 --> 00:01:43,720
‫All child threats of apparent process operating the same shared memory space.

19
00:01:45,030 --> 00:01:49,500
‫The standard operating model implements writing in Python.

20
00:01:50,630 --> 00:01:58,490
‫And finally, there's a scenario which is a single threaded single process designed that uses cooperative

21
00:01:58,490 --> 00:02:07,720
‫multitasking, cooperative means no intervention, and each task or KOTIN decides when to give up control.

22
00:02:08,700 --> 00:02:17,370
‫Asynchronous functions are able to pause while waiting on their ultimate result and let other counties

23
00:02:17,370 --> 00:02:22,020
‫run in the meantime, it gives the look and feel of concurrency.

24
00:02:23,360 --> 00:02:31,430
‫Before going on, let's see what Synchronoss and asynchronous means, a very good explanation was at

25
00:02:31,430 --> 00:02:32,150
‫VITAC.

26
00:02:32,510 --> 00:02:41,840
‫Two thousand and seventeen, let's imagine a simultaneous chess tournament with one master and 24 players.

27
00:02:42,260 --> 00:02:50,670
‫And if there are 30 pair moves on average per game, the chess master needs five seconds per move and

28
00:02:50,690 --> 00:02:53,380
‫the opponent 50 seconds per move.

29
00:02:54,200 --> 00:02:57,590
‫How much time will the tournament take if it's run?

30
00:02:57,590 --> 00:03:07,910
‫Synchronoss one game at a time and we have fifty six plus five for each pair move times 30.

31
00:03:08,030 --> 00:03:17,150
‫The number of pair moves on average per game equals one thousand eight hundred seconds or thirty minutes

32
00:03:17,150 --> 00:03:17,720
‫per game.

33
00:03:19,140 --> 00:03:29,670
‫If there are 24 players, that means 30 minutes times, 24 players equals seven hundred and twenty minutes

34
00:03:29,880 --> 00:03:31,770
‫or 12 hours.

35
00:03:33,240 --> 00:03:34,850
‫It takes quite a long time.

36
00:03:35,760 --> 00:03:43,080
‫Now, let's take a look at the asynchronous version, how much time will the tournament take if it's

37
00:03:43,080 --> 00:03:44,660
‫run asynchronously?

38
00:03:45,360 --> 00:03:53,280
‫The chess master moves from table to table, making one move at each table, one move at each table,

39
00:03:53,460 --> 00:04:01,890
‫meet twenty four times five equals one hundred twenty seconds and thirty pair moves on every spare game

40
00:04:02,080 --> 00:04:02,580
‫times.

41
00:04:02,580 --> 00:04:10,320
‫One hundred and twenty seconds equals three thousand six hundred seconds or one hour in total.

42
00:04:11,460 --> 00:04:17,700
‫So it takes 12 hours for the Sync version and one hour for the async way.


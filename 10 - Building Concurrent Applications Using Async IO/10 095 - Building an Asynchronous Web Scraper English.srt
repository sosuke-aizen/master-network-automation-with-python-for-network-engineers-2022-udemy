﻿1
00:00:00,770 --> 00:00:01,610
‫Welcome back.

2
00:00:01,970 --> 00:00:09,180
‫If we've made it this far, it's time to build a real python application and we'll build a Web scraper.

3
00:00:09,680 --> 00:00:16,010
‫The first thing you should know is that when building asynchronous python applications, you are not

4
00:00:16,010 --> 00:00:23,570
‫allowed to use any python module, but only modules that use a way table functions or code things which

5
00:00:23,570 --> 00:00:28,300
‫are not blocking and are compatible with the async IO library.

6
00:00:29,580 --> 00:00:37,710
‫For example, when someone says HTP or Web scraping, most of the time he thinks of a library called

7
00:00:37,710 --> 00:00:44,820
‫requests, however, requests, which is, by the way, an awesome library, is not compatible with

8
00:00:44,820 --> 00:00:49,020
‫a single and cannot be used in async applications.

9
00:00:49,830 --> 00:00:58,200
‫Requests is built on top of you are a library, which in turn uses Python's EPA socket modules and by

10
00:00:58,200 --> 00:01:00,900
‫default, socket operations are blocking.

11
00:01:01,350 --> 00:01:08,940
‫Here you'll find a list of python libraries that can be used with async IO to build asynchronous applications.

12
00:01:09,890 --> 00:01:18,590
‫So instead of using requests, we lose another library called a I owe HTP, which defines a way to bulk

13
00:01:18,830 --> 00:01:19,220
‫things.

14
00:01:20,140 --> 00:01:21,280
‫Let's go to coding.

15
00:01:21,650 --> 00:01:23,710
‫I'm creating a new Python script.

16
00:01:31,450 --> 00:01:38,260
‫The program will grab a Web page from a specific model and save the source code of that Web page to

17
00:01:38,260 --> 00:01:44,530
‫a file, and it will do it asynchronously, let's import the required libraries.

18
00:01:44,890 --> 00:01:47,010
‫So import a single.

19
00:01:48,760 --> 00:01:56,410
‫Ay ay, oh, HTP is not the Python standard library, and we have to install it, so I'm installing

20
00:01:56,590 --> 00:01:58,360
‫a I o htp.

21
00:01:59,570 --> 00:02:03,230
‫File settings, interpretor.

22
00:02:04,800 --> 00:02:12,570
‫Project interpreter and I'm clicking on this plus sign, and I'm searching for a I owe HTP.

23
00:02:17,010 --> 00:02:19,740
‫I'm installing the package, the library.

24
00:02:22,800 --> 00:02:26,370
‫OK, the package was installed successfully.

25
00:02:29,420 --> 00:02:37,790
‫I'm imparting it in my application to save the Web page source code into a file, we cannot use ordinary

26
00:02:37,790 --> 00:02:40,670
‫local feio because it's blocking.

27
00:02:41,790 --> 00:02:48,710
‫To read from or a right to philes asynchronously, we have to use a library called EIO Files.

28
00:02:49,590 --> 00:02:53,470
‫This isn't a Python standard library and we have to install it.

29
00:02:53,970 --> 00:02:56,190
‫I'm installing a phylis.

30
00:03:04,530 --> 00:03:05,970
‫And I'm importing it.

31
00:03:08,850 --> 00:03:16,680
‫Next, I'll define a routine which is an ordinary function defined using the async keyboard, the coating

32
00:03:16,680 --> 00:03:25,610
‫will be called fake GICs is argument a neural and Rittgers, the e-mail code of the page from that Yorkhill.

33
00:03:26,100 --> 00:03:28,500
‫So async the FH.

34
00:03:30,160 --> 00:03:38,930
‫And the oral argument, let's write the code and then I'll explain to you in detail what it does is

35
00:03:39,070 --> 00:03:44,050
‫sync with Aio HTP, that client station.

36
00:03:46,380 --> 00:03:47,510
‫A station.

37
00:03:50,100 --> 00:03:56,030
‫And response equals a wait session target of Yorkhill.

38
00:03:59,410 --> 00:04:08,140
‫On this line, I'm calling the station that get in and as any routine, I have to await it in the content

39
00:04:08,140 --> 00:04:13,150
‫of the page XHTML equals await response.

40
00:04:13,150 --> 00:04:18,400
‫That text where text is the method of the response object.

41
00:04:18,940 --> 00:04:24,220
‫And I'm returning the variable called HTML in this piece of code.

42
00:04:24,460 --> 00:04:32,050
‫I've used some new constructions on async with exactly the same thing as the ordinary with keyword,

43
00:04:32,200 --> 00:04:40,390
‫except that where an ordinary with statement calls ordinary functions or methods an async with statement

44
00:04:40,570 --> 00:04:42,190
‫calls async methods.

45
00:04:43,030 --> 00:04:50,230
‫Something short with blocks are used to call some functions and since with async await, Python now

46
00:04:50,230 --> 00:04:51,940
‫has two kinds of functions.

47
00:04:52,120 --> 00:04:55,290
‫It also needs two kinds of with blocks.

48
00:04:56,050 --> 00:05:04,120
‫Let's define another kotin that write some text to a file async D.F. Right to file.

49
00:05:06,170 --> 00:05:12,080
‫In the first argument is the file and the second one, the text to write to that file.

50
00:05:15,310 --> 00:05:23,980
‫In this function, I'll use the advise module, so I think with a phylis.

51
00:05:27,790 --> 00:05:41,170
‫That open a file, the mode is w I'll open the file in, write them out as F and await if that right

52
00:05:41,170 --> 00:05:51,540
‫of text using a I o files is similar to using the regular python methods for working with files.

53
00:05:52,800 --> 00:05:59,620
‫Just don't forget to await each Kotin without this oh, wait, I'll get an error.

54
00:06:00,450 --> 00:06:07,380
‫We'll follow the classical pattern of an async app and will create the top level KOTIN, which is main,

55
00:06:07,830 --> 00:06:11,520
‫by the way, the name Main is not the language keyboard.

56
00:06:11,760 --> 00:06:13,740
‫You can give it any name you want.

57
00:06:15,250 --> 00:06:17,800
‫So async D.F. Main.

58
00:06:18,990 --> 00:06:28,200
‫This Kotin takes his argument a table of some Jarrell's and calls or awakes the fact kotin we've just

59
00:06:28,200 --> 00:06:36,510
‫defined for Sharelle each call to fetch will return the web page source code and another call to right

60
00:06:36,510 --> 00:06:40,810
‫to file will save the output the source code to a text file.

61
00:06:41,800 --> 00:06:52,170
‫So this is a couple of Yardley's each cottin will create a task that will be spawned a asynchronously.

62
00:06:53,210 --> 00:07:02,630
‫The tasks will be saved into a list, so tasks equals empty list, and for you are and you are Alice

63
00:07:02,870 --> 00:07:09,500
‫I'm iterating over there you are, Alice, and I'll construct the file name to which I write to the

64
00:07:09,500 --> 00:07:09,830
‫source.

65
00:07:09,830 --> 00:07:19,920
‫Code of single file equals UNEV string literal and I'll split the Yorkhill to have something like Domain,

66
00:07:19,920 --> 00:07:20,890
‫the name that.

67
00:07:21,830 --> 00:07:23,180
‫So you are Geldart split.

68
00:07:25,000 --> 00:07:27,690
‫I'm splitting by double four slashes.

69
00:07:29,020 --> 00:07:37,450
‫Taking the last element, and I'm appending that text, if you want, you can print it out and I'm calling

70
00:07:37,450 --> 00:07:45,010
‫or awaiting the fake Kotin e-mail equals a weight fake of Yorkhill.

71
00:07:46,280 --> 00:07:57,670
‫And tasks that are bent over right to file a file and estimate and outside the loop, I'll ask the captain

72
00:07:57,680 --> 00:08:07,160
‫to run as soon as possible by gathering the tasks like these await a scenario that gather of star tasks.

73
00:08:10,590 --> 00:08:17,730
‫The entrance point of any async I o program is Async I 0.01, which takes its argument the top level

74
00:08:17,740 --> 00:08:25,020
‫KOTIN, which is main async biodata, Ron was introduced in Python three to seven and calling it creates

75
00:08:25,140 --> 00:08:26,220
‫the event loop.

76
00:08:26,220 --> 00:08:27,990
‫Interests are counting on it.

77
00:08:28,500 --> 00:08:33,690
‫I'm creating a table with someone else and then I'll call async I 0.01.

78
00:08:34,780 --> 00:08:38,280
‫So you are equals in the people of Samuel.

79
00:08:51,200 --> 00:08:57,890
‫They in that the run of mane of legs, our application.

80
00:08:58,810 --> 00:09:00,370
‫And I'm running the script.

81
00:09:02,760 --> 00:09:04,590
‫OK, there is an error.

82
00:09:08,020 --> 00:09:14,800
‫OK, sorry, this is a method, so we have to call it client station and a pair of parentheses.

83
00:09:17,940 --> 00:09:19,110
‫I'm running it again.

84
00:09:22,230 --> 00:09:30,480
‫It's connecting to each Web server using HTP and grabbing the page e-mail content, which is then saved

85
00:09:30,480 --> 00:09:32,520
‫to a text file asynchronously.

86
00:09:34,740 --> 00:09:38,730
‫In the left panel, we notice three files that were created.

87
00:09:42,150 --> 00:09:50,670
‫It's the HTML source code of these Web pages, if we are to measure the execution time, we would notice

88
00:09:50,850 --> 00:09:54,990
‫that it's much faster than synchronously counterpart.


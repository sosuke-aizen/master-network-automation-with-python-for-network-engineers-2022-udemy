﻿1
00:00:01,470 --> 00:00:06,450
‫Let's dive deeper into a scenario and create a new asynchronous application.

2
00:00:06,780 --> 00:00:12,210
‫This time I want to show you how to execute Shelke makes asynchronously using async.

3
00:00:13,320 --> 00:00:19,920
‫This is especially useful for sysadmins and the network engineers since it's common Torrensville comments

4
00:00:19,920 --> 00:00:25,170
‫from Python scripts if you want to automate the configuration or a troubleshooting process.

5
00:00:26,360 --> 00:00:34,310
‫The classical approach is to use the OS or subprocess modules which execute synchronously.

6
00:00:34,820 --> 00:00:41,480
‫This means that if you run a command that needs some time to copy a file or to archive a folder, the

7
00:00:41,480 --> 00:00:48,440
‫entire program will have to wait for it to finish its execution before going ahead and execute the next

8
00:00:48,440 --> 00:00:48,950
‫command.

9
00:00:49,370 --> 00:00:51,650
‫And that is really time consuming.

10
00:00:52,370 --> 00:00:58,790
‫In this example, I'll run the script on Linux because most servers run on Linux.

11
00:00:59,150 --> 00:01:02,720
‫But of course you can run the script on Windows or Mac as well.

12
00:01:03,110 --> 00:01:07,370
‫Just replace the Linux [REMOVED]'s with Wanis for Windows.

13
00:01:08,480 --> 00:01:14,030
‫All critical routine that takes its argument, the comment that gilleran asynchronously.

14
00:01:15,390 --> 00:01:20,780
‫Async D.F. run, and it has one argument, the comment seemed.

15
00:01:21,880 --> 00:01:28,270
‫To run the comment, I'll use the great subprocess shall function of the async i o library.

16
00:01:29,020 --> 00:01:30,400
‫So o8.

17
00:01:31,480 --> 00:01:34,300
‫This is a cockatiel, so I have to await it.

18
00:01:36,420 --> 00:01:41,160
‫A scenario that create subprocess shell.

19
00:01:43,340 --> 00:01:50,870
‫And the arguments, the first argument seemed the comment to run and study out.

20
00:01:54,610 --> 00:02:06,640
‫Equals a scenario that subprocess that pipe with them in uppercase and SDD, he doubler equals a scenario

21
00:02:07,600 --> 00:02:11,830
‫that suppresses that pipe by pro-Israel.

22
00:02:14,810 --> 00:02:22,880
‫This routine will return Anestis of the Precious class, where précis is a high level rapper that allows

23
00:02:22,880 --> 00:02:27,740
‫the communications with subprocesses and watches for their completion.

24
00:02:29,980 --> 00:02:38,350
‫So, Brooke, equals the next step is to call or await the proctored, communicate kotin.

25
00:02:39,460 --> 00:02:40,120
‫Oh, wait.

26
00:02:42,030 --> 00:02:43,770
‫Broke that communicate.

27
00:02:45,390 --> 00:02:52,920
‫This is a table with two elements, the study out and the STW Doubler.

28
00:02:54,310 --> 00:03:04,240
‫Standard output and standard bearer tested the out coma, studied Doubler and the equals sign.

29
00:03:05,830 --> 00:03:14,080
‫This method of data from standard output and standard that are until end of file is reached and we expect

30
00:03:14,080 --> 00:03:20,950
‫the process to terminate each Linux comment return as a status code, which is zero if there was no

31
00:03:20,950 --> 00:03:28,000
‫error and the different from zero if there was an error in regard to the status code of the process

32
00:03:28,270 --> 00:03:39,340
‫by accessing the written code attribute of the object like this print if string cmd the command existed

33
00:03:40,540 --> 00:03:47,020
‫with status code and between curly braces proc that return code.

34
00:03:48,060 --> 00:03:52,650
‫OK, we talk in code and status code are the same, our equivalent.

35
00:03:53,760 --> 00:04:00,880
‫Now, if the comment has generated any output, we can access it by using a study out that decode.

36
00:04:01,140 --> 00:04:13,410
‫So if a study out if there is any output, print a study out, colon backslash again and a study out

37
00:04:13,410 --> 00:04:14,190
‫that decode.

38
00:04:18,940 --> 00:04:26,020
‫A study out is of type bikes and time decoding to string the default decoding scheme.

39
00:04:26,170 --> 00:04:27,040
‫Is it deviate?

40
00:04:32,140 --> 00:04:38,200
‫And the same for standardbearer, so if a study either by Baylor.

41
00:04:39,450 --> 00:04:42,360
‫If there is any error, print.

42
00:04:44,250 --> 00:04:46,110
‫As did the Eedle Baylor.

43
00:04:48,180 --> 00:04:49,020
‫And the.

44
00:04:53,650 --> 00:04:55,030
‫That was the Kotin.

45
00:04:56,300 --> 00:05:04,040
‫Next, I'll define the top level KOTIN, which is the async application starting point, this team will

46
00:05:04,040 --> 00:05:11,420
‫take a list or table of Cumming's, his argument and Sponer task asynchronously for each command.

47
00:05:12,500 --> 00:05:20,180
‫Async D.F. Main Cumming's, I'm creating an empty list of tasks.

48
00:05:22,870 --> 00:05:27,010
‫And I'm iterating over the comments, at least for ACMD Incomings.

49
00:05:28,840 --> 00:05:30,400
‫Tasks that append.

50
00:05:31,690 --> 00:05:36,820
‫And I'm calling the first Cockerton, so run of CMT.

51
00:05:39,290 --> 00:05:50,380
‫And time scheduling the Kotin to run as soon as possible by gathering the tasks await a scenario that

52
00:05:50,390 --> 00:05:50,930
‫gather.

53
00:05:54,750 --> 00:05:59,040
‫It's the same pattern we've seen before, the all.

54
00:06:00,020 --> 00:06:07,820
‫I'm going to run some random Linux comments asynchronously, so Cumming's equality and the table or

55
00:06:07,820 --> 00:06:11,840
‫a list of comments, the first comment is I have config.

56
00:06:12,560 --> 00:06:14,720
‫The second one a less.

57
00:06:15,940 --> 00:06:24,190
‫Who and you name minus a you can run any comment you wish.

58
00:06:25,270 --> 00:06:33,700
‫If you've developed the script on Windows, not the time on Linux here, you should use Windows comments

59
00:06:34,300 --> 00:06:36,670
‫like IP Config, Dear and so on.

60
00:06:37,850 --> 00:06:44,240
‫And finally, a scenario that Ron of made of Cumming's.

61
00:06:46,800 --> 00:06:48,000
‫I'm writing the script.

62
00:06:54,190 --> 00:06:59,800
‫And we see how each command got executed and its output pointed at the console.

63
00:07:01,100 --> 00:07:08,090
‫If there is any error, then the ballot object will not be empty and that the dog will be displayed.

64
00:07:09,790 --> 00:07:11,470
‫For example, this is an air.

65
00:07:13,020 --> 00:07:15,420
‫Nonexisting comment on Linux.

66
00:07:18,080 --> 00:07:20,570
‫And it pointed out the error.

67
00:07:21,670 --> 00:07:23,800
‫El doubleness not found.

68
00:07:25,870 --> 00:07:34,330
‫What's the most important remark is that the comments are on a synchronously I am executing a comment

69
00:07:34,450 --> 00:07:42,040
‫that takes some time, like sleep and five X sleeping or pausing for five seconds.

70
00:07:43,210 --> 00:07:50,890
‫The other [REMOVED]'s will get executed into their output, printed out before the sleeper command has

71
00:07:50,890 --> 00:07:53,020
‫a chance to finish the execution.

72
00:07:54,280 --> 00:08:00,520
‫The other [REMOVED]'s have been executed before the sleep command finished.

73
00:08:01,570 --> 00:08:03,890
‫This is what asynchronously means.

74
00:08:04,660 --> 00:08:06,250
‫Thank you, Rexall.


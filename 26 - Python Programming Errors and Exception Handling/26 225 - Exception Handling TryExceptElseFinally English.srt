﻿1
00:00:01,070 --> 00:00:07,970
‫In the last lecture, we've seen how an exception, which is in fact a runtime error, stops the entire

2
00:00:07,970 --> 00:00:09,140
‫script execution.

3
00:00:09,470 --> 00:00:17,750
‫Now we'll see how to handle the exception in such a way that the script continues its execution with

4
00:00:17,750 --> 00:00:18,500
‫other code.

5
00:00:19,190 --> 00:00:27,050
‫To do this will use the so-called PRAI except block for exception handling see Eakes Syntax.

6
00:00:27,830 --> 00:00:32,390
‫I'll modify the script from the previous lecture in the following way.

7
00:00:33,110 --> 00:00:36,570
‫I know that this line of code could raise an exception.

8
00:00:36,740 --> 00:00:43,130
‫So here are three colon and I'll invent the three block of code.

9
00:00:44,520 --> 00:00:52,140
‫And then except colon and a message, an exception occurred.

10
00:00:54,550 --> 00:00:56,140
‫Now, I'll run the script.

11
00:00:57,560 --> 00:01:03,470
‫I'll enter zero four, so I'll generate an exception.

12
00:01:05,300 --> 00:01:13,120
‫We can see how a message was printed, so an exception occurred, so this line printed the message,

13
00:01:13,430 --> 00:01:15,800
‫but the script didn't stop.

14
00:01:16,610 --> 00:01:18,800
‫It continued its execution.

15
00:01:18,980 --> 00:01:22,250
‫These instructions have been executed.

16
00:01:23,110 --> 00:01:31,420
‫We can think of this code as follows, try to execute the intented code below the tri close, if no

17
00:01:31,420 --> 00:01:38,770
‫exceptions occur while executing that code, then skip the except close and move on to the rest of the

18
00:01:38,770 --> 00:01:39,270
‫program.

19
00:01:40,000 --> 00:01:44,260
‫In this case, Python doesn't even look at the except close.

20
00:01:45,350 --> 00:01:54,200
‫If one exception does occur, then execute the indented code below that except statement, so the sprint

21
00:01:54,200 --> 00:02:02,710
‫instruction in this example, we see how an exception didn't stop our script, our entire script.

22
00:02:03,320 --> 00:02:11,420
‫There could be also an else clause that is executed only if the three block of code didn't trace an

23
00:02:11,420 --> 00:02:12,050
‫exception.

24
00:02:12,590 --> 00:02:19,060
‫In other words, the else block of code executes together with the block of code.

25
00:02:19,070 --> 00:02:27,260
‫If there is no error after the except clause using the same level of indentation I write else colon

26
00:02:27,620 --> 00:02:32,420
‫and the else block of code print, no error.

27
00:02:32,750 --> 00:02:42,050
‫And I'll also print the value of C print C equals and the C variable enclosed by curly braces.

28
00:02:44,200 --> 00:02:47,680
‫Let's run the script again, I'll enter zero.

29
00:02:49,050 --> 00:02:54,950
‫We see how an exception occurred and the else block of code hasn't been executed.

30
00:02:55,910 --> 00:03:04,730
‫But if I ran the script one more time and I enter four, so there is no exception, we can see how the

31
00:03:04,730 --> 00:03:12,420
‫three block of code has been executed and the else block of code has been also executed.

32
00:03:12,800 --> 00:03:17,780
‫We can see here the value of C in Python.

33
00:03:17,990 --> 00:03:21,560
‫There are a lot of predefined or built-In exceptions.

34
00:03:22,130 --> 00:03:29,900
‫You can find them in the Python documentation, which is also attached to this lecture that they have

35
00:03:29,900 --> 00:03:35,190
‫an associated value indicating the detailed cause of the error.

36
00:03:36,020 --> 00:03:40,130
‫This may be a string or a couple of several items of information.

37
00:03:40,370 --> 00:03:45,830
‫In most cases, there is an error code in the spring explaining the code.

38
00:03:46,490 --> 00:03:55,310
‫Now let's modify our script to use predefined specific exceptions after the except caught on the same

39
00:03:55,310 --> 00:03:58,320
‫line, I write zero division error.

40
00:03:58,400 --> 00:04:06,260
‫This is a predefined exception as ee ee is a temporary variable of type zero division error.

41
00:04:06,260 --> 00:04:07,580
‫Exception object.

42
00:04:09,080 --> 00:04:12,050
‫And on the second line, I print a message.

43
00:04:16,520 --> 00:04:22,640
‫The object can be printed and contains information about the exception.

44
00:04:24,770 --> 00:04:30,410
‫Then we can have another except close links, right, except.

45
00:04:31,390 --> 00:04:43,000
‫Type error is e this is a temporary variable, I can name it whatever I want, print A and B must be

46
00:04:43,000 --> 00:04:43,620
‫numbers.

47
00:04:44,630 --> 00:04:49,890
‫A type error is raised when I try to divide the variables of different types.

48
00:04:49,910 --> 00:04:52,760
‫For example, an integer by a string.

49
00:04:58,440 --> 00:05:07,410
‫And the last except close except exception is e print another unspecified error.

50
00:05:09,210 --> 00:05:12,030
‫This will catch any other exception.

51
00:05:19,350 --> 00:05:23,700
‫Now, let's take a look at this code to clarify some things.

52
00:05:25,670 --> 00:05:32,480
‫If a zero division error is raised, the below block of code will be executed.

53
00:05:33,080 --> 00:05:41,890
‫If a type error is raised, only this block of code will be executed if another exception is raised.

54
00:05:42,200 --> 00:05:49,240
‫So neither zero division error nor a type error, then the building block of code is executed.

55
00:05:49,250 --> 00:05:54,770
‫If no exception is raised, the else block of code will be executed.

56
00:05:57,380 --> 00:06:05,720
‫The last except the statement that uses the exception class will catch absolutely all programming exceptions

57
00:06:05,750 --> 00:06:06,660
‫which may occur.

58
00:06:07,010 --> 00:06:13,580
‫This is not always a good practice because it will hide the actual root cause of the exception.

59
00:06:14,030 --> 00:06:21,260
‫The best practice is to create multiple except clauses according to your needs, specifying the name

60
00:06:21,260 --> 00:06:23,150
‫of the exception each time.

61
00:06:24,390 --> 00:06:25,940
‫Now, let's run the script.

62
00:06:27,960 --> 00:06:31,530
‫I'll enter zero to generate a zero division error.

63
00:06:33,690 --> 00:06:42,960
‫And we can see how it executed the line of code below the except statement that caught the zero division

64
00:06:42,960 --> 00:06:43,410
‫error.

65
00:06:44,600 --> 00:06:52,160
‫Now, let's try to generate a type air for that, I'll convert be to a string, so in fact I'll try

66
00:06:52,160 --> 00:06:54,500
‫to divide an integer by a string.

67
00:06:54,680 --> 00:06:55,550
‫So be equal.

68
00:06:55,550 --> 00:06:57,160
‫This is the R of B.

69
00:06:58,240 --> 00:07:01,270
‫The division will generate a type error.

70
00:07:04,680 --> 00:07:13,650
‫I enter six and it will convert to this integer to a string, and it executed the line of code below

71
00:07:13,650 --> 00:07:16,230
‫the except type error statement.

72
00:07:16,920 --> 00:07:21,270
‫This is the only except block of code that has been executed.

73
00:07:22,800 --> 00:07:31,140
‫Now, let's try a generic exception for that, I'll delete this variable so there'll be.

74
00:07:32,520 --> 00:07:35,640
‫Now, there is no such a variable called B.

75
00:07:37,150 --> 00:07:40,720
‫This isn't a division error nor a type error.

76
00:07:45,400 --> 00:07:46,740
‫I enter three.

77
00:07:47,790 --> 00:07:52,680
‫And you can see here the message of this except Klaus.

78
00:07:53,980 --> 00:08:02,920
‫I'll comment about the line that deletes the variable, of course, if I enter a correct value five.

79
00:08:04,170 --> 00:08:07,410
‫It will execute the else block of code.

80
00:08:08,450 --> 00:08:15,020
‫Finally, you should know that there is another clause that can be used with Trie except Ellis and the

81
00:08:15,020 --> 00:08:22,610
‫VIX, the finally close, it is executed in any event, no matter if one exception occurred or not.

82
00:08:23,090 --> 00:08:31,550
‫In real world applications, the final clause is useful for releasing external resources such as Phylis

83
00:08:31,550 --> 00:08:37,830
‫or network connections, regardless of whether the use of the resource was successful.

84
00:08:38,150 --> 00:08:41,420
‫Let's see an example using the final clause.

85
00:08:43,560 --> 00:08:45,920
‫I'm going to create a new Python script.

86
00:08:48,560 --> 00:08:59,510
‫And here I write F equals open a 30, 60 hour, here I am opening a text file in read-only mode.

87
00:09:00,290 --> 00:09:09,980
‫The open function returns a file object named F, then using the tri clause, I'll attempt to write

88
00:09:09,980 --> 00:09:10,790
‫to this file.

89
00:09:11,180 --> 00:09:16,940
‫Of course, it will erase an exception because the file has been opened in read-only mode.

90
00:09:17,150 --> 00:09:18,680
‫So f dot right.

91
00:09:19,100 --> 00:09:21,080
‫And I'll pass in a string.

92
00:09:26,130 --> 00:09:31,860
‫If there is an exception and this is the case here, I'll take that exception.

93
00:09:32,700 --> 00:09:36,360
‫So except exception is a.

94
00:09:38,490 --> 00:09:42,360
‫Print, there was an exception, Kolon.

95
00:09:43,650 --> 00:09:44,190
‫KomaI.

96
00:09:46,100 --> 00:09:53,690
‫Now, I am going to use the final clause, so finally, Colin, and the finally block of code.

97
00:09:55,020 --> 00:09:58,100
‫This code will be always executed.

98
00:10:03,520 --> 00:10:11,920
‫Now, I look close the file, no matter if the right operation was successful or not, first I'll point

99
00:10:11,920 --> 00:10:16,880
‫out if the file is closed or opened, of course, the file is opened.

100
00:10:16,900 --> 00:10:18,570
‫Nobody close to the file.

101
00:10:18,910 --> 00:10:21,880
‫So I right here, file closed.

102
00:10:22,030 --> 00:10:27,070
‫Callon and I am using the closed attribute of any file object.

103
00:10:27,370 --> 00:10:30,010
‫So if DOT closed, it will return.

104
00:10:30,010 --> 00:10:33,640
‫True if the file is closed or false otherwise.

105
00:10:35,960 --> 00:10:44,150
‫After that, I'll close the file and outside of the tri except and finally closes, I'll print the new

106
00:10:44,150 --> 00:10:45,380
‫status of the file.

107
00:10:45,440 --> 00:10:52,250
‫So if the file is open or closed, so print file, closed column.

108
00:10:52,610 --> 00:10:57,020
‫If DOT closed, I don't want another error.

109
00:10:57,110 --> 00:11:00,890
‫So I'll create an empty file called a DOT 60.

110
00:11:01,860 --> 00:11:03,270
‫This is a text file.

111
00:11:04,750 --> 00:11:12,010
‫The file exists in the same directory with my Python script, because I've used a relative path and

112
00:11:12,010 --> 00:11:14,130
‫now I'll execute the script.

113
00:11:15,150 --> 00:11:23,130
‫We can see how this line of code raised an exception and the except block of code has been executed,

114
00:11:23,760 --> 00:11:29,030
‫it pointed out there was an exception and the exception is not writable.

115
00:11:29,040 --> 00:11:31,360
‫So the file was not writable.

116
00:11:31,710 --> 00:11:36,570
‫After that, we see how the final block of code has been executed.

117
00:11:36,760 --> 00:11:38,760
‫The file wasn't closed.

118
00:11:39,630 --> 00:11:43,950
‫The closed attribute of the file object was false.

119
00:11:44,760 --> 00:11:47,340
‫In fact, this was the line number nine.

120
00:11:48,230 --> 00:11:56,750
‫I've closed the file, and after that I printed out the new status of the file, so file closed colon

121
00:11:56,900 --> 00:11:59,000
‫to the file is closed.

122
00:11:59,450 --> 00:12:06,730
‫This is a very useful example that uses the final clause of it's all about exceptions.

123
00:12:06,920 --> 00:12:07,490
‫Thank you.


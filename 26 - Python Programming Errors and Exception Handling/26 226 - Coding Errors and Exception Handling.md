
    
    
    #################################
    ## Exceptions Handling
    ## Exceptions are run-time Errors
    #################################
    
    a = 2
    
    #for ZeroDivisionError
    # b = 0
    
    #for TypeError
    b = '0'
    
    #for another Exception
    #del b
    
    try:
        ## try to execute this block of code!
        c = a / b
    except ZeroDivisionError as e:
        ## This block of code is executed when a ZeroDivisionError occurs
        print(f'There was Division by zero: {e}')
    except TypeError as e:
        ## This block of code is executed when a TypeError occurs
        print(f'There was a Type Error: {e}')
    except Exception as e:
        ## This block of code is executed when other exception occurs
        print(f'Other exception accured: {e}')
    else:
        ## This block of code is executed when no exception occurs
        print(f'No exception raised. c is {c}')
    finally:
        print('This block of code is always executed!')
    
    
    print('Continue script execution..')
    x = 2


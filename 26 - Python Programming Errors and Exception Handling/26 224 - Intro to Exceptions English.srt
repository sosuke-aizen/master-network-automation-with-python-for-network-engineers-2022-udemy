﻿1
00:00:00,760 --> 00:00:02,810
‫Hello, everyone, and welcome back.

2
00:00:03,190 --> 00:00:11,020
‫In this section, we'll discuss about errors and exceptions handling in Python, there are two types

3
00:00:11,020 --> 00:00:15,730
‫of errors syntax or parsing errors and exceptions.

4
00:00:16,150 --> 00:00:23,950
‫Syntax errors are identified at compile time before running the script, and they are the most common

5
00:00:23,950 --> 00:00:26,500
‫and easiest to find and debug.

6
00:00:27,560 --> 00:00:36,260
‫A syntax error occurs when the developer doesn't follow Python syntax, for example, he or she forgets

7
00:00:36,260 --> 00:00:39,980
‫to add a colon or uses the wrong indentation.

8
00:00:40,970 --> 00:00:43,220
‫Let's try this in Python Idol.

9
00:00:44,840 --> 00:00:55,210
‫For X in range of three and I'll press on enter, I've got a syntax error in this example.

10
00:00:55,430 --> 00:00:59,500
‫I skipped on purpose the colon after the range function.

11
00:00:59,840 --> 00:01:05,780
‫This isn't allowed by Python syntax rules and it triggered a syntax error.

12
00:01:07,290 --> 00:01:08,570
‫Let's try another one.

13
00:01:10,570 --> 00:01:11,170
‫Colin.

14
00:01:13,290 --> 00:01:14,310
‫Print X.

15
00:01:16,530 --> 00:01:20,410
‫Syntax that are expected and indented block.

16
00:01:21,260 --> 00:01:26,110
‫Now I've used the wrong indentation and I've got another syntax there.

17
00:01:27,400 --> 00:01:36,360
‫If we use an ID like bicarb, it automatically underlines syntax errors in rape and describes the error.

18
00:01:37,780 --> 00:01:47,260
‫This is the first example, and we can see here how it underlines in red, and it says Colin expected.

19
00:01:49,480 --> 00:01:58,030
‫If I write the column, but I use the wrong indentation, it also says in the end expected, this means

20
00:01:58,030 --> 00:02:03,760
‫that an ID like bicarb helps us a lot to avoid syntax errors.

21
00:02:05,510 --> 00:02:12,830
‫Even if an application compiles without errors during its execution, it could generate some errors.

22
00:02:13,250 --> 00:02:22,640
‫These are known as exceptions and are the hardest to find and debug exceptions are the runtime errors.

23
00:02:24,060 --> 00:02:32,040
‫An example of an exception is when a user tries to write to a file that was only opened in read-only

24
00:02:32,040 --> 00:02:32,430
‫mode.

25
00:02:32,910 --> 00:02:36,580
‫Another example is when we try a division by zero.

26
00:02:37,140 --> 00:02:42,970
‫These are not syntax or compile errors, but exceptions or a runtime errors.

27
00:02:43,770 --> 00:02:45,180
‫Let's see some examples.

28
00:02:46,290 --> 00:02:50,190
‫Then multiplied by five, divided by zero.

29
00:02:52,510 --> 00:02:57,850
‫The interpreter raised an exception, zero division error.

30
00:03:00,430 --> 00:03:04,180
‫Two plus six between single coach.

31
00:03:06,310 --> 00:03:12,350
‫It raised a piper unsupported operand types here.

32
00:03:12,370 --> 00:03:15,340
‫I've tried to add an integer to a string.

33
00:03:17,450 --> 00:03:21,500
‫These are all exceptions or runtime errors.

34
00:03:22,820 --> 00:03:31,250
‫Currently, if there is any type of exception in our code, even if it's not a fatal error, the entire

35
00:03:31,250 --> 00:03:32,180
‫script will stop.

36
00:03:34,050 --> 00:03:34,740
‫An example.

37
00:03:36,210 --> 00:03:40,050
‫Here in picture, I write a equals nine.

38
00:03:41,080 --> 00:03:52,990
‫Because in tough input of enter be here, I am asking for the user input and I am converting the input

39
00:03:53,260 --> 00:03:56,710
‫to an integer so B will start an integer.

40
00:03:58,270 --> 00:04:01,180
‫And our C equals A divided by B.

41
00:04:06,000 --> 00:04:13,590
‫This is my code that could raise an exception, but the script contains also other instructions.

42
00:04:26,750 --> 00:04:28,730
‫Now, I'll run the script.

43
00:04:30,650 --> 00:04:31,940
‫I enter five.

44
00:04:32,600 --> 00:04:38,540
‫There is no error and no exception, and the entire script has been executed.

45
00:04:40,280 --> 00:04:48,280
‫Now, let's see what happens if one exception is raised, far of it, I will enter zero for.

46
00:04:50,970 --> 00:05:00,480
‫And I've got a zero division error, this exception stopped the script, even if this is not a fatal

47
00:05:00,480 --> 00:05:06,090
‫error, all other instructions of my script haven't been executed.

48
00:05:07,210 --> 00:05:15,160
‫Sometimes, like in this case, if the error is not fatal, we want to handle the error in such a way

49
00:05:15,310 --> 00:05:19,750
‫that the script continues its execution with other code.

50
00:05:19,930 --> 00:05:28,750
‫In this case, it was better if it printed a message, for example, zero division error, but it continued

51
00:05:28,750 --> 00:05:31,390
‫executing these instructions.

52
00:05:32,390 --> 00:05:40,850
‫We don't want the exception to stop the entire script, to do this will use the so-called Trie except

53
00:05:40,850 --> 00:05:42,620
‫block for exception handling.

54
00:05:43,720 --> 00:05:48,790
‫Now that we've seen what is an egg and what is an exception, we'll move on.

55
00:05:48,950 --> 00:05:54,700
‫And in the next lecture, I'll show you how to efficiently catch and handle exceptions.

56
00:05:56,030 --> 00:05:57,670
‫See you in two seconds.


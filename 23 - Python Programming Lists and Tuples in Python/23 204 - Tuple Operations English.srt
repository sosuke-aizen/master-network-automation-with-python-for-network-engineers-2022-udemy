﻿1
00:00:01,140 --> 00:00:08,720
‫In this lecture, we'll continue our discussion about topolice, let's see what operations are available,

2
00:00:09,510 --> 00:00:15,270
‫we can iterate over a tuple using a for loop, exactly like a police box.

3
00:00:16,710 --> 00:00:21,690
‫We can concatenate two or more topolice by creating a new one.

4
00:00:21,930 --> 00:00:25,290
‫Of course, we cannot modify the initial topolice.

5
00:00:26,470 --> 00:00:31,090
‫We can slice a table and we can repeat a table.

6
00:00:31,900 --> 00:00:36,070
‫Let's see examples with all of these operations.

7
00:00:38,400 --> 00:00:47,370
‫Here I have a table that contains the days of the week, let's see how can we iterate over this table?

8
00:00:47,610 --> 00:00:56,790
‫So for day in day, the name of my poppel colon and now the four block of code, I'll just print each

9
00:00:56,790 --> 00:00:58,470
‫element of the tuple.

10
00:00:58,770 --> 00:01:02,370
‫So print they and I don't want a backslash.

11
00:01:02,370 --> 00:01:07,380
‫And after each element, I just want a single whitespace.

12
00:01:10,140 --> 00:01:17,960
‫Now, let's see the concatenation operation, I have a table, the one one come up to another table,

13
00:01:17,970 --> 00:01:28,470
‫the two that contains two strings, A, B and C, D, and I'm going to create another table called the

14
00:01:28,470 --> 00:01:32,370
‫three equals the one plus the two.

15
00:01:33,700 --> 00:01:40,930
‫And this is the three, so I've concatenated the two topolice and I've created a new one.

16
00:01:42,250 --> 00:01:51,310
‫If I want to repeat a couple, I can write to a three star and here, let's say three, and I've repeated

17
00:01:51,310 --> 00:01:53,020
‫the top all three times.

18
00:01:55,090 --> 00:02:05,230
‫If I want to check, if an element belongs to the table, I can use the in or not in operators, for

19
00:02:05,230 --> 00:02:08,290
‫example, Friday in this.

20
00:02:10,580 --> 00:02:19,730
‫And it will return true because Friday is an element of the papal boty firefight Friday in days it will

21
00:02:19,730 --> 00:02:20,990
‫return false.

22
00:02:23,040 --> 00:02:28,890
‫In the not in operator Fahri, not in days will return to.

23
00:02:30,040 --> 00:02:38,740
‫These two operators are used in if testing conditions like it's all about Topal operations in the next

24
00:02:38,740 --> 00:02:39,400
‫lecture.

25
00:02:39,580 --> 00:02:43,150
‫We'll see what the methods are available to topolice.


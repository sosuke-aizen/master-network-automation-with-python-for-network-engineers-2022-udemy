﻿1
00:00:00,610 --> 00:00:06,400
‫In this lecture, we'll dive deeper into Python lists and we'll talk about links operations.

2
00:00:07,940 --> 00:00:15,500
‫The first important operation is iteration Lexy with an example, How can we iterate over a least?

3
00:00:16,130 --> 00:00:23,990
‫I have a list that has three elements, three IP addresses, and I want to iterate over this list to

4
00:00:23,990 --> 00:00:27,620
‫execute a block of code for each IP in the list.

5
00:00:27,650 --> 00:00:35,510
‫For example, I have an automation script and I want to connect to each IP and install some applications

6
00:00:35,510 --> 00:00:37,760
‫or execute a backup.

7
00:00:38,900 --> 00:00:46,220
‫For that, I'm going to use a for loop, so for item, this is a temporary variable.

8
00:00:47,310 --> 00:00:48,540
‫In my least.

9
00:00:50,250 --> 00:01:00,660
‫Colon, and it starts the four block of code in this example, I'll just print the IP address, so print

10
00:01:01,110 --> 00:01:08,550
‫I am using a F string literal so f in between single codes connecting to.

11
00:01:09,560 --> 00:01:14,720
‫In between a pair of curly braces, Eitam, my temporary variable.

12
00:01:16,910 --> 00:01:18,900
‫Sorry, here I have my earpiece.

13
00:01:18,950 --> 00:01:20,600
‫This is the name of my list.

14
00:01:23,270 --> 00:01:29,210
‫And it executed the print function for each item of the list.

15
00:01:29,930 --> 00:01:32,380
‫This is how we iterate over a list.

16
00:01:34,480 --> 00:01:42,850
‫Now, let's see how can we concatenate to least be aware of concatenation because problems can appear.

17
00:01:43,990 --> 00:01:51,460
‫I create a new list, let's at least one equals, and it contains two elements to integers one, comma,

18
00:01:51,460 --> 00:01:51,700
‫two.

19
00:01:52,730 --> 00:02:01,370
‫Let's print it, least one equals least one plus in here, I'll add another list.

20
00:02:02,180 --> 00:02:06,050
‫I want to concatenate the second list to list one.

21
00:02:06,380 --> 00:02:07,940
‫So three, KOMAI four.

22
00:02:09,800 --> 00:02:18,950
‫This is list one, it contains the old list one plus three and for the second list, but when I check

23
00:02:19,020 --> 00:02:23,210
‫Exide, I notice it has another address.

24
00:02:23,510 --> 00:02:27,370
‫So in fact, here I've created another list.

25
00:02:27,800 --> 00:02:30,740
‫I didn't modify the first list.

26
00:02:31,220 --> 00:02:31,790
‫List one.

27
00:02:32,630 --> 00:02:40,240
‫If I want to modify the first list, the initial list, I must use the plus equal operator.

28
00:02:41,150 --> 00:02:47,660
‫So one more time list one plus equals and another list five comma six.

29
00:02:48,930 --> 00:02:58,980
‫This is list one is expected, but it is the same we didn't modify the initial list, so there is a

30
00:02:58,980 --> 00:03:04,940
‫difference between using the plus operator and plus equal operator.

31
00:03:05,340 --> 00:03:06,480
‫So they care.

32
00:03:07,500 --> 00:03:14,810
‫If you want to add a single element to at least, you must enclose that element by square brackets.

33
00:03:15,540 --> 00:03:19,100
‫If I write here, like, say, 10, I'll get an error.

34
00:03:19,290 --> 00:03:22,550
‫I cannot concatenate a list with an integer.

35
00:03:22,830 --> 00:03:28,950
‫I must have 10 between square brackets, even if the list contains only one element.

36
00:03:32,690 --> 00:03:41,480
‫There is also a method called extend that extends at least, in fact, it's the same as the plus equal

37
00:03:41,480 --> 00:03:43,580
‫operator Lexcen example.

38
00:03:45,000 --> 00:03:54,270
‫This is my least one, least one plus equals, and I'll let another list that contains two strings at

39
00:03:54,270 --> 00:03:57,370
‫least can contain elements of different types.

40
00:03:57,750 --> 00:03:58,710
‫So ABC.

41
00:04:00,420 --> 00:04:01,200
‫X, Y, Z.

42
00:04:02,390 --> 00:04:03,650
‫Idea of least.

43
00:04:05,770 --> 00:04:08,290
‫We can see it has the same idea.

44
00:04:12,190 --> 00:04:20,290
‫The same result can be obtained using the extend method, so at least one that extend and I'll pass

45
00:04:20,290 --> 00:04:23,950
‫in at least a B comma city.

46
00:04:26,250 --> 00:04:35,370
‫This is list one, and each side is the same as a conclusion, you can use the plus equal operator or

47
00:04:35,400 --> 00:04:36,840
‫the extend method.

48
00:04:38,060 --> 00:04:47,870
‫Another useful method is opened up, and it's a single element to the end of the list, so Extend adds

49
00:04:47,900 --> 00:04:55,880
‫more elements and append adds a single element list, one dot up append of double X.

50
00:04:59,630 --> 00:05:08,250
‫It appended an element to the end of the list, if I try to add more elements using the append method.

51
00:05:08,360 --> 00:05:10,580
‫I won't get what I expect.

52
00:05:14,490 --> 00:05:22,830
‫What happened here, in fact, it added a single element and the last element of my list is another

53
00:05:22,830 --> 00:05:23,190
‫list.

54
00:05:24,120 --> 00:05:30,570
‫It didn't add two elements to the end of the list, like the content of list one.

55
00:05:31,950 --> 00:05:35,190
‫We can't see how it added a single element.

56
00:05:36,910 --> 00:05:40,360
‫This is the difference between a bend and extend.

57
00:05:40,960 --> 00:05:46,700
‫There are many other at least the methods, but we'll talk about them in another lecture.

58
00:05:47,420 --> 00:05:53,520
‫Now, I want to show you something very important about the assignment operator.

59
00:05:53,530 --> 00:05:55,570
‫So about the equal sign.

60
00:05:57,240 --> 00:06:04,770
‫I create a new list, at least one equals one comma to list two equals list one.

61
00:06:05,640 --> 00:06:06,630
‫This is list one.

62
00:06:07,210 --> 00:06:08,600
‫This is list two.

63
00:06:09,720 --> 00:06:14,250
‫They are equal and they are stored at the same address.

64
00:06:14,250 --> 00:06:20,070
‫So list one is list to return to an hourly standard append.

65
00:06:20,400 --> 00:06:22,830
‫I let an element three.

66
00:06:23,460 --> 00:06:24,450
‫This is list one.

67
00:06:26,020 --> 00:06:36,310
‫And this is linked to the leaks are still the same when I appended this element to list one, I've also

68
00:06:36,310 --> 00:06:40,560
‫modified the list too, because they point to the same address.

69
00:06:41,110 --> 00:06:46,130
‫They are references to the same address list to dot remove.

70
00:06:46,600 --> 00:06:48,730
‫Now I am removing an element.

71
00:06:50,080 --> 00:06:58,810
‫This is list two, I've removed the first element with Valy one, and this is list one, the element

72
00:06:58,810 --> 00:07:01,660
‫has been removed also from list one.

73
00:07:03,570 --> 00:07:09,900
‫At least one equals equals, at least to at least one is at least to.

74
00:07:11,050 --> 00:07:19,800
‫Idea of Alliston and the idea of Listo, so take care when using the assignment or equal operator,

75
00:07:20,320 --> 00:07:25,720
‫it doesn't create a copy of a list, but the reference to the same address.

76
00:07:27,230 --> 00:07:35,960
‫Now, let's talk about the list slicing, slicing RETAVASE apart of a list and can also modify list.

77
00:07:37,170 --> 00:07:40,430
‫At least one equals one, two, three, four.

78
00:07:42,580 --> 00:07:45,630
‫At least one of zero, Callon, to.

79
00:07:46,930 --> 00:07:55,540
‫It will return on your list with elements from Index zero and one the start is included into the end

80
00:07:55,540 --> 00:07:56,350
‫excluded.

81
00:07:58,170 --> 00:08:07,650
‫Let's see another example at one of column three, by default, the stock index is zero.

82
00:08:07,890 --> 00:08:16,970
‫So in fact, it will return a list that contains the elements from index zero to index three excluded.

83
00:08:17,760 --> 00:08:21,570
‫So zero one and two, it will return one to three.

84
00:08:23,660 --> 00:08:31,490
‫I can also use a step from index one to index four in steps of two.

85
00:08:32,700 --> 00:08:41,440
‫This is index one, this is index three, because four is not included and it returned in steps of two.

86
00:08:42,150 --> 00:08:50,130
‫I can also use negative indexes, list one of minus three, Callon, minus one.

87
00:08:51,130 --> 00:08:58,690
‫It will return a new list from index minus three to index minus one, this is minus one, minus two,

88
00:08:58,690 --> 00:08:59,700
‫minus three.

89
00:08:59,740 --> 00:09:06,880
‫So, in fact, it will return at least with two elements, two and three, because minus one is not

90
00:09:06,880 --> 00:09:07,540
‫included.

91
00:09:08,910 --> 00:09:19,320
‫Slicing also modifies a list, so at least one of zero colon two equals seven Comite.

92
00:09:23,150 --> 00:09:31,990
‫I've modified least one, I've changed the elements from index to zero included to index to excluded.

93
00:09:32,000 --> 00:09:35,450
‫So the first two elements with these elements.

94
00:09:36,410 --> 00:09:44,270
‫I can also use something like this, least one of zero colon two equals, and here I have.

95
00:09:46,490 --> 00:09:49,850
‫A comma, B, Kamasi Komati, comma.

96
00:09:51,530 --> 00:09:52,910
‫This isn't an error.

97
00:09:54,750 --> 00:10:02,820
‫I've extended the list with these elements, even if the slicing operation returns, only two elements,

98
00:10:03,030 --> 00:10:05,610
‫I could insert five elements.

99
00:10:05,820 --> 00:10:14,190
‫So in fact, it has inserted these elements from index zero to index one or two excluded.

100
00:10:16,110 --> 00:10:26,220
‫At least one of Colin, Colin with the entire list and the least one of Colin Colin minus one, reverses

101
00:10:26,430 --> 00:10:27,070
‫my list.

102
00:10:27,540 --> 00:10:31,680
‫In fact, I'm going backwards in steps of one.

103
00:10:33,070 --> 00:10:40,810
‫That's all for the moment about leaks, operations, and in the next lecture, I'll show you, Lisa,

104
00:10:40,830 --> 00:10:41,540
‫semantics.

105
00:10:41,830 --> 00:10:44,050
‫See you in just a few seconds.


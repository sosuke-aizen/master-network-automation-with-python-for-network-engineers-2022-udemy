﻿1
00:00:00,420 --> 00:00:07,440
‫In this lecture, we'll start talking about apples to apples are another type of order, the sequence

2
00:00:07,440 --> 00:00:14,690
‫of items that are very similar to lists and like lists, support, indexing and slicing.

3
00:00:15,000 --> 00:00:23,700
‫We can refer to any item in the sequence by using its index number, which starts also from zero, not

4
00:00:23,700 --> 00:00:25,680
‫one unlikely.

5
00:00:26,070 --> 00:00:33,960
‫A table is an immutable sequence, meaning their content cannot be changed by adding, removing or replacing

6
00:00:33,960 --> 00:00:34,650
‫elements.

7
00:00:35,250 --> 00:00:38,520
‫If you need to modify a table, you create a new one.

8
00:00:38,970 --> 00:00:42,690
‫This is the key difference between topolice and leeks.

9
00:00:44,260 --> 00:00:53,260
‫To create a table, we use parentheses and commas to separate elements, but better Lexy exemplars.

10
00:00:55,440 --> 00:01:02,790
‫An empty Topal can be created using the Topal constructor or using a pair of parentheses.

11
00:01:11,570 --> 00:01:22,520
‫These are two empty topolice, let's create another Papale Topal three equals one comma to comma three.

12
00:01:24,110 --> 00:01:32,570
‫This is another couple of it contains three elements, three integers, just like a list, a couple

13
00:01:32,570 --> 00:01:35,480
‫can have elements of any type, Lexy.

14
00:01:35,570 --> 00:01:38,120
‫Another example, my Topal.

15
00:01:40,650 --> 00:01:46,770
‫Equals and between parentheses, I have one integer afloat, a string.

16
00:01:48,940 --> 00:01:50,080
‫Another pople.

17
00:01:51,560 --> 00:01:52,340
‫And the least.

18
00:01:59,560 --> 00:02:07,670
‫This table has elements of different types, integers, flags, strings, topolice or leeks.

19
00:02:11,800 --> 00:02:18,880
‫If you want to create a table using a single element, you have to use a trick, although you have only

20
00:02:18,880 --> 00:02:23,260
‫one element inside the table, you have to write the comma after it.

21
00:02:23,270 --> 00:02:25,680
‫Otherwise it will not be a paper.

22
00:02:27,320 --> 00:02:34,610
‫For example, table for equals, and they want a single element, let's say four point three.

23
00:02:35,710 --> 00:02:36,850
‫This isn't a.

24
00:02:37,420 --> 00:02:39,160
‫In fact, this is a float.

25
00:02:42,550 --> 00:02:50,400
‫If I want to create a table, I use a comma after the element, even if I have a single element.

26
00:02:51,220 --> 00:02:52,510
‫This is a tuple.

27
00:02:53,470 --> 00:02:56,140
‫A table with a single element.

28
00:02:58,690 --> 00:03:05,440
‫Indexing rules we've seen at the end strings are still applicable to topolice.

29
00:03:07,380 --> 00:03:08,490
‫This is my Topal.

30
00:03:10,390 --> 00:03:21,280
‫And this is the element at index zero, this element is an index one, index to index three and index

31
00:03:21,280 --> 00:03:21,610
‫for.

32
00:03:31,480 --> 00:03:40,450
‫If I want to get an element from the least inside the table, for example, of the ABC string, I can

33
00:03:40,450 --> 00:03:51,490
‫write my Topal off for a the least, and after that, a tool between square brackets, because this

34
00:03:51,490 --> 00:04:02,110
‫element is an index to inside the list five is at index zero, six has index one.

35
00:04:02,360 --> 00:04:04,750
‫And here we have index to.

36
00:04:09,870 --> 00:04:15,870
‫As I said earlier, a tuple is immutable, we can't modify, topple.

37
00:04:17,450 --> 00:04:20,870
‫If we try to modify or topple, we get an error.

38
00:04:26,940 --> 00:04:33,670
‫And I've got a pipe air popper object does not support item assignment.

39
00:04:34,590 --> 00:04:38,580
‫This is the key difference between topolice and the leaks.

40
00:04:40,720 --> 00:04:49,450
‫These are people's basics, and in the next lecture, we'll see many other examples with topolice.

41
00:04:49,810 --> 00:04:52,000
‫See you in just a few seconds.


﻿1
00:00:00,720 --> 00:00:08,310
‫In the last lecture we've discussed about the leaks, operations like iteration, concatenation, slicing

2
00:00:08,310 --> 00:00:09,420
‫and upending.

3
00:00:09,900 --> 00:00:12,670
‫Now it's time to take a look at the leaks methods.

4
00:00:13,230 --> 00:00:17,730
‫These are functions, urtext on objects of type list.

5
00:00:18,360 --> 00:00:26,850
‫If we want to see all the methods, we write the name of a variable that stores a list and adapt the

6
00:00:26,870 --> 00:00:33,300
‫auto completion function of eitel or bicarb will display all available methods.

7
00:00:42,180 --> 00:00:48,750
‫We can also use the built in function to display all methods awfullest.

8
00:00:56,650 --> 00:01:05,710
‫The methods that start with double underscores our special methods, also called magic methods, and

9
00:01:05,710 --> 00:01:08,760
‫we won't talk about them at this moment.

10
00:01:09,970 --> 00:01:17,050
‫If we want to see a short help of a method, we use the built in help function in between parentheses,

11
00:01:17,050 --> 00:01:24,940
‫the type of the object, which is at least a dot and the name of the method, for example, help of

12
00:01:25,180 --> 00:01:26,740
‫list, dot append.

13
00:01:30,700 --> 00:01:39,880
‫The first method will take a look at our up and extend and insert, I've already shown you some examples

14
00:01:39,910 --> 00:01:46,030
‫with a bend and extend in the previous lecture, but now I want a short review.

15
00:01:46,460 --> 00:01:54,940
‫These methods are used to add elements to the end of the list up and add a single element to the end

16
00:01:54,940 --> 00:02:04,120
‫of the list, extend its all elements of iterable, object to the end of the list and insert inserts

17
00:02:04,120 --> 00:02:08,140
‫an element at the specific index or position.

18
00:02:09,040 --> 00:02:19,810
‫Let's see examples, I create a list called Numbers from numbers, and the items are one, two, three,

19
00:02:19,810 --> 00:02:21,010
‫four and five.

20
00:02:23,710 --> 00:02:33,640
‫Now, I want to add an element to the end of the list numbers, total spend and the value of that element,

21
00:02:34,240 --> 00:02:40,450
‫it can be any type of object like, say, a, I add a string.

22
00:02:41,760 --> 00:02:50,160
‫If I want to add more elements using append, I'll get an error append adds a single element.

23
00:02:55,760 --> 00:03:05,360
‫There is the extend the method that ends or extends the least with the elements of any iterable object,

24
00:03:05,630 --> 00:03:14,930
‫for example, nums, dot, extend and I'll pass in as an argument uneatable object that can be at least

25
00:03:14,930 --> 00:03:19,580
‫a couple of string, for example, between square brackets.

26
00:03:19,610 --> 00:03:22,640
‫This is at least six, comma seven.

27
00:03:23,750 --> 00:03:27,710
‫And it extended the list with two elements.

28
00:03:29,270 --> 00:03:37,130
‫I can also pass in a string string is an iterable object, so between single codes, hello.

29
00:03:39,510 --> 00:03:47,520
‫And it extended the least with the elements of the string, so the characters of the string.

30
00:03:48,800 --> 00:03:56,560
‫You'll get them there if you piss in a single element, not an iterable object, for example, NOM's

31
00:03:56,870 --> 00:03:59,990
‫dot extend of seven.

32
00:04:01,320 --> 00:04:08,450
‫This is an error, that's because the intakes are seven is not an iterable object.

33
00:04:08,660 --> 00:04:18,580
‫If I want to add a single element, I use the append method or here I put seven between square brackets.

34
00:04:18,740 --> 00:04:26,170
‫This is a list with one element and in fact, it will add that element seven to the end of the list.

35
00:04:28,620 --> 00:04:37,950
‫Now, let's see how the insert method works, it inserts an element at a specific index or position

36
00:04:38,640 --> 00:04:47,480
‫numbers dot insert, the first argument is the index is zero and the value like, say, one hundred,

37
00:04:47,880 --> 00:04:51,300
‫in fact, it will add the value.

38
00:04:51,330 --> 00:04:54,360
‫One hundred at position zero.

39
00:04:56,170 --> 00:05:02,140
‫Another example, numbs dot insert three, comma 200.

40
00:05:05,140 --> 00:05:18,100
‫It inserted the value 200 at position three zero, one, two and three, so it inserted value 200 here

41
00:05:18,310 --> 00:05:22,390
‫after value to or before value three.

42
00:05:23,560 --> 00:05:31,690
‫If you want to add an item to the end of the list and try numbers, dot insert minus one, minus one

43
00:05:31,690 --> 00:05:37,780
‫being the index of the last element and all the value, let's say four hundred.

44
00:05:39,160 --> 00:05:46,930
‫It didn't added to the very end, but to the second to last position of X, because it doesn't count

45
00:05:46,930 --> 00:05:49,000
‫that the list will grow by one.

46
00:05:50,250 --> 00:05:55,620
‫We can see how the value of four hundred is on the second to last position.

47
00:05:57,240 --> 00:06:04,160
‫If you want to add an element to the very end of the list, you must use the Lenn function to calculate

48
00:06:04,170 --> 00:06:07,440
‫the index of the last element in the list.

49
00:06:08,970 --> 00:06:14,190
‫Land of NOM's, RETAVASE, the number of elements in the list.

50
00:06:15,250 --> 00:06:27,970
‫No doubt instead of Lenn of NOM's, this will return the index, the value seventeen comma, six hundred,

51
00:06:28,870 --> 00:06:33,980
‫these will add the value six hundred to the last position in the last.

52
00:06:39,870 --> 00:06:47,830
‫Another important method is copy, if you need a copy of a list, you should use the copy method and

53
00:06:47,880 --> 00:06:51,480
‫not the assignment operator or the equal sign.

54
00:06:52,530 --> 00:06:57,140
‫Lexy unexampled numbers equals numbers.

55
00:06:57,840 --> 00:07:01,000
‫Numbers is not a copy of the numbers list.

56
00:07:01,020 --> 00:07:03,510
‫In fact, they are the same list.

57
00:07:03,510 --> 00:07:10,160
‫They are references to the same object, for example, numbers that append minus one.

58
00:07:10,890 --> 00:07:15,900
‫We can see how minus one also belongs to the numbers list.

59
00:07:16,560 --> 00:07:25,230
‫In fact, here, using the append method, I've modified the value stored at the address referenced

60
00:07:25,230 --> 00:07:28,410
‫by both numbers and the numbers lists.

61
00:07:29,630 --> 00:07:38,770
‫If I need a copy of the numbers list, I use the copy method, let's say numbers underline copy equals

62
00:07:39,290 --> 00:07:40,830
‫nums dot copy.

63
00:07:41,240 --> 00:07:46,370
‫Now numbers underline copy and numbers are different leaks.

64
00:07:54,830 --> 00:07:58,130
‫We can see how they are saved at different addresses.

65
00:07:59,520 --> 00:08:01,590
‫And the mom's daughter spend.

66
00:08:03,050 --> 00:08:03,950
‫Minus two.

67
00:08:06,850 --> 00:08:08,440
‫Sorry, here I have a bent.

68
00:08:11,240 --> 00:08:19,280
‫Minus two has been appended to the NUMS list, but not to the numbers underline copy list.

69
00:08:21,660 --> 00:08:28,120
‫In this lecture, I've shown you a couple ways of adding toilet in the next lecture.

70
00:08:28,380 --> 00:08:31,870
‫We'll see how we can remove items from a list.

71
00:08:32,040 --> 00:08:34,190
‫See you in just a few seconds.


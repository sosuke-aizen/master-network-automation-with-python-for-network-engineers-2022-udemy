﻿1
00:00:00,450 --> 00:00:07,760
‫Let's see now, how can we start at least there are two possibilities, the least sought method or the

2
00:00:07,770 --> 00:00:17,910
‫sorted builtin function, the least sought method salk's the items of the list in place or it modifies

3
00:00:17,910 --> 00:00:18,570
‫the list.

4
00:00:19,510 --> 00:00:27,700
‫But the assorted built in function will return unpoliced with elements sorted in ascending order.

5
00:00:28,710 --> 00:00:37,740
‫So if I write sorted of numbers, this building function, this isn't a list method, this function

6
00:00:37,740 --> 00:00:42,990
‫will return another list with elements sorted in ascending order.

7
00:00:44,280 --> 00:00:51,180
‫This is the new list, the initial numbers list hasn't been modified, it's the same.

8
00:00:51,930 --> 00:00:54,990
‫Of course, we can take the new list in a variable.

9
00:01:02,220 --> 00:01:09,210
‫If we use the Dussart method, the list will be modified, so numbers that sort.

10
00:01:10,300 --> 00:01:15,660
‫And the list has been modified, now the elements are sorted.

11
00:01:16,910 --> 00:01:26,720
‫We can also sought strings in alphabetical order, Lexy, an example, Letairis equals list stuff and

12
00:01:26,720 --> 00:01:27,800
‫some letters here.

13
00:01:29,920 --> 00:01:38,740
‫This is my letters list, and now I want to sort the list illustrated in place or I'll modify it, so

14
00:01:38,740 --> 00:01:40,840
‫let's not sort.

15
00:01:44,190 --> 00:01:51,360
‫The list has been modified, it contains elements sorted in ascending order.

16
00:01:52,260 --> 00:01:57,600
‫If we want to start the list in descending order, we use the same method.

17
00:01:57,840 --> 00:02:03,590
‫But we've seen an optional argument and the reverse equals two.

18
00:02:04,170 --> 00:02:12,090
‫So let's not sort and this is the optional argument or worse equals to.

19
00:02:13,550 --> 00:02:17,780
‫Through written with uppercase Deakes, the Boolean Constant.

20
00:02:20,520 --> 00:02:24,200
‫And the list has been sorted in descending order.

21
00:02:25,870 --> 00:02:30,910
‫There are also other builtin functions that can be used with leaks.

22
00:02:32,190 --> 00:02:40,200
‫If we want the maximum value of awfullest, least we use the max function, so max of numbers.

23
00:02:41,220 --> 00:02:47,400
‫And it returned one hundred one hundred is the maximum value of my list.

24
00:02:47,940 --> 00:02:50,400
‫There is also a mean of NOM's.

25
00:02:51,410 --> 00:02:53,510
‫Or men of letters.

26
00:02:55,430 --> 00:02:58,100
‫It used the alphabetical order.

27
00:03:01,340 --> 00:03:09,550
‫There is also the some method, of course, it will calculate the sum of the elements of the list.

28
00:03:12,600 --> 00:03:15,240
‫The sum is one hundred twelve.

29
00:03:16,940 --> 00:03:25,040
‫However, if we have a list with various types of items, let's say numbers and strings mixed together,

30
00:03:25,400 --> 00:03:30,940
‫how does it compare a string with an integer and return the maximum value of the list?

31
00:03:31,460 --> 00:03:38,870
‫In that case, it will return a type error, saying that it cannot compare integers with strings.

32
00:03:39,440 --> 00:03:45,890
‫Generally speaking, comparison operators don't support different data types.

33
00:03:46,900 --> 00:03:51,400
‫This is my letters list and this is my mom's list.

34
00:03:52,340 --> 00:03:57,440
‫I'll append a string to the enemies list, so names dot append.

35
00:03:59,630 --> 00:04:01,370
‫And the thing is, Python.

36
00:04:02,970 --> 00:04:06,270
‫And now, all right, sorted of numbers.

37
00:04:09,240 --> 00:04:13,860
‫And I've got the type error or Mok's of numbers.

38
00:04:15,630 --> 00:04:20,890
‫By air, mean of numbers, the same type error.

39
00:04:21,810 --> 00:04:25,200
‫We cannot compare strings to integers.

40
00:04:26,740 --> 00:04:32,180
‫These are the most used leaks methods in the next lecture.

41
00:04:32,350 --> 00:04:36,640
‫We'll see how to transform, at least to a string and a string to a list.

42
00:04:36,790 --> 00:04:42,760
‫After that, we'll take a look at another python data structure called Topal.


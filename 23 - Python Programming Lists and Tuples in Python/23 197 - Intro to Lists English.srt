﻿1
00:00:00,700 --> 00:00:02,500
‫Hello, guys, and welcome back.

2
00:00:02,760 --> 00:00:09,500
‫In this lecture, we'll start talking about the leaks, at least represents the most versatile and used

3
00:00:09,550 --> 00:00:15,850
‫type of data structure in Python at least, is an order sequence or collection of items.

4
00:00:16,060 --> 00:00:23,740
‫It can hold any object type integers, Fluke's strengths, other leaks, papers, dictionaries and so

5
00:00:23,740 --> 00:00:24,070
‫on.

6
00:00:24,340 --> 00:00:31,830
‫All can be stored in list leaks like string's support, indexing and slicing.

7
00:00:31,960 --> 00:00:37,510
‫We can refer to any item in the sequence by using Eakes index number.

8
00:00:37,840 --> 00:00:41,090
‫So we have a concept of the first element of the list.

9
00:00:41,110 --> 00:00:44,530
‫The second element, the third element and so on.

10
00:00:44,890 --> 00:00:49,930
‫This is also valid for other sequence types like string's or topolice.

11
00:00:50,990 --> 00:00:59,480
‫In Python, like also in other programming languages like C, C++ or Java, we start indexing from zero,

12
00:00:59,570 --> 00:01:00,330
‫not one.

13
00:01:00,740 --> 00:01:07,290
‫This means that the first element of the list, when counting from left to right, has the index zero.

14
00:01:07,610 --> 00:01:11,070
‫The second element has its corresponding index one.

15
00:01:11,090 --> 00:01:13,670
‫The third element index two.

16
00:01:13,700 --> 00:01:14,840
‫And so on.

17
00:01:15,320 --> 00:01:20,670
‫A list is a mutable object and that means it can be modified.

18
00:01:20,900 --> 00:01:27,740
‫We can add elements to the list or we can remove elements from the list without creating a new list.

19
00:01:28,680 --> 00:01:36,420
‫Is the reminder a string, for example, is immutable, if we want to modify a string, we must create

20
00:01:36,420 --> 00:01:37,230
‫another one.

21
00:01:38,330 --> 00:01:44,380
‫At least uses square brackets and commas between elements to separate them.

22
00:01:45,480 --> 00:01:53,130
‫Let's see some examples, I can create an empty list, let's say list one, simply by using a pair of

23
00:01:53,130 --> 00:01:54,060
‫square brackets.

24
00:01:54,720 --> 00:01:58,680
‫Another possibility is to use the list constructor.

25
00:02:00,570 --> 00:02:03,090
‫These are two empty lists.

26
00:02:10,170 --> 00:02:16,180
‫I'll create another list, my list that holds items of different types.

27
00:02:16,560 --> 00:02:18,640
‫So the first element is an integer.

28
00:02:18,810 --> 00:02:23,430
‫The second element of float, the third element, a string.

29
00:02:24,710 --> 00:02:30,860
‫And the fourth element that Apple will talk about topolice in another lecture.

30
00:02:35,740 --> 00:02:43,930
‫This list has four elements, if I want to get the number of elements in the list, I use the built

31
00:02:43,930 --> 00:02:54,430
‫in Lenn function Lenn of my list returns for, as I said earlier, we have the concept of the first

32
00:02:54,430 --> 00:02:57,190
‫element, the second element, and so on.

33
00:02:57,760 --> 00:03:00,200
‫Our list is an order to sequence.

34
00:03:00,760 --> 00:03:05,970
‫This means I can get the elements of the list using indexes.

35
00:03:06,490 --> 00:03:10,140
‫My list of zero will return one.

36
00:03:10,990 --> 00:03:13,270
‫This is the first element of the list.

37
00:03:13,870 --> 00:03:20,830
‫My list of one will return three point to the second element of the list.

38
00:03:21,130 --> 00:03:27,830
‫Of course, my list of three will return the last element of the list and vex the paper.

39
00:03:28,270 --> 00:03:34,010
‫We can also use negative indexes, so my list of minus one will return.

40
00:03:34,030 --> 00:03:36,070
‫The last element of the list.

41
00:03:36,730 --> 00:03:41,050
‫My list of minus two will return Python.

42
00:03:42,370 --> 00:03:51,100
‫Earley's is a mutable object we can modify at least first, let's create a string is drawn equals and

43
00:03:51,100 --> 00:03:52,210
‫here Python.

44
00:03:56,830 --> 00:04:05,070
‫This is a string, if I want to modify the string, I'll get an error, so it's Darwan of zero.

45
00:04:05,110 --> 00:04:10,270
‫I want to modify the element at position zero equals.

46
00:04:10,630 --> 00:04:14,110
‫And instead of B, I want here A.

47
00:04:15,600 --> 00:04:18,140
‫And this is an error type error.

48
00:04:19,480 --> 00:04:22,720
‫A string does not support item assignment.

49
00:04:23,810 --> 00:04:29,630
‫If I want to modify the string, I can do it, but I must create another string for that.

50
00:04:30,720 --> 00:04:32,740
‫Let's see how it works for leaks.

51
00:04:33,540 --> 00:04:37,170
‫You should also know that you can create a list from a string.

52
00:04:38,270 --> 00:04:41,920
‫So at least one equals list of 81.

53
00:04:42,470 --> 00:04:43,370
‫This is my list.

54
00:04:44,540 --> 00:04:48,560
‫The elements of the list are the characters of the string.

55
00:04:49,710 --> 00:04:57,810
‫Let's see what is the ID or the memory address where the list is stored, this is Eakes address.

56
00:04:58,830 --> 00:05:07,710
‫I'll try to modify the first list element so at least one of zero equals 5.5.

57
00:05:07,920 --> 00:05:08,780
‫This is a float.

58
00:05:09,060 --> 00:05:09,980
‫This is my list.

59
00:05:10,230 --> 00:05:11,600
‫We've got no error.

60
00:05:11,730 --> 00:05:17,270
‫And if I want to see ecocide, we notice we have the same object.

61
00:05:17,280 --> 00:05:20,600
‫So the initial object has been modified.

62
00:05:21,330 --> 00:05:29,130
‫These are lists, basics, and we'll talk about lists, operations and methods in the next lecture's.

63
00:05:29,370 --> 00:05:31,560
‫See you in just a few seconds.


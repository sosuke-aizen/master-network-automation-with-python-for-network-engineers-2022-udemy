﻿1
00:00:00,390 --> 00:00:08,100
‫In this lecture, we'll talk about Topal methods, because topolice are immutable objects, they have

2
00:00:08,100 --> 00:00:16,110
‫fewer available methods than links there are and methods for adding or removing elements from a tuple.

3
00:00:16,410 --> 00:00:19,260
‫In fact, there are only two methods.

4
00:00:19,620 --> 00:00:27,840
‫The count and the index method, let's see, is I'll create a tuple from one iterable.

5
00:00:28,230 --> 00:00:33,870
‫A tuple can also be created from any iterable object using the tuple constructor.

6
00:00:34,110 --> 00:00:39,060
‫So Taiyuan equals topple off and I'll pass in a string.

7
00:00:43,290 --> 00:00:48,920
‫And two equals, and I'll write some integers.

8
00:00:53,020 --> 00:01:01,840
‫These are my topolice, the count method returns how many times an element appears in the.

9
00:01:03,400 --> 00:01:08,530
‫So let's write to you on that count of a.

10
00:01:10,040 --> 00:01:16,560
‫And it returned three weeks because a appears three times in the double.

11
00:01:17,700 --> 00:01:22,890
‫If an element doesn't belong to the people, it will return a zero.

12
00:01:24,080 --> 00:01:27,080
‫So X doesn't belong to my Topal.

13
00:01:28,810 --> 00:01:37,930
‫The index method returns the index of the specified item in the tuple, if we have duplicates, the

14
00:01:37,930 --> 00:01:43,730
‫method returns only the first match is in the case of leaks.

15
00:01:43,750 --> 00:01:48,430
‫We can also provide a starting and ending index.

16
00:01:50,610 --> 00:02:02,130
‫The one that index of a hit will return zero because a Hayes index is here and this is the first match,

17
00:02:03,090 --> 00:02:10,950
‫the to date index of two, it will return five to his index five.

18
00:02:12,390 --> 00:02:21,000
‫If I want to see the index of a nonexisting element, for example, twenty six, I'll get an air of

19
00:02:21,000 --> 00:02:27,300
‫value error x nothing Topal if I don't want to get an error.

20
00:02:27,480 --> 00:02:31,230
‫But I want to search for the index of an element.

21
00:02:31,620 --> 00:02:34,150
‫I can use an if statement.

22
00:02:34,290 --> 00:02:37,560
‫So if 26 in the two.

23
00:02:37,710 --> 00:02:41,970
‫So only if the element belongs to the tuple.

24
00:02:42,000 --> 00:02:48,240
‫I'll search for its index t to that index of twenty six.

25
00:02:49,760 --> 00:02:51,790
‫And I've got no Ed.

26
00:02:53,250 --> 00:03:04,010
‫The built in functions Max Min Lin and started are also available to topolice so Max of Taiwan, it

27
00:03:04,050 --> 00:03:07,150
‫will return the maximum element of the topic.

28
00:03:07,620 --> 00:03:08,840
‫This is F.

29
00:03:10,110 --> 00:03:13,110
‫Mean we'll return the minimum element.

30
00:03:14,710 --> 00:03:19,360
‫And Lynn will return how many elements has the Topal?

31
00:03:22,510 --> 00:03:26,020
‫I can also use the sorted method.

32
00:03:27,570 --> 00:03:36,990
‫The sorted method doesn't modify the table, it returns a new table with the elements sorted in ascending

33
00:03:36,990 --> 00:03:37,530
‫order.

34
00:03:38,220 --> 00:03:47,070
‫If I want to return a table with the elements sorted in descending order, I use an optional argument

35
00:03:47,220 --> 00:03:49,860
‫called reverse equals to.

36
00:03:56,720 --> 00:04:05,420
‫And they're returning to topple has been sorted in descending order, of course, the initial topolice

37
00:04:05,420 --> 00:04:06,980
‫haven't been modified.

38
00:04:09,810 --> 00:04:18,230
‫These were top Olympics in the next lecture will see when to use at least and when to use a table,

39
00:04:18,420 --> 00:04:24,360
‫what are the advantages of leagues and the advantages of topolice?

40
00:04:24,420 --> 00:04:26,450
‫See you in just a few seconds.


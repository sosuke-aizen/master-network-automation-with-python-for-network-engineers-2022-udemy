﻿1
00:00:01,420 --> 00:00:06,080
‫We've seen so far a couple ways of adding to a list now.

2
00:00:06,100 --> 00:00:10,060
‫We'll take a look at how can we remove items from our list.

3
00:00:10,420 --> 00:00:13,270
‫The first and the simplest method is clear.

4
00:00:13,540 --> 00:00:17,580
‫It will empty the list or remove all elements at once.

5
00:00:18,340 --> 00:00:25,630
‫I'll create a list called Leper's using the list constructor and a string as an argument.

6
00:00:26,350 --> 00:00:26,590
‫A.

7
00:00:26,590 --> 00:00:27,490
‫B, c, d.

8
00:00:30,050 --> 00:00:41,390
‫This is my list now, letters dot clear, and I've emptied the list, if we want to remove a single

9
00:00:41,390 --> 00:00:45,110
‫element from the least, we can use the pop method.

10
00:00:45,800 --> 00:00:47,630
‫I'll create a list one more time.

11
00:00:48,080 --> 00:00:50,750
‫And now letters dot pop.

12
00:00:52,270 --> 00:01:00,700
‫The pop method removes the element at the given position in the list and returns that element by default.

13
00:01:00,700 --> 00:01:07,390
‫If we don't perceive an argument, which is the position at which it will remove the element, it removes

14
00:01:07,390 --> 00:01:08,680
‫the last element.

15
00:01:09,310 --> 00:01:13,810
‫So in this case, it removed the last element of my list.

16
00:01:16,030 --> 00:01:24,250
‫But let's suppose I want to remove the first element, the element, eh, in this case, I can have

17
00:01:24,250 --> 00:01:25,270
‫something like this.

18
00:01:26,730 --> 00:01:38,340
‫Like, say, value equals letters, dot pop of zero zero is the index at which it will remove the element

19
00:01:38,850 --> 00:01:46,230
‫and that element will be returned or it will be stored in the value variable.

20
00:01:48,440 --> 00:01:58,880
‫Value contains a and A doesn't belong any more to my list, if we pass in an out of range value, it

21
00:01:58,880 --> 00:02:00,440
‫will return an error.

22
00:02:01,750 --> 00:02:12,100
‫Then he's an index out of range value pop index out of range, the last method used to remove from the

23
00:02:12,100 --> 00:02:13,590
‫list is remove.

24
00:02:13,900 --> 00:02:21,940
‫It works different in the way that we perceive the value of the element we want to remove, not index.

25
00:02:22,600 --> 00:02:26,310
‫This method will find and remove the first match.

26
00:02:26,620 --> 00:02:36,400
‫So if we pass is argument X, it will search for X and remove the first occurrence of X or it raises

27
00:02:36,400 --> 00:02:37,810
‫a value error.

28
00:02:37,990 --> 00:02:45,700
‫If the value X is not present, if there are multiple value X in the list, it removes only the first

29
00:02:45,700 --> 00:02:46,110
‫match.

30
00:02:47,980 --> 00:02:53,740
‫Comparing to pop method remove doesn't return the value it removes.

31
00:02:54,870 --> 00:02:56,340
‫Lexi unexampled.

32
00:02:58,750 --> 00:03:06,910
‫Letters, equally silly stuff, and here I'll pass in some letters, also some duplicates.

33
00:03:10,280 --> 00:03:17,780
‫This is my list, and I want to remove the value a so letters dot, remove a.

34
00:03:21,850 --> 00:03:23,810
‫Here I made a mistake.

35
00:03:23,860 --> 00:03:28,540
‫A is a string, so I must use single or double coax.

36
00:03:29,740 --> 00:03:31,480
‫A between simulcasts.

37
00:03:32,930 --> 00:03:36,710
‫It has removed the first occurrence of a.

38
00:03:39,690 --> 00:03:50,250
‫We can see how the first element of the list is now be it didn't remove all occurrences of a if you

39
00:03:50,250 --> 00:03:55,470
‫want to remove all occurrences of a value, you can use a while loop.

40
00:03:56,930 --> 00:04:02,660
‫While the value I want to remove, let's say, a in letters.

41
00:04:04,630 --> 00:04:14,740
‫So as long as this condition returns to so A belongs to the letter E, it will execute the will block

42
00:04:14,740 --> 00:04:22,510
‫of code and I have only one instruction letters that remove off a.

43
00:04:24,710 --> 00:04:33,020
‫Basically, it says that as long as a belongs to the least, it will iterate over the list and will

44
00:04:33,020 --> 00:04:34,700
‫remove that value.

45
00:04:36,340 --> 00:04:41,110
‫We can see how A has been removed from the letters list.

46
00:04:44,390 --> 00:04:52,320
‫Another liste method is index iterators, the index of the specified item in the list.

47
00:04:52,640 --> 00:04:56,990
‫It also allows us to specify a start and an end.

48
00:04:57,590 --> 00:05:01,730
‫So letters dot index of B.

49
00:05:06,740 --> 00:05:10,220
‫Sorry, the name of the list is letters, not letter.

50
00:05:15,800 --> 00:05:21,830
‫And it returned to zero because B is at index zero.

51
00:05:22,800 --> 00:05:35,580
‫Let us index of these will return to because these is at index two zero one and two, if we have duplicates

52
00:05:35,580 --> 00:05:40,550
‫in the list, the index method will return only the first match.

53
00:05:41,750 --> 00:05:45,890
‫Let's add some duplicate letters that extend.

54
00:05:47,680 --> 00:05:51,160
‫And they'll pass in some duplicate letters.

55
00:05:59,270 --> 00:06:02,600
‫Letters dot index of the.

56
00:06:03,770 --> 00:06:11,240
‫And it returned to so this is the index of the first of the first match.

57
00:06:13,130 --> 00:06:23,000
‫But I can also provide a starting point or index, for example, letters dot index of D and for.

58
00:06:24,090 --> 00:06:36,180
‫This basically says, find the index of the after the index, four and four is inclusive, so zero one,

59
00:06:36,180 --> 00:06:37,420
‫two, three, four.

60
00:06:37,770 --> 00:06:47,910
‫So after this point and this point is included, so after the hour letter and there is a D at index

61
00:06:48,090 --> 00:06:48,720
‫six.

62
00:06:50,330 --> 00:06:59,180
‫And finally, we can have a start and an end index in this case, we are looking between start and end.

63
00:07:01,660 --> 00:07:07,540
‫Letters dot index of B three comma then.

64
00:07:08,790 --> 00:07:19,260
‫It returned nine, the index of the first to be it found between index three, and then if we want the

65
00:07:19,260 --> 00:07:25,880
‫index of a value that doesn't belong to the list, we'll get an error of value error.

66
00:07:26,280 --> 00:07:31,200
‫For example, I am searching for index of X Lepper.

67
00:07:32,280 --> 00:07:34,800
‫X is not enlist.

68
00:07:37,480 --> 00:07:45,040
‫Another method will take a look at these count each three times the number of times a value appears

69
00:07:45,040 --> 00:07:50,080
‫in the list, if the value doesn't belong to the list, it returns zeer.

70
00:07:51,470 --> 00:07:52,760
‫Letters don't count.

71
00:07:54,040 --> 00:07:55,090
‫Of a.

72
00:07:56,510 --> 00:07:59,570
‫A ups two times in my list.

73
00:08:01,380 --> 00:08:03,600
‫B appears three times.

74
00:08:04,950 --> 00:08:07,860
‫But Z doesn't belong to the least.

75
00:08:08,900 --> 00:08:11,060
‫The method returned to zero.


﻿1
00:00:00,300 --> 00:00:03,960
‫In this lecture, we'll talk about topolice versus.

2
00:00:05,070 --> 00:00:12,270
‫Maybe you are wondering why should I bother using a tuple when it has fewer available methods and lacks

3
00:00:12,270 --> 00:00:14,160
‫the flexibility of a list?

4
00:00:15,030 --> 00:00:21,270
‫When you are a beginner programmer, you don't use topolice that often, but it's become more advanced

5
00:00:21,270 --> 00:00:22,080
‫in Python.

6
00:00:22,230 --> 00:00:24,830
‫You'll start to use topolice frequently.

7
00:00:25,260 --> 00:00:29,670
‫Lexi, what are the advantages of a table over a list?

8
00:00:30,330 --> 00:00:37,650
‫First, topolice are faster and the more efficient, then leaks if you have one or that collection of

9
00:00:37,650 --> 00:00:43,800
‫data that you know is not going to change, you can make it topple and it will be faster.

10
00:00:44,010 --> 00:00:49,980
‫Exemplars of collections that never change are the letters of the alphabet.

11
00:00:50,190 --> 00:00:58,450
‫The days of the week, the month of the year and many others, they are always in the same order and

12
00:00:58,450 --> 00:00:59,760
‫they never change.

13
00:01:00,630 --> 00:01:04,200
‫Python uses a process called constant folding.

14
00:01:04,480 --> 00:01:12,870
‫This means recognizing and evaluating constant expressions at compile time rather than computing them

15
00:01:12,870 --> 00:01:13,770
‫at runtime.

16
00:01:14,460 --> 00:01:21,960
‫So Python recognizes and evaluates a pople at compile time and the not at runtime.

17
00:01:22,470 --> 00:01:30,670
‫One important remark is that papers are really efficient when they contain immutable objects like numbers,

18
00:01:30,870 --> 00:01:32,820
‫strings or other topolice.

19
00:01:33,120 --> 00:01:39,690
‫If they contain immutable objects like lists, populists are not so efficient comparing to lists.

20
00:01:40,200 --> 00:01:45,210
‫Another advantage is that topolice are safer than lists.

21
00:01:45,480 --> 00:01:51,570
‫Using paperless instead of lists will make your code safer from bugs and other problems.

22
00:01:51,840 --> 00:01:57,060
‫When you use topolice, you know that they don't accidentally get changed.

23
00:01:57,400 --> 00:01:59,550
‫This is called data integrity.

24
00:02:00,210 --> 00:02:04,650
‫Then topolice can be used as keys in dictionaries.

25
00:02:04,920 --> 00:02:07,710
‫Lists are not valid as keys.

26
00:02:09,650 --> 00:02:18,500
‫Storage efficiency is also very important, a topple consumes less memory than a list here.

27
00:02:18,620 --> 00:02:20,480
‫I want to show you an example.

28
00:02:23,390 --> 00:02:31,310
‫Using the paper constructor and the range function, I'll create a table that contains one million integers.

29
00:02:32,180 --> 00:02:38,540
‫So Taiwan equals Poppel of range of one million.

30
00:02:40,630 --> 00:02:45,380
‫I can write to one million like this or to be more readable.

31
00:02:45,580 --> 00:02:51,030
‫I can use an underscore instead of a comma, which is a special character.

32
00:02:51,040 --> 00:02:53,170
‫So one million can be written.

33
00:02:53,170 --> 00:02:59,600
‫Also, like this one underlined three zeros, underline three zeros of X..

34
00:02:59,620 --> 00:03:02,440
‫OK, in Python, this is my Topal.

35
00:03:03,390 --> 00:03:14,880
‫Then I'll create a list that also contains one million integers, so L1 equals list of range of one

36
00:03:14,880 --> 00:03:15,420
‫million.

37
00:03:18,590 --> 00:03:23,390
‫Now, using the CS module, we'll talk about it in another lecture.

38
00:03:23,750 --> 00:03:28,820
‫Let's see how many bikes do these data structures occupy.

39
00:03:29,510 --> 00:03:30,780
‫So importances.

40
00:03:31,610 --> 00:03:35,360
‫And now don't get the size of this one.

41
00:03:38,450 --> 00:03:43,490
‫And don't get size of 11, which is my list.

42
00:03:45,450 --> 00:03:53,880
‫We can see how a table with a million items of type integer occupies more than eight million bytes in

43
00:03:53,880 --> 00:04:01,080
‫the list that also has one million items of type integer occupies more than nine million bikes.

44
00:04:01,410 --> 00:04:05,220
‫That's an increase of memory consumption of nine percent.

45
00:04:05,340 --> 00:04:06,450
‫And the big.

46
00:04:07,620 --> 00:04:14,700
‫It's a conclusion we can say that Apple is more efficient than at least, and it's better to use a Topal

47
00:04:14,700 --> 00:04:19,200
‫each time we have a collection of items that never changes.


﻿1
00:00:00,790 --> 00:00:08,320
‫Now I want to show you how to send how to execute comments on a networking device from a Python script

2
00:00:08,530 --> 00:00:17,800
‫using a serial console connection, even if in these examples I am using a Cisco device, you can use

3
00:00:17,800 --> 00:00:26,290
‫the serial module to configure any networking device, any networking device that has a console port.

4
00:00:26,620 --> 00:00:31,680
‫So the same methods will be used in a multi vendor environment.

5
00:00:31,780 --> 00:00:33,970
‫They are not Cisco specific.

6
00:00:34,270 --> 00:00:38,720
‫The serial module has two important methods.

7
00:00:39,250 --> 00:00:47,680
‫The first is right that is used to send a command to the device using the serial connection.

8
00:00:47,800 --> 00:00:57,250
‫And then we have the lead method that is used to hit a number of bikes from that console.

9
00:00:58,180 --> 00:01:02,230
‫Let's take first a look at how the right method works.

10
00:01:02,620 --> 00:01:06,600
‫First, we must know that it's working with bikes.

11
00:01:06,640 --> 00:01:14,190
‫So if we want to send a command, we must convert that string, that comment to a buycks object.

12
00:01:14,200 --> 00:01:19,540
‫And after that, we passed those bikes is an argument to the right function.

13
00:01:19,730 --> 00:01:24,420
‫It is mandatory to wait for the command to complete execution.

14
00:01:24,820 --> 00:01:29,770
‫So sleep of one, two or more seconds is mandatory.

15
00:01:30,670 --> 00:01:39,580
‫OK, this is very important, and when we are reading from the console first we are calling the in waiting

16
00:01:39,580 --> 00:01:48,250
‫function, these function returns an integer that represents the number of bytes that can be read from

17
00:01:48,250 --> 00:01:49,180
‫the console.

18
00:01:49,420 --> 00:01:59,470
‫After that, we are using the read function that returns Buycks object or a Buycks variable, and then

19
00:01:59,620 --> 00:02:03,560
‫we are decoding to string using the Decode function.

20
00:02:03,820 --> 00:02:06,040
‫So let's see an example here.

21
00:02:06,730 --> 00:02:13,030
‫Before starting writing code, I want to test my comments in Putti.

22
00:02:13,180 --> 00:02:16,570
‫I'll open Puti and I'll connect to the device.

23
00:02:17,050 --> 00:02:19,450
‫So first I'll press on enter.

24
00:02:19,570 --> 00:02:28,240
‫An entry is necessary, then I'll execute the enable command and after that I'll execute the show version

25
00:02:28,540 --> 00:02:36,580
‫and I'll capture the output inviable that will be displayed at the Python console.

26
00:02:37,210 --> 00:02:39,430
‫Let me turn now to bicarb.

27
00:02:39,520 --> 00:02:47,410
‫If you are using windows, you must know that you can't open two connections to the same console.

28
00:02:47,500 --> 00:02:55,270
‫So first we must close Pooty and then we can run commands from Python.

29
00:02:55,420 --> 00:03:03,070
‫If you are using Linux, you can use the same connection from multiple points so we can, for example,

30
00:03:03,070 --> 00:03:05,650
‫open a connection to the device using both the end.

31
00:03:05,650 --> 00:03:10,960
‫In the same time, we can use the same kind of support to run a Python script.

32
00:03:11,320 --> 00:03:13,130
‫So let's write console dot.

33
00:03:13,160 --> 00:03:13,580
‫Right.

34
00:03:14,470 --> 00:03:18,820
‫This means I am sending a command to my device.

35
00:03:19,120 --> 00:03:22,300
‫The command should be sent is buycks.

36
00:03:22,300 --> 00:03:24,010
‫So B E from Buycks.

37
00:03:24,290 --> 00:03:32,620
‫This is not a string, this is bytes and I'll send a backslash and backslash and means new line or an

38
00:03:32,620 --> 00:03:38,070
‫enter key and then I'll wait for the command to complete execution.

39
00:03:38,350 --> 00:03:41,960
‫For that I must import the time module.

40
00:03:41,980 --> 00:03:46,390
‫So import time and here time don't sleep.

41
00:03:47,000 --> 00:03:51,150
‫Let's say one one second is enough for this comment.

42
00:03:51,520 --> 00:03:55,270
‫After that I'll send the enable command.

43
00:03:55,540 --> 00:03:56,500
‫So console.

44
00:03:57,070 --> 00:03:57,730
‫Right.

45
00:03:59,090 --> 00:04:06,530
‫B from bikes enable backslash and backslash is very important here because after the comment we must

46
00:04:06,530 --> 00:04:10,210
‫press of the enter key, otherwise it doesn't work.

47
00:04:11,670 --> 00:04:15,220
‫We wait for the comment to complete execution.

48
00:04:15,600 --> 00:04:20,880
‫So here are right time, though, to sleep and one after that.

49
00:04:21,300 --> 00:04:32,550
‫All right, let's say show version, backslash and and then time don't sleep and let's say now three.

50
00:04:32,700 --> 00:04:38,370
‫So I suppose that this comment takes longer time to execute.

51
00:04:38,520 --> 00:04:45,930
‫One more important thing, when we are executing shortcomings, we must execute.

52
00:04:45,930 --> 00:04:48,650
‫Also, the terminal length is zero comment.

53
00:04:48,840 --> 00:04:50,700
‫So let's return here to Puti.

54
00:04:51,150 --> 00:04:57,030
‫I'll open the connection to my out there and later on the show version comment.

55
00:04:57,300 --> 00:05:01,680
‫As you can see, it doesn't display the whole output on the screen.

56
00:05:01,800 --> 00:05:11,060
‫So I have this uh more work here and I must press on uh space in order to show the next screen.

57
00:05:11,160 --> 00:05:16,620
‫So there is the terminal, a length command and zero E for no pausing.

58
00:05:16,830 --> 00:05:22,350
‫Now when I run show version, it will display the whole output.

59
00:05:23,130 --> 00:05:32,280
‫OK, so back to my script, to my Python script before the show version come in, I'll execute the terminal

60
00:05:32,280 --> 00:05:33,950
‫length zero comment.

61
00:05:34,140 --> 00:05:36,780
‫So terminal length is zero.

62
00:05:39,310 --> 00:05:45,550
‫After executing these comments, I want to display the output.

63
00:05:45,760 --> 00:05:53,500
‫So let's say bikes to be read equals console in waiting, in waiting will return to an integer that

64
00:05:53,500 --> 00:05:57,580
‫represents the number of bikes that can be read from the console.

65
00:05:57,970 --> 00:06:04,120
‫And then output equals console to treat bikes to Berat.

66
00:06:05,190 --> 00:06:11,960
‫This is a buycks, not a string, so I'll paint a string, this means output dot decode.

67
00:06:12,330 --> 00:06:18,370
‫I am decoding the bytes to a string using the default certificate encoding scheme.

68
00:06:18,600 --> 00:06:20,250
‫I think that's OK.

69
00:06:20,400 --> 00:06:23,250
‫So now I can run the script.

70
00:06:24,350 --> 00:06:31,420
‫Kind of sort of successfully opened and we can see here our comings and also the output.

71
00:06:31,550 --> 00:06:40,940
‫So I've sent the naval command, then the terminal link, the zero show version, and I've displayed

72
00:06:40,940 --> 00:06:47,450
‫at the console the whole output from my connections from the console connection.

73
00:06:47,840 --> 00:06:54,260
‫As you can see, everything works as expected in the in the next lecture.

74
00:06:54,470 --> 00:07:03,500
‫I'll show you how to refactor or how to create our own module to work easier with the serial connections

75
00:07:03,740 --> 00:07:07,070
‫to the console of networking devices.

76
00:07:07,400 --> 00:07:08,650
‫See you in a minute.


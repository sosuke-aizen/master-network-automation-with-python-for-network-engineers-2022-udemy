﻿1
00:00:00,750 --> 00:00:07,590
‫In this section, we'll talk about connecting from a computer or a laptop to a networking device using

2
00:00:07,590 --> 00:00:14,970
‫a single connection and the device console port, every professional networking device like her out

3
00:00:14,970 --> 00:00:20,160
‫there, or August, which has a console port also known as a management port.

4
00:00:20,760 --> 00:00:28,230
‫The console port is used to connect a computer directly to our out there or switch into configure either

5
00:00:28,230 --> 00:00:33,120
‫out there or switch from the computer, sees the device has no display.

6
00:00:34,440 --> 00:00:42,480
‫We also use the kind of support for low level troubleshooting, and when no other connection method

7
00:00:42,510 --> 00:00:48,840
‫works, we use it for of recovery, firmware upgrade and so on.

8
00:00:49,290 --> 00:00:55,110
‫The console port must be used to initially configure the networking device because initially there is

9
00:00:55,110 --> 00:01:02,760
‫no network connection there out there has no IP address configured and we can't connect using SSL Telnet

10
00:01:02,760 --> 00:01:13,230
‫or HTP normally or outer console port is Arjay forty five port to connect from one laptop to the console

11
00:01:13,230 --> 00:01:13,660
‫port.

12
00:01:13,680 --> 00:01:19,180
‫We need the console cable which is a Arjay forty five to DP nine.

13
00:01:19,410 --> 00:01:20,680
‫This is the cable.

14
00:01:20,880 --> 00:01:29,760
‫This is a console cable and we also need an USP to address 232 to DP nine.

15
00:01:30,360 --> 00:01:34,680
‫This is the USB to serial converter that is required.

16
00:01:35,100 --> 00:01:42,420
‫You must know that we must have physical access to the device in order to configure it using a single

17
00:01:42,420 --> 00:01:44,730
‫connection and the console port.

18
00:01:45,180 --> 00:01:53,630
‫The operating system also needs a driver for the USB to RSA 232 DP nine converter.

19
00:01:54,090 --> 00:02:01,890
‫A modem Linux operating system has normally this driver included and in Windows, depending on the version

20
00:02:02,100 --> 00:02:07,870
‫and the converter type, we must or we must not install a driver anyway.

21
00:02:08,100 --> 00:02:16,520
‫I take such a driver is a resource to this lecture and we also need an application like Pooty or Minicam.

22
00:02:16,920 --> 00:02:23,270
‫Now I want to show you how to test the connection to the outer console port.

23
00:02:23,370 --> 00:02:27,840
‫I have a Cisco device on my desk and I'll open pooty.

24
00:02:30,330 --> 00:02:40,650
‫Here, I must write the correct serial line, so let's see if my operating system has the driver included

25
00:02:40,890 --> 00:02:51,750
‫using my computer, I'll go to properties, device manager and then airports, we can see the prolific

26
00:02:51,760 --> 00:02:54,230
‫WISBEY to serial comport commentary.

27
00:02:54,390 --> 00:02:58,340
‫So this is my serial line using Putti.

28
00:02:58,380 --> 00:03:01,170
‫I'll write home free at the serial line.

29
00:03:01,530 --> 00:03:07,800
‫If there is a problem with the driver for the cable, you will see something like this.

30
00:03:07,920 --> 00:03:09,840
‫So you must be serial controller.

31
00:03:09,870 --> 00:03:13,080
‫And here is an exclamation mark.

32
00:03:13,290 --> 00:03:15,660
‫This is when something is wrong.

33
00:03:15,660 --> 00:03:18,900
‫In this case, probably you need a driver.

34
00:03:19,050 --> 00:03:23,310
‫OK, so here, I'll write code free and open.

35
00:03:24,330 --> 00:03:31,590
‫Now, I'll press on enter and I can see that I have a single connection to the router, if you are using

36
00:03:31,590 --> 00:03:35,910
‫Linux, you can check what is the serial line in the following manner.

37
00:03:36,300 --> 00:03:44,040
‫Here I am connected to a Linux that is connected using a secure line to a router.

38
00:03:44,130 --> 00:03:52,950
‫I ran the message come in and we can see that the secure line is why you must zero.

39
00:03:53,100 --> 00:03:55,380
‫So input one moment.

40
00:03:55,380 --> 00:03:58,530
‫Let's open put it here in Linux.

41
00:03:58,530 --> 00:04:00,180
‫You have to write something like this.

42
00:04:00,180 --> 00:04:02,340
‫Slash dev, slash deti.

43
00:04:02,340 --> 00:04:10,220
‫Why you s.b zero USB with uppercase letters exactly like here.

44
00:04:10,770 --> 00:04:16,680
‫OK, so if I work on Linux I'd use this connection here.

45
00:04:18,630 --> 00:04:28,530
‫OK, I'll choose cereal, and here they've selected you, why you as be easier and open, I'll recommend

46
00:04:28,530 --> 00:04:36,870
‫you to test your connection to the device before starting developing Python scripts that use the serial

47
00:04:36,870 --> 00:04:37,500
‫connection.

48
00:04:37,650 --> 00:04:45,630
‫In the next lecture, I'll show you how to connect and how to run commands from Python to a Cisco device

49
00:04:45,780 --> 00:04:50,500
‫using a serial line in the device console port.

50
00:04:50,730 --> 00:04:52,490
‫See you in a few seconds.


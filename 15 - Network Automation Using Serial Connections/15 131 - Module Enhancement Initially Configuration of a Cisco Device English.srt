﻿1
00:00:00,900 --> 00:00:09,120
‫Now I want to enhance, to improve and to extend the script from the previous lecture, so let's see

2
00:00:09,120 --> 00:00:15,780
‫what happens when we are first connecting to a default device.

3
00:00:15,990 --> 00:00:19,920
‫So to a device that hasn't been configured.

4
00:00:21,920 --> 00:00:30,950
‫So first I pressed on enter and then we have this initial configuration dialogue, so in order to be

5
00:00:30,950 --> 00:00:37,470
‫able to send commands, we must pass over this initial configuration dialog.

6
00:00:37,640 --> 00:00:43,100
‫So here I must the right now and press on enter.

7
00:00:43,310 --> 00:00:44,780
‫Wait some time.

8
00:00:49,390 --> 00:00:52,690
‫It seems I must wait many seconds.

9
00:00:53,930 --> 00:00:57,140
‫And then I'll press on enter one more time.

10
00:01:00,710 --> 00:01:10,610
‫And this is the moment when I came around comments, so let's create a function that passes over the

11
00:01:10,610 --> 00:01:21,500
‫initial configuration dialogue, so I'll create a function, let's say check initial configuration dialog.

12
00:01:22,940 --> 00:01:24,770
‫This function has an argument.

13
00:01:25,990 --> 00:01:28,990
‫The console object is working on.

14
00:01:31,740 --> 00:01:35,940
‫Console and the commands, of course, backslash.

15
00:01:36,160 --> 00:01:44,070
‫So the first new line, the first enter key I've pressed, uh, this is, uh, by default anyway we

16
00:01:44,070 --> 00:01:44,790
‫can see here.

17
00:01:45,030 --> 00:01:48,960
‫But for clarity, I still write pixelation.

18
00:01:49,960 --> 00:01:58,840
‫After that, I want to read from the console to see what is the output, I want to see if the string

19
00:01:58,840 --> 00:02:06,660
‫that I receive contains the would you like to enter the initial configuration dialog substring.

20
00:02:06,820 --> 00:02:15,940
‫So let's say prompt equals read from console and the argument is the console object.

21
00:02:16,390 --> 00:02:22,030
‫And now if Analects come here and I'll copy paste this string.

22
00:02:23,550 --> 00:02:32,790
‫Here, so if would you like to enter the initial configuration dialogue in prompt, this is the case

23
00:02:32,790 --> 00:02:40,270
‫when displaying that initial configuration dialog string now around the command.

24
00:02:40,320 --> 00:02:47,240
‫So I'm sending in fact, I'm sending the now string and I must wait a longer time.

25
00:02:47,250 --> 00:02:49,860
‫So let's say 15 seconds.

26
00:02:50,220 --> 00:02:59,340
‫I've discovered in a practical way that, uh, after entering the now string, we must wait a longer

27
00:02:59,340 --> 00:02:59,700
‫time.

28
00:02:59,850 --> 00:03:01,620
‫I have an older device.

29
00:03:01,620 --> 00:03:07,800
‫If you have a newer device, maybe here you can wait a shorter time.

30
00:03:08,250 --> 00:03:12,630
‫But, uh, for our example, I'll let 15 seconds.

31
00:03:12,900 --> 00:03:16,950
‫And then this is the second and third we must press.

32
00:03:17,100 --> 00:03:31,350
‫So command console and here it once backslash r backslash m backslash r backslash M is used by some

33
00:03:31,350 --> 00:03:39,080
‫operating systems like older windows, older Mac OS and some Cisco devices.

34
00:03:39,270 --> 00:03:48,420
‫So I've also discovered in a practical way, so doing different tasks that it works better when I am

35
00:03:48,420 --> 00:03:56,280
‫using backslash r backslash and instead of only backslash M, and if I reach this point I return to,

36
00:03:56,670 --> 00:04:00,750
‫otherwise I return false.

37
00:04:00,990 --> 00:04:06,410
‫OK, now I want to check my function to test how it works.

38
00:04:06,690 --> 00:04:10,110
‫So in another script I'll import my module.

39
00:04:10,110 --> 00:04:12,320
‫So import my cereal.

40
00:04:12,750 --> 00:04:17,750
‫OK, I'll create a new connection to the console device.

41
00:04:17,750 --> 00:04:18,110
‫So.

42
00:04:18,170 --> 00:04:32,100
‫So let's say console equals and I'll call the open console function so my cereal does not open console

43
00:04:32,910 --> 00:04:39,950
‫and here there is no need for an argument because by default I am using the com three port and the boundary

44
00:04:40,140 --> 00:04:43,560
‫is nine thousand six hundred.

45
00:04:44,100 --> 00:04:51,090
‫OK, after I've opened the connection I want to run some cumming's.

46
00:04:51,360 --> 00:04:56,280
‫So first I want to check if there is the initial configuration dialog.

47
00:04:56,670 --> 00:05:12,600
‫My serial dot check initial configuration dialog off console console is the object returned by the open

48
00:05:12,600 --> 00:05:17,460
‫console function and after that I want to run some comix.

49
00:05:17,460 --> 00:05:20,250
‫So the first comment will be enable.

50
00:05:20,260 --> 00:05:26,730
‫I want to enter the enablement, then configure terminal and I'll create a user.

51
00:05:26,970 --> 00:05:29,850
‫So my cereal dot.

52
00:05:30,930 --> 00:05:31,800
‫Veronica meant.

53
00:05:34,830 --> 00:05:37,140
‫And the comment is enable.

54
00:05:39,620 --> 00:05:41,150
‫Then my serial.

55
00:05:42,960 --> 00:05:54,490
‫The other comment and the comment is configure terminal in the last comment, I want to run username

56
00:05:55,800 --> 00:05:57,570
‫like say you want.

57
00:05:58,680 --> 00:06:10,080
‫Secret Cisco, and here another command is on and I want to return to the enable mode.

58
00:06:10,770 --> 00:06:11,180
‫OK.

59
00:06:12,150 --> 00:06:12,630
‫So.

60
00:06:13,690 --> 00:06:18,460
‫I'll open the connection using Puti and I'll talk about the.

61
00:06:20,970 --> 00:06:27,120
‫I must wait for the out to start till then I'll post the recording.

62
00:06:30,050 --> 00:06:38,600
‫OK, they're out there, started now I am closing pooty because in windows we can have two connections

63
00:06:38,750 --> 00:06:41,720
‫to the same console, so I'm closing pooty.

64
00:06:43,330 --> 00:06:50,140
‫And before writing the script, I want to display at the picture, console the output.

65
00:06:50,320 --> 00:06:58,690
‫So here I write my serial read from console off console.

66
00:06:59,290 --> 00:07:01,000
‫This is the output.

67
00:07:03,020 --> 00:07:06,500
‫And on this line, I'll point to the output.

68
00:07:06,860 --> 00:07:11,090
‫OK, here the first argument is the console object.

69
00:07:11,610 --> 00:07:14,040
‫Sorry, I forgot to write here console.

70
00:07:15,120 --> 00:07:18,320
‫OK, I think this is a clear console.

71
00:07:19,530 --> 00:07:25,800
‫Because it's sending the command, using this connection, this console connection.

72
00:07:26,160 --> 00:07:27,420
‫OK, that's better.

73
00:07:27,930 --> 00:07:29,580
‫And now I'll run the script.

74
00:07:33,560 --> 00:07:41,070
‫Extending the first backslash and command the Nable command, the configure terminal username you want

75
00:07:41,070 --> 00:07:51,750
‫to take with Cisco and the end command, and after that, after it has sent all the commands, it displayed

76
00:07:51,840 --> 00:07:52,920
‫the output.

77
00:07:54,980 --> 00:08:06,030
‫OK, this is the output, if I open pooty and they are on show run include yuzu, we can see the user

78
00:08:06,050 --> 00:08:07,720
‫that has been created.

79
00:08:07,880 --> 00:08:10,640
‫So it worked as expected.

80
00:08:12,010 --> 00:08:21,010
‫This is how we can figure out there from Python using a serial connection and throughout the console.

81
00:08:22,050 --> 00:08:30,360
‫In the next lecture, I'll show you how to configure out there from a file, so how to run commands

82
00:08:30,540 --> 00:08:31,680
‫from a file.

83
00:08:32,100 --> 00:08:36,840
‫This is very useful for the initial configuration of a device.


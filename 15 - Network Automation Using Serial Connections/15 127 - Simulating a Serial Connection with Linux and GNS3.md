

In order to test the **pyserial** module and automate the configuration of
networking devices over serial connections, you need a physical device to
connect to.

However, there is a way to simulate a serial console in GNS3 and connect to it
from a Linux machine (tested with Ubuntu, GNS3 is installed and runs on
Ubuntu).

  

 **1\. Install the following packages on Ubuntu:**

  *  _sudo apt install screen_

  *  _sudo apt install socat_

  

 **2.** Start the device in GNS3 and find out the port to which the telnet
console is mapped

On Ubuntu execute: **ps -ef | grep telnet**

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-01-18_11-40-14-9ba40bf8801aa4b35dde12d8733c0cf4.png)

In this example, the port is 5008.

  

 **3.** In a terminal, execute the next command to **map** the **telnet** port
to a **virtual serial** port:

 _socat pty,link=/home/YOUR_USER/ttyS0,raw,echo=1 tcp:localhost:5008_

  

 **4.** In another terminal, execute the next command to initiate a session:

 _screen /home/YOUR_USER/ttyS0 9600_

  

After, we press **ENTER** and close the terminal when the **device prompt**
has appeared.

  

In the first terminal, we press **Ctrl+c** to cancel the **socat** command,
and we execute the same one more time.

 _socat pty,link=/home/YOUR_USER/ttyS0,raw,echo=1 tcp:localhost:5008_

  

Now, we can use the pyserial module in our Python programs:

    
    
    import serial
    import time
    
    with serial.Serial(port='/home/YOUR_USER/ttyS0', baudrate=9600, parity='N', stopbits=1, bytesize=8, timeout=8) as console:
        if console.isOpen():
            print('Serial is Opened')
            console.write(b'\n')
            time.sleep(1)
            console.write(b'show version\n')
            time.sleep(3)
    
            bytes_to_be_read = console.inWaiting()
            output = console.read(bytes_to_be_read)
            print(output.decode())


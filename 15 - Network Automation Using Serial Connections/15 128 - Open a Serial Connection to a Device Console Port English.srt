﻿1
00:00:00,800 --> 00:00:07,640
‫Hello, guys, in this lecture, I'll show you how to connect to a kind of support of a networking device

2
00:00:07,850 --> 00:00:08,780
‫from Python.

3
00:00:09,200 --> 00:00:16,610
‫This is especially important because you can automate the many tasks using or writing Python scripts

4
00:00:16,760 --> 00:00:21,000
‫that configure devices using their console port.

5
00:00:21,620 --> 00:00:29,330
‫We are going to use the buy cereal package for connecting to the console port of a networking device.

6
00:00:29,660 --> 00:00:36,250
‫This module doesn't belong to the Python standard library and we need to install it.

7
00:00:36,680 --> 00:00:41,330
‫So here I'll go to file settings project interpretor.

8
00:00:43,250 --> 00:00:52,130
‫And they'll press on the plus side, and here I am searching for my serial biofilms, serial port extension

9
00:00:52,310 --> 00:00:53,990
‫and install package.

10
00:00:58,090 --> 00:01:01,510
‫The package has been successfully installed.

11
00:01:02,960 --> 00:01:12,710
‫And now I can import the module, so import Sariel, take care, the name of the package is by Serial,

12
00:01:12,890 --> 00:01:16,740
‫but the name of the module is simply serial.

13
00:01:17,000 --> 00:01:20,170
‫So I am importing the serial module.

14
00:01:20,690 --> 00:01:28,550
‫I ran the script just to check that the module is available so that I don't get on there and we can

15
00:01:28,550 --> 00:01:30,380
‫see that there is no error.

16
00:01:30,560 --> 00:01:39,550
‫Now the first step when working with a console port is to open that port or to open that connection.

17
00:01:39,770 --> 00:01:44,150
‫So I'll use the serial method that opens this port.

18
00:01:44,330 --> 00:01:50,180
‫This is the port used when we are connecting from a Linux operating system.

19
00:01:50,420 --> 00:01:53,930
‫So here, I'll change it to three.

20
00:01:54,140 --> 00:01:59,870
‫The serial line used in Windows or in my Windows operating system.

21
00:01:59,870 --> 00:02:00,590
‫Maybe you have.

22
00:02:00,800 --> 00:02:02,180
‫Com one or two.

23
00:02:02,340 --> 00:02:10,250
‫You have to check it in this example, I have a Cisco device on my desk that is directly connected to

24
00:02:10,250 --> 00:02:11,030
‫my laptop.

25
00:02:13,850 --> 00:02:23,360
‫A very good idea is to check the connection, the console connection using Pooty before testing it from

26
00:02:23,360 --> 00:02:27,350
‫Python, because maybe there is another problem, a physical problem.

27
00:02:27,360 --> 00:02:34,370
‫The cable doesn't work or there is a problem with the console part of the device and we try to troubleshoot

28
00:02:34,370 --> 00:02:35,030
‫from Python.

29
00:02:35,030 --> 00:02:37,110
‫But there is no Python problem.

30
00:02:37,130 --> 00:02:39,190
‫This is the console, right?

31
00:02:39,200 --> 00:02:46,340
‫The console bound rate, and these are other parameters used for this connection.

32
00:02:46,460 --> 00:02:51,230
‫Most of the time we don't have to change these parameters.

33
00:02:51,350 --> 00:03:04,970
‫And now if console is open, preened, console successfully opened else print error opening the console

34
00:03:04,970 --> 00:03:05,630
‫connection.

35
00:03:08,640 --> 00:03:10,030
‫And I run the script.

36
00:03:10,200 --> 00:03:19,300
‫So here we can see the message console successfully opened when we are using the with keyboard.

37
00:03:19,350 --> 00:03:24,690
‫There is no need to manually or to explicitly close the console connection.

38
00:03:25,050 --> 00:03:28,030
‫Python does it automatically for us.

39
00:03:28,740 --> 00:03:37,460
‫This is how we open a connection to a console port of a networking device in the next lecture.

40
00:03:37,500 --> 00:03:44,330
‫I'll show you how to run commands using this connection and how to read from this connection.

41
00:03:44,350 --> 00:03:50,190
‫So how to display the output from the serial connection.

42
00:03:50,400 --> 00:03:52,170
‫See you in a few seconds.


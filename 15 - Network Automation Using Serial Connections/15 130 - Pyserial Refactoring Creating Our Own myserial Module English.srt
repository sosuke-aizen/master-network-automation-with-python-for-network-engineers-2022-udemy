﻿1
00:00:00,530 --> 00:00:07,910
‫Hello, guys, in this lecture, I want to show you how to create your own module that uses the serial

2
00:00:07,910 --> 00:00:08,400
‫module.

3
00:00:08,750 --> 00:00:18,530
‫I want to show you how to enhance and how to extend this module this way of developing scripts that

4
00:00:18,770 --> 00:00:22,560
‫are working with the console of our outer.

5
00:00:22,820 --> 00:00:23,450
‫He's OK.

6
00:00:23,660 --> 00:00:27,670
‫But there are things that are complicated.

7
00:00:27,680 --> 00:00:32,390
‫For example, we must always convert from string to buycks.

8
00:00:32,690 --> 00:00:40,630
‫We must use the backslash and bought a new line or for the enter key we must the right here time don't

9
00:00:40,640 --> 00:00:41,060
‫sleep.

10
00:00:41,060 --> 00:00:42,380
‫And the number of seconds.

11
00:00:42,800 --> 00:00:50,410
‫All these things could be excluded by writing our own module.

12
00:00:50,600 --> 00:00:53,290
‫So I'll save this file.

13
00:00:53,720 --> 00:01:02,300
‫Let's say is my serial dot B and I'll delete this code first.

14
00:01:02,300 --> 00:01:11,630
‫I want to create a function, let's say open serial or open console that opens a connection to the console

15
00:01:11,630 --> 00:01:12,340
‫of the device.

16
00:01:12,530 --> 00:01:17,840
‫So let's say def open console and is arguments here.

17
00:01:18,020 --> 00:01:19,130
‫I need the port.

18
00:01:20,480 --> 00:01:28,060
‫By default, this is column three, this is the default argument, there is no problem if we use another

19
00:01:28,070 --> 00:01:37,940
‫port here and then bond rate, which is by default, nine thousand six hundred, I must delete the with

20
00:01:38,210 --> 00:01:47,440
‫keyword because when using the with keyboard, it will automatically close the connection when it's

21
00:01:47,450 --> 00:01:48,460
‫done with it.

22
00:01:48,680 --> 00:01:53,510
‫So I don't want to close the connection because I am going to call this function.

23
00:01:53,840 --> 00:02:00,350
‫And after calling the function, after the function returns, I still want to have a life connection

24
00:02:00,350 --> 00:02:03,910
‫because I want to execute Cumming's and so on.

25
00:02:04,040 --> 00:02:15,410
‫So I simply right here console equals and this function here and I'll delete a console from here and

26
00:02:15,410 --> 00:02:18,650
‫now if console is open, return.

27
00:02:20,190 --> 00:02:23,620
‫I return the console object.

28
00:02:23,850 --> 00:02:25,980
‫Else return false.

29
00:02:26,160 --> 00:02:31,950
‫So when the function returns false, I know that the connection couldn't be opened.

30
00:02:32,760 --> 00:02:35,980
‫This is my open console function.

31
00:02:36,330 --> 00:02:37,950
‫One more thing.

32
00:02:38,550 --> 00:02:45,600
‫It's important to return to the console object because I'll use this object in other functions, for

33
00:02:45,600 --> 00:02:46,090
‫example.

34
00:02:46,110 --> 00:02:48,010
‫Now I want to create another function.

35
00:02:48,030 --> 00:02:49,260
‫Let's say you're on a command.

36
00:02:49,350 --> 00:02:53,190
‫So Def Run Command and his argument.

37
00:02:53,190 --> 00:02:54,990
‫I have the console object.

38
00:02:55,230 --> 00:03:04,230
‫Maybe I am connected to more devices, let's say to three devices and I am using three console connections

39
00:03:04,350 --> 00:03:14,070
‫and I am sending some comments on the first device, some comments on the second device and other comments

40
00:03:14,070 --> 00:03:15,860
‫on the third device.

41
00:03:16,050 --> 00:03:21,720
‫So it's important to know to which device to send to the command.

42
00:03:22,020 --> 00:03:24,030
‫The second argument will be the command.

43
00:03:24,030 --> 00:03:30,510
‫Let's ACMD by default XA Backslash M and I want to have another argument.

44
00:03:30,720 --> 00:03:34,350
‫Let's say sleep, which is by default one.

45
00:03:35,350 --> 00:03:44,580
‫This is how many seconds should my script wait after the comment is sent to the device?

46
00:03:45,100 --> 00:03:54,010
‫So by default is one and if I want more seconds, I can simply use this argument and I'll print sending

47
00:03:54,130 --> 00:03:54,760
‫comment.

48
00:03:55,060 --> 00:03:59,400
‫Plus CMT and our console Dota, right.

49
00:03:59,560 --> 00:04:01,750
‫See the DOT in code.

50
00:04:02,090 --> 00:04:05,620
‫I am encoding from string to buycks.

51
00:04:05,950 --> 00:04:17,200
‫I must send the command is buycks not as a string plus B from Buycks backslash and this is the enter

52
00:04:17,200 --> 00:04:17,590
‫key.

53
00:04:17,590 --> 00:04:19,660
‫So the command and then the enter.

54
00:04:19,990 --> 00:04:26,620
‫So a new line and after that time dot slip and now slip the function argument.

55
00:04:27,720 --> 00:04:30,780
‫OK, this is the one comment function.

56
00:04:32,710 --> 00:04:42,460
‫And the third function I want to create now is a function that reads and displays the output from my

57
00:04:42,460 --> 00:04:43,100
‫console.

58
00:04:43,600 --> 00:04:53,070
‫So let's say read from console and also this function must have as an argument the console object.

59
00:04:53,770 --> 00:05:01,690
‫And here I ride bikes to be read equals kind of soldat in waiting.

60
00:05:01,930 --> 00:05:09,190
‫So if there is something that can be read, so if bikes to be read, this means that the bikes to be

61
00:05:09,190 --> 00:05:13,260
‫read these intakes are variable is not zero.

62
00:05:13,300 --> 00:05:22,790
‫Output equals console dot read dot bikes to be read in our return output dot the code.

63
00:05:22,810 --> 00:05:27,100
‫So I am decoding from bytes to string else.

64
00:05:27,550 --> 00:05:36,580
‫If there is nothing to be read I simply return like say false or I can write a message, something like

65
00:05:36,790 --> 00:05:39,600
‫nothing to be read from serial connection.

66
00:05:39,790 --> 00:05:40,870
‫I think that's OK.

67
00:05:41,290 --> 00:05:43,960
‫Now let's test our three functions.

68
00:05:43,990 --> 00:05:51,280
‫So first I'll open a connection to the console of my Cisco device.

69
00:05:51,430 --> 00:05:53,250
‫So I'm using Conemaugh Report.

70
00:05:53,320 --> 00:05:56,080
‫I don't need to use this argument.

71
00:05:56,080 --> 00:06:02,020
‫And also the bond rate is nine thousand six hundred, so there is no need for an argument.

72
00:06:02,020 --> 00:06:10,270
‫I use the default options, open console and then run command and let's say by default, I want to send

73
00:06:10,270 --> 00:06:11,190
‫an enter.

74
00:06:11,230 --> 00:06:15,940
‫So a new line, the enter key, so there is no need to write here a command.

75
00:06:16,540 --> 00:06:23,170
‫After that I run the show version command, so run command show version.

76
00:06:23,680 --> 00:06:32,560
‫And after that I want to read from the console output equals read from console and here open console

77
00:06:32,770 --> 00:06:34,480
‫returning to the console object.

78
00:06:34,490 --> 00:06:42,160
‫So let's right here con equals and here I also need to pass the console object as an argument.

79
00:06:42,160 --> 00:06:50,800
‫So run command cone and here the same one command corn and hear it from the console, from the cone

80
00:06:50,800 --> 00:06:51,700
‫console.

81
00:06:51,700 --> 00:06:53,650
‫And that's the only argument.

82
00:06:53,650 --> 00:06:57,460
‫And I'll print output and now I'll run the script.

83
00:06:57,460 --> 00:06:59,350
‫So run my serial.

84
00:07:03,400 --> 00:07:10,050
‫And we can see here the output, we can see how it has sent the backslash.

85
00:07:10,260 --> 00:07:14,680
‫So this is a new line here and then the short version come in.

86
00:07:14,920 --> 00:07:24,030
‫And this is the output, the output that was waiting to be displayed or to be read from the console.

87
00:07:24,910 --> 00:07:31,530
‫If there are problems, you can test it increasing this time from here.

88
00:07:31,540 --> 00:07:34,290
‫So maybe one is too low.

89
00:07:34,330 --> 00:07:42,270
‫So maybe here a two would be better because we must wait for the command to complete its execution.

90
00:07:42,400 --> 00:07:53,170
‫So sometimes we, depending on the device on and how powerful the devices, we must wait longer or shorter

91
00:07:53,170 --> 00:07:54,750
‫for a specific comment.

92
00:07:54,880 --> 00:07:57,190
‫So I'll let here two.

93
00:07:57,250 --> 00:07:58,480
‫I think two is better.

94
00:07:58,930 --> 00:08:01,180
‫One more time I'll execute the script.

95
00:08:06,060 --> 00:08:08,280
‫OK, and this is the output.

96
00:08:08,730 --> 00:08:18,360
‫Now, let's compare this way of connecting to the device and the executing commands with this way,

97
00:08:19,110 --> 00:08:25,410
‫you can notice that in this example, everything is much more readable.

98
00:08:25,590 --> 00:08:34,560
‫And the simpler we don't use a backslash and we don't convert from bytes to string and so on, we are

99
00:08:34,560 --> 00:08:42,590
‫just calling some functions and the we are sending commands using the command name.

100
00:08:42,900 --> 00:08:44,850
‫So it's much easier this way.

101
00:08:45,120 --> 00:08:51,640
‫More than that, we can use the my serial is a module in another file.

102
00:08:51,750 --> 00:08:57,810
‫So for example, I'll create a new script like, say, Serial, this one.

103
00:08:58,050 --> 00:09:07,680
‫And here I'll copy and paste this code and now import my cereal and now I'll use something like this.

104
00:09:07,690 --> 00:09:11,820
‫So my real daughter open console.

105
00:09:13,760 --> 00:09:24,260
‫My serial, doctorand command, my serial command and hear the same, I must have my serial dot read

106
00:09:24,260 --> 00:09:25,360
‫from console.

107
00:09:25,520 --> 00:09:31,990
‫OK, I'll run the script and you can see that we get the same result.

108
00:09:32,420 --> 00:09:38,600
‫So we are simply using the module we've defined earlier.

109
00:09:38,600 --> 00:09:47,740
‫So this file is a module and anyone can use the functionality of my module without knowing the insight.

110
00:09:47,840 --> 00:09:56,180
‫That person doesn't have to know how to write into the console, how to read from the console.

111
00:09:56,360 --> 00:10:02,120
‫He simply calls the wrong command function and the read from the console function.

112
00:10:02,420 --> 00:10:09,980
‫This is the recommended way to run Python scripts that are connecting to a device using a serial console

113
00:10:09,980 --> 00:10:10,640
‫connection.

114
00:10:11,470 --> 00:10:19,690
‫In the next lecture will enhance our module, our MIT serial module, with other features.


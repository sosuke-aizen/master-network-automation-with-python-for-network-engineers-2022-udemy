﻿1
00:00:00,300 --> 00:00:07,710
‫In this lecture, I want to show you how to configure a device using a serial console connection from

2
00:00:07,710 --> 00:00:08,340
‫a file.

3
00:00:08,970 --> 00:00:16,860
‫I'll create a file that contains commands that must be executed on our outer with a default configuration.

4
00:00:17,190 --> 00:00:20,550
‫This is very useful when there is a very big network.

5
00:00:20,700 --> 00:00:29,000
‫Let's assume we've received 100 counters and we must initial configure these routers.

6
00:00:29,130 --> 00:00:35,670
‫So first of all, we must configure routers with passwords, with an IP address and so on.

7
00:00:35,790 --> 00:00:37,900
‫And this takes time.

8
00:00:38,100 --> 00:00:45,020
‫So I'll create a file here, a text file like, say, config dot text in here.

9
00:00:45,120 --> 00:00:46,620
‫I'll write to the comix.

10
00:00:46,740 --> 00:00:51,910
‫So the first comment is enable then configure terminal.

11
00:00:52,410 --> 00:00:57,720
‫I'll set the enable password enable secret Cisco.

12
00:00:58,970 --> 00:01:10,820
‫Al Qaeda username, username, let's say you want secret Cisco and the for our example, I'll configure

13
00:01:11,180 --> 00:01:13,010
‫a Lubeck interface.

14
00:01:13,220 --> 00:01:24,990
‫So interface loop back zero IP address 1.0 1.0 on two hundred fifty five 255 255 255.

15
00:01:25,250 --> 00:01:29,270
‫This is a host musk end the end command.

16
00:01:29,630 --> 00:01:34,880
‫This will return to the enable mode and exit.

17
00:01:35,360 --> 00:01:38,050
‫I exit the connection.

18
00:01:38,060 --> 00:01:39,170
‫I am saving the file.

19
00:01:40,110 --> 00:01:49,230
‫And here, using the script from the previous lecture, I'll erase I'll delete these lines, and instead

20
00:01:49,230 --> 00:01:55,670
‫of these hard coded commands, I'll write with open config dot text.

21
00:01:55,680 --> 00:02:06,710
‫So I am opening the file in read-only mode as F command equals F dot ridgelines.

22
00:02:07,110 --> 00:02:09,860
‫So I am reading in a list.

23
00:02:10,320 --> 00:02:14,640
‫OK, command or let's say commands is a list.

24
00:02:14,860 --> 00:02:25,380
‫And now for CMD in commands my serial dot run command of console and CMT.

25
00:02:26,070 --> 00:02:35,010
‫I think voxel I run the script but uh first of all I have to restart the router so I can load and uh

26
00:02:35,220 --> 00:02:37,940
‫I must wait for the router to restart.

27
00:02:38,070 --> 00:02:40,680
‫I'll post the recording till then.

28
00:02:40,950 --> 00:02:42,750
‫See you in a few seconds.

29
00:02:43,050 --> 00:02:45,480
‫OK, the rotary started successfully.

30
00:02:45,780 --> 00:02:47,700
‫I am closing pooty.

31
00:02:48,890 --> 00:02:55,850
‫And I run the script, the script is running, this is the nose thing for the initial configuration

32
00:02:55,850 --> 00:03:01,040
‫dialogue, and after that it will send my comments.

33
00:03:01,040 --> 00:03:03,170
‫So the comments from this five.

34
00:03:04,320 --> 00:03:09,880
‫The Naval Command, the Conficker terminal, enable secret Cisco.

35
00:03:10,470 --> 00:03:15,990
‫It creates the username and the Lubezki interface with the IP address.

36
00:03:19,790 --> 00:03:29,510
‫And that's all OK, it didn't display at the console because the connection closed, so here the last

37
00:03:29,510 --> 00:03:36,080
‫comment is exit and it closed the connection and it couldn't read from a close connection.

38
00:03:36,080 --> 00:03:42,230
‫But let's check it opening pooty, so I'll open pooty.

39
00:03:44,380 --> 00:03:48,020
‫And we can see it's asking for a password.

40
00:03:48,310 --> 00:03:57,490
‫This is the enable password we've just sent, so it's the password and the firing a run include, let's

41
00:03:57,490 --> 00:03:58,840
‫say, interface.

42
00:04:00,200 --> 00:04:04,910
‫We can see the lookback zero interface or stripey interface brief.

43
00:04:06,910 --> 00:04:12,640
‫Here is our interface, so our Skype work is expected.

44
00:04:12,970 --> 00:04:17,740
‫This is a very good script that can be used to initial configure.

45
00:04:18,100 --> 00:04:25,690
‫Many outlets, if we have a very large number of networking devices, we can configure them from a file

46
00:04:25,690 --> 00:04:28,670
‫like in this example voxel.

47
00:04:28,840 --> 00:04:29,440
‫Thank you.


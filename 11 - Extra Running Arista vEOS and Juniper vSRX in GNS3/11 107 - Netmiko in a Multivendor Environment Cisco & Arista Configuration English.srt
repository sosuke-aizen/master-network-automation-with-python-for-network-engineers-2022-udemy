﻿1
00:00:00,620 --> 00:00:07,370
‫In this lecture, I'm going to show you how to configure these multi vendor environments using net namiko

2
00:00:07,490 --> 00:00:14,960
‫we have with these Queyras, Kraeutler and Tewari Aristarchus switches, they are running on Jenness

3
00:00:14,960 --> 00:00:21,890
‫three on another machine and they have our access to that machine and to these three devices.

4
00:00:22,250 --> 00:00:24,250
‫They aren't configured.

5
00:00:24,260 --> 00:00:32,480
‫I've configured just the IP address and stage in order to be able to connect to them and the I'm going

6
00:00:32,480 --> 00:00:38,730
‫to configure villans and the routing protocol on each device.

7
00:00:38,930 --> 00:00:42,930
‫So let's take a look at the script I've just created.

8
00:00:43,310 --> 00:00:46,100
‫First, I want to show you this file.

9
00:00:46,310 --> 00:00:47,950
‫The device is DOT.

10
00:00:47,960 --> 00:00:58,490
‫The file contains the type of the device she's quires or ARISTOS, the IP address of the device, the

11
00:00:58,490 --> 00:01:02,410
‫user name and the password used for S.H..

12
00:01:03,480 --> 00:01:13,410
‫And here I am going to read the file in a list, in fact, file content is a list and the each line

13
00:01:13,860 --> 00:01:15,200
‫is an element.

14
00:01:15,360 --> 00:01:23,640
‫After that, I am iterating through this list and I am splitting the list, using the column character

15
00:01:23,640 --> 00:01:27,260
‫as a separator, and I am creating a new list.

16
00:01:27,540 --> 00:01:30,550
‫In fact, here, this will be a list.

17
00:01:31,470 --> 00:01:36,690
‫This will be the first element of the list of the IP address will be the second element of the list,

18
00:01:37,050 --> 00:01:42,660
‫the user name, the third element of the list and the password, the last element of the list.

19
00:01:43,670 --> 00:01:48,990
‫This will be the second list with its first element, second element and so on.

20
00:01:49,140 --> 00:01:54,540
‫And the third list and all lists will belong to another list.

21
00:01:54,870 --> 00:01:58,380
‫Better legs, copy paste this code.

22
00:02:00,000 --> 00:02:08,760
‫Here and here, let's print file content, and after that, let's print the devices list.

23
00:02:11,030 --> 00:02:18,560
‫And I'm going to run the script, OK, this is the file content list, we can see a single list.

24
00:02:18,830 --> 00:02:25,430
‫This is the first element of the list, the second element of the list and the last element of the list.

25
00:02:25,730 --> 00:02:31,250
‫And then I am processing for the list by creating another list.

26
00:02:31,980 --> 00:02:34,730
‫And this is the first element of the list.

27
00:02:34,730 --> 00:02:39,620
‫The device type with the IP address is the second element of the list and so on.

28
00:02:39,890 --> 00:02:48,320
‫I need this list because it's easier to iterate and to have access to this information.

29
00:02:48,800 --> 00:02:49,810
‫Back to our script.

30
00:02:49,820 --> 00:02:52,730
‫I am iterating through the devices list.

31
00:02:53,890 --> 00:03:02,050
‫Device will be an element for my list, so this will be the device, at least in the first iteration,

32
00:03:02,170 --> 00:03:03,710
‫in the second iteration.

33
00:03:03,760 --> 00:03:06,300
‫This will be the device list and so on.

34
00:03:06,310 --> 00:03:08,050
‫And I am creating a dictionary.

35
00:03:08,620 --> 00:03:12,010
‫The device type is the first element of the device least.

36
00:03:13,210 --> 00:03:16,220
‫So to us then, I have the iPod.

37
00:03:16,240 --> 00:03:20,530
‫Guess the second element of the list, the username and password.

38
00:03:21,010 --> 00:03:27,550
‫We have the same password as the secret password, but we could have had another password if we have

39
00:03:27,550 --> 00:03:28,140
‫wanted.

40
00:03:28,990 --> 00:03:37,360
‫I can simply add here another field or I can use the get pass module after that time, creating a connection

41
00:03:37,360 --> 00:03:37,960
‫object.

42
00:03:38,170 --> 00:03:41,130
‫And I am entering the enable mode.

43
00:03:41,140 --> 00:03:48,220
‫Of course, if I'm not already in the enablement and then and this is very interesting, I am asking

44
00:03:48,220 --> 00:03:50,200
‫for a configuration file.

45
00:03:50,710 --> 00:03:55,210
‫The user must enter the configuration file for each device.

46
00:03:55,390 --> 00:03:58,840
‫Each device has its own configuration file.

47
00:03:59,290 --> 00:04:06,450
‫After entering the configuration file, I am creating the file and I am sending the comments.

48
00:04:06,610 --> 00:04:14,650
‫In fact, this part from here is exactly as in our previous script, from the previous lecture, the

49
00:04:14,650 --> 00:04:16,330
‫configuration file, for example.

50
00:04:16,580 --> 00:04:19,990
‫These queries, I am going to have a list.

51
00:04:20,380 --> 00:04:27,220
‫Each line will be an element of the list and I'm going to send the content of the list to the device.

52
00:04:27,250 --> 00:04:31,600
‫Of course, by using the sent config set function.

53
00:04:31,630 --> 00:04:39,730
‫I've explained to you in detail in the previous lecture you can keep it from there after sending the

54
00:04:39,730 --> 00:04:40,420
‫comments.

55
00:04:40,420 --> 00:04:45,360
‫I am disconnecting and I am printing forty hashes.

56
00:04:45,730 --> 00:04:49,120
‫This is for the clarity of the output.

57
00:04:49,420 --> 00:04:53,920
‫Perfect insight of the configuration files for the devices.

58
00:04:54,070 --> 00:05:03,850
‫I start the routing protocol viks all I am configuring on the C score out there and on the artist switches.

59
00:05:04,120 --> 00:05:14,080
‫I am starting and configuring the routing protocol, but I am also creating to villans this trunk connection

60
00:05:14,080 --> 00:05:21,280
‫from between the three keys and I am adding the interfaces to their VLAN.

61
00:05:21,940 --> 00:05:27,030
‫In fact, I've explained the configuration in the previous lecture.

62
00:05:27,490 --> 00:05:33,400
‫This is the configuration for the first switch and this is the configuration for the second switch.

63
00:05:33,670 --> 00:05:35,200
‫Everything is the same.

64
00:05:35,440 --> 00:05:46,570
‫Only the IP addresses of the switched Vitol interface are different, exactly as in the image you see

65
00:05:46,570 --> 00:05:50,770
‫here if I try to display the routing table shapir out.

66
00:05:52,330 --> 00:06:00,480
‫There is now a remote network of SPF is not running and there is no villain on the switch, and that's

67
00:06:00,490 --> 00:06:02,530
‫the same for each device.

68
00:06:03,070 --> 00:06:09,490
‫Now back to the machine that has access to the topology, this Linux machine.

69
00:06:09,890 --> 00:06:11,560
‫I'm going to run the script.

70
00:06:11,890 --> 00:06:15,780
‫I've copied the script to the machine earlier.

71
00:06:16,030 --> 00:06:23,040
‫So we have exactly the script and I'm going to execute the script in this way.

72
00:06:23,200 --> 00:06:27,910
‫So Python three named Michael Star, Cisco Multi Vendor.

73
00:06:29,750 --> 00:06:31,520
‫It's connecting to the first device.

74
00:06:33,630 --> 00:06:40,980
‫The first device is my daughter and configuration file is named Second-line Config Dot Texte.

75
00:06:43,110 --> 00:06:44,970
‫Asking for the configuration file.

76
00:06:46,520 --> 00:06:55,340
‫Executing this line from here, the line 31, so I am going to enter the name of X configuration file,

77
00:06:55,610 --> 00:06:57,450
‫see scandalising, config, dot.

78
00:06:58,760 --> 00:07:03,560
‫And now extending the comments to our device.

79
00:07:05,530 --> 00:07:13,090
‫These are the comments, it configured the routing protocol, and now I must enter the configuration

80
00:07:13,090 --> 00:07:21,970
‫file for the second device, the second device is the first to switch sorry, star one underlined config

81
00:07:21,970 --> 00:07:31,240
‫TotEx, the ICS connecting to the switch and executing the comments after that, disconnecting.

82
00:07:31,360 --> 00:07:34,900
‫And it goes further to the last device.

83
00:07:35,320 --> 00:07:45,370
‫We can see how it created the violence, how it created the switch to virtual interface and configured

84
00:07:45,550 --> 00:07:53,500
‫IP addresses, and how it started and configured the routing protocol here.

85
00:07:53,770 --> 00:08:01,380
‫The configuration file of the last device is a star to underline config dot text.

86
00:08:05,280 --> 00:08:09,070
‫And the last device has been configured.

87
00:08:09,750 --> 00:08:17,250
‫Now let's check the configuration and the connectivity between each device, we can already see how

88
00:08:17,250 --> 00:08:22,440
‫the routing protocol changed each state from loading to whole.

89
00:08:22,470 --> 00:08:29,480
‫If I want to ship out, I can see here all roads that have been learned through or SPF.

90
00:08:30,180 --> 00:08:32,570
‫And let's check the connectivity.

91
00:08:32,790 --> 00:08:41,790
‫For example, I want to ping from the score after each switch interface, each switch virtual interface.

92
00:08:43,060 --> 00:08:54,850
‫So from here being one hundred sixty eight, one hundred ninety two 168, the 10.1 dot to the twenty

93
00:08:55,150 --> 00:09:05,440
‫one twenty two, I can also check the connectivity between switches and between villans ping.

94
00:09:06,250 --> 00:09:08,500
‫I ping their outer being works.

95
00:09:08,770 --> 00:09:11,470
‫I love being the first three to one.

96
00:09:15,160 --> 00:09:22,100
‫And we can see that we have full connectivity between devices and everything works as expected.

97
00:09:22,420 --> 00:09:31,800
‫This is a very interesting script that can be used to configure an environment of multiple vendors.

98
00:09:32,290 --> 00:09:35,470
‫We have information about devices in a file.

99
00:09:35,620 --> 00:09:40,810
‫We can add other devices, we can add other types of devices.

100
00:09:40,810 --> 00:09:45,530
‫And we also have a configuration file for each device.

101
00:09:45,940 --> 00:09:49,900
‫So this example is also scalable.




 **Juniper vSRX** is a virtual security appliance that provides security and
networking services at the perimeter or edge in virtualized environments.
**vSRX runs as a virtual machine (VM) on a standard x86 server. vSRX** uses
one virtual CPU (vCPU) for the JCP and at least one vCPU for the PFE.

  

 **Requirements**

\- Linux x86-64

\- Qemu or VirtualBox installed ( _apt-get install qemu-system-x86_ on Ubuntu)

\- GNS3 2.1.11 or greater

\- 4 GB of memory per vSRX instance is recommended

  

 **Step 1: Juniper Account**

Create a free account on[ http://www.juniper.net](http://www.juniper.net)

  

 **Step 2: Download vSRX image from http://www.juniper.net**

Download an evalution version of **vSRX KVM Appliance**. This is a qcow2 file
(Example: media-vsrx-vmdisk-17.3R2.10.qcow2).

The size of the file is 3.6 GB.

![](https://img-c.udemycdn.com/redactor/raw/2018-11-01_11-59-56-c9bad5fbf04740d8d18a7924060ddefb.png)

  

 **Step 3: Download the vSRX appliance from GNS3 Marketplace**

<https://www.gns3.com/marketplace/appliances>

![](https://img-c.udemycdn.com/redactor/raw/2018-11-01_12-00-17-12910238bf1687563529b3d5126d391c.png)

  

 **Step 4: Import appliance in GNS3**

In GNS3 go to **File** → **Import Appliance**

  

 **Step 5: Drag & Drop sVRX and fallow the instruction on the screen. **

The process is straight-forward.

  

In GNS3 at Settings (right click on vSRX Device) use:

\- 4096 GB RAM

\- 2 CPU

\- VMWare paravirtualized Ethernet

![](https://img-c.udemycdn.com/redactor/raw/2018-11-01_11-59-04-5bfc032bb152157fc740914084d9f963.png)

  

![](https://img-c.udemycdn.com/redactor/raw/2018-11-01_11-59-31-a661ae17fb057d3c0ee179611b1559ff.png)

  

  

 **Step 5: Start the vSRX device and log in using the root username and no
password**

  

 **Note:** the startup process can take a very long time (more than 5 minutes
on an my i7 Intel CPU with 16 GB RAM and SSH Disk).

  

The output will be something like this:

  

 _Starting optional daemons: usbd._

 _Doing initial network setup:_

 _._

 _Initial interface configuration:_

 _kenv: unable to get vmtype_

 _additional daemons: eventd._

 _checking for core dump..._

 _savecore: Reboot reason(s): 0x4000: VJUNOS reboot_

 _savecore: Reboot reason(s): 0x4000: VJUNOS reboot_

 _savecore: no dumps found_

 _Additional routing options:kern.module_path: /boot//kernel;/boot/modules - >
/boot/modules;/modules/peertype;/modules/ifpfe_drv;/modules/platform;/modules;_

 _kld netpfe drv: ifpfed_ep ifpfed_esp ifpfed_ism ifpfed_ml_ha ifpfed_ppeer
ifpfed_ps ifpfed_st ifpfed_vtkld platform: if_em_vsrx if_vtnet virtio
virtio_blk virtio_console virtio_pcikld peertype: peertype_fwdd peertype_pfpc
grat_arp_delay=1: net.link.ether.inet.grat_arp_delay: 1 - > 1_

 _[: -eq: unexpected operator_

 _ipsec kldcryptosoft0: <software crypto> on motherboard_

 _kats kldIPsec: Initialized Security Association Processing._

 _resrsv._

 _Doing additional network setup:._

 _Starting final network daemons:._

 _setting ldconfig path: /usr/lib /opt/lib_

 _starting standard daemons: cron._

 _Initial rc.i386 initialization:._

 _Local package initialization:._

 _starting local daemons:set cores for group access_

 _._

 _kern.securelevel: -1 - > 1_

 _kern.timecounter.hardware: ACPI-safe - > TSC_

 _kern.timecounter.hardware: TSC - > TSC_

 _kern.maxfiles: 2500 - > 10500_

 _kern.maxfilesperproc: 2500 - > 10500_

 _The machine id is empty._

 _Cleaning up ..._

 _Thu Nov 1 11:46:26 UTC 2018_

 _Nov 1 11:46:26 init: mib-process (PID 1742) started_

  

 _Amnesiac (ttyd0)_

  

 _login: root_

  

 _root@% OS 17.3R2.10 built 2018-02-08 02:19:07 UTC_

 _root@%_

  


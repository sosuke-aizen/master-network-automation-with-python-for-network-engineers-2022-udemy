﻿1
00:00:01,370 --> 00:00:06,590
‫Now, let's take a look on how to use the net Mieko in a multi vendor environment.

2
00:00:07,370 --> 00:00:18,710
‫I have this topology with a Cisco iris out there and to our star that ran a video as an extensible operating

3
00:00:18,710 --> 00:00:19,490
‫system.

4
00:00:19,980 --> 00:00:25,170
‫And I want to show you how to run comments on these devices.

5
00:00:25,430 --> 00:00:32,530
‫In fact, we are going to configure these devices with violence and with a routing protocol.

6
00:00:32,930 --> 00:00:43,280
‫So let's take a look here at this very basic script comparing to our previous scripts I've just modified

7
00:00:43,280 --> 00:00:45,530
‫here, the device type.

8
00:00:46,070 --> 00:00:56,270
‫If until now we've configured only Cisco devices and we had here Cisco iOS for an artist to switch,

9
00:00:56,270 --> 00:01:00,110
‫we must right here to underline E OS.

10
00:01:00,650 --> 00:01:02,780
‫And then everything is the same.

11
00:01:03,290 --> 00:01:07,640
‫We create the connection object by calling the connect handler function.

12
00:01:08,700 --> 00:01:16,210
‫Here, I check if I am already in the enablement and if not, I am entering the enable mode.

13
00:01:16,470 --> 00:01:23,790
‫After that, I'm calling the send comment method of the connection object and I am sending the show

14
00:01:23,790 --> 00:01:26,030
‫interface status comment.

15
00:01:26,430 --> 00:01:36,210
‫The output is in this variable which is printed here on this line and at the end I disconnect.

16
00:01:36,930 --> 00:01:41,990
‫Net myco has been created for multi vendor environments.

17
00:01:42,000 --> 00:01:50,490
‫Each method is available no matter if we are using a Cisco on our star or a juniper and so on.

18
00:01:50,490 --> 00:01:52,300
‫We have the same comix.

19
00:01:53,100 --> 00:01:54,900
‫Now let's check this script.

20
00:01:55,320 --> 00:01:57,030
‫There is one detail here.

21
00:01:57,180 --> 00:02:05,970
‫I have this topology on another machine because this recording machine doesn't have enough or a memory.

22
00:02:06,400 --> 00:02:14,490
‫So I've installed three and all these devices on another machine, but there is no difference.

23
00:02:14,760 --> 00:02:18,080
‫I will call the script in the same way only here.

24
00:02:18,210 --> 00:02:24,600
‫You must take care that we have other IP addresses is in our previous scripts.

25
00:02:25,050 --> 00:02:29,650
‫We have the IP addresses that are shown here.

26
00:02:30,330 --> 00:02:32,570
‫This is my star.

27
00:02:32,790 --> 00:02:38,970
‫I have a console connection to the switch and this is the machine from where I will run the script.

28
00:02:42,220 --> 00:02:49,600
‫This is the script I've just explained to you, and now Python three and the name of the script, the

29
00:02:49,600 --> 00:02:52,120
‫script is asking for the password.

30
00:02:52,300 --> 00:03:01,270
‫The password is Oggi Star is connecting to the device and shortly it will display the command output.

31
00:03:01,280 --> 00:03:03,010
‫And this is the command output.

32
00:03:06,300 --> 00:03:15,270
‫For the password, I've used the get pass module, let's see now another example, let's configure this

33
00:03:15,270 --> 00:03:20,580
‫week with two valence VLAN 10 and VLAN 20.

34
00:03:21,240 --> 00:03:25,470
‫Interface E three belongs to VLAN 20.

35
00:03:25,470 --> 00:03:29,670
‫Interface E4 belongs to Vilan then.

36
00:03:29,940 --> 00:03:36,960
‫Then I create two switched virtual interfaces for Vallentine and Villon 20.

37
00:03:36,990 --> 00:03:46,200
‫With these IP addresses and the interface E to Ethernet two is a trunk interface.

38
00:03:46,800 --> 00:03:54,050
‫It uses dot one queue for trunking and the both villans are allowed here on the trunk.

39
00:03:54,360 --> 00:04:01,770
‫So we are going in fact to push these comments to the artist to switch.

40
00:04:02,370 --> 00:04:13,080
‫So I'll configure interface you to connect to is a trunk interface VLAN 10 and 20 are allowed then if

41
00:04:13,080 --> 00:04:20,560
‫they're going at three is an access port and it belongs to Vilan 20, if I can add four belongs to and

42
00:04:20,570 --> 00:04:23,640
‫then I am creating the violence.

43
00:04:23,640 --> 00:04:25,050
‫Valentin and Villon 20.

44
00:04:25,320 --> 00:04:30,570
‫If I want, I can move these lines here first.

45
00:04:30,570 --> 00:04:37,560
‫I am creating the violence and after that I am going to add the interfaces to the violence.

46
00:04:37,770 --> 00:04:43,110
‫And here I am creating the switched virtual interface for our villans.

47
00:04:43,290 --> 00:04:51,350
‫After that, I am starting the routing process by default on our routing is disabled.

48
00:04:52,410 --> 00:05:03,450
‫And after that, I am starting the SPF one routing protocol and I am adding these three networks to

49
00:05:03,510 --> 00:05:05,850
‫the SPF routing process.

50
00:05:07,130 --> 00:05:15,980
‫These are the connected networks, the network for Will and then the Network for Violent 20 and these

51
00:05:15,980 --> 00:05:17,400
‫network from here.

52
00:05:17,450 --> 00:05:24,500
‫OK, one hundred ninety to one hundred sixty eight, one hundred twenty two dot zero slash twenty four.

53
00:05:24,650 --> 00:05:31,150
‫At this moment, the aristos which isn't configured with violence and the routing protocol.

54
00:05:31,830 --> 00:05:35,790
‫Let's see, how can we run these comments on the switch.

55
00:05:36,590 --> 00:05:38,000
‫I have the script.

56
00:05:39,320 --> 00:05:42,710
‫And I am reading the file in a list.

57
00:05:43,160 --> 00:05:48,090
‫OK, I took these two lines of code here and I run the script.

58
00:05:48,110 --> 00:05:54,140
‫I want to show you what they do and they can see that a new list is created.

59
00:05:54,620 --> 00:05:56,570
‫The commands list.

60
00:05:56,840 --> 00:06:03,080
‫And the each element from my list is, in fact a line from the file.

61
00:06:03,200 --> 00:06:09,230
‫This is the first element of the list, the second element of the list, the third element of the list

62
00:06:09,230 --> 00:06:10,760
‫and so on.

63
00:06:11,150 --> 00:06:19,100
‫And after that, I'm calling the second config set function and I am printing the output now back on

64
00:06:19,100 --> 00:06:21,930
‫the machine that runs Jenness three.

65
00:06:22,370 --> 00:06:23,470
‫This is my script.

66
00:06:24,370 --> 00:06:25,760
‫Let me quickly start.

67
00:06:26,180 --> 00:06:29,420
‫This is the script I've just explained to you.

68
00:06:29,600 --> 00:06:33,170
‫It's the same script and I'm going to run the script.

69
00:06:33,890 --> 00:06:38,930
‫Of course, I have a SSX connection with our star one.

70
00:06:39,650 --> 00:06:43,040
‫Let's test it one more time, S.H..

71
00:06:43,370 --> 00:06:45,950
‫One hundred ninety to one hundred sixty eight.

72
00:06:46,340 --> 00:06:47,870
‫One hundred twenty two.

73
00:06:49,850 --> 00:06:51,770
‫Let's check it one more time.

74
00:06:54,160 --> 00:07:01,230
‫Then the username is admin with the password of Augusta.

75
00:07:02,590 --> 00:07:08,080
‫OK, and it's the enable password is also Augusta.

76
00:07:11,270 --> 00:07:21,140
‫Perfect, and now I'm going to run the Python script, Python Free, let me start by connecting to the

77
00:07:21,140 --> 00:07:21,830
‫device.

78
00:07:23,290 --> 00:07:26,950
‫And surrounding the comments, one after each other.

79
00:07:28,120 --> 00:07:36,160
‫And here we can see that the comments have been successfully executed and this is the output.

80
00:07:37,410 --> 00:07:44,460
‫OK, the USPI of routing protocol has been started, the volunteers have been created, the switch to

81
00:07:44,460 --> 00:07:49,170
‫virtual interfaces have been also created and so on.

82
00:07:49,980 --> 00:07:53,940
‫In fact, DOGIES documents are similar to Cisco iOS.

83
00:07:54,920 --> 00:08:02,990
‫Now, let's around show i.p or SBF Samari, for example, and we can see that the SPF routing protocol

84
00:08:02,990 --> 00:08:03,980
‫is running.

85
00:08:06,360 --> 00:08:16,710
‫In the next lecture, I'll show you how to configure the entire topology using Ned Mieko in an automated

86
00:08:16,710 --> 00:08:17,140
‫way.

87
00:08:17,730 --> 00:08:19,620
‫See you in a few seconds.


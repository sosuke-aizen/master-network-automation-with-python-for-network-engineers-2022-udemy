

 **EOS (Extensible Operating System)** is a Linux based Network Operating
System (NOS) developed by Arista Networks that runs on all Arista Switches.

Virtual EOS (vEOS) is a single image and can be run as a virtual machine (VM).

EOS run in a VM can be used to test almost all aspects of EOS and can be
downloaded fro free from Arista website.

  
 **Requirement:** You must be able to run vEOS in GNS3. See the specific
lecture where I explain in detail how to install and run vEOS in GNS3.

  
 **Notes:**

\- by default there is a single username named admin with no password

\- Arista CLI is similar to Cisco IOS

\- vEOS has 8 interfaces. You check them using show int status command

The Ma1 interface is for management and corresponds to the interface displayed
as Ethernet 0 (E0) in GNS3. All other vEOS Ethernet interfaces shown in CLI
match their particular GNS3 Ethernet interfaces name. For instance the vEOS
Ethernet 1 is the interface Ethernet 1 in vEOS GNS3.

-by default Arista uses Zero Touch for configuration. If you want to configure the device using the CLI and cancel it you must run zerotouch cancel and restart the system

\- SSH is the default protocol for establishing a terminal session to the
Arista switches. By default telnet is disabled.

  

 **Entering the enable mode, the global configuration mode and setting the
enable secret password and the hostname**

 _localhost login: admin_

 _localhost >enable_

 _localhost#conf t_

 _localhost(config)#enable secret arista_

 _localhost(config)#hostname Arista1_

  

 **Creating VLANs**

 _Arista1(config)#vlan 10_

 _Arista1(config-vlan-10)#exit_

 _Arista1(config)#vlan 20_

 _Arista1(config-vlan-20)#_

 _Arista1(config-vlan-20)#exit_

 _Arista1(config)#show vlan_

 _VLAN Name Status Ports_

 _\----- -------------------------------- ---------
-------------------------------_

 _1 default active Et1, Et2, Et3, Et4, Et5, Et6_

 _Et7_

 _10 VLAN0010 active_

 _20 VLAN0020 active_

  

 **Allocating ports to VLANs**

 _Arista1(config)#int e1_

 _Arista1(config-if-Et1)#switchport mode access_

_Arista1(config-if-Et1)#switchport access vlan 10_

 _Arista1(config-if-Et1)#no shut_

 _Arista1(config-if-Et1)#exit_

  

 **Creating a SVI (Switched Virtual Interface)**

 _Arista1(config)#int vlan 10_

 _Arista1(config-if-Vl10)#ip address 192.168.122.100/24_

 _Arista1(config-if-Vl10)#no shut_

 _Arista1(config-if-Vl10)#ping 192.168.122.1_

 _PING 192.168.122.1 (192.168.122.1) 72(100) bytes of data._

 _80 bytes from 192.168.122.1: icmp_seq=1 ttl=64 time=34.0 ms_

 _80 bytes from 192.168.122.1: icmp_seq=2 ttl=64 time=27.6 ms_

 _80 bytes from 192.168.122.1: icmp_seq=3 ttl=64 time=31.4 ms_

 _80 bytes from 192.168.122.1: icmp_seq=4 ttl=64 time=14.9 ms_

 _80 bytes from 192.168.122.1: icmp_seq=5 ttl=64 time=19.7 ms_

 _\--- 192.168.122.1 ping statistics ---_

 _5 packets transmitted, 5 received, 0% packet loss, time 89ms_

 _rtt min/avg/max/mdev = 14.929/25.570/34.085/7.198 ms, pipe 3, ipg/ewma
22.466/29.405 ms_

  
  
**Creating a default IPv4 Route**

 _Arista1(config)#ip route 0.0.0.0/0 192.168.122.1_

  

 **Creating a static route**

 _Arista1(config)#ip route 200.0.0.0/24 ethernet 1 192.168.122.1_

  

 **Starting the OSPF Routing Protocol**

 _Arista1(config)#ip routing_

 _Arista1(config)#router ospf ?_

_< 1-65535> Process ID_

  
_Arista1(config)#router ospf 1_

 _Arista1(config-router-ospf)#network 192.168.122.0/24 area 0_

**Testing OSPF**

 _Arista1(config)#sh ip ospf ?_

_A.B.C.D OSPF area-id in IP address format_

_access-list Named access-list_

_border-routers Border routers_

_database Database summary_

_interface Interface information_

_lsa-log LSA throttling Log_

_mpls Show MPLS information_

_neighbor Neighbor information_

_request-list Request list_

_retransmission-list Re-transmission list_

_spf-log Spf Log_

_summary OSPF Summary_

_vrf VRF name_

_< 0-4294967295> OSPF area-id in decimal format_

_< 1-65535> Process ID_

**Saving the configuration**

 _Arista1(config)#write memory_

_Copy completed successfully._

  
**Show commands are similar to Cisco IOS.**

  
Arista1(config)#sh ip interface brief

I _nterface IP Address Status Protocol MTU_

 _Management1 unassigned up up 1500_

 _Vlan10 192.168.122.100/24 up up 1500_

  

**Useful show commands**

 _show management ssh_

 _show run_

 _show vlan_

 _sh ip route_

 _sh interfaces status_


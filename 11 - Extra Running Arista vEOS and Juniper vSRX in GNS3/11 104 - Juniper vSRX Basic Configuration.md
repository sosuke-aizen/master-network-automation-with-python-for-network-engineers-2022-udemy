

 **Juniper vSRX** is a virtual security appliance that provides security and
networking services at the perimeter or edge in virtualized environments.
**vSRX** runs as a virtual machine (VM) on a standard x86 server. vSRX uses
one virtual CPU (vCPU) for the JCP and at least one vCPU for the PFE.

  

 **Requirement:** You must be able to run vSRX in GNS3. See the specific
lecture where I explain in detail how to install and run vSRX in GNS3.

  

 **Notes:**

  * the default username to log in is **root** with **no password**

  * the device configuration is saved using a commit model. When you edit the configuration, you work in a copy of the current configuration to create a candidate configuration. The changes you make to the candidate configuration are visible in the CLI immediately. To have a candidate configuration take effect, you commit the changes. At this point, the candidate file is checked for proper syntax, activated, and marked as the current, operational software configuration file

  

 _login: root_

 _\--- JUNOS 17.3R2.10 built 2018-02-08 02:19:07 UTC_

 _root@% cli_

  

 _root > show chassis fpc pic-status_ **→ it bust be online otherwise there is
a problem**

 _Slot 0 Online FPC_

 _PIC 0 Online VSRX DPDK GE_

  

 _root > show interfaces terse _

_Interface Admin Link Proto Local Remote_

 _ge-0/0/0 up up_

 _ge-0/0/0.0 up up_

  

 **Note:** If the above commands don’t show online/up there could be a GNS3
config problem. **Node Properties** under N **etwork Type** tab must be:
**VMWare paravirtualized Ethernet V3**

  

 **Entering the Global Configuration Mode and setting the password**

 _root > configure _

_Entering configuration mode_

 _root# set system root-authentication plain-text-password_

 _New password:_

 _Retype new password:_

  

 **Checking the password**

 _root@GNS3_vSRX17.3# show system root-authentication_

  

 **Changing the password**

 _root@GNS3_vSRX17.3# edit system root-authentication_

 _root@GNS3_vSRX17.3# set plain-text-password_

  

**Setting the hostname**

 _root#set system host-name GNS3_vSRX17.3_

  

 **Setting the interface ip address**

 _root# set interfaces ge-0/0/1 unit 0 family inet address 192.168.122.223/24_

 **Note:** to remove an ip address use **delete** instead of **set**

  

 **Disabling the interface (equivalent to cu cisco ios shut command)**

 _root@GNS3_vSRX17.3# set interfaces ge-0/0/0 disable_

  

 **Enabling the interface (equivalent to cisco ios no shut command)**

 _root@GNS3_vSRX17.3# delete interfaces ge-0/0/0 disable_

  

 **Checking the interface**

 _root@GNS3_vSRX17.3# run show interfaces ge-0/0/0 terse_

  

 **Starting the SSH Service**

 _root@GNS3_vSRX17.3# set system domain-name mydomain.com_

 _root# set system services ssh_

 _root# commit_

  

**Allow SSH root log in using a password**

 _root# edit system services ssh_

 _root# set root-login allow_

 _root#commit_

  

 **Setting security zones to allow incoming ping and SSH**

 _root@GNS3_vSRX17.3# set security zones security-zone trust host-inbound-
traffic system-services ping_

 _root@GNS3_vSRX17.3# set security zones security-zone trust host-inbound-
traffic system-services ssh_

 _root@GNS3_vSRX17.3# set security zones security-zone trust interfaces
ge-0/0/0_

 _root@GNS3_vSRX17.3#commit_

  

  


﻿1
00:00:01,250 --> 00:00:07,580
‫In the last lecture, we've seen how to create instance attributes using the class constructor or the

2
00:00:07,580 --> 00:00:12,890
‫unique method, another way to create instance attributes is using common methods.

3
00:00:13,340 --> 00:00:14,530
‫Lexcen example.

4
00:00:15,260 --> 00:00:19,700
‫I am going to create another method called said Energy.

5
00:00:21,180 --> 00:00:23,580
‫The first parameter is self.

6
00:00:24,240 --> 00:00:30,250
‫This is an instance to the object itself and the second parameter is energy.

7
00:00:30,930 --> 00:00:36,740
‫Now in the method body, I am creating the attribute self doubt.

8
00:00:36,780 --> 00:00:39,450
‫Energy equals energy.

9
00:00:40,610 --> 00:00:47,630
‫This new attribute called Energy, will exist only after the method is called.

10
00:00:49,280 --> 00:00:56,450
‫If I print the dictionary where all attributes are stored, we see that there is no attribute called

11
00:00:56,450 --> 00:00:57,080
‫energy.

12
00:01:02,910 --> 00:01:05,980
‫My object has only two attributes.

13
00:01:06,480 --> 00:01:08,100
‫Now let's call the method.

14
00:01:09,190 --> 00:01:13,600
‫Ah, one that said set energy of, let's say, one hundred.

15
00:01:15,870 --> 00:01:18,600
‫We can see how the new attribute appeared.

16
00:01:19,710 --> 00:01:28,110
‫We get the value of the instance attribute by writing the object name DOT and the attribute name.

17
00:01:33,240 --> 00:01:35,010
‫Run that energy.

18
00:01:41,030 --> 00:01:48,470
‫Another way to get the value of an attribute is to use the builtin, get a TTR method.

19
00:01:50,060 --> 00:02:00,890
‫I'll comment about this line end instead of R-1 dot energy, I write, get a TTR of the name of the

20
00:02:00,890 --> 00:02:04,540
‫object and the name of the attribute.

21
00:02:04,760 --> 00:02:05,810
‫So energy.

22
00:02:09,920 --> 00:02:17,250
‫And I've got the same result, if you try to get the value of an attribute that doesn't exist.

23
00:02:17,270 --> 00:02:25,300
‫An exception is raised Lexton example print are one dot producer.

24
00:02:26,150 --> 00:02:29,050
‫Of course, this attribute doesn't exist.

25
00:02:32,050 --> 00:02:40,480
‫And I've got an attribute error robot object has no attribute producer to avoid this error, you can

26
00:02:40,480 --> 00:02:47,470
‫call the getData method by in a default value is a third argument.

27
00:02:48,690 --> 00:02:58,890
‫So here I might get a TTR of our one, the name of the object, the second argument is the name of the

28
00:02:58,890 --> 00:03:07,980
‫attribute producer and the third argument is a default value that will be returned if the attribute

29
00:03:07,980 --> 00:03:09,030
‫doesn't exist.

30
00:03:09,310 --> 00:03:11,250
‫Robert, without producer.

31
00:03:13,660 --> 00:03:21,370
‫Now, when I read the script, I'll get no error and return to the starring role robot without producer.

32
00:03:21,730 --> 00:03:23,350
‫So this default value.

33
00:03:25,260 --> 00:03:32,540
‫All of these attributes we've seen so far are called instance attributes, and they are unique for each

34
00:03:32,550 --> 00:03:35,650
‫instance or object in Python.

35
00:03:35,820 --> 00:03:42,420
‫There is also another type of attribute called class attribute that is defined at the class level.

36
00:03:42,870 --> 00:03:47,430
‫Class attributes are attributes that are owned by the class itself.

37
00:03:47,790 --> 00:03:52,250
‫They will be shared by all the instances of the class.

38
00:03:52,770 --> 00:03:54,990
‫Therefore, they have the same value.

39
00:03:54,990 --> 00:04:01,100
‫For every instance, we define class attributes outside of all the methods.

40
00:04:01,380 --> 00:04:05,360
‫Usually they are placed at the top right below the class.

41
00:04:05,370 --> 00:04:13,170
‫Heather, if we want to access the attribute, we use the notation class name, the class attribute

42
00:04:13,170 --> 00:04:13,440
‫name.

43
00:04:14,250 --> 00:04:20,010
‫In my example, I want to keep track of how many robots I've created.

44
00:04:20,400 --> 00:04:24,600
‫For that, I'll create a class attribute called population.

45
00:04:24,840 --> 00:04:26,120
‫This is like a counter.

46
00:04:26,310 --> 00:04:33,090
‫So each time a new robot is created, the population class attribute will be incremented by one.

47
00:04:34,470 --> 00:04:42,330
‫Here outside any method I am creating the class attribute population equals zero.

48
00:04:43,740 --> 00:04:51,510
‫When creating an object, so a robot, the need, the method will be automatically called so inside

49
00:04:51,530 --> 00:04:52,700
‫the Vinita method.

50
00:04:52,950 --> 00:04:53,280
‫Right.

51
00:04:53,780 --> 00:04:56,040
‫Robot dot population.

52
00:04:57,320 --> 00:04:59,370
‫Plus equals one.

53
00:05:00,020 --> 00:05:03,770
‫Here I am incrementing the population by one.

54
00:05:06,030 --> 00:05:08,460
‫Now, let's create two more roebuck's.

55
00:05:09,580 --> 00:05:16,820
‫Hour two equals Robart of Calvin and the Belltrees two thousand thirty.

56
00:05:17,960 --> 00:05:27,020
‫Another robot ar3 equals a robot of Gehl and the Belltrees two thousand twenty.

57
00:05:27,490 --> 00:05:31,270
‫Now, if I want to see the robot population right.

58
00:05:31,910 --> 00:05:33,220
‫Robot population.

59
00:05:34,660 --> 00:05:38,320
‫Colma robot dot population.

60
00:05:39,210 --> 00:05:42,630
‫Here I am accessing the class attribute.

61
00:05:45,570 --> 00:05:54,060
‫And it pointed out the robot population three, this attribute is shared by all instances.

62
00:05:55,890 --> 00:06:02,960
‫We can also access the attribute using the notation object to name that class attribute name.

63
00:06:10,640 --> 00:06:12,380
‫And it's the same value.

64
00:06:13,430 --> 00:06:21,020
‫But if I try to modify the class attribute using the notation object to name that class attribute name,

65
00:06:21,350 --> 00:06:25,420
‫in fact I am creating a new instance attribute.

66
00:06:25,430 --> 00:06:28,040
‫I am not modifying the class attribute.

67
00:06:28,640 --> 00:06:35,010
‫An example are dot population plus equals, let's say 10.

68
00:06:35,690 --> 00:06:37,040
‫This is just an example.

69
00:06:38,760 --> 00:06:47,040
‫After running the script, we see how the class attribute has value three and the instance attribute

70
00:06:47,340 --> 00:06:50,670
‫has been modified and has the value 13.

71
00:06:51,420 --> 00:06:58,260
‫So here, in fact, I've created a copy of the class attribute is an instance attribute.


﻿1
00:00:01,200 --> 00:00:07,240
‫In this section will start diving into object oriented programming or OPIS.

2
00:00:07,910 --> 00:00:14,340
‫There are several different programming paradigms and up until now in this course, we've been using

3
00:00:14,340 --> 00:00:19,710
‫the procedural style of programming, which is a type of imperative programming.

4
00:00:20,570 --> 00:00:28,220
‫This style of programming involves providing a series of instructions to follow in predefined order.

5
00:00:28,550 --> 00:00:33,380
‫This is mainly achieved using variables, functions and flow control.

6
00:00:34,480 --> 00:00:42,460
‫Object oriented programming, or OPIS, is another programming paradigm and is based on classes, objects,

7
00:00:42,460 --> 00:00:48,010
‫attributes and methods, Python has been an object oriented language since.

8
00:00:48,010 --> 00:00:55,300
‫Its creation is in fact a multi paradigm programming language, meaning it supports different programming

9
00:00:55,300 --> 00:00:59,740
‫approaches, including the procedural and the GOP.

10
00:01:01,300 --> 00:01:08,230
‫Object oriented programming is an approach for modeling concrete, real world, things like cars or

11
00:01:08,230 --> 00:01:16,120
‫accounts, as well as relations between things like companies and employees or students and teachers,

12
00:01:16,390 --> 00:01:24,460
‫OPIS motels, real world entities as software objects, which have some data associated with them and

13
00:01:24,460 --> 00:01:26,200
‫can perform certain functions.

14
00:01:26,500 --> 00:01:31,450
‫It allows programmers to create their own real world data types.

15
00:01:32,650 --> 00:01:40,720
‫Let's see what all these new terms mean, a class is a kind of data pipe, just like a string integer,

16
00:01:40,720 --> 00:01:48,670
‫or at least when we create an object or for that data type, we call it an instance of a class.

17
00:01:49,240 --> 00:01:53,170
‫A class is a factory for creating objects.

18
00:01:54,620 --> 00:02:02,130
‫Without knowing it, we've already used Opie and the Vics because in Python, everything is an object.

19
00:02:02,430 --> 00:02:05,780
‫Everything is an instance of some class.

20
00:02:07,120 --> 00:02:14,740
‫If we create an integer, a string or a function, in fact, we create objects or instances of those

21
00:02:14,740 --> 00:02:17,820
‫classes, X equals five.

22
00:02:18,490 --> 00:02:25,690
‫Here I've created an instance or an object of type int if I create a list.

23
00:02:27,400 --> 00:02:29,860
‫I've also created an object.

24
00:02:32,710 --> 00:02:34,090
‫And each type is.

25
00:02:35,290 --> 00:02:37,000
‫This is the same for functions.

26
00:02:43,290 --> 00:02:52,700
‫A function is nothing more than an object of class function, the data values, which is stored inside

27
00:02:52,700 --> 00:03:00,200
‫an object are called attributes, and the functions which are associated with the object are called

28
00:03:00,200 --> 00:03:00,830
‫mattocks.

29
00:03:01,700 --> 00:03:08,990
‫We have already used the methods of some built in objects like strings and leaks here.

30
00:03:09,200 --> 00:03:10,880
‫If I write l don't.

31
00:03:11,360 --> 00:03:16,520
‫And in fact I am calling a method of a list object.

32
00:03:19,070 --> 00:03:22,340
‫All of these are methods or attributes.

33
00:03:24,740 --> 00:03:31,580
‫Now, returning to our own defined classes, for instance, we could have a class called person and

34
00:03:31,580 --> 00:03:38,870
‫an object, for instance, could represent a person with attributes like name, age, address and with

35
00:03:38,870 --> 00:03:44,250
‫behavior like walking, talking, running, which are all methoxy.

36
00:03:44,720 --> 00:03:52,760
‫I should also mention that in Python, OPIS doesn't allow us to do anything we couldn't do without OPIS.

37
00:03:53,090 --> 00:03:57,310
‫It doesn't unlock any functionality we didn't have before.

38
00:03:57,980 --> 00:04:04,340
‫Object oriented programming allows us to structure our code in a human way of thinking.

39
00:04:04,700 --> 00:04:10,580
‫With OPIS, we create code that is more usable, repeatable and organized.

40
00:04:12,590 --> 00:04:20,780
‫OK, this was an introduction to OPIS and we'll see in the next lecture how to define our own classes

41
00:04:20,780 --> 00:04:24,800
‫and create our own objects with attributes and the ethics.


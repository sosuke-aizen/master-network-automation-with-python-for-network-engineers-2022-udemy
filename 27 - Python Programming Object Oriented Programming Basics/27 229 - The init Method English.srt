﻿1
00:00:00,940 --> 00:00:08,560
‫Now, let's add functionality to our class, let's add some characteristics or attributes to our robot.

2
00:00:09,220 --> 00:00:14,230
‫There is a special method called unit, also called the class constructor.

3
00:00:14,740 --> 00:00:22,560
‫The word unit has to underscore on either side, and this is called a tanda method or a special method.

4
00:00:23,290 --> 00:00:31,240
‫Any time we create a new instance of a class, python will automatically look for this method and call

5
00:00:31,240 --> 00:00:31,480
‫it.

6
00:00:32,170 --> 00:00:34,390
‫We don't explicitly call it.

7
00:00:34,630 --> 00:00:37,150
‫It's called automatically anyway.

8
00:00:37,270 --> 00:00:42,010
‫It's not mandatory for a class to implement the Init method.

9
00:00:42,010 --> 00:00:43,000
‫It's optional.

10
00:00:44,260 --> 00:00:48,280
‫A few seconds ago, I said, method, what is a method?

11
00:00:48,580 --> 00:00:56,800
‫A method is a function defined inside the class, it will be called in conjunction with an object using

12
00:00:56,800 --> 00:01:00,040
‫the syntax object, the name DOT method.

13
00:01:01,520 --> 00:01:06,440
‫Here, for example, I've used the append, the method of a least object.

14
00:01:07,900 --> 00:01:10,990
‫I'll start defining the init method.

15
00:01:12,380 --> 00:01:16,730
‫D.F., dapple underscore, you need double underscore.

16
00:01:18,220 --> 00:01:25,130
‫Back to our elite method, the class constructor, the first parameter is the self keyword.

17
00:01:25,480 --> 00:01:32,680
‫In fact, if we use an ID like bicarb, it will automatically edit the self name.

18
00:01:32,860 --> 00:01:42,280
‫Basically conex this method to the instance of the class, the object that calls the method X, the

19
00:01:42,280 --> 00:01:44,940
‫instance of the object itself.

20
00:01:45,820 --> 00:01:52,300
‫Then we perceive any attributes or characteristics we want to add to our objects.

21
00:01:52,300 --> 00:01:58,720
‫In this example, a robot has a name and a builder, let's say simply here.

22
00:01:59,770 --> 00:02:08,700
‫Then in the function body I say self-taught name equals name, self-taught IR equals IR.

23
00:02:09,660 --> 00:02:15,060
‫This can be very confusing at first, but I'll try to clarify here.

24
00:02:15,340 --> 00:02:17,410
‫I've created two attributes.

25
00:02:17,770 --> 00:02:23,860
‫They are name and IR and they are on the left hand side of the equals.

26
00:02:23,860 --> 00:02:30,880
‫Sign the name and IR from the right hand side of the equals sign are the method parameters.

27
00:02:31,570 --> 00:02:36,880
‫So this is the object attribute and this is the method parameter.

28
00:02:38,410 --> 00:02:41,130
‫Now, if I run the script, I'll get an air.

29
00:02:43,570 --> 00:02:51,670
‫Missing two required positional arguments, name and dear, this is because when creating the object,

30
00:02:51,820 --> 00:02:54,430
‫I must specify X attributes.

31
00:02:55,810 --> 00:02:57,820
‫The first attribute is the name.

32
00:02:58,950 --> 00:03:00,360
‫Let's say Marvin.

33
00:03:01,520 --> 00:03:07,310
‫And the second attribute is the ear, like, say, two thousand forty.

34
00:03:08,270 --> 00:03:10,460
‫This robot is from the future.

35
00:03:13,610 --> 00:03:15,050
‫And it's working.

36
00:03:15,080 --> 00:03:16,190
‫There is no error.

37
00:03:16,940 --> 00:03:24,710
‫Now let's take another look at the easy method by convention, who used the same names for objects,

38
00:03:24,710 --> 00:03:27,710
‫attributes and the ethics arguments.

39
00:03:28,100 --> 00:03:30,350
‫But if I want, I can right here.

40
00:03:30,530 --> 00:03:35,330
‫And is the method argument and why is the second argument?

41
00:03:35,690 --> 00:03:38,690
‫Of course, I'll modify the following two lines.

42
00:03:39,170 --> 00:03:40,130
‫This is the same.

43
00:03:40,370 --> 00:03:44,030
‫Nothing changed and it will work like a charm.

44
00:03:47,020 --> 00:03:55,120
‫The attributes of the object are a name and ear and the method parameters are known and why?

45
00:03:56,970 --> 00:04:04,800
‫Anyway, as I said earlier, by convention, we use the same names for objects, attributes and methods,

46
00:04:04,800 --> 00:04:05,550
‫arguments.

47
00:04:09,620 --> 00:04:17,330
‫I just wanted to show you how are they connected and how method parameters are assigned to self-taught

48
00:04:17,330 --> 00:04:18,020
‫attribute.

49
00:04:18,320 --> 00:04:25,080
‫I can tell you that this was also, for me, confusing when I first started to learn OLP.

50
00:04:25,850 --> 00:04:33,140
‫Now, if we want to access the object attributes, we type in the name of the object dot in the name

51
00:04:33,140 --> 00:04:34,370
‫of the attribute.

52
00:04:36,670 --> 00:04:44,970
‫Robot name calling and here I write are on the name of the object dot and the name of the attribute,

53
00:04:45,570 --> 00:04:50,210
‫we can see how bicarb autocomplete, the attributes names.

54
00:04:50,970 --> 00:04:51,870
‫So name.

55
00:04:55,650 --> 00:05:05,370
‫And it displayed Robota name Marvin internally, attributes are saved in a dictionary where the key

56
00:05:05,400 --> 00:05:12,910
‫is the name of the attribute and the corresponding value, the value of that attribute to see this dictionary.

57
00:05:12,960 --> 00:05:16,980
‫We use the attribute to contradict of the object.

58
00:05:19,810 --> 00:05:20,620
‫Dictionary.

59
00:05:23,280 --> 00:05:25,560
‫That stores the attributes.

60
00:05:28,260 --> 00:05:33,000
‫I ran that double underscore dect.

61
00:05:37,550 --> 00:05:45,050
‫And this is the dictionary that stores the objects, attributes the name of the attribute is the key

62
00:05:45,080 --> 00:05:49,760
‫and its corresponding value, the value of that attribute.

63
00:05:52,050 --> 00:06:00,390
‫At the end of this lecture, I want to give you one more detail about the self Kiet other programming

64
00:06:00,390 --> 00:06:01,830
‫languages best.

65
00:06:01,860 --> 00:06:08,000
‫This is a hidden parameter to the method defined, for example, in C++ or Java.

66
00:06:08,190 --> 00:06:11,420
‫There is the this keyword here in Python.

67
00:06:11,550 --> 00:06:15,710
‫We must declare it explicitly and by convention.

68
00:06:15,720 --> 00:06:17,370
‫Its name is Self.

69
00:06:19,590 --> 00:06:27,810
‫It must be the first parameter of the unit method or any other method we define on a class instance,

70
00:06:28,440 --> 00:06:33,720
‫self isn't a real python language keyboard, it's just a convention.

71
00:06:33,870 --> 00:06:39,000
‫And if you want, you can use these or any other name instead of self.

72
00:06:40,470 --> 00:06:46,680
‫There is no problem if I use this as in C++ or Java instead of self.

73
00:06:48,250 --> 00:06:49,460
‫There is no air.

74
00:06:50,690 --> 00:07:02,420
‫We can also use any other name for self self being the object itself, if I write here, let's say here

75
00:07:03,410 --> 00:07:05,810
‫and here, they also write.

76
00:07:08,460 --> 00:07:13,410
‫Anyway, I don't recommend you to do so, just stick with the convention.


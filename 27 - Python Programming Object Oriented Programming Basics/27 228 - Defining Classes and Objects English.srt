﻿1
00:00:00,780 --> 00:00:08,130
‫In this lecture, we'll see how to define our own classes and create our own objects with attributes

2
00:00:08,130 --> 00:00:14,910
‫and the methods at the beginning, it could seem overwhelming because it's not just about learning some

3
00:00:14,910 --> 00:00:15,540
‫syntax.

4
00:00:15,810 --> 00:00:19,600
‫It's really about a new way of thinking and the writing code.

5
00:00:20,370 --> 00:00:27,510
‫Let's dive into the syntax of object oriented programming and don't worry about completely understanding

6
00:00:27,510 --> 00:00:29,330
‫everything I show you right now.

7
00:00:29,910 --> 00:00:34,000
‫Take it step by step and give it some time if you get stuck.

8
00:00:34,740 --> 00:00:39,780
‫This new approach of programming could take longer for you to completely understand.

9
00:00:40,470 --> 00:00:44,280
‫In order to define a class, we use the class keyword.

10
00:00:46,070 --> 00:00:51,170
‫Then type in the class name, I'll create a class called Robot.

11
00:00:52,820 --> 00:00:54,320
‫Now, take care of it.

12
00:00:54,350 --> 00:00:58,880
‫The convention is to use the Kimmel case notation for class names.

13
00:00:59,090 --> 00:01:03,360
‫This means each word in the name of the class will be capitalized.

14
00:01:04,190 --> 00:01:13,730
‫So if I have a class called my iRobot, the convention is to use my with an uppercase M and a robot

15
00:01:13,730 --> 00:01:15,230
‫with an uppercase.

16
00:01:15,230 --> 00:01:18,580
‫Ah, this is the Kamilah case convention.

17
00:01:19,160 --> 00:01:24,110
‫If there is a single word, we write it with an uppercase letter.

18
00:01:25,910 --> 00:01:33,380
‫Let's name our class simply, Robert, then, as for any other block of code we've seen so far, we'll

19
00:01:33,380 --> 00:01:34,700
‫type in a Colin.

20
00:01:36,570 --> 00:01:44,500
‫On the next roll, using one level of indentation will have the content of the class for the moment.

21
00:01:44,520 --> 00:01:48,020
‫I just want an empty class, so I'll write.

22
00:01:49,620 --> 00:01:52,920
‫This is an empty instruction, but it's mandatory.

23
00:01:52,920 --> 00:02:00,100
‫In order not to get a syntax error, a class like a function must contain at least one instruction.

24
00:02:00,900 --> 00:02:03,210
‫So this is my iRobot class.

25
00:02:04,050 --> 00:02:08,040
‫From class, we construct instances or objects.

26
00:02:08,490 --> 00:02:13,860
‫An instance is a specific object created from a particular class.

27
00:02:15,160 --> 00:02:23,890
‫We'll do that by typing the name of the object, the equals sign, and then the name of the class and

28
00:02:23,890 --> 00:02:25,420
‫a pair of parentheses.

29
00:02:27,390 --> 00:02:30,900
‫Let's say Iran equals herbut.

30
00:02:32,150 --> 00:02:42,290
‫We can see how bicarb automatically completes the name of the class, here are one is object of class

31
00:02:42,300 --> 00:02:42,950
‫robot.

32
00:02:43,930 --> 00:02:51,040
‫Is with functions on the first draftable class definition, we can type in a documentation's string

33
00:02:51,040 --> 00:02:56,880
‫or talk string in between people quotes to provide a hint about the class functionality.

34
00:02:57,220 --> 00:03:00,700
‫So let's enter some text in between triple quotes.

35
00:03:03,570 --> 00:03:06,090
‫This class implements Robert.

36
00:03:09,180 --> 00:03:17,250
‫To get the value of the dog's drink, we use the name of the object DOT and topple Underscore or Dundar

37
00:03:17,250 --> 00:03:17,700
‫Doc.

38
00:03:19,370 --> 00:03:25,380
‫Print the name of the object that double underscore doc.

39
00:03:28,510 --> 00:03:32,410
‫And we can see how it displayed the class doctoring.

40
00:03:33,690 --> 00:03:41,460
‫This is the basics of creating classes and objects, and in the next lecture, we'll see how to add

41
00:03:41,460 --> 00:03:45,690
‫characteristics or attributes to our robot class.


﻿1
00:00:01,440 --> 00:00:08,070
‫We'll continue our discussion about object oriented programming with Mexican menfolks, what part of

2
00:00:08,070 --> 00:00:15,140
‫Mexico methods they are, special methods that you can define to add magic or to enrich your classes.

3
00:00:15,540 --> 00:00:22,800
‫They are always surrounded by double standards, of course, and they are also called special or danda

4
00:00:22,800 --> 00:00:23,420
‫methods.

5
00:00:24,120 --> 00:00:29,430
‫They are special or magic because you don't have to invoke them directly.

6
00:00:30,030 --> 00:00:33,740
‫The invocation is realized behind the scene.

7
00:00:35,590 --> 00:00:43,570
‫There are a bunch of magic methods, and you can find them in the Python documentation at the S. special

8
00:00:43,570 --> 00:00:44,500
‫method, the names.

9
00:00:45,160 --> 00:00:48,010
‫This is also attached to this lecture.

10
00:00:48,430 --> 00:00:51,200
‫All these are magic methods.

11
00:00:52,000 --> 00:01:00,490
‫One of the biggest advantages of using Python magic methods is that they provide a simple way to make

12
00:01:00,490 --> 00:01:03,220
‫objects behave like builtin types.

13
00:01:04,060 --> 00:01:05,340
‫Let's take an example.

14
00:01:06,450 --> 00:01:11,310
‫I do four plus five and eight, nine.

15
00:01:12,730 --> 00:01:21,580
‫For between a single quotes plus five between single quotes, it returned forty five, of course, is

16
00:01:21,580 --> 00:01:25,400
‫a string because it's enclosed by single quotes.

17
00:01:25,870 --> 00:01:26,830
‫How did it work?

18
00:01:27,010 --> 00:01:30,110
‫How did Python know what to do here?

19
00:01:30,430 --> 00:01:40,450
‫The plus or the addition operator is a shorthand for a magic method called Dunderhead that gets automatically

20
00:01:40,450 --> 00:01:42,490
‫called on the first operand.

21
00:01:42,850 --> 00:01:51,460
‫So if the left open is an instance of the int class dunderhead does a mathematically addition.

22
00:01:51,850 --> 00:01:55,410
‫If it's a string, it does string concatenation.

23
00:01:56,110 --> 00:02:03,340
‫In fact, there is a magic method for each operator like addition, subtraction, multiplication, raising,

24
00:02:03,340 --> 00:02:05,200
‫topower and so on.

25
00:02:05,710 --> 00:02:13,900
‫We can define these Mexican methods in our classes to be able to print and subtract or multiply objects.

26
00:02:14,530 --> 00:02:22,150
‫I've just done a small modification to our robot class defined in a previous lecture here.

27
00:02:22,360 --> 00:02:26,530
‫I have to instance, attribute name and price.

28
00:02:27,740 --> 00:02:35,840
‫Now, if I try to add or compare to robotics, I get an error, it may be for robotics, it doesn't

29
00:02:35,840 --> 00:02:43,010
‫make sense to add objects of type robot, but for other classes like, say, a bank account, it makes

30
00:02:43,010 --> 00:02:50,000
‫sense to add to accounts in order to get the sum of their balances or to compare two accounts.

31
00:02:50,930 --> 00:02:54,970
‫To do this, you have to implement the corresponding Mexico ethics.

32
00:02:56,390 --> 00:03:01,010
‫First, let's try to add to robots are plus our two.

33
00:03:03,700 --> 00:03:13,360
‫And I've got the type air unsupported operand type for addition, in fact, it says that I didn't define

34
00:03:13,510 --> 00:03:16,240
‫a Mexican method for this operator.

35
00:03:18,180 --> 00:03:22,840
‫If I try to compare to Roberts, I'll get the same error.

36
00:03:23,640 --> 00:03:30,120
‫Now let's try to display information about a robot so preened one.

37
00:03:33,210 --> 00:03:35,850
‫And it printed out this message.

38
00:03:36,400 --> 00:03:39,390
‫This isn't a human readable message.

39
00:03:40,450 --> 00:03:44,040
‫Now let's go back to coding and see two examples.

40
00:03:44,430 --> 00:03:53,040
‫In the first example, I am going to overload the Thunder SETI method that is automatically called when

41
00:03:53,040 --> 00:04:01,560
‫trying to print an object and the dunderhead method that is automatically called when using the plus

42
00:04:01,590 --> 00:04:06,890
‫or the addition operator on objects inside the class.

43
00:04:07,260 --> 00:04:14,970
‫I'll define a new function called double underscore SETI Double Underscore.

44
00:04:15,240 --> 00:04:22,920
‫This is the dander or the magic method that is automatically called when printing an object.

45
00:04:23,910 --> 00:04:27,210
‫It's usually self is the first parameter.

46
00:04:28,290 --> 00:04:36,330
‫Now I'm going to create a temporary string variable, my underlying SDR equals my name is.

47
00:04:37,570 --> 00:04:50,350
‫Plus, self doubt the name plus and my Brice's plus estar of self self-taught Bryce, here I am converting

48
00:04:50,350 --> 00:04:57,880
‫an integer or a float to a string of X because the price will be an integer or a float.

49
00:04:58,240 --> 00:05:02,670
‫But I want to concatenate the price to this string.

50
00:05:02,830 --> 00:05:11,200
‫If I don't convert it to a string, it will return an error and I am returning my SDR variable.

51
00:05:14,290 --> 00:05:21,250
‫Now, when printing the object, it will automatically call this function like stright.

52
00:05:23,370 --> 00:05:31,560
‫And we can see the difference it printed out, my name is Marvin and my price is one hundred fifty.

53
00:05:33,010 --> 00:05:40,240
‫Now, let's define the second magic method, the f double underscore and double underscore.

54
00:05:42,030 --> 00:05:51,330
‫The first parameter is self and the second parameter is other self is the object that calls the method

55
00:05:51,510 --> 00:05:58,770
‫the operand from the left side of the equals sign and other the operand from the right side.

56
00:05:59,640 --> 00:06:04,470
‫What does it mean by adding to Roebuck's in this example?

57
00:06:04,620 --> 00:06:06,490
‫By adding to Roebuck's?

58
00:06:06,510 --> 00:06:09,870
‫I want to get the sum of their prices.

59
00:06:11,250 --> 00:06:14,840
‫So I create a variable price equals self-taught price.

60
00:06:15,190 --> 00:06:23,550
‫This is the operand from the left hand side of the equals sign, plus the operand from the right hand

61
00:06:23,550 --> 00:06:29,130
‫side of the equals sign other that price and I'll return the price.

62
00:06:30,300 --> 00:06:34,710
‫Now, when trying to add to Roebuck's, I won't get an error.

63
00:06:36,010 --> 00:06:42,520
‫It will automatically call this method and will return to some of their prices.

64
00:06:43,640 --> 00:06:47,000
‫So print R one plus R two.

65
00:06:49,000 --> 00:06:54,610
‫And it printed out 250 the sum of their prices.

66
00:06:56,840 --> 00:07:04,280
‫I've just implemented two magic methods, but there are a lot of magic methods, and if you want to

67
00:07:04,280 --> 00:07:11,350
‫implement that operation on your objects, you have to implement the corresponding magic method.

68
00:07:12,110 --> 00:07:15,080
‫That's all for the moment about magic methods.

69
00:07:15,440 --> 00:07:16,010
‫Thank you.


﻿1
00:00:00,330 --> 00:00:07,530
‫Let's go ahead and see how to configure a C score out there if the keys were generated on Linux and

2
00:00:07,530 --> 00:00:10,620
‫we are using Linux as the SSX client.

3
00:00:12,610 --> 00:00:22,510
‫Back on Linux, let's take a look at the public key cat home student, this is the user's home directory,

4
00:00:22,750 --> 00:00:29,160
‫that SNH, the directory that contains the keys and the public key.

5
00:00:29,400 --> 00:00:33,160
‫This is the private key and this is the public key.

6
00:00:35,450 --> 00:00:43,670
‫The public key is printed on a single line, and that's fine, but Cisco iOS only supports a maximum

7
00:00:43,820 --> 00:00:47,710
‫of 254 characters on a single line.

8
00:00:47,900 --> 00:00:51,200
‫So you want to be able to paste this in one comment.

9
00:00:51,650 --> 00:00:59,060
‫Fortunately, there is a useful Linux comment called fault that you can use to break the public key

10
00:00:59,240 --> 00:01:00,770
‫in multiple parts.

11
00:01:02,030 --> 00:01:05,570
‫Let's see the main page of the common man, Folt.

12
00:01:07,270 --> 00:01:12,010
‫So up each input line to fit in a specified width.

13
00:01:14,080 --> 00:01:20,610
‫So Folt, minus B, minus W, let's say 72.

14
00:01:20,950 --> 00:01:29,340
‫How many characters do I want to have on each line and the path to the public health student that is

15
00:01:29,350 --> 00:01:29,920
‫Sensage?

16
00:01:30,640 --> 00:01:34,150
‫And I'd underline our essay that Bob.

17
00:01:37,560 --> 00:01:45,090
‫OK, it broke the key on multiple lines, each line with 72 characters.

18
00:01:46,720 --> 00:01:56,200
‫I'll remove the SSX minus RSA part at the beginning and the comment at the end and I'll copy the key

19
00:01:56,200 --> 00:01:57,010
‫into a file.

20
00:01:59,710 --> 00:02:00,310
‫Kopi.

21
00:02:01,950 --> 00:02:03,870
‫And here based.

22
00:02:05,040 --> 00:02:06,830
‫OK, that's the public.

23
00:02:09,180 --> 00:02:19,710
‫Now back to the outer, I'll configure it to use this public key for authentication, so IPSC pop chain,

24
00:02:20,400 --> 00:02:29,730
‫the user name that will use the key is a Linux user, user name, Linux user key string.

25
00:02:30,720 --> 00:02:32,430
‫And I'll paste the key.

26
00:02:39,800 --> 00:02:51,650
‫Perfect exit, exit and the last exit in timesaving, the configuration, let's verify the public fingerprint.

27
00:02:51,680 --> 00:02:55,370
‫First, I want to be sure that their daughter has the correct key.

28
00:02:57,040 --> 00:03:01,180
‫Sure, running config pipe Biggin Popke.

29
00:03:05,370 --> 00:03:07,200
‫OK, this is the key fingerprint.

30
00:03:09,540 --> 00:03:18,990
‫And only I can see the fingerprint with the following comment, SSX kigen my nacelle minus F and the

31
00:03:18,990 --> 00:03:20,040
‫path to the key.

32
00:03:35,070 --> 00:03:37,570
‫OK, I've forgotten something important.

33
00:03:38,070 --> 00:03:46,080
‫The fingerprint displayed by this is Kraeutler, this from here is of type Amde five and by default,

34
00:03:46,080 --> 00:03:50,580
‫Linux uses a sharp 256 hash.

35
00:03:51,060 --> 00:03:58,440
‫So I must also add minus E an uppercase e m d five.

36
00:03:58,990 --> 00:04:04,010
‫I want to see a fingerprint using the empty five hash protocol.

37
00:04:04,020 --> 00:04:14,340
‫In fact, a key fingerprint is a hash and the option must be inserted before minus F, so minus E and

38
00:04:14,340 --> 00:04:14,910
‫the FIF.

39
00:04:15,000 --> 00:04:16,860
‫OK, that's the hash.

40
00:04:17,850 --> 00:04:22,290
‫It starts with 97 and ends in B6.

41
00:04:23,730 --> 00:04:27,860
‫And we know it's the same hash, so they're out there.

42
00:04:28,020 --> 00:04:30,240
‫That is the correct publicly loaded.

43
00:04:31,720 --> 00:04:35,450
‫Optional, but recommended for increased security.

44
00:04:35,470 --> 00:04:42,850
‫You can configure the router to disable SSX password authentication and you have to run these two cumming's.

45
00:04:43,900 --> 00:04:51,640
‫I want to run them on that out there, because this iOS doesn't support them, but maybe our viewers

46
00:04:51,640 --> 00:04:52,620
‫will support them.

47
00:04:52,900 --> 00:05:02,230
‫So no IPSC server authenticate user password and no IPSC server authenticate user keyboard.

48
00:05:03,490 --> 00:05:12,790
‫OK, that's all this is how you configure SSX public authentication on Cisco Iooss from both Windows

49
00:05:12,790 --> 00:05:13,810
‫and Linux.


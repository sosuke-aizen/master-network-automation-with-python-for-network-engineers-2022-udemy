﻿1
00:00:00,820 --> 00:00:01,660
‫Welcome back.

2
00:00:01,990 --> 00:00:10,450
‫Now it's time to see how to configure the demon on Linux for public key authentication, the first step

3
00:00:10,450 --> 00:00:17,380
‫is to copy the public from the machine where it was generated, Windows or Linux, to the Linux machine

4
00:00:17,380 --> 00:00:21,550
‫where the SFH server is running in a special file.

5
00:00:22,700 --> 00:00:28,370
‫In this example, this will be the Linux server to which we will authenticate.

6
00:00:29,180 --> 00:00:35,450
‫I'll show you first for the case when the client is running windows and the then for the case when the

7
00:00:35,450 --> 00:00:37,280
‫client is running Linux.

8
00:00:37,730 --> 00:00:46,110
‫So if you've generated the keypad on Windows using Pooty key generator, just open the private key input.

9
00:00:46,160 --> 00:00:53,570
‫Key generator I am opening, put the key generator and I am loading the private key.

10
00:00:56,510 --> 00:01:03,950
‫And you see here, the public, for a pasting into open SSX authorized Kei's file.

11
00:01:06,160 --> 00:01:12,370
‫Now, carefully, I am selecting the entire key and copy it to the clipboard.

12
00:01:13,790 --> 00:01:14,220
‫Copy.

13
00:01:15,080 --> 00:01:23,510
‫The next step is to move to the Linux server and paste the public contents in a file called Authorised

14
00:01:23,510 --> 00:01:30,320
‫Underline Keys, which is in that SSX directory in user home directory.

15
00:01:31,210 --> 00:01:37,990
‫Notice that in order to do this, you need access to the Linux machine with the user that will authenticate

16
00:01:37,990 --> 00:01:39,460
‫using the Quebecer.

17
00:01:41,010 --> 00:01:45,900
‫I am listing that SSA directory from user's home directory.

18
00:01:46,860 --> 00:01:50,270
‫This is the user's home directory, that is S.H..

19
00:01:50,550 --> 00:01:52,140
‫This is a hidden directory.

20
00:01:52,170 --> 00:01:55,980
‫It starts with a that there is no key inside.

21
00:01:56,520 --> 00:02:01,400
‫Also, notice that authorized underlined keys does not exist.

22
00:02:01,410 --> 00:02:07,410
‫So I will create the file using VM, which is my preferred editor.

23
00:02:07,600 --> 00:02:09,000
‫I'll create the file.

24
00:02:18,390 --> 00:02:23,340
‫And I'm pasting the clipboard contents, which is the public key into the file.

25
00:02:25,070 --> 00:02:29,900
‫And I'm saving the fight, OK, this, that's all, let's test it.

26
00:02:30,970 --> 00:02:41,500
‫Let's check if the demon is running on the server system, Sitel status, SSX, and it's running.

27
00:02:43,020 --> 00:02:51,180
‫We notice that the server is listening on Port 22 on each IP address of the server.

28
00:02:52,980 --> 00:02:54,040
‫Broken windows.

29
00:02:54,360 --> 00:02:58,950
‫I am opening Pooty and looking for connection.

30
00:03:00,220 --> 00:03:03,130
‫Essayistic and authentication.

31
00:03:04,390 --> 00:03:09,580
‫I'm clicking on the browse button and select the private key.

32
00:03:15,290 --> 00:03:20,300
‫Now, on the main screen, I'm entering the IP address of the Linux server.

33
00:03:20,660 --> 00:03:21,590
‫Let's check it.

34
00:03:23,740 --> 00:03:26,520
‫OK, this is the IP address of the Linux server.

35
00:03:30,960 --> 00:03:39,270
‫I'm giving this station a name and then save it so that I will not lose the settings like, say, Lennox

36
00:03:39,750 --> 00:03:45,660
‫to this is the house name Save and I am clicking on Open.

37
00:03:48,940 --> 00:03:50,530
‫They use their name student.

38
00:03:52,450 --> 00:03:57,640
‫And I mean, it has successfully authenticated the user.

39
00:03:58,910 --> 00:04:04,250
‫We see the message authenticating with public key and the key comment.

40
00:04:06,300 --> 00:04:13,500
‫We've just configured the Essex public authentication for a Windows client in the Linux server.

41
00:04:14,430 --> 00:04:22,260
‫Let's go ahead and see how to configure the Linux server to accept SSX public authentication if the

42
00:04:22,260 --> 00:04:25,850
‫keypad was generated on another Linux machine.

43
00:04:27,130 --> 00:04:34,330
‫So if the client runs on Linux, in fact, this is similar to the previous example.

44
00:04:34,360 --> 00:04:40,540
‫The only difference being that the keys were generated on Linux, not on Windows.

45
00:04:40,570 --> 00:04:46,450
‫OK, this is the client, the machine on which I've generated the keys.

46
00:04:47,230 --> 00:04:50,170
‫And this is the server, the same server.

47
00:04:54,710 --> 00:05:01,610
‫The first step is the same as in the case of Windows, and that is to copy the public from the machine

48
00:05:01,610 --> 00:05:07,010
‫on weeks it was generated to the Linux machine to which we want to connect.

49
00:05:07,290 --> 00:05:12,590
‫This is the machine where the keys were generated and this is the key.

50
00:05:16,080 --> 00:05:20,060
‫I underline RSA, that pub the public.

51
00:05:21,360 --> 00:05:26,430
‫I'll it on the other Linux machine using SICP.

52
00:05:27,470 --> 00:05:35,480
‫Of course, I can also copy it to the clipboard and then paste it in authorized keys, but this time

53
00:05:35,480 --> 00:05:44,450
‫I prefer to copy the public key file to the Linux server machine to use SICP on the Linux destination.

54
00:05:44,480 --> 00:05:52,460
‫There should be an openness to select a server running and accepting connections on Port 22 or another

55
00:05:52,460 --> 00:05:52,720
‫one.

56
00:05:53,000 --> 00:05:55,940
‫We know of extra from the previous example.

57
00:05:57,450 --> 00:06:09,810
‫So I am hoping the file SICP minus B and an upper case B ended up part of X default 22, but I use this

58
00:06:09,810 --> 00:06:17,100
‫option because maybe your server is listening on another port and now the path to the file.

59
00:06:17,100 --> 00:06:17,870
‫I will copy.

60
00:06:18,450 --> 00:06:21,840
‫So the user's home directory, home student.

61
00:06:22,620 --> 00:06:28,200
‫OK, that's the same as deal that SSA.

62
00:06:29,370 --> 00:06:32,430
‫I'd underline risc that Bob.

63
00:06:33,740 --> 00:06:42,770
‫Now, the user that will authenticate to the server student the IP address of the server.

64
00:06:48,580 --> 00:06:57,730
‫And the remote part where the file will be copied, Tilde, I'll copy the file in the student's home

65
00:06:57,730 --> 00:06:58,360
‫directory.

66
00:06:59,470 --> 00:07:08,020
‫OK, it's asking for the password, SSX public authentication is not yet configured, so I'm authenticating

67
00:07:08,020 --> 00:07:09,220
‫using the password.

68
00:07:10,440 --> 00:07:13,270
‫OK, and the file has been copied.

69
00:07:18,030 --> 00:07:22,990
‫This is the file I've just copied now on the Linux server.

70
00:07:23,160 --> 00:07:28,280
‫I led the public key at the end of the file called Authorized Keys.

71
00:07:29,160 --> 00:07:36,030
‫You can have more keys in authorized keys because there could be more users that authenticate to the

72
00:07:36,030 --> 00:07:44,760
‫server and each user will have its own key keeper take care not to overwrite the file, but append tweaks

73
00:07:44,760 --> 00:07:45,030
‫and.

74
00:07:46,610 --> 00:07:56,920
‫And I'm using the kept comment, kept the public file double greater than symbol and the path to authorized

75
00:07:56,930 --> 00:07:59,000
‫underline keys file.

76
00:08:03,460 --> 00:08:10,710
‫Doppelt, greater than symbol, rejects the output of cat comment to the file, upending the redirected

77
00:08:10,710 --> 00:08:12,200
‫output at the end.

78
00:08:13,110 --> 00:08:16,380
‫Let's see the contents of authorized keys.

79
00:08:21,700 --> 00:08:24,220
‫And we see their two public keys.

80
00:08:24,720 --> 00:08:32,140
‫This is the public that was generated on Windows, the first example, and this is the public view that

81
00:08:32,140 --> 00:08:40,180
‫was generated on Linux, the file I've just copied and appended to the end of this file.

82
00:08:40,960 --> 00:08:47,080
‫OK, this is all I've configured public authentication on this Linux server.

83
00:08:47,350 --> 00:08:52,840
‫Let's check if it works from the other Linux machine, which is the client.

84
00:08:52,840 --> 00:09:00,100
‫I'll try to connect to the Linux server I've just configured into the Linux server should not ask for

85
00:09:00,100 --> 00:09:05,410
‫the password, but authenticate the user using public key authentication.

86
00:09:05,860 --> 00:09:11,380
‫SSX, the user student at the IP address of the server.

87
00:09:14,500 --> 00:09:16,360
‫Perfect, I'm in.

88
00:09:17,470 --> 00:09:20,980
‫I have successfully authenticated on the server.

89
00:09:24,880 --> 00:09:34,600
‫If there is any problem, you can add minus the option to SSX comment, minus V V comes from verbose

90
00:09:34,720 --> 00:09:40,020
‫and it will print out a lot of messages for you to troubleshoot the problem.

91
00:09:42,470 --> 00:09:45,040
‫And you can look for any possible errors.

92
00:09:47,380 --> 00:09:47,800
‫OK.

93
00:09:48,130 --> 00:09:55,780
‫Also note that it's important to let the private key has their permission set only for the owner, if

94
00:09:55,780 --> 00:09:59,520
‫the permissions are to lose, it could not work.

95
00:10:01,370 --> 00:10:06,740
‫So change mode four hundred and the path to the private key.

96
00:10:12,260 --> 00:10:21,830
‫OK, one last thing he recommended to disable password authentication on the SSA server, to do that,

97
00:10:21,860 --> 00:10:22,730
‫you become a good.

98
00:10:25,380 --> 00:10:35,970
‫Open the SSX demon configuration file, which is in Etsy, SSX, SAGD underlined config.

99
00:10:37,960 --> 00:10:44,980
‫Don't forget this D from demand, there is also a file called SSX underlying conflict.

100
00:10:44,980 --> 00:10:51,430
‫So without the and here I'm searching for password authentication.

101
00:10:55,160 --> 00:11:01,460
‫OK, by default, yes, and I'll change it to no, no.

102
00:11:02,550 --> 00:11:05,820
‫I have a disabled password authentication.

103
00:11:07,960 --> 00:11:17,890
‫I'm saving the file and restart the service, the new configuration will become active only after restarting

104
00:11:17,890 --> 00:11:19,790
‫the server system.

105
00:11:19,990 --> 00:11:23,140
‫It'll restart S6.

106
00:11:25,030 --> 00:11:25,610
‫Perfect.

107
00:11:26,050 --> 00:11:27,520
‫Thank you, Voxel.


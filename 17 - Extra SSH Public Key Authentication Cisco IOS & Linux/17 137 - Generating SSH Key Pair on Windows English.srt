﻿1
00:00:01,640 --> 00:00:07,380
‫Let's move ahead and start configuring SSX public key authentication.

2
00:00:07,760 --> 00:00:09,490
‫First, let's see what we need.

3
00:00:09,860 --> 00:00:18,650
‫We need an client that supports RSA authentication and all the modem SSX clients are good for this.

4
00:00:19,290 --> 00:00:27,590
‫Then we need the private and public keep bear for each user will generate the least bear in a minute,

5
00:00:27,840 --> 00:00:30,460
‫both on Windows and Linux.

6
00:00:31,130 --> 00:00:39,230
‫And if we want to authenticate to networking devices like Cisco, we need an iOS that supports this

7
00:00:39,230 --> 00:00:39,800
‫feature.

8
00:00:39,950 --> 00:00:45,530
‫In fact, we need the NSA server or demand that supports public view authentication.

9
00:00:47,070 --> 00:00:55,280
‫This lecture is recommended if you are using Windows to authenticate to Cisco devices or to Linux servers,

10
00:00:55,560 --> 00:01:03,420
‫if you are using Linux as the 86 client, you can skip this part and move to generating SSA to keep

11
00:01:03,420 --> 00:01:04,830
‫bear on Linux.

12
00:01:06,440 --> 00:01:14,990
‫Put is the most common choice is assessed, each client on Windows Boutique self does not generate the

13
00:01:14,990 --> 00:01:20,630
‫keeper, but there is another utility called petechiae generator that can do this.

14
00:01:22,070 --> 00:01:27,530
‫Boutique regenerator is open source and free software, so lets download it.

15
00:01:29,460 --> 00:01:34,410
‫I'll simply search on Google for a download, put the key generator.

16
00:01:40,380 --> 00:01:43,140
‫And I'll open this website.

17
00:01:46,520 --> 00:01:54,350
‫The page contains the download the links for the latest released version of Beauty and Beauty Key Generator,

18
00:01:54,680 --> 00:01:59,480
‫and I'm searching for Jen Dopp sexy boutique.

19
00:01:59,480 --> 00:02:02,290
‫Jen, that's sexy, OK?

20
00:02:02,600 --> 00:02:07,550
‫And I'm downloading Pooty Jen for 60 beat operating systems.

21
00:02:12,150 --> 00:02:16,350
‫OK, once downloaded, will open the application.

22
00:02:18,470 --> 00:02:20,480
‫This is pretty key generator.

23
00:02:21,980 --> 00:02:31,070
‫The type of keys RSA and its length will be of two thousand forty eight, because this is the minimum

24
00:02:31,070 --> 00:02:38,570
‫recommended size for a key, if you need stronger security, you can choose a key with a length of four

25
00:02:38,570 --> 00:02:41,040
‫thousand ninety six weeks or more.

26
00:02:41,900 --> 00:02:46,610
‫This is the main screen of the application and I'll click on Generate.

27
00:02:48,610 --> 00:02:55,900
‫To generate our random key pooty key generator needs a lot of randomness and I have to continuously

28
00:02:55,900 --> 00:03:01,630
‫move the mouse over this area for it to be able to generate barkeeper.

29
00:03:02,670 --> 00:03:10,590
‫And the kids are ready, the next step is to make the comment for the kids and save them somewhere on

30
00:03:10,590 --> 00:03:11,170
‫the desk.

31
00:03:11,730 --> 00:03:19,500
‫Don't forget to add the comment because maybe you'll generate more kids and don't want to mix them up.

32
00:03:20,980 --> 00:03:26,380
‫For example, Windows RSA Key and the current debate.

33
00:03:27,770 --> 00:03:36,200
‫At the beginning of this section, I've said that the private key can be further secured by encrypting

34
00:03:36,200 --> 00:03:38,040
‫it with a pass for us.

35
00:03:38,570 --> 00:03:43,520
‫This is the moment when you can encrypt the private key with a passphrase.

36
00:03:44,580 --> 00:03:46,560
‫You're right, the path for us here.

37
00:03:47,690 --> 00:03:56,480
‫In this example, I won't add any for us because I want to keep things simple and don't want to enter

38
00:03:56,480 --> 00:04:00,320
‫the past for us each time I authenticate to the server.

39
00:04:02,190 --> 00:04:10,590
‫And I'm saving the phylis, the public, I'm saving it on my desktop, and I'll give it a name.

40
00:04:11,840 --> 00:04:15,920
‫Like I say, Windows underline, as they say that, Bob.

41
00:04:18,420 --> 00:04:20,670
‫And I'm saving the private key.

42
00:04:22,090 --> 00:04:24,700
‫OK, let's say windows.

43
00:04:26,030 --> 00:04:29,600
‫Privat, that p, p, k.

44
00:04:31,110 --> 00:04:36,570
‫OK, that's all I've generated the SSA to keep Bear on windows.

45
00:04:38,080 --> 00:04:46,670
‫Before we can use the keys to authenticate to Cisco, iOS or to Linux, we have to configure them first.

46
00:04:47,080 --> 00:04:54,700
‫I'll show you how to configure the SFH demon on both Cisco and the Linux in the next Lexus.

47
00:04:55,480 --> 00:04:56,770
‫See you in a minute.


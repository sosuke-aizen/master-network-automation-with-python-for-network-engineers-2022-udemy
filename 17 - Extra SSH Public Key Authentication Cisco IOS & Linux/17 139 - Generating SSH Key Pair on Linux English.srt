﻿1
00:00:01,540 --> 00:00:02,450
‫Welcome back.

2
00:00:02,950 --> 00:00:10,870
‫In the last election, we've seen how to generate an SNH public and private keep bear on windows using

3
00:00:10,870 --> 00:00:12,340
‫boutique generator.

4
00:00:12,950 --> 00:00:19,080
‫However, if you are not using Windows but Linux, it is of no help to you.

5
00:00:19,720 --> 00:00:22,530
‫You have to generate the keys on Linux.

6
00:00:23,020 --> 00:00:29,410
‫So if you are using Windows is your desktop operating system, you can skip this lecture.

7
00:00:30,420 --> 00:00:37,980
‫In this lecture, I'll show you how to generate the SSX keep bear used for public key authentication

8
00:00:38,130 --> 00:00:39,240
‫on Linux.

9
00:00:39,960 --> 00:00:49,020
‫Most modern Linux distributions come with SSX package that already contains besides the SSA command,

10
00:00:49,020 --> 00:00:59,790
‫which is the SSX client, another command called SSA Key that is used to generate the SSA repair used

11
00:00:59,790 --> 00:01:01,110
‫for authentication.

12
00:01:01,890 --> 00:01:11,930
‫So on this Linux machine called Linux one, I'm executing a search kitchen to generate the Keys SSA

13
00:01:12,120 --> 00:01:13,450
‫to minus KIGEN.

14
00:01:13,590 --> 00:01:20,610
‫This is the command and the options minus B two thousand forty eight.

15
00:01:21,790 --> 00:01:36,640
‫Minus T, RSA, an uppercase C, and a comment between double quotes, let's say Linux user and the

16
00:01:36,640 --> 00:01:42,050
‫date January 2000 20 before hitting enter.

17
00:01:42,250 --> 00:01:50,530
‫Let me explain to you the options minus B specifies the number of beats in the key to create.

18
00:01:50,860 --> 00:01:57,700
‫Generally two thousand forty eight beats is considered sufficient if you want better.

19
00:01:57,700 --> 00:02:08,800
‫Security is a larger key minus D specifies the type of key to create and we are using an RSA key pair.

20
00:02:09,310 --> 00:02:19,240
‫This is the most common option and minus C take care of an uppercase C letter specifies a comment.

21
00:02:19,570 --> 00:02:24,220
‫Feel free to write anything that uniquely identifies this key.

22
00:02:24,640 --> 00:02:26,920
‫And I'm hitting the enter key.

23
00:02:28,080 --> 00:02:31,800
‫I'm hitting the turkey again and one more time.

24
00:02:33,760 --> 00:02:43,180
‫Perfect, as you probably have noticed, the generated files are the underlying RSA pop for the public

25
00:02:43,180 --> 00:02:43,580
‫key.

26
00:02:44,330 --> 00:02:51,820
‫OK, this is the public key and I the underline RSA for the private key.

27
00:02:53,310 --> 00:03:02,370
‫And the files have been saved in a hidden directory called that SSX in user's home directory.

28
00:03:04,640 --> 00:03:11,960
‫Also, notice that I didn't enter any passwords, so the private key is not encrypted.

29
00:03:12,770 --> 00:03:13,750
‫But that's OK.

30
00:03:16,360 --> 00:03:25,030
‫Let's see the context of that SSX directory, the user's home directories, Slashdot, SSX.

31
00:03:25,750 --> 00:03:27,460
‫This is a hidden director.

32
00:03:27,880 --> 00:03:29,000
‫Keep this in mind.

33
00:03:29,770 --> 00:03:34,120
‫OK, these are the keys, the private key.

34
00:03:35,280 --> 00:03:43,770
‫And the publication of XOL, now that we have the keys, will move to the next step, which is to configure

35
00:03:43,770 --> 00:03:52,350
‫the SSA server or deman for public authentication, I'll show you how to configure it both on Cisco

36
00:03:52,350 --> 00:03:57,390
‫Io's and Linux in the next lecture's over this course.


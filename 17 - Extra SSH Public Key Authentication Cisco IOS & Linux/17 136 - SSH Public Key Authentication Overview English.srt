﻿1
00:00:01,980 --> 00:00:10,230
‫Hello, guys, and welcome in this section, I'll show you how to configure SSX public key authentication

2
00:00:10,410 --> 00:00:13,110
‫on Cisco, iOS and Linux.

3
00:00:13,620 --> 00:00:21,840
‫B.K., or public key authentication is an authentication method that uses a key player for authentication

4
00:00:21,960 --> 00:00:25,050
‫instead of a password, which is the default method.

5
00:00:25,560 --> 00:00:33,210
‫This type of authentication is considered the more secure because the default password method suffers

6
00:00:33,210 --> 00:00:40,380
‫from potential security vulnerabilities, like a brute force, login attempts or key loggers installed

7
00:00:40,380 --> 00:00:41,250
‫on the client.

8
00:00:42,120 --> 00:00:50,220
‫Another advantage of using public key authentication is that you are able to log in to servers from

9
00:00:50,220 --> 00:00:54,950
‫within scripts or automation tools that run unattended.

10
00:00:55,410 --> 00:01:03,390
‫For example, you can set up periodic updates for your servers using arsing, or you can use a configuration

11
00:01:03,390 --> 00:01:10,980
‫management tool like Ansible, and you run these applications automatically from a current job without

12
00:01:10,980 --> 00:01:14,760
‫any human intervention for entering the password.

13
00:01:16,130 --> 00:01:22,370
‫In piqué, there are two types of keest generated a public and the private key.

14
00:01:23,240 --> 00:01:31,160
‫In public authentication, a user creates a both a public and the private key and the then transfers

15
00:01:31,160 --> 00:01:37,020
‫a copy of the public key to the SSA server to the user wants to secure access.

16
00:01:37,640 --> 00:01:45,140
‫The private keys kept on the user's local machine and is used to verify the identity of the user when

17
00:01:45,140 --> 00:01:47,990
‫the user attempts to connect to the server.

18
00:01:48,740 --> 00:01:56,810
‫Anyone user or device that pays the public will be able to encrypt the data that can only be decrypted

19
00:01:56,810 --> 00:02:00,560
‫by the user or the device that has the private key.

20
00:02:01,040 --> 00:02:09,050
‫This means that you can share the public with anyone and they will have the ability to send encrypted

21
00:02:09,050 --> 00:02:14,820
‫messages, messages that will be decrypted only by using the private key.

22
00:02:15,650 --> 00:02:23,830
‫So in a nutshell, for SSA, the private keys stays on the client and the public keys stays on the server.

23
00:02:24,230 --> 00:02:30,900
‫The private keys used to encrypt the messages that will be decrypted by the server using the public.

24
00:02:31,490 --> 00:02:35,330
‫However, you need to protect the privacy of the private key.

25
00:02:35,600 --> 00:02:39,740
‫Anyone who has it can decrypt the encrypted messages.

26
00:02:40,190 --> 00:02:47,690
‫This can be accomplished by encrypting the private key with a passphrase which is similarly to a password.

27
00:02:48,200 --> 00:02:52,400
‫Before the client can read the private key.

28
00:02:52,430 --> 00:02:59,480
‫In order to perform the public authentication process, you'll be asked to supply the passphrase in

29
00:02:59,480 --> 00:03:04,750
‫order to decrypt the private key one more secure systems.

30
00:03:04,760 --> 00:03:13,760
‫You can simplify this process either by creating an unencrypted key, so a key with no for or by entering

31
00:03:13,760 --> 00:03:23,090
‫your passphrase once and then cashing the key in memory for later use openness, which contains a tool

32
00:03:23,090 --> 00:03:27,560
‫called essayistic agent, which simplifies this process.

33
00:03:28,760 --> 00:03:38,110
‫I can honestly say that it's common to have an unencrypted private key, especially in cases where age

34
00:03:38,150 --> 00:03:46,940
‫is used to automate different things in this section will generate a public and a private key both on

35
00:03:46,940 --> 00:03:55,340
‫Windows and the Linux will then configure both Cisco, iOS and the Linux for SSL authentication.

36
00:03:55,790 --> 00:04:03,890
‫So at the end of the section, you will have the knowledge to authenticate using PKK from both Windows

37
00:04:03,890 --> 00:04:08,390
‫and Linux to both Cisco OS and Linux.

38
00:04:08,820 --> 00:04:12,120
‫OK, that was just a short introduction to Piqué.

39
00:04:12,320 --> 00:04:20,180
‫Let's take a short break and in the next lecture's I'll show you some Hentzen examples of how to configure

40
00:04:20,330 --> 00:04:22,110
‫public key authentication.

41
00:04:22,400 --> 00:04:23,800
‫See you in a minute.


﻿1
00:00:00,900 --> 00:00:06,460
‫It's time to show you how to configure Cisco icons for public authentication.

2
00:00:06,780 --> 00:00:13,650
‫I've created this simple topology with only one out there and the cloud, which is in fact my Windows

3
00:00:13,650 --> 00:00:14,670
‫recording machine.

4
00:00:15,120 --> 00:00:22,320
‫There is also a layer of connectivity between the windows, which will be the S6 client and their out

5
00:00:22,320 --> 00:00:24,960
‫there, which will be the S.H. server.

6
00:00:25,530 --> 00:00:30,510
‫Let's test the layer of connectivity between Windows and their outer.

7
00:00:31,290 --> 00:00:32,760
‫I am pinging their outer.

8
00:00:35,980 --> 00:00:37,320
‫And it's working.

9
00:00:39,320 --> 00:00:48,280
‫Let's start with a basic SSX configuration will enable essayistic on the router at this moment, SSX

10
00:00:48,320 --> 00:00:50,570
‫is not enabled on the router.

11
00:00:51,630 --> 00:00:59,310
‫We can see that Port 22 is closed out there and it's not possible to connect to the router using Pooty

12
00:00:59,340 --> 00:01:00,420
‫and S.H..

13
00:01:02,030 --> 00:01:05,270
‫Let's see if Port 22 is open.

14
00:01:06,170 --> 00:01:12,350
‫And I'm using the telnet comment to let the IP address of the router and the Port 20 to.

15
00:01:16,260 --> 00:01:21,590
‫And we see that the connection failed, and that means that the port is closed.

16
00:01:22,910 --> 00:01:26,090
‫I'm opening a console connection to their outher.

17
00:01:30,230 --> 00:01:38,960
‫The first step is to configure a hostname, I am entering the global configuration mode and I am executing

18
00:01:38,960 --> 00:01:43,490
‫the command hostname and honey like say I want.

19
00:01:44,090 --> 00:01:51,650
‫Anyway, the host name was already configured, but configuring a hostname is mandatory for enabling

20
00:01:51,650 --> 00:01:52,990
‫SSX good.

21
00:01:53,450 --> 00:01:56,780
‫The second step is to configure a domain name.

22
00:01:57,840 --> 00:02:08,910
‫IP domain name and any domain domain dot com next will enable the SSX demon, which also means generating

23
00:02:08,910 --> 00:02:21,030
‫an RSA key pair on their outer crypto key, generate RSA, choose a key with a length of at least two

24
00:02:21,030 --> 00:02:22,920
‫thousand forty eight beeks.

25
00:02:24,200 --> 00:02:29,730
‫If you go with the default, which is 512, it won't work.

26
00:02:30,050 --> 00:02:35,690
‫The client will not authenticate to this server with such a short key.

27
00:02:36,440 --> 00:02:40,610
‫So the key will have a length of 2048 because.

28
00:02:46,080 --> 00:02:47,640
‫SSX enabled.

29
00:02:49,510 --> 00:02:59,160
‫And I enable a stage version to IPSC stage version to perfect now porked 22 is open on the out.

30
00:02:59,780 --> 00:03:03,190
‫Let's check it again using the telnet comment.

31
00:03:04,030 --> 00:03:07,510
‫And we noticed that it it's connected to the outer.

32
00:03:10,050 --> 00:03:19,410
‫The next step is to configure the white lines to accept SSX and the local authentication, local authentication

33
00:03:19,410 --> 00:03:23,550
‫means that the users are defined locally on the device.

34
00:03:24,760 --> 00:03:28,150
‫It's not training or a yes or a tax server.

35
00:03:29,670 --> 00:03:43,090
‫So line with why zero and for transport input SFH, I can also allow tallit or not, if I add telnet

36
00:03:43,710 --> 00:03:52,050
‫tell, it will also be permitted on the device and if I don't add telnet, I can connect only using

37
00:03:52,050 --> 00:03:52,770
‫S.H..

38
00:03:53,340 --> 00:03:59,280
‫Let's allow both S.H. and tell and log in local.

39
00:04:00,360 --> 00:04:08,600
‫OK, all I've enabled SSX on the router and the default authentication method is using the password.

40
00:04:09,000 --> 00:04:13,230
‫Let's try to connect to the router using SSL and Pooty.

41
00:04:14,750 --> 00:04:23,210
‫I am opening Puti the IP address of the router then that one, that one that, then the default port

42
00:04:23,210 --> 00:04:24,110
‫is 22.

43
00:04:24,110 --> 00:04:25,820
‫SSX and open.

44
00:04:27,480 --> 00:04:36,570
‫And it's working, there is already a user configured on the router yuan and the password is Cisco.

45
00:04:40,040 --> 00:04:42,530
‫Perfect example, just fine.

46
00:04:44,840 --> 00:04:50,960
‫The next step is to enable public authentication on they're out there and the Vietnamese to import the

47
00:04:50,960 --> 00:04:57,230
‫public key from our Windows or Linux operating system, depending on what we're using.

48
00:04:58,390 --> 00:05:06,550
‫If we are using windows and we've generated the keys on windows, we'll open the public key file in

49
00:05:06,550 --> 00:05:07,610
‫a text editor.

50
00:05:08,140 --> 00:05:13,150
‫Let's open the public key we've just generated in the last lecture.

51
00:05:14,250 --> 00:05:22,530
‫This is the public I generated the key with Pooty Key generator, and I am opening the file in Notepad

52
00:05:22,530 --> 00:05:31,320
‫Plus plus this is the public and I will copy to the clipboard only the public without the begin comment

53
00:05:31,470 --> 00:05:32,880
‫and end parts.

54
00:05:34,520 --> 00:05:41,570
‫So only the public this is the public key that I will copy to the clipboard copy.

55
00:05:43,960 --> 00:05:53,920
‫And I will paste it into the score out there, back to the score out there, I'm writing in global configuration

56
00:05:53,920 --> 00:05:57,690
‫mode, IP, SSX, Bob Keychain.

57
00:05:58,330 --> 00:06:06,940
‫This public will be used by a specific user in this example, let's say Windows user, so user name

58
00:06:07,360 --> 00:06:13,060
‫Windows user, then keys string and enter.

59
00:06:13,750 --> 00:06:21,560
‫Once I entered the Keys string comment, I can keep adding lines until I type the exit command.

60
00:06:21,640 --> 00:06:28,960
‫In this case, I'll simply paste the contents of the clipboard, which is the publicly and based.

61
00:06:31,330 --> 00:06:38,440
‫Perfect, I'm typing exit exit again and the last exit comment.

62
00:06:39,900 --> 00:06:44,730
‫And I am saving the configuration by executing two and right.

63
00:06:46,550 --> 00:06:51,350
‫To that, it's possible to add the multiple public keys for a single user.

64
00:06:51,650 --> 00:06:55,220
‫This allows you to have multiple users access.

65
00:06:55,230 --> 00:06:59,570
‫They're out there with the same username, but with different key pairs.

66
00:07:00,410 --> 00:07:02,520
‫OK, the configuration is done.

67
00:07:02,690 --> 00:07:04,400
‫Let's check if it works.

68
00:07:06,100 --> 00:07:12,040
‫I'll try to connect from Windows using beauty to the sea squirt out there and see if it authenticates

69
00:07:12,040 --> 00:07:14,020
‫the client using the keys.

70
00:07:14,980 --> 00:07:18,880
‫Let's verify first that the writer has the correct public key.

71
00:07:19,060 --> 00:07:24,090
‫We can print out the key fingerprint or using the following comment.

72
00:07:24,400 --> 00:07:33,040
‫So from enablement either on show running config pipe a vertical bar, begin Popke.

73
00:07:35,340 --> 00:07:42,630
‫OK, this is the key fingerprint, and I'll verify if it's the same inputting key generator.

74
00:07:45,930 --> 00:07:52,050
‫I am opening petechiae generator and I am loading the private key.

75
00:07:52,560 --> 00:07:57,000
‫This is the private key and this is the corresponding public.

76
00:07:57,480 --> 00:07:59,350
‫Let's check the fingerprints.

77
00:08:00,390 --> 00:08:06,340
‫This is the key fingerprint extending in six C f.

78
00:08:07,360 --> 00:08:08,530
‫And they're out there.

79
00:08:09,810 --> 00:08:18,190
‫Six, if it's the same, by the way, this is a hexadecimal string on the router, we have the correct

80
00:08:18,190 --> 00:08:18,490
‫key.

81
00:08:20,050 --> 00:08:23,770
‫If the key fingerprints or Hesh matches, we can continue.

82
00:08:25,090 --> 00:08:35,310
‫I'm closing pooty key generator, so on Windows, I am opening Pooty and looking for connection, SFH

83
00:08:36,190 --> 00:08:37,720
‫and authentication.

84
00:08:39,050 --> 00:08:46,280
‫Here I am clicking on the browse button and select the private key used for authentication.

85
00:08:47,330 --> 00:08:55,370
‫Private key file for authentication, browse desktop and the private keys and open.

86
00:08:56,490 --> 00:09:02,520
‫Now, on the main screen, I can save the session so that I will not lose the settings.

87
00:09:03,430 --> 00:09:10,900
‫All right, here, the IP address, if they're out there, then that one, that one, but then the default

88
00:09:10,900 --> 00:09:13,540
‫part is 22 SSX.

89
00:09:13,870 --> 00:09:21,280
‫And I'll give this station a name like, say, Aaron, Cisco and Safe.

90
00:09:22,170 --> 00:09:25,500
‫I've saved the session and open.

91
00:09:27,400 --> 00:09:34,510
‫It's asking for the user name Windows user, and I'm hitting enter.

92
00:09:36,120 --> 00:09:44,730
‫Great, we have successfully authenticated to the router using the SSA to keep bear with the message,

93
00:09:44,910 --> 00:09:52,860
‫authenticating with public key and the key comment, it didn't ask for a password anymore.

94
00:09:54,390 --> 00:10:02,400
‫OK, that's all in the second part of this lecture, I'll show you how to configure the escort out there

95
00:10:02,430 --> 00:10:09,220
‫for S.H. Public key authentication if the SSX client is a Linux machine.

96
00:10:10,020 --> 00:10:11,450
‫See you in a second.


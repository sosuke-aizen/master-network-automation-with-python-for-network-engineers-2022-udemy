

 **How to solve these coding challenges:**

  * Write your solution in PyCharm or in your preferred Python IDE.

  * If your solution is not correct, then try to understand the error messages, watch the video again, rewrite the solution and test it again. Repeat this step until you get the correct solution.

  *  **Save** the solution in a file for future reference or recap.

  

 **Challenge #1**

Create a Python script that reads a text file into a list and then converts
the list back into a string which is the entire file content.

[Click](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB) to
download a [sample text
file](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB).

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1OWngEcsi-sljqOkzgiOJ9DA0eB6pc8pc).

  

 **Challenge #2**

Create a Python function called **tail** that reads the last _n_ lines of a
text file. The function has two arguments: the file name and n (the number of
lines to read). This is similar to the Linux _`tail`_ command.

Example: tail('sample_file.txt', 5) will return the last 5 lines from
sample_file.txt.

[Click](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB) to
download a [sample text
file](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB).

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1HC4Pvn5eeatkkLqLf_ogmkJn8phI9Skz).

  

 **Challenge #3**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=1HC4Pvn5eeatkkLqLf_ogmkJn8phI9Skz)
so that the script that prints out the last _n_ lines of the file refreshes
the output every 3 seconds (as the file changes or updates). This is similar
to ` _tail -f`_ Linux command

[Click](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB) to
download a [sample text
file](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB).

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1DB9duUj6uYXN7LDb-vA6y__-zj9IXMl_).

  

 **Challenge #4**

Write a Python program to count the number of lines, words and characters in a
text file. This is similar to the Linux _`wc`_ command. Create a function if
possible.

[Click](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB) to
download a [sample text
file](https://drive.google.com/open?id=1BHRIztTDMbiUntvwmP5is8QObuWnHrxB).

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1VXCL0cN36pesUdFGRNgA4-wpQMgEUzzP).

  

 **Challenge #5**

Write a Python program that calculates the net amount of a bank account based
on the transactions saved in a text file.

The file format is as following:

 _D:50_

 _W:100_

 **D** means deposit while **W** means withdrawal.

Suppose that the [following
file](https://drive.google.com/open?id=11KIYrwtVbSkIqjNaNMY9QSvrK-T2iyhH) is
supplied to the program:

 _D:300_

 _D:300_

 _W:500_

 _D:200_

Then, the output should be: 300

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1-NxFLBlinAaTuFBy2wE0df-PVrwfURyR).

  

 **Challenge #6**

Write a Python script that compares two text files line by line and display
the lines that differ.

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1kQVI54XRsgY-KpOmdcVEDu0LV93C1mU5).

  

 **Challenge #7**

Consider [this dictionary
file.](https://drive.google.com/open?id=11gd07l9njsRAyXoLK_r7A2dtyQoiyfjC)

Write a Python script that reads the file in a dictionary. The words in the
file will be the dictionary keys and the length of each word the corresponding
values.

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1ZfYWGSqjycSNm7AcVk60TNa2ob7KR0pz).

  

 **Challenge #8**

Consider [the dictionary
file](https://drive.google.com/open?id=11gd07l9njsRAyXoLK_r7A2dtyQoiyfjC) from
the previous challenge.

Write a Python script that finds the first 100 longest words in the file.

Tip: [See](https://drive.google.com/open?id=1B6ecwzp3qhxdWrVxCI5WRlvpytZ4WfxL)
how to get a sorted view of a dictionary.

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=15w7rXsc_czzA3Hmstt8_UxkRqdh0TLqV).

  

 **Challenge #9**

Consider [this dictionary
file.](https://drive.google.com/open?id=11gd07l9njsRAyXoLK_r7A2dtyQoiyfjC)

Write a Python script that finds the number of occurrences of each letter of
the alphabet in all the words in the dictionary.

You want to see how many times do ‘a’, ‘b’, ‘c’ and so on appear in all the
words in the dictionary.

Which is the most frequently used letter in English words?

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1YCq1WQ0cTbv4oTrSJOtG29OH8CccMHor).

  

 **Challenge #10**

Continue the previous challenge and find the 3 most frequently used letters in
all English Words.

You should get: ('e', 67681), ('s', 50872), ('i', 50818)

 **Are you stuck?** Do you want to see the solution for this exercise? Click[
here](https://drive.google.com/open?id=1nrMvbc4rPt1xuUEgDIU7dF-vj7BpSBK2).


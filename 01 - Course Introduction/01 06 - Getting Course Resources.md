

 **All presentations and Python Scripts** used in this course are available as
a **GDrive Shared Directory**.

You can access the course resources here:
<https://drive.google.com/drive/folders/113HoK-
nhAwPN9AzSL8bL5JXSlUJr_PAS?usp=sharing>

The best way to learn to program is to write every piece of code by yourself
so I encourage you to do it and if you get stuck to check it with my examples.

Many scripts are ready to be used in your Network Environment with none or
very few changes.

I'll constantly update this shared directory with new scripts of other
updates.


﻿1
00:00:01,020 --> 00:00:08,130
‫Congratulations for enrolling in this course, welcome to Network Automation with Python for networking

2
00:00:08,130 --> 00:00:13,730
‫genius Python is one of the most used programming languages in the world.

3
00:00:14,310 --> 00:00:22,050
‫It's the de facto standard for network automation, but is also heavily used in other areas like machine

4
00:00:22,050 --> 00:00:24,780
‫learning, robotics or data science.

5
00:00:25,590 --> 00:00:30,300
‫These fields are exploding with progress into new opportunities.

6
00:00:31,600 --> 00:00:38,850
‫There's no need to tell you the Python programming network programmability and automation are the most

7
00:00:38,850 --> 00:00:43,000
‫valuable skills we have right now in the networking industry.

8
00:00:43,710 --> 00:00:49,730
‫By taking this course, you'll boost your career and increase your value in the job market.

9
00:00:50,640 --> 00:00:56,830
‫Network automation is clearly, without any doubt, the direction of today's industry.

10
00:00:57,660 --> 00:01:04,650
‫If you are looking for one of the hottest topics in the industry, then this course is definitely for

11
00:01:04,650 --> 00:01:04,890
‫you.

12
00:01:05,460 --> 00:01:12,690
‫If your desk is to configure, troubleshoot or monitor networking devices or servers, then you are

13
00:01:12,690 --> 00:01:17,160
‫in the right place using a Step-By-Step practical approach.

14
00:01:17,310 --> 00:01:23,580
‫I will teach you how to automate the network configuration and troubleshooting quests and how to be

15
00:01:23,580 --> 00:01:32,310
‫more efficient if your daily job, if you are a network engineer, network architect or sysadmin, understanding

16
00:01:32,310 --> 00:01:34,260
‫python is a critical skill.

17
00:01:34,260 --> 00:01:42,180
‫If you want to build a successful career by gaining these skills, he can ask for a raise or even look

18
00:01:42,180 --> 00:01:44,850
‫for a better job in the future.

19
00:01:44,870 --> 00:01:51,660
‫That comes, I believe the best network engineers will be able to build their own python Pythonesque

20
00:01:52,290 --> 00:02:01,830
‫or know how to understand and modify existing ones to query the APIs for their network devices, controllers

21
00:02:01,830 --> 00:02:09,840
‫and management systems using a programmable approach or to contribute Buki open source projects, most

22
00:02:09,840 --> 00:02:15,210
‫of them written in Python, is that they are becoming the foundation of network automation.


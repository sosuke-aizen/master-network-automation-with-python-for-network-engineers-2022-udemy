﻿1
00:00:00,850 --> 00:00:07,390
‫Hello and welcome to the Course Curriculum Overview, my mission is to teach you network automation

2
00:00:07,390 --> 00:00:13,840
‫with Python to the level where you can get hired, get a promotion, or you simply become more efficient

3
00:00:13,840 --> 00:00:15,020
‫at your daily job.

4
00:00:15,790 --> 00:00:22,480
‫Before we started, we'll have an insight on what this course will cover and how you can get the most

5
00:00:22,480 --> 00:00:23,290
‫out of it.

6
00:00:24,070 --> 00:00:27,060
‫This course has more than 200 EDV.

7
00:00:27,070 --> 00:00:33,010
‫Those challenges for you to solve with answers and tons of quizzes.

8
00:00:33,890 --> 00:00:41,960
‫I want you to be aware that all the videos are high definition quality, so if it happens to see them

9
00:00:41,960 --> 00:00:49,460
‫blurry, then it's not from the video itself, but from the streaming provider, server infrastructure

10
00:00:49,670 --> 00:00:51,200
‫or Internet connection.

11
00:00:51,620 --> 00:00:58,040
‫Try to select the quality of the video manually in the player or get in touch with me to see how to

12
00:00:58,040 --> 00:00:58,620
‫solve it.

13
00:00:58,970 --> 00:01:02,780
‫What is the best path to be successful in this course?

14
00:01:03,350 --> 00:01:10,550
‫Before diving into network automation, you need to have basic knowledge of general python programming.

15
00:01:11,060 --> 00:01:19,490
‫You need to be confident in using python syntax, string's links, dictionaries working with phylis

16
00:01:19,640 --> 00:01:21,710
‫flow control or functions.

17
00:01:22,870 --> 00:01:29,860
‫If this is not the case and you are here without having any prior experience with Python, there is

18
00:01:29,860 --> 00:01:31,090
‫no need to panic.

19
00:01:31,640 --> 00:01:32,950
‫At the end of the course.

20
00:01:33,160 --> 00:01:40,510
‫I've added lots of sections which are named Python programming, where I teach you basic python programming

21
00:01:40,510 --> 00:01:41,320
‫from scratch.

22
00:01:42,100 --> 00:01:50,440
‫Watch those videos, practice simultaneously with me and you'll learn all about python key concepts

23
00:01:50,830 --> 00:01:52,360
‫after you are done with it.

24
00:01:52,570 --> 00:01:59,650
‫Or if you already know some basic python programming, you can continue with the actual network automation

25
00:01:59,650 --> 00:02:00,050
‫part.

26
00:02:00,340 --> 00:02:07,360
‫So from the beginning of the course, let's get ahead and see how I structured the sections in this

27
00:02:07,360 --> 00:02:07,750
‫course.

28
00:02:08,570 --> 00:02:15,400
‫The first one is, of course, introduction, and it also contains the lecture you are watching right

29
00:02:15,400 --> 00:02:15,760
‫now.

30
00:02:16,210 --> 00:02:17,620
‫In this section.

31
00:02:17,830 --> 00:02:23,920
‫You you'll find out why is network automation with Python important and what is the direction of the

32
00:02:23,920 --> 00:02:27,930
‫network industry in the second decade of the new millennium?

33
00:02:28,630 --> 00:02:36,550
‫Here in the resource section, you'll find available for you to download all Python scripts that later

34
00:02:36,550 --> 00:02:39,220
‫on will be developed in this course.

35
00:02:39,970 --> 00:02:47,530
‫You'll also find out how to join the private and active community with thousands of members just like

36
00:02:47,530 --> 00:02:47,750
‫you.

37
00:02:48,290 --> 00:02:51,610
‫I'll also be there every day to answer your questions.

38
00:02:52,240 --> 00:02:56,980
‫Next, I'll show you step by step how to set up the lab environment.

39
00:02:57,940 --> 00:03:04,920
‫You learn how to install Python and the picture I.D. If you are a beginner, how to install Jeunesse

40
00:03:04,930 --> 00:03:12,730
‫Thuggy on Windows, Linux or Mac and how to run and connect to Cisco images inside your free Linux.

41
00:03:12,730 --> 00:03:16,750
‫PVM is also required for Linux automation tasks.

42
00:03:17,230 --> 00:03:24,730
‫At the end of this section you'll have a working development environment appropriate for this course.

43
00:03:25,180 --> 00:03:33,580
‫Of course you can skip this section if you have already installed Python and three, you have loaded

44
00:03:33,580 --> 00:03:38,530
‫Cisco, Iris, INGENIUS three and everything works well together.

45
00:03:39,750 --> 00:03:44,860
‫In the next section, I'll show you how to work with text files in Python.

46
00:03:45,420 --> 00:03:51,270
‫This is more like a general Python programming section that the network automation one, but I consider

47
00:03:51,270 --> 00:03:55,170
‫it is a prerequisite to become efficient in network automation.

48
00:03:55,620 --> 00:04:04,560
‫Most configurations of professional devices like routers, switches or Linux servers are saved as text

49
00:04:04,560 --> 00:04:05,000
‫files.

50
00:04:05,250 --> 00:04:12,150
‫So it's important for you to know how to read from or write to those files in order to work with configures

51
00:04:13,050 --> 00:04:14,410
‫in this section.

52
00:04:14,580 --> 00:04:20,730
‫I'll teach you all you need to know about working with text files, including the CEV module.

53
00:04:21,390 --> 00:04:25,550
‫At the end, you'll find some quizzes to test your understanding.

54
00:04:26,040 --> 00:04:30,510
‫Of course, if you already know that, you can skip that section entirely.

55
00:04:31,230 --> 00:04:36,460
‫Next will dive deep into JSON, arrest API and data serialization.

56
00:04:36,840 --> 00:04:44,610
‫This is an important topic when it comes to modern network infrastructures is they use APIs in some

57
00:04:44,610 --> 00:04:47,520
‫format to retrieve or push configurations.

58
00:04:48,540 --> 00:04:55,980
‫Next, we learn about network automation using SSX and how to connect to networking devices, and the

59
00:04:55,980 --> 00:05:02,820
‫Linux servers from Python will use the standard programming module to Ron Cummings on the Linux servers

60
00:05:02,820 --> 00:05:04,590
‫and on Cisco devices.

61
00:05:04,980 --> 00:05:11,190
‫I'll show you how to back up the configuration of multiple devices, how to configure multiple servers

62
00:05:11,190 --> 00:05:17,370
‫or even advanced concepts such as how to implement multi threading for higher efficiency.

63
00:05:18,360 --> 00:05:26,610
‫This section is really useful because we'll develop our own module name to my bar and our work will

64
00:05:26,610 --> 00:05:35,100
‫be much simplified at the end of this section, will create a script that uses SICP to copy files to

65
00:05:35,250 --> 00:05:39,420
‫or from Linux servers or other devices that supported.

66
00:05:40,890 --> 00:05:46,980
‫You will test your knowledge with pairs of hands on real world challenges with solutions.

67
00:05:47,340 --> 00:05:52,230
‫I find that is really important for me to deeply understand what you've just learned.

68
00:05:53,730 --> 00:06:01,660
‫The next section is about Namiko, which is one of the most useful Python library used in network automation.

69
00:06:02,190 --> 00:06:09,210
‫I'll show you many live examples on how to configure Cisco devices, Linux servers, how to pick up

70
00:06:09,210 --> 00:06:15,540
‫the configuration, how to send comings from a file, or how to use multithreaded for concurrent execution

71
00:06:15,690 --> 00:06:19,680
‫in order to overcome the speed limitation of Python.

72
00:06:20,480 --> 00:06:27,840
‫There's also a practical example where I show you how to configure a multi vendor topology that consists

73
00:06:27,840 --> 00:06:30,810
‫of Cisco and ARISTOS devices.

74
00:06:31,650 --> 00:06:37,410
‫This will end with tens of Hentzen coding exercises for you to solve.

75
00:06:38,680 --> 00:06:45,700
‫The next section is about napalm, which means the network automation and programmability abstraction

76
00:06:45,700 --> 00:06:53,290
‫layer with multi vendor support will see how to use a napalm in a multi-platform environment, how to

77
00:06:53,290 --> 00:06:58,420
‫retrieve information from devices, and how to manage configuration files.

78
00:06:59,180 --> 00:07:02,350
‫Then we'll move on to the Telnet library.

79
00:07:02,890 --> 00:07:09,250
‫You'll learn how to connect to a device using tell it how to run comings from Python scripts and how

80
00:07:09,250 --> 00:07:18,640
‫to automate simple tasks here will develop and test our own Python class that uses the telnet loop module

81
00:07:18,760 --> 00:07:20,980
‫to simplify device configuration.

82
00:07:21,460 --> 00:07:28,300
‫You'll also understand what is an object of type buycks, how it differs from a string, and how to

83
00:07:28,300 --> 00:07:35,530
‫decode from bytes to string and to encode from string to bikes using a well known encoding scheme.

84
00:07:36,370 --> 00:07:43,000
‫And as usual, you'll have tens of challenges to practice the skills you've just acquired.

85
00:07:44,400 --> 00:07:50,840
‫Afterwards, we have a section on automating the configuration, using CIGALE connections to the console

86
00:07:50,850 --> 00:07:57,780
‫port of networking devices, you learn how to connect to console port, how to run comings from Python

87
00:07:57,960 --> 00:08:05,640
‫and how to initially configure a device will develop our own module that uses the bicycle module in

88
00:08:05,640 --> 00:08:07,770
‫order to simplify the configuration.

89
00:08:09,020 --> 00:08:16,520
‫The following two sections offered is a kind of bonus are about Ansible, which is a mighty automation

90
00:08:16,520 --> 00:08:24,440
‫tool used by big companies like Cisco, Johnny Power, Twitter or even Naza, you'll see how to install

91
00:08:24,440 --> 00:08:32,300
‫Ansible, what are Ansible components and how to run Ansible ad hoc cumming's on Cisco devices or Linux

92
00:08:32,300 --> 00:08:32,960
‫servers.

93
00:08:33,350 --> 00:08:41,090
‫In the second part will talk about Yamal files and Ansible Playbook's will develop test and run playbooks

94
00:08:41,090 --> 00:08:45,530
‫that automate configurations of Cisco ahistoric Linux machines.

95
00:08:46,160 --> 00:08:52,420
‫At the end, you will have a good understanding of how to use Ansible playbooks in order to automate

96
00:08:52,460 --> 00:08:57,470
‫simple or complex tasks in a multi vendor networking environment.

97
00:08:58,820 --> 00:09:06,700
‫The final part of this course consists of more than six hours of in-depth general python programming,

98
00:09:07,160 --> 00:09:14,120
‫if you are a beginner in Python or if you want to recap some core python concepts, these sections are

99
00:09:14,120 --> 00:09:14,540
‫for you.

100
00:09:15,110 --> 00:09:21,830
‫Please start the course from this point, if you are a complete beginner in Python will take a deep

101
00:09:21,830 --> 00:09:29,870
‫dive into core python concepts like variables, operators, flow control leaks, populous six dictionary

102
00:09:29,960 --> 00:09:33,470
‫functions, errors and exceptions, and so on.

103
00:09:34,040 --> 00:09:38,660
‫You'll learn general python programming from scratch and that's not all.

104
00:09:38,990 --> 00:09:43,310
‫I'll add the new content regularly and I'll update the existing one.

105
00:09:44,270 --> 00:09:47,090
‫That's how the entire curriculum course overview.

106
00:09:47,240 --> 00:09:50,540
‫So let's go ahead and get started with the course.

107
00:09:51,140 --> 00:09:51,710
‫Thanks.

108
00:09:51,710 --> 00:09:53,690
‫And I'll see you in the next lecture.


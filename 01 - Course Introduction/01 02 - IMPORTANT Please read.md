

 **Congratulations** again on taking this course! I'm sure it will help you. I
don't need to tell you that **Python Programming and Network Automation and
Programmability are one of the most valuable technical skills to have right
now.**

  

 **About videos quality**

 **Udemy uses Dynamic Adaptive Streaming for video delivery.** That means that
it tries to deliver videos at the highest _usable_ quality for each specific
user.

**From the experience of many students sometimes it fails and you'll see
blurred videos.** If you know you have a decent Internet connection, the best
you can do is to try to set manually the resolution to 720p in Udemy web
player (by default it's set to Auto).

This is **NOT** a course problem (each lecture is FullHD or HD quality), but
an Internet slow connection or a Udemy technical issue. If the problem
persists, please get in touch with me to see how we can solve it.

  

 **Just a quick tip about reviewing this course.**

Sometimes, Udemy asks for review and rating for the course very early. I think
it's difficult to know about the course just by watching a couple of videos.
Isn't it?

 **Here's my request:** Please take time to go through the course and once you
get an idea, then please leave your honest review and rating.

And if you think there is something not clear **please get in touch with me.**
I would love to hear from you about what is missing so that I can improve that
area of the course and help you learn and grow in your career.

  

Let's keep moving!

  


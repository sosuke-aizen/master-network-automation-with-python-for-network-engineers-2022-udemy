

In order to get the most of this course **you must already have some Python
Programming knowledge**.

If you are here without having any prior experience with Python **there is no
reason to panic.** I've included in this course everything you need in order
to become a very successful Python Programmer. **In fact this course will also
teach you general Python Programming.**

At the end of the curriculum I've added more than **6 hours of in-depth
general Python Programming,** so you can learn everything from scratch. You’ll
learn about variables, operators, built-in types, strings, lists, tuples,
sets, dictionaries, flow control, functions and so on. ****

**Just go through each of the [Python Programming] sections in the order I
provided them in, so that you will benefit from a coherent and consistent
learning process.**

After you acquire basic Python Programming knowledge you can continue with
Network Automation with Python.

Of course, if you already have some prior Python experience, just skip the
[Python Programming] sections at the end of the curriculum and go on with the
next sections.

  

**And one last request:**

For basic Python syntax related questions, please try to do a simple **Google
search** for your particular issue, before asking questions in the course
discussion area. The recommended way to do this is to just type the name of
the language (Python in this case), followed by the problem you are dealing
with.

Please remember that the main objective of this course is to teach you
**Network Automation with Python** in order to improve your Python Network
Programming Skills and **NOT** to make you a Full Python Developer.

  

Now, let's start!


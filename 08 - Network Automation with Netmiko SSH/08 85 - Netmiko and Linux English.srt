﻿1
00:00:00,830 --> 00:00:07,760
‫Netmiko is a useful Python library used to automate configuration tasks in a multi vendor or platform

2
00:00:07,760 --> 00:00:09,440
‫network environment.

3
00:00:09,440 --> 00:00:16,510
‫In this video I want to show you how to execute a command on a Linux server from Python using Netmiko.

4
00:00:16,580 --> 00:00:27,990
‫I have a Linux VM  that’s running in a VirtualBox.  The network interface of the VM is in bridged mode so

5
00:00:27,990 --> 00:00:33,150
‫it has access to the same line as the windows recording machine that runs Python.

6
00:00:35,910 --> 00:00:38,260
‫Before returning to PyCharm,

7
00:00:38,280 --> 00:00:44,490
‫I want to check the connection between the Linux VM and the windows host where the Python script will

8
00:00:44,490 --> 00:00:46,430
‫be run.

9
00:00:46,500 --> 00:00:49,290
‫This is the Linux IP address and I'll ping it

10
00:00:55,250 --> 00:01:00,280
‫and it's working; and I'll also test SSH using Putty

11
00:01:08,420 --> 00:01:09,590
‫SSH is working.

12
00:01:09,890 --> 00:01:13,440
‫The user is u1 and the password pass

13
00:01:14,590 --> 00:01:17,120
‫123. Perfect!

14
00:01:19,850 --> 00:01:26,180
‫If something isn't working properly you have to troubleshoot the problem and fix it. First

15
00:01:26,240 --> 00:01:29,960
‫check that the correct IP is configured on Linux by running

16
00:01:29,960 --> 00:01:39,410
‫ifconfig and second check that the ssh daemon is running and listening on port 22.  You

17
00:01:39,410 --> 00:01:45,560
‫can check this by executing sudo systemctl status ssh.

18
00:01:51,940 --> 00:01:58,320
‫And it's active and running! And it's listening on port22.

19
00:01:58,330 --> 00:02:00,720
‫Now back to Python and Netmiko

20
00:02:01,060 --> 00:02:06,450
‫we connect to Linux machines the same way as to any other networking devices.

21
00:02:07,320 --> 00:02:15,500
‫So here instead of Cisco device I have Linux and Linux,

22
00:02:15,910 --> 00:02:21,780
‫the device type will be Linux and I'll change the IP address,

23
00:02:25,770 --> 00:02:29,530
‫the user name u1 in the password pass

24
00:02:29,550 --> 00:02:40,950
‫123. Don't use a password like this one, use a strong one with at least 12 or 14 random characters.

25
00:02:40,990 --> 00:02:46,110
‫This is just for our tasks to write it easier. And here

26
00:02:46,190 --> 00:02:54,000
‫the value of the secret key is in fact the sudo password, the password used to become root.

27
00:02:54,880 --> 00:02:56,290
‫So sudo password

28
00:02:59,060 --> 00:03:02,990
‫and it's pass 123

29
00:03:03,110 --> 00:03:08,930
‫Now let's suppose we have many servers and we want to install a package on each of them.

30
00:03:08,930 --> 00:03:17,460
‫In our example we'll install the Apache web server. In Linux all administrative tasks must be executed

31
00:03:17,530 --> 00:03:18,720
‫as root.

32
00:03:18,840 --> 00:03:25,410
‫If you want to configure a service, install a program, create a new user or change any configuration you

33
00:03:25,410 --> 00:03:31,380
‫must run the required commands only as rot. First let's how to do it in putty!

34
00:03:42,960 --> 00:03:45,080
‫To execute a command as root

35
00:03:45,240 --> 00:03:47,250
‫you have more possibilities.

36
00:03:47,430 --> 00:03:54,810
‫One of them is to write sudo before the command  name like say sudo cat/etc/

37
00:03:54,840 --> 00:03:55,290
‫Shadow

38
00:03:58,940 --> 00:04:00,600
‫and it's asking for

39
00:04:00,600 --> 00:04:03,190
‫the user's password.

40
00:04:03,240 --> 00:04:03,500
‫Okay.

41
00:04:03,510 --> 00:04:06,390
‫I've entered the wrong password, one more time!

42
00:04:09,170 --> 00:04:11,630
‫And the command was executed.

43
00:04:11,660 --> 00:04:12,810
‫as root.

44
00:04:13,250 --> 00:04:24,290
‫Or, another way is to temporarily become root in the terminal by running sudo su. I'm root! This is

45
00:04:24,290 --> 00:04:27,950
‫the method used by Netmiko. Now as root

46
00:04:28,040 --> 00:04:36,900
‫I can run any administrative command I want and the command required to install Apache is apt update

47
00:04:37,570 --> 00:04:46,300
‫&& apt install -y apache2. -y option will assume yes

48
00:04:46,410 --> 00:04:51,150
‫as answer to all prompts and run non interactively.

49
00:04:51,390 --> 00:04:59,340
‫This is necessary when scripting apt because there is no one to answer yes or no. I won't execute the

50
00:04:59,340 --> 00:05:06,480
‫command now because I'll do it from Python.

51
00:05:06,520 --> 00:05:10,230
‫I just want to check that Apache2 is not already installed.

52
00:05:11,540 --> 00:05:25,340
‫To see the status I am executing dpkg --get-selections | grep apache2. It

53
00:05:25,340 --> 00:05:31,580
‫will tell us if Apache is installed or not; and it didn't return anything

54
00:05:31,730 --> 00:05:34,340
‫so Apache is not installed.

55
00:05:34,430 --> 00:05:35,730
‫Now back to Python

56
00:05:35,930 --> 00:05:37,220
‫I'll do the same.

57
00:05:37,370 --> 00:05:46,100
‫First I am becoming root. I am calling the enable method of the connection object: connection.enable

58
00:05:47,700 --> 00:05:53,080
‫We become root the same way as we become admin on Cisco.

59
00:05:53,160 --> 00:06:04,050
‫This is equivalent to sudo su and I 'll send the commands output= connection.send command

60
00:06:04,050 --> 00:06:06,460
‫apt update

61
00:06:06,510 --> 00:06:11,850
‫&& apt install -y apache2

62
00:06:15,500 --> 00:06:17,430
‫okay and I'm running the script

63
00:06:23,420 --> 00:06:32,890
‫It's connecting to the Linux machine, downloading and installing Apache. In order for it to work the Linux

64
00:06:32,890 --> 00:06:35,740
‫machine needs an active Internet connection

65
00:06:38,900 --> 00:06:45,020
‫It has finished without error and back at the terminal I want to check that Apache 2 is there.

66
00:06:48,690 --> 00:06:52,530
‫I think I've closed putty. No problem, I'm connecting again.

67
00:07:03,130 --> 00:07:04,030
‫I'm becoming root

68
00:07:06,960 --> 00:07:15,010
‫and I am executing dpkg --get-selections | grep apache2

69
00:07:15,670 --> 00:07:19,190
‫It was installed; perfect!

70
00:07:19,300 --> 00:07:26,320
‫You've just seen how to automate the configuration of a Linux server. In this example we had only one Linux

71
00:07:26,320 --> 00:07:27,010
‫machine.

72
00:07:27,220 --> 00:07:34,630
‫But if we had the task to install a program or add the user on a hundred Linux servers all we have to do

73
00:07:34,810 --> 00:07:41,530
‫is create a list with all servers, iterate over the list using a for loop and execute the necessary

74
00:07:41,530 --> 00:07:48,160
‫commands on each server in the list, like we did in the previous lectures for Cisco devices.


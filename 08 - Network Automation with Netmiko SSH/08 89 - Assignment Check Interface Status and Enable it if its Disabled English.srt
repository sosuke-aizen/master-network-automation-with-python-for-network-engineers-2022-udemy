﻿1
00:00:01,890 --> 00:00:07,980
‫Now that you know how to use Netmiko to configure networking devices I wanted to give you an

2
00:00:07,980 --> 00:00:15,690
‫assignment. I wanted to create a Python script that sends some commands to a networking device only if

3
00:00:15,690 --> 00:00:18,010
‫some criteria are met.

4
00:00:18,240 --> 00:00:21,030
‫In this case I want the script to check if a router's

5
00:00:21,030 --> 00:00:28,860
‫interface is up or down and if that interface is down, the script should  send the no shut

6
00:00:28,860 --> 00:00:37,380
‫command in order to enable that interface. The name of the interface must be entered by the user at

7
00:00:37,390 --> 00:00:38,450
‫the console.

8
00:00:40,280 --> 00:00:44,270
‫In the next video I'll show you how would I do it,

9
00:00:44,300 --> 00:00:46,290
‫in case you get stuck.

10
00:00:46,370 --> 00:00:47,060
‫Good luck!


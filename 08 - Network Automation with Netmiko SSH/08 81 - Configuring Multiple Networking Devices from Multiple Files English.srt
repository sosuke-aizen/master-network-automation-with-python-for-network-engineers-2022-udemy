﻿1
00:00:01,160 --> 00:00:03,060
‫Hello and welcome back!

2
00:00:03,080 --> 00:00:09,170
‫We'll dive deeper into network automation using Python and Netmiko and I'll show you how to configure

3
00:00:09,170 --> 00:00:18,050
‫multiple networking devices from multiple config files. We'll use this topology with three routers. Before

4
00:00:18,050 --> 00:00:19,020
‫beginning

5
00:00:19,070 --> 00:00:25,810
‫I recommend you to test the network connection between the admin host and the devices that run in GNS3,

6
00:00:25,820 --> 00:00:32,810
‫test the Layer 3 connectivity using ping and SSH using putty.

7
00:00:32,980 --> 00:00:37,410
‫I won't do it as I've already tested and it's working just fine.

8
00:00:38,590 --> 00:00:43,850
‫If something isn't working properly you have to troubleshoot the problem and fix it.

9
00:00:48,620 --> 00:00:53,430
‫Check that the correct ip is configured on each router's interface, that

10
00:00:53,470 --> 00:01:01,610
‫the interface is up and so on.

11
00:01:01,620 --> 00:01:05,640
‫Now let's return to Python! To save time

12
00:01:05,640 --> 00:01:11,270
‫I've already created the script and I'll explain to you in great detail what it does.

13
00:01:13,160 --> 00:01:22,030
‫After importing the ConnectHandler class I am opening a text file and reading it in a list. The device

14
00:01:22,060 --> 00:01:24,220
‫.txt file contains

15
00:01:24,220 --> 00:01:29,870
‫in fact the IP of the routers we want to configure.

16
00:01:30,070 --> 00:01:31,490
‫This is the file.

17
00:01:31,780 --> 00:01:34,520
‫Each IP is on its own line.

18
00:01:36,930 --> 00:01:45,740
‫So devices is a list where each element is an IP address of a router; then I'm creating an empty list

19
00:01:45,800 --> 00:01:48,380
‫called device_list.

20
00:01:48,570 --> 00:01:53,450
‫It will contain a dictionary for each outer in our topology.

21
00:01:53,570 --> 00:02:00,140
‫I am iterating over the devices and for each IP I'm creating a dictionary that is appended to the device

22
00:02:00,140 --> 00:02:10,550
‫list. All the information about the device is identical, except the IP address,here. If you want to use

23
00:02:10,550 --> 00:02:13,940
‫different usernames or passwords for the routers,

24
00:02:14,030 --> 00:02:19,160
‫just write them in the file and use them when creating the dictionary.

25
00:02:19,280 --> 00:02:26,030
‫We'll see an example of that uses this approach later in the course. If you want to see the contents

26
00:02:26,210 --> 00:02:36,940
‫of the device list just print it out. And to make it clear I'll print the list print (device_list).

27
00:02:37,000 --> 00:02:42,080
‫I do not want to continue running the script so I'll exit here.

28
00:02:42,130 --> 00:02:43,870
‫I just want to see the list.

29
00:02:47,640 --> 00:02:48,240
‫Okay.

30
00:02:48,390 --> 00:02:51,080
‫This is a list, square brackets,

31
00:02:51,270 --> 00:02:52,670
‫this is the first element,

32
00:02:52,710 --> 00:02:54,200
‫so the first dictionary,

33
00:02:58,370 --> 00:03:05,750
‫the second element, which is the second dictionary for the second router in our topology and the last

34
00:03:05,750 --> 00:03:06,140
‫element.

35
00:03:15,460 --> 00:03:21,360
‫Then I am iterating over the device list, connecting to each device,

36
00:03:22,170 --> 00:03:30,090
‫entering the enable mode, prompting the user for a configuration file, this line here,

37
00:03:31,040 --> 00:03:39,440
‫the user will enter a configuration file for each device and finally executing the commands in the configuration

38
00:03:39,440 --> 00:03:46,250
‫file on each device. I'm using the send_config_from_file method

39
00:03:46,350 --> 00:03:55,640
‫we have already seen in the last lecture. And at the end I'm disconnecting! Notice that

40
00:03:55,640 --> 00:04:04,310
‫I've used f string literals to concatenate strings. This is the Pythonic way when working with strings.

41
00:04:06,270 --> 00:04:13,200
‫I am also adding after each iteration a line of 30 hashes just to see it better.

42
00:04:13,450 --> 00:04:20,910
‫You'll see in a minute what's all about.

43
00:04:21,050 --> 00:04:25,800
‫I've already created the file with commands for each router.

44
00:04:25,850 --> 00:04:31,840
‫I'm creating a loopback interface on each router and enabling OSPF.

45
00:04:31,850 --> 00:04:34,040
‫Of course you can write any other command.

46
00:04:37,650 --> 00:04:40,790
‫Before running the script let's check the routers.

47
00:04:40,860 --> 00:04:49,910
‫I want to see if the interfaces and that OSPF is not already enabled so show ip interface brief

48
00:04:52,740 --> 00:05:03,590
‫and there is no loopback interface and show ip protocols. OSPF is not running; do these checks for each

49
00:05:03,710 --> 00:05:07,240
‫router. And I am running the script,

50
00:05:10,430 --> 00:05:19,230
‫connecting to the first router, entering the enable mode and it's prompting for a configuration file.

51
00:05:20,250 --> 00:05:28,770
‫So for the first router the file is called CiscoRouter1.txt and I'm writing Cisco

52
00:05:28,770 --> 00:05:30,250
‫Router1.txt

53
00:05:33,140 --> 00:05:35,880
‫and it's sending the commands.

54
00:05:35,930 --> 00:05:44,150
‫These are the commands from the file that were sent to the  router and now the same for the second router,

55
00:05:44,610 --> 00:05:56,890
‫the file is CiscoRouter2.txt, it's sending the commands  and the lastly the configuration file

56
00:05:57,070 --> 00:06:02,030
‫for the third, the last router CiscoRouter

57
00:06:02,040 --> 00:06:03,380
‫3.txt

58
00:06:07,960 --> 00:06:12,670
‫Perfect, let's check the routers! Okay!

59
00:06:12,790 --> 00:06:16,650
‫We see how the OSPF protocol was enabled

60
00:06:16,750 --> 00:06:25,360
‫show ip protocols OSPF was enabled and show ip interface brief; look,

61
00:06:25,450 --> 00:06:27,160
‫this is the loopback interface!

62
00:06:27,370 --> 00:06:34,620
‫And of course the OSPF routing protocol and the loopback back interface were configured on the other

63
00:06:34,630 --> 00:06:39,440
‫routers as well.

64
00:06:39,650 --> 00:06:46,230
‫See how our router has already two neighbors, here in the neighbor table.

65
00:06:46,630 --> 00:06:49,200
‫It's the other two routers of the topology.

66
00:06:52,710 --> 00:06:59,160
‫The script is working just fine but we can notice that it's a little bit slow.

67
00:06:59,190 --> 00:07:04,090
‫We have only three routers and have executed only a few commands.

68
00:07:04,170 --> 00:07:08,630
‫Just imagine what would happen and how long would it take

69
00:07:08,850 --> 00:07:17,450
‫if we had 100 routers and executed 50 commands on each router. It would take a very long amount of

70
00:07:17,450 --> 00:07:18,650
‫time.

71
00:07:18,650 --> 00:07:25,850
‫What we can do is improve the script with multi threading ! Iinstead of configuring the  routers one after

72
00:07:25,850 --> 00:07:32,420
‫another, which means sequentially, we could start a thread for each router in the topology and configure

73
00:07:32,420 --> 00:07:34,080
‫them concurrently.

74
00:07:34,400 --> 00:07:38,150
‫We'll take a deep look at how to do that later in the course.


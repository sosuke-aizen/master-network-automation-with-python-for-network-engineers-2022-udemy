﻿1
00:00:01,550 --> 00:00:03,620
‫Hello guys and welcome back!

2
00:00:03,620 --> 00:00:09,330
‫We'll start a series of lectures on  Netmiko and multithreading.
‫Until now in the course we’ve seen

3
00:00:09,350 --> 00:00:11,270
‫how simple and useful

4
00:00:11,270 --> 00:00:19,440
‫Netmiko is for Network Automation. With just a few lines of Python code we can automate the configuration

5
00:00:19,530 --> 00:00:23,460
‫of multiple networking devices and servers.

6
00:00:23,460 --> 00:00:31,780
‫It's not only time saving but very efficient to.  Let's take a look again at this script.

7
00:00:31,970 --> 00:00:39,890
‫We've developed it together in a previous lecture. It simply backups the configuration of all devices

8
00:00:40,040 --> 00:00:42,320
‫in this topology.

9
00:00:42,320 --> 00:00:50,980
‫It executes the show run command on each device and then saves the output to a file. The name of the

10
00:00:50,980 --> 00:00:56,290
‫file consists of the routers hostname and the current date.

11
00:00:56,290 --> 00:01:00,120
‫This is one example of a file, of a backup file.

12
00:01:00,280 --> 00:01:01,940
‫Watch that video again

13
00:01:02,080 --> 00:01:04,420
‫if you need a quick recap; let's run it.

14
00:01:09,000 --> 00:01:12,900
‫While the script is running we realize its drawback.

15
00:01:13,050 --> 00:01:15,030
‫It's a very very slow!

16
00:01:15,090 --> 00:01:24,510
‫In this example there are only three routers and we notice that it takes some time! We send command

17
00:01:24,510 --> 00:01:29,990
‫after command through the ssh connection and wait for the router to finish executing them.

18
00:01:30,820 --> 00:01:34,520
‫When we are done with the first router we go to the second.

19
00:01:34,660 --> 00:01:40,240
‫So the script works sequentially and that's and not good at all. By default

20
00:01:40,240 --> 00:01:45,430
‫a Python script runs as a single process with a single thread inside it.

21
00:01:46,580 --> 00:01:54,960
‫Let's measure how long it takes! At the beginning of the script, before connecting to the device,

22
00:01:55,010 --> 00:01:58,350
‫I'm getting the current timestamp.

23
00:01:58,380 --> 00:02:03,090
‫This is also known as the Unix epoch and it's a way to track

24
00:02:03,090 --> 00:02:05,940
‫time as a running total of seconds.

25
00:02:06,150 --> 00:02:11,820
‫The count starts on January 1st, 1970 at UTC.

26
00:02:12,060 --> 00:02:22,070
‫So import time and the start= time.time. Just to see how it looks like I'll execute the

27
00:02:22,070 --> 00:02:27,420
‫same Python statements in idle; so import time and time.time.

28
00:02:29,370 --> 00:02:38,420
‫This is the current timestamp as a total number of seconds. You can transform this timestamp into any

29
00:02:38,420 --> 00:02:43,600
‫date and time format and after the script disconnects

30
00:02:43,620 --> 00:02:48,640
‫I'm getting the timestamp of that moment so end= time.time.

31
00:02:50,620 --> 00:02:56,610
‫Now we can tell exactly how long the script took by subtracting start  from end.

32
00:02:58,020 --> 00:03:03,150
‫So we want to see how many seconds have passed between these two points in time.

33
00:03:04,830 --> 00:03:15,800
‫This, at line 4, and the one at line 47; so print, an f string literal, total execution time

34
00:03:19,160 --> 00:03:25,580
‫and -start. I'm running the script and waiting!

35
00:03:41,270 --> 00:03:51,180
‫So the total execution time was 27 seconds, approximately 9 seconds per device.

36
00:03:51,250 --> 00:03:57,500
‫Just imagine what would happen in a big network with 500 routers.

37
00:03:57,670 --> 00:04:09,750
‫A basic math tells us that it will take 9*500 and that's 4500 seconds,

38
00:04:12,930 --> 00:04:15,460
‫or 75 minutes.

39
00:04:15,510 --> 00:04:16,760
‫This is unacceptable.

40
00:04:17,950 --> 00:04:24,620
‫What we can do is improve our script with multi threading. Instead of sequentially connecting to the

41
00:04:24,620 --> 00:04:25,560
‫routers

42
00:04:25,570 --> 00:04:34,050
‫it would be a better idea to connect to the routers at the same time and execute the backup tasks concurrently.

43
00:04:34,130 --> 00:04:39,520
‫We'll take a short break and in the next lecture I'll show you how to implement multi threading in Python

44
00:04:39,520 --> 00:04:41,450
‫for your network automation scripts.


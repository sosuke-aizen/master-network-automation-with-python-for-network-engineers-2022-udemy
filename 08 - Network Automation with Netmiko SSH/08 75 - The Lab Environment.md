

For this section you will need Cisco Routers running in GNS3 and a Linux VM
hence we’re gonna automate administrative tasks both on Cisco and Linux.

  

 **Cisco and GNS3**

You can import the topology used in this section in GNS3 or you can create it
by yourself.

 **Topology 1** (One Router): [GNS3
Project](https://drive.google.com/open?id=1gcNOXq7wAod0EHZKz-GPjfCd-7VwBnB3) |
[PNG](https://drive.google.com/open?id=1z4nvsAuZd7udRUD4dqzLUsasS3dCI1v_)

 **Topology 2** (Three Routers): [GNS3
Project](https://drive.google.com/open?id=1gqx8pGamn02D0dzOpbd3FKIPOhRJZBgz) |
[PNG](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

 **Note:** If you import it on Linux or Mac, you may also have to change the
Cloud to a NAT Cloud.

  

 **Linux**

To be easier for you I've exported a custom Linux Mint as an OVA file and all
you have to do is import it (double click on it) in VirtualBox.

 **Linux Mint 19.3 OVA file** (user **student** and password **student** ).
Big file: 6 GB

[https://drive.google.com/file/d/12Sa_HVYSyu0y6nST9HidRlQPZ1cUY14e/view?usp=sharing](https://drive.google.com/file/d/12Sa_HVYSyu0y6nST9HidRlQPZ1cUY14e/view?usp=sharing&authuser=0)

Or if you prefer to download and install it manually click on the following
link: <https://www.linuxmint.com/download.php>


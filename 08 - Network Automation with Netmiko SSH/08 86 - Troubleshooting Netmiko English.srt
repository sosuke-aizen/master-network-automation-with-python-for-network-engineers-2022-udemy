﻿1
00:00:02,710 --> 00:00:06,850
‫In this video I'll show you how to properly troubleshoot Netmiko

2
00:00:07,090 --> 00:00:13,630
‫if you are running into problems. Take a look at this simple Python script that connects to a router,

3
00:00:13,990 --> 00:00:19,390
‫executes the show version command, prints out the output and then disconnects.

4
00:00:22,340 --> 00:00:23,270
‫I'm running the script.

5
00:00:27,140 --> 00:00:34,570
‫The script is running without issues! Let's  assume that there is a problem.

6
00:00:34,780 --> 00:00:35,270
‫Netmiko

7
00:00:35,260 --> 00:00:41,110
‫can't authenticate the device or there is another error. In order to troubleshoot

8
00:00:41,200 --> 00:00:47,970
‫most of the problems we should start the logging process. To start the logging process

9
00:00:47,980 --> 00:00:53,260
‫we simply add these three lines of code you are seeing here.

10
00:00:53,260 --> 00:01:01,480
‫This is the official documentation of Netmiko. I copy and paste this piece of code in my Python

11
00:01:01,480 --> 00:01:01,900
‫script.

12
00:01:07,490 --> 00:01:15,170
‫It's importing the logging module and enabling Netmiko logging of all  reads and writes

13
00:01:15,290 --> 00:01:23,680
‫of the communications channel. And this will create a file named test.log in the current working

14
00:01:23,740 --> 00:01:26,470
‫directory. Let’s run the script

15
00:01:26,470 --> 00:01:26,770
‫again!

16
00:01:33,820 --> 00:01:35,330
‫Nothing has changed.

17
00:01:35,410 --> 00:01:38,890
‫Just that a new file called test.log was created.

18
00:01:39,550 --> 00:01:42,010
‫We see it in the current working directory.

19
00:01:45,280 --> 00:01:46,690
‫Inside the file

20
00:01:46,690 --> 00:01:53,380
‫we see lots of low level information about the communication between the Netmiko and the SSH

21
00:01:53,450 --> 00:01:59,530
‫Demon that runs on the router. We see the SSH authentication part,

22
00:01:59,530 --> 00:02:07,210
‫how Netmiko has automatically disabled paging by executing terminal length 0 command, how it

23
00:02:07,210 --> 00:02:12,640
‫has added a \n after each command and the many other low level details.

24
00:02:12,790 --> 00:02:19,360
‫If you get any error you'll probably find it here and it will help you to further troubleshoot the issue.

25
00:02:20,700 --> 00:02:22,140
‫Another way to troubleshoot

26
00:02:22,140 --> 00:02:27,330
‫Netmiko is to use the read and write channel methods of the connection object.

27
00:02:27,390 --> 00:02:31,980
‫These are low level methods to write from or write to the SSH

28
00:02:31,980 --> 00:02:32,460
‫channel.

29
00:02:35,220 --> 00:02:42,450
‫In this example I've executed only the show version command but the behind the scenes Netmiko did

30
00:02:42,450 --> 00:02:43,230
‫a lot of work.

31
00:02:44,190 --> 00:02:47,710
‫Let's take a look at the log file again.

32
00:02:47,760 --> 00:02:55,950
‫It executed the terminal length 0 command, then sent a \n so a new line, it read the

33
00:02:55,950 --> 00:02:58,320
‫prompt and send the show version

34
00:02:58,320 --> 00:03:03,870
‫command as an object of type bytes, not string and so on.

35
00:03:04,170 --> 00:03:06,410
‫So there is a lot of complexity.

36
00:03:06,640 --> 00:03:12,460
‫Let's change the script to use right in regional methods instead of sent command.

37
00:03:13,050 --> 00:03:22,820
‫So I am commenting out these two lines of code and the connection.righ_channel (show version and

38
00:03:22,880 --> 00:03:31,310
‫\n then I'll call the time.slip function: time.slip (2)

39
00:03:31,350 --> 00:03:36,890
‫I'll wait 2 seconds for the device to finish executing that command

40
00:03:36,940 --> 00:03:39,330
‫Don't forget to import the time module!

41
00:03:42,390 --> 00:03:45,060
‫And I'm reading from the channel:

42
00:03:45,060 --> 00:03:53,800
‫output = connection.read_channel and print(output)

43
00:03:56,640 --> 00:04:01,370
‫And I'm running the script; it will run just as before.

44
00:04:04,570 --> 00:04:05,970
‫Okay! That's all!

45
00:04:06,070 --> 00:04:09,550
‫These are some common ways to troubleshoot Netmiko.


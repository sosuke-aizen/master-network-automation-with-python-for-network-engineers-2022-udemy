﻿1
00:00:01,370 --> 00:00:02,590
‫Hello guys!

2
00:00:02,660 --> 00:00:08,870
‫Now you have the knowledge to connect and execute a command on a networking device from Python using

3
00:00:08,870 --> 00:00:09,980
‫Netmiko.

4
00:00:09,980 --> 00:00:16,100
‫Let's move on and see the real power of Netmiko when it comes to network automation.

5
00:00:16,130 --> 00:00:24,500
‫I want to show you an easy way to send the list of commands to a networking device. The classical approach

6
00:00:24,530 --> 00:00:30,380
‫to execute more than one command, like for example of the approach we've seen in the last section

7
00:00:30,410 --> 00:00:37,090
‫at Paramiko, is to create a list of commands including the ones to become admin and the password

8
00:00:37,090 --> 00:00:44,970
‫and then to use a for loop to execute command after command. Even though this works just fine

9
00:00:45,000 --> 00:00:52,200
‫the process could be prone to errors. Netmiko on the other hand provides us with a method that can

10
00:00:52,200 --> 00:00:59,040
‫do the entire job for us. Let's go to coding and see exactly how to do it!

11
00:01:02,830 --> 00:01:07,110
‫I'll use the same simple topology and I'll start from the same baseline.

12
00:01:08,620 --> 00:01:15,100
‫The script imports Netmiko and then using addiction I get that represents the device and the Connect

13
00:01:15,100 --> 00:01:21,660
‫Handler method authenticate to the device and returns a connection object.

14
00:01:21,760 --> 00:01:28,480
‫This is the connection object! And at the end I'm closing the connection.

15
00:01:28,530 --> 00:01:34,320
‫The first thing to do after successfully authenticating is to enter the enable mode.

16
00:01:34,320 --> 00:01:39,280
‫I am just calling the enable method without any argument.

17
00:01:39,330 --> 00:01:42,600
‫I want to print out a message to know what it does

18
00:01:47,550 --> 00:01:53,810
‫and connection.enable. In this example

19
00:01:53,850 --> 00:02:01,260
‫I want to set up a new loopback interface and create a new user. If you don't know what a loopback

20
00:02:01,270 --> 00:02:02,390
‫interface is

21
00:02:02,560 --> 00:02:06,400
‫I can tell you that it's a virtual and logical interface

22
00:02:06,430 --> 00:02:13,310
‫that's always up and is used by network protocols like for example routing protocols.

23
00:02:13,400 --> 00:02:17,860
‫I'm creating a list with the commands that will be executed.

24
00:02:17,860 --> 00:02:21,910
‫In this case I'm starting from global configuration mode.

25
00:02:22,090 --> 00:02:29,590
‫It will enter into global configuration mode automatically and it's not necessary to execute the config

26
00:02:29,590 --> 00:02:32,290
‫terminal comment manually.

27
00:02:32,290 --> 00:02:34,870
‫So I'm creating a list called commands

28
00:02:37,720 --> 00:02:46,100
‫the first command command's interface loopback 0, the name of the interface,

29
00:02:46,260 --> 00:02:57,440
‫the second command IP address 1.1.1.1 255 255 255.

30
00:02:57,450 --> 00:03:00,070
‫This is a host network mask.

31
00:03:00,240 --> 00:03:09,270
‫Then the exit command, I am exiting from the interface configuration mode and being in the global configuration

32
00:03:09,270 --> 00:03:10,040
‫mode

33
00:03:10,160 --> 00:03:19,780
‫I'm creating a user username netmiko secret cisco. Having the list with the commands

34
00:03:19,780 --> 00:03:27,880
‫I'll call the send config set method of the connection object, so connection.sent_config_

35
00:03:27,890 --> 00:03:28,300
‫set

36
00:03:31,220 --> 00:03:36,420
‫It takes as argument the commands list.

37
00:03:36,440 --> 00:03:43,640
‫Note that this method exits the global configuration mode into the enable mode at the end.

38
00:03:43,640 --> 00:03:50,130
‫So if we print the prompt at this moment we'll notice we are in the enable mode.

39
00:03:50,470 --> 00:03:51,910
‫I am printing the prompt!

40
00:03:56,840 --> 00:03:57,910
‫At this point

41
00:03:57,950 --> 00:04:06,000
‫we can save the configuration. As you probably already know on Cisco, as on any other vendors as well,

42
00:04:06,020 --> 00:04:11,010
‫it's necessary to save the configuration if you want it to be still available

43
00:04:11,090 --> 00:04:19,380
‫after the restart. To do that you normally execute copy running startup config or write memory.

44
00:04:20,910 --> 00:04:27,390
‫So connection.send_command and the command is write memory.

45
00:04:32,390 --> 00:04:39,410
‫This is how you save the configuration! Before running the script I want to check that the loopback

46
00:04:39,410 --> 00:04:42,620
‫interface and the user don't already exist.

47
00:04:45,210 --> 00:04:49,560
‫So here in putty I'm executing show ip interface brief

48
00:04:52,430 --> 00:04:59,940
‫and there's no loopback interface and show run include user.

49
00:05:00,100 --> 00:05:04,820
‫There is no user called netmiko. I'm running the script

50
00:05:09,060 --> 00:05:09,680
‫Okay,

51
00:05:09,730 --> 00:05:14,460
‫it's done! Let's return to putty and check again

52
00:05:14,460 --> 00:05:23,200
‫the interfaces and the users; the new user is there and also the loopback

53
00:05:23,210 --> 00:05:29,260
‫interface. Look, this is the interface!

54
00:05:29,310 --> 00:05:38,250
‫Note that the prompt after calling the send config set method is cisco router 1 and a hash sign.

55
00:05:38,310 --> 00:05:45,630
‫This proves that it has automatically exited the global configuration mode after calling the function.

56
00:05:47,010 --> 00:05:49,920
‫The script was executed successfully.

57
00:05:49,920 --> 00:05:59,960
‫But I am sure you wonder why it didn't display a thing. It didn't display the commands and that's because

58
00:06:00,050 --> 00:06:04,240
‫I didn't capture the output and printed.

59
00:06:04,280 --> 00:06:11,630
‫If you want to see the commands and any possible output just take it in a variable and print that variable

60
00:06:11,810 --> 00:06:22,200
‫like this: output=connection.send_config_set and then print output

61
00:06:25,410 --> 00:06:27,120
‫I'm running the script again.

62
00:06:34,450 --> 00:06:44,510
‫And see how it has executed the commands; also note how it entered automatically the global configuration

63
00:06:44,510 --> 00:06:51,770
‫mode before starting sending the commands and exited the global configuration mode at the end.

64
00:06:53,810 --> 00:07:03,910
‫Notice this end command here, it's not in my list; so configured terminal  and end were automatically

65
00:07:03,910 --> 00:07:05,230
‫run by Netmiko.

66
00:07:07,920 --> 00:07:14,280
‫Before finishing this lecture I want to show you a variation of running commands this way.

67
00:07:17,050 --> 00:07:24,460
‫Maybe you don't like that you have to create a list of  commands; a list means square brackets, comma between

68
00:07:24,460 --> 00:07:26,560
‫elements, each element

69
00:07:26,620 --> 00:07:32,340
‫so each  command should be enclosed by single or double quotes and so on.

70
00:07:32,350 --> 00:07:39,000
‫Maybe you prefer to write the  commands as a string like you normally do at the terminal or in a file

71
00:07:39,010 --> 00:07:41,020
‫for example.

72
00:07:41,110 --> 00:07:51,440
‫I am commenting out this line! Let's say you want to execute three  commands, so: cmd= and a string

73
00:07:52,370 --> 00:08:06,560
‫for example 'ip ssh version 2;access-list 1 permit any; and the last command

74
00:08:06,580 --> 00:08:13,180
‫ip domain-name network-automation.io'

75
00:08:17,030 --> 00:08:22,010
‫You have to write the commands as a string, using a unique separator.

76
00:08:22,100 --> 00:08:31,700
‫In this case the separator is a semicolon; and then I'll call the send config set method like this cmd

77
00:08:31,770 --> 00:08:35,080
‫.split and the separator.

78
00:08:36,970 --> 00:08:40,970
‫In fact this will transform this string into a list.

79
00:08:41,030 --> 00:08:42,120
‫It will return a list.

80
00:08:44,910 --> 00:08:45,960
‫I'm running this script!

81
00:08:49,870 --> 00:08:58,790
‫It will execute the commands and there will be no error. And another variation is when you want to write

82
00:08:58,860 --> 00:09:02,370
‫each command on its own line like in a file.

83
00:09:03,930 --> 00:09:12,240
‫So cmd= and this time I'm gonna use a multi string line, which is a string enclosed by triple quotas

84
00:09:14,140 --> 00:09:26,960
‫like this: the first command ip ssh version 2, on the next line access- list 1 permit ip any

85
00:09:28,930 --> 00:09:33,910
‫and the last command on the next line ip domain-name

86
00:09:34,310 --> 00:09:43,730
‫net-auto.io. And when calling this method, the argument of split will be a  \n.

87
00:09:43,750 --> 00:10:00,350
‫I'm splitting by \n like this. And I'm running the script again and I'll get the same results.

88
00:10:00,510 --> 00:10:06,430
‫Okay, there is a small error, that's because this command was not correct.

89
00:10:06,640 --> 00:10:14,760
‫So the correct command is access-list 1 permit any ; however it doesn't have to do anything with

90
00:10:14,760 --> 00:10:15,570
‫Python

91
00:10:15,570 --> 00:10:16,410
‫and Netmiko.

92
00:10:23,070 --> 00:10:24,840
‫Okay, thank you!

93
00:10:24,850 --> 00:10:27,210
‫That's all! In the next lecture

94
00:10:27,360 --> 00:10:31,280
‫I'll show you how to execute  commands from a file using Netmiko.


﻿1
00:00:01,470 --> 00:00:02,890
‫Welcome back!

2
00:00:02,910 --> 00:00:09,060
‫In the last lecture we've seen how to connect to networking devices and execute show commands from Python

3
00:00:09,060 --> 00:00:11,110
‫scripts using Netmiko.

4
00:00:11,360 --> 00:00:17,130
‫Let's move on and see how to execute administration or  privileged commands.

5
00:00:17,130 --> 00:00:24,720
‫For example if instead of the sh ip int brief command I execute show run which is an administrative

6
00:00:24,720 --> 00:00:26,340
‫command I’ll get an error.

7
00:00:37,350 --> 00:00:39,970
‫Invalid input detected.

8
00:00:39,970 --> 00:00:47,590
‫Let's see what this error is about! I'm connecting to the same router using putty and I'm running the

9
00:00:47,590 --> 00:00:48,070
‫command

10
00:00:53,810 --> 00:01:01,430
‫show run and we see the same error ! In order to execute administrative commands

11
00:01:01,520 --> 00:01:11,000
‫we must first enter the enable a.k.a. privileged exec mode and only then run the command; so I'm executing

12
00:01:11,420 --> 00:01:20,130
‫the enable command, entering the enable password, which is cisco, and running the show run command.

13
00:01:23,580 --> 00:01:26,610
‫And this is the output!

14
00:01:26,610 --> 00:01:28,960
‫Now back to Python and Netmiko,

15
00:01:29,100 --> 00:01:36,390
‫in order to enter the enable mode before executing the command will call the enable method of the connection

16
00:01:36,390 --> 00:01:36,890
‫object.

17
00:01:38,310 --> 00:01:49,650
‫So I'm entering the enable mode: connection.enable. That's all! I'm running the script again and there

18
00:01:49,650 --> 00:01:53,320
‫will be no error.

19
00:01:53,350 --> 00:01:58,290
‫This is the output of the show run command.

20
00:01:58,300 --> 00:02:07,110
‫Now let's talk about the prompt! From the display prompt you can find out the mode in which you are! Cisco

21
00:02:07,280 --> 00:02:14,870
‫and also other vendors network operating systems are model and that means that each command is available

22
00:02:14,930 --> 00:02:18,160
‫only in a specific configuration mode.

23
00:02:18,200 --> 00:02:25,700
‫For example if we have the hostname and greater than sign, as the device prompt, we know we are in the

24
00:02:25,790 --> 00:02:27,040
‫unprivileged mode.

25
00:02:28,180 --> 00:02:36,240
‫And if we have a hash sign instead of the greater then, then we know we are in the enable mode.

26
00:02:36,360 --> 00:02:44,720
‫The same is applicable to Linux; if the prompt is the dollar sign then we are logged in an  unprivileged

27
00:02:44,720 --> 00:02:45,850
‫user.

28
00:02:46,310 --> 00:02:52,000
‫And if the prompt ends in a hash sign we know that we are logged in as root.

29
00:02:56,040 --> 00:03:00,190
‫See the difference! In our Python scripts

30
00:03:00,190 --> 00:03:04,260
‫how can we find the prompt into this way the mode we are in?

31
00:03:06,340 --> 00:03:13,030
‫When we want to see the prompt we call the find prompt method of the connection object like this: prompt

32
00:03:14,110 --> 00:03:23,230
‫=connection.find_prompt and print(prompt) and I'm running it.

33
00:03:23,290 --> 00:03:26,230
‫Look here, this is the prompt!

34
00:03:26,370 --> 00:03:35,620
‫Now let's see the prompt after calling the enable method and entering the privileged exact mode. I am

35
00:03:35,620 --> 00:03:38,890
‫copying and then pasting these two lines

36
00:03:45,120 --> 00:03:51,350
‫And we notice the new prompt; so we can have something like this:

37
00:03:51,410 --> 00:04:03,230
‫if '>' in prompt:
‫and I'm indenting this line of code, that means that if the greater than

38
00:04:03,230 --> 00:04:10,780
‫sign is in the prompt, meaning we are in the user exact mode, we are entering the privileged exact mode.

39
00:04:19,100 --> 00:04:23,540
‫Let's move on and talk about global configuration mode!

40
00:04:23,540 --> 00:04:29,730
‫Each time you execute a configuration command you have to be in the global configuration mode.

41
00:04:31,340 --> 00:04:40,100
‫This means that from the enable mode you execute conf t and being in the global configuration mode

42
00:04:40,200 --> 00:04:48,740
‫you are allowed to run configuration commands, like for example to create a new user: username u2,

43
00:04:49,600 --> 00:04:59,530
‫secret cisco. It worked! Let's return to Python and check if we are already in the global config mode,

44
00:05:01,720 --> 00:05:06,910
‫so print connection.check_config_mode.

45
00:05:06,970 --> 00:05:13,990
‫This method will return false because we are in the enable mode, not in the global configuration mode,

46
00:05:18,010 --> 00:05:20,890
‫so just after printing the output, at the end:

47
00:05:26,370 --> 00:05:31,140
‫false! To enter the global configuration mode

48
00:05:31,240 --> 00:05:40,300
‫We call the config mode method of the connection object and we can have something like this, if not end

49
00:05:40,330 --> 00:05:41,650
‫the call to the method

50
00:05:43,000 --> 00:05:53,180
‫so if it returns false then connection.config _mode is entering the global configuration mode.

51
00:05:54,820 --> 00:06:01,270
‫Now if we call the check config method again it will return true because we are in the config mode.

52
00:06:11,530 --> 00:06:14,670
‫Sorry, the method's check_config_mode.

53
00:06:14,740 --> 00:06:17,740
‫This is how you check if you are in the configuration mode.

54
00:06:27,120 --> 00:06:32,900
‫And it returned true this time! Being in the global configuration mode

55
00:06:32,940 --> 00:06:41,410
‫we are allowed to run global configuration commands like say create a new user. So connection.

56
00:06:41,470 --> 00:06:52,450
‫send_command and the command that creates an user : username u3, secret cisco

57
00:06:55,490 --> 00:07:01,260
‫It's not necessary to write a backslash n at the end of the command because Netmiko does

58
00:07:01,390 --> 00:07:06,800
‫that automatically for us. Before running the script

59
00:07:06,810 --> 00:07:15,100
‫I want to check that the user does not already exist; so in putty and the enable mode I'm running show

60
00:07:15,120 --> 00:07:17,750
‫run I

61
00:07:18,000 --> 00:07:19,080
‫iInclude user

62
00:07:22,370 --> 00:07:29,940
‫and the user called u3 does not exist. I'm running the Python script and we are awaiting.

63
00:07:33,470 --> 00:07:37,850
‫It ran without errors and we'll display the users again!

64
00:07:42,960 --> 00:07:47,070
‫As expected there is a new user called u3.

65
00:07:47,190 --> 00:07:56,510
‫It was created by our Python script! Perfect! If we want to exit the global configuration mode in order

66
00:07:56,510 --> 00:08:03,860
‫to be able to run a show command we call the exit config mode method like this: connection.

67
00:08:04,100 --> 00:08:13,220
‫exit_config_mode and I'll check if I'm still in the configuration mode.

68
00:08:13,470 --> 00:08:15,710
‫It should return false.

69
00:08:16,050 --> 00:08:19,020
‫It has exited the global configuration mode!

70
00:08:23,460 --> 00:08:28,940
‫False! Thanks, that's all about configuration mode.


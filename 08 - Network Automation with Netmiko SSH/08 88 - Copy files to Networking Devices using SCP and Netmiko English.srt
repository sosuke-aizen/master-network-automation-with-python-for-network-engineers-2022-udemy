﻿1
00:00:01,430 --> 00:00:07,650
‫In the last lecture I've shown you how to prepare a Cisco device for SCP.

2
00:00:07,670 --> 00:00:13,020
‫Now it's time to see how can we copy files to networking devices,

3
00:00:13,040 --> 00:00:21,500
‫in this case to a Cisco device, from another machine using SCP and the Netmiko library. We have the

4
00:00:21,500 --> 00:00:22,540
‫same topology,

5
00:00:22,550 --> 00:00:28,950
‫we have a single router and I want to copy a file from my Windows recording box to this router.

6
00:00:29,180 --> 00:00:36,080
‫After importing the ConnectHandler class from Netmiko and creating a device dictionary

7
00:00:36,080 --> 00:00:45,530
‫I've created a connection object. In order to use SCP we must import a function called file transfer.

8
00:00:45,560 --> 00:00:50,690
‫So from netmiko import file_transfer.

9
00:00:50,780 --> 00:00:55,170
‫This is the function that will be used for SCP.

10
00:00:55,280 --> 00:01:03,680
‫We have two possibilities: when we want to copy a file to the device and the second possibility when we

11
00:01:03,680 --> 00:01:07,460
‫want to copy a file from the device.

12
00:01:07,460 --> 00:01:09,380
‫First, let's check one more time

13
00:01:09,410 --> 00:01:13,820
‫what is the name of the storage unit.

14
00:01:13,820 --> 00:01:22,030
‫I'll open a console to this router and I'll  dear all file systems. Some devices have a flash memory,

15
00:01:22,040 --> 00:01:27,700
‫other devices have other kind of memory and so on. This device

16
00:01:27,710 --> 00:01:33,160
‫this GNS3 device has a disk0 for storage.

17
00:01:33,170 --> 00:01:43,790
‫In fact it's the PCMCIA disk we've added when I set up the lab so I'm going to copy the file to disk

18
00:01:43,900 --> 00:01:44,750
‫0.

19
00:01:44,780 --> 00:01:46,400
‫Back to our Python script here

20
00:01:46,440 --> 00:01:49,010
‫I write: transfer_output

21
00:01:49,430 --> 00:01:54,910
‫This is just a variable because the function will return an information that can be displayed.

22
00:01:54,980 --> 00:01:58,480
‫And now I'm going to call the file transfer function.

23
00:01:58,860 --> 00:02:05,070
‫So file transfer of ; an now the first argument is our connection object.

24
00:02:05,120 --> 00:02:08,030
‫The second argument is the source file.

25
00:02:08,300 --> 00:02:17,240
‫OK let's copy a file from the local machine to the router. I have here a file named ospf.txt

26
00:02:17,250 --> 00:02:19,380
‫That's the file I'm going to copy.

27
00:02:19,430 --> 00:02:22,040
‫So here between single quotas I write:

28
00:02:22,040 --> 00:02:23,890
‫ospf.txt

29
00:02:24,140 --> 00:02:27,370
‫Then we have test_file=

30
00:02:27,470 --> 00:02:32,370
‫This is the name of the file that will be created on the router.

31
00:02:32,390 --> 00:02:35,190
‫Let's say ospf1.txt

32
00:02:35,420 --> 00:02:38,460
‫The next argument is the file system,

33
00:02:38,540 --> 00:02:41,550
‫where are we going to copy the file:

34
00:02:41,570 --> 00:02:47,800
‫so file system= and I've said before that its name is disk 0.

35
00:02:48,140 --> 00:02:52,270
‫So we write disk0:

36
00:02:52,490 --> 00:02:54,330
‫Then we have the direction,

37
00:02:54,380 --> 00:03:02,630
‫if I want to copy a file from the local machine to a remote device the direction is put and overwrite

38
00:03:02,630 --> 00:03:04,520
‫file and we can have here:

39
00:03:04,520 --> 00:03:05,660
‫true or false.

40
00:03:05,690 --> 00:03:11,780
‫if we want to overwrite the file, if it already exists or not.

41
00:03:11,830 --> 00:03:12,920
‫Let's say here true.

42
00:03:16,710 --> 00:03:18,950
‫This is the function and here

43
00:03:18,990 --> 00:03:27,970
‫I'll print the  transfer output variable: print transfer output and I also have to close the connection.

44
00:03:34,020 --> 00:03:35,260
‫Back on the router

45
00:03:35,280 --> 00:03:43,410
‫I'll ran the dir command on disk 0 and we can see there is no file on that disk.

46
00:03:43,460 --> 00:03:45,570
‫Now I'm going to run the script.

47
00:03:45,570 --> 00:03:55,300
‫The script is running and shortly we'll see the content of transfer_ output variable.

48
00:03:55,390 --> 00:03:58,620
‫We have an authentication failed error.

49
00:03:58,720 --> 00:04:00,320
‫Let's check the username,  OK,

50
00:04:00,340 --> 00:04:03,710
‫I forgot to modify our username,

51
00:04:03,730 --> 00:04:05,920
‫so here the user name is

52
00:04:05,950 --> 00:04:12,490
‫admin, the user that we've created in the previous lecture and I'll run the script one more

53
00:04:12,490 --> 00:04:12,970
‫time.

54
00:04:19,300 --> 00:04:23,620
‫OK, the file has been transferred and now back to the device let's

55
00:04:23,760 --> 00:04:28,160
‫see what is the content of disk 0 and we can see here

56
00:04:28,240 --> 00:04:32,200
‫the file ospf1.txt

57
00:04:32,230 --> 00:04:39,670
‫This is how we copy a file to a networking device using SCP and the Netmiko library.


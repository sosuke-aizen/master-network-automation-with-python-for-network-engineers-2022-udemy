﻿1
00:00:00,680 --> 00:00:06,280
‫Hello guys and welcome back to a new section on the network automation with Python.

2
00:00:06,290 --> 00:00:11,940
‫Now it's time to dive deep into Netmiko. In the last section

3
00:00:11,940 --> 00:00:18,810
‫we saw that Paramiko is the standard Python library used to automate configuration tasks of networking

4
00:00:18,810 --> 00:00:27,310
‫devices using SSH. Let's move on and see what what Netmiko is and what is good for.
‫Netmiko is a

5
00:00:27,310 --> 00:00:31,030
‫multi vendor network library based on Paramiko.

6
00:00:31,240 --> 00:00:42,290
‫It runs on top of Paramiko and is used to reduce its complexity. Both Paramiko and Netmiko are alternatives

7
00:00:42,290 --> 00:00:51,950
‫to configure devices that do not support APIs. An API is a structured mode of sending and receiving

8
00:00:51,950 --> 00:00:55,300
‫structured data from network devices

9
00:00:55,600 --> 00:01:00,660
‫Maybe you wonder what’s the difference between Paramiko and Netmiko.

10
00:01:00,770 --> 00:01:02,600
‫When do we use Paramiko

11
00:01:02,930 --> 00:01:10,660
‫and when do we use Netmiko? Paramiko can be used to communicate with any device that supports

12
00:01:10,750 --> 00:01:11,480
‫ssh.

13
00:01:12,100 --> 00:01:19,180
‫If you can connect to a device or to a server using ssh, for example using putty, then you can use

14
00:01:19,180 --> 00:01:22,200
‫Python and Paramiko as well.

15
00:01:22,410 --> 00:01:30,090
‫On the other hand, although Netmiko is easier to use than Paramiko,  it supports only some devices. But

16
00:01:31,380 --> 00:01:40,200
‫it supports however the most important and used vendors. You can see a listt with all supported platforms

17
00:01:40,440 --> 00:01:50,000
‫here at this link; so we see that we can use Netmiko to automate configuration tasks on Cisco,

18
00:01:50,340 --> 00:01:54,480
‫HP, Arista, Juniper or Linux.

19
00:01:54,510 --> 00:02:00,700
‫There are many other platforms supported by Netmiko, but they were limited tested.

20
00:02:00,900 --> 00:02:11,550
‫For example Alcadel, Dell, Huawei or Palo Alto.
‫As a conclusion if we want to automate the network configuration

21
00:02:11,550 --> 00:02:16,950
‫using ssh and Python we can use either Paramiko or Netmiko.

22
00:02:17,100 --> 00:02:22,440
‫If the platform is supported we could choose Netmiko because it's easier to handle,

23
00:02:22,770 --> 00:02:29,680
‫we write less code and reduce the possibility of having errors but if there is a special device that’s

24
00:02:29,730 --> 00:02:38,500
‫not being supported by Netmiko we could go ahead with Paramiko. Let's move on and install Netmiko.

25
00:02:38,500 --> 00:02:41,690
‫Same as Paramiko

26
00:02:41,700 --> 00:02:47,850
‫it doesn't belong to the Python Standard Library but the installation is very easy.

27
00:02:47,850 --> 00:02:56,370
‫If you use PyCharm you'll go to files settings search for an interpreter

28
00:02:58,770 --> 00:03:09,000
‫and then click on the plus sign, in the right upper corner and then search for Netmiko.

29
00:03:09,100 --> 00:03:16,560
‫It has found Netmiko and I'm clicking on install package; it's installing Netmiko.

30
00:03:21,280 --> 00:03:28,460
‫And Netmiko was successfully installed. To check the installation

31
00:03:28,450 --> 00:03:34,270
‫I'm creating a new Python script and in the file I'll import it and then run the script.

32
00:03:40,840 --> 00:03:43,040
‫import netmiko

33
00:03:43,260 --> 00:03:51,850
‫I'm running the script and we notice that there is no error ; and that means that netmiko was successfully

34
00:03:51,850 --> 00:03:53,020
‫installed.

35
00:03:53,110 --> 00:03:59,370
‫If there is any error it means that there was a problem with the installation and further troubleshooting

36
00:03:59,500 --> 00:04:01,470
‫is required.

37
00:04:01,480 --> 00:04:07,900
‫Finally I have one a mark! Never named your Python script the same as the module.

38
00:04:07,900 --> 00:04:14,410
‫For example if you name your script "Netmiko" and you try to import Netmiko module inside

39
00:04:14,410 --> 00:04:15,240
‫the script

40
00:04:15,310 --> 00:04:21,640
‫you'll get an error. A Python file is in fact a module so your newly created script would shadow

41
00:04:21,970 --> 00:04:29,510
‫the module you've imported. We'll take a short break and in the next lecture I'll show you how to connect

42
00:04:29,520 --> 00:04:36,620
‫to networking devices from Python using Netmiko and how to execute commands on those devices.


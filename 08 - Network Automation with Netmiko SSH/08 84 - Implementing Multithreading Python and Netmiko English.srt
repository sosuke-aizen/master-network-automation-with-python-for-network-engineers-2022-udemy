﻿1
00:00:01,380 --> 00:00:08,800
‫In this lecture will implement multi threading. We'll take the same script and change it!

2
00:00:08,890 --> 00:00:17,680
‫I'm gonna create a function called backup that backups the configuration of a networking device.

3
00:00:17,680 --> 00:00:26,080
‫The function will have one argument, the device which is our dictionary, this will be the so-called target

4
00:00:26,080 --> 00:00:26,810
‫function.

5
00:00:26,950 --> 00:00:29,970
‫the function executed by each thread.

6
00:00:32,820 --> 00:00:37,050
‫I'm cutting and pasting this code into the function's body

7
00:00:46,030 --> 00:00:50,680
‫so when calling the function it will connect to the device,

8
00:00:50,880 --> 00:00:57,370
‫its the function's argument and then execute this piece of code,

9
00:00:59,320 --> 00:01:04,100
‫the code that backups the configuration.

10
00:01:04,250 --> 00:01:10,970
‫Don't forget to add this line to, then the file will read

11
00:01:10,970 --> 00:01:19,620
‫the devices.txt file which contains the IP addresses of the devices to backup. This is the

12
00:01:19,640 --> 00:01:24,830
‫file and in the for loop I'm calling the function:

13
00:01:27,740 --> 00:01:36,140
‫the calling argument is named cisco_device so I can write here cisco_device or to keep it simpler

14
00:01:36,140 --> 00:01:37,850
‫I'll use only the device name.

15
00:01:37,940 --> 00:01:43,230
‫It's the same, you can write anything you like.

16
00:01:43,240 --> 00:01:44,770
‫This is also ok!

17
00:01:50,690 --> 00:01:51,340
‫Okay.

18
00:01:51,420 --> 00:01:59,400
‫This is in fact the same script. I did just organize the code in a function and instead of executing

19
00:01:59,670 --> 00:02:04,650
‫the code in the for loop, this code, I am calling the function.

20
00:02:04,750 --> 00:02:06,220
‫There is no difference.

21
00:02:06,240 --> 00:02:09,110
‫Let's run it to see that it's working as before.

22
00:02:14,610 --> 00:02:17,570
‫We have to wait almost 30 seconds.

23
00:02:20,790 --> 00:02:22,380
‫It takes some time.

24
00:02:26,860 --> 00:02:33,170
‫Okay it's working as expected and it took 27 seconds,

25
00:02:33,170 --> 00:02:36,460
‫just like before.

26
00:02:36,670 --> 00:02:46,090
‫Now I'm starting the multi threading part. First I'll import the threading module that implements multi

27
00:02:46,090 --> 00:02:48,790
‫threading in Python import_threading

28
00:02:56,940 --> 00:02:59,030
‫and inside the for loop

29
00:02:59,070 --> 00:03:01,470
‫I'll start a thread for each device.

30
00:03:01,470 --> 00:03:04,590
‫In this example there are 3 devices,

31
00:03:04,710 --> 00:03:12,330
‫So 3 threads will be started; each thread will back up the configuration of a single device.

32
00:03:12,330 --> 00:03:20,250
‫The pattern when working with threads is the following. I am creating a list that will store the threads.

33
00:03:20,250 --> 00:03:28,260
‫Remember that a Python list can store any type of object, including abstract ones like threads; so threads

34
00:03:28,620 --> 00:03:29,630
‫=list

35
00:03:29,640 --> 00:03:34,300
‫This is an empty list.

36
00:03:34,360 --> 00:03:40,970
‫I'm not calling the backup function anymore but I'm creating a thread like this:

37
00:03:40,970 --> 00:03:50,480
‫th= threading ( the name of the module) .thread and the first argument target= and the

38
00:03:50,480 --> 00:03:52,330
‫target function backup

39
00:03:52,370 --> 00:04:00,860
‫I'm not calling the function. I'm just writing its name, so without parentheses, and the second argument

40
00:04:01,130 --> 00:04:11,420
‫is args= and the functions arguments, in this case there is only one argument, the device.

41
00:04:11,420 --> 00:04:20,480
‫The second argument called args is of type tuple and that's why I've added this comma after device.

42
00:04:21,810 --> 00:04:24,990
‫When you want a tuple with only one element

43
00:04:25,080 --> 00:04:29,820
‫you should put a comma after that single element, like this:

44
00:04:29,820 --> 00:04:33,580
‫for example t=(1)

45
00:04:33,660 --> 00:04:35,350
‫This is not a tuple,

46
00:04:35,490 --> 00:04:38,740
‫this is an int, an integer.

47
00:04:38,850 --> 00:04:49,230
‫If you want a tuple you put a comma after the element; now t is of type tuple; okay!

48
00:04:49,480 --> 00:05:02,350
‫So after creating the thread I will append it to the thread list so threads.append(th) so the

49
00:05:02,360 --> 00:05:05,970
‫threads were created but not started yet.

50
00:05:06,970 --> 00:05:11,360
‫To start the threads outside this for loop

51
00:05:11,360 --> 00:05:17,670
‫okay here, I'll iterate over the threads list and start each thread.

52
00:05:17,770 --> 00:05:28,380
‫So for th in threads:
‫th.start()

53
00:05:28,380 --> 00:05:30,550
‫I'll wait for the threads to finish.

54
00:05:30,630 --> 00:05:41,700
‫So for th in threads:
‫th.join
‫The join method will make the main program wait for each thread to

55
00:05:41,700 --> 00:05:48,150
‫finish executing and that's all, I've implemented multi threading!

56
00:05:49,430 --> 00:05:57,410
‫If you want to go deeper into multi threading see the section on multi threading and multi processing of

57
00:05:57,410 --> 00:06:08,800
‫my Master Python Programming Course. I'm running the script; we'll see a big difference; we notice a big

58
00:06:08,800 --> 00:06:09,630
‫difference:

59
00:06:09,640 --> 00:06:18,490
‫a Python thread starts for each router and the routers  in the topology backed up simultaneously.

60
00:06:18,490 --> 00:06:22,410
‫This is a very big improvement in terms of speed.

61
00:06:22,510 --> 00:06:31,820
‫We see that the entire script takes only 9 seconds for all 3 routers. Remember that the script that

62
00:06:31,830 --> 00:06:39,480
‫backed up the routers sequentially took 29 seconds so 3 times longer.

63
00:06:39,480 --> 00:06:43,630
‫Okay, that's all ! Take a look again at this video

64
00:06:43,740 --> 00:06:51,150
‫until you deeply understand each concept since multi threading is extremely important for network automation.


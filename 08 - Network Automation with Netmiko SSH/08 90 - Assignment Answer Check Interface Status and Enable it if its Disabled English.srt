﻿1
00:00:00,840 --> 00:00:07,080
‫This lecture is the answer to the assignment given to you in the previous lecture.

2
00:00:07,080 --> 00:00:12,980
‫This is just a recording of me thinking loud and walking through the assignment.

3
00:00:13,020 --> 00:00:18,840
‫Maybe you took another approach and that's fine as long as you get the end result.

4
00:00:19,140 --> 00:00:26,460
‫So I'm starting from this template I'm importing the  Netmiko ConnectHandler and I am creating a net

5
00:00:26,500 --> 00:00:32,950
‫connect object. I am entering the enable mode and here I'll start coding.

6
00:00:33,150 --> 00:00:36,710
‫First, the user must enter the interface

7
00:00:36,780 --> 00:00:38,480
‫he wants to check.

8
00:00:38,670 --> 00:00:46,480
‫So interface=input (enter the interface you want to enable)

9
00:00:53,390 --> 00:01:00,350
‫then I'll send the show ip interface command to check the status of that interface.

10
00:01:00,580 --> 00:01:11,660
‫So output=net_connect.send command and the command is show ip interface + interface.

11
00:01:11,740 --> 00:01:19,920
‫I am concatenating with my interface variable, the interface the user has just entered.

12
00:01:20,100 --> 00:01:27,100
‫OK, now I want to see what happens if the user enters an invalid interface.

13
00:01:27,160 --> 00:01:37,090
‫So this is my router and I write show ip interface and Ethernet 100; there is no such an interface.

14
00:01:37,270 --> 00:01:41,620
‫So the output is invalid input detected.

15
00:01:41,680 --> 00:01:50,130
‫I'm going to copy this string and here if and paste. In output

16
00:01:50,160 --> 00:01:54,660
‫this is when the user enters an invalid interface

17
00:01:54,720 --> 00:01:56,670
‫I am printing a message.

18
00:01:56,670 --> 00:02:00,930
‫So print you entered an invalid interface.

19
00:02:05,590 --> 00:02:12,340
‫Else; this is when the user entered a valid interface.

20
00:02:12,340 --> 00:02:16,760
‫Now let's see how does it look like an enabled interface.

21
00:02:16,840 --> 00:02:30,970
‫So show ip interface0/1 ethernet0/1 is up line protocol is up. In fact

22
00:02:31,000 --> 00:02:34,410
‫I want to check the first line of the output.

23
00:02:34,420 --> 00:02:38,090
‫I don't need the entire output.

24
00:02:38,200 --> 00:02:42,960
‫Let's see how can I get the first line, this line from here.

25
00:02:43,010 --> 00:02:45,130
‫I'm going to use something like this:

26
00:02:45,140 --> 00:02:49,680
‫first line=output

27
00:02:49,700 --> 00:02:54,050
‫This is the output of my show ip interface

28
00:02:54,200 --> 00:02:54,750
‫command

29
00:02:55,020 --> 00:02:58,550
‫And I'm going to use the split lines method,

30
00:02:58,730 --> 00:03:00,470
‫so split lines

31
00:03:01,780 --> 00:03:10,170
‫The split lines method returns a list, each element of my list is a line from that string

32
00:03:10,350 --> 00:03:11,730
‫and I only want

33
00:03:11,760 --> 00:03:17,680
‫the first element; the first element is at index 0.

34
00:03:17,730 --> 00:03:21,580
‫So these should be the first line of the output.

35
00:03:21,780 --> 00:03:26,200
‫I want to check that by simply printing these variable.

36
00:03:26,490 --> 00:03:28,020
‫So print first line.

37
00:03:28,230 --> 00:03:31,810
‫I'm going to run the script to check how it works

38
00:03:31,840 --> 00:03:34,350
‫till this point. OK.

39
00:03:34,370 --> 00:03:38,500
‫This IP address is off  router Cisco2

40
00:03:38,510 --> 00:03:40,180
‫so it's a good IP address,

41
00:03:40,190 --> 00:03:42,020
‫it's a valid IP.

42
00:03:42,150 --> 00:03:44,470
‫And I'm going to simply run the script

43
00:03:52,790 --> 00:03:53,450
‫OK.

44
00:03:53,520 --> 00:03:59,350
‫It's asking for the interface, let's say 0

45
00:03:59,390 --> 00:04:00,290
‫/1

46
00:04:00,330 --> 00:04:02,490
‫So this interface from here

47
00:04:06,060 --> 00:04:09,760
‫and the first line is Etherent 0/1 is up

48
00:04:09,770 --> 00:04:13,640
‫line protocol is up.

49
00:04:13,650 --> 00:04:17,760
‫If I disable the interface

50
00:04:17,910 --> 00:04:25,110
‫conf t  interface e0/1 shut and I run the script one more time.

51
00:04:26,100 --> 00:04:28,230
‫We can see a different output

52
00:04:30,780 --> 00:04:32,730
‫interface

53
00:04:32,760 --> 00:04:33,660
‫e0/1

54
00:04:37,230 --> 00:04:42,860
‫and the interface is administratively down, line protocol is done.

55
00:04:43,050 --> 00:04:43,690
‫OK.

56
00:04:43,840 --> 00:04:44,710
‫That's good!

57
00:04:44,970 --> 00:04:51,000
‫And now I am looking for the up string  in the first line variable.

58
00:04:51,000 --> 00:04:53,590
‫So if not up in first line

59
00:04:53,640 --> 00:04:56,000
‫so the interface is down

60
00:04:56,130 --> 00:04:58,380
‫I am printing a message:

61
00:04:58,380 --> 00:05:02,360
‫The interface is down enabling the interface.

62
00:05:11,200 --> 00:05:20,210
‫How do I enable the interface? I must go  and run interface and the name of the interface e0/1

63
00:05:20,340 --> 00:05:24,760
‫and the no shop command and an exit.

64
00:05:24,950 --> 00:05:29,750
‫So netmiko must send these 3 commands.

65
00:05:29,750 --> 00:05:40,680
‫I am going to create a list let's say commands=  and the elements of my list will be the commands

66
00:05:40,830 --> 00:05:46,740
‫interface and now here the name of my interface and this is the string, the user

67
00:05:46,760 --> 00:05:49,390
‫entered + interface.

68
00:05:49,530 --> 00:05:51,570
‫This is the first element.

69
00:05:51,630 --> 00:05:56,160
‫The second element no shut and exit.

70
00:05:56,160 --> 00:06:01,140
‫So the third command, the third element of my list.

71
00:06:01,360 --> 00:06:06,710
‫And now I'm going to use the send config_set method.

72
00:06:07,050 --> 00:06:14,200
‫Let's say output =net_connect.send config_set

73
00:06:14,850 --> 00:06:17,330
‫And the argument is my list,

74
00:06:17,370 --> 00:06:22,470
‫so commands. In  fact here Netmiko

75
00:06:22,470 --> 00:06:25,930
‫will send these 3 commands.

76
00:06:26,090 --> 00:06:31,530
‫I'm going to print the output because I want to see the commands it has executed

77
00:06:31,710 --> 00:06:39,630
‫and to be much clearer I'll print 40 hash signs so # *40

78
00:06:39,920 --> 00:06:47,260
‫and the message, the interface, has been enabled. OK

79
00:06:47,270 --> 00:06:50,390
‫This is how I enable the interface.

80
00:06:50,390 --> 00:06:55,400
‫But what if the interface is up, it's already enabled.

81
00:06:55,550 --> 00:07:01,130
‫First I have here an error: the string is up,

82
00:07:01,150 --> 00:07:02,370
‫not ip.

83
00:07:02,500 --> 00:07:05,580
‫Sorry, if not up in first light.

84
00:07:05,620 --> 00:07:10,730
‫If the interface is up I'm just printing a message.

85
00:07:10,750 --> 00:07:19,820
‫So print interface + interface + is already enabled.

86
00:07:23,130 --> 00:07:24,820
‫And I think that's all.

87
00:07:24,880 --> 00:07:29,180
‫Now I want to check my script. First here

88
00:07:29,230 --> 00:07:32,810
‫I want to see the status of the interface.

89
00:07:32,920 --> 00:07:43,960
‫So show interface e0/1 The interface is up, line protocol is up. My script should only

90
00:07:44,050 --> 00:07:46,200
‫execute this line.

91
00:07:46,540 --> 00:07:47,160
‫OK.

92
00:07:47,260 --> 00:07:48,520
‫Let's run  the script.

93
00:07:54,650 --> 00:07:59,360
‫The interface is e0/1.

94
00:07:59,510 --> 00:08:08,210
‫This is the content of the first line Vvariable and it's displaying that the interface is already enabled

95
00:08:09,110 --> 00:08:11,600
‫and that's what I was expecting.

96
00:08:11,930 --> 00:08:14,510
‫Now let's disable the interface!

97
00:08:22,650 --> 00:08:23,250
‫OK.

98
00:08:23,360 --> 00:08:26,730
‫The interface is down and let's run the script.

99
00:08:34,870 --> 00:08:38,300
‫The interface is down enabling the interface.

100
00:08:38,650 --> 00:08:42,340
‫it's executing the commands.

101
00:08:42,360 --> 00:08:47,530
‫These are the commands and the message the interface has been enabled.

102
00:08:47,760 --> 00:08:52,230
‫Let's check it on the router.

103
00:08:52,300 --> 00:09:01,660
‫We can see how the interface has been enabled, it's up and the last test I want to do is to check if it

104
00:09:01,660 --> 00:09:07,720
‫displays this message if I enter an invalid interface.

105
00:09:08,800 --> 00:09:10,700
‫So one more time I'll run the script.

106
00:09:14,080 --> 00:09:22,570
‫And I write here let's say 9/500 You entered an invalid interface.

107
00:09:22,710 --> 00:09:23,860
‫That's good.

108
00:09:23,870 --> 00:09:33,140
‫I used this interface, and not this one, because this is the interface I am using with my SSH connection.

109
00:09:33,140 --> 00:09:42,250
‫So if I disable this interface e0/0 interface, my SSH connection will drop and that's

110
00:09:42,260 --> 00:09:43,480
‫not what I want.

111
00:09:43,660 --> 00:09:44,380
‫OK.

112
00:09:44,480 --> 00:09:46,500
‫Everything works just fine

113
00:09:46,550 --> 00:09:49,570
‫and this is my answer to your assignment.


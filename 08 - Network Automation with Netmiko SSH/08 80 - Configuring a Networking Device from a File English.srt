﻿1
00:00:01,940 --> 00:00:09,410
‫In the last lecture I've shown you how to run multiple commands on a networking device using Netmiko.

2
00:00:09,410 --> 00:00:16,250
‫We had to call the send config set method of the connection object! We'll go ahead and see how to run commands

3
00:00:16,310 --> 00:00:24,640
‫from a file! We'll use the same simple topology with only one router and the direct connection to it.

4
00:00:24,700 --> 00:00:29,520
‫There is basically no difference if there are more routers or another topology.

5
00:00:29,590 --> 00:00:31,540
‫The principles remain the same.

6
00:00:32,260 --> 00:00:39,560
‫We'll have many more examples in the next lectures, including typologies with more routers.

7
00:00:39,730 --> 00:00:47,300
‫I want you to build your skills from ground up! We'll start from the same baseline as before. I don't

8
00:00:47,300 --> 00:00:52,450
‫go into details any more because I've already explained to you what it does.

9
00:00:54,090 --> 00:01:03,390
‫So in this example I'll enable  OSPF routing protocol on the router, on this one. In the same directory

10
00:01:04,050 --> 00:01:08,530
‫I'll create a text file called ospf.txt

11
00:01:14,020 --> 00:01:22,110
‫And the file will contain the following commands, the commands necessary to enable OSPF so router

12
00:01:22,120 --> 00:01:37,590
‫ospf1 net  0.0.0.0.0.0.0.0 area 0 the backbone area; distance 60,

13
00:01:37,700 --> 00:01:44,560
‫this is the administrative distance and default information originate.

14
00:01:49,750 --> 00:01:50,700
‫Okay!

15
00:01:50,780 --> 00:01:59,080
‫That's the file! We don't go into details about these commands because I have chosen OSPF randomly.

16
00:01:59,080 --> 00:02:04,860
‫I just want to send commands from a file; you can send any commands you wish.

17
00:02:07,510 --> 00:02:11,950
‫I'll start writing the code after calling the enable method.

18
00:02:11,980 --> 00:02:20,410
‫So after entering the enable mode. Netmiko offers us a method called send_config _from_file which sends

19
00:02:20,590 --> 00:02:29,530
‫all the commands from a file into the ssh connection;
‫so I am printing a message sending commands  from

20
00:02:29,530 --> 00:02:29,860
‫file

21
00:02:34,900 --> 00:02:45,620
‫and connection.send_config_from_file, this one. And the argument is the file 'ospf.txt

22
00:02:45,950 --> 00:02:51,040
‫Pay attention to use a valid path!

23
00:02:54,000 --> 00:02:55,620
‫Before running the script

24
00:02:55,710 --> 00:03:04,020
‫I'll open a console connection to the  router and run a debug command.

25
00:03:04,230 --> 00:03:08,190
‫I want to see how the router gets configured from the Python script.

26
00:03:08,940 --> 00:03:19,120
‫So debug ip spf events. If we execute these commands from putty in an SSH connection

27
00:03:19,180 --> 00:03:27,920
‫don't forget to also execute terminal monitor. By default the debug messages are displayed only at

28
00:03:27,940 --> 00:03:29,600
‫console.

29
00:03:29,610 --> 00:03:30,970
‫This is not necessary

30
00:03:31,080 --> 00:03:34,450
‫if you open a console connection like I did,

31
00:03:34,620 --> 00:03:38,750
‫only if you are connecting to the router using putty and SSH.

32
00:03:40,330 --> 00:03:41,490
‫Back to Python

33
00:03:41,500 --> 00:03:42,430
‫I'm running this script,

34
00:03:48,010 --> 00:03:54,170
‫sending commands from file and closing connection. The script has finished

35
00:03:54,190 --> 00:04:02,020
‫without error and if we take a look in the console, here, we see the debug output and that the protocol

36
00:04:02,020 --> 00:04:04,100
‫was enabled.

37
00:04:04,110 --> 00:04:09,660
‫Now if you want to see the commands in Python you just take the output and print it out.

38
00:04:09,780 --> 00:04:14,980
‫For example here it didn't display the commands it executed.

39
00:04:15,970 --> 00:04:19,860
‫So output= and then print(output)

40
00:04:24,310 --> 00:04:25,390
‫We'll see the commands!

41
00:04:29,640 --> 00:04:31,940
‫These are the commands!

42
00:04:31,940 --> 00:04:37,440
‫It's very simple to run commands on a remote device or server from a file using Netmiko.


﻿1
00:00:01,490 --> 00:00:07,250
‫Let's move on and see how to connect to a network a device and and run commands using Netmiko.

2
00:00:10,000 --> 00:00:18,050
‫I've created this topology with only one Cisco router as I want to keep it simple for the moment. Even

3
00:00:18,050 --> 00:00:24,140
‫if there is a more complex topology, with more devices, there would be no big difference.

4
00:00:24,140 --> 00:00:31,610
‫All we need is a network connection available and SSH access from the host that runs the Python scripts

5
00:00:32,000 --> 00:00:39,110
‫to the device we want to configure. Before beginning I'd recommend you to test the network connection

6
00:00:39,410 --> 00:00:42,260
‫between the admin host and the device

7
00:00:42,290 --> 00:00:49,700
‫that runs in GNS3. So I am opening a terminal and pinging it

8
00:00:54,530 --> 00:01:04,570
‫Ping is working and I'll also test SSH using putty. I want to be sure that I have SSH access to

9
00:01:04,570 --> 00:01:05,260
‫the device.

10
00:01:09,120 --> 00:01:13,920
‫This is putty and I'm connecting to the device, to the router.

11
00:01:17,760 --> 00:01:28,740
‫It's working. The username's u1 and the password cisco ! Perfect! If something isn't working properly

12
00:01:29,010 --> 00:01:32,380
‫you have to troubleshoot the problem and make it work.

13
00:01:32,490 --> 00:01:38,850
‫Check that the correct IP address is configured on the router’s interface, that the interface

14
00:01:38,940 --> 00:01:40,290
‫is up and so on.

15
00:01:45,620 --> 00:01:52,890
‫Now let’s return to Python and start creating our script.
‫I'm not going to import the entire

16
00:01:52,900 --> 00:02:02,780
‫Netmiko library but only a class called Netmiko.
‫So from the Netmiko library I'm importing the Nemiko

17
00:02:02,860 --> 00:02:03,340
‫class.

18
00:02:06,390 --> 00:02:15,250
‫Pay attention that its name starts in uppercase N;  then I'll create a connection object by calling

19
00:02:15,250 --> 00:02:22,600
‫the class constructor: connection=netmiko (this is the class constructor, this is how you create

20
00:02:22,600 --> 00:02:32,840
‫an object in Python) and the arguments host= and the IP address, the router's IP address

21
00:02:32,850 --> 00:02:50,130
‫10.1.1.10, port=22, username=u1, password =cisco and device

22
00:02:50,190 --> 00:02:53,940
‫_type =cisco_ios

23
00:02:56,580 --> 00:03:04,800
‫The last argument device_type is the supported platform; we can see all supported platforms at this

24
00:03:04,800 --> 00:03:11,510
‫link. This being a Cisco router I'm gonna chose Cisco IOS

25
00:03:16,850 --> 00:03:24,860
‫Okay, let's test the script up to this point. I want to see if it successfully connects and authenticate

26
00:03:24,890 --> 00:03:25,730
‫to the device

27
00:03:31,400 --> 00:03:33,130
‫and there is no error.

28
00:03:33,140 --> 00:03:36,160
‫If for example I use a wrong username

29
00:03:36,230 --> 00:03:38,970
‫it'll return an error. For example

30
00:03:38,960 --> 00:03:39,930
‫I'll write here

31
00:03:39,950 --> 00:03:44,510
‫u2. I'll get an error, and authentication error.

32
00:03:48,620 --> 00:03:50,560
‫So authentication failed!

33
00:03:54,150 --> 00:04:02,140
‫I'll put back again u1. After successfully authenticating to the  router I can send commands through

34
00:04:02,140 --> 00:04:10,930
‫the SSH connection. To do that I'll use the send command the method of the connection object, so connection

35
00:04:11,440 --> 00:04:15,350
‫.send_command and the command

36
00:04:15,490 --> 00:04:22,760
‫the cisco_ios command show ip interface brief.

37
00:04:22,980 --> 00:04:28,190
‫This will return an output and I'm saving it in a variable.

38
00:04:28,470 --> 00:04:38,350
‫output = and I'm printing out the output variable so print (output) and finally I am disconnecting

39
00:04:38,440 --> 00:04:39,740
‫from the device.

40
00:04:42,240 --> 00:04:47,840
‫connection.disconnect Let's run the script again!

41
00:04:53,780 --> 00:04:56,770
‫And we see the output of the show command.

42
00:04:58,760 --> 00:05:05,600
‫Notice how Netmiko removed unnecessary information, like the router prompt and the executed command,

43
00:05:06,050 --> 00:05:16,100
‫and printed out only the output. And compared to the examples seen at Paramiko in the previous section

44
00:05:16,240 --> 00:05:23,110
‫it was not necessary to execute the terminal length 0 command for it to display the entire output of

45
00:05:23,110 --> 00:05:24,660
‫the show command.

46
00:05:24,670 --> 00:05:33,580
‫Also note that the no call to time.sleep.
‫That was required at Paramikol to pause the execution

47
00:05:33,580 --> 00:05:37,120
‫of the script and wait for the device.

48
00:05:37,120 --> 00:05:40,470
‫Netmiko did this automatically for us.

49
00:05:40,720 --> 00:05:44,370
‫That was the first method to connect to a device.

50
00:05:44,470 --> 00:05:50,900
‫The second method I'm going to show you is the method you'll see in many other examples.

51
00:05:51,100 --> 00:06:00,210
‫Instead of using the Netmiko class it uses another one called ConnectHandler and the dictionary. I'm

52
00:06:00,240 --> 00:06:06,750
‫commenting out the first two lines of the code, the import  statement and the call to Netmiko

53
00:06:08,670 --> 00:06:17,510
‫and instead I'm writing: from netmiko import  ConnectHandler
‫and I'll paste a dictionary.

54
00:06:24,280 --> 00:06:32,930
‫This is the dictionary that contains information about the device: the device type, the host, username,

55
00:06:32,950 --> 00:06:35,400
‫password, port, secret.

56
00:06:35,440 --> 00:06:45,930
‫This is the default enable password and verbose which is true. And the connection object will be created like

57
00:06:45,930 --> 00:06:49,120
‫this connection=Connect

58
00:06:49,110 --> 00:06:56,020
‫Handler (**cisco_device)

59
00:06:56,090 --> 00:07:04,190
‫This is the star star key works style of calling functions and in fact it unpacks the dictionary into

60
00:07:04,190 --> 00:07:13,160
‫the functions arguments. I've talked more about this in the previous section. I am running the Python

61
00:07:13,160 --> 00:07:16,630
‫script and I'll get the same output

62
00:07:27,670 --> 00:07:35,460
‫That's how you connect, authenticate and execute commands using SSH on remote networking devices.

63
00:07:35,470 --> 00:07:39,590
‫Note that I've executed an unprivileged show command.

64
00:07:39,820 --> 00:07:45,850
‫We'll take a short break and in the next lecture we'll see how to enter the enable mode and execute

65
00:07:45,850 --> 00:07:47,140
‫administration commands.


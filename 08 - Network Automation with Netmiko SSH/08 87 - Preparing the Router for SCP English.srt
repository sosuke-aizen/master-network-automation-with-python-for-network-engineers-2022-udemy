﻿1
00:00:01,350 --> 00:00:09,890
‫In this lecture I'll show you how to prepare a Cisco router for SCP, for secure copy or copy over SSH.

2
00:00:09,930 --> 00:00:12,450
‫First if you are using GNS3

3
00:00:12,760 --> 00:00:19,390
‫you must add a PCMCIA slot and format it using the CLI.

4
00:00:19,450 --> 00:00:22,350
‫I'll use a standard,  a classical router,

5
00:00:22,360 --> 00:00:26,980
‫this is a router from series 7200,

6
00:00:26,990 --> 00:00:29,830
‫it uses classical IOS,

7
00:00:30,230 --> 00:00:34,670
‫OK, this is the IOS , it's a K9 imagine,

8
00:00:34,750 --> 00:00:37,340
‫it contains photographical features

9
00:00:37,540 --> 00:00:47,070
‫and here I must add a PCMCIA slot because by defaulting GNS3 a router doesn't have a flash memory,

10
00:00:47,170 --> 00:00:52,730
‫so there is no storage where we can save files using SCP.

11
00:00:52,750 --> 00:00:58,720
‫I'll use a PCMCIA slot on disk 0.

12
00:00:58,720 --> 00:01:04,110
‫So let's create here a disck of let's say 32MG.

13
00:01:04,180 --> 00:01:05,420
‫That's enough

14
00:01:05,620 --> 00:01:07,160
‫in most cases.

15
00:01:09,510 --> 00:01:10,580
‫After that

16
00:01:10,680 --> 00:01:19,930
‫I'll start the router, I'll open a console connection to the router.

17
00:01:19,970 --> 00:01:25,880
‫The second step is to create a user with a privilege level of 15,

18
00:01:25,880 --> 00:01:34,700
‫this is an admin user; we need such a user because it's not possible to run the enable command through

19
00:01:34,960 --> 00:01:43,640
‫SCP and that there are cases, for example in our next lecture, when we are going to use Nepal,

20
00:01:43,700 --> 00:01:54,110
‫there we will copy configuration files and commit changes using SCP  so it's unnecessary that the

21
00:01:54,100 --> 00:02:02,390
‫use of that connects using SSH to be an admin user. Before creating the user

22
00:02:02,400 --> 00:02:04,620
‫I must format the disk

23
00:02:04,650 --> 00:02:13,560
‫I've added. I'll run the command dir all the file systems because I want to see how does it recognize

24
00:02:13,680 --> 00:02:17,870
‫the disk and we can see we have a disk 0.

25
00:02:17,880 --> 00:02:20,300
‫Maybe you have a disck 1 or a slot 0,

26
00:02:20,320 --> 00:02:31,670
‫or maybe there is another name for your disk; so let's format disck 0:   format disk0:

27
00:02:31,860 --> 00:02:42,720
‫And now I'll confirm the operations; the disk has been formatted and we can go to the second step. From

28
00:02:42,780 --> 00:02:44,290
‫the global configuration mode

29
00:02:44,310 --> 00:02:47,880
‫I'll create such a user so username

30
00:02:47,950 --> 00:02:57,050
‫let's say admin privilage 15 secret cisco, the name of my user is admin, its password

31
00:02:57,200 --> 00:03:02,220
‫is Cisco and it's an administrator user.

32
00:03:02,290 --> 00:03:11,490
‫After that we must verify that SSH server is running and that the router accepts SSH connections,

33
00:03:11,490 --> 00:03:15,950
‫that's because SCP uses SSH.

34
00:03:15,950 --> 00:03:20,760
‫This is a default configuration so there is no SSH server configure.

35
00:03:21,000 --> 00:03:23,790
‫I'll configure SSH on this router.

36
00:03:23,820 --> 00:03:30,520
‫First I must create a domain name: ip domain name let's say python automation.com.

37
00:03:30,750 --> 00:03:32,710
‫And now I run the command crypto key

38
00:03:32,720 --> 00:03:35,180
‫generate rsa

39
00:03:35,190 --> 00:03:45,680
‫I want a key that has a length of 1024 bits generating the key and the SSH server

40
00:03:45,740 --> 00:03:48,460
‫has been enabled. Now I'll configure it.

41
00:03:48,470 --> 00:03:52,010
‫vty lines so: line wty 0

42
00:03:52,370 --> 00:03:55,080
‫let's see how many lines does this router have,

43
00:03:55,220 --> 00:04:06,260
‫so we have a lot of lines 1 8 6 9 and here I write: transporting input telnet ssh. I permit both Telnet

44
00:04:06,310 --> 00:04:14,090
‫and SSH and log in local. Back to our typology I'm going to add a cloud in order for the host

45
00:04:14,090 --> 00:04:18,710
‫operating system, my windows recording box to communicate with the router.

46
00:04:18,710 --> 00:04:30,150
‫So here I'm searching for cloud and I add the cloud to the topology.

47
00:04:30,240 --> 00:04:35,630
‫The cloud has been added to my topology and now at properties, here,

48
00:04:35,690 --> 00:04:44,550
‫I click on Show special Ethernet interfaces, refresh and I'll choose GNS3  Loopback;

49
00:04:44,720 --> 00:04:50,130
‫I've created this  Loopback interface in Windows prior to this lab.

50
00:04:50,150 --> 00:04:59,690
‫I have another tutorial on how to create this Loopback interface in Windows; and add and I'm deleting

51
00:04:59,720 --> 00:05:00,230
‫the

52
00:05:00,410 --> 00:05:08,030
‫Ethernet interface, apply and OK. And now I can connect my router with my cloud.

53
00:05:12,870 --> 00:05:16,230
‫Let's configure an IP address on this router,

54
00:05:16,330 --> 00:05:23,080
‫let's say 10.1.1.10 /24 and here in the console interface

55
00:05:23,150 --> 00:05:31,870
‫f0/0 ip  address 10.1.1.10 with a subnet masck of 255.

56
00:05:32,170 --> 00:05:40,690
‫255.255.0 and no shut and now using cmd.exe I'm going

57
00:05:40,690 --> 00:05:42,910
‫to ping the router. First

58
00:05:42,970 --> 00:05:48,450
‫I want to check that my Windows has an IP address in the same subnet, that it's OK,

59
00:05:48,580 --> 00:05:49,890
‫and from here ping,

60
00:05:50,080 --> 00:05:52,120
‫10.1.1.10

61
00:05:54,710 --> 00:05:56,970
‫and ping is working.

62
00:05:57,110 --> 00:05:59,180
‫Telnet also works.

63
00:05:59,330 --> 00:06:01,390
‫Let's check it.

64
00:06:02,310 --> 00:06:09,880
‫OK admin cisco and I want to check also if SSH is working.

65
00:06:09,950 --> 00:06:11,990
‫I'm opening putty, i write here

66
00:06:11,990 --> 00:06:19,730
‫10.1.1.10 and open; and as we can see SSH is working.

67
00:06:19,800 --> 00:06:22,780
‫So admin cisco

68
00:06:23,310 --> 00:06:27,180
‫I am on the router. After the first 3 steps

69
00:06:27,210 --> 00:06:31,490
‫I must enable the SCP server.

70
00:06:31,620 --> 00:06:37,180
‫So back to the router, here, using the router console, from the global configuration mode

71
00:06:37,330 --> 00:06:48,770
‫I write: ip scp server enabled and the last step is to start Triple A or to configure triple A on that device.

72
00:06:48,810 --> 00:06:59,640
‫It's needed for SCP; aaa  new model aaa authentication login default local aaa authorization

73
00:06:59,760 --> 00:07:03,270
‫exec default local and none.

74
00:07:03,480 --> 00:07:08,790
‫And I am saving the configuration. At the moment

75
00:07:08,810 --> 00:07:13,360
‫my router is ready for SCP. In the next lecture

76
00:07:13,370 --> 00:07:22,080
‫I'll show you how to use SCP and how to copy files to this device using Netmiko. See you in a second!


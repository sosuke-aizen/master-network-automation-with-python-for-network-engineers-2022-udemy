﻿1
00:00:01,270 --> 00:00:03,200
‫Hello and welcome back!

2
00:00:03,310 --> 00:00:09,910
‫In this lecture I'll show you how to bake up the configuration of all devices in our topology using

3
00:00:09,940 --> 00:00:10,820
‫Netmiko.

4
00:00:11,050 --> 00:00:18,550
‫I want to stress the importance of the backup process because in case of a failure you can simply restart

5
00:00:18,550 --> 00:00:21,070
‫the backup in just a few seconds.

6
00:00:21,070 --> 00:00:25,240
‫This is one of the first things a network admin should do.

7
00:00:25,270 --> 00:00:31,270
‫This is our topology, the same way we used in the last lecture and we'll start from the same baseline.

8
00:00:32,940 --> 00:00:43,140
‫The Netmiko script connects to the router and enters the enable mode and then disconnects. We'll backup the

9
00:00:43,140 --> 00:00:46,930
‫configuration of one router and then will add

10
00:00:46,980 --> 00:00:54,300
‫the part that does that for all the routers in the topology. The configuration of our Cisco device can

11
00:00:54,300 --> 00:01:01,840
‫be obtained by executing the show run command, so I'm executing the  command. I'm saving the output in

12
00:01:01,840 --> 00:01:04,510
‫a variable and just to test it

13
00:01:04,520 --> 00:01:05,790
‫I am printing it out.

14
00:01:05,890 --> 00:01:11,050
‫So output=connection.send command show run

15
00:01:14,040 --> 00:01:18,930
‫and print( output). I'm running the script.

16
00:01:18,930 --> 00:01:21,090
‫I'll see the routers' configuration!

17
00:01:25,660 --> 00:01:26,420
‫Perfect!

18
00:01:26,450 --> 00:01:28,270
‫This is the the routers' configuration!

19
00:01:31,740 --> 00:01:39,150
‫Instead of printing it out I'll save it to a file, the backup file. The name of the file should be composed

20
00:01:39,210 --> 00:01:45,000
‫of the routers' hostname, the backup word and .txt at the end.

21
00:01:45,090 --> 00:01:47,130
‫So let's get the hostname.

22
00:01:47,250 --> 00:01:54,720
‫The idea is to get the prompt because the prompt consists of the hostname and the hash sign; okay!

23
00:01:54,740 --> 00:02:04,270
‫This is the prompt; so prompt=connection.find_prompt and I'm printing it.

24
00:02:13,270 --> 00:02:14,020
‫It's okay!

25
00:02:14,050 --> 00:02:24,360
‫But I want to remove the hash sign at the end and I'll do that using slicing, so hostname=prompt

26
00:02:25,600 --> 00:02:32,720
‫a pair of square brackets 0:-1.

27
00:02:32,930 --> 00:02:40,370
‫This will return a new string starting from the first element, the element at index 0, up to the last

28
00:02:40,370 --> 00:02:48,780
‫one, which has index 1 excluded. The last element will be excluded! To make it clear

29
00:02:48,930 --> 00:03:00,180
‫let's take a look in idle,  for example s= and the string python and now slicing s a pair of

30
00:03:00,180 --> 00:03:04,760
‫square brackets 0:-1

31
00:03:04,920 --> 00:03:08,910
‫It will return a new string without n, which is the last element

32
00:03:13,060 --> 00:03:18,350
‫or -2 it'll return on your string without the last two elements,

33
00:03:18,370 --> 00:03:21,580
‫the last two characters and so on.

34
00:03:21,580 --> 00:03:27,910
‫This is how string slicing works and if it's not clear I'd recommend you to take another look at the

35
00:03:27,910 --> 00:03:33,880
‫string section where I've dived deeper into it. Let's print the hostname!

36
00:03:43,460 --> 00:03:44,750
‫Perfect!

37
00:03:44,840 --> 00:03:53,330
‫This is the hostname. Now I'm creating the file name using an f string literal to concatenate more strings:

38
00:03:53,750 --> 00:04:03,380
‫filename = f'
‫and a pair of single quotas and between curly braces the variable hostname

39
00:04:06,100 --> 00:04:08,490
‫-backup.txt.

40
00:04:08,830 --> 00:04:18,070
‫This will be the file name. The last step is to save the output of the command to this file so with open

41
00:04:19,750 --> 00:04:29,840
‫the name of the file, file name, the mode W write the file will be created as backup,

42
00:04:29,850 --> 00:04:37,860
‫this is a file object : and backup.right(output).

43
00:04:37,890 --> 00:04:41,970
‫This is how you write to a file and I'll also print out a message.

44
00:04:41,970 --> 00:04:44,020
‫Something like this:

45
00:04:44,020 --> 00:04:51,730
‫backup of hostname between curly braces completed successfully.

46
00:04:55,420 --> 00:04:58,820
‫And 30 hash signs to see it clearer.

47
00:05:00,880 --> 00:05:07,360
‫I'm running the script and a new file called  router1-backup up.txt will be

48
00:05:07,360 --> 00:05:13,500
‫created in the current working directory.

49
00:05:13,640 --> 00:05:15,220
‫Look, this is the file!

50
00:05:15,350 --> 00:05:16,550
‫This is the backup file.

51
00:05:17,520 --> 00:05:24,200
‫It contains the output of the show run command, the router's backup.

52
00:05:24,220 --> 00:05:31,510
‫This is how we backup the configuration of a single router . Let's enhance the script to back up the configuration

53
00:05:31,810 --> 00:05:39,000
‫of all routers in our topology. For this example, to make it more general,

54
00:05:39,020 --> 00:05:45,120
‫I've created a file that contains the IP address of the routers to backup.

55
00:05:45,190 --> 00:05:48,820
‫This is the file; we've also used it in the last lecture.

56
00:05:51,310 --> 00:06:01,710
‫So I am reading the file into a list with open the name of the file devices.txt as f and the

57
00:06:01,710 --> 00:06:04,820
‫devices=

58
00:06:04,890 --> 00:06:13,880
‫f.read.splitlines. This is how you read a file into a list. If you want to see the list content

59
00:06:14,060 --> 00:06:23,720
‫you can print it out, the devices variable. It's a list and each element is an IP of the file. Having the list

60
00:06:23,760 --> 00:06:29,480
‫I'll iterate over it and execute the code below for each element in the list,

61
00:06:29,490 --> 00:06:36,650
‫so for each IP address. Remember that you always use a for loop when you want to execute a repetitive

62
00:06:36,650 --> 00:06:47,250
‫task so: for ip in devices : and I'm indenting the code below.

63
00:06:47,330 --> 00:06:54,520
‫This is the syntax of a for loop in Python. I'm selecting it and pressing on

64
00:06:54,530 --> 00:06:54,920
‫tab

65
00:06:59,170 --> 00:07:06,640
‫and instead of this hardcoded value, the IP address, I'll use the temporary variable of the for loop,

66
00:07:06,760 --> 00:07:07,120
‫IP.

67
00:07:10,570 --> 00:07:18,400
‫At the first iteration IP will be the first elements so the first IP in the list, in the file it's, the

68
00:07:18,400 --> 00:07:21,780
‫second iteration the second ip and so on.

69
00:07:22,960 --> 00:07:23,940
‫Let's run the script!

70
00:07:28,160 --> 00:07:38,480
‫It's connecting to the first router and the backup completed successfully, the second router the backup

71
00:07:38,480 --> 00:07:39,320
‫is ready

72
00:07:40,270 --> 00:07:45,490
‫and the last router. And it's done!

73
00:07:45,770 --> 00:07:51,190
‫And here, on the left side, we'll see the backup files of the routers.

74
00:07:51,200 --> 00:07:53,970
‫This is the backup file of the first router,

75
00:07:54,050 --> 00:08:00,670
‫the second router and the third router. This is good but not perfect!

76
00:08:00,670 --> 00:08:07,290
‫If tomorrow you turn and run the script again you'll overwrite the file and that's not good.

77
00:08:07,450 --> 00:08:10,900
‫Normally you want to keep a versions of your backups.

78
00:08:10,900 --> 00:08:16,780
‫For example you backup the configuration once a day and keep one month of backups.

79
00:08:16,810 --> 00:08:23,410
‫This way you can see the configuration changes that were made over time or can restore the configuration

80
00:08:23,410 --> 00:08:25,390
‫from a specific date in the past.

81
00:08:26,440 --> 00:08:30,470
‫So we need the data and time in the file name.

82
00:08:30,660 --> 00:08:35,870
‫I've already explained in detail how to do that in the Paramiko section.

83
00:08:35,910 --> 00:08:45,480
‫Take a look there if you feel the need! Now I'll just copy and paste that code, this code.

84
00:08:55,270 --> 00:09:00,550
‫I'm not using the hour or the minute, only of the year, the month and that day

85
00:09:04,450 --> 00:09:07,460
‫and I'll add them in the file name.

86
00:09:07,510 --> 00:09:11,170
‫You can add them anywhere you like.

87
00:09:11,260 --> 00:09:20,900
‫For example here I 'm adding the year, I'll put an underscore here, minus the month,

88
00:09:23,460 --> 00:09:24,870
‫minus the day

89
00:09:27,260 --> 00:09:33,970
‫and the another underscore after the day; or I can remove the back up word.

90
00:09:34,040 --> 00:09:37,710
‫It doesn't matter; and I'm running the script.

91
00:09:41,770 --> 00:09:52,800
‫Look here how will be named the backup files.

92
00:09:52,810 --> 00:09:57,820
‫Notice how the file names contain the hostname and the current date.

93
00:10:00,310 --> 00:10:08,620
‫Now we can keep versions! This script is very useful but unfortunately slow.

94
00:10:08,640 --> 00:10:16,530
‫Just imagine how long would it take to backup the configuration of a large network with tens or hundreds

95
00:10:16,530 --> 00:10:18,150
‫of devices.

96
00:10:18,150 --> 00:10:25,610
‫Fortunately there is a solution which is called concurrent execution and it's implemented using multiple

97
00:10:25,610 --> 00:10:28,380
‫processing or multi threading.

98
00:10:28,410 --> 00:10:34,950
‫Now we'll take a short break and I'll show you how to implement that in another lecture of this course.


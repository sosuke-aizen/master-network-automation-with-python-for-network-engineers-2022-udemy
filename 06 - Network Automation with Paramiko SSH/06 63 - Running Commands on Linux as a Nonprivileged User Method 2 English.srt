﻿1
00:00:01,580 --> 00:00:08,640
‫In this lecture I'll show you another method of sending commands to Linux servers. This time

2
00:00:08,690 --> 00:00:15,500
‫each command will run  in its own shell and have its own standard output, input and error. We’ll start

3
00:00:15,500 --> 00:00:17,370
‫from the same baseline.

4
00:00:17,510 --> 00:00:20,690
‫I'll save the previous script is another file

5
00:00:24,060 --> 00:00:28,020
‫and then remove everything excepting the connecting part

6
00:00:33,770 --> 00:00:34,380
‫Okay!

7
00:00:34,400 --> 00:00:35,600
‫This is our baseline.

8
00:00:38,390 --> 00:00:41,040
‫To execute a comment on the server

9
00:00:41,150 --> 00:00:51,870
‫we call this time the exec_command() method of the ssh client so ssh_client.exec_command

10
00:00:54,220 --> 00:01:01,370
‫and the argument will be the command that it will execute let's say ('ifconfig')

11
00:01:02,420 --> 00:01:10,940
‫and \n . This method exec_command will return a tuple with 3 elements of type paramiko.

12
00:01:10,940 --> 00:01:19,450
‫channel.ChannelFile. They are the standard input, output and error of the command that is being

13
00:01:19,450 --> 00:01:34,620
‫executed so stdi,stdout, sderr. To read the output of the command

14
00:01:35,250 --> 00:01:39,230
‫we call the read method of the stdout object

15
00:01:39,240 --> 00:01:51,830
‫that was just returned so output=stdout.read output is af type bytes and if we want

16
00:01:51,830 --> 00:01:59,100
‫a string we must decoded as usual so output=output.decode

17
00:02:02,410 --> 00:02:05,330
‫and print output.

18
00:02:05,490 --> 00:02:09,040
‫I am running the script and will see the command output.

19
00:02:14,180 --> 00:02:21,040
‫This is the command output! We are seeing information about the Linux interfaces.

20
00:02:23,730 --> 00:02:29,130
‫If you want to execute a second command you have to call the exact command method

21
00:02:29,130 --> 00:02:35,570
‫again! This will return its own input, output and error objects.

22
00:02:35,930 --> 00:02:43,150
‫When a command finishes executing, the channel will be closed and cannot be reused.

23
00:02:43,310 --> 00:02:47,840
‫So you must open a new channel if you wish to execute another command.

24
00:02:47,870 --> 00:02:54,710
‫Let's run another command, for example who. It will tell us who is logged into the server.

25
00:02:56,500 --> 00:03:03,580
‫I'll copy and paste these lines; instead of ifconfig

26
00:03:03,580 --> 00:03:08,650
‫I am executing who. After each command

27
00:03:08,690 --> 00:03:15,360
‫it's a good idea to pause the script execution for a small period of time so that the server has enough

28
00:03:15,360 --> 00:03:23,990
‫time to complete the command execution and write to the output buffer time.sleep

29
00:03:24,120 --> 00:03:25,890
‫0.5

30
00:03:26,060 --> 00:03:35,630
‫I am pausing for a half of a second. I'm running the script and we see that the commands are executed

31
00:03:35,720 --> 00:03:39,000
‫successfully.

32
00:03:39,050 --> 00:03:43,230
‫This is the output of the second command.

33
00:03:43,250 --> 00:03:49,970
‫Now if there is any error, like for example we execute a non-existent command or there is a Permission

34
00:03:49,970 --> 00:03:51,000
‫denied error,

35
00:03:51,170 --> 00:04:01,600
‫we'll get to the error by reading from stderr object returned when we called the method. I'm

36
00:04:01,610 --> 00:04:10,780
‫executing a non existing command for example who and I'm writing a few letters, random letters.If I run

37
00:04:10,780 --> 00:04:16,340
‫the script I'll see no error; to get  the error

38
00:04:16,340 --> 00:04:28,480
‫I can do print(stderr.read.decode)  I'm doing everything in one line of code

39
00:04:33,280 --> 00:04:39,240
‫and we see the error:" command not found "and a "Permission denied error"

40
00:04:39,610 --> 00:04:43,300
‫for example  cat/etc/shadow

41
00:04:48,220 --> 00:04:50,780
‫And we've got permission denied.

42
00:04:50,920 --> 00:04:51,280
‫That's all!

43
00:04:51,280 --> 00:04:53,050
‫That's how you execute

44
00:04:53,050 --> 00:04:57,790
‫commands from Python scripts on Linux servers. In the next lecture

45
00:04:57,790 --> 00:05:01,200
‫I'll show you how to execute administration commands as root.


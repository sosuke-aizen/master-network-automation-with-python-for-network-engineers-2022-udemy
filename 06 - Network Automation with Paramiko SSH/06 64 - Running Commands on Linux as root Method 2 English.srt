﻿1
00:00:01,280 --> 00:00:05,470
‫In Linux all administrative tasks must be executed

2
00:00:05,540 --> 00:00:13,340
‫as root. If you want to configure a service, install a program, create a new user or change any configuration

3
00:00:13,640 --> 00:00:14,430
‫you must run

4
00:00:14,430 --> 00:00:17,900
‫the required commands only as root.

5
00:00:17,990 --> 00:00:21,520
‫First let's see how to do it in putty.

6
00:00:21,560 --> 00:00:29,090
‫This is an SSH connection to the Linux machine and I am executing a command that normally requires root privileges

7
00:00:29,510 --> 00:00:30,680
‫like say cat/

8
00:00:30,750 --> 00:00:41,530
‫shadow. If I execute the command as a normal user, like this, I'll get a permission denied  error. To

9
00:00:41,530 --> 00:00:42,100
‫start

10
00:00:42,100 --> 00:00:44,810
‫the command as root I have to write

11
00:00:44,810 --> 00:00:55,920
‫sudo before the command name, like this. And it's asking the user 's password. I am entering the password

12
00:00:56,310 --> 00:00:57,140
‫and hitting

13
00:00:57,150 --> 00:00:59,620
‫enter pass

14
00:00:59,780 --> 00:01:01,310
‫123

15
00:01:01,770 --> 00:01:05,370
‫And we see that the command has been successfully executed.

16
00:01:06,800 --> 00:01:14,630
‫Note that only users that belong to the sudo group are allowed to execute commands as rude by prefixing

17
00:01:14,630 --> 00:01:21,510
‫them with sudo. The user should belong to sudo group.

18
00:01:21,890 --> 00:01:29,750
‫Let's return to PyCharm and see how to do it with Paramiko. I'm saving the last script ss another

19
00:01:29,750 --> 00:01:30,260
‫file

20
00:01:33,440 --> 00:01:39,930
‫and I'll remove some parts of it. In this example

21
00:01:39,950 --> 00:01:49,180
‫I'm gonna create a new user so the executed command will be useradd: stdin, stdout,

22
00:01:50,140 --> 00:01:54,250
‫stderr = ssh_client

23
00:01:54,370 --> 00:02:05,940
‫.exec_command and the command sudo useradd let's say u2; when executing a command as root

24
00:02:06,010 --> 00:02:15,610
‫you need to pass a second argument to the exact command method which is get_pty=True. After sending

25
00:02:15,670 --> 00:02:18,370
‫the sudo useradd command

26
00:02:18,580 --> 00:02:22,330
‫we have to send the password as well.

27
00:02:22,360 --> 00:02:30,910
‫See how it has asked for the password after executing the sudo command in putty. Look at this line!

28
00:02:32,360 --> 00:02:43,200
‫To do that we'll use the write() method of the stdin object returned by exec_command() method. sdtin

29
00:02:43,310 --> 00:02:46,280
‫.write

30
00:02:46,580 --> 00:02:49,110
‫and the password pass

31
00:02:49,240 --> 00:02:53,030
‫123\n'

32
00:02:53,130 --> 00:02:59,930
‫I am also adding a call to time.sleep to be sure that it has enough time to finish executing the

33
00:02:59,930 --> 00:03:07,040
‫command. Just to check that the command was executed successfully

34
00:03:07,390 --> 00:03:11,860
‫I'm gonna execute a second command that displays the Linux users!

35
00:03:28,140 --> 00:03:38,640
‫and print stdout.read .decode and time.sleep1 Let's run the script!

36
00:03:42,610 --> 00:03:51,520
‫We notice that the user was added successfully! The script has created the user successfully.

37
00:03:51,520 --> 00:03:54,540
‫This is the user! You've just seen

38
00:03:54,540 --> 00:03:58,260
‫another way to automate the configuration of a Linux server.


﻿1
00:00:01,610 --> 00:00:08,330
‫Now that you know how to connect to a networking device using  ssh and paramiko we’ll go on and I’ll

2
00:00:08,330 --> 00:00:15,170
‫show you how to execute commands from Python scripts. We’ll continue with the script from the previous

3
00:00:15,170 --> 00:00:24,450
‫lecture. I'll close the connection only if it's active, so if and this method

4
00:00:27,780 --> 00:00:31,240
‫==true

5
00:00:31,460 --> 00:00:38,040
‫this means that the connection is active then I'll close it.

6
00:00:38,270 --> 00:00:44,180
‫So I am intending these two lines and I'm removing this line.

7
00:00:44,580 --> 00:00:46,060
‫Okay, that's better!

8
00:00:49,460 --> 00:00:52,420
‫After connecting to the SSH demon

9
00:00:52,490 --> 00:01:00,040
‫the next step is to create a shell object by calling the invoke shell method of the SSH client

10
00:01:02,120 --> 00:01:13,550
‫so shell =ssh_client.invoke_shell; this will request an interactive shell session on

11
00:01:13,550 --> 00:01:19,440
‫the channel. To send a command to the remote device

12
00:01:19,670 --> 00:01:28,360
‫will use the send method of the shell object like this shell.send, the command

13
00:01:28,570 --> 00:01:33,840
‫for example show version and \n

14
00:01:34,030 --> 00:01:41,830
‫This is a  Cisco IOS command which will display information about the OS and the hardware.

15
00:01:42,640 --> 00:01:44,150
‫\n at

16
00:01:44,170 --> 00:01:46,330
‫the end is mandatory.

17
00:01:46,390 --> 00:01:47,750
‫This is the enter key you

18
00:01:47,770 --> 00:01:53,810
‫normally hit at the end of the command.
‫After sending the command

19
00:01:53,950 --> 00:02:02,770
‫we must wait for the remote device to have enough time to execute it; so I'll put the script to sleep for

20
00:02:02,770 --> 00:02:04,050
‫let's say one second.

21
00:02:04,960 --> 00:02:09,790
‫I'm importing the time module and then calling the sleep function.

22
00:02:17,070 --> 00:02:26,010
‫Let's see the output of the command! To get to the output will use the  recv() method of the shell

23
00:02:26,040 --> 00:02:35,420
‫object like this: output = shell.recv

24
00:02:35,540 --> 00:02:38,150
‫This method takes as argument

25
00:02:38,150 --> 00:02:49,750
‫the number of bytes to be received at once and returns an object of type bytes, for example 10000. Let's

26
00:02:49,810 --> 00:02:52,770
‫see the type of the output variable.

27
00:02:57,180 --> 00:02:58,140
‫I'm running this script.

28
00:03:01,720 --> 00:03:11,100
‫And we see that it's an object of type bytes. Probably you want a string object not one of type bytes.

29
00:03:11,100 --> 00:03:14,250
‫This is what you actually get when running a command.

30
00:03:14,610 --> 00:03:22,020
‫As you already know from the previous lectures you can decode an object of type bytes into a string

31
00:03:22,410 --> 00:03:25,910
‫using the decode method and a decoding scheme.

32
00:03:27,660 --> 00:03:41,780
‫So output=output.decode and the scheme utf-8 which is the default anyway. And I am printing

33
00:03:41,870 --> 00:03:47,140
‫the output so: print( output) I'm running the script again

34
00:03:50,190 --> 00:04:01,580
‫and this is the output; see how I've executed the show version command on the device.

35
00:04:01,600 --> 00:04:06,580
‫It looks great but somehow it seems that we didn't get the whole output.

36
00:04:07,580 --> 00:04:13,180
‫Let's see what happens when we execute the same command it to the console,

37
00:04:14,320 --> 00:04:16,010
‫at the router's console!

38
00:04:16,140 --> 00:04:16,590
‫So here:

39
00:04:20,010 --> 00:04:30,940
‫show version and we notice that it doesn't display the entire output at once, it displays only a part

40
00:04:30,940 --> 00:04:31,460
‫of it,

41
00:04:31,480 --> 00:04:39,670
‫a few lines, and then it waits for the user to press enter to see the next line, like this, or a space

42
00:04:39,880 --> 00:04:42,750
‫to see the next page.

43
00:04:42,780 --> 00:04:50,030
‫Now I'm sure we can imagine that when running this command from Python there is no one to press on space

44
00:04:50,300 --> 00:04:54,990
‫or enter. What we can do is configure the device

45
00:04:55,000 --> 00:05:03,920
‫the Cisco router, so that it displays the entire command output at once, without pausing. And there's

46
00:05:03,960 --> 00:05:11,550
‫the terminal length command which has an option that sets the number of lines of output to display on

47
00:05:11,550 --> 00:05:15,770
‫the terminal screen for the current session before pausing.

48
00:05:15,930 --> 00:05:21,970
‫And if you set that number to 0 it will not pause while displaying the output.

49
00:05:22,320 --> 00:05:29,940
‫So before sending the show version command we must send another comment which is terminal length 0.

50
00:05:29,960 --> 00:05:40,000
‫shell.send and the command 'terminal length 0'

51
00:05:41,150 --> 00:05:50,890
‫And don't forget \n. Let's run the script again!

52
00:05:51,080 --> 00:05:51,960
‫Perfect!

53
00:05:51,980 --> 00:06:00,270
‫Now we are seeing the entire output of the show version command. In this example

54
00:06:00,350 --> 00:06:05,280
‫I've sent two commands to the device that has executed them.

55
00:06:05,360 --> 00:06:12,260
‫If you want to send more commands you just have to call the send method several times,

56
00:06:12,260 --> 00:06:15,290
‫one at a time for each command.

57
00:06:15,380 --> 00:06:22,970
‫For example after executing the show version command  I am also executing the show IP interface brief

58
00:06:22,970 --> 00:06:25,150
‫command, like this:

59
00:06:26,690 --> 00:06:36,010
‫shell.send and the command show ip interface brief and \n

60
00:06:43,720 --> 00:06:44,810
‫Perfect!

61
00:06:44,820 --> 00:06:52,180
‫This is the output of the first command and then it has displayed the output of the second command.

62
00:06:53,550 --> 00:07:00,720
‫That's how you execute commands on remote networking devices using ssh and paramiko.


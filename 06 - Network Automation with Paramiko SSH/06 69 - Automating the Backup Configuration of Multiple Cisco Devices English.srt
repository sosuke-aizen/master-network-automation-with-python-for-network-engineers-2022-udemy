﻿1
00:00:02,050 --> 00:00:03,100
‫Hi guys!

2
00:00:03,100 --> 00:00:09,370
‫In the previous lecture we've seen how to back up the running configuration of a Cisco device.

3
00:00:09,370 --> 00:00:16,000
‫Now I want to show you how to back up the configuration of multiple devices, like all devices in this

4
00:00:16,000 --> 00:00:17,770
‫topology.

5
00:00:17,810 --> 00:00:23,300
‫We have three routers but we can have 100 routers as well.

6
00:00:23,470 --> 00:00:31,150
‫There would be no notable difference. I'd take the script from the previous lecture and I'll save it

7
00:00:31,270 --> 00:00:32,140
‫as another file.

8
00:00:43,460 --> 00:00:49,120
‫Each device has its own IP address and its credentials.

9
00:00:49,180 --> 00:00:50,380
‫This will be router 1

10
00:00:53,910 --> 00:00:56,200
‫router 2 and router 3.

11
00:00:58,920 --> 00:01:02,530
‫and I'm changing the IP address.

12
00:01:02,710 --> 00:01:05,470
‫This is 20 and 30.

13
00:01:05,800 --> 00:01:11,070
‫In this example I'm connecting to the routers using the same username and password,

14
00:01:11,200 --> 00:01:19,390
‫but there is no problem if each router has its own unique user and password; just to use them accordingly.

15
00:01:19,390 --> 00:01:27,420
‫The next step is to create a Python list that contains the dictionaries; for example are routers=

16
00:01:27,970 --> 00:01:38,070
‫and the list. And the elements are router 1, router 2, and router  3. As you already know

17
00:01:38,310 --> 00:01:45,800
‫when you want to execute the same repetitive task you usually use a for loop, so let's  iterate over the

18
00:01:45,800 --> 00:01:57,180
‫router's list: for router in routers: and I'll indent the code below; this is the syntax

19
00:01:57,300 --> 00:01:58,860
‫of a for loop in Python,

20
00:02:02,520 --> 00:02:10,260
‫all of this code. Perfect! That's all! Let's run the script!

21
00:02:10,260 --> 00:02:12,660
‫Pay attention here on the left side.

22
00:02:21,270 --> 00:02:28,060
‫A backup file for each router in the topology will be created in the current working directory.

23
00:02:28,450 --> 00:02:36,070
‫Look, these are the files: for the first router, the second router and the backup for the third router.


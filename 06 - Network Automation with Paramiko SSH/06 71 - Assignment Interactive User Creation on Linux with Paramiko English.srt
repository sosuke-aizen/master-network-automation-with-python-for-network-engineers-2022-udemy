﻿1
00:00:00,550 --> 00:00:07,780
‫Hello guys! I want to give you an assignment. Now that you know how to execute commands on Linux from Python

2
00:00:07,810 --> 00:00:15,160
‫I want you to create a script or to modify the script from the previous lecture, the script that creates

3
00:00:15,250 --> 00:00:17,390
‫a new user on Linux,

4
00:00:17,560 --> 00:00:25,240
‫in order for the script to ask for the user that authenticates on Linux and then it should ask for

5
00:00:25,420 --> 00:00:26,200
‫the user

6
00:00:26,230 --> 00:00:32,110
‫that must be created and then the script displays the Linux users

7
00:00:32,140 --> 00:00:41,410
‫if you choose yes when asking that. This is a very common administration task and can be easily automated

8
00:00:41,590 --> 00:00:47,140
‫from Python. In the next video I'll show you how would I do it

9
00:00:47,170 --> 00:00:49,300
‫in case you get stuck.

10
00:00:49,480 --> 00:00:50,140
‫Good luck!


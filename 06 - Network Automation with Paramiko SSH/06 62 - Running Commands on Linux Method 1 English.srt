﻿1
00:00:01,910 --> 00:00:08,270
‫In this lecture we’ll take a look at how to execute commands on Linux servers from Python scripts

2
00:00:08,330 --> 00:00:19,270
‫using ssh and paramiko. I have a  Linux VM that’s running in VirtualBox. The network interface of the

3
00:00:19,270 --> 00:00:27,070
‫VM is in bridged mode so it has access to the same LAN as the windows recording machine, where the Phyton

4
00:00:27,070 --> 00:00:28,740
‫scripts are executed.

5
00:00:31,290 --> 00:00:38,970
‫Before returning to Python I want to check the network connection between the Linux VM and the Windows

6
00:00:38,970 --> 00:00:46,090
‫host where the Python scripts will be run.This is the Linux IP address and I'll ping it

7
00:00:49,410 --> 00:00:53,080
‫and ping it's working and ssh;

8
00:01:02,060 --> 00:01:03,920
‫ssh is working as well!

9
00:01:07,960 --> 00:01:15,790
‫If something isn't working properly you have to troubleshoot the problem and fix it. First check that

10
00:01:15,790 --> 00:01:23,020
‫that the correct ip is configured on Linux by running ifconfig and second  that the ssh daemon is

11
00:01:23,250 --> 00:01:32,530
‫running and listening on port 22. You can check this  by executing sudo systemctl status

12
00:01:32,620 --> 00:01:33,060
‫ssh.

13
00:01:42,530 --> 00:01:51,810
‫The SSH daemon is active and running. Let's return to our Python script. We connect to Linux machines,

14
00:01:51,930 --> 00:02:01,640
‫the same way as to any other networking devices. I'm just changing the IP address and the credentials.

15
00:02:01,640 --> 00:02:05,240
‫So instead of router there is the name Linux,

16
00:02:10,890 --> 00:02:11,970
‫the IP address,

17
00:02:18,860 --> 00:02:29,710
‫the user name u1 and the password pass123; it's a simple test password. In this

18
00:02:29,780 --> 00:02:35,550
‫simple example I'll just display all user accounts. On Linux

19
00:02:35,580 --> 00:02:39,070
‫the accounts are saved in /etc/password

20
00:02:39,200 --> 00:02:41,180
‫so I'm gonna display the file contents

21
00:02:47,380 --> 00:02:53,010
‫'cat /etc/passwd\n

22
00:02:53,410 --> 00:03:02,130
‫This is the file; don't forget the call to time.sleep to be sure that it has enough time to finish

23
00:03:02,220 --> 00:03:06,450
‫executing the command. And I'm executing the script

24
00:03:09,990 --> 00:03:12,880
‫and we see the contents of the file.

25
00:03:12,960 --> 00:03:23,550
‫These are the Linux accounts, very nice. Let's move on and see how to execute a command as root. To start

26
00:03:23,550 --> 00:03:25,150
‫the command as root

27
00:03:25,150 --> 00:03:25,610
‫you write

28
00:03:25,620 --> 00:03:33,040
‫sudo before the command name, like this: shell.send and the command:

29
00:03:33,270 --> 00:03:41,660
‫sudo cat /etc/shadow  and \n

30
00:03:41,710 --> 00:03:50,010
‫This is another important system file where the hashes of the user's passwords are saved.

31
00:03:50,030 --> 00:03:54,700
‫I am also adding a call to time.sleep to be sure that it has enough time.

32
00:03:56,660 --> 00:04:02,770
‫When executing a command with sudo it will be asking for the user's password.

33
00:04:02,870 --> 00:04:14,280
‫For example sudo  cat/etc/ shadow it's prompting for the password so I am sending the password as any

34
00:04:14,290 --> 00:04:22,410
‫other command after the sudo command so: shell send and the user's password

35
00:04:22,510 --> 00:04:35,120
‫asked by sudo  pass123 \n and the call to time. sleep. I'm running this script

36
00:04:39,400 --> 00:04:41,110
‫Perfect!

37
00:04:41,190 --> 00:04:44,750
‫This is the contents of etc/ shadow.

38
00:04:46,070 --> 00:04:54,680
‫You've just seen how to automate the configuration of Linux operating system. In this example we had only

39
00:04:54,710 --> 00:04:56,190
‫one Linux machine.

40
00:04:56,660 --> 00:05:05,420
‫But if we had the task to add a user or to update a program on a hundred Linux servers all we have to

41
00:05:05,420 --> 00:05:13,790
‫do is to create the list with all servers, iterate over the list using a for loop and execute the necessary

42
00:05:13,790 --> 00:05:17,390
‫comimands on each server in the list.

43
00:05:17,390 --> 00:05:20,720
‫I'll show you many more examples in the next lectures.


﻿1
00:00:00,870 --> 00:00:02,030
‫Welcome back!

2
00:00:02,040 --> 00:00:09,180
‫This time I'll show you how to import myparamiko module into another Python script and call the functions

3
00:00:09,270 --> 00:00:10,700
‫there.

4
00:00:10,710 --> 00:00:17,400
‫Note that the Python script is in fact a module that can be imported and used in other scripts.

5
00:00:17,410 --> 00:00:22,850
‫I'm creating a new Python script: automate Linux tasks

6
00:00:26,800 --> 00:00:38,870
‫and I'm importing the module: import myparamico And I'm running it and surprise, even though there

7
00:00:38,870 --> 00:00:41,000
‫is only the import statement,

8
00:00:41,000 --> 00:00:44,630
‫Python executes the script, myparamiko script!

9
00:00:48,700 --> 00:00:55,330
‫It has executed this code! Running this script that imports the module

10
00:00:55,440 --> 00:00:59,740
‫it's the same as running my paramiko, the module,

11
00:00:59,790 --> 00:01:03,210
‫this script. It is the same!

12
00:01:14,470 --> 00:01:21,820
‫So in Python importing a module means running the script that represents the module and at least in

13
00:01:21,820 --> 00:01:25,250
‫this example it's not what I intended to do.

14
00:01:25,270 --> 00:01:28,510
‫I do not want to execute these Cisco commands,

15
00:01:28,630 --> 00:01:37,260
‫I just want to automate some tasks on Linux. What we can do is go to myparamiko and add a line of code

16
00:01:37,410 --> 00:01:40,200
‫after the functions definitions,

17
00:01:40,200 --> 00:01:47,230
‫so where the code that is executed starts, here.

18
00:01:47,280 --> 00:01:54,330
‫This is the code that gets executed and I'm writing: if __name

19
00:01:54,330 --> 00:01:56,280
‫this is pronounced dander

20
00:01:56,340 --> 00:02:01,770
‫name == between quotes or double quotes,

21
00:02:01,770 --> 00:02:10,040
‫it's a string, dundar main, so double underscores and main on each site.

22
00:02:10,230 --> 00:02:13,290
‫And I am indenting the code below.

23
00:02:13,620 --> 00:02:21,080
‫This is the if syntax, like this. In my master Python programming course

24
00:02:21,100 --> 00:02:29,260
‫I've explained in great detail what that does. There I go deep into key Python concepts like this one.

25
00:02:30,400 --> 00:02:31,600
‫In a nutshell

26
00:02:31,600 --> 00:02:37,780
‫it means that this code will be executed only when directly running this script,

27
00:02:37,780 --> 00:02:42,820
‫my paramiko, not when importing it into another file.

28
00:02:42,820 --> 00:02:47,760
‫So if I run the script, the code will get executed.

29
00:02:47,950 --> 00:02:50,350
‫Let's check it! okay!

30
00:02:50,380 --> 00:02:51,910
‫The code is executing!

31
00:02:55,120 --> 00:02:58,810
‫But if I run script that imports the module,

32
00:02:58,810 --> 00:03:05,910
‫this one, automate Linux tasks, the code will not get executed like before.

33
00:03:06,030 --> 00:03:13,830
‫You see nothing gets executed; and if it doesn't find the module and it says " myparamiko module

34
00:03:13,830 --> 00:03:17,940
‫not found" pay attention to have both files,

35
00:03:17,940 --> 00:03:23,100
‫so the module file and the other file that imports it, in the same directory.

36
00:03:23,100 --> 00:03:30,780
‫You see I have them in this directory called network automation. Now that we know how to properly

37
00:03:30,780 --> 00:03:32,030
‫import a module

38
00:03:32,040 --> 00:03:38,150
‫let's take a look at how to access the functions defined inside the module. So to call a function

39
00:03:38,280 --> 00:03:45,830
‫we use the name of the module, dot and the name of the function we want to call. Let's connect to the

40
00:03:45,830 --> 00:03:47,190
‫Linux VM.

41
00:03:47,210 --> 00:03:55,040
‫I've also used in the previous lectures, to this one. So client = the name of the module

42
00:03:55,050 --> 00:03:57,720
‫myparamiko. and the name of the function

43
00:03:57,720 --> 00:04:00,780
‫connect, the server's ip address,

44
00:04:03,890 --> 00:04:09,920
‫the port, user u1 and the pssword is pass

45
00:04:10,010 --> 00:04:15,310
‫123 and shell=myparamiko

46
00:04:15,560 --> 00:04:18,970
‫.gets_shell and the argument is the client,

47
00:04:18,980 --> 00:04:28,070
‫the ssh_client object and I start sending commands, for example myparamiko.send_command the

48
00:04:28,070 --> 00:04:37,490
‫first argument is the shell and the command, let's say uname -a, its a Linux command and I'm closing

49
00:04:37,490 --> 00:04:38,120
‫the connection.

50
00:04:38,140 --> 00:04:42,670
‫so myparamiko.close and client and I'm running the script:

51
00:04:44,590 --> 00:04:48,630
‫connecting to the server and it's sending the command.

52
00:04:48,700 --> 00:04:53,420
‫Of course it doesn't display a thing because I didn't call the show function.

53
00:04:53,560 --> 00:05:02,410
‫So let's display the output: output=myparaniko.show and the shell and print(output).

54
00:05:03,490 --> 00:05:07,350
‫I'm running it again!

55
00:05:07,460 --> 00:05:14,180
‫Look, this is the output and I can also send commands that will be run as root.

56
00:05:14,300 --> 00:05:24,410
‫Let's say I want to create a new group, a new Linux group, so cmd, the command, is sudo groupadd, let's say developers

57
00:05:25,580 --> 00:05:34,990
‫That's how you add a new group in Linux and now myparamiko.sent command shell cmd,

58
00:05:35,030 --> 00:05:45,620
‫the command and then I'm sending the sudo password: myparamiko.send_command and the sudo pasword

59
00:05:45,650 --> 00:05:47,370
‫is pass 1 2 3.

60
00:05:47,620 --> 00:05:57,130
‫So shell and pass 123 and if I want, I use the third argument which is the timeout, lets say

61
00:05:57,130 --> 00:06:05,590
‫two seconds, I'm waiting for two seconds. By default it was 1. And I want to see the last line in the group

62
00:06:05,590 --> 00:06:11,790
‫file and I want to send another command that displays the last created group.

63
00:06:12,250 --> 00:06:20,560
‫So myparamiko.send_ command shell and now tail -n1 it will display the last line of

64
00:06:20,560 --> 00:06:25,900
‫the file, and the file is /etc/group.

65
00:06:25,900 --> 00:06:30,630
‫This is the file where the groups are saved; and I am executing the script.

66
00:06:39,630 --> 00:06:40,620
‫Perfect!

67
00:06:40,680 --> 00:06:43,430
‫This is the output of the tail command.

68
00:06:43,530 --> 00:06:52,130
‫We see the developers group was created;  it's working just fine but maybe you want to see only the output

69
00:06:52,130 --> 00:06:55,460
‫from a specific command like the last one.

70
00:06:55,460 --> 00:07:02,180
‫You do not want to see the entire output from all executed commands and the commands as well. So maybe

71
00:07:02,180 --> 00:07:10,390
‫you don't want to see the output of uname-a or how this command was executed,

72
00:07:10,430 --> 00:07:12,850
‫the Linux prompt and so on.

73
00:07:12,860 --> 00:07:19,220
‫You can simply flash the buffer at the specific point simply by calling the show function without printing

74
00:07:19,340 --> 00:07:21,710
‫the output, like this:

75
00:07:21,890 --> 00:07:22,650
‫so here,

76
00:07:22,760 --> 00:07:25,870
‫before executing the tail command

77
00:07:25,980 --> 00:07:33,460
‫I'm flashing the buffer: myparamiko.show and shell and that's all.

78
00:07:34,010 --> 00:07:39,590
‫Remember that the show function calls the recv method,

79
00:07:42,420 --> 00:07:49,590
‫this one. So this method in fact flashes the buffer where the output is saved.

80
00:07:49,920 --> 00:07:54,320
‫Execute the script again! We'll see only the output of this command,

81
00:08:03,580 --> 00:08:05,470
‫exactly what we expected.


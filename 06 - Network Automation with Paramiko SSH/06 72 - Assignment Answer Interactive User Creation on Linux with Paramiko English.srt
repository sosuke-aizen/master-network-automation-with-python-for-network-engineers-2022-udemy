﻿1
00:00:01,130 --> 00:00:07,250
‫This lecture is the answer to the assignment given to you in the previous lecture.

2
00:00:07,280 --> 00:00:12,430
‫This is just a recording of me thinking loud and walking through the assignment.

3
00:00:12,620 --> 00:00:18,330
‫Maybe you took another approach and that's fine as long as you get the end result.

4
00:00:18,570 --> 00:00:28,170
‫OK, I'll take the script from the previous lecture and I'll save it as another file. Ok,

5
00:00:28,180 --> 00:00:32,040
‫so here I create a new variable username input

6
00:00:32,080 --> 00:00:35,010
‫I want the script to ask for the username,

7
00:00:35,110 --> 00:00:39,630
‫the  user name that authenticates on Linux; inpu

8
00:00:39,640 --> 00:00:48,160
‫and this is a string username and here instead of this hardcoded value I write username my new

9
00:00:48,170 --> 00:00:48,790
‫variable

10
00:00:49,130 --> 00:00:53,480
‫Ok, I don't want this username to be hardcoded here.

11
00:00:53,620 --> 00:00:58,260
‫So my script will ask for the user that will be created.

12
00:00:58,260 --> 00:01:06,490
‫So let's say new user=input of Enter the user you want to create

13
00:01:09,640 --> 00:01:15,650
‫and the I want a variable for the command that will be executed.

14
00:01:15,650 --> 00:01:22,510
‫so let's say command = and I'll copy paste this value from here.

15
00:01:22,700 --> 00:01:23,000
‫OK.

16
00:01:23,000 --> 00:01:27,920
‫So sudo useradd -m- d /home

17
00:01:28,040 --> 00:01:32,580
‫and instead of user 2 I'll have my new user.

18
00:01:32,750 --> 00:01:37,810
‫So I'll concatenate a string here = new =user.=

19
00:01:37,850 --> 00:01:39,040
‫plus OK.

20
00:01:39,080 --> 00:01:41,680
‫It's very important to have a space here.

21
00:01:41,960 --> 00:01:43,520
‫If I write something like this

22
00:01:43,520 --> 00:01:44,390
‫I'll get an error.

23
00:01:44,420 --> 00:01:47,330
‫So this space is mandatory.

24
00:01:47,330 --> 00:01:52,070
‫-s/bin/bash + new user.

25
00:01:52,090 --> 00:01:55,190
‫OK here I also must have a space.

26
00:01:55,390 --> 00:01:56,620
‫The space from here.

27
00:01:56,640 --> 00:01:59,340
‫Otherwise I'll get an error.

28
00:01:59,480 --> 00:02:06,170
‫I can't print the value of this variable if I want to be sure that everything is OK.

29
00:02:06,320 --> 00:02:12,530
‫OK in here instead of this string I'll have command, so my variable,

30
00:02:12,810 --> 00:02:19,120
‫my variable from here; and this single code doesn't have to be here so I erased it.

31
00:02:19,190 --> 00:02:20,990
‫Everything is just fine.

32
00:02:20,990 --> 00:02:31,700
‫Now let's print a message print (a new user has been created ) and now I want my script to ask if it should

33
00:02:31,940 --> 00:02:39,550
‫display the users so the content of /etc/passwd input

34
00:02:39,790 --> 00:02:48,870
‫let's say answer=input ( display the users yes or no).

35
00:02:49,070 --> 00:02:53,930
‫And if I choose yes it will display the content of that fine.

36
00:02:53,930 --> 00:02:56,470
‫Otherwise it won't display.

37
00:02:56,600 --> 00:03:06,770
‫So if I answer is equal to Y then I'll execute this command from here so I'll indent this line with

38
00:03:06,980 --> 00:03:10,750
‫four spaces and I'll print the user's variable.

39
00:03:10,970 --> 00:03:12,810
‫Ok that's all.

40
00:03:12,890 --> 00:03:18,470
‫I saved the script and I'll run it from the command prompt cd desktop

41
00:03:18,560 --> 00:03:21,550
‫cd network automation

42
00:03:22,290 --> 00:03:25,360
‫OK and this is my script.

43
00:03:25,380 --> 00:03:31,710
‫So Python and the name of the script ! OK.

44
00:03:31,870 --> 00:03:36,650
‫It's asking for the username that will authenticate on Linux

45
00:03:37,150 --> 00:03:41,280
‫So Andrei the password: my pass

46
00:03:41,290 --> 00:03:42,680
‫123

47
00:03:42,820 --> 00:03:45,540
‫And now I must enter the user.

48
00:03:45,540 --> 00:03:47,030
‫I want to create,

49
00:03:47,230 --> 00:03:55,520
‫let's say Dan, a new user has been created displayed the users and I choose yes.

50
00:03:55,540 --> 00:04:05,320
‫So why and it must display the users and you can see here our new user named Dan so our script

51
00:04:05,320 --> 00:04:07,320
‫works as expected.

52
00:04:07,330 --> 00:04:10,330
‫It created the home directory for my user,

53
00:04:10,330 --> 00:04:13,620
‫the shell is bin/bash so everything is just fine.

54
00:04:13,750 --> 00:04:17,230
‫OK this is my answer to your assignment!

55
00:04:17,230 --> 00:04:19,270
‫See you in the next lecture!


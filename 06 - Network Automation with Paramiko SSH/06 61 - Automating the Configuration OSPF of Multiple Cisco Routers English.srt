﻿1
00:00:01,930 --> 00:00:04,120
‫Hello guys and welcome back!

2
00:00:04,120 --> 00:00:10,210
‫In the previous videos we've seen how to configure a single device from a Python script.

3
00:00:10,210 --> 00:00:15,160
‫Now it's time to show you how to automate the configuration of multiple devices.

4
00:00:16,470 --> 00:00:23,980
‫I've built this topology with three routers and I want to enable the OSPF routing protocol on each

5
00:00:23,990 --> 00:00:24,910
‫router.

6
00:00:25,200 --> 00:00:30,600
‫I've chosen o OSPF randomly. Instead of OSPF

7
00:00:30,600 --> 00:00:32,480
‫you can configure anything you wish.

8
00:00:33,520 --> 00:00:41,440
‫We'll start configuring a single router and then we'll see how to do it for all routers in the topology.

9
00:00:43,000 --> 00:00:45,580
‫There's a working connection to each router

10
00:00:45,580 --> 00:00:55,270
‫so before starting this lab I advise you to check the ssh access to the router using putty.  Let's see what

11
00:00:55,300 --> 00:01:03,200
‫are the required commands in order to enable the OSPF
‫routing protocol. First I'll execute the commands

12
00:01:03,230 --> 00:01:07,390
‫in putty and then I'll copy the same commands into the Python script.

13
00:01:09,100 --> 00:01:17,140
‫I'm connecting to the first router using putty; it's IP address is 10.1.1.10

14
00:01:22,500 --> 00:01:30,450
‫this is the first router; username u1 and password cisco and I'm in.

15
00:01:30,850 --> 00:01:35,440
‫So the first command is enable, to enter the enable mode,

16
00:01:35,440 --> 00:01:43,780
‫the password is Cisco then I'm entering the global configuration mode: configure terminal or conf t

17
00:01:43,780 --> 00:01:53,950
‫and I'm starting the OSPF routing protocol router ospf and a process id, let's say 1.

18
00:01:54,450 --> 00:02:00,260
‫And I'm starting OSPF on all interfaces and connected networks.

19
00:02:00,420 --> 00:02:08,100
‫So net 0.0.0.0.0.0.0.0  area 0 the

20
00:02:08,100 --> 00:02:09,080
‫backbone area.

21
00:02:09,980 --> 00:02:15,030
‫Okay, that's all! And I want to see that OSPF was started.

22
00:02:15,510 --> 00:02:25,650
‫So show ip protocols; okay,  "ospf 1" was enabled.

23
00:02:25,650 --> 00:02:26,220
‫Perfect!

24
00:02:26,220 --> 00:02:31,690
‫These are the commands that will be sent from Python. For the moment

25
00:02:31,740 --> 00:02:37,180
‫I am disabling OSPF; I want to enable it from the Python script.

26
00:02:37,740 --> 00:02:40,360
‫So no router

27
00:02:40,560 --> 00:02:47,430
‫ospf 1; let's return to PyCharm and start from this template.

28
00:02:48,780 --> 00:02:56,180
‫I'm connected to the router and I have an active shell and at the end I'm closing the connection.

29
00:02:56,840 --> 00:03:04,280
‫As we've already seen to send a command we have to call the send method of the shell object: shell.

30
00:03:04,280 --> 00:03:13,180
‫send and the first command was enable, to enter the enable mode. At the end of each command

31
00:03:13,210 --> 00:03:15,380
‫there should be a \n

32
00:03:15,430 --> 00:03:16,390
‫Keep this in mind!

33
00:03:18,540 --> 00:03:22,480
‫Then I'm sending the password, the enable password:

34
00:03:22,530 --> 00:03:23,380
‫cisco

35
00:03:23,520 --> 00:03:24,950
‫This isn't a command,

36
00:03:24,960 --> 00:03:25,890
‫It's the password.

37
00:03:31,100 --> 00:03:36,680
‫The next command is conf t or configured terminal.

38
00:03:36,830 --> 00:03:38,970
‫I'm starting the routing protocol

39
00:03:39,020 --> 00:03:50,480
‫ospf and then net 0.0.0.0.0.0.0.0 and area is zero.

40
00:03:50,570 --> 00:03:55,070
‫We don't focus on OSPF but on Python programming.

41
00:03:57,140 --> 00:04:01,920
‫Then end and I'm also sending a show command,

42
00:04:01,970 --> 00:04:07,460
‫I want to see that OSPF was started, so terminal length 0

43
00:04:07,460 --> 00:04:09,010
‫I want to see the entire output

44
00:04:14,620 --> 00:04:16,060
‫and show ip protocols

45
00:04:18,610 --> 00:04:23,510
‫and I'll also add time.sleep and 2 seconds.

46
00:04:23,630 --> 00:04:32,070
‫I want the script to wait for the router to finish executing the commands. I am taking the output

47
00:04:32,430 --> 00:04:33,680
‫and printing it out.

48
00:04:35,450 --> 00:04:47,270
‫shell.recv of let's say 10000 bytes .decode I'm decoding on the same line, I'm decoding from

49
00:04:47,270 --> 00:04:49,960
‫bytes to string and print output.

50
00:04:52,260 --> 00:04:53,120
‫I'm running this script!

51
00:04:59,150 --> 00:05:10,460
‫Perfect! See how it has sent the commands, these are the commands and this is the output of show ip

52
00:05:10,490 --> 00:05:15,030
‫protocols command, so ospf was enabled,

53
00:05:15,200 --> 00:05:25,380
‫was started. We've just configured ospf on the first router, on this one; let's see how to multiply

54
00:05:25,380 --> 00:05:33,510
‫the configuration for all routers in the topology. I'm creating a dictionary for each device.

55
00:05:33,900 --> 00:05:40,710
‫Each device has its own IP address and its credentials; this is router

56
00:05:40,720 --> 00:05:47,910
‫1, this is router 2 and I'm changing the IP address.

57
00:05:48,010 --> 00:05:54,080
‫The second router has an IP address that ends in 20 and the last one,

58
00:05:54,100 --> 00:05:58,390
‫the third router has an IP address that ends in 30.

59
00:06:05,610 --> 00:06:07,000
‫In this example

60
00:06:07,080 --> 00:06:11,250
‫I'm connecting to the routers using the same username and password.

61
00:06:11,610 --> 00:06:20,150
‫But there is no problem if each router has its own unique user and password, just use them accordingly.

62
00:06:20,230 --> 00:06:29,170
‫The next step is to create a Python list that contains the dictionaries; so  routers=

63
00:06:29,570 --> 00:06:38,770
‫And this is a list: router 1, this the first element, router 2  the second and router 3

64
00:06:38,780 --> 00:06:41,780
‫the last element of the list.

65
00:06:42,010 --> 00:06:50,110
‫Now when you want to execute the same repetitive task you usually use a for loop so let's iterate over

66
00:06:50,290 --> 00:06:55,460
‫the routers' list: for router in routers

67
00:06:55,740 --> 00:06:59,980
‫: and I'm indenting the below code,

68
00:07:01,570 --> 00:07:09,570
‫this piece of code. I'm selecting it and pressing on tab and that's all.

69
00:07:12,460 --> 00:07:19,170
‫It will execute this piece of code for each your router in the  routers' list.Let's run the script!

70
00:07:21,340 --> 00:07:31,800
‫It's connecting to the first device, it's connecting to the second device and the output and the 3rd

71
00:07:31,800 --> 00:07:32,160
‫router.

72
00:07:33,210 --> 00:07:35,340
‫So all routers have been configured!

73
00:07:41,630 --> 00:07:49,350
‫That's all! We've automated the OSPF configuration on all devices in our topology.

74
00:07:49,470 --> 00:07:56,120
‫Now if instead of 3 routers you have 100 routers that must be configured

75
00:07:56,190 --> 00:08:01,380
‫you just add the other routers to the list, here.

76
00:08:01,480 --> 00:08:01,960
‫Thank you!


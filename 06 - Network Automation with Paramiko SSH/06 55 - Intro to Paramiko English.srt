﻿1
00:00:00,930 --> 00:00:03,110
‫Hello guys and welcome back!

2
00:00:03,120 --> 00:00:09,960
‫We'll start an important section on Paramiko which is the Python implementation of the SSH protocol

3
00:00:10,380 --> 00:00:17,670
‫Paramiko itself is a pure Python interface around SSH and networking concepts.

4
00:00:17,670 --> 00:00:25,940
‫It uses the C programming language to obtain the highest performance for low level cryptographic concepts.

5
00:00:25,950 --> 00:00:33,180
‫This section is especially important because SSH is probably the most to use the network protocol

6
00:00:33,450 --> 00:00:38,730
‫for the remote management of networking devices and servers.

7
00:00:38,730 --> 00:00:45,330
‫When a network engineer wants to configure or troubleshoot a networking device like a Cisco Router, a

8
00:00:45,810 --> 00:00:52,810
‫security appliance or a Linux Enterprise server he will use in most cases SSH.

9
00:00:53,430 --> 00:01:01,180
‫So we have the opportunity to automate the configuration of networking devices using Python scripts.

10
00:01:01,310 --> 00:01:09,960
‫This way, repetitive tasks which are bored but also prone to errors, can be easily automated to save

11
00:01:10,050 --> 00:01:16,080
‫time, resources and to be much more efficient. In a nutshell

12
00:01:16,110 --> 00:01:24,340
‫any device that can be configured using SSH can be also configured from Python using Paramiko.

13
00:01:24,630 --> 00:01:32,710
‫And even though there are many other powerful Python libraries used for automation like Netmiko,

14
00:01:32,740 --> 00:01:36,820
‫Nornir or  Napalm, Paramiko is most of the time

15
00:01:36,960 --> 00:01:44,430
‫the tool of choice when we want to use SSH from Python Scripts. Paramiko doesn't belong to

16
00:01:44,430 --> 00:01:46,550
‫the standard Python library

17
00:01:46,620 --> 00:01:51,630
‫so in order to use Paramiko we must first install it.

18
00:01:51,690 --> 00:01:53,390
‫Let's take a look at how to do it!

19
00:01:55,240 --> 00:02:00,090
‫Using pip we simply write pip install paramiko in a terminal;

20
00:02:06,480 --> 00:02:15,420
‫pip will download and install Paramiko automatically.
‫Okay, it has installed Paramiko.

21
00:02:15,590 --> 00:02:20,020
‫I'll take this opportunity to update Pip as well.

22
00:02:27,310 --> 00:02:34,630
‫If we are using PyCharm and a virtual environment, which is in fact an isolated environment different

23
00:02:34,630 --> 00:02:36,700
‫from the main Python installation,

24
00:02:36,730 --> 00:02:38,150
‫that's is not enough.

25
00:02:38,260 --> 00:02:47,070
‫We have to install Paramiko also in the virtual environment. To easily test if it is necessary or

26
00:02:47,070 --> 00:02:51,010
‫not, because it depends on how  IDE,

27
00:02:51,030 --> 00:02:53,640
‫in this case  Pycharm, is configured

28
00:02:53,640 --> 00:02:55,860
‫we can simply create a new Python script,

29
00:02:55,980 --> 00:02:59,040
‫import Paramiko and then run the script.

30
00:02:59,200 --> 00:03:05,490
‫Let's do it ! So in this Python script I'm importing Paramiko

31
00:03:08,150 --> 00:03:12,850
‫and I'm executing the script; if there is no error,

32
00:03:12,860 --> 00:03:19,670
‫the Python interpreter used by Py Charm has access to the  paramiko module we’ve just installed using

33
00:03:19,670 --> 00:03:20,340
‫pip.

34
00:03:20,600 --> 00:03:24,820
‫But if you get this error "ModuleNotFoundError:

35
00:03:25,010 --> 00:03:33,190
‫No module named 'paramiko' "  that means that you have to install paramiko in the virtual environment used

36
00:03:33,220 --> 00:03:41,560
‫Pycharm. So in Pycharm go to File -> Settings, search for Interpreter,

37
00:03:45,260 --> 00:03:53,210
‫click on project interpreter then on the plus sign in the upper right corner and then search

38
00:03:53,270 --> 00:03:54,260
‫for Paramiko.

39
00:03:59,100 --> 00:04:00,640
‫The module was found.

40
00:04:00,690 --> 00:04:01,840
‫So I'm going to install it.

41
00:04:06,730 --> 00:04:08,540
‫It's installing Paramiko.

42
00:04:12,250 --> 00:04:13,040
‫Okay!

43
00:04:13,080 --> 00:04:15,390
‫It was installed successfully

44
00:04:18,020 --> 00:04:23,000
‫To test the installation we execute the script that imports Paramiko again!

45
00:04:26,470 --> 00:04:33,350
‫And we notice that there is no error and that means that Paramiko was successfully installed.

46
00:04:33,370 --> 00:04:39,520
‫We'll take a short break and in the next lecture I'll show you how to connect to networking devices

47
00:04:39,790 --> 00:04:41,680
‫from Python using Paramiko.


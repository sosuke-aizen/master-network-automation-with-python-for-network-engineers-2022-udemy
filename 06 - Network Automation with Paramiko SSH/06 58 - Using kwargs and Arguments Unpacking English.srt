﻿1
00:00:01,910 --> 00:00:08,570
‫Before diving deeper into paramiko I want to show you another better way  to call the connect() method

2
00:00:08,990 --> 00:00:18,050
‫of the ssh_client object. Sometimes you want to have all of the information about a device in a single

3
00:00:18,050 --> 00:00:20,960
‫variable, like a Python dictionary.

4
00:00:21,020 --> 00:00:24,800
‫This can be also obtained from a Jason file,

5
00:00:24,800 --> 00:00:35,320
‫something like this: I'm commenting out this piece of code so router= and a Python dictionary

6
00:00:36,900 --> 00:00:50,270
‫{'hostname : and the value, the device IP address, 10.1.1.10  port:

7
00:00:51,150 --> 00:00:59,440
‫'22', 'username': 'u1', 'password'

8
00:00:59,480 --> 00:01:00,750
‫: 'Cisco'}

9
00:01:07,250 --> 00:01:11,300
‫What is important is to have the same names for the dictionary

10
00:01:11,300 --> 00:01:14,890
‫keys as for the methd's arguments.

11
00:01:15,060 --> 00:01:24,530
‫For example if the method’s argument is called hostname then the dictionary key should be  also called

12
00:01:24,620 --> 00:01:33,090
‫hostname and not something else. And I'll call the connect method using the keyword arguments like

13
00:01:33,090 --> 00:01:33,420
‫this:

14
00:01:33,720 --> 00:01:44,870
‫ssh_client.connect (**router, and the last two arguments look_for_keys=False,

15
00:01:46,070 --> 00:01:48,830
‫and  allow_agent=False)

16
00:01:52,470 --> 00:02:00,900
‫**router is called a keyword argument of type dictionary and the double asterisks will

17
00:02:00,990 --> 00:02:09,630
‫unpack the dictionary; the keys will be the methods arguments and the dictionary values will be the values

18
00:02:09,930 --> 00:02:12,600
‫of the method's arguments.

19
00:02:12,790 --> 00:02:21,880
‫This is very useful in Python and you'll see it in many frameworks and libraries. So these two lines

20
00:02:21,970 --> 00:02:24,510
‫of code do the same thing.

21
00:02:24,640 --> 00:02:37,740
‫This line, line number 11, and this one, line number 6. The advantage of using a dictionary and arguments

22
00:02:37,890 --> 00:02:46,740
‫unpacking is that we can use those values also in other parts of the script and they also stay connected.

23
00:02:46,740 --> 00:02:50,520
‫For example I'll change this line,like this:

24
00:02:50,520 --> 00:02:52,440
‫I'll use an f string argument

25
00:02:52,440 --> 00:03:03,040
‫So f and here between curly braces I write router, the name of the dictionary, and between square

26
00:03:03,040 --> 00:03:10,480
‫brackets and double quotes hostname; this will return the value of this key,

27
00:03:10,780 --> 00:03:11,980
‫so the IP address,

28
00:03:15,620 --> 00:03:16,270
‫It's the same.

29
00:03:19,430 --> 00:03:20,160
‫OK.

30
00:03:20,220 --> 00:03:28,000
‫That was a very short lecture on Python keyword arguments and arguments unpacking. I’ve dived deep into

31
00:03:28,000 --> 00:03:33,400
‫them in the functions section of my Master Python programming course.

32
00:03:33,540 --> 00:03:36,300
‫You can take another look there if you feel the need.


﻿1
00:00:01,290 --> 00:00:08,700
‫We've seen how to execute commands  from Python scripts, both on networking devices like Cisco and Linux

2
00:00:08,700 --> 00:00:09,950
‫servers.

3
00:00:09,960 --> 00:00:14,020
‫This is very useful, but it also has a drawback.

4
00:00:14,220 --> 00:00:15,570
‫It's very very slow.

5
00:00:18,900 --> 00:00:20,360
‫In this example

6
00:00:20,520 --> 00:00:26,160
‫I'm creating backups for only three routers and we see that it takes some time.

7
00:00:28,610 --> 00:00:30,430
‫It's still running.

8
00:00:30,680 --> 00:00:37,550
‫We sent command after command through the ssh connection and wait for the  router to finish executing

9
00:00:37,550 --> 00:00:38,290
‫them.

10
00:00:38,510 --> 00:00:42,330
‫When we are done with the first router, we go to the second.

11
00:00:42,800 --> 00:00:48,360
‫So the script works sequentially and that;'s are not good at all. By default

12
00:00:48,380 --> 00:00:54,710
‫a Python script runs as a single process with a single thread inside it.

13
00:00:55,040 --> 00:01:04,660
‫Suppose that each backup takes five seconds and we have a big network with 500 routers. A basic Math

14
00:01:04,990 --> 00:01:13,600
‫tells us that we need 5 * 500 = 2500 seconds or  42 minutes

15
00:01:13,660 --> 00:01:16,990
‫to finish executing the backup script.

16
00:01:16,990 --> 00:01:18,650
‫This is unacceptable!

17
00:01:18,730 --> 00:01:25,900
‫What we can do is improve our script with multi threading. Instead of sequentially connecting to their

18
00:01:25,900 --> 00:01:26,940
‫routers

19
00:01:26,980 --> 00:01:35,610
‫it would be a better idea to connect to the routers at the same time and execute the back up tasks concurrently.

20
00:01:35,680 --> 00:01:41,140
‫So, in this lecture I'll show you how to implement multi threading in Python

21
00:01:41,140 --> 00:01:49,560
‫for your network automation scripts. Enough talking, let's go to work! We'll take the last example and

22
00:01:49,650 --> 00:01:53,860
‫change it. At the beginning of the script

23
00:01:53,870 --> 00:01:59,480
‫I'm going to create a function called backup that backup the  configuration of a router.

24
00:01:59,720 --> 00:02:07,300
‫The function will have one argument , the route, r which is our dictionary. This will be the so-called

25
00:02:07,330 --> 00:02:08,570
‫target function,

26
00:02:08,710 --> 00:02:17,860
‫the function executed by each thread; so def backup and its paramiter is (router)

27
00:02:19,650 --> 00:02:26,820
‫And I'm cutting and pasting this code in functions body until  the end

28
00:02:31,160 --> 00:02:32,300
‫and paste it here

29
00:02:39,370 --> 00:02:40,980
‫And in the for loop

30
00:02:41,040 --> 00:02:45,540
‫I'm calling the function: backup of router.

31
00:02:46,310 --> 00:02:47,690
‫It's the same script.

32
00:02:47,980 --> 00:02:54,970
‫Instead of having the code inside the for loop I've created a function with that code and call it inside

33
00:02:54,970 --> 00:03:02,350
‫the loop; there should be no difference from the previous script. Just to check that it works

34
00:03:02,390 --> 00:03:05,620
‫I'm running it; it's working the same!

35
00:03:12,620 --> 00:03:13,280
‫Now

36
00:03:13,360 --> 00:03:20,830
‫I'm starting the multi threading part. First I'll import the threading module that implements multi threading

37
00:03:20,950 --> 00:03:24,130
‫in Python: import threading

38
00:03:28,060 --> 00:03:29,630
‫Inside the for loop

39
00:03:29,650 --> 00:03:32,120
‫I'll start a thread for each device,

40
00:03:32,260 --> 00:03:35,150
‫in this example there are three devices,

41
00:03:35,320 --> 00:03:43,220
‫so three threads will be started. Each thread will back up the configuration of a single device. The pattern

42
00:03:43,220 --> 00:03:47,230
‫when working with threads is the following:

43
00:03:47,230 --> 00:03:50,520
‫I'm creating a list that will store the threads.

44
00:03:50,590 --> 00:04:00,660
‫Remember that the Python list can store any type of object including abstract ones like threads,  so  threads=

45
00:04:01,180 --> 00:04:02,990
‫and the list constructor.

46
00:04:03,070 --> 00:04:04,180
‫This is an empty list.

47
00:04:08,020 --> 00:04:16,860
‫And inside the for loop I'm writing th= threading the name of the module.thread.

48
00:04:17,100 --> 00:04:20,900
‫In fact it's a constructor that creates a thread.

49
00:04:21,100 --> 00:04:29,050
‫The first argument target= and the name of our function called the target function backup.

50
00:04:29,170 --> 00:04:37,300
‫I'm not calling the function I'm just writing its name so without parentheses and the second argument

51
00:04:38,260 --> 00:04:43,530
‫args= and the functions arguments.

52
00:04:43,720 --> 00:04:51,140
‫In this case there is only one argument: router. The second argument, called args,

53
00:04:51,280 --> 00:04:54,080
‫it's  of type tuple and that's why

54
00:04:54,220 --> 00:04:57,700
‫I've added a comma after the router.

55
00:04:57,700 --> 00:05:04,870
‫When you want a tuple with only one element you should put a comma after that single element.
‫OK.

56
00:05:04,870 --> 00:05:12,900
‫This is a tuple, that's why I've used this comma to be a tuple and I am appending the th variable

57
00:05:12,960 --> 00:05:23,680
‫of type thread to the threads list so:    threads.append(th) Then, outside the

58
00:05:23,820 --> 00:05:25,650
‫the 1st for loop

59
00:05:25,650 --> 00:05:29,420
‫I'll I'll iterate over the threads list and start each thread

60
00:05:29,460 --> 00:05:35,610
‫for th in threads:
‫th.start()

61
00:05:35,710 --> 00:05:41,340
‫And in another for loop, at the end of the script, I'll wait for the threats to finish.

62
00:05:41,820 --> 00:05:51,510
‫So  for th in threads: th.join
‫the join method will make the main program wait for each

63
00:05:51,510 --> 00:05:57,150
‫thread to finish executing and that's all, I'm running the script.

64
00:05:59,240 --> 00:06:01,400
‫And we notice a big difference.

65
00:06:01,400 --> 00:06:07,820
‫A Python thread starts for each router and the routers in topology are backed up simultaneously.

66
00:06:10,080 --> 00:06:11,270
‫It's already done!

67
00:06:11,340 --> 00:06:14,910
‫This is a very big improvement in terms of speed.

68
00:06:19,470 --> 00:06:20,820
‫And one last remark.

69
00:06:20,940 --> 00:06:27,030
‫If you go deeper into multi processing and multi threading you'll see that there is a difference between

70
00:06:27,030 --> 00:06:34,610
‫concurrency and parallelism and in fact the CPU does not execute the threads in parallel.

71
00:06:34,620 --> 00:06:40,530
‫This is however an advanced topic and it's of no use for us for the moment.


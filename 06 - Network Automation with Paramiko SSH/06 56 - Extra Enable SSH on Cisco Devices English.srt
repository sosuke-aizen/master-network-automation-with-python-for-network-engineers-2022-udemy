﻿1
00:00:01,700 --> 00:00:06,840
‫In this lecture I'll show you how to enable SSH on a Cisco router.

2
00:00:06,860 --> 00:00:13,640
‫I've created this simple topology with only one router and the cloud which is in fact my Windows

3
00:00:13,640 --> 00:00:15,110
‫recording machine.

4
00:00:15,200 --> 00:00:22,100
‫There is also a layer3 connectivity between the Windows which will be the SSH each client and the

5
00:00:22,200 --> 00:00:25,270
‫router which will be the SSH server.

6
00:00:25,580 --> 00:00:31,400
‫Let's test the layer3 connectivity between Windows and the router.

7
00:00:31,400 --> 00:00:32,750
‫I am pinging the router

8
00:00:36,080 --> 00:00:37,300
‫and it's working.

9
00:00:39,410 --> 00:00:46,460
‫Let's start with a basic SSH configuration: we'll enable SSH  on the router.

10
00:00:46,460 --> 00:00:55,130
‫At this moment SSH is not enabled on the router; we can see that port 22 is closed on the  router

11
00:00:55,520 --> 00:01:02,020
‫and it's not possible to connect to the router using putty and SSH.

12
00:01:02,120 --> 00:01:11,250
‫Let's see if Port 22 is open and I'm using the Telnet command: telnet the IP address of the router and

13
00:01:11,310 --> 00:01:11,930
‫the port

14
00:01:12,010 --> 00:01:12,330
‫22

15
00:01:16,370 --> 00:01:25,330
‫and we see that the connection failed and that means that the port is closed; I'm opening a console connection

16
00:01:25,330 --> 00:01:30,240
‫to the router.

17
00:01:30,280 --> 00:01:34,300
‫The first step is to configure a hostname.

18
00:01:34,300 --> 00:01:42,200
‫I am entering the global configuration mode and I am executing the command hostname and a name, let's

19
00:01:42,220 --> 00:01:51,610
‫say R1;  anyway the hostname was already configured, but configuring a hostname is mandatory for enabling

20
00:01:51,700 --> 00:01:52,400
‫SSH.

21
00:01:52,690 --> 00:01:53,490
‫Good.

22
00:01:53,500 --> 00:02:02,860
‫The second step is to configure a Domain Name IP: ip domain-name and any domain domain.com.

23
00:02:03,160 --> 00:02:12,520
‫Next we'll enable the SSH daemon which also means generating an rsa key pair on the router crypto

24
00:02:12,520 --> 00:02:14,670
‫key

25
00:02:14,680 --> 00:02:25,510
‫generate rsa; choose a key with a length of at least 2048 bits; if you go with the

26
00:02:25,510 --> 00:02:28,810
‫default which is 512

27
00:02:28,810 --> 00:02:30,100
‫it won't work.

28
00:02:30,100 --> 00:02:38,290
‫The client will not authenticate to this server with such a short key; so the key will have a length

29
00:02:38,590 --> 00:02:40,230
‫of 2048

30
00:02:40,240 --> 00:02:40,600
‫bits

31
00:02:46,110 --> 00:02:55,800
‫ssh enabled and I enable ssh version 2: ip ssh version 2 perfect!

32
00:02:56,210 --> 00:03:00,210
‫Now Port 22 is open on the router! Let's

33
00:03:00,230 --> 00:03:10,050
‫check it again using the telnet command. And we notice that it has connected to the router.

34
00:03:10,090 --> 00:03:19,390
‫The next step is to configure the VTY lines to accept SSH and local authentication. Local authentication

35
00:03:19,390 --> 00:03:24,640
‫means that the users are defined locally on the device.

36
00:03:24,850 --> 00:03:38,420
‫It’s not using a Radius or Tacacs server.
‫so line vty 0 and 4 transport input ssh

37
00:03:38,670 --> 00:03:48,060
‫I can also allow telnet or not. If I add telnet, telnet will also be permitted on the device and if I

38
00:03:48,060 --> 00:03:59,250
‫don't add telnet I can connect only using ssh; let's allow both ssh and telnet and log in local

39
00:04:00,430 --> 00:04:08,590
‫OK, that's all. I've  enabled SSH on the router and the default authentication method is using the password.

40
00:04:08,920 --> 00:04:18,390
‫Let’s try to connect to the router using ssh and putty!
‫I am opening putty, the IP address of the

41
00:04:18,390 --> 00:04:29,720
‫router 10.1.1.10  the default port is 22 ssh and open and it's working.

42
00:04:29,810 --> 00:04:36,560
‫There is already a user configured on the router u1 and the password is cisco.

43
00:04:39,700 --> 00:04:42,350
‫Perfect,it's working just fine!

44
00:04:42,460 --> 00:04:46,630
‫You've just enabled SSH on the router for remote management.


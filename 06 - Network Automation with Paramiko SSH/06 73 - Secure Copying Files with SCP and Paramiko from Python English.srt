﻿1
00:00:00,920 --> 00:00:09,300
‫After we've seen how to use Paramiko to automate simple tasks for Linux or for Cisco IOS now

2
00:00:09,480 --> 00:00:18,230
‫it's time to see how to use SCP to copy files or directories from Python.

3
00:00:18,360 --> 00:00:26,580
‫Once we have reliable programmatic file transfer mechanism then there are several interesting and useful

4
00:00:26,580 --> 00:00:36,400
‫automation use cases for example loading new software images, loading devices initial configuration, restoring

5
00:00:36,480 --> 00:00:45,670
‫a configuration for a failed device, loading a completely new configuration file and so on. SCP or secure

6
00:00:45,670 --> 00:00:53,850
‫copy is a protocol used to copy files from a client to a server or from one server to a client in a

7
00:00:53,850 --> 00:00:54,970
‫network.

8
00:00:54,990 --> 00:01:03,610
‫It uses ssh and the connection is secure; it's the encrypted ssh connection.

9
00:01:03,630 --> 00:01:13,710
‫In this example I'll use the SCP module that uses Paramiko transport layer to send and receive files.

10
00:01:13,920 --> 00:01:15,230
‫In a new file

11
00:01:15,450 --> 00:01:27,180
‫I import the Paramiko module and then from SCP module I import the SCP client.

12
00:01:27,240 --> 00:01:32,910
‫It is very important to be sure that SCP is installed.

13
00:01:32,970 --> 00:01:41,700
‫So if you are using PyCharm you have to go to settings project interpreter and here you must see the

14
00:01:41,700 --> 00:01:43,130
‫SCP model.

15
00:01:43,200 --> 00:01:50,910
‫If you don't see here the SCP module you can simply install it by pressing the plus sign and looking

16
00:01:50,910 --> 00:01:53,570
‫here for SCP.

17
00:01:53,820 --> 00:02:02,310
‫Scp Module for Paramiko is already installed on my machine. In order to use SCP

18
00:02:02,370 --> 00:02:06,950
‫we must first connect to the device using Paramiko.

19
00:02:07,110 --> 00:02:19,040
‫So from a previous script I'll copy the connection line so I'll copy the code that is used to connect

20
00:02:19,040 --> 00:02:21,960
‫to the device and paste it here.

21
00:02:21,990 --> 00:02:32,850
‫I am going to use a Linux server with this IP address to copy files from my windows recording box to

22
00:02:33,300 --> 00:02:34,670
‫to that server.

23
00:02:34,830 --> 00:02:42,180
‫So her the IP address is 192.68.0

24
00:02:42,210 --> 00:02:43,980
‫123

25
00:02:44,010 --> 00:02:47,330
‫The port is 2299

26
00:02:47,370 --> 00:02:49,730
‫username is Andrei

27
00:02:49,920 --> 00:03:03,480
‫the password is my pass 123; it's important not to save passwords in files I've saved the

28
00:03:03,480 --> 00:03:09,210
‫password inside this file just because we are just testing how it works.

29
00:03:09,230 --> 00:03:14,010
‫But in a real environment you don't have to save passwords.

30
00:03:14,040 --> 00:03:15,460
‫This is dangerous.

31
00:03:15,480 --> 00:03:20,010
‫The IP address must be written between single codes.

32
00:03:20,190 --> 00:03:26,760
‫Now I'll create an SCP object by calling the SCP client function.

33
00:03:26,760 --> 00:03:34,520
‫This function takes a single argument and that argument is the Paramiko transport layer.

34
00:03:34,650 --> 00:03:43,060
‫So here as an argument I'll write ssh _client .get_transport

35
00:03:43,070 --> 00:03:50,240
‫Let's see how can we copy a file from this machine to the Linux server.

36
00:03:50,250 --> 00:03:53,660
‫So if I want to upload a file

37
00:03:53,760 --> 00:04:01,710
‫I'll use scp. put the first argument is the path to the local file,

38
00:04:01,740 --> 00:04:05,150
‫I have a file from the current directory,

39
00:04:05,250 --> 00:04:10,540
‫you can use any path you want as long as that path is valid;

40
00:04:10,680 --> 00:04:15,090
‫so for example I'll copy the devices.txt file,

41
00:04:15,200 --> 00:04:21,330
‫ok, this file, and I'll copy the file to a remote location.

42
00:04:21,330 --> 00:04:28,780
‫So for example I'll copy the file in /tmp/aa.txt

43
00:04:28,890 --> 00:04:34,770
‫So this is the new name for my file. Before running the script

44
00:04:34,770 --> 00:04:44,100
‫I must close the connection so scp close; here, on this machine, I list the contents of /tmp

45
00:04:44,340 --> 00:04:45,930
‫directory.

46
00:04:46,080 --> 00:04:50,410
‫There is no file named aa.txt.

47
00:04:50,670 --> 00:05:00,360
‫And now I run the script. The script has been executed without error and now if I go here on my server

48
00:05:00,600 --> 00:05:07,050
‫I can see the aa.txt file inside/tmp directory.

49
00:05:07,080 --> 00:05:15,680
‫If I don't use an absolute path here it will copy the file inside the home directory of the user

50
00:05:15,780 --> 00:05:18,930
‫that authenticates through ssh.

51
00:05:19,080 --> 00:05:28,230
‫So if I run the script one more time the file will appear inside the home directory of user Andrei and

52
00:05:28,410 --> 00:05:29,900
‫there is the file.

53
00:05:30,420 --> 00:05:36,910
‫Now I want to copy a directory so copy a directory.

54
00:05:37,230 --> 00:05:42,850
‫I want to copy a directory from this machine to my Linux server.

55
00:05:43,080 --> 00:05:48,760
‫So scp.put the first argument is the directory,

56
00:05:48,870 --> 00:05:54,650
‫for example here I have a directory named directory1,  inside this directory

57
00:05:54,650 --> 00:05:56,140
‫there are 3 files.

58
00:05:56,490 --> 00:06:04,280
‫I'll copy this directory: directory1, the second argument is recursive

59
00:06:04,640 --> 00:06:06,420
‫=true

60
00:06:06,560 --> 00:06:16,710
‫It means it will copy my directory and its content and the remote path means where will it copy the

61
00:06:16,770 --> 00:06:17,580
‫directory.

62
00:06:17,580 --> 00:06:22,290
‫So in this example it will be that directory to /tmp.

63
00:06:22,290 --> 00:06:26,590
‫This is a string so it must be written between single quotes.

64
00:06:26,670 --> 00:06:28,990
‫And now I run the script.

65
00:06:29,370 --> 00:06:33,930
‫The script ran without error and if I list

66
00:06:33,930 --> 00:06:41,370
‫the /tmp directory we can see here that we have a directory named directory1 and inside

67
00:06:41,490 --> 00:06:42,640
‫that directory

68
00:06:42,750 --> 00:06:47,460
‫I have my files, my 3 files. These are the files.

69
00:06:47,520 --> 00:06:55,320
‫In fact these are backup configuration of my Cisco devices from previous lectures.

70
00:06:55,330 --> 00:07:03,550
‫One remark is that it has overwritten the aa.txt file so there is no warning something like:

71
00:07:03,550 --> 00:07:05,020
‫"Do you want to override the file?"

72
00:07:05,100 --> 00:07:09,380
‫so if the destination already exists it will overwrite it.

73
00:07:09,450 --> 00:07:18,750
‫If we want to copy of file from a remote location we can use the get method so scp.get the first

74
00:07:18,750 --> 00:07:20,720
‫argument is the remote path

75
00:07:20,820 --> 00:07:32,040
‫so I'll copy a file from /etc let's say passwd, the file that contains the users and the

76
00:07:32,040 --> 00:07:34,860
‫second argument is the local path,

77
00:07:34,860 --> 00:07:37,480
‫where should it copy  the file,

78
00:07:37,770 --> 00:07:43,500
‫let's say in the current directory as psswd.

79
00:07:43,500 --> 00:07:50,090
‫This could be any valid path. Let's comment these lines.

80
00:07:50,460 --> 00:07:58,000
‫And now I run the script and we can see here the file that has been copied.

81
00:07:58,020 --> 00:08:05,300
‫This is the file; it has been copied from the remote Linux server to this local machine.

82
00:08:05,340 --> 00:08:09,270
‫Of course here we can use any valid path.

83
00:08:09,300 --> 00:08:19,420
‫For example let's copy this file into another directory let's say into C users

84
00:08:19,440 --> 00:08:20,190
‫ad

85
00:08:20,190 --> 00:08:25,290
‫This is the directory where I want to copy the file to.

86
00:08:25,560 --> 00:08:28,780
‫So here I'll paste the path.

87
00:08:29,070 --> 00:08:35,700
‫One remark is that we must have permissions to write into this directory.

88
00:08:35,700 --> 00:08:44,510
‫This is my user directory so I have permissions here. And if it's an Windows path we must use two backslashes.

89
00:08:44,710 --> 00:08:53,460
‫OK, if I don't use a name here, filename it will copy as the same name but I can use here another name, for

90
00:08:53,460 --> 00:08:54,340
‫example

91
00:08:54,450 --> 00:08:59,380
‫passwd -linux.

92
00:08:59,590 --> 00:09:00,540
‫txt

93
00:09:00,570 --> 00:09:02,120
‫This is just an example.

94
00:09:02,460 --> 00:09:03,970
‫And run the script.

95
00:09:04,110 --> 00:09:09,220
‫And if I go here I can see that the file has been already copied.

96
00:09:10,960 --> 00:09:13,230
‫OK this is the file.

97
00:09:13,500 --> 00:09:23,970
‫This is how we copy files to remote locations or from remote locations using SSH and the Paramiko

98
00:09:24,060 --> 00:09:25,720
‫module in Python.


﻿1
00:00:01,280 --> 00:00:02,560
‫Welcome back!

2
00:00:02,600 --> 00:00:09,220
‫In this lecture I'll show you how to connect to networking devices and Linux servers from Python Scripts

3
00:00:09,240 --> 00:00:17,100
‫using ssh and paramiko. We’ll use this very simple topology with only one router.

4
00:00:17,240 --> 00:00:23,260
‫Even though there is another topology with more devices there should be no difference.

5
00:00:23,270 --> 00:00:30,560
‫All we need is a layer 3 connectivity and SSH access from the machine that runs the Python scripts

6
00:00:30,560 --> 00:00:31,380
‫to the device

7
00:00:31,400 --> 00:00:35,050
‫we want to configure. Before beginning

8
00:00:35,120 --> 00:00:42,390
‫I recommend you to test the network connection between the admin host and the device that runs in GNS3.

9
00:00:42,390 --> 00:00:51,050
‫So I am opening a terminal and pinging it!
‫ping and its IP address

10
00:00:51,050 --> 00:00:59,040
‫10.1.1.10 and ping is working and I also test ssh using putty.

11
00:00:59,240 --> 00:01:08,820
‫I want to be sure that I have SSH access to the device. I am opening putty and I'm writing the IP

12
00:01:08,820 --> 00:01:19,570
‫address of the router 10.1.1.10 the port is 22 and shh and open and it's working.

13
00:01:20,190 --> 00:01:22,600
‫user u1 and password

14
00:01:22,640 --> 00:01:33,360
‫cisco; if something isn't working properly you have to troubleshoot the problem and make it work.

15
00:01:35,470 --> 00:01:42,430
‫Check that the correct IP address is configured on the router's interface, that the interface is up

16
00:01:42,670 --> 00:01:43,440
‫and so on.

17
00:01:50,580 --> 00:01:55,230
‫Now let's turn Python and start creating our script.

18
00:01:55,270 --> 00:02:02,950
‫The first thing to do is to import the paramiko module then I'll create a Python object of type ssh

19
00:02:03,090 --> 00:02:03,910
‫client.

20
00:02:04,170 --> 00:02:13,830
‫You can think of it is a putty or another ssh client application so: ssh_client= paramiko.

21
00:02:14,430 --> 00:02:23,600
‫SSHClient;  if I print out its type we''ll notice it's an object paramiko.client.

22
00:02:23,690 --> 00:02:30,940
‫SSHClient class so print( type(ssh_client)) and I'm executing the script.

23
00:02:32,890 --> 00:02:42,170
‫This is the type of the object; the next step is to connect to the SSH demon that runs on the networking

24
00:02:42,170 --> 00:02:42,870
‫device

25
00:02:42,890 --> 00:02:49,190
‫in GNS3 using the SSH client we've just created. To do that

26
00:02:49,190 --> 00:02:58,300
‫I'm gonna call the Connect method so: ssh_client.connect and I'm going to use keyword arguments

27
00:02:58,600 --> 00:03:07,580
‫and that means the name of the argument equals its value; if I just write to their value without

28
00:03:07,580 --> 00:03:14,480
‫specifying the name of the argument I have to write  the arguments in the order that they have been

29
00:03:14,480 --> 00:03:20,030
‫provided in the methods definition and I don't want to memorize that order.

30
00:03:21,710 --> 00:03:32,150
‫So hostname = and the servers IP address 10.1.1.10, the second argument part

31
00:03:32,150 --> 00:03:45,310
‫=22, the default ssh port, username=u1 , passward =and the users password which

32
00:03:45,310 --> 00:03:55,040
‫is cisco, written in lowercase. And there are two more arguments and I'll write to them on the next line

33
00:03:55,700 --> 00:04:07,970
‫so I can hit to the enter key: the first argument is look_for_keys=false and allow_agent=

34
00:04:08,120 --> 00:04:08,950
‫false.

35
00:04:09,520 --> 00:04:10,440
‫Okay.

36
00:04:10,790 --> 00:04:20,630
‫There are mainly two methods to authenticate to an SSA demon: using username and password or using public

37
00:04:20,630 --> 00:04:22,210
‫key authentication.

38
00:04:22,250 --> 00:04:30,490
‫In this case we do not use public key authentication, but a user and a password, that's way

39
00:04:30,620 --> 00:04:40,090
‫I've written look_for_keys=false. We are not using keys and allow agent false we do not use

40
00:04:40,240 --> 00:04:49,120
‫the SSH agent, which is another application that keeps a decrypted, a clear text copy of the private key,

41
00:04:49,390 --> 00:04:50,920
‫in the RAM memory.

42
00:04:51,250 --> 00:04:54,780
‫So these two arguments are related.

43
00:04:55,860 --> 00:04:57,200
‫Let's tried to run the script!

44
00:05:02,200 --> 00:05:11,570
‫And we notice an error: "server and the server as IP address not found in  known hosts."

45
00:05:11,590 --> 00:05:14,570
‫What does that mean and how can we solve it?

46
00:05:15,580 --> 00:05:25,070
‫I'm sure that if you have used SSH before you already know what is this about. When an SSH client

47
00:05:25,160 --> 00:05:31,790
‫authenticates to an SSH server it uses a fingerprint to identify it.

48
00:05:31,940 --> 00:05:39,470
‫Sometimes you might have seen a warning related to the host fingerprint, either that it cannot be verified

49
00:05:39,770 --> 00:05:41,870
‫or that it has changed.

50
00:05:41,870 --> 00:05:50,150
‫Let me show you this warning in putty! I am opening putty and I'm connecting to another SSH server.

51
00:05:53,140 --> 00:05:56,360
‫And open.

52
00:05:56,470 --> 00:05:59,000
‫This is the warning I'm talking about.

53
00:05:59,290 --> 00:06:07,480
‫The host key is randomly generated when the SSH server is set up and it's used to identify the server

54
00:06:07,750 --> 00:06:09,310
‫you are connecting to.

55
00:06:09,340 --> 00:06:16,620
‫This mitigates possible Men in the Middle Attacks; in our Python script example

56
00:06:16,620 --> 00:06:21,970
‫the SSH client, which is implemented by Paramiko, cannot verify

57
00:06:21,970 --> 00:06:23,700
‫the servers host the key

58
00:06:23,760 --> 00:06:33,250
‫therefore this error. What we can do is call another method that automatically accepts the server host

59
00:06:33,250 --> 00:06:40,790
‫key, before connecting to the server ;so here before connecting

60
00:06:40,790 --> 00:06:50,560
‫I'm calling another method ssh_client.set_missing_host_key_policy and the argument is paramiko.

61
00:06:50,560 --> 00:07:01,500
‫AutoAddpPolicy and this is a function so don't forget to call it; use a pair of parentheses.

62
00:07:03,000 --> 00:07:04,380
‫In simpler words

63
00:07:04,380 --> 00:07:07,870
‫this means that it will accept the host key.

64
00:07:08,060 --> 00:07:17,000
‫I am running the script again and there is no error. If you want to check that the connection is active

65
00:07:17,000 --> 00:07:26,930
‫you can write something like this: print(ssh_client.get_transport.is_active.

66
00:07:29,550 --> 00:07:31,860
‫And it will return true or false:

67
00:07:34,660 --> 00:07:39,480
‫true ; so the connection is active. At this point

68
00:07:39,620 --> 00:07:45,440
‫we can send commands to the device through the SSH connection to execute them.

69
00:07:48,940 --> 00:07:56,160
‫However we'll see how to do it in detail in the next lecture. Before exiting the script

70
00:07:56,170 --> 00:08:06,270
‫it's a good practice to close the client by calling the close method so ssh_client .close. And

71
00:08:06,270 --> 00:08:14,040
‫one last thing I want to do is to print out some messages so that the user knows what's happening.

72
00:08:17,470 --> 00:08:24,340
‫print connecting to and the ssh demon IP address

73
00:08:27,210 --> 00:08:27,660
‫and here

74
00:08:27,660 --> 00:08:30,120
‫print ('closing connection')

75
00:08:34,960 --> 00:08:38,350
‫I'm running the script again to check that there is no error.

76
00:08:41,340 --> 00:08:42,150
‫Okay!

77
00:08:42,180 --> 00:08:49,470
‫This script does nothing useful for the moment, it simply connects successfully to the device and then

78
00:08:49,470 --> 00:08:57,420
‫disconnects. In the next lecture will add more functionality and that means executing commands on the

79
00:08:57,420 --> 00:08:58,800
‫remote devices.


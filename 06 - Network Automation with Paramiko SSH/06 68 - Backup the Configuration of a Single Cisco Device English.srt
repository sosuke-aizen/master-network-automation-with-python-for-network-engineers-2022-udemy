﻿1
00:00:01,610 --> 00:00:04,470
‫Now I want to show you something really useful.

2
00:00:04,580 --> 00:00:10,790
‫I want to show you how to automate the configuration backup of a Cisco networking device.

3
00:00:10,790 --> 00:00:12,720
‫This is very important and it's

4
00:00:12,800 --> 00:00:18,420
‫one of the first things a network admin should do. In case of a failure

5
00:00:18,500 --> 00:00:24,510
‫you can simply restore the backup in just a few seconds. In this lab

6
00:00:24,580 --> 00:00:30,690
‫I'll show you how to back up the running configuration of one router and in a following lecture

7
00:00:31,030 --> 00:00:38,660
‫I'll show you how to backup the configuration of multiple routers like the routers in this topology.

8
00:00:38,780 --> 00:00:44,990
‫I've already done the script to save time and I'm gonna explain in detail what it does.

9
00:00:45,140 --> 00:00:47,060
‫In fact there is nothing new.

10
00:00:47,240 --> 00:00:51,420
‫I've already explained all the concepts in the last lectures.

11
00:00:52,350 --> 00:01:00,060
‫I've imported the module we've developed in the last lectures and I've used the functions defined inside

12
00:01:01,290 --> 00:01:07,020
‫I've defined the diction and I get that stores information about the device like its IP address,

13
00:01:07,150 --> 00:01:15,390
‫the SSH port and the credentials. And I am connecting to the device, getting a client and  the shell objects

14
00:01:17,320 --> 00:01:24,940
‫Then I'll start sending commands, the terminal length zero command to display the entire output at once,

15
00:01:25,250 --> 00:01:29,650
‫the enable command to enter the enable or admin mode,

16
00:01:29,920 --> 00:01:31,770
‫then the  enable password

17
00:01:31,870 --> 00:01:38,790
‫and finally the show run command which displays the running configuration of the router. I am taking

18
00:01:38,850 --> 00:01:48,700
‫the output in a variable of type String and printing it out and before exiting I am closing the connection.

19
00:01:48,720 --> 00:01:50,470
‫Okay, let's run the script!

20
00:01:51,790 --> 00:02:00,040
‫We see how it's sending the commands and displaying the output of show running config. What I want to

21
00:02:00,040 --> 00:02:07,050
‫do next is save the output into a file but I am not going to save the entire output,

22
00:02:07,150 --> 00:02:11,960
‫only the IOS commands that need to be executed on the router

23
00:02:11,980 --> 00:02:17,260
‫in case you want to restore the data. I do not want to save this part.

24
00:02:19,920 --> 00:02:28,470
‫Okay the commands, the password string, show run, building configuration and so on!

25
00:02:29,490 --> 00:02:32,660
‫In fact I want to save it from this line.

26
00:02:33,440 --> 00:02:35,490
‫Version 15.5.

27
00:02:35,510 --> 00:02:39,830
‫This is in fact the first command. When I want to restore it

28
00:02:39,830 --> 00:02:43,460
‫I'll copy and paste the commands into the router config, like this:

29
00:02:45,220 --> 00:02:45,880
‫Copy

30
00:02:52,060 --> 00:02:53,970
‫and into router's console

31
00:02:54,000 --> 00:03:01,270
‫I am pasting them, but in global configuration mode so configure terminal and I'm pasting the commands.

32
00:03:02,500 --> 00:03:13,400
‫Ok, let's see how to remove the first lines and the last line of the output, this one, the router's hostname,

33
00:03:15,070 --> 00:03:16,710
‫And it's very simple.

34
00:03:16,710 --> 00:03:23,360
‫I'm creating a Python list from the output string by calling the split lines method and then I'll

35
00:03:23,400 --> 00:03:26,970
‫remove some elements from the list using a list slicing

36
00:03:30,110 --> 00:03:33,770
‫so output_list = output

37
00:03:33,830 --> 00:03:35,480
‫.splitlines()

38
00:03:39,130 --> 00:03:40,270
‫Let's see its contents!

39
00:03:50,430 --> 00:03:51,170
‫Okay!

40
00:03:51,220 --> 00:03:58,200
‫This is the output as a list. I'll remove the first nine elements.

41
00:03:58,290 --> 00:04:09,610
‫One two three four five six seven eight nine even ten.

42
00:04:10,000 --> 00:04:22,880
‫So the first 10 elements! output_list = output_list[10:-1]

43
00:04:22,910 --> 00:04:31,140
‫I'm taking all list elements from index 10 included until index -1, which is the last element,

44
00:04:31,140 --> 00:04:36,290
‫excluded; this is how lists slicing works.

45
00:04:36,480 --> 00:04:37,350
‫Let's see the list!

46
00:04:46,270 --> 00:04:47,390
‫This is a comment

47
00:04:47,560 --> 00:04:53,980
‫so if I want I can start from the 11h element, like this.

48
00:04:59,370 --> 00:04:59,840
‫OK.

49
00:04:59,900 --> 00:05:03,740
‫It's better! The next step is to create the string.

50
00:05:03,740 --> 00:05:11,140
‫I'm going to save to the file from the list ; we can write a string and not a Python list to a file. To get

51
00:05:11,140 --> 00:05:12,310
‫a string from our list

52
00:05:12,310 --> 00:05:24,230
‫I'll use the join method so: output = '\n'.join(output_list)

53
00:05:24,230 --> 00:05:25,120
‫It

54
00:05:25,130 --> 00:05:31,400
‫will join the elements of the list putting a backslash n between them.

55
00:05:31,410 --> 00:05:32,840
‫Let's see the output!

56
00:05:40,080 --> 00:05:40,750
‫okay!

57
00:05:40,780 --> 00:05:48,360
‫The last line is not there anymore and it starts with version 15.

58
00:05:48,360 --> 00:05:48,960
‫.5

59
00:05:52,030 --> 00:05:55,410
‫The last step is to save the output to a file,

60
00:05:55,450 --> 00:06:02,980
‫the backup file. So with open and the name of the file let's se 'router1-backup.txt',

61
00:06:03,020 --> 00:06:08,400
‫I'm opening the file in write mode

62
00:06:08,580 --> 00:06:16,560
‫and that means it will create a file as if f .right and output

63
00:06:20,090 --> 00:06:27,810
‫I'm running the script; a new file called router1- backup.txt will be created in

64
00:06:27,810 --> 00:06:29,860
‫the current working directory.

65
00:06:29,880 --> 00:06:36,250
‫This is the file! You see it's the router's configuration backup!

66
00:06:36,550 --> 00:06:38,700
‫This is good, but not perfect.

67
00:06:39,250 --> 00:06:46,720
‫If I return tomorrow and run the script again it will overwrite the file and that's not good.

68
00:06:46,890 --> 00:06:50,420
‫Normally, you want to keep versions of your backups.

69
00:06:50,490 --> 00:06:58,210
‫For example you want to back up your configuration once a day and keep one month of backups. This way

70
00:06:58,300 --> 00:07:04,300
‫you can see the configuration changes that were made over time or can restore the configuration from

71
00:07:04,300 --> 00:07:05,340
‫a specific date

72
00:07:05,350 --> 00:07:12,620
‫In the past. So, as a conclusion, we need the date and time in the file name.

73
00:07:13,030 --> 00:07:14,020
‫Let's see how to do it!

74
00:07:14,560 --> 00:07:24,550
‫So from datetime import datetime, date time is the module
‫and inside it there is another class called

75
00:07:24,930 --> 00:07:36,810
‫daytime and now the current date and time = datetime.now and  year=now.year

76
00:07:37,000 --> 00:07:43,790
‫this is the current year; month=now .month day=

77
00:07:43,870 --> 00:07:46,430
‫now.day

78
00:07:47,920 --> 00:07:51,940
‫and if you wan the hour and the minute hour=

79
00:07:51,940 --> 00:07:52,930
‫now.hour

80
00:07:55,790 --> 00:07:58,200
‫and I'm creating the file name.

81
00:07:58,200 --> 00:08:06,720
‫So file_name= and an F string literal; the name of the file will start with the router's

82
00:08:06,900 --> 00:08:13,370
‫IP address so router of server IP between curly braces

83
00:08:16,780 --> 00:08:26,930
‫un underline year month and day and of course .txt

84
00:08:27,680 --> 00:08:31,590
‫I will not use the hour and the minute anymore, but you can do it.

85
00:08:31,700 --> 00:08:32,690
‫Let's see the file name!

86
00:08:32,900 --> 00:08:34,070
‫So print file name

87
00:08:35,140 --> 00:08:36,560
‫Let's see what is its content!

88
00:08:43,060 --> 00:08:43,850
‫Perfect!

89
00:08:44,020 --> 00:08:55,040
‫The name of the device, the year, the month and the date so instead of this string value I'll use the variable

90
00:08:55,040 --> 00:09:03,480
‫called final name. Take care not to put this between quotes because it will not be a variable, but a string

91
00:09:03,600 --> 00:09:05,210
‫and that's not good.

92
00:09:05,230 --> 00:09:13,980
‫It's a variable that already contains a string, like this. I'm running the script again.

93
00:09:14,070 --> 00:09:14,520
‫Look here!

94
00:09:19,610 --> 00:09:25,880
‫We notice how the urouter's configuration backup was saved in this file, in the current directory.

95
00:09:29,820 --> 00:09:36,090
‫When you want to restore it you simply copy and paste the file contents into the router global configuration

96
00:09:36,090 --> 00:09:38,310
‫mode; okay!

97
00:09:38,370 --> 00:09:44,680
‫That's all! You know how to back up the configuration of a Cisco device! In the next lecture I'll show

98
00:09:44,680 --> 00:09:50,080
‫you how to back up the configuration of multiple devices! See you in a second!


﻿1
00:00:01,180 --> 00:00:08,810
‫Let's move on and see how to use the functions we've just defined. The first step is to connect to the

2
00:00:08,810 --> 00:00:11,420
‫remote a device using ssh,

3
00:00:11,450 --> 00:00:13,450
‫so I'm gonna call the connect function.

4
00:00:14,300 --> 00:00:18,560
‫So client= the function returns a client object.=

5
00:00:18,560 --> 00:00:23,350
‫connect, the first argument is the server's IP address,

6
00:00:23,580 --> 00:00:32,020
‫10.1.1.10 the port, the default 22, username u1 and password

7
00:00:32,090 --> 00:00:41,040
‫cisco. Then I'll create a shell object by calling the get shall function. this one!

8
00:00:41,500 --> 00:00:51,760
‫So shell=get_shell and the function takes as argument the ssh_client.

9
00:00:51,770 --> 00:00:54,410
‫It's called client, just client.

10
00:00:54,530 --> 00:00:57,590
‫You can call a variable any way you want.

11
00:00:58,070 --> 00:01:05,560
‫And at this point I can simply start sending commands so send_command,

12
00:01:05,750 --> 00:01:14,000
‫the first argument is the shell variable, into which the command will be send, and the first command will

13
00:01:14,000 --> 00:01:23,640
‫be enable; the second command will be in fact the password, the enable password cisco,

14
00:01:28,810 --> 00:01:30,870
‫then terminal length zero

15
00:01:38,610 --> 00:01:41,460
‫and 2 show commands, for example show version

16
00:01:45,300 --> 00:01:46,580
‫and show IP interface

17
00:01:46,590 --> 00:02:00,810
‫brief. I've sent five  commands and now I want to print out the output so output=show(shell)

18
00:02:02,130 --> 00:02:06,600
‫and print (output). And I'm running the script!

19
00:02:11,630 --> 00:02:12,380
‫It's working!

20
00:02:15,710 --> 00:02:23,620
‫This is the output: the output of the first comment, the show version command and the output of the second

21
00:02:23,620 --> 00:02:27,180
‫command which is the show_ip interface brief. Perfect!

22
00:02:27,430 --> 00:02:29,450
‫It's working as expected.

23
00:02:29,630 --> 00:02:36,210
‫I'm sure you realize it's much simpler to work this way using these functions.

24
00:02:36,400 --> 00:02:44,710
‫You don't have to focus on all details. Moreover if someone that is not an effective Python programmer,

25
00:02:44,950 --> 00:02:49,320
‫but  a good the networking engineer, wants to automate his daily tasks

26
00:02:49,480 --> 00:02:52,460
‫he simply has to learn how to use the functions.

27
00:02:52,480 --> 00:02:58,800
‫It's not necessary for him to dive deep in total details like like \n after each command, timeouts,

28
00:02:58,810 --> 00:03:02,210
‫decoding from bytes to string and so on.

29
00:03:02,320 --> 00:03:03,620
‫One last remark!

30
00:03:04,910 --> 00:03:12,470
‫If you want to save the information about the device in a dictionary you can do it. Maybe you want to

31
00:03:12,470 --> 00:03:18,690
‫keep these values connected and use them also in other parts of the script.

32
00:03:18,860 --> 00:03:27,980
‫So let's try something like this: router1= and in fact I'll copy and paste a line from

33
00:03:27,980 --> 00:03:30,030
‫another script, from a previous one.

34
00:03:30,260 --> 00:03:34,580
‫I want to save some time, this line of code.

35
00:03:41,010 --> 00:03:49,920
‫And when calling the connect function, instead of giving these 4 arguments, I'll simply write **

36
00:03:50,180 --> 00:03:58,140
‫router1; it's the same. I have already explained in another lecture how this works.

37
00:04:01,000 --> 00:04:01,530
‫Okay!

38
00:04:01,590 --> 00:04:04,480
‫There is one small change required.

39
00:04:04,720 --> 00:04:09,730
‫The name of the arguments, in my case are server IP and server port,

40
00:04:09,760 --> 00:04:13,580
‫so I have to change the name of the dictionary's keys.

41
00:04:13,660 --> 00:04:15,790
‫So here we have server_ip

42
00:04:19,880 --> 00:04:20,720
‫server_port

43
00:04:24,820 --> 00:04:32,060
‫here itd user and here passwd

44
00:04:32,280 --> 00:04:39,190
‫This is how I call the arguments; they need to have the same names as the dictionary's keys.

45
00:04:39,480 --> 00:04:42,850
‫I'm running the script again and it's working.

46
00:04:42,970 --> 00:04:45,360
‫It's sending the commands to the device!


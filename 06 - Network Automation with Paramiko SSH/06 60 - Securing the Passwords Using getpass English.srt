﻿1
00:00:01,920 --> 00:00:08,850
‫In the last lecture we've developed this simple Python script that connects to a Cisco device using

2
00:00:08,850 --> 00:00:12,460
‫SSH, executes some commands and displays

3
00:00:12,480 --> 00:00:19,600
‫the output. One of the problems of this script is that the password is hardcoded.

4
00:00:19,650 --> 00:00:26,850
‫Anyone that has access to the script sees the password and it's not acceptable at all.

5
00:00:26,850 --> 00:00:33,060
‫Instead of writing the password inside the script we could prompt the user that runs the script

6
00:00:33,060 --> 00:00:38,180
‫for the password in a secure manner. Let's modify the script!

7
00:00:38,690 --> 00:00:49,610
‫I'm importing the get pass module: impor_getpass and I'm prompting the user for the password without

8
00:00:49,640 --> 00:01:02,940
‫echoing, so: password= getpass.getpass and a string as argument, for example enter password

9
00:01:05,220 --> 00:01:11,160
‫The user will enter the password which will be saved in the password variable.

10
00:01:11,340 --> 00:01:17,580
‫So here instead of the hardcoded password I'll use the password variable.

11
00:01:18,730 --> 00:01:25,820
‫So password, this is the dictionary key, -:password

12
00:01:25,990 --> 00:01:30,650
‫the variable I've just defined.

13
00:01:30,710 --> 00:01:39,230
‫Now if you run the script in PyCharm it won't work. PyCharm uses a modified console which is incompatible

14
00:01:39,320 --> 00:01:44,470
‫with the getpass module.

15
00:01:44,640 --> 00:01:48,780
‫It doesn't work,it doesn't display a thing.

16
00:01:49,170 --> 00:01:58,190
‫So you have to run the script in terminal. Let's how to do it ! I'm clicking with the right mouse button

17
00:01:58,340 --> 00:02:00,990
‫on the directory that contains the file,

18
00:02:01,040 --> 00:02:12,390
‫in this case network automation, copy and absolute path and then in a terminal I'll cd to that directory; I'll

19
00:02:12,390 --> 00:02:18,100
‫move into that directory so cd and paste.

20
00:02:18,180 --> 00:02:22,520
‫So this is the directory that contains the Python script.

21
00:02:22,620 --> 00:02:28,100
‫You can see its content by executing dir.

22
00:02:28,210 --> 00:02:33,400
‫This is our script and to execute the script in terminal

23
00:02:33,400 --> 00:02:34,700
‫you write Python,

24
00:02:34,900 --> 00:02:40,960
‫if you are executing the script on Linux probably you have to write Python 3 and the name of the

25
00:02:40,960 --> 00:02:45,190
‫script paramiko_execute_commands_cisco_getpass

26
00:02:45,220 --> 00:02:46,290
‫It's a long name.

27
00:02:46,330 --> 00:02:56,730
‫It doesn't matter; copy and paste and it's prompting for the password.

28
00:02:56,750 --> 00:02:57,770
‫This line,

29
00:02:57,770 --> 00:02:59,650
‫line 7 in the script!

30
00:02:59,660 --> 00:03:10,760
‫Enter password and I'm entering the password which is Cisco. I've entered the password but it didn't

31
00:03:10,850 --> 00:03:21,410
‫display a thing; it's a secure way of getting passwords and I'm hitting enter and you see the output!

32
00:03:26,180 --> 00:03:26,890
‫OK.

33
00:03:26,910 --> 00:03:34,200
‫That's how you enter passwords in a secure manner instead of hard coding them in scripts.


﻿1
00:00:01,430 --> 00:00:02,740
‫Hello guys!

2
00:00:02,780 --> 00:00:09,160
‫In the previous lectures I've shown you how to  the paramiko module which implements SSH to automate

3
00:00:09,160 --> 00:00:13,970
‫simple configuration tasks on Cisco devices or on Linux servers.

4
00:00:17,920 --> 00:00:24,850
‫Paramiko is a very nice and useful Python module but it can be complicated at least for beginner

5
00:00:24,850 --> 00:00:26,350
‫programmers.

6
00:00:26,350 --> 00:00:32,090
‫If someone is not a Python expert he can have some difficulties using Paramiko.

7
00:00:32,620 --> 00:00:35,640
‫He has to set the missing host key policy,

8
00:00:35,860 --> 00:00:44,620
‫he must add a \n after each command, pause the script execution after the command, decode from objects of type

9
00:00:44,710 --> 00:00:47,560
‫bytes to string and so on.

10
00:00:47,700 --> 00:00:53,800
‫A network Engineer that wants to automate a simple task can easily get  stuck.

11
00:00:53,800 --> 00:01:01,210
‫What I do now is refactor the previous scripts in such a way that even if someone that's not a pPython

12
00:01:01,210 --> 00:01:05,860
‫expert can use Python and pParamiko to automate its daily tasks.

13
00:01:07,270 --> 00:01:11,840
‫So I'll create a new Python script which is in fact a Python module.

14
00:01:11,860 --> 00:01:20,320
‫Remember that a Python script is also a module that can be imported in other scripts and there I'll define

15
00:01:20,320 --> 00:01:24,510
‫some functions that hide the complexity.

16
00:01:24,560 --> 00:01:27,940
‫I'll call the new Python script myparamiko.

17
00:01:28,880 --> 00:01:36,020
‫After defining the function you'll simply call the functions without going into all details.

18
00:01:36,110 --> 00:01:42,860
‫I'll follow the same logic from the scripts we've already developed and the first function will be connect.

19
00:01:45,130 --> 00:01:55,100
‫So I'm imparting the module paramiko and also time; they will be used in the script and def connect

20
00:01:55,250 --> 00:02:03,030
‫the first function; in order to connect to a device you need its IP address,

21
00:02:03,030 --> 00:02:06,030
‫its port, username and password.

22
00:02:08,790 --> 00:02:15,600
‫So the first argument will be server IP, server port, user and password.

23
00:02:19,510 --> 00:02:23,940
‫And I am copying and pasting in the new script

24
00:02:24,070 --> 00:02:27,290
‫these lines of code, these two lines

25
00:02:30,630 --> 00:02:33,510
‫and the connection part, these two lines.

26
00:02:38,060 --> 00:02:42,710
‫Here instead of router of hostname I have server IP,

27
00:02:42,770 --> 00:02:52,280
‫the functions argument. And I'll also modified the connect function so hostname=server_ip

28
00:02:54,910 --> 00:03:05,560
‫port=server_port, username=user and password =

29
00:03:05,680 --> 00:03:06,760
‫passwd

30
00:03:10,130 --> 00:03:10,630
‫perfect.

31
00:03:11,290 --> 00:03:16,850
‫And I am  returning the ssh_client because otherwise I lose it.

32
00:03:16,850 --> 00:03:23,750
‫If you want to use a local variable outside the function in which it's defined you have to return it.

33
00:03:24,160 --> 00:03:26,840
‫So return ssh_

34
00:03:26,880 --> 00:03:28,210
‫client.

35
00:03:28,430 --> 00:03:34,190
‫The second function is of course get shell; having the ssh_client

36
00:03:34,190 --> 00:03:35,290
‫it will return the shell.

37
00:03:40,260 --> 00:03:52,660
‫def get_shell
‫it has one parameter the ssh_client and it's returning the shell object shell=

38
00:03:53,200 --> 00:03:56,290
‫ssh_client.invoke_shell

39
00:03:59,940 --> 00:04:02,250
‫and of course return shell.

40
00:04:02,370 --> 00:04:11,310
‫I want to use this variable outside the function as well. And the third function is send command.

41
00:04:11,680 --> 00:04:16,610
‫It will send a command through ssh to the remote device to be executed.

42
00:04:17,780 --> 00:04:19,750
‫def send command

43
00:04:22,740 --> 00:04:28,830
‫the shell, the comment and the timeout.

44
00:04:28,890 --> 00:04:39,010
‫This will be a default argument, lets say its default value is 1; and shell.send command

45
00:04:39,850 --> 00:04:41,700
‫+\n

46
00:04:41,900 --> 00:04:52,960
‫I'm concatenating a \n  character to the command and time.sleep of timeout and that's all. The

47
00:04:52,960 --> 00:04:55,020
‫next function will be show.

48
00:04:55,030 --> 00:05:04,680
‫It will simply return the output from shell’s buffer;
‫def show, it takes in the shell and

49
00:05:04,680 --> 00:05:06,760
‫how many bytes to read and return,

50
00:05:06,900 --> 00:05:18,470
‫let's say 10000 output=shell.recv of n. And return output.decode.

51
00:05:18,990 --> 00:05:22,590
‫I'm returning the string directly, not the byte's object.

52
00:05:23,490 --> 00:05:25,880
‫And the last function will be close,

53
00:05:26,100 --> 00:05:29,100
‫which of course closes the connection to the server.

54
00:05:29,130 --> 00:05:35,560
‫So def close(ssh_client);
‫I am printing a message,

55
00:05:36,380 --> 00:05:37,670
‫closing the connection

56
00:05:40,640 --> 00:05:44,510
‫and I'm closing the connection only if it's active.

57
00:05:44,510 --> 00:05:47,790
‫In fact I copy paste this part from here.

58
00:05:52,080 --> 00:05:58,480
‫I'll also print out the message when sending the command, so print, an F string literal,

59
00:05:58,900 --> 00:06:07,890
‫sending command: and the command variable between curly braces; okay, that's all!

60
00:06:08,040 --> 00:06:15,360
‫It's not so complicated; in fact I've just copied and pasted the code we had already written in some

61
00:06:15,360 --> 00:06:16,430
‫functions.

62
00:06:16,440 --> 00:06:20,980
‫Remember that the main purpose of a function is to not repeat code.

63
00:06:21,210 --> 00:06:24,030
‫So in the future we'll just call the functions

64
00:06:24,030 --> 00:06:30,020
‫each time we want to connect to a device instead of effectively writing that Python code.

65
00:06:30,120 --> 00:06:35,280
‫This is more efficient and there is no risk of making mistakes.

66
00:06:35,280 --> 00:06:40,770
‫We'll take a short break and in the next lectures I'll show you how to use these functions.


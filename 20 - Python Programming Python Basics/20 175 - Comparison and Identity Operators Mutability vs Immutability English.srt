﻿1
00:00:00,300 --> 00:00:07,860
‫In this lecture, we'll talk about the Python comparison operators, comparison operators are used to

2
00:00:07,860 --> 00:00:13,950
‫compare to values and it either returns true or false according to the condition.

3
00:00:14,950 --> 00:00:24,880
‫The first comparison operator is equal to or to equal signs it returns true if both operations are equal.

4
00:00:26,140 --> 00:00:31,480
‫The openings are the values that the operator operates on.

5
00:00:32,580 --> 00:00:35,070
‫So if I have two plus three.

6
00:00:36,470 --> 00:00:45,050
‫Here are the plus sign is the operator that performs additions, two and three are the operations and

7
00:00:45,050 --> 00:00:48,320
‫five is the output of the operation.

8
00:00:49,660 --> 00:00:57,820
‫Now back to our equal two operator, let's write something like this to two equal signs and to end it

9
00:00:58,330 --> 00:01:01,470
‫to because two is equal to two.

10
00:01:01,990 --> 00:01:03,070
‫But if I write.

11
00:01:04,120 --> 00:01:15,610
‫A equals five, B equals six, A double equals minus B, E return false because A is not equal.

12
00:01:15,610 --> 00:01:19,300
‫To be the same is valid also for things.

13
00:01:19,690 --> 00:01:23,860
‫So if I have a string, let's say name equal to 10.

14
00:01:24,880 --> 00:01:35,170
‫And another thing, Lexy, address equals London, if I write name double equals signs address, I'll

15
00:01:35,170 --> 00:01:36,310
‫get false.

16
00:01:37,660 --> 00:01:40,060
‫Then we have not equal to.

17
00:01:40,920 --> 00:01:45,060
‫We use an exclamation mark and the equals sign.

18
00:01:45,540 --> 00:01:46,680
‫Let's try an example.

19
00:01:49,450 --> 00:01:57,610
‫So A, not equal to B, and the result is true because A is not equal to be the same for name, not

20
00:01:57,610 --> 00:01:59,170
‫equal to address.

21
00:02:00,450 --> 00:02:08,140
‫Another operator is greater when it returns, true, if the left open is greater than the right.

22
00:02:08,670 --> 00:02:11,460
‫So let's say A greater than B.

23
00:02:12,790 --> 00:02:22,360
‫Falsey is not greater than B, but A is less than B, let's have another variable, C equals five.

24
00:02:23,420 --> 00:02:33,260
‫In this case, a greater than or equal to see returns, true, because A is five and C is five, so

25
00:02:33,260 --> 00:02:39,710
‫A is greater than or equal to see a less than or equal to see a return.

26
00:02:39,740 --> 00:02:48,350
‫Also true, but a greater than B false vex because A is not greater than or equal to B.

27
00:02:50,270 --> 00:02:53,600
‫These are Python comparison operators.

28
00:02:53,990 --> 00:02:57,210
‫Now let's take a look at identity operators.

29
00:02:57,950 --> 00:03:01,250
‫There are is and is not operators.

30
00:03:01,670 --> 00:03:09,360
‫They don't compare the values stored in variables, but the memory address referenced by the variables.

31
00:03:10,250 --> 00:03:19,010
‫So if we write a ESB, it will return true if both variables are saved at the same memory address and

32
00:03:19,010 --> 00:03:20,270
‫false otherwise.

33
00:03:20,900 --> 00:03:28,480
‫Before trying some examples with identity operators, let's talk about the mutability and immutability.

34
00:03:29,360 --> 00:03:31,730
‫Let's dive into some details.

35
00:03:32,210 --> 00:03:41,120
‫The value of a mutable variable can be changed after it has been created, but the value of an immutable

36
00:03:41,120 --> 00:03:43,220
‫variable cannot be changed.

37
00:03:43,490 --> 00:03:50,360
‫If we try to change the value of an immutable variable, Python will create, in fact, a new variable

38
00:03:50,480 --> 00:03:53,150
‫that stauss the changed the value.

39
00:03:54,760 --> 00:04:04,480
‫In Texas, float's string's topless photos of sex are all immutable variables, and this means that

40
00:04:04,510 --> 00:04:05,920
‫they can't be changed.

41
00:04:06,310 --> 00:04:11,710
‫Leasebacks, sex and dictionaries are immutable and they can be changed.

42
00:04:12,370 --> 00:04:15,650
‫Let's see some examples here.

43
00:04:15,970 --> 00:04:22,990
‫I create an integer variable of the variable and a string variable.

44
00:04:26,440 --> 00:04:33,170
‫All these variables are immutable, their value can't be changed.

45
00:04:33,380 --> 00:04:37,420
‫Lexi, what is the address where the variable is stored?

46
00:04:37,630 --> 00:04:39,210
‫This is the address.

47
00:04:39,850 --> 00:04:41,650
‫This is the address of B..

48
00:04:42,220 --> 00:04:45,370
‫And this is the address of name.

49
00:04:46,290 --> 00:04:50,790
‫And now let's try to change them a equals to eight.

50
00:04:53,780 --> 00:05:02,870
‫As you can see, there is another address, in fact, this is another variable, the old a variable

51
00:05:03,020 --> 00:05:11,060
‫had this address and the venue, a variable has this address, another address, and the same is for

52
00:05:11,060 --> 00:05:17,380
‫float B plus equal to for idea of B, and we got this value.

53
00:05:17,900 --> 00:05:20,230
‫We can see there is another value.

54
00:05:20,390 --> 00:05:24,760
‫The new address of B is different from this value.

55
00:05:25,460 --> 00:05:31,430
‫If I want to modify the name variable, let's say name equals then.

56
00:05:32,720 --> 00:05:37,100
‫We can see that it is saved at another location.

57
00:05:38,640 --> 00:05:42,540
‫We can see how this value is different from this value.

58
00:05:43,870 --> 00:05:53,320
‫So the value of an integer float or string variable can't be changed, but let's take at least for the

59
00:05:53,320 --> 00:06:00,950
‫moment, it's enough to know that a list is a sequence of altered objects, any kind of objects.

60
00:06:01,660 --> 00:06:03,990
‫So let's say names equals.

61
00:06:04,000 --> 00:06:05,710
‫And I have here three names.

62
00:06:07,220 --> 00:06:12,590
‫I use square brackets and between elements, I have coma's.

63
00:06:20,310 --> 00:06:28,140
‫This is a list and this is its address, and now I'll create another list using the copy function,

64
00:06:28,920 --> 00:06:32,010
‫this function will create a copy of a list.

65
00:06:32,320 --> 00:06:38,100
‫So let's say new names equals name that copy.

66
00:06:40,530 --> 00:06:42,600
‫Sorry, I have here names.

67
00:06:44,540 --> 00:06:49,490
‫This is the name Celeste, and this is the new names list.

68
00:06:50,600 --> 00:07:01,280
‫As you can see, they contain the same values, so if I write names is equal to new names, I'll obtain

69
00:07:01,280 --> 00:07:04,990
‫two because they have the same values.

70
00:07:05,480 --> 00:07:10,340
‫But if I write names, these new names.

71
00:07:12,040 --> 00:07:21,130
‫I've got the list because they are different variables or different objects, they are saved at different

72
00:07:21,130 --> 00:07:21,970
‫addresses.

73
00:07:28,210 --> 00:07:36,460
‫Now, let's see the last example of this lecture I have when you least named languages with four elements.

74
00:07:37,090 --> 00:07:41,830
‫This is our list, ecocide I.D., this value.

75
00:07:43,250 --> 00:07:47,330
‫And I want to add a new element to this list.

76
00:07:50,090 --> 00:07:56,760
‫If we want to add another element to a list, the append method is used.

77
00:07:57,320 --> 00:08:00,110
‫So here I write to say JavaScript.

78
00:08:02,820 --> 00:08:09,390
‫This is my list one more time, we can see the new element and now let's check its address.

79
00:08:11,580 --> 00:08:20,220
‫As you can see, it has the same address, even if I've added a new element to this, because lists

80
00:08:20,220 --> 00:08:22,290
‫are mutable objects.

81
00:08:23,040 --> 00:08:30,550
‫There are also other types of operators, but we'll discuss in detail about them in other lectures.

82
00:08:30,850 --> 00:08:36,480
‫They are logical operators which are and or and not membership operators.

83
00:08:36,660 --> 00:08:38,940
‫We are in and not in.

84
00:08:39,150 --> 00:08:48,390
‫And bitwise operators used to perform bitwise operations on binary numbers that involve the manipulation

85
00:08:48,390 --> 00:08:50,070
‫of individual bits.


﻿1
00:00:01,180 --> 00:00:08,470
‫In this lecture, we'll take a look at the comics in Python when working with any programming language,

2
00:00:08,470 --> 00:00:15,460
‫you include comics in the code to notate your work, these documents, what certain parts of the code

3
00:00:15,460 --> 00:00:23,230
‫are for, and the other developers, you included, know what you intended to do when you wrote the

4
00:00:23,230 --> 00:00:23,650
‫code.

5
00:00:24,010 --> 00:00:31,010
‫This is a very good practice and the good developers make heavy use of comics without them.

6
00:00:31,180 --> 00:00:33,420
‫Things can get really confusing.

7
00:00:35,130 --> 00:00:42,840
‫Comics in Python started with the harsh character and extent to which the end of the physical line,

8
00:00:43,440 --> 00:00:51,680
‫a comment may appear at the start of a line or a following white space or code, but not within a string,

9
00:00:51,690 --> 00:00:55,440
‫literal, a harsh character within a string.

10
00:00:55,440 --> 00:00:57,900
‫Literal is just the harsh character.

11
00:00:58,320 --> 00:01:03,420
‫Comics are to clarify code and are not interpreted by Python.

12
00:01:04,990 --> 00:01:06,190
‫Let's see example.

13
00:01:06,970 --> 00:01:08,530
‫This line is a comment.

14
00:01:12,450 --> 00:01:17,550
‫Let's try another comment, a equals for.

15
00:01:18,830 --> 00:01:25,490
‫The entire line is also a comment, and that's because it starts with a hash sign.

16
00:01:26,800 --> 00:01:35,470
‫If I write X equals nine here, I am creating a new variable, but after that I am starting a comment.

17
00:01:40,760 --> 00:01:47,770
‫In other programming languages, there are ways to comment an entire block of code here in Python,

18
00:01:47,780 --> 00:01:51,890
‫if you want to comment multiple lines, you prefix each line.

19
00:01:51,890 --> 00:01:54,230
‫You want to comment with a hash sign.

20
00:01:55,570 --> 00:02:04,630
‫In an idea like by charm, you can use a shortcut and viks control, plus forget to comment about an

21
00:02:04,630 --> 00:02:10,240
‫entire block of code by commenting each individual line or for that block of code.

22
00:02:11,720 --> 00:02:19,750
‫So if I want to comment out of these three lines, I can write a character in front of instruction,

23
00:02:20,120 --> 00:02:27,020
‫so at the beginning of the line, or I can use a shortcut for that, I am selecting the code.

24
00:02:27,020 --> 00:02:35,900
‫I want to comment and I'm pressing gun control and forward slash and it come into doubt the entire block

25
00:02:35,900 --> 00:02:36,380
‫of code.

26
00:02:37,130 --> 00:02:44,270
‫If I want to uncommented this block of code, I am selecting the block of code and I am pressing one

27
00:02:44,270 --> 00:02:51,170
‫more time on control and forward slash and the block of code has been uncommented.

28
00:02:52,210 --> 00:02:54,860
‫This is all about comics in Python.

29
00:02:55,000 --> 00:02:55,630
‫Thank you.


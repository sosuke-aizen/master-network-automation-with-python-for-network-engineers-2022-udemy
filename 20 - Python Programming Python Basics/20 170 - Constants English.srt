﻿1
00:00:01,370 --> 00:00:07,790
‫If you are coming from other programming languages, maybe you wonder, how do we define a constant

2
00:00:07,790 --> 00:00:15,500
‫in Python, there are values in the real world that will never change the mathematical constant.

3
00:00:15,500 --> 00:00:26,210
‫Pi with three decimal places will always be three point one for one a day, will always have 24 hours.

4
00:00:26,480 --> 00:00:36,240
‫And the boiling point of water at sea level will always be 180 degrees Celsius or 212 degrees Fahrenheit.

5
00:00:36,890 --> 00:00:40,340
‫These variables are known as constants.

6
00:00:41,000 --> 00:00:45,920
‫Python does not have anything in the language to declare a constant.

7
00:00:46,280 --> 00:00:55,430
‫A constant is declarative like any other variable using its name, the equals sign and the value in

8
00:00:55,430 --> 00:00:56,000
‫general.

9
00:00:56,210 --> 00:01:02,600
‫Constant values are often written with uppercase letters to indicate that.

10
00:01:03,740 --> 00:01:10,400
‫Let's declare some constants by equals three point one four two.

11
00:01:12,770 --> 00:01:16,520
‫Hours in a day equals twenty four.

12
00:01:18,110 --> 00:01:20,660
‫And the water boiling point.

13
00:01:23,360 --> 00:01:26,210
‫Equals one hundred degrees.

14
00:01:27,300 --> 00:01:37,260
‫All of these are constants I've declared to them exactly like any other variable, but I've used uppercase

15
00:01:37,260 --> 00:01:37,950
‫letters.


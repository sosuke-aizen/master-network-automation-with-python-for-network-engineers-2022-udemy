﻿1
00:00:00,560 --> 00:00:08,000
‫In this lecture, we'll take a look at the numbers and math operators in Python, there are three types

2
00:00:08,000 --> 00:00:16,640
‫of numbers in Texas which are whole numbers, floating point numbers or floats, which are numbers positive

3
00:00:16,640 --> 00:00:21,650
‫or negative, containing one or more decimals and complex numbers.

4
00:00:22,070 --> 00:00:29,630
‫Complex numbers are used in electrical engineering and we want to discuss about them in our course.

5
00:00:29,930 --> 00:00:35,150
‫Instead, we will concentrate ourselves on integers and floats.

6
00:00:36,150 --> 00:00:43,770
‫First, you must understand what is an operator, an operator is a symbol of the programming language

7
00:00:43,800 --> 00:00:53,190
‫which is able to operate on values, there are arithmetic operators, assignment operators, comparison

8
00:00:53,190 --> 00:00:57,540
‫operators, identity operators and logical operators.

9
00:00:57,870 --> 00:01:00,420
‫Now let's take them one by one.

10
00:01:00,870 --> 00:01:08,190
‫Arithmetic operators are used with numeric values to perform common mathematical operations.

11
00:01:08,890 --> 00:01:18,930
‫There are the following arithmetic operators, addition, subtraction, division floor or integer division,

12
00:01:19,110 --> 00:01:30,480
‫multiplication exponentiation or erasing topower and modulus or let's see, examples with each of them.

13
00:01:30,900 --> 00:01:40,920
‫I create a variable called Price Equals Five and then I write the price plus six and the result is 11.

14
00:01:41,430 --> 00:01:51,930
‫If I want to subtract from a variable, I write price minus three integers are positive or negative

15
00:01:51,930 --> 00:01:58,030
‫values, so I can also have four minus 10 Aleksi afloat.

16
00:01:58,740 --> 00:02:09,120
‫Let's take for example, a equals three point four and B equals two point three and I'm calculating

17
00:02:09,150 --> 00:02:16,050
‫A plus B. This is the result, A minus B or A divided by B.

18
00:02:16,830 --> 00:02:23,080
‫We also have the floor division that always returns an integer.

19
00:02:23,430 --> 00:02:33,690
‫So if I write a two four slashes, which is the floor division, and B the result is one because it

20
00:02:33,690 --> 00:02:36,450
‫only returns a whole number.

21
00:02:38,570 --> 00:02:40,280
‫Then we have the multiplication.

22
00:02:41,470 --> 00:02:51,040
‫Let's say price multiplied by three or A multiplied by B or five, multiplied by five hundred.

23
00:02:52,790 --> 00:03:03,140
‫The exponentiation or raising topower, for example, let's see how many IPV six there are so two to

24
00:03:03,140 --> 00:03:06,550
‫the power of 128.

25
00:03:06,890 --> 00:03:12,410
‫This is because an IPV six uses 128 bits.

26
00:03:12,860 --> 00:03:14,400
‫And this is the number.

27
00:03:14,420 --> 00:03:16,600
‫This is a very big number.

28
00:03:17,000 --> 00:03:20,510
‫So there are a lot of IPV six in the world.

29
00:03:21,680 --> 00:03:30,170
‫Now let's move to another operator and fix the models or simply model ITO, it turns the remainder of

30
00:03:30,170 --> 00:03:38,960
‫the division of left operand by the right, so only the remainder, if we divide, stand by for the

31
00:03:38,960 --> 00:03:43,550
‫quotient is two and the remainder is also to.

32
00:03:43,850 --> 00:03:47,210
‫So the mode operator will return to.

33
00:03:48,240 --> 00:03:58,230
‫Let's try another example, eight mode five and the next three, because five goes one time in eight

34
00:03:58,560 --> 00:04:08,010
‫and the remainder is three 14 mode four is two foregoes three times in 14.

35
00:04:08,160 --> 00:04:10,650
‫And there is the remainder of two.

36
00:04:11,280 --> 00:04:17,970
‫If we use in a mathematical expression int and float, the result will be a float.

37
00:04:18,480 --> 00:04:26,170
‫So five multiplied by three point zero is fifteen point zero.

38
00:04:26,220 --> 00:04:29,170
‫So this is the float because this is a float.

39
00:04:30,000 --> 00:04:36,270
‫Now I want to talk about assignment operators, which I used to assign values to variables.

40
00:04:36,930 --> 00:04:46,290
‫The assignment operators are simple assignment, the equals sign, the increment assignment, the decrement

41
00:04:46,290 --> 00:04:54,720
‫assignment, the multiplication assignment, the division assignment, the power assignment, the modulus

42
00:04:54,780 --> 00:05:02,780
‫assignment and the floor division assignment lists three examples with each of them.

43
00:05:03,240 --> 00:05:03,780
‫Let's see.

44
00:05:03,780 --> 00:05:07,830
‫First, a simple assignment, a equals one.

45
00:05:08,840 --> 00:05:16,960
‫It assigns the video from the right hand side of the equals sign to the variable on the left hand side,

46
00:05:17,960 --> 00:05:26,840
‫then if I write a equals a plus one first, it will evaluate the expression from the right hand side

47
00:05:27,080 --> 00:05:28,550
‫and it's a plus one.

48
00:05:28,970 --> 00:05:35,140
‫The result will be two and then it will assign the value of two to eight.

49
00:05:35,870 --> 00:05:37,730
‫So a will be two.

50
00:05:38,420 --> 00:05:46,270
‫Instead of writing A equals eight plus one, I could simply write a plus equals one.

51
00:05:46,730 --> 00:05:52,740
‫In this case it adds one to the current value of A, which is two.

52
00:05:52,760 --> 00:05:57,730
‫So the result will be three and will assign that result.

53
00:05:57,740 --> 00:05:59,080
‫So three to eight.

54
00:05:59,540 --> 00:06:01,010
‫So A will be three.

55
00:06:01,790 --> 00:06:07,110
‫But if I write a plus equals five, the result will be eight.

56
00:06:07,160 --> 00:06:11,970
‫So in fact it will add five to the value of eight, which is three.

57
00:06:12,110 --> 00:06:14,320
‫So the new value of A is eight.

58
00:06:14,720 --> 00:06:18,410
‫The same happens with the decrement assignment.

59
00:06:19,840 --> 00:06:29,470
‫A minus equals six, this means it will subtract six from the value of A, which is eight.

60
00:06:29,710 --> 00:06:36,880
‫The result will be two and will assign that result, that value to the A variable.

61
00:06:37,540 --> 00:06:43,350
‫If I write a minus equals seven, A will be minus five.

62
00:06:43,960 --> 00:06:53,650
‫So it will subtract seven from two and the result will be assigned to a lexical multiplication assignment.

63
00:06:54,310 --> 00:06:56,650
‫A star equals five.

64
00:06:56,830 --> 00:07:04,530
‫The value of A is minus five, minus five multiplied by five is minus twenty five.

65
00:07:05,050 --> 00:07:07,720
‫So A will be minus twenty five.

66
00:07:08,560 --> 00:07:15,460
‫Now if I write a star equals one, A will remain minus twenty five.

67
00:07:15,760 --> 00:07:18,780
‫Let's try now the division assignment.

68
00:07:19,420 --> 00:07:27,100
‫It will divide the variable by a value and assign the result to that variable to the same variable.

69
00:07:27,550 --> 00:07:30,850
‫A four slash equals five.

70
00:07:32,250 --> 00:07:40,950
‫So first, it will divide A by five and the result will be minus five because a D minus twenty five

71
00:07:41,250 --> 00:07:44,210
‫and we'll assign that value to eight.

72
00:07:44,430 --> 00:07:46,200
‫So A will B minus five.

73
00:07:47,250 --> 00:07:49,810
‫Now I want to show you the power assignment.

74
00:07:50,100 --> 00:07:58,220
‫Let's take another variable, B equals two and then be star star equals four.

75
00:07:58,890 --> 00:08:01,800
‫What is the value of the first?

76
00:08:02,130 --> 00:08:14,460
‫Python calculated two to the power of four and five x 16 and assigned that value to be so B is 16.

77
00:08:15,420 --> 00:08:25,500
‫Let's the floor division assignment, let's say C equals 15 and our C to four slashes.

78
00:08:25,560 --> 00:08:30,180
‫This is the floor division operator in Python equals three.

79
00:08:31,900 --> 00:08:43,720
‫What will be the value of C, C will be five legs, because it calculated 15 divided by three is integers

80
00:08:44,140 --> 00:08:48,100
‫and the result, which is five, has been assigned to C.

81
00:08:49,130 --> 00:08:58,610
‫OK, see E five, please note that we don't have a plus, plus or minus minus operator in Python, if

82
00:08:58,610 --> 00:09:02,750
‫we want to incremental, no, we can simply write something like this.

83
00:09:02,990 --> 00:09:08,870
‫Let's take C plus equal to one and C is six.

84
00:09:09,830 --> 00:09:15,000
‫In C++ or in Java, we could simply write C++.

85
00:09:15,050 --> 00:09:16,850
‫This is not permitted in Python.

86
00:09:17,480 --> 00:09:19,520
‫This is an invalid syntax.

87
00:09:21,950 --> 00:09:30,260
‫There are also some predefined building functions that perform basic math operations, these functions

88
00:09:30,260 --> 00:09:33,030
‫are built in or they are already defined.

89
00:09:33,290 --> 00:09:35,470
‫We don't have to define them.

90
00:09:35,600 --> 00:09:37,160
‫We simply use them.

91
00:09:37,400 --> 00:09:44,900
‫We have to define what function this function finds, the quotient and the remainder simultaneously.

92
00:09:45,790 --> 00:09:51,430
‫Let's try it so devoid of 15 coma for.

93
00:09:53,030 --> 00:10:02,420
‫This is the quotient, and because I mean the result of 15 divided by five and this is the remainder,

94
00:10:02,430 --> 00:10:13,670
‫so the result of 15 Moad five, then we have the P o w function, it raises a number to a certain power.

95
00:10:14,480 --> 00:10:22,990
‫So instead of two stars start, then I can write BRW to chromatin and I've got the same result.

96
00:10:23,000 --> 00:10:29,690
‫And instead of 10 stars start, then I can write p o w 10 chromatin.

97
00:10:30,520 --> 00:10:32,530
‫And I've got the same result.

98
00:10:33,580 --> 00:10:41,500
‫Then we have the round function, it rounds a number to a certain decimal point, for example, let's

99
00:10:41,500 --> 00:10:50,260
‫calculate 18 divided by 13, and we've got this number that has many decimal digits.

100
00:10:51,040 --> 00:11:00,880
‫If I want to display this number only with three decimal digits, I can right around 18, divided by

101
00:11:00,880 --> 00:11:02,690
‫13, comma three.

102
00:11:03,070 --> 00:11:04,960
‫So with three digits.

103
00:11:06,340 --> 00:11:08,950
‫OK, and it rounded the number.

104
00:11:10,420 --> 00:11:19,480
‫Of course, if we have a variable, a one point and a lot of digits, I can have around a with four

105
00:11:19,480 --> 00:11:19,990
‫digits.

106
00:11:21,100 --> 00:11:25,390
‫So it displayed only the first four digits.

107
00:11:26,630 --> 00:11:35,240
‫It didn't modify the variable, the variable remained the same, it only displayed the value of a variable

108
00:11:35,450 --> 00:11:37,790
‫with that number of decimals.

109
00:11:40,570 --> 00:11:47,480
‫And then you have another function called some, as its name implies, it will calculate a sum.

110
00:11:48,440 --> 00:11:49,550
‫Let's try an example.

111
00:11:51,070 --> 00:11:59,560
‫First, let's create a list, let's say numbers equals and then one comma, five comma, sixty six,

112
00:11:59,710 --> 00:12:03,730
‫comma 100, and then the sum of numbers.

113
00:12:04,760 --> 00:12:06,320
‫And this is the result.

114
00:12:07,400 --> 00:12:15,410
‫The sum function takes uneatable is argument, we'll talk about it, three bullets in the next lecture.

115
00:12:17,550 --> 00:12:24,450
‫Then we have the max function that returns the maximum value from a list of values.

116
00:12:24,780 --> 00:12:36,780
‫So let's say Max, of four to one thousand fifty five and minus one, and the maximum of value is one

117
00:12:36,780 --> 00:12:37,440
‫thousand.

118
00:12:38,570 --> 00:12:47,900
‫And there is also the main function that returns the minimum value, so minus seven for minus 10.

119
00:12:48,680 --> 00:12:49,370
‫Five hundred.

120
00:12:50,250 --> 00:12:54,150
‫Of course, the minimum value is minus 10.

121
00:12:55,850 --> 00:13:02,970
‫That's enough for the moment in the next lecture will talk about comparison and identity operators.

122
00:13:03,200 --> 00:13:04,480
‫See you in a minute.


﻿1
00:00:00,450 --> 00:00:07,980
‫In this lecture, we'll take a look at boolean expressions and operators, a boolean expression is a

2
00:00:07,980 --> 00:00:11,580
‫logical statement that is either true or false.

3
00:00:12,030 --> 00:00:20,160
‫Boolean expressions are very useful in Wilen loops or in if testing conditions about which we'll discuss

4
00:00:20,160 --> 00:00:22,800
‫in detail in following lectures.

5
00:00:23,280 --> 00:00:33,810
‫There are three boolean or logical operators in Python and or and not both end and or operators need

6
00:00:33,810 --> 00:00:39,750
‫two expressions and the final result will be evaluated to true or false.

7
00:00:41,510 --> 00:00:50,090
‫Let's take a look first at end operator, the William and operator returns true if both expressions

8
00:00:50,240 --> 00:00:54,590
‫from the left hand side and from the right hand side return true.

9
00:00:54,770 --> 00:01:04,250
‫If any expression is false, the entire expression is evaluated to force the right child age.

10
00:01:04,400 --> 00:01:15,410
‫This is a variable equals six no child age greater than the zero end child age less than 18.

11
00:01:16,730 --> 00:01:22,550
‫It will return true because both expressions are evaluated to two.

12
00:01:23,670 --> 00:01:31,470
‫This expression is true and this expression is true, if one expression evaluate it to force the entire

13
00:01:31,470 --> 00:01:37,740
‫expression, evaluate it, to force the same python expression could be written in the following way.

14
00:01:38,420 --> 00:01:42,270
‫Zero less than child, less than 18.

15
00:01:43,170 --> 00:01:46,110
‫But in my opinion, this is not very readable.

16
00:01:46,440 --> 00:01:51,530
‫So using the end operator makes the code more readable.

17
00:01:52,320 --> 00:01:54,270
‫Let's see other examples.

18
00:01:55,220 --> 00:02:01,160
‫A equals equals A. and three greater than or equal to three.

19
00:02:02,030 --> 00:02:03,410
‫And it's to.

20
00:02:04,370 --> 00:02:08,290
‫Because both expressions are evaluated to two.

21
00:02:09,530 --> 00:02:21,020
‫But if I write a equals, then and then a greater then six weeks to end, let's say nine, less than

22
00:02:21,350 --> 00:02:28,280
‫seven, the second expression from the right hand side, this one will be evaluated to force.

23
00:02:28,460 --> 00:02:33,320
‫So the entire expression will be evaluated to force.

24
00:02:34,310 --> 00:02:43,400
‫In a nutshell, we can say that the entire expression will be evaluated to two only when both expressions

25
00:02:43,400 --> 00:02:52,100
‫from the left hand side and from the right hand side are evaluated to throw in a boolean expression,

26
00:02:52,100 --> 00:02:56,090
‫you can use parentheses to improve your code readability.

27
00:02:57,530 --> 00:03:05,840
‫So you can write something like this, A equals equals seven and a greater than 10.

28
00:03:07,690 --> 00:03:16,630
‫The result will be false because both expressions will be evaluated to false, and I've used parentheses.

29
00:03:17,410 --> 00:03:22,630
‫Now let's talk about or operator the Boolean or operator returns.

30
00:03:22,630 --> 00:03:30,190
‫True, if any operand is true, it will return false if any expression or operand evaluates to false.

31
00:03:31,210 --> 00:03:41,290
‫Let's try some examples, let's say A comma, B equals one Kamata so A is one and B is two and now a

32
00:03:42,100 --> 00:03:47,540
‫diabolical signs six or B greater than zero.

33
00:03:48,730 --> 00:03:57,430
‫The expression has been evaluated to two because at least one operand or one expression has been evaluated

34
00:03:57,430 --> 00:03:57,930
‫to two.

35
00:03:58,210 --> 00:04:02,410
‫And this is the expression that has been evaluated to two.

36
00:04:02,650 --> 00:04:05,130
‫So B is greater than zero.

37
00:04:06,010 --> 00:04:17,740
‫But if I write a diabolical equal signs six or B less than zero, the result will be false because both

38
00:04:17,740 --> 00:04:21,130
‫expressions have been evaluated to false.

39
00:04:22,510 --> 00:04:29,470
‫Let's try another example, A is not equal to six, or B is less than zero.

40
00:04:31,070 --> 00:04:37,820
‫The result will be true because this expression has been evaluated to true.

41
00:04:39,370 --> 00:04:47,290
‫And the last boolean operator is not and it will simply negate the expression to value.

42
00:04:48,740 --> 00:04:57,950
‫If I tried not to equals equals two, I've got false because two equals equals two evaluates to true

43
00:04:58,190 --> 00:05:00,530
‫and not true equals false.

44
00:05:00,980 --> 00:05:01,760
‫Not false.

45
00:05:01,760 --> 00:05:02,390
‫Equals true.

46
00:05:02,510 --> 00:05:03,680
‫Not true.

47
00:05:03,680 --> 00:05:04,500
‫Equals false.

48
00:05:04,910 --> 00:05:06,650
‫We can also have something like this.

49
00:05:16,990 --> 00:05:24,550
‫In this example, it negated only the first expression, not the result of the entire expression.

50
00:05:24,850 --> 00:05:27,930
‫Let's take another example to make it clear.

51
00:05:28,880 --> 00:05:36,700
‫Let's say a equals equals one or B equals equals two, of course, this expression evaluates to true

52
00:05:36,950 --> 00:05:42,020
‫and now looks right here, not in the same expression.

53
00:05:42,710 --> 00:05:45,410
‫It will negate the first expression.

54
00:05:45,890 --> 00:05:49,840
‫So it will be false or two and the result will be two.

55
00:05:50,150 --> 00:05:58,890
‫If I want to negate the entire expression, I must use parentheses this all about boolean variables

56
00:05:58,910 --> 00:06:00,230
‫and operators.

57
00:06:00,470 --> 00:06:02,330
‫See you in the next lecture.


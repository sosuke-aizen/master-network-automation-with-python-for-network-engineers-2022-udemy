﻿1
00:00:01,260 --> 00:00:07,290
‫You know, this lecture will discuss the variables, what is available, how do we create a variable

2
00:00:07,590 --> 00:00:09,780
‫in how to use variables in Python?

3
00:00:10,470 --> 00:00:15,330
‫A variable is one of the fundamental concepts of any programming language.

4
00:00:15,690 --> 00:00:24,350
‫No matter if we are programming in C, C++, Java or Python, we'll use variables to store the data

5
00:00:24,360 --> 00:00:26,550
‫the application is working with.

6
00:00:27,060 --> 00:00:32,670
‫Variables are how your program remembers the values, to be clear.

7
00:00:32,700 --> 00:00:40,170
‫I'd like to give you a simple example like suppose you develop a simple script that converts miles to

8
00:00:40,170 --> 00:00:40,980
‫kilometers.

9
00:00:41,790 --> 00:00:45,930
‫When running the script, it will ask for the number of miles.

10
00:00:46,270 --> 00:00:48,780
‫Then the user will enter a value.

11
00:00:48,780 --> 00:00:56,850
‫Let's say them in the script will output the corresponding value in kilometers, which is sixteen point

12
00:00:56,850 --> 00:01:03,320
‫zero nine because one mile represents one point six zero nine kilometers.

13
00:01:03,870 --> 00:01:12,360
‫The program must save somewhere the value them and target by the user, and it will save it in a variable

14
00:01:12,540 --> 00:01:15,420
‫somewhere in the memory, in the RAM memory.

15
00:01:15,960 --> 00:01:22,950
‫So he can define a variable is a name for a memory, location or address in stores of value.

16
00:01:22,950 --> 00:01:31,560
‫In our example, then a variable has a name and is the variable at WARP implies the value stored in

17
00:01:31,560 --> 00:01:34,320
‫it can be changed in Python.

18
00:01:34,410 --> 00:01:42,300
‫You simply create a new variable by typing the variable name the equals sign into the value that will

19
00:01:42,300 --> 00:01:43,830
‫be stored in that variable.

20
00:01:44,130 --> 00:01:47,130
‫For example, Miles equals 10.

21
00:01:49,520 --> 00:01:54,320
‫I've created a variable named Miles that stores the value of them.

22
00:01:54,660 --> 00:02:02,780
‫Now, if you want to modify the value stored in the mildest variable, you're right Miles equals LLC

23
00:02:03,320 --> 00:02:12,470
‫15 15 is the new value start in our variable will have a long discussion about immutable and immutable

24
00:02:12,470 --> 00:02:17,750
‫variables, which means what happens with the variable when we modify it.

25
00:02:17,960 --> 00:02:20,570
‫But we won't enter into details now.

26
00:02:21,110 --> 00:02:25,370
‫You can also create the multiple variables in a single line like this.

27
00:02:26,030 --> 00:02:32,120
‫A comma b c equals one comma five command line.

28
00:02:32,750 --> 00:02:39,800
‫I've created three variables A, B and C with a value of one, five and nine.

29
00:02:40,750 --> 00:02:46,780
‫You can see that A is one, B is five and C is nine.

30
00:02:48,760 --> 00:02:53,320
‫Of course, variables can store also other type of data.

31
00:02:53,500 --> 00:03:01,300
‫Not only numbers, let's create a variable called age and a variable called name.

32
00:03:02,480 --> 00:03:10,670
‫The value stored in the age variable will be 30, and the value stored in the name of variable will

33
00:03:10,670 --> 00:03:11,150
‫be John.

34
00:03:14,860 --> 00:03:18,430
‫Age equals 30 and the name equals John.

35
00:03:19,720 --> 00:03:27,330
‫I've created two variables a variable called age that stores the index are value of 30.

36
00:03:27,640 --> 00:03:35,230
‫And another variable called name that starts a string or a sequence of characters with the value of

37
00:03:35,230 --> 00:03:35,590
‫John.

38
00:03:36,970 --> 00:03:44,620
‫Often you'll see terms like defining a variable, declaring a variable or initializing a variable.

39
00:03:44,890 --> 00:03:52,360
‫In other programming languages like, for example, C or C++, there is a very clear distinction between

40
00:03:52,360 --> 00:03:55,900
‫declaring a variable and defining a variable.

41
00:03:56,500 --> 00:04:04,120
‫Declaring means introducing a new name and type, for example, into miles, which means a new variable

42
00:04:04,180 --> 00:04:08,020
‫called the meireles will be created and it will store.

43
00:04:08,050 --> 00:04:14,230
‫Earning tags are value, and the defining means allocating a value to that variable.

44
00:04:14,440 --> 00:04:16,900
‫For example, Miles equals 10.

45
00:04:17,900 --> 00:04:25,880
‫In Python, we don't have such a distinction because we don't explicitly specify the type of data the

46
00:04:25,880 --> 00:04:35,120
‫variable will start, a variable is also sometimes called a name in or any type of value, a whole number

47
00:04:35,150 --> 00:04:43,130
‫or integer, a floating point number or simply float or a sequence of characters or a string, at least

48
00:04:43,380 --> 00:04:45,410
‫a reference to a file and so on.

49
00:04:46,450 --> 00:04:55,740
‫Now, let's see some rules about the naming variable names can contain lepers, the Greeks and the underscore

50
00:04:55,780 --> 00:04:59,750
‫character of variable name can't start with a number.

51
00:04:59,770 --> 00:05:03,670
‫It can only start with underscore or letters.

52
00:05:03,970 --> 00:05:08,350
‫If it starts with an underscore, it has special meaning to Python.

53
00:05:08,560 --> 00:05:09,790
‫So you should avoid it.

54
00:05:11,350 --> 00:05:13,990
‫This is a valid name for a variable.

55
00:05:14,230 --> 00:05:17,920
‫But this isn't a valid name for a variable.

56
00:05:18,620 --> 00:05:22,450
‫That's because we can't start a variable with a digit.

57
00:05:23,350 --> 00:05:29,200
‫Of course, we can use digits, but not as the first character of the variable name.

58
00:05:30,100 --> 00:05:30,790
‫So you.

59
00:05:30,980 --> 00:05:33,310
‫For he's a valid name.

60
00:05:37,190 --> 00:05:46,070
‫Then you must know that we can't use special characters in variable names like spaces, commas, columns,

61
00:05:46,340 --> 00:05:52,220
‫exclamation marks, parentheses, square brackets, curly braces and so on.

62
00:05:52,970 --> 00:05:59,060
‫For example, your space age equals 10 isn't a valid name.

63
00:05:59,300 --> 00:06:05,000
‫I've used space between your &8, and that's not permitted.

64
00:06:05,990 --> 00:06:08,570
‫I can use an underscore.

65
00:06:08,750 --> 00:06:10,190
‫And that would be OK.

66
00:06:10,580 --> 00:06:12,290
‫This is a valid name.

67
00:06:12,950 --> 00:06:23,360
‫You should also avoid using works that have special meaning in Python like least SDR set for and so

68
00:06:23,360 --> 00:06:23,510
‫on.

69
00:06:23,960 --> 00:06:25,880
‫So this is not recommended.

70
00:06:26,180 --> 00:06:34,430
‫This is a valid name, but as you can see, I use a special colour for it representing the SDR name

71
00:06:34,760 --> 00:06:39,170
‫of X because SDR has a special meaning to Python.

72
00:06:39,710 --> 00:06:42,260
‫They suck, but not recommended.

73
00:06:42,800 --> 00:06:46,070
‫You can use SDR one, and that's better.

74
00:06:47,610 --> 00:06:49,740
‫Python is case sensitive.

75
00:06:50,070 --> 00:06:54,640
‫It makes a distinction between lower and uppercase letter.

76
00:06:55,170 --> 00:06:57,120
‫And so are variable names.

77
00:06:57,990 --> 00:07:07,350
‫If I have a variable called age 40 and another variable with an uppercase a also name age 60.

78
00:07:07,740 --> 00:07:10,500
‫In fact, here I've created two variables.

79
00:07:14,040 --> 00:07:21,450
‫Sometimes the camel case notation is used that mystery group works and then each walk in the middle

80
00:07:21,450 --> 00:07:28,470
‫of the phrase begins with a capital letter with no intervening spaces or punctuation.

81
00:07:28,740 --> 00:07:33,780
‫Like, for example, marks permitted value, let's say one hundred.

82
00:07:34,920 --> 00:07:41,520
‫This is camel case notation or your age equals 20.

83
00:07:42,000 --> 00:07:49,020
‫Pep eight, which introduces a standard style guide for Python code, doesn't recommend using camel

84
00:07:49,020 --> 00:07:55,590
‫case, but words separated by Underscores, which is known as snake case.

85
00:07:56,280 --> 00:08:00,750
‫So the recommended way is to use a name like Max.

86
00:08:01,020 --> 00:08:06,000
‫Underline permitted underline value equals one hundred.

87
00:08:07,670 --> 00:08:11,480
‫Or your underlying eight equals 20?

88
00:08:18,100 --> 00:08:24,610
‫Of course, you don't have to follow exactly of these conventions as long as you try to remain consistent

89
00:08:24,820 --> 00:08:26,080
‫within your own code.

90
00:08:26,590 --> 00:08:34,750
‫These are best practices also recommended by Pep eight, which is the style guide for Python code,

91
00:08:34,990 --> 00:08:40,660
‫and it would be great to take a look at the this document and see what are the main points.

92
00:08:41,500 --> 00:08:50,370
‫You can search for Pep eight on Google, and the first website will be the Pep eight style guide for

93
00:08:50,380 --> 00:08:51,190
‫Python code.

94
00:08:51,850 --> 00:08:58,360
‫Here we find the many advices on how to write a readable and high quality Python code.

95
00:08:59,940 --> 00:09:06,450
‫That's all about the variable basics, and in the next lectures, we'll take a look at the comments,

96
00:09:06,660 --> 00:09:08,850
‫constants and variable types.

97
00:09:09,450 --> 00:09:11,430
‫See you in just a few seconds.


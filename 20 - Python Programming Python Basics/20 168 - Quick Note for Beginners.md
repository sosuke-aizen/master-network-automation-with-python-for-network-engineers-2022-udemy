

 **Beginners start the course from here!**

If you are here without having any prior experience with Python or you have
some experience but feel the need of in-depth and complete explanations about
core Python concepts, this section and those that fallow are for you. **It
will teach you General Python Programming.**

 **Just go through each of the lectures in the order I provided them in, so
that you will benefit from a coherent and consistent learning process.**


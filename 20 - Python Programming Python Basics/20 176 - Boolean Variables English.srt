﻿1
00:00:00,780 --> 00:00:08,070
‫Hello and welcome to this lecture on booleans, there are boolean variables and boolean expressions,

2
00:00:08,520 --> 00:00:16,980
‫a boolean variable is in fact an object of the class, which is an int or integer subclass.

3
00:00:17,830 --> 00:00:26,220
‫So a boolean variable is a kind of integer, a more specialized integer in python.

4
00:00:26,420 --> 00:00:36,450
‫There are two boolean constants defined to always return with uppercase D and false return with uppercase

5
00:00:36,450 --> 00:00:36,780
‫F.

6
00:00:37,710 --> 00:00:42,930
‫They can also be interpreted as integers one and zero.

7
00:00:44,070 --> 00:00:52,200
‫In Python, there is also another constant called none that is frequently used to represent the absence

8
00:00:52,200 --> 00:00:57,710
‫of a value is when default arguments are not best to a function.

9
00:00:58,440 --> 00:01:05,460
‫If we cast or convert a bull to an end, we get one for two and zero for a false.

10
00:01:06,700 --> 00:01:13,270
‫So end of two is one and end of false is zero.

11
00:01:14,430 --> 00:01:24,450
‫We can use double equal signs or EZ operators to test an expression index because the address of true

12
00:01:24,480 --> 00:01:26,390
‫or false is constant.

13
00:01:26,670 --> 00:01:29,280
‫It's the same during program execution.

14
00:01:31,920 --> 00:01:35,760
‫If I want to see what is the address of Tooele, I got the.

15
00:01:37,110 --> 00:01:40,890
‫Then let's try for greater than three.

16
00:01:42,340 --> 00:01:50,680
‫Of course, this is true, and if I want to see what's the address of this expression, I've got the

17
00:01:50,680 --> 00:01:51,440
‫same will.

18
00:01:51,970 --> 00:01:59,320
‫So, in fact, it's the same if I write for greater than three equals equals two.

19
00:02:00,230 --> 00:02:06,650
‫Or for greater than three, is two weeks the same?

20
00:02:07,980 --> 00:02:16,650
‫The value of two is one in the value of falls is zero, but two and one respectively false and zero

21
00:02:16,830 --> 00:02:21,600
‫are not the same objects or are not saved in the same memory address.

22
00:02:22,660 --> 00:02:25,090
‫So two equals equals one.

23
00:02:25,450 --> 00:02:37,600
‫That's true, but two is one is false because the address of one is not the same as the address of two.

24
00:02:39,350 --> 00:02:41,120
‫Let's try other examples.

25
00:02:41,930 --> 00:02:44,150
‫Three equals equals false.

26
00:02:45,840 --> 00:02:46,590
‫That's false.

27
00:02:47,950 --> 00:02:56,350
‫For between single Coke's equals equals four is also false because type matters.

28
00:02:56,650 --> 00:03:02,050
‫Here we have a string written between single coats and this is an integer.

29
00:03:02,230 --> 00:03:07,300
‫So we are comparing a string to an integer and the result is false.

30
00:03:08,560 --> 00:03:12,340
‫Then A, come out, B equals one to.

31
00:03:13,670 --> 00:03:24,490
‫A greater Bamby be less than or equal to A and both expressions are evaluated to false.

32
00:03:26,240 --> 00:03:27,530
‫A greater than be.

33
00:03:28,880 --> 00:03:31,220
‫B equals equals to.

34
00:03:32,550 --> 00:03:38,160
‫The first expression he's evaluated to false and the second expression is to.

35
00:03:39,380 --> 00:03:48,800
‫Being a type of integer bool variables have all properties and methods of integers, but also some specialized

36
00:03:48,800 --> 00:03:56,390
‫ones like end and or maybe it doesn't make a lot of sense to write something like this in your code,

37
00:03:56,540 --> 00:03:58,730
‫but you could in order to make a point.

38
00:03:58,850 --> 00:04:05,540
‫And the VIX that pools are integers, any integer operation also works with balls.

39
00:04:06,470 --> 00:04:09,470
‫So if I write to greater than false.

40
00:04:11,160 --> 00:04:14,350
‫That's true because one is greater than zero.

41
00:04:14,700 --> 00:04:18,720
‫I can write X equals two plus two.

42
00:04:19,380 --> 00:04:26,070
‫X is two because I have one plus one equals two or 100.

43
00:04:26,070 --> 00:04:29,550
‫Multiplied by false is a zero.

44
00:04:30,610 --> 00:04:34,150
‫Because I have multiplied 100 by zero.

45
00:04:35,830 --> 00:04:44,890
‫Now, let's talk about truthiness of variables or objects, all python objects have an associated truth

46
00:04:44,890 --> 00:04:45,380
‫value.

47
00:04:46,300 --> 00:04:51,850
‫We get the truth value of any object by calling the bull function.

48
00:04:52,060 --> 00:04:54,520
‫And the object is an argument.

49
00:04:55,370 --> 00:05:04,430
‫So if we have an integer, bull of zero is false and bull of X is true for any value of X.

50
00:05:05,640 --> 00:05:12,420
‫For afloat, we have the same pool of zero point zero dollars and pool of any other value is to.

51
00:05:13,360 --> 00:05:22,750
‫For sequences, strings, leaks and topolice, we have false if the sequence is empty and true otherwise

52
00:05:23,440 --> 00:05:32,860
‫for dictionaries and sex, we have false if they are empty and true otherwise and for custom classes.

53
00:05:32,860 --> 00:05:38,230
‫So classes that we define by ourselves, we must implement.

54
00:05:38,230 --> 00:05:47,170
‫The Underbool and Vanderlyn methods will talk more about this in a lecture where we'll discuss about

55
00:05:47,170 --> 00:05:49,030
‫object oriented programming.

56
00:05:50,920 --> 00:06:02,920
‫Back to our quote here, A is one, so bowl of one, a straw bowl of zero is false, bowl of minus one

57
00:06:03,280 --> 00:06:04,300
‫is also true.

58
00:06:04,840 --> 00:06:11,130
‫In fact, bowl of any number that is different from zero is true.

59
00:06:11,830 --> 00:06:13,090
‫Now let's have a string.

60
00:06:13,090 --> 00:06:16,120
‫So bowl off and let's say here Python.

61
00:06:16,960 --> 00:06:26,230
‫The extra botibol of an empty string is false now for at least, let's say least one equals and here

62
00:06:26,230 --> 00:06:27,310
‫I have an empty list.

63
00:06:27,910 --> 00:06:30,610
‫Bowl of least one is false.

64
00:06:31,660 --> 00:06:39,250
‫But bull of any non-empty list, so let's say one come out to come on three legs through.

65
00:06:40,200 --> 00:06:48,000
‫It's important to understand and use correctly the truthiness of an object, because it's used sometimes

66
00:06:48,000 --> 00:06:54,450
‫when testing expressions, something like this, like, say, prices equals.

67
00:06:54,990 --> 00:06:58,530
‫And here I have at one hundred comma 200.

68
00:07:00,220 --> 00:07:10,060
‫And now if prices don't worry, we'll discuss in detail about this in another lecture, Brent, prices,

69
00:07:10,060 --> 00:07:11,220
‫truth, value.

70
00:07:13,200 --> 00:07:13,490
‫Through.

71
00:07:14,850 --> 00:07:22,380
‫It means pool of prices equals to else prices, truth value falls.

72
00:07:25,540 --> 00:07:33,610
‫And it's true because prices it's not an empty list and pool of prices equals to now looks empty the

73
00:07:33,610 --> 00:07:34,120
‫least.

74
00:07:38,690 --> 00:07:41,840
‫And let's execute the code one more time.

75
00:07:45,040 --> 00:07:49,840
‫Prices, truth, value is false because the list is empty.

76
00:07:51,690 --> 00:07:58,530
‫In the next lecture, we'll talk about boolean operators, see you in just a few seconds.


﻿1
00:00:00,860 --> 00:00:07,620
‫Welcome back, everyone, to this lecture on variables and types in Python, now that you know what

2
00:00:07,640 --> 00:00:14,440
‫a dynamically typed programming language is, let's see what basic data types do we have in Python?

3
00:00:15,080 --> 00:00:20,630
‫The type will be associated with the value stored in the variable at runtime.

4
00:00:21,170 --> 00:00:28,520
‫Will quickly discuss about the main python data types, and then our lectures will go into more detail

5
00:00:28,520 --> 00:00:30,020
‫about each of them.

6
00:00:31,470 --> 00:00:41,010
‫We have numbers in Texas or whole numbers and floating point numbers or simply floats, for example,

7
00:00:41,010 --> 00:00:43,410
‫H equals 20.

8
00:00:43,650 --> 00:00:50,240
‫This is an integer and then Miles equals four point three.

9
00:00:50,670 --> 00:00:51,780
‫This is a float.

10
00:00:55,610 --> 00:01:05,480
‫Another type of data are the booleans, booleans are logical values indicating false, true or none,

11
00:01:05,600 --> 00:01:16,900
‫like, say, X equals to the type of X is Boulle or type of free to equal signs three.

12
00:01:17,810 --> 00:01:20,280
‫So three is equal to three.

13
00:01:20,690 --> 00:01:26,750
‫This expression evaluates to two and the type of two is bool.

14
00:01:27,870 --> 00:01:37,270
‫We have them strings, strings are ordered, the sequence of characters like say, name equals Andre.

15
00:01:37,970 --> 00:01:41,010
‫This is a string type of name.

16
00:01:41,940 --> 00:01:54,300
‫A star, then we have lists, let's say years equals and between square brackets, I have two thousand

17
00:01:54,570 --> 00:01:58,410
‫one, 2002 and 2003.

18
00:01:59,680 --> 00:02:05,830
‫And between values, I use coma's type of years at least.

19
00:02:06,770 --> 00:02:14,510
‫Then have topless populists are similar to leasebacks baatar, immutable sequence of objects where leaks

20
00:02:14,510 --> 00:02:24,980
‫are mutable so like, say, the yuan equals and now between parentheses one, John and like say three.

21
00:02:27,080 --> 00:02:33,550
‫This is a people the type of data stored in Taiwan is a popular.

22
00:02:36,350 --> 00:02:45,470
‫Then we have sex, they are mutable collection of unordered, unique objects, let's create a set called

23
00:02:45,470 --> 00:02:46,160
‫Vowells.

24
00:02:47,900 --> 00:02:50,360
‫Equals in between curly braces.

25
00:02:50,780 --> 00:02:52,340
‫All right, a.

26
00:02:54,300 --> 00:02:54,710
‫E!

27
00:02:57,300 --> 00:02:57,690
‫Oh.

28
00:02:59,190 --> 00:03:02,010
‫And you this is a set.

29
00:03:03,490 --> 00:03:07,540
‫A set contains only unique items or elements.

30
00:03:11,450 --> 00:03:14,360
‫Another type of data is the dictionary.

31
00:03:15,520 --> 00:03:23,890
‫A dictionary is a collection of unordered key value pairs, let's create a dictionary called Berson

32
00:03:24,370 --> 00:03:25,150
‫Equals.

33
00:03:25,450 --> 00:03:34,270
‫And between a pair of curly braces, I have a key called name and the value like, say, Andre, another

34
00:03:34,270 --> 00:03:38,620
‫key called the location and the value, let's say Europe.

35
00:03:42,100 --> 00:03:46,870
‫Another key called Lexie Age and the value 40.

36
00:03:48,100 --> 00:03:49,510
‫This is a dictionary.

37
00:03:53,280 --> 00:04:00,690
‫Now that we've seen what are the main data types of python core programming language will move forward

38
00:04:00,960 --> 00:04:06,000
‫and in the next lectures, we'll take a closer look at each of them.


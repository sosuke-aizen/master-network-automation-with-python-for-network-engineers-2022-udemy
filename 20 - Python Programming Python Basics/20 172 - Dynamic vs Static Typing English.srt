﻿1
00:00:01,080 --> 00:00:07,680
‫In the last lecture, we've talked about the very basics, what is a variable, what is the purpose

2
00:00:07,680 --> 00:00:10,740
‫of a variable and how can we define variables?

3
00:00:11,640 --> 00:00:16,350
‫Now we'll take a look at what type of data can be stored in a variable.

4
00:00:16,920 --> 00:00:25,560
‫Python is a dynamically typed programming language in contrast to C, C++ or Java, which are statically

5
00:00:25,560 --> 00:00:26,070
‫typed.

6
00:00:26,760 --> 00:00:33,780
‫A language is statically typed if a type of a variable is known at compile time.

7
00:00:33,900 --> 00:00:41,250
‫So before running the script and this means that the programmer must specify the type of the data that

8
00:00:41,250 --> 00:00:49,230
‫will be stored in a variable when he or she creates the variable in a statically typed language like

9
00:00:49,230 --> 00:00:58,620
‫Java, a variable would be declared is, for example, int score and then defined as score equals 10.

10
00:01:03,280 --> 00:01:11,440
‫This is how we declare and define a variable in a statically typed language, a language like Python

11
00:01:11,440 --> 00:01:20,080
‫is dynamically typed and it means that the pipe is associated with runtime values and the not with named

12
00:01:20,080 --> 00:01:20,920
‫variables.

13
00:01:21,340 --> 00:01:28,180
‫So values stored in variables have type and not of a name or the variable itself.

14
00:01:28,900 --> 00:01:36,070
‫This means that you as a programmer can write a little bit quicker because you never have to specify

15
00:01:36,070 --> 00:01:37,180
‫variable types.

16
00:01:37,810 --> 00:01:44,230
‫In Python, we create a variable named score simply by writing score equals then.

17
00:01:47,740 --> 00:01:55,540
‫We can see what type of data of variable stores using the building type function, each variable or

18
00:01:55,540 --> 00:02:04,390
‫object of variable is in fact an object of a class, has a type of value and an I.D. and that they are

19
00:02:04,390 --> 00:02:07,440
‫constantly using variable lifetime.

20
00:02:07,870 --> 00:02:10,390
‫So type of score.

21
00:02:11,800 --> 00:02:21,160
‫And we can see that the value stored in this variable is of type integer or int if I create another

22
00:02:21,160 --> 00:02:30,880
‫variable X, a name equals Andre in the least, like, say, my friends equals in our list.

23
00:02:32,610 --> 00:02:39,390
‫We'll talk a lot about the leaks in another section, and for this moment, it's enough for you to know

24
00:02:39,390 --> 00:02:45,030
‫that at least is another type of data that stores a sequence of objects.

25
00:02:46,980 --> 00:02:53,580
‫We can check the type of the value stored in the name variable by typing type and the name.

26
00:02:54,600 --> 00:03:01,380
‫And you can see that the value is of type A star or a string of type.

27
00:03:02,500 --> 00:03:08,170
‫My friends and we can see that the variable stories at least.

28
00:03:09,100 --> 00:03:14,170
‫If I have a variable like, say, PI equals three point four one.

29
00:03:15,510 --> 00:03:17,820
‫Type of pie equals float.

30
00:03:18,030 --> 00:03:21,600
‫This is a float number, a number with decimals.

31
00:03:22,110 --> 00:03:26,620
‫Earlier, I said that a variable is a constant idea.

32
00:03:27,390 --> 00:03:30,360
‫What is that idea and how do we get it?

33
00:03:31,080 --> 00:03:36,510
‫There is a function of building function, so a function of X already defined.

34
00:03:36,810 --> 00:03:40,530
‫We just use it simply called idy end.

35
00:03:40,590 --> 00:03:44,970
‫This function has one argument and that's the object.

36
00:03:45,270 --> 00:03:53,550
‫This building function returns the memory address where the value referenced by the variable is stored

37
00:03:53,970 --> 00:03:59,190
‫or simply said this is the address of the variable in memory.

38
00:04:02,180 --> 00:04:08,810
‫The values taught by this variable is saved in memory at this address.

39
00:04:09,950 --> 00:04:18,350
‫We can also see a hexadecimal value if we like, there is another building function Hecks and then D

40
00:04:18,380 --> 00:04:19,610
‫of name.

41
00:04:21,020 --> 00:04:27,800
‫This is the hexadecimal address of the value that is stored in that variable.

42
00:04:29,230 --> 00:04:35,450
‫Strictly speaking, if I write my VA equals, then my VA is not equal to them.

43
00:04:35,930 --> 00:04:43,980
‫Instead, my VA is a reference to an integer object containing the value thing and is located at the

44
00:04:43,990 --> 00:04:47,650
‫memory address so it doesn't buy the idea function.

45
00:04:48,980 --> 00:04:57,200
‫It's a conclusion in the colloquial programers language, except that my verse equals 10, but in fact

46
00:04:57,200 --> 00:05:06,680
‫my verse is an alias or a reference to an object stored at the address written by the idea of building

47
00:05:06,680 --> 00:05:07,300
‫function.

48
00:05:07,730 --> 00:05:14,440
‫So at this address, in the next lecture, we'll talk about variable types.

49
00:05:14,720 --> 00:05:16,820
‫See you in just a few seconds.




 **More information about the challenges:**

  * Solve these challenges only after watching all the videos in the section, in the order that I’ve provided.

  * For Cisco automation tasks I’d recommend you to use [this topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN) in GNS3 or create a similar one. [Import the GNS3 Project.](https://drive.google.com/open?id=1gqx8pGamn02D0dzOpbd3FKIPOhRJZBgz)

  * For most challenges, you can use Linux instead of Cisco. Just send Linux commands instead of Cisco IOS.

  * If your solution is not correct, then try to understand the error messages, watch the video again, rewrite the solution, and test it again. Repeat this step until you get the correct solution.

  *  **Save** the solution in a file for future reference or recap.

  

 **Challenge #1**

Create a Python script that connects to a Cisco Router using SSH and Netmiko.
The script should execute the **show arp** command in order to display the ARP
table.

Print out the output of the command.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1F4aQI37aDwoxKULg6RJCiNMlFjyhS3WQ).

  

 **Challenge #2**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=1F4aQI37aDwoxKULg6RJCiNMlFjyhS3WQ)
so that the Python script reads the IP address of the device, the port, the
username, and the password from a file.

The file contains the login information on a single line in the format:
_IP:PORT:USERNAME:PASSWORD:ENABLE_PASSWORD_

Example: _10.1.1.10:22:u1:cisco:cisco_

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/file/d/15_m3NApL_WE8-xcCl99gKtfLBg3Z5_TV/view?usp=sharing).

  

 **Challenge #3**

Create a Python script that connects to a Cisco Router using SSH and Netmiko.
The script should get the prompt, process it and then print the hostname part.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1s_QpR0F92XdEB0noUHlMYVl9HfstAKvv).

  

 **Challenge #4**

Create a Python script that connects to a Cisco Router using SSH and Netmiko.
The script should execute the **show ip int brief** and **show run** commands.

Print out the output of each command.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1LyKSJMP0SUpBwZTr63--ZBWoPVQAjH7V).

  

 **Challenge #5**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=1LyKSJMP0SUpBwZTr63--ZBWoPVQAjH7V)
so that the script saves the output of each command into its own file. The
name of the file should contain the router’s hostname.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1-rnVydWewcbDsPYUHcPT0-VpDjMtw6lB).

  

 **Challenge #6**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=1-rnVydWewcbDsPYUHcPT0-VpDjMtw6lB)
so that the script will prompt for both the user that authenticates and the
enable passwords securely (use getpass module). Run the script in the terminal
(you can not run it in PyCharm).

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1UFGLaNXlYAzOpWzWDlFvnkSJYTrbWeP9).

  

 **Challenge #7**

Create a Python script that connects to a Cisco Router using SSH and Netmiko.
The script should create a user and then save the running configuration of the
router.

To create a user execute: _username admin secret topsecret_ command in the
global configuration mode.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1i7fQnro7A5bWinjypp4wRd4G755Kw3fd).

  

 **Challenge #8**

Create a Python script that connects to a Cisco Router using SSH and Netmiko.
The script should create an ACL (access control list) by executing the
following 3 commands:

 **access-list 101 permit tcp any any eq 80**

 **access-list 101 permit tcp any any eq 443**

 **access-list 101 deny ip any any**

Note: Try to execute the commands by a single method call.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=18n4jZuAjc6n5fbwzo4YdtIOzBh9sYGPB).

  

 **Challenge #9**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=18n4jZuAjc6n5fbwzo4YdtIOzBh9sYGPB)
so that the script will also display the commands that were executed.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1gq_96H_b17pr4-Tsbl-2opHPnk5UXpHf).

  

 **Challenge #10**

Create a Python script that connects to a Cisco Router using SSH and Netmiko
and executes all the commands from [this
file.](https://drive.google.com/open?id=1SS6-WcupEkHqsdu-QQVDwyOY6o4g4mZi)

Note: Try to execute the commands by a single method call.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=10ezZwPyvZe8EHwgNh5uNbbKXrMCokxxv).

  

 **Challenge #11**

Consider a [topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-
eCZHkeUChiS-VbN) with multiple devices like [this
one.](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

Create a Python script that connects to each Router using Netmiko, execute
show ip interface brief and display the output.

The IP addresses of the routers are saved in a Python list.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1jT93pHZeRcL5nnh8ZZRk9P7rxiEIyC8Y).

  

 **Challenge #12**

Change the [solution from the previous
challenge](https://drive.google.com/open?id=18n4jZuAjc6n5fbwzo4YdtIOzBh9sYGPB)
so that the script will save the output to a file instead of printing it out.

Each filename should contain the hostname and current date.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1v3IpllYPTbh_aBQnx9Y6vCzkLIK20BY1).

  

 **Challenge #13**

Create a function called **execute()** that has 2 arguments: a device of type
dictionary and a command to execute on that device. After executing the
command the function will disconnect.

The function will use Netmiko to connect and execute the command on the
device.

Call the function to execute a command on Cisco router and on a Linux Server.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=199kca7sPGzjNoQ2wgUu68QIKXNUuoYdp).

  

 **Challenge #14**

Change the solution from the previous challenge so that the function receives
a list of global configuration commands as its second argument and executes
those commands on the device using Netmiko.

Example:

cmd = [ **'no router rip'** , **'int loopback 0'** , **'ip address 1.1.1.1
255.255.255.255'** , **'end'** , **'sh ip int loopback 0'** ]

execute(cisco_device, cmd)

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1w6AzedxSqkR2fnaxh4fbwfgJDuGfMfoi).

  

 **Challenge #15**

Consider a [topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-
eCZHkeUChiS-VbN) with multiple devices like [this
one.](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

Call the function defined at [Challenge 13
](https://drive.google.com/open?id=1w6AzedxSqkR2fnaxh4fbwfgJDuGfMfoi)for each
device in the topology to execute some commands on each device. The commands
that will be executed on each device can be different.

The script should work sequentially.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1_bv6lDW8Y6dUel3d1RSo3MiRL-BnTxld).

  

 **Challenge #16**

Consider the [solution from the previous
challenge.](https://drive.google.com/open?id=1_bv6lDW8Y6dUel3d1RSo3MiRL-
BnTxld)

Implement exception handling (try...except) so that if a device in the list is
down, the script will move on to the next one and doesn’t stop and fail when
trying to access the device that is down.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/file/d/1p40Yxz6rCCMV7_qDd76Xo2e9uZtpNjp2/view?usp=sharing).

  

 **Challenge #17**

Change the [solution of the previous
challenge](https://drive.google.com/open?id=1_bv6lDW8Y6dUel3d1RSo3MiRL-BnTxld)
so that the script will work concurrently (implement multithreading).

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1Cg4NKYc9FCCVxd-WjmDwpH6dXvLgqUhD).

  

 **Challenge #18**

Combine the solutions
([sol1](https://drive.google.com/open?id=1_bv6lDW8Y6dUel3d1RSo3MiRL-BnTxld) |
[sol2](https://drive.google.com/open?id=1Cg4NKYc9FCCVxd-WjmDwpH6dXvLgqUhD)) of
the previous two challenges so that you measure how long it takes to execute
the same commands sequentially vs. concurrently.

The script output should be similar to this:

 _Script execution time (SEQUENTIALLY): 19.855618715286255_

 _Script execution time (CONCURRENTLY): 6.843954801559448_

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1DRfLXilUyme3RsvFPqnsId7-YdVnRUNP).


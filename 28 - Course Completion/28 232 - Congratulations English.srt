﻿1
00:00:00,240 --> 00:00:07,380
‫Congratulations on completing this course, you have now a solid understanding of Python programming

2
00:00:07,380 --> 00:00:13,510
‫used in network automation in a multi vendor environment from Cisco to star or Linux.

3
00:00:14,290 --> 00:00:21,810
‫Now you are ready to simplify boring and repetitive administrative tasks by creating your own python

4
00:00:21,810 --> 00:00:22,770
‫applications.

5
00:00:23,250 --> 00:00:30,210
‫The information you've got from this training will help you to reduce your workload by putting these

6
00:00:30,210 --> 00:00:32,010
‫new skills into practice.

7
00:00:32,370 --> 00:00:38,500
‫If you wish they did a feature or an example that hasn't been discussed here.

8
00:00:38,700 --> 00:00:40,330
‫Please send me a message.

9
00:00:40,740 --> 00:00:48,000
‫I've tried to show you the most important Python libraries used in network automation and other advanced

10
00:00:48,000 --> 00:00:49,140
‫python concepts.

11
00:00:49,320 --> 00:00:54,570
‫But I know there could be other features that we haven't discussed here.

12
00:00:55,110 --> 00:01:03,540
‫I'll do my best to keep this course up to date if you are satisfied with the content in the course and

13
00:01:03,540 --> 00:01:05,800
‫consider that I did a good job.

14
00:01:06,000 --> 00:01:09,810
‫I would ask you that you leave an honest review.

15
00:01:10,470 --> 00:01:15,750
‫Now, if you are not satisfied with the course content, please get in touch with me.

16
00:01:15,990 --> 00:01:20,570
‫You me has multiple ways for you to contact instructors.

17
00:01:21,150 --> 00:01:25,830
‫Thank you for enrolling in this course and I wish you all the best.


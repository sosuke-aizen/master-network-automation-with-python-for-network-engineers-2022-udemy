﻿1
00:00:01,350 --> 00:00:08,850
‫In this lecture, I'll show you how to install Python three on Windows, then typically that involves

2
00:00:08,850 --> 00:00:13,430
‫downloading the Windows installer and running it on your machine.

3
00:00:14,040 --> 00:00:19,530
‫If you are using another version of Windows, the installation instructions are very similar.

4
00:00:20,100 --> 00:00:25,650
‫Now, if you are using another OS like Linux Dharmic, you can skip this lecture.

5
00:00:26,930 --> 00:00:33,830
‫The first step for installing Python is to visit the official Python website and download the latest

6
00:00:33,830 --> 00:00:34,910
‫version of Python.

7
00:00:36,030 --> 00:00:41,450
‫The official Python website is w w w dot python dot org.

8
00:00:42,580 --> 00:00:49,840
‫I'll hover the mouse over the download menu and download python free dot seven dot to.

9
00:00:50,800 --> 00:00:57,520
‫By the way, don't worry, if you find here another version, the most important thing is the major

10
00:00:57,520 --> 00:00:59,500
‫version, which is version three.

11
00:01:00,110 --> 00:01:02,260
‫This will be around for years.

12
00:01:02,290 --> 00:01:08,230
‫And also the content of this course will be 100 percent applicable and valid.

13
00:01:09,700 --> 00:01:18,300
‫However, if possible, use a version greater than or equal to Python three dot six after downloading

14
00:01:18,310 --> 00:01:22,210
‫the installer, let's double click the file and run it.

15
00:01:23,200 --> 00:01:30,820
‫A new window opens up with an important setting in order to avoid future problems.

16
00:01:31,040 --> 00:01:34,840
‫Select Ed Bilum VEDAT seven Towpath.

17
00:01:36,390 --> 00:01:39,210
‫After that, click on Install now.

18
00:01:41,380 --> 00:01:48,040
‫After a few seconds, depending on how fast these are, Moshin Python will be installed on windows.

19
00:01:53,170 --> 00:01:58,900
‫At the end of the installation, we see the message set up was successful.

20
00:01:59,840 --> 00:02:03,790
‫Now, let's test whether the installation was successful.

21
00:02:05,270 --> 00:02:12,110
‫Open Windows ACMD or command line by simply typing SMD in your start menu.

22
00:02:13,090 --> 00:02:16,240
‫Then pipe by then and hit enter.

23
00:02:17,150 --> 00:02:21,730
‫You should get a similar output to the one you are seeing on the screen right now.

24
00:02:24,870 --> 00:02:30,810
‫OK, that's all at this moment, we have the latest version of Python installed.


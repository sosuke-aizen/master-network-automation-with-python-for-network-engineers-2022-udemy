﻿1
00:00:00,930 --> 00:00:08,460
‫In this lecture, I'll show you how to create an around a Python scripts in which a Python script is

2
00:00:08,460 --> 00:00:14,370
‫nothing more than a text file with the dot UI extension that contains Python code.

3
00:00:15,030 --> 00:00:23,130
‫Bicarb is probably the most popular integrated development environment or idea for Python.

4
00:00:23,580 --> 00:00:32,550
‫It includes great features such as excellent code completion and inspection with advanced debugger and

5
00:00:32,550 --> 00:00:38,520
‫support for web programming and various frameworks such as Django and Flask.

6
00:00:39,210 --> 00:00:47,130
‫We'll use bicarb throughout this course when we have to develop more complex python scripts with tens

7
00:00:47,130 --> 00:00:52,920
‫of lines of code or projects that consist of many python files or modules.

8
00:00:54,220 --> 00:00:56,410
‫Using by charm is simple.

9
00:00:57,520 --> 00:00:59,080
‫Let's open bicarb.

10
00:01:01,560 --> 00:01:09,210
‫After opening bicarb, you basically create one or more projects and inside each project you create

11
00:01:09,210 --> 00:01:17,600
‫Python scripts, which are text files with by extension a project is a directory in your file system.

12
00:01:18,360 --> 00:01:22,050
‫I'll create a project simply called My Python Project.

13
00:01:27,810 --> 00:01:35,190
‫By will create by default a virtual environment, which is an isolated working environment for each

14
00:01:35,190 --> 00:01:35,850
‫project.

15
00:01:39,230 --> 00:01:41,900
‫Bicarb is creating the project.

16
00:01:45,680 --> 00:01:52,910
‫After creating the project, I'll right click on the project to name then New and Python file.

17
00:01:53,950 --> 00:01:56,890
‫And I'll create a file called Script One.

18
00:01:58,110 --> 00:02:03,870
‫If I write a script one, it will create, in fact, a file called Script One Dot by.

19
00:02:06,140 --> 00:02:10,070
‫It will automatically add the dot by extension.

20
00:02:11,630 --> 00:02:19,490
‫This is my Python script now I'll write some Python code, don't focus on the code for this moment,

21
00:02:19,610 --> 00:02:23,450
‫but only on how to run the script using picture.

22
00:02:24,410 --> 00:02:36,590
‫And I write X equals five and print X star star five, it will print X raised to the power of five,

23
00:02:37,310 --> 00:02:40,400
‫in fact, five raised to the power of five.

24
00:02:42,590 --> 00:02:49,700
‫Comparing to Python Idol, where if you write the name of a variable, it will automatically print X

25
00:02:49,700 --> 00:02:57,680
‫value here in picture X mandatory to use the print function if you want to display the value of a variable.

26
00:02:58,580 --> 00:03:05,630
‫Now, I'll execute order on the script and the code will be run within the I.D. environment.

27
00:03:06,110 --> 00:03:07,700
‫There are several options.

28
00:03:08,700 --> 00:03:14,700
‫Right, click anywhere in the script and then left click on Iran and the name of the script.

29
00:03:16,250 --> 00:03:20,360
‫And we see the result here in its own console.

30
00:03:21,550 --> 00:03:28,750
‫Another option to run the script is to go to Iran and click on Iran in the name of the script.

31
00:03:29,700 --> 00:03:31,610
‫And I've got the same result.

32
00:03:32,880 --> 00:03:41,040
‫There is also a shortcut, which is by default control shift, and then, of course, you can change

33
00:03:41,040 --> 00:03:48,140
‫the shortcut or any other aspect of bicarb by going to file and settings.

34
00:03:50,660 --> 00:03:58,040
‫Motivated by charm in the course of KOTH analyses encounters any errors, they are immediately reported,

35
00:03:58,050 --> 00:04:03,530
‫for example, print why this is an error and I've done it on purpose.

36
00:04:04,930 --> 00:04:12,460
‫The code is underlined with the curvy line, and if I hover my mouse over these curvy line, detailed

37
00:04:12,460 --> 00:04:16,390
‫information about the error appears as a tooltip.

38
00:04:17,780 --> 00:04:24,560
‫In this case, unresolved reference, why I've used an undefined variable.

39
00:04:25,900 --> 00:04:34,150
‫And we can also see information about the error in the picture kind of soul, these were some basics

40
00:04:34,150 --> 00:04:41,560
‫about using bicarb, but you can find a lot of resources in the picture, documentation that is also

41
00:04:41,560 --> 00:04:43,450
‫attached to this lecture.


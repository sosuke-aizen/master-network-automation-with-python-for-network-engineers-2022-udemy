﻿1
00:00:01,350 --> 00:00:08,190
‫In this lecture I'd like to show you another way to execute Python scripts and the that's using the shell

2
00:00:08,280 --> 00:00:11,050
‫or the command line.

3
00:00:11,050 --> 00:00:17,040
‫This is especially useful when we connect to a remote machine or set of our using for example

4
00:00:17,050 --> 00:00:23,950
‫SSH andrun the Python script on that machine; we don't have access to a graphical environment

5
00:00:24,010 --> 00:00:28,390
‫to open Python idle or PyCharm to execute the script.

6
00:00:28,390 --> 00:00:32,520
‫In fact this is the only way to execute the script.

7
00:00:32,530 --> 00:00:42,780
‫In this case let's create a Python script using any editor like notepad or even PyCharm.

8
00:00:42,800 --> 00:00:50,310
‫in this example I use an editor called notepad plus plus; here inside the editor

9
00:00:50,330 --> 00:00:56,510
‫I write some Python instructions and then save the script on my desktop.

10
00:00:56,510 --> 00:01:03,690
‫Let's say a =4 and print a multiplied by four.

11
00:01:05,200 --> 00:01:10,190
‫I am saving the script on my desktop as script to dot P Y.

12
00:01:12,560 --> 00:01:24,160
‫Now I'll open CMD or command prompt so CMD go to desktop using the cd command and check the content of

13
00:01:24,160 --> 00:01:27,040
‫the current directory by running the dir command.

14
00:01:28,310 --> 00:01:36,110
‫OK there is a script called script 2 and then run Python and the name of the script.

15
00:01:36,110 --> 00:01:38,230
‫Script2.py

16
00:01:40,150 --> 00:01:42,370
‫And we see here the result.

17
00:01:42,510 --> 00:01:49,890
‫So a which is 4 multiplied by 4;  The script has been executed.

18
00:01:49,980 --> 00:01:56,560
‫If you are not in the same directory with the Python script you must use a valid, absolute or relative

19
00:01:56,640 --> 00:01:57,010
‫path.

20
00:01:57,450 --> 00:02:00,270
‫Otherwise you'll get an error.

21
00:02:00,270 --> 00:02:05,460
‫For example I'll change the directory and try to run the script again.

22
00:02:07,070 --> 00:02:14,840
‫And there is an error: " No such file or directory" and the correct way to execute this script is Python

23
00:02:15,200 --> 00:02:16,940
‫and a valid path.

24
00:02:17,660 --> 00:02:25,820
‫So desktop slash script2.py and we see the same result.

25
00:02:27,090 --> 00:02:29,540
‫Also take care that in Linux

26
00:02:29,550 --> 00:02:32,800
‫there could be more versions of Python installed.

27
00:02:33,000 --> 00:02:39,910
‫In this case you should choose the desired Python version and execute the script using that version.

28
00:02:39,970 --> 00:02:41,680
‫Let's see an example.

29
00:02:41,910 --> 00:02:45,940
‫I have an SSH connection to a Linux machine and here

30
00:02:45,960 --> 00:02:48,850
‫I have a Python script.

31
00:02:48,940 --> 00:02:57,460
‫This is the Python script and I'll write Python and I'll press 2 times on the tab key and I see there

32
00:02:57,460 --> 00:03:01,080
‫are many versions of Python: Python 2,

33
00:03:01,210 --> 00:03:11,530
‫Python 2.7 , Python3.5 and so on. And I'll execute the script using Phyton3.7

34
00:03:11,530 --> 00:03:14,860
‫Python3.7 and the name of the script.

35
00:03:16,880 --> 00:03:19,790
‫And the script has been executed.

36
00:03:19,790 --> 00:03:28,760
‫If I try to execute the script using a wrong version for example Python 2 I could get an error.

37
00:03:29,060 --> 00:03:36,140
‫In this case there is no error because this is a very simple Python script that works both in Python2

38
00:03:36,140 --> 00:03:38,000
‫and Python 3.




####  **Installing GNS3 on Linux**

These instructions are for Ubuntu (64-bit only) and all distributions based on
it (like Linux Mint). **To install GNS3 on Linux you need a non-root user with
sudo privilege.**

  

 **1\. In the terminal run the following commands as a non-root user with sudo
privilege:**

 _sudo add-apt-repository ppa:gns3/ppa_

 _sudo dpkg --add-architecture i386_

 _sudo apt update_

 _sudo apt install gns3-gui gns3-server dynamips gns3-iou_

  

When prompted whether non-root users should be allowed to use **Wireshark**
and **ubridge** , select **‘Yes’** both times.

Note: If you run GNS3 on Linux it’s not necessary to install the GNS3 VM like
in Windows.

  

 **2\. Add your user to the ubridge group, then log out from the Graphical
Environment and log in again.**

 _sudo usermod -aG ubridge your_username_

  

####  **Running Cisco IOU in GNS3 on Linux**

In order to run Cisco IOU in GNS3 on Linux follow the steps:

  

 **1\. Execute the Python license script for IOU**

$python3 license-python3.py

 **OUTPUT:**

*********************************************************************

Cisco IOU License Generator - Kal 2011, python port of 2006 C version

hostid=007f0101, hostname=andrei-hp, ioukey=7f0479

Add the following text to ~/.iourc:

 _[license]_

andrei-hp = f260872a107fc18e;

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Already copy to the file iourc.txt

You can disable the phone home feature with something like:

echo '127.0.0.127 xml.cisco.com' >> /etc/hosts

  

 **2\. Create a file called .iourc in the user’s home directory and paste the
two lines with the license from the previous script execution.**

In this example:

 _[license]_

 _andrei-hp = f260872a107fc18e;_

  

 **3\. Open GNS3 and go to Edit - > Preferences -> IOU on Unix**

Using **Browse** select the file called .iourc from the previous step.

  

![]()

  

 **4\. In GNS3 go to Edit - > Preferences -> IOU on Unix** and click on
**New**

Give the image a name (e.g. Cisco IOU L3), choose **New Image** , **Type L3
Image,** and using Browse **select the IOU L3 image on the disk**. Click
Finish.

Allow it to copy the image in the default image directory when asking.

![]()

  

 **4\. Add the execution permission to the image file**

Execute: _chmod +x ~/GNS3/images/IOU/i86bi-
linux-l3-adventerprisek9-ms.155-2.T.bin_

Note: replace the above file with the iou file on your disk

  

####  **Connecting to the Networking Devices that run in GNS3 from Guest OS**

You can [download and import this small project
](https://drive.google.com/file/d/1KNxJdkpzHNXeVQFUOGvOtkhigwlwPEBP/view?usp=sharing)in
GNS3 or even better create your own.

1\. Create a New Project in GNS3

2\. Drag & Drop a **Nat Cloud** to the project workspace. This is in fact your
Guest OS (Linux).

 **Important:** If you get an error related to the **virbr0 interface** when
adding the Nat Cloud to the project, install and enable firewalld. This is
required for the virbr0 interface in some cases.

  

 _sudo apt install firewalld_

 _sudo systemctl enable firewalld_

Restart the system and add the Nat Cloud again to the project.

  

3\. Drag & Drop IOU L3 devices and Ethernet Switches to the project workspace.
Connect them according to your desired topology.

Configure the devices with IP addresses and enable SSH and Telnet.

You can now ping and connect to the devices using SSH and Telnet from your
Guest OS (Linux)

  

 **Important:** By default, the network address of the GNS3 network behind the
Nat Cloud is 192.168.122.0/24

Give the devices IPs in this network. 192.168.122.10/24, 192.168.122.20/24,
etc are good for L3 IOU devices.

  

Example of a topology that can be used in this course:

![]()

  

 **Note** : It was reported in some versions of GNS3 that adding an Ethernet
Switch breaks the connection between IOU L2 and Nat Cloud. This is supposed to
be a bug. Replace the Switch with a Hub in this case.




 **Installing Python 3 on Linux (Debian Based Distros like Ubuntu, Linux Mint,
etc)**

  * If you are using Linux (and I encourage you to use it for increased security and stability), Python 3 is already installed.

  *  **Python IDLE**  is probably not installed, but you can install it quite simply by typing in terminal:  **sudo apt-get install idle3**

  * You can check what version of Python is already installed by typing in a terminal: **python3 --version**

  * Ubuntu and other Debian-based Distros like for example Linux Mint don't include the latest Python 3 version. 

You can use Felix Krull's deadsnakes PPA at
<https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa>

In a terminal run the following commands:

 ** _sudo add-apt-repository ppa:deadsnakes/ppa_**

 ** _sudo apt-get update_**

 ** _sudo apt-get install python3.7 idle-python3.7 python3-pip_**

  

  

 **Installing Python 3 on Mac**

There are basically 2 ways to install Python 3 on Mac:

 **1.** Go to <https://www.python.org>, download the latest version of Python
for Mac, click the installation package you've just downloaded, and install
it. The installation process is straight-forward.

 **2.** Use the [Homebrew](https://brew.sh/) package manager. This approach is
also recommended by community guides like [The Hitchhiker’s Guide to
Python](https://docs.python-guide.org/starting/install3/osx/).

For detailed instructions follow this guide:
<https://realpython.com/installing-python/#macos-mac-os-x>

  

After installing Python 3, check if it was installed successfully by opening a
terminal, type **python3,** and hit Enter.

If the Python Interpreter started then the installation was successful.

  

 **Note:**  If you run the **python** command and not **python3** it's
possible that it is linked to Python 2, not Python 3 and Python 2 interpreter
will start. This course is entirely based on Python 3, so take care to use
only the version 3.

  

 **Installing Python on Mobile Devices**

You can also install and run Python on Android and Apple (iOS) devices.

Follow the instructions in the article below for installing Python on Mobile
Devices.

  

 **Note:**  I'd strongly recommend going through this course using a laptop or
a PC since we're going to work with a lot of code and keeping the pace is
important.

 **Watching this training on a smartphone is not recommended because of
readability issues.**


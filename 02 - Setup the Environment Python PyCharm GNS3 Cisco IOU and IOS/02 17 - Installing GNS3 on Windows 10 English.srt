﻿1
00:00:01,030 --> 00:00:06,850
‫In this lecture we'll take a deeper look at how to install GNS3 on Windows 10.

2
00:00:06,850 --> 00:00:14,740
‫I'd recommend you to start with a fresh installation. Please uninstall any GNS3 versions that

3
00:00:14,740 --> 00:00:23,050
‫could have been prior installed. Most modern computers run on a 64 bit operating system

4
00:00:23,130 --> 00:00:32,370
‫so we are discussing about installing the 64 bit version of GNS3. The latest release versions

5
00:00:32,490 --> 00:00:43,170
‫of GNS3 support only 64 bit operating systems. If you are using an older CPU that runs 32 bit

6
00:00:43,230 --> 00:00:51,320
‫instructions or you have installed on your computer a 32 bit operating system you must read

7
00:00:51,440 --> 00:00:57,540
‫this installation guide. You'll find it as a resource to this lecture.

8
00:00:58,680 --> 00:01:07,870
‫Let's move on and see what are some minimum requirements for a Windows GNS3 environment.

9
00:01:07,870 --> 00:01:17,590
‫Your computer needs to have at least 4 GB of RAM, 1 GB of storage and 2 CPU cores.

10
00:01:17,890 --> 00:01:24,230
‫Even though it's not required an SSD disk would be very helpful.

11
00:01:24,240 --> 00:01:29,080
‫You can see that the GNS3 topology consumes some resources.

12
00:01:29,220 --> 00:01:36,150
‫The hardware requirements listed here are the minimum requirements for a small GNS3

13
00:01:36,150 --> 00:01:37,380
‫environment.

14
00:01:37,530 --> 00:01:44,850
‫If you want to create complex environments with many devices your hardware requirements will increase

15
00:01:46,320 --> 00:01:54,390
‫From my own experience I can tell you that you can run a topology with some Cisco devices without consuming

16
00:01:54,540 --> 00:01:56,230
‫a lot of resources,

17
00:01:56,280 --> 00:02:01,670
‫if you run Cisco IOU or IOS on Unix.

18
00:02:01,980 --> 00:02:08,150
‫These are special images created by Cisco for labs in Cisco Learning Center.

19
00:02:09,440 --> 00:02:13,670
‫I'll show you in another lecture,  how to install and run

20
00:02:13,670 --> 00:02:20,560
‫Cisco IOU in GNS3 on Windows 10. Ok. Let’s return to our installation.

21
00:02:20,570 --> 00:02:28,340
‫The first step is to download GNS3 all in one for Windows from gns3.com website.

22
00:02:28,340 --> 00:02:38,290
‫In order to download the GNS3 an account is required.

23
00:02:38,290 --> 00:02:41,890
‫Please create a free account with an email address,

24
00:02:41,890 --> 00:02:51,410
‫if you haven't already got one,  it will take you just one minute.  I'm going to download the GNS3 all in one for Windows 10.

25
00:02:51,500 --> 00:03:00,820
‫And the download has started.

26
00:03:00,820 --> 00:03:06,890
‫In this lecture when I do something that takes a longer time I'll pause the recording just to save some time.

27
00:03:07,060 --> 00:03:13,790
‫So don't be surprised if some operations take more time than you see in this video.

28
00:03:14,040 --> 00:03:19,720
‫GNS3 for Windows was downloaded and I'll click on the installation kit.

29
00:03:22,740 --> 00:03:25,080
‫I'll click next to continue.

30
00:03:25,080 --> 00:03:34,470
‫I accept the license agreement and other default options. Okay!  During the installation GNS3

31
00:03:34,470 --> 00:03:42,840
‫will download and install additional software like Wireshark, Dynamis, Solarwinds, Microsoft Visual,

32
00:03:42,840 --> 00:03:46,790
‫C++ Redistribute and many more.

33
00:03:46,800 --> 00:03:53,910
‫My advice is to install these applications because they are needed for different purposes and I've noticed

34
00:03:54,000 --> 00:04:00,560
‫that if you don't install them at the beginning you can have later other issues with GNS3.

35
00:04:00,580 --> 00:04:05,650
‫So if you are unsure what to install

36
00:04:05,650 --> 00:04:09,580
‫leave all software selections at their defaults.

37
00:04:09,880 --> 00:04:13,550
‫I'll click on Next and then on Install.

38
00:04:17,340 --> 00:04:19,800
‫GNS3 is being installed.

39
00:04:28,810 --> 00:04:30,290
‫I'll close this window.

40
00:04:31,830 --> 00:04:40,230
‫And next is asking if I want a free license for solarwinds and I'll click on yes

41
00:04:45,010 --> 00:04:46,090
‫okay.

42
00:04:46,200 --> 00:04:49,880
‫GNS3 has been successfully installed on my Windows 10.

43
00:04:50,610 --> 00:04:52,340
‫I don't want to start

44
00:04:52,440 --> 00:04:55,440
‫GNS3 at the moment; and finish!

45
00:04:59,840 --> 00:05:07,150
‫The next step is to download and install GNS3 VM. At this address

46
00:05:07,180 --> 00:05:16,620
‫https://gns3.com/software I click on download VM for GNS3. This is recommended in most

47
00:05:16,620 --> 00:05:25,880
‫situations when you are using Windows or MacOS. Installing GNS3 all in one alone is not enough,

48
00:05:26,010 --> 00:05:35,200
‫there are VMs for both VirtualBox and VmWare.  In this tutorial I'll use GNS3 VM for VirtualBox.

49
00:05:35,210 --> 00:05:36,430
‫.

50
00:05:36,900 --> 00:05:45,790
‫This GNS3 VM is in fact an Ubuntu Linux distribution that will be run on VirtualBox. So I

51
00:05:45,790 --> 00:05:50,880
‫am downloading the virtual box VM.

52
00:05:50,920 --> 00:05:55,690
‫This is a zip archive of type OVA that will be imported

53
00:05:55,710 --> 00:05:59,890
‫in VirtualBox as an appliance. We'll do that in a minute.

54
00:06:02,510 --> 00:06:08,750
‫By the way, if you are running GNS3 on Linux this step is not required.

55
00:06:08,750 --> 00:06:19,330
‫You don't have to install any VM. GNS3 VM is downloading. The next step is to download and install

56
00:06:19,330 --> 00:06:27,930
‫Virtual Box or VM Ware depending on what type of VM you have selected at the previous step.

57
00:06:27,930 --> 00:06:33,240
‫Virtual Box is free software and can be downloaded from this address.

58
00:06:37,870 --> 00:06:40,780
‫Download virtual box for your operating system,

59
00:06:40,810 --> 00:06:48,320
‫in this case for Windows, and install it. I've already installed it and I won't do it again.

60
00:06:48,370 --> 00:06:55,750
‫The installation process is straightforward, just accept the default options and press on Next button

61
00:06:55,960 --> 00:07:04,960
‫a few times. The next step is to import the GNS3 VM we've just downloaded in VirtualBox.

62
00:07:05,050 --> 00:07:12,530
‫That’s a zip archive of type OVA.  And I am extracting the archive on my desktop.

63
00:07:27,200 --> 00:07:39,080
‫I’m opening VirtualBox then go to File -> Import Appliance.
‫And I’m selecting the OVA file I’ve just extracted

64
00:07:39,090 --> 00:07:41,250
‫on my Desktop

65
00:07:46,670 --> 00:07:52,390
‫and next and finally I click on import

66
00:07:59,120 --> 00:07:59,800
‫OK.

67
00:07:59,820 --> 00:08:03,300
‫It has been imported successfully.

68
00:08:03,300 --> 00:08:05,100
‫I'm starting GNS3

69
00:08:12,210 --> 00:08:22,410
‫The GNS GUI has started and I'll select run applications in a virtual machine and next, host the binding

70
00:08:22,680 --> 00:08:32,760
‫local host and the port will be 3080 of type TCP and Next. I'll click one more time on Next

71
00:08:35,450 --> 00:08:36,920
‫and I've got this error.

72
00:08:36,920 --> 00:08:44,470
‫I wanted this error on purpose for you to see it. Sometimes even though you are using Virtual box it's

73
00:08:44,570 --> 00:08:48,140
‫asking for a specific VMWare package.

74
00:08:48,170 --> 00:08:52,430
‫So I'm going to download and install VIX API;

75
00:09:00,020 --> 00:09:02,010
‫If you don't get that error

76
00:09:02,060 --> 00:09:07,560
‫you can skip this step; download SDK 1.15

77
00:09:13,640 --> 00:09:17,030
‫it's downloading and then I will install it

78
00:09:30,840 --> 00:09:31,400
‫okay.

79
00:09:31,490 --> 00:09:35,830
‫VMware VIX has been successfully installed

80
00:09:39,030 --> 00:09:42,890
‫I'm closing GNS3 and then started again

81
00:09:47,710 --> 00:10:00,510
‫I'm starting a GNS3 GUI again; I go to Edit -> Preferences -> GNS3 VM and click on Enable the

82
00:10:00,510 --> 00:10:10,780
‫GNS3 VM. If I cannot choose a virtualization engine from the list I'll click on okay and I'll go again to

83
00:10:10,900 --> 00:10:21,250
‫Edit ->Preferences -> GNS3 VM,  Enable the GNS3 VM and I'll select virtual box from the drop down list

84
00:10:21,910 --> 00:10:31,100
‫apply and okay. We can notice how GNS3 is automatically starting Virtual box;

85
00:10:34,990 --> 00:10:41,200
‫we don't need to manually start  Virtual Box and I'll click on okay.

86
00:10:43,490 --> 00:10:53,180
‫Perfect! GNS3 has been successfully installed and we can run Cisco IOS images or images from other vendors.

87
00:10:53,540 --> 00:11:01,280
‫That's all; you have the knowledge to install all GNS3 components on Windows 10.

88
00:11:01,340 --> 00:11:08,780
‫If you have any problems with the installation don't hesitate to ask me and I'll be more than glad to help you solve the issue.

89
00:11:09,470 --> 00:11:17,300
‫In the next lecture we'll take a look at how to run Cisco IOU or IOS on Unix in

90
00:11:17,390 --> 00:11:17,810
‫GNS3.


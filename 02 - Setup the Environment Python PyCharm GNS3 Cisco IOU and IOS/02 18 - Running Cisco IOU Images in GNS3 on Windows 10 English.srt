﻿1
00:00:01,570 --> 00:00:03,760
‫Hello guys and welcome back!

2
00:00:03,760 --> 00:00:08,270
‫In the last lecture we've seen how to install GNS3 on Windows 10.

3
00:00:08,270 --> 00:00:12,380
‫Let's go ahead and take a look at how to run Cisco IOU  in GNS3 on Windows.

4
00:00:12,420 --> 00:00:24,420
‫IOU comes from IOS on Unix and is a modified IOS used by Cisco for CCIE labs

5
00:00:24,420 --> 00:00:35,450
‫and virtual lab pods used in CISCO Learning Center. There are 2 IOU versions:

6
00:00:35,780 --> 00:00:43,790
‫L2 and L3. IOU images are resource friendly and can be run smoothly on a normal

7
00:00:43,820 --> 00:00:45,620
‫laptop or PC.

8
00:00:45,620 --> 00:00:56,320
‫They don't require as many resources as the normal IOS images do. Please be aware that due to legal requirements

9
00:00:56,560 --> 00:01:05,530
‫I am unable to provide you with IOS or IOU images. You will need to get your own images to use them

10
00:01:05,710 --> 00:01:08,290
‫with  GNS3.

11
00:01:08,320 --> 00:01:14,790
‫Now let's suppose you have acquired these images from Cisco and I'll show you how to run IOU

12
00:01:14,790 --> 00:01:18,640
‫images in GNS 3 on Windows 10.

13
00:01:18,780 --> 00:01:23,430
‫The first step is to generate a license for the IOU.

14
00:01:23,430 --> 00:01:32,340
‫Ok, let's do that! And I'll go to the Desktop directory that contains the IOU image.

15
00:01:32,390 --> 00:01:40,490
‫There is a python script that will generate the license and must be copied to Genesis free VM Janice

16
00:01:40,560 --> 00:01:44,750
‫GNS3 should be running. Let's start GNS3.

17
00:01:52,200 --> 00:02:03,880
‫You notice how GNS 3 is automatically starting GNS3 VM . This is GNS3 GUI and this is GNS3 VM.

18
00:02:03,910 --> 00:02:09,470
‫Let's see how we can connect to the VM machine.

19
00:02:09,500 --> 00:02:18,160
‫Remember that this VM machine is in fact an Ubuntu Linux system so we can connect to it using SSH.

20
00:02:18,430 --> 00:02:27,240
‫I'm going to use WinSCP which uses SSH behind the scenes to copy the Python licence file to GNS3 VM.

21
00:02:27,240 --> 00:02:38,680
‫WinSCP is free software and can be downloaded from this website. In the VM window

22
00:02:38,710 --> 00:02:43,240
‫we see the information required to connect to the machine.

23
00:02:43,280 --> 00:02:52,600
‫We anotice the IP address, the username and its password. I am opening  WinSCP and connecting

24
00:02:52,600 --> 00:02:59,570
‫to the VM using the information we saw in the previous window; so the IP address,

25
00:03:04,360 --> 00:03:11,640
‫the user is GNS3 and the password GNS3 and log in.

26
00:03:15,800 --> 00:03:22,800
‫Perfect! The authentication was successful and I am connected to the VM.

27
00:03:22,880 --> 00:03:31,290
‫I'm going to copy the Python license script in a specific directory; that specific directory is

28
00:03:31,340 --> 00:03:38,810
‫/opt/gns3/images/IOU

29
00:03:38,810 --> 00:03:42,030
‫If the directory does not exist I will create it.

30
00:03:44,640 --> 00:03:50,140
‫New -> Directory,  IOU.  Okay.

31
00:03:51,100 --> 00:03:58,450
‫I have both the Python license script and the IOU image in a directory on my desktop.

32
00:03:59,440 --> 00:04:03,830
‫This is my desktop and this is the Python license script.

33
00:04:05,090 --> 00:04:08,930
‫I'll copy it to that directory using drag and drop

34
00:04:12,130 --> 00:04:12,590
‫okay.

35
00:04:12,630 --> 00:04:17,950
‫It's done; the license file was copied to the VM.

36
00:04:18,030 --> 00:04:25,190
‫The next step is to log in GNS3 VM using SSH and execute the Python script I’ve just copied.

37
00:04:25,290 --> 00:04:30,000
‫In GNS3 VM window

38
00:04:30,000 --> 00:04:31,040
‫I'll select

39
00:04:31,170 --> 00:04:38,660
‫OK and then a new window will appear. In order to execute the Python script

40
00:04:38,680 --> 00:04:42,840
‫I'll open a shell - this option.

41
00:04:42,840 --> 00:04:44,050
‫Perfect.

42
00:04:44,050 --> 00:04:53,840
‫Now I'm moving to the directory where I've copied the Python license script so

43
00:04:53,880 --> 00:05:02,150
‫cd /opt/gns3/images/IOU in uppercase letters.

44
00:05:02,370 --> 00:05:11,080
‫Let's see the contents of the current directory! okay! The script is there and I'm gonna execute it like

45
00:05:11,080 --> 00:05:12,250
‫this.

46
00:05:12,250 --> 00:05:17,020
‫python 3, a whitespace and the name of the script

47
00:05:19,740 --> 00:05:28,930
‫The script has generated the information you are seeing right now; I am refreshing the directory in WinSCP

48
00:05:29,050 --> 00:05:43,630
‫A file called iourc.txt has been created in the current directory. Let's see its contents! cat and

49
00:05:44,350 --> 00:05:53,730
‫the file! Okay! The next step is to copy the generated file which is  iourc.txt on

50
00:05:53,730 --> 00:06:03,750
‫Windows! We'll need it forGNS3 GUI. I'm going to use WinSCP which is already connected to the VM to

51
00:06:03,750 --> 00:06:11,940
‫copy the generated file to Windows and I'll drag and drop the file to the desktop directory on Windows.

52
00:06:16,540 --> 00:06:28,430
‫Now in GNS3 I'll go to Edit -> Preferences -> IOS on Unix. I'll copy and paste the license file contents

53
00:06:28,490 --> 00:06:29,990
‫here, in this window.

54
00:06:33,100 --> 00:06:47,610
‫I am opening the file, select all, copy and paste. I'll also click on browse and I'll select the license

55
00:06:47,610 --> 00:07:01,120
‫file which iourc.txt , so desktop and the file, open and apply. The next step is to

56
00:07:01,120 --> 00:07:12,110
‫select an IOU image to use in GNS3. I'll go to IOU Devices, then click on New, Run this IOU device

57
00:07:12,110 --> 00:07:17,460
‫on the GNS3 VM and Next.

58
00:07:17,870 --> 00:07:28,410
‫And I'm giving a name to the device, let’s say Cisco IOU L3. This is in fact a router, its type is L3 Image and using Browse I’ll select the IOU image that’s on my desktop.

59
00:07:28,410 --> 00:07:41,510
‫That's the image. And we notice how its uploading the selected image to GNS3 VM.

60
00:07:41,510 --> 00:07:49,000
‫It's not necessary to copy it manually!

61
00:07:50,520 --> 00:07:56,380
‫Okay, it's done! Now I can use Cisco IOU devices in GNS3

62
00:07:56,400 --> 00:08:06,650
‫Apply and OK! Let's create a new project to test the configuration! I'm creating a new blank project

63
00:08:07,100 --> 00:08:18,200
‫and save it as test project ! On the left side I'll click on browse all devices and I'm selecting Cisco

64
00:08:18,320 --> 00:08:24,370
‫IOU L3; using drag and drop I'll add 2 routers to the project.

65
00:08:27,100 --> 00:08:29,740
‫And I’m connecting the routers using a new link.

66
00:08:33,190 --> 00:08:40,180
‫Now to test the L3 connection between the routers I need to first configure them.

67
00:08:40,180 --> 00:08:48,670
‫I'm starting the routers by clicking on this green button; now opening a console to each router. By the way

68
00:08:48,760 --> 00:08:56,110
‫there are many consoles available and you can change the default one by going to

69
00:08:56,140 --> 00:08:57,190
‫Edit -> Preferences and then

70
00:08:57,190 --> 00:08:58,110
‫Console Applications.

71
00:09:01,330 --> 00:09:10,600
‫You can choose from Putty, SecureCRT, Solar Putty, TeraTerm and so on. I am selecting Putty

72
00:09:13,490 --> 00:09:20,290
‫and clicking on this button I am opening a console to each device, to each router.

73
00:09:21,010 --> 00:09:27,740
‫Okay, let's return to routers and set up an IP address for each of them in the same network.

74
00:09:27,870 --> 00:09:33,300
‫I want to see the name of their interfaces and I'll write the IP addresses

75
00:09:33,330 --> 00:09:41,920
‫I'm going to configure. This will be 10.0.0.1 and this will be

76
00:09:41,940 --> 00:09:52,860
‫10.0.0.2 Let's configure the IP addresses config interface and its name e 0/0

77
00:09:52,860 --> 00:10:04,000
‫IP address and the IP address  10.0.0.1 and the subnet mask 255.255.255.0

78
00:10:04,020 --> 00:10:15,780
‫and no shut; no shut is enabling the interface; and I'm saving the configuration

79
00:10:16,080 --> 00:10:29,550
‫by executing do write;  and the second router config interface is e0/0 IP address

80
00:10:29,970 --> 00:10:34,470
‫10.0.0.2  with the same subnet mask

81
00:10:37,340 --> 00:10:42,410
‫and of course no shut; and I'm saving the configuration

82
00:10:44,890 --> 00:10:45,430
‫okay.

83
00:10:49,010 --> 00:10:56,060
‫From the enable mode I'm gonna ping the other router.  so ping 10.0.0.1

84
00:10:56,060 --> 00:11:07,000
‫and ping is working! Okay ! That's all! I've shown you how to run Cisco IOU images in  GNS3 GUI

85
00:11:07,060 --> 00:11:15,040
‫and GNS3 VM and how to successfully connect and configure L3 IOU images.


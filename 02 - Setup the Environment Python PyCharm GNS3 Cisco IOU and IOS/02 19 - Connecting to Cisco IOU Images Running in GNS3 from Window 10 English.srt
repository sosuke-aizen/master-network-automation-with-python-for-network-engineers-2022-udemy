﻿1
00:00:01,140 --> 00:00:02,850
‫Welcome back!

2
00:00:02,880 --> 00:00:09,930
‫In the last 2 lectures we've talked about how to install GNS3 on Windows and how to load and run Cisco IOU images in GNS3;

3
00:00:09,930 --> 00:00:17,820
‫In order to develop Python scripts for Network Automation

4
00:00:17,820 --> 00:00:26,370
‫you need to be able to connect from Python to networking devices running in GNS3. So in effect

5
00:00:26,430 --> 00:00:29,830
‫we need to communicate from our operating system,

6
00:00:29,940 --> 00:00:38,970
‫in this case from Windows, to the devices that run in GNS3.

7
00:00:38,970 --> 00:00:41,580
‫Let’s see how to configure Windows for that.

8
00:00:41,670 --> 00:00:47,040
‫The first step is to create a spatial loopback interface on Windows

9
00:00:47,040 --> 00:00:55,120
‫This interface will be used to communicate to devices running in GNS3.

10
00:00:55,190 --> 00:01:07,740
‫I am pressing the Windows Key and R key at the same time to open the run box and I type hdwwiz.

11
00:01:07,740 --> 00:01:12,450
‫This will open the Add Hardware Wizard window.

12
00:01:12,450 --> 00:01:22,450
‫It's recommended to run it as administrator and I'll click on Next; then I'll select install the hardware

13
00:01:22,630 --> 00:01:31,900
‫that I manually select from a list and next. From this list I search for network adapters and then

14
00:01:31,900 --> 00:01:33,100
‫click on Next

15
00:01:37,470 --> 00:01:40,090
‫I'm selecting Microsoft.

16
00:01:40,500 --> 00:01:50,680
‫And on the right side I'm selecting Microsoft KM test Loopback Adapter  and next; I'll click one more

17
00:01:50,680 --> 00:01:55,120
‫time on next to start installing the new network adapter.

18
00:01:58,680 --> 00:02:06,550
‫Okay! It's done, the new network adapter of type Loopback interface has been successfully installed.

19
00:02:06,590 --> 00:02:10,670
‫The next step is to open network connections.

20
00:02:10,700 --> 00:02:15,550
‫There are many ways to do that and I'll open it via run or command prompt.

21
00:02:15,560 --> 00:02:24,280
‫I am pressing the Windows Key and R Key at the same time to open the run box.

22
00:02:24,450 --> 00:02:38,050
‫Then I'm typing and a ncpa.cpl and I'm hitting enter. The network connections window is opening

23
00:02:38,140 --> 00:02:39,420
‫immediately.

24
00:02:39,460 --> 00:02:44,140
‫Another way to open network connections is to go to Control Panel -> Network And Internet -> View Network status and tasks and then -> Change adapter settings.

25
00:02:46,800 --> 00:02:57,800
‫Okay, in this

26
00:02:57,800 --> 00:02:58,500
‫window

27
00:02:58,550 --> 00:03:01,790
‫We can see Network Adaptor of type Loopback we’ve just installed.

28
00:03:01,880 --> 00:03:07,140
‫I’ll rename it to GNS3Loopback.

29
00:03:14,040 --> 00:03:15,250
‫At this point

30
00:03:15,300 --> 00:03:18,210
‫it could be necessary to restart the system.

31
00:03:18,210 --> 00:03:22,050
‫However in this video I won't restart it.

32
00:03:22,050 --> 00:03:27,160
‫I'll just continue to show you the remaining required steps.

33
00:03:27,260 --> 00:03:31,850
‫The next step is to configure an IP address for this adapter.

34
00:03:32,010 --> 00:03:38,230
‫I am clicking on the adapter using the right mouse button and properties.

35
00:03:40,220 --> 00:03:45,080
‫I’m selecting “Internet Protocol version 4” and  “properties”

36
00:03:46,710 --> 00:03:52,390
‫And use the following IP address: for the exemples in this course

37
00:03:52,410 --> 00:04:05,880
‫I'll choose 10.1.1.2 with a subnet mask of 255.255.255 OK and

38
00:04:06,090 --> 00:04:15,290
‫close; the loopback adapter has been configured.

39
00:04:15,430 --> 00:04:22,750
‫The next step is to open GNS3 and create a new project to test the configuration.

40
00:04:22,840 --> 00:04:27,250
‫I am stopping these routers and I am creating a new project

41
00:04:33,800 --> 00:04:42,820
‫I'll drag and drop a new Cisco IOU L3 device and a cloud to the project. When you are at the cloud

42
00:04:42,820 --> 00:04:45,340
‫it will ask you to choose a server.

43
00:04:45,340 --> 00:04:49,720
‫Please select Desktop, and not GNS3 VM

44
00:04:54,060 --> 00:04:59,020
‫This cloud is in fact the Windows machine that communicates to the GNS3

45
00:04:59,050 --> 00:05:01,590
‫devices.

46
00:05:01,660 --> 00:05:08,400
‫Finally I have to configure the cloud interface that is used to communicate to the devices.

47
00:05:08,530 --> 00:05:18,140
‫Right click on the cloud,  configure , show special Ethernet interfaces and from the drop down list I’ll add the GNS3 Loopback interface

48
00:05:18,130 --> 00:05:29,010
‫Apply and Ok. Let's connect the device to the cloud and then start

49
00:05:29,070 --> 00:05:29,670
‫the router.

50
00:05:34,490 --> 00:05:40,610
‫okay and I'm starting the router; After starting the router

51
00:05:40,710 --> 00:05:47,170
‫I'm opening a console and then configure an IP address on the interface that is connected to the cloud

52
00:05:49,050 --> 00:05:53,460
‫Okay That interface is 0/0.

53
00:05:53,460 --> 00:06:01,800
‫Remember that the IP address of the Loopback interface is 10.1.1.2 and its subnet mask

54
00:06:02,040 --> 00:06:04,470
‫is /24.

55
00:06:04,470 --> 00:06:10,890
‫And for the routers interface I'll set 10.1.1.10/24

56
00:06:13,460 --> 00:06:26,440
‫config t,  interface e0/0,  ip address 10.1.1.10  and the mask; and no shut

57
00:06:26,450 --> 00:06:31,810
‫to enable the interface.

58
00:06:31,990 --> 00:06:37,610
‫OK, let's check if the interface is up: do show ip interface brief

59
00:06:40,780 --> 00:06:51,690
‫OK the interface is up; let's check the L3 connectivity between Windows and the Cisco router. I am

60
00:06:51,690 --> 00:06:56,340
‫opening cmd.exe and then ping the router

61
00:07:00,250 --> 00:07:01,900
‫and it's working.

62
00:07:01,900 --> 00:07:09,210
‫We have successfully configured this topology; if you try to ping the Windows machine from the router

63
00:07:09,320 --> 00:07:18,140
‫it's possible not to work because by default the Windows Firewall drops incoming packets. Okay.

64
00:07:18,290 --> 00:07:21,430
‫This is the setup we'll use during the course.

65
00:07:21,440 --> 00:07:21,950
‫Thank you.


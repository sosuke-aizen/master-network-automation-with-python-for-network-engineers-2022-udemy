

The easiest way to install PyCharm on Linux (Ubuntu or Linux Mint) which
worked every time for me includes the following steps:

 **1.** Go to <https://www.jetbrains.com/pycharm/download/#section=linux>  
There are two versions available:  
 **a) Community Edition** \- Lightweight IDE for Python & Scientific
Development  
 **b) Professional Edition** \- Full-featured IDE for Python & Web
Development, has 30-day free trial and license is required

 **2.** Download the **Community Edition (CE)**.

**3.** By default it will save it in the **Downloads** directory of your Home
Directory(~/Downloads).

 **4.** Open a terminal

 **5.** Change the current working directory to the directory where the
PyCharm archive has been downloaded

 **6\. cd ~/Downloads/**

 **7.** Extract the contents of the tar archive by running:

 **tar -xzf pycharm-community-2018.3.5.tar.gz**

 **Note:**  Probably you'll download another version. Here you must change the
name of the tar archive to the actual name of the file you have downloaded.
Run  **ls** command to see the content of the current directory.

 **8.** Move to the directory that contains PyCharm (the directory created
after extracting the tar archive):

 **cd pycharm-community-2018.3.5**

 **9.** Move to the **bin/** directory:

 **cd bin**

 **10.** Check there is a file called **pycharm.sh** and execute it by
running:

 **bash pycharm.sh**

or:

 **./pycharm.sh**

  

 **11.** A window like this will open:

  

![](https://img-c.udemycdn.com/redactor/raw/2019-03-15_18-05-22-440c042095f6d1a41bed2401a4b8d47b.png)

  

Next, select the defaults options in the bottom left corner:

![](https://img-c.udemycdn.com/redactor/raw/2019-03-15_18-05-38-f6427ae63f3cbaf72cb6219425fa38a7.png)

  

  

We are done, create projects, and start working!

  

![](https://img-c.udemycdn.com/redactor/raw/2019-03-15_18-05-54-748c83485bd6ae76877a4bc33ee8e272.png)

  

![](https://img-c.udemycdn.com/redactor/raw/2019-03-15_18-06-07-5bd1ffe05d3d447639580478eaffa6c0.png)

  

  

Optional, we can create a **symlink** to open PyCharm easier, simply by typing
**pycharm** in terminal:

 **sudo ln -s ~/Download/pycharm-community-2018.3.5/bin/pycharm.sh
/usr/local/bin/pycharm**

  

  

 **PyCharm Installation on Mac**

  

 **1.** Download PyCharm for Mac

<https://www.jetbrains.com/pycharm/download/#section=mac>

**2.** Open the `**PyCharm-*.dmg**` the package that you've downloaded, and
drag PyCharm to the **Applications** folder.


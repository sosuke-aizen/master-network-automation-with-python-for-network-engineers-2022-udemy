

I know that the process of setting up the lab is not the easiest one and lots
of things could go wrong. But don't worry, I've got you covered!

You need to acquire the Cisco IOS/IOU image, set up GNS3, connect it to your
host OS, and do many other settings.

  

To speed things up, if you get into problems, there is still a solution, at
least a temporary one until you make your own lab environment work.

  

I’ve set up a Linux Mint VM that has everything set up correctly: GNS3, Cisco
IOU, all required network connections, SSH, Python, Pycharm, and so on.

  

Download this [OVA file](https://drive.google.com/file/d/11kpPkycVKChmYRtzIHx-
Fb34ESfxr9VW/view?usp=sharing), import it into VirtualBox and start the VM.

The login credentials are: **student/student**

  

On the desktop, you’ll find shortcuts to GNS3 and PyCharm. Open them and then
you are ready to go.

I’ve even created a project in GNS3 (File -> Open Project) with 3 Routers that
are already configured. You can ping and connect to them using SSH from your
Python scripts.

  

 **Linux Mint 20.3
OVA:**<https://drive.google.com/file/d/11kpPkycVKChmYRtzIHx-
Fb34ESfxr9VW/view?usp=sharing>


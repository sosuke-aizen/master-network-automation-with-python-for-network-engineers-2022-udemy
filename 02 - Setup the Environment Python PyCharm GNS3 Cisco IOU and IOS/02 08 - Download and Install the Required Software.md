

In order to be able to run and test all examples from this course, you will
need to download and install the following **free software.**

For the examples, within this course, you can use any OS you like (Windows,
Linux, Mac). They have been roughly tested on Linux and Windows.

  

 **1\. Python 3**

Download and Install **Python 3** for your Operating System from:
<https://www.python.org/downloads/>

There are Linux, Mac, and Windows versions.

  

Some OS-specific details:

 **Linux**

\- If you are using Linux (and I encourage you to use it) there are already
installed both Python 2.7 and Python 3

\- **Python IDLE**  is probably not installed, but you can install it quite
simply by typing in terminal:  **sudo apt-get install idle3**

\- You can check what version of Python is already installed by typing:
**python3 --version**

\- Ubuntu and other Debian-based Distros like for example Linux Mint don't
include the latest Python 3 version.

You can use Felix Krull's deadsnakes PPA at
<https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa>:

 ** _sudo add-apt-repository ppa:deadsnakes/ppa_**

 ** _sudo apt-get update_**

 ** _sudo apt-get install python3.7 idle-python3.7 python3-pip_**

  

 **Windows**

\- When installing the Windows Version of Python it's recommended to choose to
**add the Python interpreter to the system PATH**

  

 **2\. PyCharm**

PyCharm is one of the most used Python IDE. You can download and install the
**Community Edition** for **free** from
<https://www.jetbrains.com/pycharm/download>

  

**Installing on Windows:  **

In the next lecture, you'll find a video tutorial on how to install PyCharm on
Windows and how to run Python scripts from both PyCharm and the console. The
installation process is straight-forward.

  

 **Installing on Linux:**

If you are using Linux, after downloading the PyCharm Community Edition
archive and extracting it, using a terminal go to  **bin/** directory of
pycharm-community-edition (ex: from pycharm-community-2018.2.4) and run: **
./pycharm.sh**

If there is an error **ModuleNotFoundError: No module named 'distutils.core'**
you must also install the python3-distutils package:  **sudo apt-get install
python3-distutils**

  

 **3\. GNS3**

GNS3 can be downloaded for free from <https://gns3.com/software>

A free account is required.

  

 **Installing on Windows:**

In the next lecture, you'll find a video tutorial on how to install GNS3 on
Windows and how to run Cisco IOU Images.

  

 **Installing on Linux (Ubuntu, Linux Mint, etc):**

From the terminal run:

 ** _sudo add-apt-repository ppa:gns3/ppa_**

 ** _sudo dpkg --add-architecture i386_**

 ** _sudo apt-get update_**

 ** _sudo apt-get install gns3-gui gns3-iou dynamips_**

  

If you want to run Cisco IOU images in GNS3 in Linux it's very simple. You
must acquire a L3 IOU Image and the Python licence script.

a) In GNS3 go the Edit -> Preferences -> IOS on Unix -> IOU Device

Add a new image, select the IOU File (the filename is similar to i86bi-
linux-l3-adventerprisek9-15.4.1T.bin) as an L3 Image, and click ok.

Make the IOU file executable: chmod +x PATH_TO_IOU.bin

Example: chmod +x /home/your_username/GNS3/images/IOU/i86bi-
linux-l3-adventerprisek9-15.4.1T.bin

b) Run the Python license script and paste the license in GNS3 go the Edit ->
Preferences -> IOS on Unix.

  

 **Note:**  Unfortunately due to legal requirements, I am unable to provide
IOS  or IOU images. You will need to provide your own images to use with GNS3.
Contact Cisco for further details.

  

 **4\. VirtualBox**

You need to install VirtualBox only if you are running Windows and you like to
test the scripts that automate Linux Administration Tasks.

Download link: [https://www.virtualbox.org/wiki/Downloads
](https://www.virtualbox.org/wiki/Downloads)

  

**5\. Linux**

You need to install Linux in VirtualBox if you are using Windows and want to
test Python scripts that automate Linux administration tasks.

You could install in VirtualBox Ubuntu Server or Linux Mint.

<https://www.ubuntu.com/download/server>

[https://linuxmint.com/download_all.php
](https://linuxmint.com/download_all.php)

  

**Additional software needed to emulate networking devices in GNS3:**

 **1\. Cisco Images**

Unfortunately due to legal requirements, I am unable to provide IOS  images or
any other Cisco images. You will need to provide your own images to use with
GNS3.

  

**[OPTIONAL  SOFTWARE]**

 **2\. Arista vEOS**

You can download, install, and run for free Arista vEOS images in GNS3.

In another lecture, I'll explain in detail **How to Run Arista vEOS on GNS3.**
However, this course if based mainly on Cisco Devices, but configuring devices
from other vendors is similar.

 **3\. Juniper vSRX**

You can download, install, and run for free Juniper vSRX images in GNS3.

In another lecture, I'll explain in detail **How to Run Juniper vSRX on
GNS3.** However, this course is based mainly on Cisco Devices, but configuring
devices from other vendors is similar.


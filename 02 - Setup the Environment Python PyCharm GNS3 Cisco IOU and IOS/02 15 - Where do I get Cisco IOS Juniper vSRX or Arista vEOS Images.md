

In order to be able to run and test all concepts from this course, you will
need to emulate Networking Devices from different vendors like Cisco, Arista,
Juniper.

You can test it on physical devices you may have at home or work or you can
install and run the Devices OS in GNS3 (the most used approach).

  

 **1\. Cisco**

 **In another lecture I  have a video tutorial on how to setup and run GNS3
with Cisco IOU Images in Windows. **

Unfortunately due to legal requirements, I am unable to provide  IOS images or
any other Cisco images like IOU (IOS on Unix). You will need to provide your
own  images to use them with GNS3. However, I can assist you in getting the
required images. Please get in touch with me if you can not find the Cisco
images on your own.

Some vendors make their software images freely available, but unfortunately
this is not true for Cisco images.

**Options:**

Download images directly from Cisco:
<https://software.cisco.com/download/navigator.html>

**Note:  **A Service Contract is required for downloading of images.

If you don’t have a service contract with Cisco, you can purchase a  VIRL
license to get access to multiple images that are very useful for  GNS3 labs.
The cost of this varies from $79.99 to $199.99 per year:

<http://virl.cisco.com/getvirl/>

 **NOTE:** You can use VIRL images without license authentication. You can
therefore download the VIRL images, integrate  them with GNS3 and use them
offline if needed.

If you own a physical router Cisco router, you can copy the image from the
router and import it into GNS3.

  

 **2\. Arista vEOS**

You can download, install and run for free Arista vEOS images in GNS3.

In another lecture I'll explain in detail **How to Run Arista vEOS on GNS3.**
However, this course if based mainly on Cisco Devices, but configuring devices
from other vendors is similar.

  

 **3\. Juniper vSRX**

You can download, install and run for free Juniper vSRX images in GNS3.

In another lecture I'll explain in detail **How to Run Juniper vSRX on GNS3.**
However, this course if based mainly on Cisco Devices, but configuring devices
from other vendors is similar.


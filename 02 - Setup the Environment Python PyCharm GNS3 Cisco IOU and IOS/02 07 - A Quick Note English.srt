﻿1
00:00:00,460 --> 00:00:07,400
‫In this section I'll show you how to set up the development environment used in this course.

2
00:00:07,560 --> 00:00:11,890
‫I'll show you how to install Python 3, PyCharm IDE,

3
00:00:11,970 --> 00:00:18,840
‫I'll show you different ways of running Python scripts, how to install GNS3 VM on Windows 10

4
00:00:19,040 --> 00:00:27,570
‫and on Linux,  how to run Cisco  IOS on Unix or IOU on GNS3 VM on Windows 10 and how to

5
00:00:27,570 --> 00:00:31,730
‫connect from your host OS, such as for example in Windows 10,

6
00:00:31,740 --> 00:00:35,250
‫to your devices running inside GNS3.

7
00:00:36,390 --> 00:00:43,540
‫If you've already done this before and everything works smoothly you can skip this section.

8
00:00:43,550 --> 00:00:44,650
‫As a final test

9
00:00:44,670 --> 00:00:53,470
‫you must be able to ping and connect from your development OS to Cisco devices that run in

10
00:00:53,530 --> 00:00:53,860
‫GNS3.


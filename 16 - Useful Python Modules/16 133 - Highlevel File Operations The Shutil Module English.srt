﻿1
00:00:00,880 --> 00:00:09,210
‫Another useful module will take a look at these S.A.G. util or shuttle, it means Shell Utility's S.A.G.

2
00:00:09,210 --> 00:00:12,700
‫standing for Shell and util standing for utilities.

3
00:00:13,170 --> 00:00:20,970
‫This module offers a number of high level operations on file and directories such as copying, moving,

4
00:00:21,000 --> 00:00:22,710
‫renaming or deleting.

5
00:00:24,230 --> 00:00:32,420
‫Some file operations can also be done using the OS module, but the many Python programmers prefer to

6
00:00:32,420 --> 00:00:38,150
‫use shuttle because it hides the complexity of the operation.

7
00:00:39,110 --> 00:00:48,050
‫Shuttle functions are built on top of OS module, and each call may represent a lot of other calls to

8
00:00:48,050 --> 00:00:49,440
‫lower level functions.

9
00:00:50,180 --> 00:00:51,440
‫Let's try examples.

10
00:00:53,600 --> 00:01:02,450
‫Here on my desktop, there are two directories, A and B, and each directory contains a text file.

11
00:01:03,940 --> 00:01:11,110
‫There are more functions used to copy files and directories, the first function will take a look at

12
00:01:11,260 --> 00:01:12,280
‫each copy file.

13
00:01:12,730 --> 00:01:20,650
‫It copies the content of the file name source file to a file named destination file source file end

14
00:01:20,650 --> 00:01:25,030
‫destination file are valid path names given as things.

15
00:01:25,750 --> 00:01:34,100
‫Keep in mind that copy files function copies only the content of the file either metadata like the files

16
00:01:34,120 --> 00:01:40,000
‫permission smout the files creation and modification times are not preserved.

17
00:01:42,170 --> 00:01:45,350
‫First, I will import the shuttle module.

18
00:01:49,180 --> 00:01:52,150
‫And I'll shoot shuttle that copy file.

19
00:01:54,040 --> 00:02:01,780
‫The first argument is the source file and the second argument, the destination in this example, I'll

20
00:02:01,780 --> 00:02:09,760
‫copy file one dot, the D in B directory is file three, dot the.

21
00:02:17,560 --> 00:02:25,660
‫And we can see how it copied file one from a directory to be directory is file free.

22
00:02:29,170 --> 00:02:37,630
‫Another useful function is copy, it copies the file data in the files permission most other metadata

23
00:02:37,720 --> 00:02:41,740
‫like the files creation and modification times is not preserved.

24
00:02:42,040 --> 00:02:43,210
‫Lexia an example.

25
00:02:46,960 --> 00:02:56,650
‫So instead of shapefile, I use copy and I'll copy the same file one dot texte in B directory.

26
00:02:59,320 --> 00:03:01,520
‫And its name remains the same.

27
00:03:02,170 --> 00:03:08,270
‫Now, if we check the B directory, we can see if there is a file called file one dot texte.

28
00:03:09,690 --> 00:03:17,640
‫If you want to preserve all file of metadata from the original used copy to instead, this is identical

29
00:03:17,640 --> 00:03:25,470
‫to copy function, except that it also attempts to preserve file metadata like the files creation and

30
00:03:25,470 --> 00:03:26,790
‫modification times.

31
00:03:27,250 --> 00:03:34,200
‫Take care that even the high level function copy too, cannot copy all file metadata.

32
00:03:35,340 --> 00:03:42,120
‫On Windows Linux, our Mac information like File, Onlar or ECLSS are not copied.

33
00:03:43,490 --> 00:03:52,340
‫I'll copy the same file in the same directory, so the destination is a directory as, let's say file

34
00:03:52,340 --> 00:03:53,540
‫to dot the.

35
00:03:55,890 --> 00:03:59,190
‫And I'll use copy to instead of copy.

36
00:04:01,630 --> 00:04:03,970
‫Now, let's check the directory.

37
00:04:06,540 --> 00:04:11,310
‫And they can see how a new file called File to dot text appeared.

38
00:04:12,930 --> 00:04:19,890
‫If you want to copy an entire directory, use the copy three function, it recursively copies an entire

39
00:04:19,890 --> 00:04:20,790
‫directory tree.

40
00:04:21,240 --> 00:04:24,780
‫The destination directory must not already exist.

41
00:04:25,030 --> 00:04:32,700
‫It will be created as well as missing parent directories, permissions and types of directories are

42
00:04:32,700 --> 00:04:41,970
‫copied with copy start function and individual files are copied using copy to function like an example.

43
00:04:43,360 --> 00:04:47,800
‫I copied a directory called A in the directory called B.

44
00:04:47,950 --> 00:04:48,970
‫S C.

45
00:04:50,610 --> 00:04:53,010
‫So should deal that copy three.

46
00:04:54,850 --> 00:04:56,380
‫Now, the source directory.

47
00:04:57,290 --> 00:04:58,180
‫This is a.

48
00:05:03,350 --> 00:05:08,540
‫And the destination directory is a new directory called C inside B.

49
00:05:15,210 --> 00:05:17,880
‫OK, let's check the B directory.

50
00:05:19,210 --> 00:05:22,060
‫And there is a new directory called C.

51
00:05:25,460 --> 00:05:33,080
‫Another useful function is move it recursively moves a file or directory to another location.

52
00:05:33,320 --> 00:05:39,640
‫If the destination is an existing directory, then the source is moved inside that directory.

53
00:05:39,770 --> 00:05:45,590
‫And if the source and the destination are in the same directory, then the source is renamed.

54
00:05:46,700 --> 00:05:50,330
‫Let's see two examples in the first example.

55
00:05:50,450 --> 00:05:54,410
‫I'll rename the directory to X.

56
00:06:00,310 --> 00:06:06,310
‫This is the directory that will be renamed and its new name will be X.

57
00:06:11,880 --> 00:06:19,130
‫Let's check it and we can see how on my desktop, a new directory called X appeared.

58
00:06:19,140 --> 00:06:24,360
‫In fact, this is the old A directory that has been renamed.

59
00:06:27,310 --> 00:06:32,140
‫Now, let's move the directory into B directory.

60
00:06:35,100 --> 00:06:41,370
‫This is the source and now the destination, I'll move it inside beat directory.

61
00:06:44,030 --> 00:06:52,220
‫Now, on my desktop, there is no directory, but inside B, if there is a directory called X, this

62
00:06:52,220 --> 00:06:54,380
‫is the directory that has been moved.

63
00:06:56,180 --> 00:07:03,140
‫In the last example of this tutorial, I'll show you how to remove a directory for that, we use the

64
00:07:03,170 --> 00:07:07,670
‫our M3 function, which deletes an entire three three.

65
00:07:08,980 --> 00:07:10,990
‫Let's delete the beat directory.

66
00:07:13,900 --> 00:07:16,360
‫Should deal, but I am three.

67
00:07:17,910 --> 00:07:20,370
‫And the directory that will be removed.

68
00:07:23,680 --> 00:07:29,080
‫Now, on my desktop, there is no directory, it has been removed.

69
00:07:31,160 --> 00:07:39,440
‫These are the most used cases, but if you want to see other examples, please check the module oficial

70
00:07:39,440 --> 00:07:40,370
‫documentation.

71
00:07:41,470 --> 00:07:42,040
‫Thank you.


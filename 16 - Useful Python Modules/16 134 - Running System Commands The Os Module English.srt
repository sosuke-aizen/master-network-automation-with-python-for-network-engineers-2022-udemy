﻿1
00:00:00,770 --> 00:00:07,790
‫In this lecture, I'll show you how to execute the system coming from Python scripts while making a

2
00:00:07,790 --> 00:00:08,910
‫program in Python.

3
00:00:09,110 --> 00:00:12,440
‫You may need to execute some shell companies for a program.

4
00:00:13,310 --> 00:00:18,920
‫Running external or shell comments is very popular with Python developers.

5
00:00:19,310 --> 00:00:22,300
‫There are two modules that can be used for this task.

6
00:00:22,580 --> 00:00:25,790
‫The OS and the subprocess modules.

7
00:00:26,000 --> 00:00:31,540
‫Let's see the first example on how to execute the command using the OS module.

8
00:00:32,160 --> 00:00:35,830
‫There is the be open function that can be used.

9
00:00:36,230 --> 00:00:44,900
‫It opens a pipe to or from command and returns a value which is the open file object connected to the

10
00:00:44,900 --> 00:00:53,150
‫pipe import os os dot be open off and the command is argument.

11
00:00:54,670 --> 00:01:00,130
‫For example, AAFP minus a I want to see the ARP table.

12
00:01:02,430 --> 00:01:08,100
‫And it returned a special file object connected to a pipe that can be at.

13
00:01:09,550 --> 00:01:15,970
‫I create a variable like, say, output equals and the above be open function.

14
00:01:17,790 --> 00:01:22,770
‫In order to read from this object, we use the right method.

15
00:01:25,610 --> 00:01:29,640
‫And I'll print the value stored in the output variable.

16
00:01:29,810 --> 00:01:31,400
‫So print output.

17
00:01:33,340 --> 00:01:35,680
‫And this is the table.

18
00:01:37,530 --> 00:01:43,920
‫This function works on Windows, Linux or Mac, Lexy, an example on Linux.

19
00:01:45,630 --> 00:01:57,840
‫This is a python interpreter opened on a Linux machine and here I write import OS output equals os dot

20
00:01:57,840 --> 00:02:05,400
‫p open the command ls minus L forward slash etsi.

21
00:02:07,060 --> 00:02:08,110
‫Don't read.

22
00:02:10,640 --> 00:02:12,410
‫And print output.

23
00:02:15,880 --> 00:02:18,520
‫Sorry, I've misspelled the variable.

24
00:02:20,990 --> 00:02:22,250
‫So without the.

25
00:02:26,010 --> 00:02:28,020
‫And this is the output.

26
00:02:30,160 --> 00:02:38,710
‫There is also the OS daughter system function that can be used to run commands, but it's not the recommended

27
00:02:38,710 --> 00:02:42,170
‫way, according to the official Python documentation.

28
00:02:42,410 --> 00:02:49,480
‫However, Lexcen example, I am going to create an empty text file on my desktop.

29
00:02:50,480 --> 00:02:59,330
‫OS Daudt system, and now the comment, this comment will create an empty text file on my desktop.

30
00:03:09,430 --> 00:03:17,320
‫This is the error status of the command, and zero means no error now on my desktop.

31
00:03:17,830 --> 00:03:20,860
‫There is a new file called ABC The.

32
00:03:22,620 --> 00:03:30,730
‫OK, in this lecture, we've seen how to execute system commands using OS module in the next lecture.

33
00:03:30,990 --> 00:03:37,290
‫We'll take a look at how to execute the shortcomings using another module called subprocess.

34
00:03:37,800 --> 00:03:39,780
‫See you in just a few seconds.


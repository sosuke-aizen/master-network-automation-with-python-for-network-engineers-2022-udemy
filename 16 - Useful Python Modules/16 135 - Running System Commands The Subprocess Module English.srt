﻿1
00:00:01,520 --> 00:00:08,690
‫Another way to run the system coming from Python scripts is to use the subprocess module, and this

2
00:00:08,690 --> 00:00:09,860
‫is the recommended way.

3
00:00:10,310 --> 00:00:18,530
‫The subprocess module provides more powerful facilities for spawning new processes and retrieving their

4
00:00:18,620 --> 00:00:19,250
‫results.

5
00:00:19,670 --> 00:00:24,400
‫Using subprocess is preferable to using the Ossa module.

6
00:00:24,950 --> 00:00:31,600
‫The advantage of subprocess versus system is that it is more flexible.

7
00:00:31,760 --> 00:00:39,550
‫We have access to standard output standardbearer the real status code and it offers better error handling.

8
00:00:39,890 --> 00:00:44,090
‫Let's see how to use subprocess to execute the system.

9
00:00:44,090 --> 00:00:44,690
‫Cumming's.

10
00:00:45,990 --> 00:00:52,710
‫The subprocess module offers many functions, but there are two important functions will take a look

11
00:00:52,710 --> 00:00:56,010
‫at the archaic call and check output.

12
00:00:56,370 --> 00:01:03,900
‫The first function check call across the command with arguments and waits for the command to complete

13
00:01:04,230 --> 00:01:06,550
‫if the return code was zero.

14
00:01:06,750 --> 00:01:10,440
‫It means that there was no error and it returns.

15
00:01:10,740 --> 00:01:15,060
‫Otherwise, it raises called process error exception.

16
00:01:16,140 --> 00:01:24,450
‫The second function check output is similar to check call and moreover, it also returns the comment

17
00:01:24,450 --> 00:01:25,040
‫output.

18
00:01:25,230 --> 00:01:33,780
‫So as a conclusion, we use check Cole to run a command and check output, to run a comment and capture

19
00:01:33,810 --> 00:01:34,640
‫its output.

20
00:01:35,750 --> 00:01:44,690
‫The argument of chick output function is at least that has as elements the comment and the arguments.

21
00:01:45,530 --> 00:01:53,480
‫Lexy, an example I create a variable called Samed equals at least AAFP.

22
00:01:54,620 --> 00:01:58,900
‫And the second element, the command argument.

23
00:01:59,450 --> 00:02:00,770
‫So minus a.

24
00:02:03,270 --> 00:02:12,210
‫And now import subprocess, subprocess dot check underlying output of CMT.

25
00:02:15,980 --> 00:02:24,530
‫And it returned the output of the comment is an object of type pushbikes, we can convert it to a string

26
00:02:24,560 --> 00:02:26,270
‫using the Decode method.

27
00:02:27,700 --> 00:02:29,900
‫Let's see another example.

28
00:02:30,160 --> 00:02:34,810
‫Now we are going to execute the pink comment from Python.

29
00:02:36,220 --> 00:02:47,820
‫Let's see how the pink comment is structured so pink minus n the number of requests it will send like,

30
00:02:47,920 --> 00:02:54,170
‫say, to and the I.P. address, like, say, a 2.8 notepad dot eight.

31
00:02:54,550 --> 00:02:55,990
‫I am sending to a..

32
00:02:56,020 --> 00:02:58,060
‫Request page six to eight.

33
00:02:58,300 --> 00:03:00,340
‫Dot, dot, dot eight.

34
00:03:01,180 --> 00:03:05,980
‫By the way, this is the public DNS server of Google.

35
00:03:10,110 --> 00:03:22,920
‫So ACMD equals the first element being the second element minus N, the third element, too, and the

36
00:03:22,920 --> 00:03:25,200
‫last element of the Apricus.

37
00:03:29,090 --> 00:03:35,920
‫Output equals subprocess dot check output of CMT.

38
00:03:39,550 --> 00:03:42,340
‫Lexi, what is the type of output?

39
00:03:45,980 --> 00:03:50,210
‫And we can see an object of type buycks.

40
00:03:51,830 --> 00:03:56,420
‫If I want to convert it to a string, I use the Decode method.

41
00:03:58,010 --> 00:04:03,500
‫So astar underlying output equals output dot decode.

42
00:04:05,830 --> 00:04:09,730
‫And print is the underlying output.

43
00:04:12,130 --> 00:04:14,230
‫And this is the output.

44
00:04:17,770 --> 00:04:26,140
‫Of course, it works in the same memory also on other operating systems, like see an example on Linux,

45
00:04:26,890 --> 00:04:35,200
‫in this example, I am running the I-F config minus a comment that displays information about network

46
00:04:35,200 --> 00:04:36,130
‫interfaces.

47
00:04:55,370 --> 00:05:02,150
‫This is how you execute shall commence on Linux, Windows or Mac, thank you.


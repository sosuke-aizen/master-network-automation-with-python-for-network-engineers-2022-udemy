﻿1
00:00:00,580 --> 00:00:04,820
‫In the last lecture, we've seen some examples using Granges.

2
00:00:05,200 --> 00:00:07,970
‫Now I'd like to dive into some more details.

3
00:00:08,230 --> 00:00:10,180
‫We use ranges each time.

4
00:00:10,180 --> 00:00:14,120
‫We want our application to execute a repetitive task.

5
00:00:14,620 --> 00:00:20,260
‫Let's assume we have a lottery application and we need to pick three random winners.

6
00:00:20,710 --> 00:00:27,490
‫In this case, we could use a range and the loop to repeat three times the process of choosing a random

7
00:00:27,490 --> 00:00:28,030
‫winner.

8
00:00:28,600 --> 00:00:33,040
‫Let's say a simple example for X in a range of three.

9
00:00:34,290 --> 00:00:43,380
‫Colleen, it follows the four block of code and here I'm just printing a message, print F from F String

10
00:00:44,190 --> 00:00:45,360
‫Choo's weener.

11
00:00:46,340 --> 00:00:56,480
‫And between curly braces X, the temporary variable of my for loop, and it has executed three times

12
00:00:56,780 --> 00:01:05,450
‫the four block of code, in this case, the four block of code has only one instruction, but there

13
00:01:05,450 --> 00:01:11,510
‫could be more instructions as long as they use the same indentation level.

14
00:01:12,750 --> 00:01:20,490
‫Range is, in fact, a pipe that represents an immutable sequence of numbers and is commonly used for

15
00:01:20,490 --> 00:01:23,910
‫looping a specific number of times in four loops.

16
00:01:24,870 --> 00:01:33,660
‫There is a built in function called range that returns ranges of numbers, python ranges come in multiple

17
00:01:33,660 --> 00:01:34,170
‫forms.

18
00:01:34,530 --> 00:01:40,380
‫The general form of a range is the range of and we have three arguments.

19
00:01:40,980 --> 00:01:47,610
‫The start, the stop and the step by default start is zero and is included.

20
00:01:47,970 --> 00:01:54,410
‫Stop is excluded and by default step is one but better.

21
00:01:54,420 --> 00:01:56,280
‫Let's see some examples.

22
00:01:57,290 --> 00:02:03,290
‫I create a variable that stores, all right, let's say, are equal range of 10.

23
00:02:04,250 --> 00:02:12,170
‫In this case, the start is zero, this is the default, the stop is 10 and the step is one.

24
00:02:12,950 --> 00:02:18,950
‫If I want to display the numbers from the range, I must convert the range to at least.

25
00:02:20,230 --> 00:02:26,000
‫We'll talk a lot about leaks in a couple of lectures for this moment.

26
00:02:26,110 --> 00:02:31,090
‫You should know that reconvert arrange to at least by calling the list constructor.

27
00:02:31,090 --> 00:02:32,500
‫So least of our.

28
00:02:33,770 --> 00:02:43,310
‫And these are the numbers of migrants, we can see how zero is included and 10 is excluded.

29
00:02:45,050 --> 00:02:54,680
‫Lexia, another example are equals a range of 10, coma 20, now the start these 10 and these included

30
00:02:54,680 --> 00:02:57,410
‫in the stop is 20 and is excluded.

31
00:02:57,950 --> 00:03:03,410
‫Of course I released the numbers from 10 to 19.

32
00:03:04,250 --> 00:03:10,190
‫If I want to list only even numbers, I use here a step of two.

33
00:03:10,850 --> 00:03:17,300
‫This means from 10 to 20, 20 is excluded in steps of two.

34
00:03:20,530 --> 00:03:29,260
‫We can also have a negative step, an example, our equals range of the start argument is 100.

35
00:03:29,260 --> 00:03:34,780
‫The top argument is minus 50 and the step is minus three.

36
00:03:35,230 --> 00:03:43,570
‫In fact, I'll go backwards from 100 to minus 50 in steps of three.

37
00:03:46,730 --> 00:03:53,330
‫And these are the numbers, one hundred included, 50 excluded.

38
00:03:54,790 --> 00:04:01,150
‫If you want to use a wrench in a for loop, you don't need to convert that range to at least.

39
00:04:02,440 --> 00:04:15,910
‫So it's enough to write for X in R, R is orange print X, so here I'll display the numbers of the range

40
00:04:16,420 --> 00:04:18,460
‫and these are the numbers.

41
00:04:19,650 --> 00:04:22,330
‫This is all about changes for the moment.

42
00:04:22,530 --> 00:04:23,070
‫Thank you.


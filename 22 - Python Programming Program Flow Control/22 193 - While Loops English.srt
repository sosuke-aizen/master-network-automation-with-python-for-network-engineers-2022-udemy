﻿1
00:00:00,600 --> 00:00:08,280
‫In this lecture, we'll talk about while loops a while loop will continue to execute a block of code,

2
00:00:08,280 --> 00:00:16,650
‫while some test condition remains to the test, condition is some sort of boolean expression that returns

3
00:00:16,650 --> 00:00:23,690
‫true or false and the block of code will continue to execute while the condition remains true.

4
00:00:25,030 --> 00:00:33,700
‫In Python, a while, Loop can also have an El clause, then GICs executed when the while boolean condition

5
00:00:33,700 --> 00:00:35,560
‫is evaluated to force.

6
00:00:36,280 --> 00:00:42,520
‫So if some boolean condition is false, it will execute this block of code.

7
00:00:43,480 --> 00:00:45,090
‫Let's see some examples.

8
00:00:46,420 --> 00:00:50,530
‫I create a new Python script while one.

9
00:00:51,490 --> 00:01:02,140
‫Please avoid to use python keywords as filenames, so avoid to use Wilfork or EEF or other works as

10
00:01:02,290 --> 00:01:03,130
‫filenames.

11
00:01:04,970 --> 00:01:08,390
‫And he are X equals zero, and while.

12
00:01:09,910 --> 00:01:18,960
‫It's less than 10 Colen and the wild block of code follows using an indentation of four spaces inside

13
00:01:18,970 --> 00:01:22,930
‫the wild block of Code I imprinting the current value of X.

14
00:01:24,020 --> 00:01:30,860
‫So the current value of X is and between prices X.

15
00:01:32,810 --> 00:01:41,870
‫If I ran this code, it will execute the print statement over and over again and the X because X is

16
00:01:41,870 --> 00:01:44,600
‫zero and zero is less than 10.

17
00:01:45,320 --> 00:01:47,920
‫This is what is called an infinite loop.

18
00:01:48,170 --> 00:01:52,960
‫And in most cases, this is an error on the script.

19
00:01:54,050 --> 00:02:02,180
‫We can see how it continuously executes the print function and displays the current value of X is zero.

20
00:02:03,020 --> 00:02:10,850
‫If you run the script using the command line, you should press on control and see controversy here

21
00:02:10,850 --> 00:02:11,720
‫in bicarb.

22
00:02:11,870 --> 00:02:13,340
‫I'll press on stop.

23
00:02:14,350 --> 00:02:16,190
‫And the script has been stopped.

24
00:02:16,750 --> 00:02:21,740
‫Otherwise, if I don't stop the script manually, it will run forever.

25
00:02:22,390 --> 00:02:29,110
‫What I should do in order to get out of this infinite loop is to modify something inside the while loop

26
00:02:29,110 --> 00:02:33,870
‫in such a manner that the testing condition becomes false.

27
00:02:34,720 --> 00:02:41,230
‫So let's simply increment X by one unit in each iteration here.

28
00:02:41,230 --> 00:02:44,020
‫After printing the current value of X..

29
00:02:44,590 --> 00:02:48,100
‫I write X equals X plus one.

30
00:02:49,370 --> 00:02:59,680
‫That's the same as X plus equals one now when I read the script, it will display the numbers from zero

31
00:02:59,920 --> 00:03:00,910
‫to nine.

32
00:03:02,070 --> 00:03:04,710
‫Let's see in detail what happened.

33
00:03:05,370 --> 00:03:13,830
‫First, we've created a variable called X that stores the value zero, then we have a while loop and

34
00:03:13,830 --> 00:03:16,320
‫the test condition is X less.

35
00:03:16,320 --> 00:03:23,010
‫Then then, of course, this condition returns true because the zero is less than 10.

36
00:03:24,370 --> 00:03:30,220
‫The wild block of code is executed, and that means these two lines.

37
00:03:31,340 --> 00:03:41,660
‫The first line displays the current value of X, and after that, X is incremented by one unit, so

38
00:03:41,660 --> 00:03:43,010
‫X becomes two.

39
00:03:43,940 --> 00:03:52,730
‫Then in the next while iteration, the condition is to less than then, of course, two is less than

40
00:03:52,730 --> 00:03:56,230
‫10, so the block of code will be executed one more time.

41
00:03:56,540 --> 00:04:02,080
‫It brings the current value of X, and after that X becomes three.

42
00:04:02,630 --> 00:04:06,170
‫So two plus one three three is less than then.

43
00:04:06,830 --> 00:04:10,070
‫It executes the block of code one more time.

44
00:04:11,470 --> 00:04:22,930
‫X becomes four and so on until X becomes nine nine, less than 10 X through, it executes the print

45
00:04:22,930 --> 00:04:26,860
‫function and after that, X becomes ten.

46
00:04:26,870 --> 00:04:30,520
‫At this point, the while testing condition will return.

47
00:04:30,520 --> 00:04:35,340
‫False then is not less than ten, then is equal to ten.

48
00:04:35,680 --> 00:04:42,820
‫So the condition returns false and the while block of code is not executed any more.

49
00:04:43,630 --> 00:04:47,580
‫The script exists because there is no other instruction.

50
00:04:47,590 --> 00:04:55,660
‫After the while loop here at the end of the while loop I can have an LS clause.

51
00:04:55,990 --> 00:04:59,500
‫So Alice Callon and here print.

52
00:05:00,250 --> 00:05:02,470
‫This is the statement.

53
00:05:06,800 --> 00:05:09,860
‫X is not less than 10.

54
00:05:12,960 --> 00:05:14,700
‫X is X.

55
00:05:16,050 --> 00:05:18,110
‫And I run the script one more time.

56
00:05:21,290 --> 00:05:29,600
‫We can see how it displayed the numbers from zero to nine, and when X became 10, the condition has

57
00:05:29,600 --> 00:05:37,930
‫been evaluated to false and it has executed the else block of code, which consists of only one instruction,

58
00:05:38,240 --> 00:05:39,470
‫the print function.

59
00:05:39,950 --> 00:05:47,960
‫If the while loop starts with a condition that evaluates to false, it executes only the else block

60
00:05:47,960 --> 00:05:48,500
‫of code.

61
00:05:48,770 --> 00:05:51,890
‫So let's say X equals one hundred.

62
00:05:52,400 --> 00:05:55,430
‫One hundred is not less than ten.

63
00:05:55,670 --> 00:06:00,920
‫The condition returns false, so it will execute only this line of code.

64
00:06:07,040 --> 00:06:14,390
‫As a conclusion, we can say that in Python a while a loop can also have an else statement that gets

65
00:06:14,390 --> 00:06:19,230
‫executed when the while testing condition is not true anymore.

66
00:06:20,000 --> 00:06:27,050
‫A fix all for the moment about while loops in the next lecture, I'll show you how to use break and

67
00:06:27,050 --> 00:06:29,240
‫continue in while loops.


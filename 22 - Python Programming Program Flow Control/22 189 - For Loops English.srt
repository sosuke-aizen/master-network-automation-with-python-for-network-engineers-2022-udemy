﻿1
00:00:00,660 --> 00:00:07,880
‫In the last lecture we've discussed about if Alif Ellis' statements, now it's time to take a look at

2
00:00:07,890 --> 00:00:16,530
‫for Loop's, one of the reasons a computer is so useful is that it can repeat operations multiple times

3
00:00:16,530 --> 00:00:19,050
‫very quickly in programming.

4
00:00:19,050 --> 00:00:23,970
‫A for loop represents one possibility to execute repetitive tasks.

5
00:00:24,450 --> 00:00:33,240
‫In fact, it takes a range of values and assigns them one by one to a variable and executes a block

6
00:00:33,240 --> 00:00:33,960
‫of code.

7
00:00:33,990 --> 00:00:41,870
‫Wanis for each variable you'll see many times in Python documentation the term iterable.

8
00:00:42,090 --> 00:00:47,090
‫So let's see, what does it all mean in Python?

9
00:00:47,220 --> 00:00:54,410
‫Many objects are iterable, meaning that we can iterate over each element in that sequence.

10
00:00:54,900 --> 00:01:03,330
‫All we need is a collection of items and the possibility of getting one item from the collection once

11
00:01:03,840 --> 00:01:04,830
‫iterating.

12
00:01:04,860 --> 00:01:08,400
‫It's a way to get items out of the collection.

13
00:01:08,430 --> 00:01:18,710
‫One by one example of etre able objects are leaks, strings, topolice, six dictionaries and so on.

14
00:01:19,290 --> 00:01:24,540
‫We use for loops to execute a piece of code for each iteration.

15
00:01:26,160 --> 00:01:29,700
‫The syntax of a for loop is the following.

16
00:01:30,390 --> 00:01:36,860
‫There is the four kiwifruit, then a temporary variable in this case item.

17
00:01:37,230 --> 00:01:38,790
‫This isn't a cure.

18
00:01:39,060 --> 00:01:46,770
‫I can have here any name I want then in and there a keyboard and the name of my iterable.

19
00:01:47,400 --> 00:01:52,890
‫This can be a string, at least arrange a dictionary and so on.

20
00:01:53,280 --> 00:01:56,740
‫And then we have the four block of code.

21
00:01:56,940 --> 00:02:01,950
‫Of course, this block of code must be indented with four spaces.

22
00:02:03,930 --> 00:02:12,570
‫Each line of code below the fourth statement that uses the same level of indentation belongs to the

23
00:02:12,570 --> 00:02:13,440
‫four block.

24
00:02:14,950 --> 00:02:24,670
‫Then one by one item takes a value from my collection, from my iterable object, and inside the four

25
00:02:24,670 --> 00:02:29,620
‫block of code, I have access to that value in this example.

26
00:02:29,800 --> 00:02:36,010
‫This code will print the elements of the list one by one.

27
00:02:37,200 --> 00:02:39,990
‫Let's see some examples in picture.

28
00:02:40,050 --> 00:02:41,790
‫I am going to create a new file.

29
00:02:43,520 --> 00:02:44,900
‫Let's say for one.

30
00:02:46,290 --> 00:02:54,150
‫Don't use a reserved works, so it's not recommended to use the four name for your file, they are reserved,

31
00:02:54,150 --> 00:02:54,570
‫cured.

32
00:02:55,350 --> 00:03:02,670
‫I create a list like, say, Oase from operating systems, Windows, Linux, Mac and Android.

33
00:03:06,050 --> 00:03:12,740
‫We'll have a long discussion about the leaks, but till then, you should know that at least is created

34
00:03:12,740 --> 00:03:16,370
‫using square brackets and comma between elements.

35
00:03:17,500 --> 00:03:23,450
‫And now for this is a court item, I could simply write only I.

36
00:03:23,620 --> 00:03:32,080
‫So this is a temporary variable in and now the name of my iterable in this case was Colan.

37
00:03:32,620 --> 00:03:37,230
‫This is mandatory and now using an indented line.

38
00:03:37,390 --> 00:03:42,940
‫So using force bases, bicarb has automatically added force bases.

39
00:03:44,110 --> 00:03:45,730
‫One, two, three, four.

40
00:03:46,000 --> 00:03:56,980
‫All right, Brent, and I'm going to use an F string, so if industry is the name of the item in the

41
00:03:56,980 --> 00:04:06,730
‫list is and between curly braces item, in fact, this is a placeholder for this variable.

42
00:04:07,570 --> 00:04:08,920
‫And let's run the script.

43
00:04:10,840 --> 00:04:16,270
‫You can see how item takes a value from my least one by one.

44
00:04:17,620 --> 00:04:24,010
‫There is no problem if I write here Diabelli and here Diabelli the same.

45
00:04:33,740 --> 00:04:43,400
‫We can also iterate over a string, a string is also an iterable object, so star one equals I learned

46
00:04:43,400 --> 00:04:44,630
‫Python programming.

47
00:04:47,840 --> 00:04:52,610
‫And for Char in Astron Collen.

48
00:04:53,860 --> 00:04:59,500
‫And using an indented block of code like, say, print char.

49
00:05:02,580 --> 00:05:09,390
‫Let's run the script, and it printed each character off my string one by one.

50
00:05:12,180 --> 00:05:17,760
‫By default, the print function adds a new line after each printed line.

51
00:05:17,880 --> 00:05:26,910
‫So if I want here, I use and this is an argument equals and I'm replacing that default backslash end

52
00:05:27,150 --> 00:05:28,920
‫with like, say, a minus sign.

53
00:05:29,990 --> 00:05:36,830
‫When I learned the script, it will display something like this, so the first item of my collection,

54
00:05:37,010 --> 00:05:44,540
‫then a minus sign, the second item of the collection takes a space in the minus sign and so on.

55
00:05:46,750 --> 00:05:54,670
‫Lake's another example, let's create a list called Numbers Equals, and here some values like, say,

56
00:05:54,670 --> 00:06:06,220
‫zero five nine 11, 90, 130 and the for loop for I in numbers print I.

57
00:06:06,700 --> 00:06:10,210
‫In fact, it will print the values of this list.

58
00:06:15,070 --> 00:06:25,510
‫Now, what if I want to check for even and odd numbers, so I'll use a nested if this means an if statement

59
00:06:25,690 --> 00:06:29,170
‫inside of the first statement here, I need to make a test.

60
00:06:30,150 --> 00:06:41,640
‫If I moved two is equal to zero, that means that the remainder of I divide it by two is zero and I

61
00:06:41,820 --> 00:06:47,190
‫is zero at the second iteration, five, nine, 11 and so on.

62
00:06:47,760 --> 00:06:56,130
‫Here I must use a colon and this line of code must be indented because it belongs to the if block.

63
00:06:57,000 --> 00:06:58,770
‫One, two, three, four.

64
00:06:59,700 --> 00:07:11,280
‫Else this has the same level of indentation is the if caught print and let's say here if or no in-between

65
00:07:11,280 --> 00:07:16,890
‫curly braces, I in here, if even no I.

66
00:07:21,270 --> 00:07:22,800
‫And I'll run the script.

67
00:07:23,750 --> 00:07:28,580
‫We can see how it displayed all the numbers and even numbers.

68
00:07:29,480 --> 00:07:39,940
‫At the first iteration, I e zero, it compared the remainder of zero, divided by two with zero and

69
00:07:39,950 --> 00:07:40,420
‫the next.

70
00:07:41,090 --> 00:07:45,450
‫So even number zero zero isn't even number eight.

71
00:07:45,470 --> 00:07:48,170
‫The second iteration I e five.

72
00:07:48,860 --> 00:07:54,580
‫If we divide five by two, we have remained very different from zero.

73
00:07:55,070 --> 00:08:03,320
‫So it executed this line, the else block of code and it displayed all the number five.

74
00:08:04,320 --> 00:08:13,320
‫The third, the number is also an odd number and so on, let's move on and see how we can iterate over

75
00:08:13,320 --> 00:08:14,810
‫a range of numbers.

76
00:08:15,360 --> 00:08:23,880
‫The range function returns, a sequence of numbers starting from zero by default and increments by one

77
00:08:23,880 --> 00:08:27,210
‫by default, and ends at a specific number.

78
00:08:29,940 --> 00:08:39,470
‫So here, instead of numbers, ileus, range of 100, in fact, the range function will return to a

79
00:08:39,480 --> 00:08:45,150
‫sequence of numbers starting from zero till 99.

80
00:08:45,150 --> 00:08:53,880
‫So 100 is excluded and for each number in that sequence, it will do these tasks.

81
00:08:58,290 --> 00:09:08,690
‫I've displayed all numbers from zero to 99 with the message, if that number is an odd or uneven number,

82
00:09:09,150 --> 00:09:13,080
‫we'll use ranges a lot in other scripts.

83
00:09:13,200 --> 00:09:16,290
‫So I want to show you other examples.

84
00:09:16,680 --> 00:09:27,580
‫Let's say for I in range of four come 20 print I, the range function will return.

85
00:09:27,610 --> 00:09:33,770
‫A sequence of numbers from four till 2020 is excluded.

86
00:09:33,780 --> 00:09:37,560
‫So the last number it will print will be nineteen.

87
00:09:40,510 --> 00:09:50,050
‫And the last to the third argument is a step I can go in steps of two, and this means from four included

88
00:09:50,500 --> 00:09:53,980
‫to 20 excluded in steps of two.

89
00:09:56,050 --> 00:10:05,380
‫And this is the result, there is no problem if I have here 40 insteps of minus two, so from 40 to

90
00:10:05,380 --> 00:10:13,240
‫20, insteps of minus to 40 is included and 20 is excluded.

91
00:10:14,280 --> 00:10:21,810
‫That's enough for the moment, about four loops and in the next lectures will dive deeper into ranges

92
00:10:21,810 --> 00:10:22,560
‫and loops.


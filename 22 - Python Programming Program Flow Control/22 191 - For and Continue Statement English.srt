﻿1
00:00:00,780 --> 00:00:07,850
‫In this lecture, we'll take a look at continue, break and pass statements that are used in loops,

2
00:00:08,640 --> 00:00:15,180
‫the continuous statement that was borrowed from sea, continuous with the next iteration of the loop

3
00:00:15,180 --> 00:00:20,100
‫or in other words, returns the control to the beginning of the for loop.

4
00:00:20,790 --> 00:00:27,210
‫The continuous statement rejects all the remaining statements in the current iteration of the loop and

5
00:00:27,210 --> 00:00:30,860
‫moves the control back to the top of the loop.

6
00:00:31,720 --> 00:00:39,520
‫Usually the situation where to continue is useful is when you want to keep the remaining code in the

7
00:00:39,520 --> 00:00:47,290
‫loop and continue iteration, the continuous statement can be used both in Willes and for loops.

8
00:00:48,340 --> 00:00:49,900
‫See some examples.

9
00:00:50,320 --> 00:00:58,900
‫I'll create the for loop that displays even and all the numbers between zero and one hundred.

10
00:00:59,440 --> 00:01:06,600
‫So four X in range of 100 Callon and now the four block of code.

11
00:01:06,760 --> 00:01:08,830
‫And here I have a test condition.

12
00:01:08,950 --> 00:01:19,050
‫If X mod two equals equals zero, that means the remainder of X divided by two is zero.

13
00:01:19,180 --> 00:01:28,510
‫So X is an even number kolon and hairbrained found an even number comics and continue.

14
00:01:29,680 --> 00:01:39,130
‫When the interpreter hits this line, it will simply go to the beginning of the for loop, so it will

15
00:01:39,130 --> 00:01:45,110
‫execute the next iteration and it won't reach the next line.

16
00:01:45,760 --> 00:01:52,480
‫The next line of code that brings all the numbers doesn't belong to the if block of code.

17
00:01:52,750 --> 00:01:55,450
‫It belongs to the four block of code.

18
00:01:55,450 --> 00:02:00,100
‫And that's why it's indented is the same level as the keyword.

19
00:02:00,400 --> 00:02:03,970
‫I'll move the indentation for spaces to the left.

20
00:02:04,980 --> 00:02:06,110
‫Let's run the script.

21
00:02:07,930 --> 00:02:17,210
‫And we can see how it displayed even and odd numbers between zero and one hundred one hundred excluded.

22
00:02:17,410 --> 00:02:20,450
‫So, in fact, ninety nine is the last number.

23
00:02:21,070 --> 00:02:24,640
‫Let's try another example for later in.

24
00:02:24,940 --> 00:02:28,240
‫And here I have a string python colon.

25
00:02:28,390 --> 00:02:32,530
‫If letter is equal to, let's say, t colon.

26
00:02:32,530 --> 00:02:39,130
‫And on the next line with four spaces indented continue print current letter.

27
00:02:44,650 --> 00:02:54,100
‫In this case, I iterate over a string, a string is a sequence of characters, and I print letter by

28
00:02:54,100 --> 00:03:04,210
‫letter excluding the T letter Viks, because when letter is equal to T, the continuous statement will

29
00:03:04,210 --> 00:03:11,470
‫return the execution to the next iteration and it won't print that the letter.

30
00:03:12,650 --> 00:03:15,470
‫It won't reach line number 12.

31
00:03:15,890 --> 00:03:24,950
‫That line is reached and executed only when the continuous statement hasn't been executed or when the

32
00:03:24,950 --> 00:03:28,550
‫letter is not equal to the Alexander the script.

33
00:03:29,390 --> 00:03:34,500
‫And we can see how it displayed B, Y, eight O and M.

34
00:03:34,940 --> 00:03:36,650
‫So now the letter.

35
00:03:38,330 --> 00:03:45,470
‫In Python, there is also the pesky work that can be used when a statement is syntactically required,

36
00:03:45,480 --> 00:03:52,680
‫but you don't have any comment or code to execute, the statement is a operation.

37
00:03:52,850 --> 00:03:56,270
‫Nothing happens when it executes you.

38
00:03:56,270 --> 00:03:59,840
‫Use the best keyboard to avoid a syntax error.

39
00:04:00,350 --> 00:04:04,620
‫Lexcen example for X in the range of 10.

40
00:04:05,330 --> 00:04:10,000
‫And here I don't want to implement the four block of code.

41
00:04:10,130 --> 00:04:12,430
‫I just want to write the comment.

42
00:04:12,650 --> 00:04:18,890
‫So something like this implement this block of code in order to display even a numbers between zero

43
00:04:18,890 --> 00:04:19,940
‫and ninety nine.

44
00:04:20,890 --> 00:04:28,990
‫And that's all this is like a to do or a homework, if I try to execute the script, I'll get another

45
00:04:29,740 --> 00:04:34,120
‫unexpected end or file while passing.

46
00:04:34,360 --> 00:04:42,910
‫So here there must be at least one instruction for that I can write because this is a all instruction.

47
00:04:42,910 --> 00:04:44,080
‫Nothing happens.

48
00:04:44,320 --> 00:04:47,940
‫And in the future I can implement this block of code.

49
00:04:48,310 --> 00:04:51,490
‫I run the script again and there is no error.

50
00:04:53,450 --> 00:04:58,560
‫This is all about continue and Pascual's keywords in the next election.

51
00:04:58,790 --> 00:05:01,010
‫Will take a look at the break your.

52
00:05:02,060 --> 00:05:04,150
‫See you in just a few seconds.


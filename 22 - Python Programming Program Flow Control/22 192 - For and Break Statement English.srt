﻿1
00:00:00,890 --> 00:00:08,960
‫The brake statement that was also borrowed from sea breaks out of the innermost inclosing for or while

2
00:00:08,960 --> 00:00:17,180
‫loop it Terminix or interrupts the car and loop and resume's execution at the next statement after the

3
00:00:17,180 --> 00:00:22,610
‫for or while loop if brake statement is inside a nested loop.

4
00:00:22,880 --> 00:00:28,550
‫This is a loop inside another loop brake will terminate the innermost loop.

5
00:00:28,940 --> 00:00:33,020
‫Also note that loop state mikes may have one else close.

6
00:00:33,470 --> 00:00:40,700
‫It is executed when the loop terminates through the exhaustion of the list over which we iterate, but

7
00:00:40,700 --> 00:00:48,920
‫not when the loop is terminated by a brake statement continued brake and state statements in four.

8
00:00:48,920 --> 00:00:51,470
‫And while loops are specific to Python.

9
00:00:52,570 --> 00:00:59,410
‫Let's see some examples, let's create a new script for and break.

10
00:01:01,140 --> 00:01:09,650
‫For number in the range of 10 Kolon, and here I start the fourth block of code if number equals equals

11
00:01:09,660 --> 00:01:15,690
‫five Kolon break break belongs to the F block of code.

12
00:01:16,620 --> 00:01:20,400
‫In fact, that block of code has only one instruction.

13
00:01:20,430 --> 00:01:29,160
‫The break instruction and then at the same indentation level is the keyword print.

14
00:01:29,520 --> 00:01:31,590
‫No, let's run the script.

15
00:01:33,610 --> 00:01:44,230
‫We can see how it displayed the numbers from zero to four vics, because when number became five, this

16
00:01:44,230 --> 00:01:54,270
‫condition was evaluated total because number is five and the brake statement broke out of the for loop.

17
00:01:55,180 --> 00:02:02,470
‫If I had had another instruction outside the for loop, that instruction would have been executed.

18
00:02:03,340 --> 00:02:08,050
‫For example, print instruction outside for loop.

19
00:02:11,620 --> 00:02:13,980
‫And they'll execute the script one more time.

20
00:02:14,880 --> 00:02:22,590
‫We can see how it displayed all numbers from zero to four, and after that it executed the line number

21
00:02:22,590 --> 00:02:26,520
‫six, which is an instruction outside the for loop.

22
00:02:29,390 --> 00:02:34,750
‫Another example for letter in Python, Callon print letter.

23
00:02:36,510 --> 00:02:40,200
‫And deflater is equal to, oh, break.

24
00:02:41,530 --> 00:02:50,260
‫Here I am iterating over this string and I am printing a letter by letter about when the letter is equal

25
00:02:50,260 --> 00:02:56,680
‫to Oh, I am breaking the for loop, so I am getting out of the for loop.

26
00:02:57,010 --> 00:02:58,810
‫Let's execute the script.

27
00:02:59,910 --> 00:03:02,930
‫And they can see how it printed deal.

28
00:03:03,040 --> 00:03:06,600
‫Oh, it didn't print the end letter.

29
00:03:07,510 --> 00:03:19,630
‫Let's see a last example for X in range, like, say, one twenty, if X Mod 13 equals equals zero.

30
00:03:20,640 --> 00:03:21,720
‫And break.

31
00:03:24,300 --> 00:03:29,950
‫Else print, there is no number divisible by 13 in the range.

32
00:03:33,860 --> 00:03:42,680
‫Maybe it looks strange, but there is no air here, the statement belongs to the for loop and the not

33
00:03:42,680 --> 00:03:49,070
‫to the if statement in Python a for loop can have one else statement.

34
00:03:49,700 --> 00:03:51,500
‫This is Python specific.

35
00:03:51,770 --> 00:03:55,520
‫Before running the script, I want to explain to you this code.

36
00:03:56,680 --> 00:04:01,160
‫I am iterating over these range from one to 19.

37
00:04:02,160 --> 00:04:09,470
‫20 is not included, and if there is a number divisible by 13, I'll break the for loop.

38
00:04:09,780 --> 00:04:18,330
‫That means I'll get out the for loop and I'll execute the next instruction after the LS clause, which

39
00:04:18,330 --> 00:04:19,910
‫belongs to the for loop.

40
00:04:19,920 --> 00:04:22,020
‫In this case that there is no instruction.

41
00:04:22,260 --> 00:04:31,560
‫But if the for loop doesn't end by executing the brake statement, it will execute the statement and

42
00:04:31,560 --> 00:04:35,540
‫will print that there is no number divisible by 13 in the range.

43
00:04:35,940 --> 00:04:45,470
‫So line number 19 will be executed only when line number 17 is not executed.

44
00:04:45,720 --> 00:04:49,050
‫So the brake statement is not executed.

45
00:04:49,890 --> 00:04:56,610
‫Of course, if I run the script, it will execute the brake statement because in the ranks there is

46
00:04:56,610 --> 00:05:02,690
‫the number 13 and 13 is divisible by 13 and it will break.

47
00:05:02,700 --> 00:05:05,430
‫So it won't display this message.

48
00:05:10,630 --> 00:05:19,420
‫But if instead 20 here I have 12, there is no number divisible by 13 in this range, it will display

49
00:05:19,420 --> 00:05:25,600
‫this message and VIX because the brake statement won't be executed.

50
00:05:28,750 --> 00:05:33,430
‫This is all about break in for loops, see you in the next lecture.


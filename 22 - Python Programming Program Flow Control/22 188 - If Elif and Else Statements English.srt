﻿1
00:00:00,240 --> 00:00:08,820
‫Hello, everyone in this section will discuss about if Alif and Ellis statements that are used for decision

2
00:00:08,820 --> 00:00:16,680
‫making, this is program flow in general and allows us to have logic in our applications and execute

3
00:00:16,680 --> 00:00:21,060
‫a section of code only when some criteria are met.

4
00:00:22,040 --> 00:00:29,570
‫Let's suppose we have some sort of booking application for airline tickets and the application takes

5
00:00:29,570 --> 00:00:36,920
‫the balance of a bank account, and only if that value is greater than the price of the ticket, it

6
00:00:36,920 --> 00:00:41,390
‫will continue the booking process, reserve the Sikhs and so on.

7
00:00:42,670 --> 00:00:51,730
‫Contraflow in Python makes use of Colon's and indentation indentation is crucial for Python in the six

8
00:00:51,730 --> 00:00:54,430
‫feet apart from other programming languages.

9
00:00:54,670 --> 00:01:00,940
‫In fact, it makes Python code more readable in other programming languages.

10
00:01:01,210 --> 00:01:04,680
‫We use curly braces to define blocks of code.

11
00:01:04,930 --> 00:01:07,750
‫But here in Python, we use indentation.

12
00:01:08,200 --> 00:01:11,920
‫The standard for indentation is four spaces.

13
00:01:13,040 --> 00:01:21,320
‫It isn't a syntax error if you use a tap instead of four spaces, but it's not recommended, Bepaid

14
00:01:21,320 --> 00:01:25,010
‫recommends using only spaces for indentation.

15
00:01:25,820 --> 00:01:32,950
‫In fact, many Python ideas like bicarb or sublime, replace a tape with four spaces.

16
00:01:33,110 --> 00:01:37,620
‫So, in fact, when you press on tape, it will insert the four spaces.

17
00:01:38,240 --> 00:01:43,810
‫So this is the syntax of a basic if alif else statement.

18
00:01:44,800 --> 00:01:53,770
‫Here, if Ayliffe and Ellis are Python language, keywords in some condition is usually some sort of

19
00:01:53,770 --> 00:02:01,840
‫comparison operation we saw in the first part of this course, basically the above code says something

20
00:02:01,840 --> 00:02:04,350
‫like this, if some condition is true.

21
00:02:04,360 --> 00:02:07,930
‫So the first condition executed only this piece of code.

22
00:02:08,470 --> 00:02:16,300
‫If the first condition is evaluated to false test, the second condition, it's very important to know

23
00:02:16,480 --> 00:02:24,640
‫that the second condition will be tested only if the first condition has been evaluated to false, if

24
00:02:24,640 --> 00:02:29,250
‫the second condition is to execute only that block of code.

25
00:02:29,770 --> 00:02:37,210
‫But if it's false, execute the block of code that comes after the L's statement or keyword.

26
00:02:38,240 --> 00:02:47,540
‫So this program will execute only a block of code, the block of code that comes after the if statement,

27
00:02:47,550 --> 00:02:57,230
‫the block of code that comes after the ALIF statement, or the block of code that comes after the statement.

28
00:02:57,470 --> 00:03:03,530
‫So only one block of code note that we can have only one if statement.

29
00:03:03,680 --> 00:03:09,520
‫It's not mandatory to always have an alif and else statements.

30
00:03:10,160 --> 00:03:19,490
‫You should also know that those columns are amendatory and indicate that an indented block of code follows.

31
00:03:20,390 --> 00:03:28,850
‫But let's see better, some examples will use bicarb to take advantage of auto completion and other

32
00:03:28,850 --> 00:03:30,360
‫advanced features.

33
00:03:30,380 --> 00:03:31,070
‫It has.

34
00:03:32,110 --> 00:03:39,250
‫Of course, you can write your code in Python Idel Notepad or any other Python ID.

35
00:03:40,590 --> 00:03:48,600
‫I'll define a variable called price equals one hundred and balance equals ninety.

36
00:03:49,810 --> 00:03:51,310
‫This is an account balance.

37
00:03:52,260 --> 00:04:01,230
‫If this is a python keyboard and now the test condition or the comparison operation balance greater

38
00:04:01,230 --> 00:04:12,380
‫than or equal to Price Kolon, this is also a keyboard and specifies that a new block of code follows,

39
00:04:12,750 --> 00:04:18,110
‫as you can see, by Ricksha already indented this line of code.

40
00:04:18,510 --> 00:04:22,460
‫So it inserted here four spaces.

41
00:04:22,770 --> 00:04:25,040
‫And here I am printing a message.

42
00:04:25,050 --> 00:04:27,600
‫I'm going to use an F string.

43
00:04:27,840 --> 00:04:30,450
‫We've discussed about F strings.

44
00:04:30,690 --> 00:04:34,020
‫It string operations in a previous lecture.

45
00:04:34,170 --> 00:04:41,470
‫So F from F string you can buy this product and your new balance will be.

46
00:04:41,490 --> 00:04:44,820
‫And here I am using a replacement field.

47
00:04:44,830 --> 00:04:48,870
‫So between curly braces I have balance minus price.

48
00:04:51,510 --> 00:04:59,910
‫Now it's waiting for the next line of this block of code, but this block of code has only one line.

49
00:05:00,150 --> 00:05:08,520
‫So I'll press on backspace and I'll write from the same level as the keyboard else.

50
00:05:08,520 --> 00:05:15,470
‫Collen, enter the next line of code is indented and I'll print insufficient funds.

51
00:05:15,690 --> 00:05:18,760
‫Please deposit in between curly braces.

52
00:05:19,050 --> 00:05:22,440
‫This is only a placeholder price minus Bellus.

53
00:05:23,560 --> 00:05:32,640
‫And here I must use an F, an upper or lower case F, because this is an F string is enough.

54
00:05:32,650 --> 00:05:34,120
‫Now I run the script.

55
00:05:35,440 --> 00:05:39,750
‫As you can see, it has displayed insufficient funds.

56
00:05:39,790 --> 00:05:41,560
‫Please deposit ten.

57
00:05:43,430 --> 00:05:44,330
‫What happened?

58
00:05:45,240 --> 00:05:53,850
‫The first condition has been evaluated to force because balance is 90 and the 90 is not greater than

59
00:05:53,850 --> 00:06:01,530
‫or equal to 100, so it executed the block of code below the statement.

60
00:06:02,340 --> 00:06:10,240
‫But if the price is 50 in this case, this condition will be evaluated to two.

61
00:06:11,420 --> 00:06:19,310
‫Ninety is greater than or equal to 50, it will print, you can buy this product and your new balance

62
00:06:19,310 --> 00:06:23,020
‫will be forty, so ninety minus 50.

63
00:06:23,630 --> 00:06:24,800
‫Let's run the script.

64
00:06:25,280 --> 00:06:29,480
‫And this is the result, exactly what we expected.

65
00:06:30,540 --> 00:06:35,400
‫Now, if I delete these four spaces, I'll get an error.

66
00:06:38,540 --> 00:06:39,740
‫So this is a neighbor.

67
00:06:41,440 --> 00:06:45,730
‫Indentation ever expected an indented block?

68
00:06:46,740 --> 00:06:54,660
‫And I'll press on Deb by default, bicarb inserts for spaces for a ATAP.

69
00:06:56,210 --> 00:07:05,270
‫Let's see, another example answer equals in put off, do you want to continue enter yes or no?

70
00:07:05,960 --> 00:07:10,490
‫Let's suppose we have an application and we need the user input.

71
00:07:13,970 --> 00:07:22,090
‫And now, if in the first test condition is unnecessary, equals, equals, yes, if the user entered

72
00:07:22,100 --> 00:07:25,970
‫yes and I print, the message will move on.

73
00:07:26,630 --> 00:07:35,360
‫I am using single quotes inside single quotes, so I'll use a backslash here to escape this single code.

74
00:07:35,540 --> 00:07:37,190
‫Otherwise I'll get an error.

75
00:07:37,940 --> 00:07:42,950
‫Or I can use single quotes inside double elif.

76
00:07:43,340 --> 00:07:45,230
‫This is the second condition.

77
00:07:45,830 --> 00:07:48,830
‫Answer equals equals no.

78
00:07:48,890 --> 00:07:54,170
‫So if the user answered no and the message will be will stop.

79
00:07:58,110 --> 00:08:01,530
‫And Ellis print invalid answer.

80
00:08:02,470 --> 00:08:11,320
‫What will happen when I run the script first, the script will stop and it will wait for the user input,

81
00:08:11,560 --> 00:08:21,010
‫the user will enter a string and that string that is stored in the A.R.T. variable will be evaluated

82
00:08:21,010 --> 00:08:22,460
‫to the yes string.

83
00:08:22,570 --> 00:08:27,400
‫So if A.R is equal to yes, it will print this message.

84
00:08:27,820 --> 00:08:35,540
‫If Ensler isn't equal to yes, it will evaluate this condition if this condition is true.

85
00:08:35,560 --> 00:08:42,640
‫So if the answer is equal to now, it will print, will stop and the script will exit.

86
00:08:42,970 --> 00:08:45,070
‫It won't execute this line.

87
00:08:45,340 --> 00:08:52,690
‫But if the answer is not equal to now, so if this condition is evaluated to force, it will execute

88
00:08:52,690 --> 00:08:55,620
‫this line of code as a conclusion.

89
00:08:55,630 --> 00:09:03,820
‫We can say that our script will execute a single line of code based on the result of the IEF or ALIF

90
00:09:03,820 --> 00:09:04,630
‫conditions.

91
00:09:06,240 --> 00:09:15,930
‫I ran the script asking for my input, let's say yes, and it has displayed will move on.

92
00:09:16,440 --> 00:09:17,670
‫Let's run the script again.

93
00:09:18,120 --> 00:09:22,470
‫I enter now and he displayed will stop.

94
00:09:23,930 --> 00:09:27,770
‫One more time and let's enter three Pollocks.

95
00:09:29,520 --> 00:09:32,590
‫And it displayed invalid answer.

96
00:09:35,530 --> 00:09:41,890
‫Of course, we can have more lines of code here as long as they use the same indentation.

97
00:09:42,130 --> 00:09:48,010
‫So he actually say X equals seven print X. This is OK.

98
00:09:51,380 --> 00:09:58,370
‫But if I use another indentation, so, for example, I'll try to use here only two spaces, I'll get

99
00:09:58,370 --> 00:10:04,040
‫an error, we can see how bicarb has already underlined in red.

100
00:10:05,080 --> 00:10:14,520
‫We have a syntax error because I've used the wrong indentation here, so one to four spaces and that's

101
00:10:14,530 --> 00:10:14,910
‫OK.

102
00:10:15,250 --> 00:10:20,330
‫Another improvement here would be to make the answer case insensitive.

103
00:10:20,620 --> 00:10:22,720
‫Python is case sensitive.

104
00:10:22,870 --> 00:10:30,520
‫So, for example, if the user enters, yes, with an uppercase letter, it will display invalid answer

105
00:10:30,520 --> 00:10:34,740
‫and the text because the first condition is evaluated to false.

106
00:10:34,780 --> 00:10:42,340
‫We can simply solve this problem by using a string function and that function is lower.

107
00:10:42,550 --> 00:10:47,070
‫So answer this is a string dot lower equals equals.

108
00:10:47,080 --> 00:10:47,590
‫Yes.

109
00:10:47,890 --> 00:10:51,430
‫That means that if the user enters yes.

110
00:10:51,460 --> 00:10:59,620
‫With uppercase letters or only with an uppercase Y letter, it will convert that string to lowercase

111
00:10:59,620 --> 00:11:06,580
‫letters and then it will compare to the Yes string written with lowercase letters.

112
00:11:07,000 --> 00:11:11,340
‫And we need to do the same for the ELIF statement.

113
00:11:11,380 --> 00:11:12,940
‫So answer about lour.

114
00:11:13,390 --> 00:11:16,800
‫I ran the script one more time and I'll enter.

115
00:11:16,840 --> 00:11:17,380
‫Yes.

116
00:11:18,190 --> 00:11:27,070
‫Even if I've used the mixed lour and uppercase letters, the condition has been evaluated to two and

117
00:11:27,070 --> 00:11:28,120
‫it displayed.

118
00:11:28,120 --> 00:11:28,980
‫We'll move on.

119
00:11:29,800 --> 00:11:37,390
‫This is a nice example where you can use string functions about which we've discussed in the previous

120
00:11:37,390 --> 00:11:38,050
‫lecture.

121
00:11:39,280 --> 00:11:47,290
‫OK, Alex, the basics of if Alif and L's statements, and I hope they are straightforward to you,

122
00:11:47,690 --> 00:11:53,440
‫they are used in any application to execute simple or complex tasks.


﻿1
00:00:00,600 --> 00:00:06,840
‫In this lecture, we'll continue our discussion about the loop's, as we've seen earlier in another

2
00:00:06,840 --> 00:00:11,270
‫lecture, we can use break and continue statements with four loops.

3
00:00:11,670 --> 00:00:19,980
‫These instructions break and continue, can also be used with while loops having almost the same effect,

4
00:00:20,610 --> 00:00:27,470
‫the continuous statement continuous with the next iteration of the loop, or in other words, we turn

5
00:00:27,660 --> 00:00:30,930
‫the control to the beginning of the while loop.

6
00:00:32,010 --> 00:00:33,620
‫Let's see a simple example.

7
00:00:34,970 --> 00:00:43,940
‫I'll create a script that displays all numbers divisible by 13 that are less than 100.

8
00:00:44,980 --> 00:00:56,590
‫And I'm starting from 12 X equals 12, while X less than 100, I am incrementing X by one unit, so

9
00:00:56,590 --> 00:00:58,960
‫X plus equals one.

10
00:00:59,680 --> 00:01:08,430
‫If I don't increment X, this condition will never evaluate to false and I'll have an infinite loop.

11
00:01:08,860 --> 00:01:15,370
‫And now if X Mod 13 is not equal to zero, Callon.

12
00:01:16,420 --> 00:01:25,810
‫And continue, I cannot continue on the same line because there is a single instruction or I can use

13
00:01:25,990 --> 00:01:35,980
‫an indented line below Eve and write it on the following line, it's the same in print X.

14
00:01:36,460 --> 00:01:38,340
‫First, let's run the script.

15
00:01:39,310 --> 00:01:47,990
‫We can see how it displayed all numbers divisible by 13 that are less than one hundred.

16
00:01:48,610 --> 00:01:49,680
‫How did it work?

17
00:01:50,730 --> 00:01:59,280
‫I am starting from 12, 12 is less than 100, so the wild testing conditions, we took a stroll X becomes

18
00:01:59,280 --> 00:02:06,820
‫13 and then I am testing if X is divisible or not, by 13.

19
00:02:07,140 --> 00:02:13,680
‫Of course, I'm using the mode operator about which we've discussed a lot in a previous lecture.

20
00:02:13,950 --> 00:02:23,190
‫If there is a number X that is not divisible by 13, the continue statement we the control of the script

21
00:02:23,310 --> 00:02:30,800
‫to the beginning of the while loop or in other words, it will go here on line number two.

22
00:02:30,810 --> 00:02:33,180
‫It will test the condition one more time.

23
00:02:33,390 --> 00:02:38,850
‫And if the condition is true, it will execute the will block of code one more time.

24
00:02:39,180 --> 00:02:41,850
‫So in fact, it will print X.

25
00:02:42,540 --> 00:02:53,610
‫So line number five, only if the number is divisible by 13 and X because the continuous statement is

26
00:02:53,610 --> 00:02:54,840
‫not executed.

27
00:02:55,080 --> 00:02:56,760
‫This is straightforward.

28
00:02:57,780 --> 00:03:06,780
‫Now, I want to show you a common error if I don't increment X here on line number three, so before

29
00:03:06,780 --> 00:03:15,840
‫the if statement, let's say I take this line from here and increment the X variable here.

30
00:03:16,840 --> 00:03:18,340
‫I have one question for you.

31
00:03:18,400 --> 00:03:23,770
‫What do you think will happen, let's run the script to find it out.

32
00:03:24,750 --> 00:03:33,900
‫And nothing happens there because my script entered an infinite loop, I must manually stop the script.

33
00:03:35,980 --> 00:03:39,580
‫That's because it will never reach this line.

34
00:03:39,940 --> 00:03:47,590
‫Why is that first X is 12 and 12 is less than one hundred twelve.

35
00:03:47,590 --> 00:03:52,180
‫Mod 13 is different than zero is in fact, 12.

36
00:03:52,570 --> 00:04:00,700
‫And it executes the continual statement and it will enter the loop one more time without incrementing

37
00:04:00,700 --> 00:04:02,030
‫the X variable.

38
00:04:02,650 --> 00:04:07,510
‫So X remains 12, 12 is less than one hundred.

39
00:04:07,900 --> 00:04:14,260
‫This condition is evaluated through the continuous statement is executed and so on.

40
00:04:14,530 --> 00:04:24,610
‫So it's important to increment the X variable before executing the continual instruction of X all about

41
00:04:24,610 --> 00:04:32,170
‫continue in while loops and in the next lecture will see how we can use the brake statement in while

42
00:04:32,170 --> 00:04:32,680
‫loops.


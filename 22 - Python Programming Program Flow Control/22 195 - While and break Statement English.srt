﻿1
00:00:00,840 --> 00:00:02,740
‫Hello, guys, and welcome back.

2
00:00:02,910 --> 00:00:10,020
‫In this lecture, we'll see how to use the brake statement in while loops the brake statement breaks

3
00:00:10,020 --> 00:00:17,790
‫out of the innermost inclosing for or a while, loop it Terminix or interrupts the current loop and

4
00:00:17,790 --> 00:00:27,390
‫resume's execution at the next statement after the for or while loop if brake statement is inside a

5
00:00:27,390 --> 00:00:28,350
‫nested loop.

6
00:00:28,350 --> 00:00:33,570
‫So a loop inside another loop brake will terminate the innermost loop.

7
00:00:34,580 --> 00:00:44,150
‫In a previous lecture, we've seen in detail how it works with for loops, the brake statement works

8
00:00:44,150 --> 00:00:47,390
‫almost the same with while loops.

9
00:00:48,730 --> 00:00:57,160
‫Let's see an example, I'll create a very simple game where a user must guess a number, and if the

10
00:00:57,160 --> 00:01:06,820
‫correct number was guessed, he or she wins the game and the Python code is while true, this means

11
00:01:07,120 --> 00:01:10,510
‫that the while block of code will be run.

12
00:01:10,510 --> 00:01:19,180
‫As long as I don't break out of it using the brake statement, I create a variable guess equals input

13
00:01:19,330 --> 00:01:22,460
‫off and the message displayed.

14
00:01:22,460 --> 00:01:25,630
‫This gets a lucky number between one and 10.

15
00:01:30,110 --> 00:01:38,660
‫Now, I have a testing condition, if end of guess, I must convert it to an integer because the input

16
00:01:38,660 --> 00:01:40,680
‫function returns a string.

17
00:01:40,820 --> 00:01:49,040
‫So this is a string and here I am, converting it to an integer equals equals eight.

18
00:01:49,070 --> 00:01:51,050
‫This is the lucky number.

19
00:01:52,430 --> 00:01:55,790
‫I am printing a message you won.

20
00:01:57,230 --> 00:01:58,310
‫And break.

21
00:02:01,210 --> 00:02:04,510
‫It will break out of the while loop.

22
00:02:05,630 --> 00:02:12,860
‫The next instruction of it is indented as the same level as the if kiwifruit is the print function,

23
00:02:12,890 --> 00:02:14,690
‫that displays a message.

24
00:02:16,000 --> 00:02:25,000
‫Using an F string, literal, I am displaying guess was not a lucky number, of course, guess enclosed

25
00:02:25,000 --> 00:02:29,710
‫by curly braces because this is a variable.

26
00:02:38,630 --> 00:02:47,450
‫Now, I'll run the script, guess a lucky number between one and 10 and I enter seven.

27
00:02:49,100 --> 00:02:57,470
‫Seven was not a lucky number, what happened, I entered the seven end of seven is not equal to eight,

28
00:02:57,980 --> 00:03:04,280
‫so the condition is evaluated to force and it won't execute this block of code.

29
00:03:04,460 --> 00:03:09,020
‫It will execute this line after executing this line.

30
00:03:09,440 --> 00:03:12,680
‫It will test the while condition one more time.

31
00:03:12,920 --> 00:03:14,320
‫And the condition is true.

32
00:03:14,630 --> 00:03:17,930
‫It is in fact the boolean constant true.

33
00:03:18,350 --> 00:03:25,280
‫And it displays the message one more time, gets a lucky number between one and ten.

34
00:03:26,470 --> 00:03:30,670
‫And this time I enter eight, wow, I won.

35
00:03:33,000 --> 00:03:36,130
‫That's because it, of course, is equal to eight.

36
00:03:36,450 --> 00:03:46,650
‫So the if condition evaluated to two and it executed the if block of code, in fact, these two lines.

37
00:03:47,750 --> 00:03:56,060
‫Here he printed the message you want, and after that, the brake statement interrupted the while loop

38
00:03:56,240 --> 00:04:01,460
‫and broke out of the loop and the script Terminix.

39
00:04:02,940 --> 00:04:09,550
‫The next example I'll show you is a script that finds prime numbers that are less than 1000.

40
00:04:09,960 --> 00:04:14,000
‫I've already created the script in order to save some time.

41
00:04:14,070 --> 00:04:17,340
‫Let me explain to you what the script does.

42
00:04:17,340 --> 00:04:22,470
‫A prime number is a whole number that is divisible only by itself.

43
00:04:22,470 --> 00:04:27,180
‫And one, for example, five and seven are prime numbers.

44
00:04:28,270 --> 00:04:36,850
‫We have two while loops, an outer and an inner loop on the first line, I create a variable Y equals

45
00:04:36,850 --> 00:04:37,710
‫one thousand.

46
00:04:38,050 --> 00:04:46,210
‫In fact, I'll go backwards from one thousand to two to find prime numbers inside the first while loop,

47
00:04:46,210 --> 00:04:50,080
‫I create a variable called X that is equal to half.

48
00:04:50,080 --> 00:04:50,470
‫Why?

49
00:04:51,220 --> 00:04:59,050
‫Using another loop, a nested while loop, I'm going to search for factors and that these are numbers

50
00:04:59,200 --> 00:05:09,310
‫that could divide my Y number from X to two going backwards x enough to search for factors from two

51
00:05:09,460 --> 00:05:11,680
‫to half Y which is X.

52
00:05:13,690 --> 00:05:24,490
‫If Wiimote, X equals zero means that Y can be divided by X and Y is not a prime number, the brake

53
00:05:24,490 --> 00:05:33,760
‫statement that will be executed in this case jumps outside the nested while loop and executes the next

54
00:05:33,760 --> 00:05:40,340
‫line after the nested loop and the X Y minus equals one.

55
00:05:40,750 --> 00:05:46,930
‫So I decrement Y by one unit and enter the main outer while loop again.

56
00:05:47,980 --> 00:05:54,750
‫If there is no X number that divides Y, Y is a prime number.

57
00:05:55,000 --> 00:06:04,760
‫The break statement is not executed and the L block of code which brings Y is prime will be executed.

58
00:06:04,960 --> 00:06:14,280
‫Take care that the clause belongs to the while loop and the not to the if statement are on the script.

59
00:06:15,310 --> 00:06:22,420
‫We can see how it found and displayed prime numbers from one thousand backwords.


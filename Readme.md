# Master Network Automation with Python for Network Engineers 2022 Udemy

### 01 - Course Introduction/:

- 01 01 - Why Network Automation with Python Why Now English.srt
- 01 01 - Why Network Automation with Python Why Now.mp4
- 01 02 - IMPORTANT Please read.html
- 01 02 - IMPORTANT Please read.md
- 01 03 - Quick inside Course Overview English.srt
- 01 03 - Quick inside Course Overview.mp4
- 01 04 - IMPORTANT FOR BEGINNERS How to Learn Python Programming.html
- 01 04 - IMPORTANT FOR BEGINNERS How to Learn Python Programming.md
- 01 05 - Join Our Online Community.html
- 01 05 - Join Our Online Community.md
- 01 06 - Getting Course Resources.html
- 01 06 - Getting Course Resources.md

### 02 - Setup the Environment Python PyCharm GNS3 Cisco IOU and IOS/:

- 02 07 - A Quick Note English.srt
- 02 07 - A Quick Note.mp4
- 02 08 - Download and Install the Required Software.html
- 02 08 - Download and Install the Required Software.md
- 02 09 - Installing Python 3 on Windows English.srt
- 02 09 - Installing Python 3 on Windows.mp4
- 02 10 - Installing Python 3 on Linux and Mac.html
- 02 10 - Installing Python 3 on Linux and Mac.md
- 02 11 - Installing PyCharm IDE on Windows English.srt
- 02 11 - Installing PyCharm IDE on Windows.mp4
- 02 12 - Installing PyCharm IDE on Linux and Mac.html
- 02 12 - Installing PyCharm IDE on Linux and Mac.md
- 02 13 - Running Python Scripts using PyCharm English.srt
- 02 13 - Running Python Scripts using PyCharm.mp4
- 02 14 - Running Python Scripts using the Command Line English.srt
- 02 14 - Running Python Scripts using the Command Line.mp4
- 02 15 - Where do I get Cisco IOS Juniper vSRX or Arista vEOS Images.html
- 02 15 - Where do I get Cisco IOS Juniper vSRX or Arista vEOS Images.md
- 02 16 - Just a few Words about Windows Installation English.srt
- 02 16 - Just a few Words about Windows Installation.mp4
- 02 17 - Download GNS3 for Windows 32 bit.txt
- 02 17 - GNS3 Installation for Windows.txt
- 02 17 - install-gns3-windows.pdf
- 02 17 - Installing GNS3 on Windows 10 English.srt
- 02 17 - Installing GNS3 on Windows 10.mp4
- 02 18 - run-cisco-iou.pdf
- 02 18 - Running Cisco IOU Images in GNS3 on Windows 10 English.srt
- 02 18 - Running Cisco IOU Images in GNS3 on Windows 10.mp4
- 02 19 - Connecting to Cisco IOU Images Running in GNS3 from Window 10 English.srt
- 02 19 - Connecting to Cisco IOU Images Running in GNS3 from Window 10.mp4
- 02 19 - windows-loopback-interface.pdf
- 02 20 - Installing GNS3 and Running Cisco IOU on Linux.html
- 02 20 - Installing GNS3 and Running Cisco IOU on Linux.md
- 02 21 - What if it still doesnt work.html
- 02 21 - What if it still doesnt work.md

### 03 - Working with Text Files in Python/:

- 03 22 - Intro English.srt
- 03 22 - Intro.mp4
- 03 23 - configuration.txt
- 03 23 - Opening and Reading Files English.srt
- 03 23 - Opening and Reading Files.mp4
- 03 24 - Reading Files Tell Seek and Cursors English.srt
- 03 24 - Reading Files Tell Seek and Cursors.mp4
- 03 25 - The With Keyword English.srt
- 03 25 - The With Keyword.mp4
- 03 26 - Coding Exercise Solution.html
- 03 26 - Coding Exercise Solution.md
- 03 27 - Reading Files into a List English.srt
- 03 27 - Reading Files into a List.mp4
- 03 28 - Writing to Text Files English.srt
- 03 28 - Writing to Text Files.mp4
- 03 29 - Coding Exercise Solution.html
- 03 29 - Coding Exercise Solution.md
- 03 30 - Coding Exercise Solution.html
- 03 30 - Coding Exercise Solution.md
- 03 31 - Coding Section Working with Text Files.html
- 03 31 - Coding Section Working with Text Files.md
- 03 32 - airtravel.csv
- 03 32 - Reading CSV Files English.srt
- 03 32 - Reading CSV Files.mp4
- 03 33 - people.csv
- 03 33 - Writing CSV Files English.srt
- 03 33 - Writing CSV Files.mp4
- 03 34 - passwd.csv
- 03 34 - Using CSV Custom Delimiters English.srt
- 03 34 - Using CSV Custom Delimiters.mp4
- 03 35 - items.csv
- 03 35 - people.csv
- 03 35 - Using CSV Dialects English.srt
- 03 35 - Using CSV Dialects.mp4
- 03 36 - airtravel.csv
- 03 36 - Coding Section Working with CSV Files.html
- 03 36 - Coding Section Working with CSV Files.md
- 03 36 - items.csv
- 03 36 - passwd.csv
- 03 36 - people.csv
- 03 37 - Assignment File Processing English.srt
- 03 37 - Assignment File Processing.mp4
- 03 37 - devices.txt
- 03 38 - Assignment Answer 1 File Processing English.srt
- 03 38 - Assignment Answer 1 File Processing.mp4
- 03 39 - Assignment Answer 2 CSV Module File Processing English.srt
- 03 39 - Assignment Answer 2 CSV Module File Processing.mp4

### 04 - HandsOn Challenges Working With Files/:

- 04 40 - HandsOn Challenges Working With Text Files.html
- 04 40 - HandsOn Challenges Working With Text Files.md
- 04 41 - HandsOn Challenges Working With CSV Files.html
- 04 41 - HandsOn Challenges Working With CSV Files.md

### 05 - Data Serialization and Deserialization in Python Pickle and JSON/:

- 05 42 - Intro to Data Serialization English.srt
- 05 42 - Intro to Data Serialization.mp4
- 05 43 - Pickle Data Serialization and Deserialization English.srt
- 05 43 - Pickle Data Serialization and Deserialization.mp4
- 05 44 - Coding Pickle.html
- 05 44 - Coding Pickle.md
- 05 45 - JSON Data Serialization English.srt
- 05 45 - JSON Data Serialization.mp4
- 05 46 - JSON Data Deserialization English.srt
- 05 46 - JSON Data Deserialization.mp4
- 05 47 - Coding JSON.html
- 05 47 - Coding JSON.md
- 05 48 - Assignment JSON and RequestsREST API English.srt
- 05 48 - Assignment JSON and RequestsREST API.mp4
- 05 49 - Assignment Answer JSON and RequestsREST API English.srt
- 05 49 - Assignment Answer JSON and RequestsREST API.mp4
- 05 50 - Coding Challenge Simplify Serialization.html
- 05 50 - Coding Challenge Simplify Serialization.md
- 05 51 - Coding Challenge Solution Simplify Serialization.html
- 05 51 - Coding Challenge Solution Simplify Serialization.md
- 05 52 - Coding Challenge JSON and RequestsREST API.html
- 05 52 - Coding Challenge JSON and RequestsREST API.md
- 05 53 - Coding Challenge Solution JSON and RequestsREST API.html
- 05 53 - Coding Challenge Solution JSON and RequestsREST API.md

### 06 - Network Automation with Paramiko SSH/:

- 06 54 - The Lab Environment.html
- 06 54 - The Lab Environment.md
- 06 55 - Intro to Paramiko English.srt
- 06 55 - Intro to Paramiko.mp4
- 06 56 - Extra Enable SSH on Cisco Devices English.srt
- 06 56 - Extra Enable SSH on Cisco Devices.mp4
- 06 57 - Connecting to Networking Devices using Paramiko English.srt
- 06 57 - Connecting to Networking Devices using Paramiko.mp4
- 06 57 - paramiko-connect.py
- 06 58 - paramiko-connect-kwargs.py
- 06 58 - Using kwargs and Arguments Unpacking English.srt
- 06 58 - Using kwargs and Arguments Unpacking.mp4
- 06 59 - paramiko-execute-commands-cisco.py
- 06 59 - Running Commands on Cisco Devices English.srt
- 06 59 - Running Commands on Cisco Devices.mp4
- 06 60 - paramiko-execute-commands-cisco-getpass.py
- 06 60 - Securing the Passwords Using getpass English.srt
- 06 60 - Securing the Passwords Using getpass.mp4
- 06 61 - Automating the Configuration OSPF of Multiple Cisco Routers English.srt
- 06 61 - Automating the Configuration OSPF of Multiple Cisco Routers.mp4
- 06 61 - paramiko-multiple-routers-ospf.py
- 06 62 - paramiko-execute-commands-linux-1.py
- 06 62 - Running Commands on Linux Method 1 English.srt
- 06 62 - Running Commands on Linux Method 1.mp4
- 06 63 - paramiko-execute-commands-linux-2.py
- 06 63 - Running Commands on Linux as a Nonprivileged User Method 2 English.srt
- 06 63 - Running Commands on Linux as a Nonprivileged User Method 2.mp4
- 06 64 - paramiko-execute-commands-linux-2-root.py
- 06 64 - Running Commands on Linux as root Method 2 English.srt
- 06 64 - Running Commands on Linux as root Method 2.mp4
- 06 65 - myparamiko.py
- 06 65 - Paramiko Refactoring Creating myparamiko Module English.srt
- 06 65 - Paramiko Refactoring Creating myparamiko Module.mp4
- 06 66 - Running Commands Using myparamiko on Cisco IOS English.srt
- 06 66 - Running Commands Using myparamiko on Cisco IOS.mp4
- 06 67 - Importing myparamiko Module English.srt
- 06 67 - Importing myparamiko Module.mp4
- 06 68 - Backup the Configuration of a Single Cisco Device English.srt
- 06 68 - Backup the Configuration of a Single Cisco Device.mp4
- 06 68 - myparamiko-backup-single-device.py
- 06 69 - Automating the Backup Configuration of Multiple Cisco Devices English.srt
- 06 69 - Automating the Backup Configuration of Multiple Cisco Devices.mp4
- 06 69 - myparamiko-backup-multiple-devices.py
- 06 70 - Implementing Multithreading Python and Paramiko English.srt
- 06 70 - Implementing Multithreading Python and Paramiko.mp4
- 06 70 - myparamiko-backup-multiple-devices-threading.py
- 06 71 - Assignment Interactive User Creation on Linux with Paramiko English.srt
- 06 71 - Assignment Interactive User Creation on Linux with Paramiko.mp4
- 06 72 - Assignment Answer Interactive User Creation on Linux with Paramiko English.srt
- 06 72 - Assignment Answer Interactive User Creation on Linux with Paramiko.mp4
- 06 73 - Secure Copying Files with SCP and Paramiko from Python English.srt
- 06 73 - Secure Copying Files with SCP and Paramiko from Python.mp4

### 07 - HandsOn Challenges Network Automation with Paramiko/:

- 07 74 - HandsOn Challenges Paramiko.html
- 07 74 - HandsOn Challenges Paramiko.md

### 08 - Network Automation with Netmiko SSH/:

- 08 75 - The Lab Environment.html
- 08 75 - The Lab Environment.md
- 08 76 - Intro to Netmiko Netmiko vs Paramiko English.srt
- 08 76 - Intro to Netmiko Netmiko vs Paramiko.mp4
- 08 77 - Connecting and Running a Command on a Networking Devices English.srt
- 08 77 - Connecting and Running a Command on a Networking Devices.mp4
- 08 77 - netmiko-connect-and-run.py
- 08 78 - Netmiko Prompt Enable & Global Config Mode English.srt
- 08 78 - Netmiko Prompt Enable & Global Config Mode.mp4
- 08 78 - netmiko-prompt-global-config.py
- 08 79 - netmiko-run-multiple-commands.py
- 08 79 - Running Multiple Commands on a Networking Device English.srt
- 08 79 - Running Multiple Commands on a Networking Device.mp4
- 08 80 - Configuring a Networking Device from a File English.srt
- 08 80 - Configuring a Networking Device from a File.mp4
- 08 80 - nemiko-run-commands-from-file.py
- 08 80 - ospf.txt
- 08 81 - CiscoRouter1.txt
- 08 81 - CiscoRouter2.txt
- 08 81 - CiscoRouter3.txt
- 08 81 - Configuring Multiple Networking Devices from Multiple Files English.srt
- 08 81 - Configuring Multiple Networking Devices from Multiple Files.mp4
- 08 81 - devices.txt
- 08 81 - nemiko-configure-multiple-devices-multiple-files.py
- 08 82 - Automating the Backup of Multiple Cisco Devices Using Netmiko English.srt
- 08 82 - Automating the Backup of Multiple Cisco Devices Using Netmiko.mp4
- 08 82 - netmiko-backup-routers.py
- 08 83 - Netmiko Without Multithreading English.srt
- 08 83 - Netmiko Without Multithreading.mp4
- 08 84 - devices.txt
- 08 84 - Implementing Multithreading Python and Netmiko English.srt
- 08 84 - Implementing Multithreading Python and Netmiko.mp4
- 08 84 - netmiko-and-multithreading.py
- 08 85 - Netmiko and Linux English.srt
- 08 85 - Netmiko and Linux.mp4
- 08 86 - nemiko-troubleshooting.py
- 08 86 - Troubleshooting Netmiko English.srt
- 08 86 - Troubleshooting Netmiko.mp4
- 08 87 - Preparing the Router for SCP English.srt
- 08 87 - Preparing the Router for SCP.mp4
- 08 88 - Copy files to Networking Devices using SCP and Netmiko English.srt
- 08 88 - Copy files to Networking Devices using SCP and Netmiko.mp4
- 08 89 - Assignment Check Interface Status and Enable it if its Disabled English.srt
- 08 89 - Assignment Check Interface Status and Enable it if its Disabled.mp4
- 08 90 - Assignment Answer Check Interface Status and Enable it if its Disabled English.srt
- 08 90 - Assignment Answer Check Interface Status and Enable it if its Disabled.mp4

### 09 - HandsOn Challenges Network Automation with Netmiko/:

- 09 91 - HandsOn Challenges Netmiko.html
- 09 91 - HandsOn Challenges Netmiko.md

### 10 - Building Concurrent Applications Using Async IO/:

- 10 092 - Python Concurrency Ecosystem English.srt
- 10 092 - Python Concurrency Ecosystem.mp4
- 10 093 - asyncio-implementing.py
- 10 093 - Implementing Async IO English.srt
- 10 093 - Implementing Async IO.mp4
- 10 094 - Coding Implementing Async IO.html
- 10 094 - Coding Implementing Async IO.md
- 10 095 - AIOFILES.txt
- 10 095 - AIOHTTP.txt
- 10 095 - asyncio-aiohttp.py
- 10 095 - Building an Asynchronous Web Scraper English.srt
- 10 095 - Building an Asynchronous Web Scraper.mp4
- 10 096 - Coding Building an Async Web Scraper.html
- 10 096 - Coding Building an Async Web Scraper.md
- 10 097 - asyncio-subprocess.py
- 10 097 - Running Shell Commands Subprocesses Asynchronously English.srt
- 10 097 - Running Shell Commands Subprocesses Asynchronously.mp4
- 10 098 - Coding Running Shell Commands.html
- 10 098 - Coding Running Shell Commands.md
- 10 099 - AsyncSSH Asynchronous SSH English.srt
- 10 099 - AsyncSSH Asynchronous SSH.mp4
- 10 099 - AsyncSSH.txt
- 10 100 - asyncio-asyncssh.py
- 10 100 - AsyncSSH Running Multiple Clients English.srt
- 10 100 - AsyncSSH Running Multiple Clients.mp4
- 10 101 - Coding AsyncSSH Multiple Clients.html
- 10 101 - Coding AsyncSSH Multiple Clients.md

### 11 - Extra Running Arista vEOS and Juniper vSRX in GNS3/:

- 11 102 - How to Run Arista vEOS in GNS3.html
- 11 102 - How to Run Arista vEOS in GNS3.md
- 11 103 - How to Run Juniper vSRX in GNS3.html
- 11 103 - How to Run Juniper vSRX in GNS3.md
- 11 104 - Juniper vSRX Basic Configuration.html
- 11 104 - Juniper vSRX Basic Configuration.md
- 11 105 - Arista EOS Configuration Guide.txt
- 11 105 - Arista vEOS Basic Configuration.html
- 11 105 - Arista vEOS Basic Configuration.md
- 11 106 - Configure Arista EOS Switches English.srt
- 11 106 - Configure Arista EOS Switches.mp4
- 11 107 - Netmiko in a Multivendor Environment Cisco & Arista Configuration English.srt
- 11 107 - Netmiko in a Multivendor Environment Cisco & Arista Configuration.mp4

### 12 - Network Automation with Napalm/:

- 12 108 - Intro to Napalm English.srt
- 12 108 - Intro to Napalm.mp4
- 12 109 - Installing Napalm Connecting to a Device English.srt
- 12 109 - Installing Napalm Connecting to a Device.mp4
- 12 110 - Displaying Information English.srt
- 12 110 - Displaying Information.mp4
- 12 111 - Retrieving Information facts interfaces arp table etc English.srt
- 12 111 - Retrieving Information facts interfaces arp table etc.mp4
- 12 112 - Checking Connectivity Between Devices napalmping English.srt
- 12 112 - Checking Connectivity Between Devices napalmping.mp4
- 12 113 - Configuration Management English.srt
- 12 113 - Configuration Management.mp4
- 12 114 - Merging Configurations English.srt
- 12 114 - Merging Configurations.mp4
- 12 115 - Configuration Rollback English.srt
- 12 115 - Configuration Rollback.mp4

### 13 - Network Automation with Telnet/:

- 13 116 - Bytes Objects Encoding and Decoding English.srt
- 13 116 - Bytes Objects Encoding and Decoding.mp4
- 13 116 - str-and-bytes.py
- 13 117 - Telnet Protocol Basics Configure and Connect to Cisco Devices English.srt
- 13 117 - Telnet Protocol Basics Configure and Connect to Cisco Devices.mp4
- 13 118 - Connecting to Network Devices with Telnet from Python English.srt
- 13 118 - Connecting to Network Devices with Telnet from Python.mp4
- 13 118 - telnet-connect-and-run-commands.py
- 13 119 - Configuring Multiple Devices Network Automation English.srt
- 13 119 - Configuring Multiple Devices Network Automation.mp4
- 13 119 - telnet-configure-multiple-devices.py
- 13 120 - Securing the Passwords Using getpass English.srt
- 13 120 - Securing the Passwords Using getpass.mp4
- 13 120 - telnet-configure-multiple-getpass.py
- 13 121 - telnet-class.py
- 13 121 - Telnet Enhancement Refactoring Using Object Oriented Programming English.srt
- 13 121 - Telnet Enhancement Refactoring Using Object Oriented Programming.mp4
- 13 122 - telnet-class-01.py
- 13 122 - Testing the Custom Telnet Class Configure Loopback Interfaces and OSPF English.srt
- 13 122 - Testing the Custom Telnet Class Configure Loopback Interfaces and OSPF.mp4
- 13 123 - Configuring Multiple Devices Using the Custom Telnet Class English.srt
- 13 123 - Configuring Multiple Devices Using the Custom Telnet Class.mp4
- 13 123 - telnet-class-02.py
- 13 124 - Improving the Custom Telnet Class English.srt
- 13 124 - Improving the Custom Telnet Class.mp4
- 13 124 - telnet-class-improvement.py

### 14 - HandsOn Challenges Network Automation with Telnet/:

- 14 125 - HandsOn Challenges Telnet.html
- 14 125 - HandsOn Challenges Telnet.md

### 15 - Network Automation Using Serial Connections/:

- 15 126 - prolific-usb-serial-driver-for-windows.zip
- 15 126 - Serial Communication Basics Connecting to a Console Port English.srt
- 15 126 - Serial Communication Basics Connecting to a Console Port.mp4
- 15 127 - Simulating a Serial Connection with Linux and GNS3.html
- 15 127 - Simulating a Serial Connection with Linux and GNS3.md
- 15 128 - Open a Serial Connection to a Device Console Port English.srt
- 15 128 - Open a Serial Connection to a Device Console Port.mp4
- 15 128 - pySerial API.txt
- 15 129 - Configure Cisco Devices using Serial Connections English.srt
- 15 129 - Configure Cisco Devices using Serial Connections.mp4
- 15 130 - Pyserial Refactoring Creating Our Own myserial Module English.srt
- 15 130 - Pyserial Refactoring Creating Our Own myserial Module.mp4
- 15 131 - Module Enhancement Initially Configuration of a Cisco Device English.srt
- 15 131 - Module Enhancement Initially Configuration of a Cisco Device.mp4
- 15 132 - Initial Configuration Automation From a File English.srt
- 15 132 - Initial Configuration Automation From a File.mp4

### 16 - Useful Python Modules/:

- 16 133 - Highlevel File Operations The Shutil Module English.srt
- 16 133 - Highlevel File Operations The Shutil Module.mp4
- 16 133 - Shutil Module Documentation.txt
- 16 134 - Running System Commands The Os Module English.srt
- 16 134 - Running System Commands The Os Module.mp4
- 16 135 - Running System Commands The Subprocess Module English.srt
- 16 135 - Running System Commands The Subprocess Module.mp4
- 16 135 - Subprocess Module Documentation.txt

### 17 - Extra SSH Public Key Authentication Cisco IOS & Linux/:

- 17 136 - Public Key authentication for SSH.txt
- 17 136 - SSH Public Key Authentication Overview English.srt
- 17 136 - SSH Public Key Authentication Overview.mp4
- 17 137 - Generating SSH Key Pair on Windows English.srt
- 17 137 - Generating SSH Key Pair on Windows.mp4
- 17 138 - What about MacOS.html
- 17 138 - What about MacOS.md
- 17 139 - Generating SSH Key Pair on Linux English.srt
- 17 139 - Generating SSH Key Pair on Linux.mp4
- 17 140 - Configuring SSH Public Key Authentication on Cisco IOS Part 1 Windows English.srt
- 17 140 - Configuring SSH Public Key Authentication on Cisco IOS Part 1 Windows.mp4
- 17 141 - Configuring SSH Public Key Authentication on Cisco IOS Part 2 Linux English.srt
- 17 141 - Configuring SSH Public Key Authentication on Cisco IOS Part 2 Linux.mp4
- 17 142 - Configuring SSH Public Key Authentication on Linux English.srt
- 17 142 - Configuring SSH Public Key Authentication on Linux.mp4

### 18 - Extra Ansible Automate for Everyone/:

- 18 143 - About This Section.html
- 18 143 - About This Section.md
- 18 144 - What is Ansible English.srt
- 18 144 - What is Ansible.mp4
- 18 145 - Ansible Components English.srt
- 18 145 - Ansible Components.mp4
- 18 146 - Installing Ansible English.srt
- 18 146 - Installing Ansible.mp4
- 18 147 - Ansible Inventory File.txt
- 18 147 - Inventory and ansiblecfg Files English.srt
- 18 147 - Inventory and ansiblecfg Files.mp4
- 18 148 - Intro to AdHoc Commands English.srt
- 18 148 - Intro to AdHoc Commands.mp4
- 18 149 - AdHoc Commands Part 1 command shell raw Modules English.srt
- 18 149 - AdHoc Commands Part 1 command shell raw Modules.mp4
- 18 149 - Ansible AdHoc Commands.txt
- 18 149 - Commands Modules.txt
- 18 150 - AdHoc Commands Part 2 setup file copy Modules English.srt
- 18 150 - AdHoc Commands Part 2 setup file copy Modules.mp4
- 18 150 - Copy Module.txt
- 18 150 - File Module.txt
- 18 151 - AdHoc Commands Part 3 apt and service Modules English.srt
- 18 151 - AdHoc Commands Part 3 apt and service Modules.mp4
- 18 151 - Apt Module.txt
- 18 151 - Service Module.txt
- 18 152 - AdHoc Commands Examples.html
- 18 152 - AdHoc Commands Examples.md

### 19 - Extra Ansible Playbooks/:

- 19 153 - YAML Files English.srt
- 19 153 - YAML Files.mp4
- 19 153 - YAML Validator.txt
- 19 154 - Ansible Resources.html
- 19 154 - Ansible Resources.md
- 19 155 - Intro to Ansible Playbooks English.srt
- 19 155 - Intro to Ansible Playbooks.mp4
- 19 156 - ioscommand Modul English.srt
- 19 156 - ioscommand module.txt
- 19 156 - ioscommand Modul.mp4
- 19 157 - Saving Output to a File English.srt
- 19 157 - Saving Output to a File.mp4
- 19 158 - networkcli vs local Privileged Exec Commands in Playbooks English.srt
- 19 158 - networkcli vs local Privileged Exec Commands in Playbooks.mp4
- 19 159 - Behavioral Inventory Parameters English.srt
- 19 159 - Behavioral Inventory Parameters.mp4
- 19 160 - Behavioral Inventory Parameters in Playbooks and Inventory Files English.srt
- 19 160 - Behavioral Inventory Parameters in Playbooks and Inventory Files.mp4
- 19 161 - iosconfig Module English.srt
- 19 161 - iosconfig Module.mp4
- 19 162 - iosconfig Module parents Argument English.srt
- 19 162 - iosconfig Module parents Argument.mp4
- 19 163 - Backup runningconfig Using iosconfig Module English.srt
- 19 163 - Backup runningconfig Using iosconfig Module.mp4
- 19 164 - Ansible Loops Create Linux Users English.srt
- 19 164 - Ansible Loops Create Linux Users.mp4
- 19 165 - Arista eosconfig Module English.srt
- 19 165 - Arista eosconfig Module.mp4
- 19 166 - Playbook with Multiple Plays for a Multivendor Topology Cisco & Arista English.srt
- 19 166 - Playbook with Multiple Plays for a Multivendor Topology Cisco & Arista.mp4
- 19 167 - Ansible Vault English.srt
- 19 167 - Ansible Vault.mp4
- 19 167 - ansible-vault.zip

### 20 - Python Programming Python Basics/:

- 20 168 - Quick Note for Beginners.html
- 20 168 - Quick Note for Beginners.md
- 20 169 - Slides-Variables.pdf
- 20 169 - Variables English.srt
- 20 169 - Variables.mp4
- 20 170 - Constants English.srt
- 20 170 - Constants.mp4
- 20 171 - Comments English.srt
- 20 171 - Comments.mp4
- 20 171 - Slides-Comments.pdf
- 20 172 - Dynamic vs Static Typing English.srt
- 20 172 - Dynamic vs Static Typing.mp4
- 20 172 - Slides-Statically-vs-Dinamically-Typing.pdf
- 20 173 - Builtin Types English.srt
- 20 173 - Builtin Types.mp4
- 20 173 - Slides-Built-in-Types.pdf
- 20 174 - Numbers and Math Operators English.srt
- 20 174 - Numbers and Math Operators.mp4
- 20 175 - Comparison and Identity Operators Mutability vs Immutability English.srt
- 20 175 - Comparison and Identity Operators Mutability vs Immutability.mp4
- 20 175 - Slides-Camparison-Operators.pdf
- 20 176 - Boolean Variables English.srt
- 20 176 - Boolean Variables.mp4
- 20 176 - Slides-Boolean-Variables.pdf
- 20 177 - Boolean Operators English.srt
- 20 177 - Boolean Operators.mp4
- 20 177 - Slides-Boolean-Expressions.pdf
- 20 178 - Coding Python Basics.html
- 20 178 - Coding Python Basics.md

### 21 - Python Programming Strings in Python/:

- 21 179 - Intro to Strings English.srt
- 21 179 - Intro to Strings.mp4
- 21 179 - Slides-Intro-to-Strings.pdf
- 21 180 - Get User Input English.srt
- 21 180 - Get User Input.mp4
- 21 181 - Type Casting English.srt
- 21 181 - Type Casting.mp4
- 21 182 - Slides-String-Indexing.pdf
- 21 182 - String Indexing and Operations English.srt
- 21 182 - String Indexing and Operations.mp4
- 21 183 - String Slicing English.srt
- 21 183 - String Slicing.mp4
- 21 184 - Slides-String-Formatting.pdf
- 21 184 - String Formatting FStrings English.srt
- 21 184 - String Formatting FStrings.mp4
- 21 185 - Recap Printing Strings English.srt
- 21 185 - Recap Printing Strings.mp4
- 21 186 - String Methods English.srt
- 21 186 - String Methods.mp4
- 21 187 - Coding Strings in Python.html
- 21 187 - Coding Strings in Python.md

### 22 - Python Programming Program Flow Control/:

- 22 188 - If Elif and Else Statements English.srt
- 22 188 - If Elif and Else Statements.mp4
- 22 188 - if.elif.else.pdf
- 22 189 - For Loops English.srt
- 22 189 - For Loops.mp4
- 22 189 - Slides-For.pdf
- 22 190 - Ranges Indepth English.srt
- 22 190 - Ranges Indepth.mp4
- 22 190 - Slides-Ranges.pdf
- 22 191 - For and Continue Statement English.srt
- 22 191 - For and Continue Statement.mp4
- 22 192 - For and Break Statement English.srt
- 22 192 - For and Break Statement.mp4
- 22 192 - Slides-For-and-Break.pdf
- 22 193 - Slides-While.pdf
- 22 193 - While Loops English.srt
- 22 193 - While Loops.mp4
- 22 194 - While and continue Statement English.srt
- 22 194 - While and continue Statement.mp4
- 22 195 - While and break Statement English.srt
- 22 195 - While and break Statement.mp4
- 22 196 - Coding Program Flow Control.html
- 22 196 - Coding Program Flow Control.md

### 23 - Python Programming Lists and Tuples in Python/:

- 23 197 - Intro to Lists English.srt
- 23 197 - Intro to Lists.mp4
- 23 197 - Slides-Intro-to-Lists.pdf
- 23 198 - List Operations English.srt
- 23 198 - List Operations.mp4
- 23 198 - Slides-List-Operations.pdf
- 23 199 - List Methods Part 1 Append Extend Insert Copy English.srt
- 23 199 - List Methods Part 1 Append Extend Insert Copy.mp4
- 23 200 - List Methods Part 2 Clear Pop Index Count English.srt
- 23 200 - List Methods Part 2 Clear Pop Index Count.mp4
- 23 201 - List Methods Part 3 Sort Max Min and Sum English.srt
- 23 201 - List Methods Part 3 Sort Max Min and Sum.mp4
- 23 202 - Coding Lists in Python.html
- 23 202 - Coding Lists in Python.md
- 23 203 - Intro to Tuples English.srt
- 23 203 - Intro to Tuples.mp4
- 23 203 - Slides-Intro-to-Tuples.pdf
- 23 204 - Slides-Tuple-Operations.pdf
- 23 204 - Tuple Operations English.srt
- 23 204 - Tuple Operations.mp4
- 23 205 - Tuple Methods English.srt
- 23 205 - Tuple Methods.mp4
- 23 206 - Slides-Tuples-vs-Lists.pdf
- 23 206 - Tuples vs Lists English.srt
- 23 206 - Tuples vs Lists.mp4
- 23 207 - Coding Tuples in Python.html
- 23 207 - Coding Tuples in Python.md

### 24 - Python Programming Sets Frozensets and Dictionaries in Python/:

- 24 208 - Intro to Sets English.srt
- 24 208 - Intro to Sets.mp4
- 24 208 - Slides-Intro-to-Sets.pdf
- 24 209 - Set Methods Part 1 Add Clear Copy Remove Discard Pop English.srt
- 24 209 - Set Methods Part 1 Add Clear Copy Remove Discard Pop.mp4
- 24 209 - Slides-Set-Methods-part1.pdf
- 24 210 - Set Methods Part 2 Difference Symmetric Difference Union Intersection English.srt
- 24 210 - Set Methods Part 2 Difference Symmetric Difference Union Intersection.mp4
- 24 210 - Slides-Set-Methods-part2.pdf
- 24 211 - Fronzensets English.srt
- 24 211 - Fronzensets.mp4
- 24 211 - Slides-Frozensets.pdf
- 24 212 - Coding Sets and Frozensets in Python.html
- 24 212 - Coding Sets and Frozensets in Python.md
- 24 213 - Intro to Dictionaries English.srt
- 24 213 - Intro to Dictionaries.mp4
- 24 213 - Slides-Intro-to-Dictionaries.pdf
- 24 214 - Working with Dictionaries English.srt
- 24 214 - Working with Dictionaries.mp4
- 24 215 - Dictionary Operations and Methods English.srt
- 24 215 - Dictionary Operations and Methods.mp4
- 24 215 - Slides-Dictionary-Operations-and-Methods.pdf
- 24 216 - Coding Dictionaries in Python.html
- 24 216 - Coding Dictionaries in Python.md

### 25 - Python Programming Functions in Python/:

- 25 217 - Intro to Functions English.srt
- 25 217 - Intro to Functions.mp4
- 25 218 - Slides-The-return-keyword.pdf
- 25 218 - The return Keyword English.srt
- 25 218 - The return Keyword.mp4
- 25 219 - Functions Arguments Positional Default Keyword args and kwargs English.srt
- 25 219 - Functions Arguments Positional Default Keyword args and kwargs.mp4
- 25 219 - Slides-Function-Arguments.pdf
- 25 220 - Scopes and Namespaces English.srt
- 25 220 - Scopes and Namespaces.mp4
- 25 220 - Slides-Scopes-and-Namespaces.pdf
- 25 221 - Lambda Expressions English.srt
- 25 221 - Lambda Expressions.mp4
- 25 221 - Slides-Lambdas.pdf
- 25 222 - Coding Functions and Lambda Expressions in Python.html
- 25 222 - Coding Functions and Lambda Expressions in Python.md
- 25 223 - The Special Constant None English.srt
- 25 223 - The Special Constant None.mp4

### 26 - Python Programming Errors and Exception Handling/:

- 26 224 - Intro to Exceptions English.srt
- 26 224 - Intro to Exceptions.mp4
- 26 224 - Slides-Errors-and-Exceptions.pdf
- 26 225 - Exception Handling TryExceptElseFinally English.srt
- 26 225 - Exception Handling TryExceptElseFinally.mp4
- 26 226 - Coding Errors and Exception Handling.html
- 26 226 - Coding Errors and Exception Handling.md

### 27 - Python Programming Object Oriented Programming Basics/:

- 27 227 - Intro to Object Oriented Programming English.srt
- 27 227 - Intro to Object Oriented Programming.mp4
- 27 227 - Slides-Intro-to-OOP.pdf
- 27 228 - Defining Classes and Objects English.srt
- 27 228 - Defining Classes and Objects.mp4
- 27 229 - The init Method English.srt
- 27 229 - The init Method.mp4
- 27 230 - Instance Attributes and Class Attributes English.srt
- 27 230 - Instance Attributes and Class Attributes.mp4
- 27 231 - Magic Methods English.srt
- 27 231 - Magic Methods.mp4
- 27 231 - Slides-Magic-Methods.pdf

### 28 - Course Completion/:

- 28 232 - Congratulations English.srt
- 28 232 - Congratulations.mp4
- 28 233 - BONUS.html
- 28 233 - BONUS.md

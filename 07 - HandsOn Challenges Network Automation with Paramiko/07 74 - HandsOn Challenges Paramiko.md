

 **More information about the challenges:**

  * Solve these challenges only after watching all the videos in the section, in the order that I’ve provided.

  * For Cisco automation tasks I’d recommend you to use [this topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN) in GNS3 or create a similar one. [Import the GNS3 Project.](https://drive.google.com/open?id=1gqx8pGamn02D0dzOpbd3FKIPOhRJZBgz)

  * For most challenges, you can use Linux instead of Cisco. Just send Linux commands instead of Cisco IOS.

  * If your solution is not correct, then try to understand the error messages, watch the video again, rewrite the solution, and test it again. Repeat this step until you get the correct solution.

  *  **Save** the solution in a file for future reference or recap.

  

 **Challenge #1**

Create a Python script that connects to a Cisco Router using SSH and Paramiko.
The script should execute the **show users** command in order to display the
logged-in users.

Print out the output of the command.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1AliuNfeKElAlRWw1SUQtB6TWV6g9_asW).

  

 **Challenge #2**

Change the [solution from Challenge
#1](https://drive.google.com/open?id=1AliuNfeKElAlRWw1SUQtB6TWV6g9_asW) so
that it will prompt the user for the SSH password securely (use _getpass_
module). Run the script in the terminal (you can not run it in PyCharm).

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1wxop5ZqvJ6mdaTcK-6UlNH6eF5F-nJJB).

  

 **Challenge #3**

Change the [solution from Challenge
#1](https://drive.google.com/open?id=1AliuNfeKElAlRWw1SUQtB6TWV6g9_asW) so
that it will save the output to a file instead of printing it.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1NebLZxivFvv5CasPGApb0wpGZ5R2Zy-_).

  

 **Challenge #4**

Consider the solution from [Challenge
#1](https://drive.google.com/open?id=1AliuNfeKElAlRWw1SUQtB6TWV6g9_asW)

Change the connecting part so that the **connect()** method of the
**ssh_client can** receive a **kwargs argument like this:
**ssh_client.connect(**router)**

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1gaDHD7bAM5ZJbA0QDjRitP_UUIsN0r6T).

  

 **Challenge #5**

Create a Python script that connects to a Cisco Router using SSH and Paramiko,
enters the enable mode, and then executes **show running-config**

The entire output should be saved to a file in the current working directory.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1gTgDpXPepc1HCtqzzwMrXpDxQOh-zT1b).

  

 **Challenge #6**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1iOMPL9T7-huOVuaCH7Pn5biH91r36pXP)
that executes **show ip interface brief** on a remote Cisco Router and
displays the output.

Although the script connects and authenticates successfully, it doesn’t
display the entire output of the command, but only a part of it.

Your task is to troubleshoot the issue and solve it so that it displays the
entire output.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1-NhGs6JXHh2IgF6FvwLv_tfp9tDHlW6Q).

  

 **Challenge #7**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1pHmVDXBARJM1oHK5yMFRWqMzuvYQKWrk)
that executes **show ip interface brief** on a remote Cisco Router and
displays the output.

However, instead of the normal output of the command (a string), it displays
some sort of gibberish.

Your task is to troubleshoot the issue and solve it so that it displays the
entire output.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1QrDeR2XdZh94GSTxaI8S-vfGxfBtK6XZ).

  

 **Challenge #8**

A Network Engineer has created [this Python
script](https://drive.google.com/open?id=1o73DRV1MRfTtQ5tNgTmOheD5Kq1Sdd08)
that executes **show ip interface brief** on a remote Cisco Router and
displays the output.

However, since there is a single error in the script it can't display the
output of the command.

Your task is to troubleshoot the issue and solve it so that it works as
expected.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1xvLDh8o-mL7hwyfNhQlBZyztf3kkX0OS).

  

 **Challenge #9**

Create a Python script that connects to a Cisco Router using SSH and Paramiko
and executes a list of commands. The commands are saved in a Python list.

An example of a list with commands to execute:

 _# the second element (cisco) is the enable command_

 _commands = [_ ** _'enable'_** _,_ ** _'cisco'_** _,_ ** _'conf t'_** _,_ **
_'username admin1 secret cisco'_** _,_ ** _'access-list 1 permit any'_** _,_
** _'end'_** _,_ ** _'terminal length 0'_** _,_ ** _'sh run | i user'_** _]_

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1Jn4mzrIDPW0enACKBhTphCNzQg1MnhAc).

  

 **Challenge #10**

Create a Python script that connects to a Cisco Router using SSH and Paramiko
and executes all the commands from a text file.

An example of a text[ file with
commands.](https://drive.google.com/open?id=1Ct_t5TDeR47qYi4E38YSHPmB5z15qoKS)

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1cqFTISA1C6NuKQKAeRzb8TM5gh3Q4-Z_).

  

 **Challenge #11**

Consider [myparamiko.py](https://drive.google.com/open?id=1u0raPjFA-
tnUMSidg8xNOogZ3So4PlSJ) script developed in the course.

Add a new function called **send_from_list()** that receives as an argument a
Python list of commands and sends all commands to the remote device to be
executed.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1Is1G2BtgOSF6a29b2bheWOoMl88WLDuh).

  

 **Challenge #12**

Consider [myparamiko.py](https://drive.google.com/open?id=1u0raPjFA-
tnUMSidg8xNOogZ3So4PlSJ) script developed in the course.

Add a new function called **send_from_file()** that receives as an argument a
text file with commands and sends all commands to the remote device to be
executed.

Example of[ text file with
commands.](https://drive.google.com/open?id=1Ct_t5TDeR47qYi4E38YSHPmB5z15qoKS)

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1iaq6SH7MQN1wSxZj9hIH4lmTPZ5-iKrP).

  

 **Challenge #13**

Consider a [topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-
eCZHkeUChiS-VbN) with multiple devices like [this
one.](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

For each device in the topology, you have a Python dictionary that stores the
SSH connection information (IP, port, username, password) **but also a
filename** that contains the commands to be sent to that device.

Example:

 _router1 = {_ ** _'server_ip'_** _:_ ** _'192.168.122.10'_** _,_ **
_'server_port'_** _:_ ** _'22'_** _,_ ** _'user'_** _:_ ** _'u1'_** _,_ **
_'passwd'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_ ** _'_**[ **
_ospf.txt_**](https://drive.google.com/open?id=1_ZK6q2rABpmZE9mlkWjiZ65FF8vZ5KeK)
** _'_** _}_

 _router2 = {_ ** _'server_ip'_** _:_ ** _'192.168.122.20'_** _,_ **
_'server_port'_** _:_ ** _'22'_** _,_ ** _'user'_** _:_ ** _'u1'_** _,_ **
_'passwd'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_ ** _'_**[ **
_eigrp.txt_**](https://drive.google.com/open?id=1EaNKyHyISWbvtDrWQQ1v1cikqEayFhaM)
** _'_** _}_

 _router3 = {_ ** _'server_ip'_** _:_ ** _'192.168.122.30'_** _,_ **
_'server_port'_** _:_ ** _'22'_** _,_ ** _'user'_** _:_ ** _'u1'_** _,_ **
_'passwd'_** _:_ ** _'cisco'_** _,_ ** _'config'_** _:_ ** _'_**[ **
_router3.conf'_**](https://drive.google.com/open?id=1kR722HzqmiBCloUOQQ9SSvigSJDDIJ5V)
_}_

 _…._

Create a Python script that connects to each device using SSH and Paramiko and
executes the commands from the file (which is the value of the dictionary
_config_ key).

Use [myparamiko.py](https://drive.google.com/open?id=1u0raPjFA-
tnUMSidg8xNOogZ3So4PlSJ) that was developed in the course or create the script
from scratch.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1wYKhY-Hya1tE7NBCfgy8nSJ6fi8vP7UL).

  

 **Challenge #14**

Consider a [topology](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-
eCZHkeUChiS-VbN) with multiple devices like [this
one.](https://drive.google.com/open?id=1HSfIWiwpmXDVqTAf-eCZHkeUChiS-VbN)

Create a function called **execute_command()** that takes 2 arguments: a
dictionary with information about the device (ip, port, credentials) and a
**show command** to execute on the device.

Using a for loop iterate over the routers in the topology and call the
function for each router.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1JWBj7tB78jc9yrxESOJ5j9pDxmziRdzW).

  

 **Challenge #15**

Change the solution from [Challenge
#14](https://drive.google.com/open?id=1JWBj7tB78jc9yrxESOJ5j9pDxmziRdzW) to
use **multithreading** and execute the command concurrently on all routers in
the topology.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1ECDVbeW-PS0SY89xRxEyTHp_XrwWhDZ-).

  

 **Challenge #16**

Change the [solution from Challenge
#13](https://drive.google.com/open?id=1wYKhY-Hya1tE7NBCfgy8nSJ6fi8vP7UL) to
use multithreading and automate the configuration of the routers concurrently.

 **Are you stuck?** Do you want to see the solution to this exercise? Click[
here](https://drive.google.com/open?id=1T7uGv9Y_IKiZkTUPgY1i9Oz6nbKELvly).


﻿1
00:00:00,760 --> 00:00:08,170
‫In this lecture, we'll dive deeper into dictionary operations and methods, the first operation, we'll

2
00:00:08,170 --> 00:00:10,660
‫take a look at its membership operation.

3
00:00:10,910 --> 00:00:14,930
‫It asks if a key is present in the dictionary or not.

4
00:00:15,370 --> 00:00:19,750
‫For that, we use the in or not in operators.

5
00:00:20,690 --> 00:00:26,820
‫If we want to test, if our value is present in the dictionary, there is no quick way around it.

6
00:00:27,050 --> 00:00:33,080
‫We must iterate over the values in the dictionary and search for that specific one.

7
00:00:34,220 --> 00:00:34,910
‫An example.

8
00:00:36,620 --> 00:00:40,970
‫I have a dictionary called Person with three key value pairs.

9
00:00:43,010 --> 00:00:50,030
‫If I want to test, if a key is present in the dictionary, I write the name of the key, for example,

10
00:00:50,030 --> 00:00:56,800
‫name in person, and it returned to Vex because the key belongs to the dictionary.

11
00:00:57,320 --> 00:01:04,670
‫But if instead of name I write here X multiple times, it return false.

12
00:01:04,670 --> 00:01:07,370
‫That key doesn't belong to the dictionary.

13
00:01:08,780 --> 00:01:16,520
‫There are three methods that deal with keys and values in a dictionary, the first one is keys method.

14
00:01:16,850 --> 00:01:23,840
‫This method is used to obtain a dictionary view having the keys of the dictionary as elements.

15
00:01:24,260 --> 00:01:28,610
‫This view can be easily converted to a list using the list function.

16
00:01:29,480 --> 00:01:32,060
‫A similar method is values.

17
00:01:32,570 --> 00:01:37,740
‫This is used to get the values of the dictionary like the Keys method.

18
00:01:37,760 --> 00:01:42,860
‫It returns a dictionary view having the values of the dictionary as elements.

19
00:01:42,860 --> 00:01:47,090
‫We can convert to this object to a list using the list function.

20
00:01:47,480 --> 00:01:51,320
‫And then there is the third method called items.

21
00:01:51,620 --> 00:01:57,920
‫It returns a list of topolice, each tuple containing the key and the value of each dictionary.

22
00:01:57,920 --> 00:02:00,650
‫Bear Lexi examples.

23
00:02:02,340 --> 00:02:04,200
‫Bahnson Dot Ki's.

24
00:02:05,750 --> 00:02:11,060
‫And it returned a dictionary view that contains the keys.

25
00:02:11,210 --> 00:02:18,110
‫This is not a list, this is an object of type dict keys called also a dictionary.

26
00:02:18,110 --> 00:02:22,280
‫Keys, if you eat, can be easily converted to a list.

27
00:02:22,700 --> 00:02:27,950
‫For example, keys equals a list of person dot keys.

28
00:02:29,960 --> 00:02:34,550
‫And this is the list that contains the keys of the dictionary.

29
00:02:36,750 --> 00:02:45,060
‫Similarly, we can use the values method, so values equals list of person dot values.

30
00:02:48,040 --> 00:02:50,230
‫And these are the values.

31
00:02:52,000 --> 00:02:55,720
‫Let's see the last method person thought items.

32
00:02:57,180 --> 00:03:04,590
‫It returned topolice, each Topal contains the key and the value of each dictionary bear.

33
00:03:05,850 --> 00:03:14,160
‫One important thing is that the author of Keys, Values and items returned to by these three methods

34
00:03:14,310 --> 00:03:15,060
‫is the same.

35
00:03:15,330 --> 00:03:21,740
‫That means that, for example, the third key we talked about, the Keys method corresponds with the

36
00:03:21,750 --> 00:03:24,900
‫third, the value returned by the values method.

37
00:03:26,440 --> 00:03:31,870
‫Also, note that the views returned to by these three methods are dynamic.

38
00:03:32,140 --> 00:03:35,170
‫They reflect any changes in the dictionary.

39
00:03:35,800 --> 00:03:36,910
‫Lexia, an example.

40
00:03:38,070 --> 00:03:42,420
‫I modify my dictionary, Barson, of, let's say, eight.

41
00:03:44,580 --> 00:03:45,900
‫Equals 40.

42
00:03:47,720 --> 00:03:48,800
‫This is my dictionary.

43
00:03:49,890 --> 00:03:54,950
‫And keys equals person dot keys.

44
00:03:56,270 --> 00:03:59,900
‫Values, equality, person, dot values.

45
00:04:01,520 --> 00:04:11,450
‫These are the keys and these are the values, and now I do person of eight equals, let's say here 60.

46
00:04:18,180 --> 00:04:25,980
‫Without calling the values method one more time, it has automatically updated the content of the values

47
00:04:26,310 --> 00:04:34,240
‫variable, we can see how inside the values variable I have the new value, which is 60.

48
00:04:34,440 --> 00:04:42,660
‫That means that the variable returned by the keys and values methods are dynamically updated.

49
00:04:44,250 --> 00:04:53,080
‫You should also know that dictionary view is returned by keys and items, methods behave like sex and

50
00:04:53,160 --> 00:04:55,050
‫sex because keys are unique.

51
00:04:55,470 --> 00:05:02,910
‫This means that sex operations are available also for keys and items like an example.

52
00:05:03,630 --> 00:05:07,950
‫I create another dictionary called the one with two pairs.

53
00:05:12,510 --> 00:05:16,290
‫And the tool that contains three bears.

54
00:05:22,800 --> 00:05:25,640
‫K1 equals the one that kills.

55
00:05:28,360 --> 00:05:35,560
‫And Kate, to equals the two dot keys, this is K one, this is K to.

56
00:05:38,220 --> 00:05:43,920
‫These objects behave like sex, so I can try a said operation.

57
00:05:44,780 --> 00:05:52,070
‫In a previous lecture, we've talked about six operations, let's try, for example, the union SOKE

58
00:05:52,100 --> 00:05:57,500
‫one, a vertical bar or a pipe, K2, and this is the union.

59
00:05:58,950 --> 00:06:02,400
‫And the result is a set, this is a set.

60
00:06:03,740 --> 00:06:10,610
‫Now that we know how to get the keys and the values of dictionaries, let's see how to iterate over

61
00:06:10,610 --> 00:06:11,540
‫a dictionary.

62
00:06:14,850 --> 00:06:25,500
‫I can iterate over the keys, over the values or over the items, see exemplars for Kay in person,

63
00:06:25,890 --> 00:06:26,700
‫dot keys.

64
00:06:27,880 --> 00:06:31,240
‫Colleen, and this is the four block of code.

65
00:06:32,920 --> 00:06:37,600
‫I am going to print each key using an F string literal.

66
00:06:39,780 --> 00:06:49,920
‫Makes a key kolon and the temporary variable called K. between curly braces, in this example, I am

67
00:06:49,920 --> 00:06:52,760
‫iterating over the keys of the dictionary.

68
00:06:57,620 --> 00:07:05,510
‫I can iterate over the keys also without specifying any method, only the name of the dictionary.

69
00:07:05,690 --> 00:07:06,950
‫So this is the same.

70
00:07:09,140 --> 00:07:16,610
‫If I want to get over the values, I can have something like this, so for V in person, dot values.

71
00:07:21,120 --> 00:07:24,270
‫And here I have value, Callon V..

72
00:07:25,910 --> 00:07:27,530
‫These are the values.

73
00:07:29,400 --> 00:07:38,790
‫And the last example, I am iterating over both the keys and the values, OK, of this is the syntax

74
00:07:39,030 --> 00:07:40,740
‫in person items.

75
00:07:44,600 --> 00:07:55,090
‫Colon and between curly braces, k cormie value colon and between curly braces v k and we are the temporary

76
00:07:55,090 --> 00:07:57,340
‫variables of the four loop.

77
00:08:00,800 --> 00:08:11,510
‫Of how we iterate over a dictionary, another useful method is update update is used to extend a dictionary

78
00:08:11,540 --> 00:08:16,610
‫with the items of another dictionary, like the examples.

79
00:08:17,520 --> 00:08:27,030
‫Beyond this is another dictionary, and here I have location in Berlin, Alexei Job, Callon Developer.

80
00:08:31,550 --> 00:08:36,080
‫And now I write the person dot update of Byone.

81
00:08:39,270 --> 00:08:45,930
‫You can see how I've extended the person dictionary with the content of the Peukan Dictionary.

82
00:08:47,910 --> 00:08:53,280
‫In the end of this lecture, I want to tell you one more word about order.

83
00:08:53,670 --> 00:09:01,680
‫Even if a dictionary is unordered and we cannot count on order, starting with Python three Dota seeks

84
00:09:01,680 --> 00:09:05,850
‫a dictionary remains Audibert in order of inspection.

85
00:09:06,930 --> 00:09:09,010
‫This is all about dictionaries.

86
00:09:09,060 --> 00:09:09,630
‫Thank you.


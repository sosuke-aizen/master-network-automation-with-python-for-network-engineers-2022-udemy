﻿1
00:00:00,610 --> 00:00:09,100
‫In the last lectures we've discussed about sex, what are they what operations and methods do they implement

2
00:00:09,310 --> 00:00:10,990
‫and when to use sex?

3
00:00:11,320 --> 00:00:14,080
‫Now it's time to take a look at the frozen sex.

4
00:00:14,410 --> 00:00:18,240
‫Frozen sex are nothing more than immutable sex.

5
00:00:18,640 --> 00:00:25,870
‫They have the same properties and behavior as sex, except they cannot be imitated or modified.

6
00:00:26,380 --> 00:00:35,830
‫This means that any of the mutation operations we saw at sex like Ed Update and so on are not going

7
00:00:35,830 --> 00:00:36,460
‫to work.

8
00:00:36,460 --> 00:00:46,300
‫With frozen sex being immutable, frozen sex can be used as keys in dictionaries or as elements in another

9
00:00:46,300 --> 00:00:47,650
‫set or frozen set.

10
00:00:48,010 --> 00:00:52,630
‫And this can be really useful to create the frozen asset.

11
00:00:52,630 --> 00:00:59,990
‫We use the frozen set function or we create the frozen set from another iterable object, like a string

12
00:01:00,010 --> 00:01:03,700
‫liste Topal or set by passing in the ether.

13
00:01:03,700 --> 00:01:07,480
‫Able is the argument to the frozen set function.

14
00:01:07,870 --> 00:01:09,460
‫Let's see examples.

15
00:01:10,030 --> 00:01:14,820
‫I'm going to create an empty frozen asset using the frozen set function.

16
00:01:15,460 --> 00:01:19,720
‫In fact, this is the constructor of the frozen asset class.

17
00:01:21,140 --> 00:01:24,820
‫Now, I'll create another frozen set from earliest.

18
00:01:31,500 --> 00:01:35,130
‫We can see how the frozen asset has no duplicates.

19
00:01:36,690 --> 00:01:41,050
‫A was on said can be an element of another set.

20
00:01:42,390 --> 00:01:51,920
‫I create set is one, this is an empty set, another empty set, a to and I'll try to add as to twist

21
00:01:51,930 --> 00:01:55,480
‫one is to not add of hesta.

22
00:01:55,920 --> 00:02:02,550
‫I'll get an error because a mutable object cannot be an element of a set.

23
00:02:03,060 --> 00:02:10,110
‫But if I want to add the first set is the element of the set, I won't get an error.

24
00:02:13,920 --> 00:02:18,270
‫Let's see another difference between sex and frozen sex.

25
00:02:21,300 --> 00:02:31,380
‫That one is an empty set and now set to equal is set, one that could be set to is a copy of that one,

26
00:02:31,530 --> 00:02:40,350
‫but they are different objects such as that one is said to will return false legs, do the same with

27
00:02:40,350 --> 00:02:51,060
‫four thousand six if that one equals four hours on a set and if set two equals FZ one that copy.

28
00:02:52,260 --> 00:02:58,920
‫Being immutable, they are the same object, they are saved at the same memory address.

29
00:02:59,250 --> 00:03:09,750
‫So if that one is ever said to will return to all methods and operations that don't modify the set are

30
00:03:09,750 --> 00:03:12,240
‫also available to four thousand six.

31
00:03:13,300 --> 00:03:15,910
‫For example, let's see the intersection.

32
00:03:22,180 --> 00:03:28,810
‫These are two sex and using these sex, I'm going to create two frozen sex.

33
00:03:34,860 --> 00:03:46,590
‫Aswan, that intersection off is to eat with a set with the common element and the next three, if Aswan

34
00:03:46,590 --> 00:03:56,520
‫and I can use the intersection method or the end operator, if it's too we talked for hours on set with

35
00:03:56,520 --> 00:03:58,950
‫a single element of the common element.

36
00:04:00,050 --> 00:04:07,880
‫All of these operations can be performed on the mixed sex and frozen sex, and the result will have

37
00:04:07,880 --> 00:04:18,680
‫the type of the first open, so S1 and if it's two, will return a set of X because the first operand,

38
00:04:18,680 --> 00:04:20,480
‫which is S1, is a set.

39
00:04:20,780 --> 00:04:21,710
‫But right.

40
00:04:21,710 --> 00:04:30,080
‫If it's two and it's one, it will return a frozen set because the first operand is a frozen set.

41
00:04:35,410 --> 00:04:38,500
‫This is all about frozen sex, thank you.


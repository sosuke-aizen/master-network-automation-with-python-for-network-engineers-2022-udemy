﻿1
00:00:00,810 --> 00:00:08,850
‫In the last lecture we've discussed about the six BASIX six are unauthorized collections of unique,

2
00:00:08,850 --> 00:00:15,880
‫immutable elements to stress the importance of the fact that the elements in ASEP have no water.

3
00:00:16,200 --> 00:00:19,050
‫I would like to give you a very short example.

4
00:00:20,320 --> 00:00:28,810
‫I create a set as one equals one, common three and as two equals two.

5
00:00:28,990 --> 00:00:29,770
‫Common one.

6
00:00:30,000 --> 00:00:30,460
‫My three.

7
00:00:31,760 --> 00:00:41,620
‫These two states are equal, even if they have the same elements, but in another order as one equals

8
00:00:41,630 --> 00:00:51,140
‫equals as two will return to as one is as two return false, they are not the same set.

9
00:00:52,270 --> 00:00:57,550
‫Anyway, that's not applicable to leaks, I'm creating a list.

10
00:00:58,960 --> 00:01:02,890
‫And L2 equals to common commentary.

11
00:01:04,110 --> 00:01:06,460
‫L1 equals equals L2.

12
00:01:06,900 --> 00:01:10,260
‫Sorry, I should have used to equal size.

13
00:01:15,000 --> 00:01:19,440
‫And L1 is allowed to return also false.

14
00:01:21,550 --> 00:01:29,740
‫Now it's time to see what are the most important set methods and operations that is the ADD method.

15
00:01:29,980 --> 00:01:32,520
‫It adds an element to the set.

16
00:01:33,070 --> 00:01:41,830
‫The clear method removes all elements from the set and the copy method returns a copy of the set.

17
00:01:42,780 --> 00:01:50,370
‫Take care of it is in the case of topolice and the leaks, the assignment operator, so the equal sign

18
00:01:50,550 --> 00:01:55,510
‫doesn't create a copy of a set pattern reference to the same address.

19
00:01:56,100 --> 00:01:59,160
‫I'll show you shortly some examples.

20
00:01:59,460 --> 00:02:02,970
‫Then we have to discard and remove methods.

21
00:02:03,330 --> 00:02:07,050
‫They remove the specified element from the set.

22
00:02:08,410 --> 00:02:17,590
‫The move method searches for the give one element in the set and removes it if the element best to move

23
00:02:17,590 --> 00:02:19,240
‫method doesn't exist.

24
00:02:19,360 --> 00:02:23,550
‫A key error exception is for down the discard.

25
00:02:23,550 --> 00:02:28,060
‫The method removes an element from a set if it is a member.

26
00:02:28,510 --> 00:02:30,590
‫If the element is not a member.

27
00:02:30,760 --> 00:02:31,990
‫It does nothing.

28
00:02:32,470 --> 00:02:34,540
‫Then we have the pop method.

29
00:02:34,720 --> 00:02:38,590
‫It removes and returns a random element from the set.

30
00:02:39,100 --> 00:02:42,580
‫Lexy examples with all these methods.

31
00:02:44,880 --> 00:02:47,910
‫As one equals one comma, two commentary.

32
00:02:49,940 --> 00:02:58,790
‫A standard ad of ABC, this is a string, so I've added an element to the set.

33
00:03:00,180 --> 00:03:04,770
‫Isn't that clear, removes all elements from the set?

34
00:03:06,240 --> 00:03:08,460
‫Now, Aswan is an empty set.

35
00:03:10,100 --> 00:03:12,890
‫Now, I want to show you the copy method.

36
00:03:16,130 --> 00:03:25,370
‫This is my is one set and now is two equals as one as three equals a standard copy.

37
00:03:27,110 --> 00:03:31,750
‫S1 and S2 are references to the same address.

38
00:03:31,760 --> 00:03:33,780
‫In fact, they are the same object.

39
00:03:34,400 --> 00:03:39,290
‫Let's test it as one dot at of five point six.

40
00:03:40,560 --> 00:03:48,240
‫Is to also contains that element, we can see how five point six belongs to us to.

41
00:03:50,610 --> 00:03:53,820
‫But if I write a story about it, then.

42
00:03:55,210 --> 00:03:57,790
‫I've modified only S3.

43
00:03:58,830 --> 00:04:02,640
‫S1 and S2 haven't been modified.

44
00:04:03,800 --> 00:04:11,390
‫A swan is as to this will return to but a swan is as free, will return false.

45
00:04:12,000 --> 00:04:16,730
‫That's the difference between the copy method and the assignment operator.

46
00:04:19,320 --> 00:04:24,510
‫Let's move method is one that we move off three.

47
00:04:26,040 --> 00:04:28,410
‫It removed the value three.

48
00:04:30,080 --> 00:04:38,670
‫If I seen an item that doesn't belong to the set, I'll get an exception, an ad or a swan that remove

49
00:04:38,870 --> 00:04:42,140
‫all of them then doesn't belong to the set.

50
00:04:43,760 --> 00:04:48,590
‫It's one that discard often won't return an error.

51
00:04:49,980 --> 00:04:57,270
‫Lexi, now the pop method, it removes and returns a random element from the set, let's create a set

52
00:04:57,270 --> 00:04:58,230
‫called Names.

53
00:04:58,800 --> 00:05:02,430
‫The first element is then John and Mary.

54
00:05:05,010 --> 00:05:08,250
‫And winner equals names, dot, pop.

55
00:05:08,940 --> 00:05:12,180
‫In fact, I am choosing a random winner.

56
00:05:16,230 --> 00:05:17,760
‫And the winner is then.


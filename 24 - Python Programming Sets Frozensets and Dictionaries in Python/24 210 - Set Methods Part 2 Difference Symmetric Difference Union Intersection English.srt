﻿1
00:00:01,110 --> 00:00:08,880
‫Python has an implementation of Forsakes that supports many set operations, we've seen some methods

2
00:00:08,880 --> 00:00:12,180
‫and operations in the previous lecture, Licalsi.

3
00:00:12,300 --> 00:00:15,110
‫Now, what other operations are available?

4
00:00:16,050 --> 00:00:23,070
‫All the following operations return a new set, don't mutate of your original set or six.

5
00:00:25,220 --> 00:00:33,440
‫The first method is different if we set one and set two are two six, the difference method returns

6
00:00:33,440 --> 00:00:41,210
‫a new set containing the difference between set one and set two, which are the elements that exist

7
00:00:41,210 --> 00:00:44,450
‫only inside one, but not in set to.

8
00:00:45,700 --> 00:00:46,900
‫Lexia, an example.

9
00:00:57,560 --> 00:01:01,250
‫Said one dot difference of said to.

10
00:01:03,280 --> 00:01:11,860
‫And it took it to a new set with two elements, the elements that belong to Satan but not to settle

11
00:01:12,460 --> 00:01:18,790
‫the previous sin tax is equivalent to set one minus set to.

12
00:01:19,730 --> 00:01:25,180
‫There is a slight difference between using the method and using the operator.

13
00:01:26,320 --> 00:01:34,570
‫When using the method, the argument can also be uneatable, but when using the operator, both arguments

14
00:01:34,570 --> 00:01:36,230
‫must be of typeset.

15
00:01:36,490 --> 00:01:41,370
‫In fact, Python will convert the iterable we passed in to a set.

16
00:01:41,950 --> 00:01:48,020
‫This is applicable to all the methods and operations that will follow in this lecture.

17
00:01:48,580 --> 00:01:55,450
‫So say one dot difference and I am passing at least at least being uneatable.

18
00:01:58,000 --> 00:02:04,660
‫It didn't return an error, but if I write that one minus and the least.

19
00:02:06,250 --> 00:02:07,610
‫I'll get an error.

20
00:02:08,800 --> 00:02:17,200
‫Another method is the semantic difference, the semantic difference of two six six one and set two is

21
00:02:17,200 --> 00:02:24,580
‫the set of elements which are in either of the six, but not in both election example.

22
00:02:29,140 --> 00:02:40,360
‫Said one dot symmetric difference offset set to end it with a set that contains the elements that are

23
00:02:40,390 --> 00:02:43,750
‫in either of the six but not in both.

24
00:02:45,550 --> 00:02:49,750
‫We can also, right, said one car, it said to.

25
00:02:51,200 --> 00:02:52,820
‫We've got the same result.

26
00:02:54,350 --> 00:03:01,730
‫The next method we are going to talk about is the union, the union of two or more six is the set of

27
00:03:01,730 --> 00:03:05,090
‫all unique elements present in all six.

28
00:03:06,250 --> 00:03:12,580
‫In Python, Unión allows an arbitrary number of arguments, Oleksiy, an example.

29
00:03:14,060 --> 00:03:17,840
‫Set one that union off set to.

30
00:03:19,320 --> 00:03:24,150
‫The same result can be obtained using a pipe or a vertical bar.

31
00:03:27,290 --> 00:03:30,380
‫Sorry, we have set one, not S1.

32
00:03:32,820 --> 00:03:34,410
‫And this is the union.

33
00:03:35,790 --> 00:03:42,450
‫The next method is at the intersection of the intersection method returns, a set that contains the

34
00:03:42,450 --> 00:03:50,040
‫element that exist in both sake's or in all six, if the method is used with more than two.

35
00:03:50,040 --> 00:03:50,460
‫Six.

36
00:03:54,880 --> 00:03:58,720
‫Set on that intersection off, set to.

37
00:04:02,350 --> 00:04:07,210
‫My six have a single element in common and the next five.

38
00:04:08,730 --> 00:04:12,840
‫We can also, right, set one end set to.

39
00:04:15,500 --> 00:04:19,120
‫Another method is, is this joint?

40
00:04:19,550 --> 00:04:28,820
‫It returns true if two sake's are disjoined, six, if not, it returns false to six are set to be disjoined

41
00:04:29,120 --> 00:04:31,550
‫if they have no common elements.

42
00:04:33,310 --> 00:04:37,450
‫One that is disjoined of said to.

43
00:04:39,470 --> 00:04:47,120
‫It returned to force because they have an element in common, but if I create another set, say, said,

44
00:04:47,120 --> 00:04:49,340
‫forget equals six, comma seven.

45
00:04:51,610 --> 00:04:57,490
‫Set one that is disjoined of set free will return to.

46
00:05:00,690 --> 00:05:09,810
‫There are also the less less than or equal, greater than and greater than or equal operators used for

47
00:05:09,810 --> 00:05:11,580
‫containment testing.

48
00:05:12,890 --> 00:05:20,870
‫Less then operator returns to the set from the left hand side of the operator is contained in the set

49
00:05:20,870 --> 00:05:28,610
‫from the right hand side of the operator, less than or equal returns, true, if it is included in

50
00:05:28,610 --> 00:05:30,200
‫or equal to another set.

51
00:05:32,930 --> 00:05:38,300
‫An example, this is set, one set, too, and set free.

52
00:05:39,560 --> 00:05:45,620
‫I'm creating a new set called Set for equals six come seven Comite.

53
00:05:47,300 --> 00:05:57,860
‫Said three less than or equal set for it returned to said one greater than set free will return false.

54
00:05:58,590 --> 00:06:02,430
‫OK, these are the most important set methods.

55
00:06:02,900 --> 00:06:10,490
‫There are also other methods that may be are not used so frequently and you can find them in Python

56
00:06:10,500 --> 00:06:13,160
‫official documentation regarding sex.

57
00:06:13,730 --> 00:06:19,120
‫This is a text to this lecture as a final thought about sex.

58
00:06:19,130 --> 00:06:27,380
‫I want to tell you that the look up time for an element in a set is extremely low compared to the look

59
00:06:27,380 --> 00:06:35,210
‫up for an element in a list or a couple more than that, the lookup time is constant no matter how big

60
00:06:35,210 --> 00:06:44,820
‫the cities this is because a set is implemented as a hash table and Python does a hash lookup, it doesn't

61
00:06:44,820 --> 00:06:48,580
‫scan through all the elements in the set to find it.

62
00:06:48,920 --> 00:06:51,050
‫It does a direct lookup.

63
00:06:51,620 --> 00:06:58,600
‫The disadvantage of six comparing two lists and topolice is that they consume more memory.

64
00:06:58,940 --> 00:07:02,660
‫So there is a tradeoff between speed and memory.


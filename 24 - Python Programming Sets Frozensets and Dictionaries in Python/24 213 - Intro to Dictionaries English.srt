﻿1
00:00:00,490 --> 00:00:02,200
‫Hello, guys, and welcome back.

2
00:00:02,350 --> 00:00:07,360
‫In this section will start to take a deep look at dictionaries in Python.

3
00:00:07,750 --> 00:00:15,820
‫Dictionaries are everywhere in python modules, classes, objects, even six are all implemented as

4
00:00:15,820 --> 00:00:16,630
‫dictionaries.

5
00:00:17,230 --> 00:00:25,300
‫In Python, a dictionary is an unordered collection of key value pairs separated by commas and enclosed

6
00:00:25,300 --> 00:00:26,470
‫by curly braces.

7
00:00:27,090 --> 00:00:34,150
‫A dictionary is a mutable object, meaning that we can add to or remove elements from the dictionary.

8
00:00:34,690 --> 00:00:41,140
‫If you are used to other programming languages like C, JavaScript, Ruby, ergo, you can think of

9
00:00:41,140 --> 00:00:47,860
‫a dictionary being just like an object in JavaScript or in Ruby or a map engo.

10
00:00:48,900 --> 00:00:57,630
‫The basic structure of a dictionary element includes two things one is the key and then the value we

11
00:00:57,630 --> 00:01:01,230
‫use a colon to separate the key from the value.

12
00:01:02,040 --> 00:01:09,540
‫Values can be any python object, but keys need to be hash or immutable objects.

13
00:01:09,990 --> 00:01:17,140
‫We can have an integer, a string tuple or a frozen set is a key, but not a list or asset.

14
00:01:17,550 --> 00:01:21,990
‫The last two are immutable objects and are not allowed as keys.

15
00:01:23,080 --> 00:01:31,210
‫One important thing to know is that Keys must also be unique, so keys are unique and immutable.

16
00:01:31,510 --> 00:01:34,600
‫Values, by the other hand, don't have to be unique.

17
00:01:35,170 --> 00:01:36,310
‫Keep this in mind.

18
00:01:37,640 --> 00:01:39,560
‫Let's see how to create a dictionary.

19
00:01:40,190 --> 00:01:47,510
‫This is an empty dictionary or dict to equals, and I am using the dict function.

20
00:01:54,970 --> 00:02:03,730
‫Another dictionary person equals the first key is a string, let's say name, and the associated value

21
00:02:03,910 --> 00:02:04,840
‫is John.

22
00:02:06,670 --> 00:02:14,580
‫Another key age, exhausting, so it must be enclosed in a single or double Coggs.

23
00:02:16,020 --> 00:02:17,750
‫Equals 30.

24
00:02:17,940 --> 00:02:21,030
‫This is an integer, the value of this key.

25
00:02:25,360 --> 00:02:33,010
‫If I want to add another key value pair, I simply write the name of the dictionary and the key between

26
00:02:33,010 --> 00:02:35,320
‫square brackets equals the value.

27
00:02:35,650 --> 00:02:39,040
‫So person of, let's say the name of the new key.

28
00:02:39,430 --> 00:02:40,030
‫Great.

29
00:02:42,970 --> 00:02:49,960
‫Equals and the value is at least like, say, seven, 10, comma five.

30
00:02:53,770 --> 00:03:02,260
‫Earlier, I said that peace must be immutable objects, so if I want to have a key, at least I'll get

31
00:03:02,260 --> 00:03:02,470
‫an.

32
00:03:03,190 --> 00:03:07,390
‫Let's create the dictionary one more time with a list is a key.

33
00:03:14,990 --> 00:03:18,830
‫Sorry, of course, between the key and the value, I must use a Colin.

34
00:03:21,970 --> 00:03:30,280
‫Type error on a ball type list, but if instead of list I have a tuple, I'll get no error.

35
00:03:30,940 --> 00:03:36,610
‫This is a Topal because I've used parentheses and there is no error.

36
00:03:39,810 --> 00:03:47,640
‫In the next lecture, we'll see how we can work with dictionaries and what operations and methods are

37
00:03:47,640 --> 00:03:48,350
‫available.


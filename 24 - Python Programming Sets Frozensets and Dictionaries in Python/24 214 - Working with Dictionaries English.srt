﻿1
00:00:00,780 --> 00:00:08,610
‫Now let's see how we can work with dictionaries first, let's extract the corresponding value of a specified

2
00:00:08,610 --> 00:00:09,000
‫key.

3
00:00:09,450 --> 00:00:15,720
‫This can be done similarly to accessing elements of a list only that we don't specify an index.

4
00:00:15,720 --> 00:00:20,250
‫We specify the associated key for the value we want to extract.

5
00:00:21,390 --> 00:00:24,540
‫So person in the key between square brackets.

6
00:00:26,900 --> 00:00:32,210
‫And I've extracted the value of the key named H.

7
00:00:33,540 --> 00:00:40,920
‫Earlier, I said that a dictionary is immutable, how can we add a new key value pair to the dictionary?

8
00:00:41,460 --> 00:00:45,900
‫This is done by simply assigning a new value to the new key.

9
00:00:46,860 --> 00:00:52,380
‫So person of and the new key city equals Burlin.

10
00:00:57,310 --> 00:01:04,050
‫If you want to modify the value associated with a key, you simply write the name of the dictionary

11
00:01:04,060 --> 00:01:07,900
‫and the key between square brackets and the new value.

12
00:01:09,440 --> 00:01:10,840
‫Person of CTY.

13
00:01:11,950 --> 00:01:13,660
‫Equals London.

14
00:01:18,490 --> 00:01:26,350
‫If the key doesn't exist, it raises a key error exception, we'll see what are these exceptions in

15
00:01:26,350 --> 00:01:27,600
‫another lecture?

16
00:01:28,000 --> 00:01:33,750
‫So person of let's say five five doesn't exist is a key.

17
00:01:34,600 --> 00:01:36,670
‫So I've got the key error.

18
00:01:38,080 --> 00:01:45,430
‫If you want to avoid this error, you can use the A function that returns the value associated with

19
00:01:45,430 --> 00:01:51,820
‫the key if the key exists or a default value if specified in the key doesn't exist.

20
00:01:52,180 --> 00:01:58,330
‫If the key doesn't exist and no default value is given, it returns none.

21
00:02:00,120 --> 00:02:05,610
‫Person don't get the name of the method of the.

22
00:02:07,460 --> 00:02:13,790
‫If I don't specify a default value, it will return money, of course, if the key doesn't exist and

23
00:02:13,790 --> 00:02:20,150
‫this is the case here, or I can specify a default value, for example, key does not exist.

24
00:02:25,160 --> 00:02:27,560
‫It returned that default value.

25
00:02:29,600 --> 00:02:34,910
‫If I specify an existing key, it will return the corresponding value.

26
00:02:36,810 --> 00:02:44,700
‫We can also delete a bear from the dictionary using the Bell instruction bell, the name of the dictionary

27
00:02:44,940 --> 00:02:45,600
‫and the key.

28
00:02:48,590 --> 00:02:51,770
‫And I've deleted that key value pair.

29
00:02:53,760 --> 00:03:02,310
‫If the kid doesn't exist, it returns Akir, let's delete one more time the key value associated with

30
00:03:02,310 --> 00:03:03,410
‫the key.

31
00:03:03,450 --> 00:03:07,740
‫It doesn't exist because I've already deleted it.

32
00:03:09,320 --> 00:03:10,640
‫OK, so keep our.

33
00:03:11,390 --> 00:03:15,830
‫And if I want to avoid this error, I can use the pop method.

34
00:03:16,900 --> 00:03:23,780
‫Pop method removes a air and returns the value of a specified key from the dictionary.

35
00:03:24,220 --> 00:03:27,170
‫It works similarly to get method.

36
00:03:28,060 --> 00:03:31,530
‫So if the kid doesn't exist, it returns a key error.

37
00:03:31,690 --> 00:03:35,630
‫But I can specify a default value for this case.

38
00:03:36,040 --> 00:03:37,170
‫Let's see how it works.

39
00:03:39,070 --> 00:03:45,220
‫So name equals person dot pop off in the name of the name.

40
00:03:47,970 --> 00:03:52,500
‫It removed that key value pair and returned the value.

41
00:03:53,710 --> 00:03:58,960
‫So name contains the value associated with the name Kee.

42
00:04:01,030 --> 00:04:03,730
‫That bear has been removed from the dictionary.

43
00:04:04,760 --> 00:04:11,870
‫If I try to pop with that value one more time, I'll get on there because that key doesn't exist.

44
00:04:12,440 --> 00:04:17,900
‫But I can specify here a default value like, say, no key.

45
00:04:20,100 --> 00:04:24,690
‫And there is no error and the default value has been returned.

46
00:04:25,770 --> 00:04:33,030
‫There is also the land function available, and it's the same function available to leeks and topolice,

47
00:04:33,960 --> 00:04:37,560
‫it returns the number of key value pairs in the dictionary.

48
00:04:38,320 --> 00:04:45,060
‫So land of person be talking to one because there is only one pair in the dictionary.

49
00:04:47,090 --> 00:04:52,580
‫Another method is clear, it clear out all items of the dictionary.

50
00:04:57,980 --> 00:05:00,110
‫Now the dictionary is empty.

51
00:05:01,820 --> 00:05:06,610
‫That's enough for the moment about dictionary basics in the next election.

52
00:05:06,630 --> 00:05:11,690
‫We will see what are the most important dictionary methods and operations.


﻿1
00:00:00,980 --> 00:00:10,160
‫Now we'll start talking about six six hour unordered collections of unique elements, asset is not a

2
00:00:10,160 --> 00:00:11,840
‫sequence type in Python.

3
00:00:12,080 --> 00:00:17,090
‫You cannot access items in a set by index because there is no order.

4
00:00:17,390 --> 00:00:20,240
‫There is no concept of the first element of the set.

5
00:00:20,270 --> 00:00:24,020
‫The second element, the third element and so on.

6
00:00:24,350 --> 00:00:30,060
‫In real world, there are many collections that are unordered without duplicates.

7
00:00:30,650 --> 00:00:32,800
‫Let's take just a few examples.

8
00:00:33,440 --> 00:00:39,140
‫Social Security numbers, email addresses, IP addresses and so on.

9
00:00:39,290 --> 00:00:44,390
‫All of these are collections of unique and unordered elements.

10
00:00:44,630 --> 00:00:51,110
‫Duplicates are not allowed if we want to have such collections in our applications.

11
00:00:51,290 --> 00:00:53,260
‫It's a good idea to save them.

12
00:00:53,300 --> 00:00:55,500
‫Is six US.

13
00:00:55,550 --> 00:01:00,020
‫It is a mutable object and that means it can be modified.

14
00:01:00,290 --> 00:01:05,050
‫We can add elements or we can remove elements from the set of.

15
00:01:05,080 --> 00:01:14,480
‫There is also another object type called frozen asset that is immutable as it uses curly braces and

16
00:01:14,480 --> 00:01:17,030
‫commas to separate elements.

17
00:01:17,900 --> 00:01:30,110
‫Lexy examples to create an empty set, we use the set constructor S1 equals set and a pair of parentheses.

18
00:01:33,980 --> 00:01:40,460
‫We cannot create an empty set using a pair of curly braces that creates a dictionary.

19
00:01:45,240 --> 00:01:46,710
‫This is a dictionary.

20
00:01:47,720 --> 00:01:56,090
‫Like leeks and apples, the elements of the set can have different pipes, let's create a set that contains

21
00:01:56,090 --> 00:01:59,480
‫an integer afloat and the string.

22
00:02:03,690 --> 00:02:11,120
‫One key difference is that the elements of the set can have different type, but must be immutable.

23
00:02:11,280 --> 00:02:17,250
‫For example, a list which is a mutable object cannot be an element of a set.

24
00:02:18,680 --> 00:02:25,740
‫I create another set as two that contains an integer afloat and the tuple.

25
00:02:26,040 --> 00:02:28,770
‫I use parentheses to create a table.

26
00:02:33,280 --> 00:02:42,400
‫And that's OK, all items of the set are immutable, but if I try to create another set that contains

27
00:02:42,400 --> 00:02:47,420
‫at least at least is a mutable object, I'll get an error.

28
00:02:47,680 --> 00:02:50,220
‫I use square brackets to create a list.

29
00:02:52,820 --> 00:02:56,630
‫I've got the type error on Hezbollah type list.

30
00:02:57,930 --> 00:03:01,200
‫In this context, unaccessible means mutable.

31
00:03:02,370 --> 00:03:11,640
‫As I said earlier, a citizen unordered collection, and that means we cannot use indexes to get items

32
00:03:11,640 --> 00:03:18,490
‫of the set indexes mean order if I try S1 of zero.

33
00:03:18,810 --> 00:03:25,080
‫So with the element with index zero, I'll get an error set.

34
00:03:25,080 --> 00:03:27,810
‫Object does not support indexing.

35
00:03:29,380 --> 00:03:37,070
‫We can transfer at least a PayPal and a set into the other by using the corresponding constructor.

36
00:03:37,780 --> 00:03:41,650
‫I have at least one two one three Topal.

37
00:03:45,050 --> 00:03:46,250
‫And by creator said.

38
00:03:47,260 --> 00:03:52,310
‫Using the set constructor and I'll pasión the list.

39
00:03:59,920 --> 00:04:04,690
‫I can also pass in a string, which is an iterable object.

40
00:04:06,320 --> 00:04:09,680
‫Like, say, Python programming is cool.

41
00:04:12,830 --> 00:04:21,320
‫This is one we can see how the very one appears only once, even if one appeared twice in the least.

42
00:04:23,520 --> 00:04:34,140
‫This is a story there is also no duplicate, and this is S. three, there is no duplicate and no order.

43
00:04:37,680 --> 00:04:45,330
‫A common use for sex is to take a list that contains duplicates and return a set of that list.

44
00:04:47,330 --> 00:04:55,910
‫This is a list that contains some mock addresses, but there are duplicates, if I want a list without

45
00:04:55,910 --> 00:05:01,910
‫duplicates, I must transform the list to a set and then back to a list.

46
00:05:03,240 --> 00:05:05,760
‫So set of max.

47
00:05:07,330 --> 00:05:16,450
‫These are the Mac addresses without duplicates, and if I want to have a list, I can have Max unique

48
00:05:16,870 --> 00:05:21,700
‫equals list of set of Max.

49
00:05:26,130 --> 00:05:29,820
‫And these are the unique mock addresses.

50
00:05:34,750 --> 00:05:37,060
‫There are three unique addresses.

51
00:05:38,030 --> 00:05:45,530
‫An asset is an iterable object, we can iterate over a set exactly like overpoliced a paper or a string.

52
00:05:49,930 --> 00:05:53,320
‫For item in S to Colin.

53
00:05:55,400 --> 00:05:56,660
‫Print as to.

54
00:05:58,770 --> 00:06:00,360
‫Sorry, print item.

55
00:06:02,470 --> 00:06:05,320
‫These are the elements of the set.

56
00:06:06,500 --> 00:06:12,420
‫Even if it seems that the items are in the same order, they are not.

57
00:06:13,010 --> 00:06:14,810
‫Don't count on Ardler.

58
00:06:19,500 --> 00:06:21,720
‫Now it seems they are in the same order.

59
00:06:22,110 --> 00:06:32,430
‫Let's try another example as one equals an opening curly brace, one of a string ABC afloat and another

60
00:06:32,430 --> 00:06:32,910
‫string.

61
00:06:36,860 --> 00:06:45,590
‫This is my set, and you can see how the items are in another order to check if an element belongs or

62
00:06:45,590 --> 00:06:57,110
‫not to a set, there are the in and not in operators, for example, one in a one and of extra or X

63
00:06:57,110 --> 00:06:58,340
‫in a swan.

64
00:06:58,700 --> 00:06:59,360
‫X false.

65
00:07:00,970 --> 00:07:05,140
‫But why not in a swan returned to.

66
00:07:06,290 --> 00:07:13,300
‫That's enough for the moment about the six basics, there are a couple of set methods and we'll talk

67
00:07:13,300 --> 00:07:15,460
‫about them in the next video.


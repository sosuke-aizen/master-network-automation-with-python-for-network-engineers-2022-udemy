
    
    
    #################################
    ## Dictionaries in Python
    ## A dictionary is an unordered collection of key: value pairs
    #################################
    
    dict1 = {}      # empty dictionary
    dict2 = dict()  # empty dictionary
    
    ## Keys for dictionaries have to be immutable types. This is to ensure that
    ## the key can be converted to a constant hash value for quick look-ups.
    #invalid_dict = {[1,2,3]: "123"}  # => Raises a TypeError: unhashable type: 'list'
    #a key of type tuple is permitted (immutable). Values can be of any type, however.
    valid_dict = {(1,2,3):[1,2,3], 3: 'abc', 'abc':{14,'a'}, 4.5:True}
    
    product = {'id':1234, 'name':'Laptop'}
    product['seller'] = 'Amazon'        # adds a new key:value pair
    product.update({'price':888.99})    # another way to add to a dictionary
    
    p = product['price']                # getting the value of a key,  p is 888.00
    
    del product['seller']        # removing a key:value pair
    print(product)               # => {'id': 1234, 'name': 'Laptop', 'price': 888.99}
    #seller = product['seller']  # Looking up a non-existing key is a KeyError
    
    # Use get() method to retrieve the value of a key
    product.get("id")                   # => 1234
    product.get("review")               # => None -> it doesn't return KeyError
    # The get method supports a default argument when the value is missing
    product.get("id", 4)                # => 1234
    product.get("review", 'N/A')        # => 'N/A'
    
    ## pop() removes specified key and return the corresponding value.
    ## If key is not found, a default value is given, otherwise KeyError is raised
    name = product.pop('name')    # name is 'Laptop'
    print(product)                # => {'id': 1234, 'price': 888.99}
    
    #name = product.pop('name')                 # => KeyError: 'name', key 'name' doesn't exist anymore
    name = product.pop('name', 'No such key')   # => name is 'No such key'
    
    
    #################################
    ## Dictionary Operations and Methods
    #################################
    
    product = {'id':1234, 'price':888.99}
    
    ## in and not in operators test dictionary key membership
    ## there is no direct method to test if a corresponding value belongs to the dictionary
    'price' in product  # => True
    5 in product        # => False
    
    ## Dictionary views
    keys = product.keys()       # getting all keys as an iterable
    keys = list(keys)           # it can be converted to a list
    print(keys)                 # => ['id', 'price']
    
    values = product.values()   # getting all values as an iterable
    values = list(values)       # it can be converted to a list
    print(values)               # => [1234, 888.99]
    
    key_values = product.items()    # getting all key:value pairs as tuples
    key_values = list(key_values)   # it can be converted to a list of tuples
    print(key_values)               # => [('id', 1234), ('price', 888.99)]
    
    ## Iterating over a dictionary
    
    ## Iterating over the keys
    for k in product:
        print(f'key:{k}')
    #equivalent to:
    for k in product.keys():
        print(f'key:{k}')
    
    ## Iterating over the values
    for v in product.values():
        print(f'value:{v}')
    
    # Iterating over both the keys and the values
    for k,v in product.items():
        print(f'key:{k}, value:{v}')
    
    ## Creates a copy of a dictionary
    prod = product.copy()
    ## Returns the no. of key:value pairs in the dictionary
    len(prod)
    # Removes all items from dictionary
    product.clear()


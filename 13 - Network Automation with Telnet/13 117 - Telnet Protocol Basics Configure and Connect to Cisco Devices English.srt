﻿1
00:00:00,390 --> 00:00:07,440
‫Hello and welcome back in this lecture, will talk about the telnet protocol and how to configure Cisco

2
00:00:07,440 --> 00:00:14,580
‫devices to accept incoming telnet connections, as you probably already know there, that is a protocol

3
00:00:14,580 --> 00:00:16,500
‫used for remote management.

4
00:00:16,980 --> 00:00:18,420
‫In most cases.

5
00:00:18,420 --> 00:00:25,560
‫We use S.H. for remote management, but there are times when I could use telnet as well.

6
00:00:26,580 --> 00:00:34,200
‫Bellette is one of the first protocols used for remote management and is supported by virtually any

7
00:00:34,200 --> 00:00:42,000
‫device, especially the old ones, which do not support this message, or maybe someone who simply prefers

8
00:00:42,000 --> 00:00:46,760
‫to use it because it's simple and the security is not an issue.

9
00:00:47,950 --> 00:00:55,450
‫Note that compared to Secich, Bellette is not secure, the connection between the client and the server

10
00:00:55,570 --> 00:00:59,920
‫is not encrypted and all data is transmitted in clear text.

11
00:01:00,980 --> 00:01:08,480
‫However, there are cases when another protocol secures the connection and then there is no security

12
00:01:08,480 --> 00:01:09,680
‫issue at all.

13
00:01:10,130 --> 00:01:17,870
‫An example would be when we connect to a Telnet server using a VPN or inside a private LAN, which is

14
00:01:17,870 --> 00:01:18,980
‫considered secure.

15
00:01:19,880 --> 00:01:26,020
‫So my advice is to use telnet only if there is another layer of security applied.

16
00:01:26,360 --> 00:01:31,310
‫Do not use it over the public Internet usage instead.

17
00:01:32,710 --> 00:01:41,050
‫In the first example of this section will use this simple topology with only one router, I want to

18
00:01:41,050 --> 00:01:44,170
‫keep it simple and build it from the ground up.

19
00:01:44,650 --> 00:01:50,830
‫The router has already configured with an IP address and there is a layer of reconnection available

20
00:01:51,010 --> 00:01:54,100
‫between the host that runs Python and IT.

21
00:01:55,850 --> 00:01:57,860
‫Let's check it using pink.

22
00:01:58,070 --> 00:02:00,860
‫So pink and the IP address of the router.

23
00:02:02,810 --> 00:02:07,670
‫The then that one, that one that Ben being is working.

24
00:02:08,750 --> 00:02:13,440
‫The next step is to configure the delta, to accept incoming to let connections.

25
00:02:13,970 --> 00:02:15,800
‫Let's see how to configure it.

26
00:02:17,290 --> 00:02:25,420
‫I'm opening a console and entering the enable mode and then the global configuration mode, the enable

27
00:02:25,420 --> 00:02:30,010
‫password is Cisco Konforti.

28
00:02:31,130 --> 00:02:38,160
‫And in the global configuration mode, I'm going to create a new user called my admin and the password

29
00:02:38,210 --> 00:02:43,520
‫Cisco user name, my admin secret Cisco.

30
00:02:45,250 --> 00:02:51,640
‫If this is the first time when you set up that out there, it's also recommended to configure and enable

31
00:02:51,640 --> 00:02:56,830
‫password, so execute, enable secret and your desired password.

32
00:02:57,130 --> 00:03:00,370
‫In this example, I have the Cisco password.

33
00:03:01,030 --> 00:03:04,000
‫I've already done it, so I won't do it again.

34
00:03:04,450 --> 00:03:11,830
‫Then I'm entering the virtual alliance used for incoming Bellette and SSX connections, which the activity

35
00:03:11,980 --> 00:03:16,030
‫lines so line with UI zero.

36
00:03:17,280 --> 00:03:22,050
‫And for here, I'm executing Log-in local.

37
00:03:25,400 --> 00:03:32,870
‫The router will use for authentication the users defined locally under water and then transport input,

38
00:03:33,380 --> 00:03:35,170
‫Bellette and SSX.

39
00:03:35,300 --> 00:03:41,180
‫I'm allowing both Telnet and essayistic as the incoming protocols of XOL.

40
00:03:41,360 --> 00:03:42,650
‫I'm saving the config.

41
00:03:44,000 --> 00:03:49,460
‫Let's test telnet, their outlook is the telnet server, and we need a Telnet client.

42
00:03:50,440 --> 00:03:56,890
‫We can use party or the one that's included in Windows first, let's try putting.

43
00:03:59,960 --> 00:04:01,790
‫Of the appearance of their outer.

44
00:04:02,850 --> 00:04:11,760
‫10.1, that Dunbarton, the default port is twenty three and I'm choosing the ballot connection type

45
00:04:12,690 --> 00:04:13,530
‫and open.

46
00:04:16,520 --> 00:04:18,920
‫And it's asking for the username.

47
00:04:20,240 --> 00:04:23,660
‫My admin and password, Cisco.

48
00:04:26,460 --> 00:04:27,360
‫And I mean.

49
00:04:31,130 --> 00:04:37,940
‫If you don't have party installed or simply prefer the classical telnet comment, you must first enable

50
00:04:37,940 --> 00:04:38,150
‫it.

51
00:04:38,840 --> 00:04:44,950
‫This is required for Windows 10 for older versions of Windows or Linux.

52
00:04:44,960 --> 00:04:50,850
‫The Telnet client, which is the Telnet comment, is already available in the START menu.

53
00:04:51,050 --> 00:04:52,550
‫We searched for telnet.

54
00:04:54,530 --> 00:04:58,130
‫Then we click on Talk and Windows features on or off.

55
00:05:02,760 --> 00:05:06,360
‫Here in the east, I'm selecting capelet Indochine.

56
00:05:08,180 --> 00:05:14,180
‫I've already done it, so it's not necessary to select it again, but in your case, check the telnet

57
00:05:14,180 --> 00:05:14,630
‫client.

58
00:05:16,070 --> 00:05:21,440
‫Now, from that, I can write the letter and the IP address of the router.

59
00:05:27,630 --> 00:05:28,290
‫And I mean.

60
00:05:29,330 --> 00:05:35,810
‫In the next lecture, I'll show you how to connect from Python scripts to networking devices using Pallette.


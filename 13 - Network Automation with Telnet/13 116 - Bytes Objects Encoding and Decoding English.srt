﻿1
00:00:00,940 --> 00:00:08,110
‫In this lecture we'll discuss objects of typebytes, encoding and decoding. That is useful for you to

2
00:00:08,110 --> 00:00:15,360
‫know since many network automation libraries use objects of type of bytes instead of strings. Telnet

3
00:00:15,460 --> 00:00:23,800
‫and Pyserial which is the module used to connect and configure devices serially are only 2 examples.

4
00:00:23,840 --> 00:00:30,800
‫The only thing a computer can store is bytes. To store anything in a computer memory

5
00:00:30,830 --> 00:00:34,880
‫you must first encoded or converted to bytes.

6
00:00:34,880 --> 00:00:42,890
‫For example if you want to store music you must first encoded, using an encoding scheme like MP3

7
00:00:43,130 --> 00:00:44,490
‫or WAV.

8
00:00:44,600 --> 00:00:52,250
‫If you want to store a picture you must first encoded using  encode it using PNG or JPEG.
‫Or if you want to store a text,

9
00:00:52,790 --> 00:01:05,510
‫you should first encode it using ASCII or UTF-8. MP3, WAV, PNG, JPEG, ASCII and UTF-8 are examples of

10
00:01:05,600 --> 00:01:16,110
‫encoding schemes. An encoding scheme is a format to represent audio, images, text and so on as a sequence

11
00:01:16,170 --> 00:01:22,340
‫of bytes. Now, at this exact moment, when you are watching this video,

12
00:01:22,470 --> 00:01:29,430
‫your computer is working with bytes to display it is there are seeing it. Let's go to coding and dive

13
00:01:29,430 --> 00:01:37,710
‫deeper into strings and encoding, respectively decoding. A string is a sequence of characters that are

14
00:01:37,710 --> 00:01:45,930
‫an abstract concept and can't be directly stored on disk. What you can start on disk or in memory is

15
00:01:45,930 --> 00:01:54,210
‫an object of type bytes which is in fact a sequence of bytes that can be stored. Bytes objects are an

16
00:01:54,210 --> 00:02:03,780
‫immutable sequence of small integers in the range of 0 and 255, printed as UTF-8 characters when

17
00:02:03,780 --> 00:02:14,270
‫displayed. By the way ASCII is only a subset of UTF 8 consisting of only 128 characters.

18
00:02:14,490 --> 00:02:25,890
‫For example, the code for literal ‘a’ is 97 in decimal or  61 in hexadecimal and for ‘b’ it’s 98

19
00:02:25,950 --> 00:02:29,210
‫in decimal or  62 in hexadecimal.

20
00:02:29,250 --> 00:02:33,270
‫So each time when a program wants to store the letter a

21
00:02:33,420 --> 00:02:38,040
‫it stores in fact a byte with value 97 in decimal.

22
00:02:38,580 --> 00:02:46,140
‫The mapping between character strings and bytes strings or objects is done using encoding and decoding.

23
00:02:47,490 --> 00:02:50,040
‫Let's see some live examples!

24
00:02:50,250 --> 00:02:54,880
‫I'm defining a string s1 = 'abc'

25
00:02:55,020 --> 00:03:04,160
‫It consists of 3 characters and I'm encoding the string to an object of type bytes so b1 =

26
00:03:04,640 --> 00:03:11,580
‫s1.encode
‫the default encoding scheme is ('utf-8')

27
00:03:11,590 --> 00:03:13,600
‫So it's not mandatory to write it

28
00:03:19,130 --> 00:03:21,290
‫Let's see the type of b1!

29
00:03:28,600 --> 00:03:38,570
‫and we see that b1 is an object of type bytes. Being a sequence of bytes let's print the first one so

30
00:03:38,570 --> 00:03:44,770
‫print(b1[0])
‫this is the byte at the index 0

31
00:03:47,650 --> 00:03:48,930
‫and it's 97

32
00:03:52,670 --> 00:04:00,980
‫the ASCII code for letter a, 97; the second character is b.

33
00:04:01,130 --> 00:04:07,550
‫So the second byte will be 98; 98 is the decimal value for b.

34
00:04:14,600 --> 00:04:20,640
‫Let's see what happens if I print out to b1, which is of type bytes so print(b1[1])

35
00:04:24,200 --> 00:04:32,430
‫and surprise it has printed it as a string, not as a sequence of bytes as we've expected.

36
00:04:32,480 --> 00:04:40,010
‫The reason is that the built-in print() function is used to print strings so it tried and managed to

37
00:04:40,010 --> 00:04:47,570
‫get a string representation; but that's not a string; that is a bytes object and we know that from the

38
00:04:47,570 --> 00:04:57,360
‫b prefix. Let's try another example! This time will define a string as a sequence of non ASCII characters.

39
00:04:57,720 --> 00:05:03,880
‫Remember that ASCII can represent only 128 characters.

40
00:05:03,990 --> 00:05:13,540
‫These are the Latin letters including the English alphabet, the digits and the most used symbols. s2

41
00:05:13,660 --> 00:05:19,880
‫=and a string in Japanese.

42
00:05:19,910 --> 00:05:26,760
‫This means hacking in Japanese or at least that's what the Google says.

43
00:05:26,830 --> 00:05:29,560
‫These are not ASCII characters.

44
00:05:29,980 --> 00:05:31,550
‫They are UTF8

45
00:05:31,720 --> 00:05:33,500
‫I'm encoding the string.

46
00:05:33,730 --> 00:05:45,140
‫I'll get the bytes object and then I'll printed it out so b2 = s2.encode By default it is

47
00:05:45,140 --> 00:05:47,900
‫UTF8 and print

48
00:05:47,960 --> 00:05:55,200
‫b2.

49
00:05:55,450 --> 00:05:57,790
‫Notice that the print function

50
00:05:57,790 --> 00:06:06,070
‫couldn't get a string representation like before; it printed out the sequence of bytes but in base

51
00:06:06,130 --> 00:06:16,710
‫16; that's what that backslash X means: base 16. Let's see the first byte in the sequence:

52
00:06:22,100 --> 00:06:30,470
‫It's 227. To get back the string we must decoded using the same scheme which is by

53
00:06:30,470 --> 00:06:31,950
‫default UTF8.

54
00:06:32,150 --> 00:06:44,940
‫So s3, another string, b2 the bytes object.decode and I want to see s3 and if it's equal

55
00:06:45,140 --> 00:06:50,660
‫to s2 so s3==s2

56
00:06:53,540 --> 00:07:00,050
‫We've got back the same string and it is equal to s2; it returned true.


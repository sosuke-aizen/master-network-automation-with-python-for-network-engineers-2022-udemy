﻿1
00:00:00,490 --> 00:00:01,310
‫Welcome back.

2
00:00:01,870 --> 00:00:08,410
‫In the previous video, we've seen how to send commands to a single device from Python using Talibe

3
00:00:09,100 --> 00:00:15,880
‫will move ahead and see how to automate the configuration of multiple devices will use this topology

4
00:00:16,150 --> 00:00:21,450
‫with three routers, the same one used in Parkhomenko Internet misconceptions sections.

5
00:00:22,270 --> 00:00:25,870
‫I want to create a static default out on each outer.

6
00:00:26,290 --> 00:00:29,980
‫The main idea is to see how to execute configuration Cumming's.

7
00:00:29,980 --> 00:00:33,010
‫While of course you can choose to configure anything you want.

8
00:00:34,720 --> 00:00:36,310
‫I'm starting their autres.

9
00:00:38,430 --> 00:00:45,060
‫There is a working connection to each other, so before starting this lab, I advise you to check the

10
00:00:45,060 --> 00:00:52,440
‫layer three connectivity using pink and also the access to the routers using party or telnet comment.

11
00:00:53,110 --> 00:00:57,960
‫I'll do this only for one hour in the topology since I've already tested it.

12
00:01:00,130 --> 00:01:03,220
‫Ping and the IP address of the first outer.

13
00:01:09,360 --> 00:01:14,280
‫And exciting and tailored and excite address.

14
00:01:20,230 --> 00:01:21,640
‫He's working as well.

15
00:01:23,220 --> 00:01:24,700
‫We'll start from scratch.

16
00:01:25,290 --> 00:01:28,530
‫First, I'm importing the third module.

17
00:01:31,040 --> 00:01:35,360
‫I'm also importing the time module because we'll need it.

18
00:01:37,100 --> 00:01:43,130
‫And I'm creating a dictionary for each device in the topology, three quarters of dictionaries.

19
00:01:45,470 --> 00:01:55,690
‫Routon equals and a pair of curly braces host the key, Callon and the value, the IP address.

20
00:01:58,190 --> 00:02:03,250
‫The second key will be yuzu and the X value you want.

21
00:02:04,040 --> 00:02:07,150
‫Then comes the password and the value.

22
00:02:07,330 --> 00:02:11,180
‫Cisco, this dictionary is for the first outer.

23
00:02:14,230 --> 00:02:21,940
‫I'll create another two dictionaries for the second out and for the third quarter, and I'll change

24
00:02:21,940 --> 00:02:23,320
‫only the IP addresses.

25
00:02:29,120 --> 00:02:35,270
‫In this example, I'm connecting to the routers, using the same username and password, but there is

26
00:02:35,270 --> 00:02:42,470
‫no problem if each router has its own unique user and password, just change the dictionaries accordingly.

27
00:02:44,650 --> 00:02:52,510
‫The next step is to create a python list that contains the dictionaries, the list will be called rooters,

28
00:02:53,410 --> 00:02:58,510
‫and it contains three dictionaries are out there, one or two.

29
00:02:58,990 --> 00:03:00,340
‫And Lauter three.

30
00:03:05,030 --> 00:03:10,210
‫When you want to execute the same repetitive task, you usually use a for loop.

31
00:03:10,730 --> 00:03:21,260
‫So let's iterate over the rooters list for her out there in Rotaries Colon and the four block of code.

32
00:03:22,850 --> 00:03:31,610
‫I'm printing a message because I want to know what the script does and I'm using an F string, so connecting

33
00:03:31,610 --> 00:03:38,990
‫to and between curly braces are out outer of host.

34
00:03:42,640 --> 00:03:49,210
‫It return the value of the host key, so in fact, it will printout connecting to enter the IP address

35
00:03:49,210 --> 00:03:52,660
‫of the router, I want to know two weeks after it connects.

36
00:03:54,940 --> 00:04:01,570
‫Notice that I've used double quotes because single quotes were used to define the string.

37
00:04:02,950 --> 00:04:06,940
‫So it's allowed to use double quotes inside single quotes.

38
00:04:08,160 --> 00:04:15,960
‫If I had used the single quotes, I would have gotten there in time pasting the code that connects and

39
00:04:15,960 --> 00:04:18,930
‫authenticate to the author from the last script.

40
00:04:19,650 --> 00:04:20,850
‫From the previous lecture.

41
00:04:22,050 --> 00:04:22,590
‫This one.

42
00:04:26,940 --> 00:04:28,380
‫I'll take the entire script.

43
00:04:32,540 --> 00:04:42,260
‫I'll change it easily, so the value of the host argument is rather the temporary variable of the for

44
00:04:42,260 --> 00:04:45,580
‫loop of host.

45
00:04:46,020 --> 00:04:51,590
‫This will give its IP address and I won't use the port argument anymore.

46
00:04:51,830 --> 00:04:53,420
‫It's the default 23.

47
00:04:54,830 --> 00:04:59,330
‫Instead of yuzu, I have her out there of yuzu.

48
00:05:04,180 --> 00:05:08,230
‫And instead of password, I'll have Ruther of password.

49
00:05:14,370 --> 00:05:17,730
‫I'm removing the show interface, brief comment.

50
00:05:20,410 --> 00:05:28,270
‫And instead of sending the Cheron comment, I'm sending the country comment, so I'm entering the global

51
00:05:28,270 --> 00:05:37,120
‫configuration mode Consti, I'm in the global configuration mode and I'll execute the comment that seeks

52
00:05:37,270 --> 00:05:40,230
‫a default or a static default about.

53
00:05:42,580 --> 00:05:49,570
‫By the way, this topology is a good candidate for a default out because it's a subnetwork and that

54
00:05:49,570 --> 00:05:52,900
‫means a network with only one connection to the outside.

55
00:05:53,230 --> 00:05:55,030
‫In this case, the cloud.

56
00:05:56,570 --> 00:06:09,920
‫So that right and the comment is a series of bytes IPR out zero zero zero zero zero zero zero zero.

57
00:06:11,350 --> 00:06:15,430
‫The outgoing interface is zero zero.

58
00:06:18,800 --> 00:06:23,810
‫And the next top IP address, the IP address of the cloud.

59
00:06:26,810 --> 00:06:34,070
‫When creating a static default route on an Internet network, which is a broadcast multiple access type

60
00:06:34,070 --> 00:06:40,610
‫of network, both the outgoing interface and the next top IP address must be specified.

61
00:06:41,990 --> 00:06:49,100
‫If these networking concepts are new to you, I'd advise you to enroll in the Cisco system in a course

62
00:06:49,430 --> 00:06:55,000
‫where all of these basic networking concepts are explained in great detail.

63
00:06:55,970 --> 00:06:59,420
‫Cisco CCMA is such a wonderful course.

64
00:07:00,670 --> 00:07:07,420
‫After setting the default drought, I want to display the routing table, I want to see the default

65
00:07:07,420 --> 00:07:08,860
‫out in the routing table.

66
00:07:09,250 --> 00:07:09,880
‫So at the end.

67
00:07:09,880 --> 00:07:10,570
‫That's right.

68
00:07:11,500 --> 00:07:12,010
‫And.

69
00:07:13,230 --> 00:07:17,940
‫I'm going into the enable mode and then I'll execute Srei Pirot.

70
00:07:21,970 --> 00:07:26,360
‫Should I be allowed into this part remains the same.

71
00:07:26,980 --> 00:07:28,270
‫I'll remove this line.

72
00:07:30,740 --> 00:07:37,330
‫At the end of the for loop, I led a series of hashes just to see it clear.

73
00:07:39,080 --> 00:07:46,880
‫So let's say 50 hashes before running this script, I want to check that there is no default out on

74
00:07:46,880 --> 00:07:47,420
‫the router.

75
00:07:48,190 --> 00:07:50,420
‫So I'm connecting to the router.

76
00:07:52,670 --> 00:07:54,650
‫I'm entering the enable mode.

77
00:07:56,690 --> 00:08:03,500
‫And by executing shipping out, this is the comment that displays the routing table and there is no

78
00:08:03,500 --> 00:08:06,320
‫default draft and I'm running the script.

79
00:08:07,800 --> 00:08:09,720
‫It's connecting to the first outher.

80
00:08:15,420 --> 00:08:21,000
‫OK, I've forgotten a backslash end at the end of the show, I put out the comment.

81
00:08:23,230 --> 00:08:30,190
‫And also here, so don't forget to add a backslash in after each comment, and I'm running the script.

82
00:08:32,190 --> 00:08:35,700
‫Connecting to the first quarter, these are the cumming's.

83
00:08:37,220 --> 00:08:38,570
‫This is the default route.

84
00:08:40,440 --> 00:08:47,480
‫The second coming out there and the fallout in the last quarter is working just fine.

85
00:08:48,730 --> 00:08:50,080
‫Voxel, thank you.


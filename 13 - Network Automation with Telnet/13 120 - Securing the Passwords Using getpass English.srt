﻿1
00:00:00,660 --> 00:00:07,650
‫In the last lecture, we've developed this simple Python script that connects to some networking devices

2
00:00:07,650 --> 00:00:16,890
‫using Telnet, executes some configuration and shortcomings, IPR outright Pirot and so on, and displays

3
00:00:16,980 --> 00:00:17,730
‫the output.

4
00:00:20,530 --> 00:00:28,900
‫One of the problems of this script is that the password is hard coded, anyone that has access to the

5
00:00:28,900 --> 00:00:33,390
‫script seized the passport, and that's not acceptable at all.

6
00:00:33,880 --> 00:00:40,660
‫Instead of writing the passport inside the script, we could prompt the user that runs the script for

7
00:00:40,660 --> 00:00:42,460
‫the passport in a secure manner.

8
00:00:43,420 --> 00:00:44,800
‫Let's modify the script.

9
00:00:45,760 --> 00:00:54,220
‫I'm importing the get pass module import get pass in time, removing the passport from the dictionary's.

10
00:00:56,470 --> 00:00:57,880
‫Both the key and the.

11
00:01:07,950 --> 00:01:17,400
‫Inside the loop, I'm prompting the user for each doubter's password without echoing, so password equals

12
00:01:17,760 --> 00:01:21,480
‫get past that get pass and a string is argument.

13
00:01:23,210 --> 00:01:31,430
‫This will be the prompting string, for example, enter password, the user will enter the password,

14
00:01:31,790 --> 00:01:34,370
‫which will be saved in the password variable.

15
00:01:36,100 --> 00:01:41,470
‫So here, instead of the hardcoded password, I use the password variable.

16
00:01:45,620 --> 00:01:54,140
‫And I'm using the password that was entered by the user also is the Nable password, if you want, you

17
00:01:54,140 --> 00:01:56,380
‫can ask the user for another password.

18
00:01:57,260 --> 00:01:59,210
‫You can do it as homework.

19
00:02:02,290 --> 00:02:09,460
‫So pasada that in code I'm encoding it to buycks plus pixelation.

20
00:02:12,420 --> 00:02:18,280
‫And they all remember that if you run the script in primetime, it won't work.

21
00:02:19,810 --> 00:02:27,460
‫Bike sharing uses a modified console, which is incompatible with the get pass module, so it Henkes.

22
00:02:29,660 --> 00:02:33,660
‫You have to run the script in a terminal, but it's very easy.

23
00:02:34,910 --> 00:02:44,810
‫I'm opening a terminal SMD data and I'm moving to the directory that contains the script.

24
00:02:50,270 --> 00:02:54,230
‫Click with the right mouse button on the file in Pitchwoman.

25
00:02:55,590 --> 00:02:57,810
‫And copy absolute Beth.

26
00:03:00,780 --> 00:03:03,810
‫And in the terminology D and based.

27
00:03:05,080 --> 00:03:11,710
‫This is the path and I'm erasing the name of the file, I want to move to the directory that contains

28
00:03:11,710 --> 00:03:15,820
‫the file, like the content of the directory.

29
00:03:17,480 --> 00:03:24,140
‫And this is my file, Bellette configure multiple underlying Gipper's, maybe not the best the name,

30
00:03:24,500 --> 00:03:26,500
‫but that doesn't matter anyway.

31
00:03:26,990 --> 00:03:30,080
‫So Python in the name of the script.

32
00:03:34,020 --> 00:03:37,750
‫And it's working, it's asking for the password.

33
00:03:38,730 --> 00:03:42,420
‫OK, this is, in fact line 13.

34
00:03:42,930 --> 00:03:45,930
‫I'm entering the password, which is Cisco.

35
00:03:49,170 --> 00:03:57,260
‫And it has executed the cumming's asking for the best start of the second quarter, I'm entering it

36
00:03:58,860 --> 00:04:00,360
‫and for the last autre.

37
00:04:02,920 --> 00:04:03,460
‫Perfect.

38
00:04:04,790 --> 00:04:08,750
‫The VIX, how you prompt the user for the password in a secure manner.


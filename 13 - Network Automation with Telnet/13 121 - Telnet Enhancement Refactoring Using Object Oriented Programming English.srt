﻿1
00:00:00,630 --> 00:00:02,350
‫Hello, guys, and welcome back.

2
00:00:02,610 --> 00:00:09,310
‫In the last lecture, we've seen how to configure multiple networking devices using Python and Telnet

3
00:00:09,310 --> 00:00:17,430
‫it, we've developed this Python script that configures a default static out on many Cisco devices.

4
00:00:21,550 --> 00:00:28,120
‫The script can be used also with other vendors because that is a standard protocol and they are just

5
00:00:28,120 --> 00:00:34,900
‫sending comments from Python, if you want to configure another platform or operating system, you just

6
00:00:34,900 --> 00:00:36,670
‫send to the appropriate comix.

7
00:00:37,750 --> 00:00:45,090
‫This works just fine, but it can be complicated, at least for beginner programmers, if someone is

8
00:00:45,090 --> 00:00:50,640
‫not a Python expert, but a good networking junior, he can have some difficulties.

9
00:00:51,710 --> 00:00:59,840
‫He has to send the Cummings, his bikes objects, not string's, he must add a backslash end after comment.

10
00:01:01,100 --> 00:01:09,470
‫Post the script execution, decode from objects of type of bikes to strings and so on, a network engineer

11
00:01:09,470 --> 00:01:13,970
‫that tries to automate a simple task can easily get stuck.

12
00:01:15,410 --> 00:01:22,430
‫What they'll do now is refactor this script in such a way that even someone that's not a Python expert

13
00:01:22,640 --> 00:01:26,240
‫can use Python and tell it to automate its daily tasks.

14
00:01:27,530 --> 00:01:34,700
‫If in the previous six years, when we've discussed Bahrami and Namiko, I've created some functions

15
00:01:34,700 --> 00:01:41,960
‫to simplify the usage, this time I'll create a custom class using object oriented programming that

16
00:01:41,960 --> 00:01:43,400
‫will hide its complexity.

17
00:01:44,360 --> 00:01:46,730
‫The user will only have to use the class.

18
00:01:47,360 --> 00:01:51,470
‫I want to keep it simple because complexity is our enemy.

19
00:01:52,720 --> 00:02:00,040
‫Before continuing with this lecture, I strongly recommend you to keep the concepts of object oriented

20
00:02:00,040 --> 00:02:01,270
‫programming in Python.

21
00:02:01,840 --> 00:02:06,680
‫These are very abstract concepts and beginners can get easily stuck.

22
00:02:07,270 --> 00:02:15,130
‫I've added everything you need to know on OLP or object oriented programming in the general Python programming

23
00:02:15,130 --> 00:02:17,560
‫sections at the end of the curriculum.

24
00:02:18,040 --> 00:02:24,940
‫So pause this video, watch that part on OPIS and then come back and resume.

25
00:02:25,060 --> 00:02:32,470
‫From this point, I consider you already know the syntax and the basics of object oriented programming.

26
00:02:33,370 --> 00:02:38,380
‫Back to by charm, I'll create a new Python script called Tell Class.

27
00:02:44,810 --> 00:02:51,800
‫I'm defining a class called Device, these to represent the abstract concept of a networking device

28
00:02:51,800 --> 00:02:52,460
‫of our outer.

29
00:02:53,960 --> 00:02:55,610
‫So class device.

30
00:02:58,060 --> 00:02:59,950
‫And the class implementation.

31
00:03:00,880 --> 00:03:07,570
‫Inside the glass, I'm starting with the constructor, it will be automatically called each time a new

32
00:03:07,570 --> 00:03:10,120
‫object of type device will be created.

33
00:03:12,270 --> 00:03:21,330
‫In Python, the constructor's name is Donder in it, so in it and double underscores on each side.

34
00:03:22,420 --> 00:03:31,330
‫The constructor will set the object attributes, let's think of what are the attributes or the properties

35
00:03:31,540 --> 00:03:34,380
‫of a device in the context of telnet.

36
00:03:34,810 --> 00:03:36,910
‫And there are the IP address.

37
00:03:37,750 --> 00:03:42,490
‫So host the user, name the password.

38
00:03:43,420 --> 00:03:45,970
‫And there is also a telnet connection.

39
00:03:45,970 --> 00:03:47,290
‫Ratel Atlantean.

40
00:03:48,580 --> 00:03:50,200
‫By default, this will be non.

41
00:03:51,730 --> 00:03:59,050
‫The first argument of any method is self, and this is a reference to the object that will call the

42
00:03:59,050 --> 00:03:59,530
‫method.

43
00:04:01,270 --> 00:04:08,290
‫And inside the constructor implementation, I'll create the attributes of the device, objects that

44
00:04:08,290 --> 00:04:09,100
‫will be created.

45
00:04:11,000 --> 00:04:23,690
‫Self-doubt host equals host self, that username equals username and self, that password equals password.

46
00:04:25,670 --> 00:04:29,890
‫Itself, that's the last attribute, equal citizen.

47
00:04:31,240 --> 00:04:39,320
‫End is by default non, it will be initialized later, the name on the left hand side of the equal sign.

48
00:04:39,370 --> 00:04:44,500
‫So host username passata A.M. are the objects attributes.

49
00:04:45,780 --> 00:04:51,550
‫And the names on the right hand side of the equals sign are the methods arguments.

50
00:04:52,080 --> 00:04:59,070
‫So, for example, I can write here and here and it's very good, but the best practice is to use the

51
00:04:59,070 --> 00:05:04,020
‫same names for both the arguments and the objects attributes.

52
00:05:05,400 --> 00:05:13,310
‫Next, I'll follow the flow from the previous script and I'll define some methods for the device objects.

53
00:05:13,590 --> 00:05:15,420
‫The first one will be connect.

54
00:05:16,600 --> 00:05:19,330
‫This is the first thing that the script does.

55
00:05:22,170 --> 00:05:28,560
‫So D.F. Connect and it has no arguments besides the default self.

56
00:05:30,510 --> 00:05:32,760
‫I'm importing Belnap Liebe.

57
00:05:34,970 --> 00:05:41,780
‫This is the first time when we use it and to create a telnet connection, we call the telnet method

58
00:05:41,900 --> 00:05:49,710
‫of telling that this will return a telnet connection, which is the attribute of our device object so

59
00:05:50,270 --> 00:05:51,560
‫itself that that's.

60
00:05:52,890 --> 00:05:58,200
‫This is how we access the attribute equals fell asleep that Bellette.

61
00:06:00,120 --> 00:06:08,820
‫And its argument is self that host the host or the IP address attribute.

62
00:06:11,230 --> 00:06:20,360
‫After connecting to the device, the next logical step is to authenticate to the device so they've authenticated.

63
00:06:22,940 --> 00:06:26,330
‫The method has only one argument, which is self.

64
00:06:27,950 --> 00:06:35,510
‫Authenticating means sending the username and password and I'll copy and paste the piece of code from

65
00:06:35,510 --> 00:06:37,490
‫the script we've already created.

66
00:06:41,470 --> 00:06:45,670
‫These four lines of code, they perform the authentication.

67
00:06:48,940 --> 00:06:56,290
‫Once again, DOCSIS, an attribute we use self doubt and the name of the attribute, so self that can.

68
00:07:03,360 --> 00:07:06,270
‫Instead of kraeutler of user.

69
00:07:07,320 --> 00:07:12,330
‫So the value of the dictionary, I'll use self that username.

70
00:07:13,750 --> 00:07:18,220
‫The object attribute and he are self-taught peasant.

71
00:07:24,230 --> 00:07:31,280
‫Being authenticated on the remote device, we can easily send comments, so D.F. send.

72
00:07:33,340 --> 00:07:43,240
‫Self-command and timeout, which is by default, half of a second, I'll also import the time module

73
00:07:44,140 --> 00:07:45,670
‫at the beginning of the script.

74
00:07:47,850 --> 00:07:55,680
‫And I'm printing out the message, I want to know what it does, sending command.

75
00:07:56,890 --> 00:08:00,190
‫And the command argument between curly braces.

76
00:08:01,590 --> 00:08:07,320
‫Self, that's not the right command, not in code.

77
00:08:10,030 --> 00:08:11,530
‫Plus, pixelation.

78
00:08:13,940 --> 00:08:17,060
‫Backslash N is an object of type buycks.

79
00:08:18,210 --> 00:08:23,940
‫I'll also post the script for a very small amount of time so that the device has enough time to finish

80
00:08:23,940 --> 00:08:25,130
‫executing the comment.

81
00:08:25,140 --> 00:08:27,830
‫So time that slip of time what?

82
00:08:30,120 --> 00:08:37,680
‫This is by default, zero point five, but when calling the method, we can give another value, his

83
00:08:37,680 --> 00:08:38,250
‫argument.

84
00:08:40,480 --> 00:08:47,410
‫And the last method will be show that will return the output of the comments, so the show.

85
00:08:49,160 --> 00:08:54,890
‫And its implementation output equals self-taught deden that read all.

86
00:08:58,780 --> 00:09:04,480
‫That decode I'm decoding from bytes to string also on this line.

87
00:09:05,510 --> 00:09:11,240
‫The default encoding scheme is your defeat and, of course, return output.

88
00:09:13,310 --> 00:09:20,690
‫Of course, all this is our custom tailored class will take a short break and in the next lecture we'll

89
00:09:20,690 --> 00:09:21,650
‫see how to use it.


﻿1
00:00:00,940 --> 00:00:09,100
‫Let's continue our example and read the part that connects and configure all devices in the topology.

2
00:00:10,030 --> 00:00:13,960
‫We have to configure more gravitas, not only one.

3
00:00:14,110 --> 00:00:21,610
‫So basically, we need to iterate over a list of routers using a for loop and execute this piece of

4
00:00:21,610 --> 00:00:24,370
‫code for out there in the list.

5
00:00:25,500 --> 00:00:33,200
‫I'm going to create a dictionary for each other, for example, are on dict equals, and it's a dictionary

6
00:00:34,230 --> 00:00:37,560
‫host Kolon and the IP address.

7
00:00:41,160 --> 00:00:43,890
‫Username, Kolon Yuan.

8
00:00:45,110 --> 00:00:54,290
‫Password, Cisco and other values that can be unique to each other, for example, I can have a different

9
00:00:54,290 --> 00:00:56,590
‫enabled password for each other.

10
00:00:57,260 --> 00:01:02,550
‫This means enable password calling and password.

11
00:01:02,570 --> 00:01:07,460
‫Cisco, it's the same for each year out there, but I could have different ones.

12
00:01:07,910 --> 00:01:11,270
‫And the Lubic interface IP address.

13
00:01:11,600 --> 00:01:19,250
‫So loop back IP and the value for the first quarter.

14
00:01:19,260 --> 00:01:20,120
‫It will be one.

15
00:01:20,120 --> 00:01:21,410
‫That one.

16
00:01:21,770 --> 00:01:25,830
‫And I'll copy and paste this dictionary for the other two quarters.

17
00:01:27,130 --> 00:01:32,930
‫This is for the second router and for the third quarter and I'll change them accordingly.

18
00:01:33,470 --> 00:01:34,730
‫The IP addresses.

19
00:01:36,110 --> 00:01:41,350
‫And the IP addresses of the Lubeck interfaces and for the fact quarter.

20
00:01:43,870 --> 00:01:52,330
‫Now I'm defining a new list that stores the dictionaries are out there equals or at least of dictionaries.

21
00:01:52,630 --> 00:02:00,010
‫The first element will be our one decade or two decade and the last dictionary.

22
00:02:01,590 --> 00:02:10,500
‫This is the list I'm going to iterate over for our inner OK is the temporary variable of the for loop.

23
00:02:11,100 --> 00:02:17,520
‫Colin and Eileen indent this piece of code that there are three elements in the list.

24
00:02:17,700 --> 00:02:21,840
‫So three Thracians, each iteration will create an object.

25
00:02:22,710 --> 00:02:26,850
‫The name of the object will be simply outer.

26
00:02:29,010 --> 00:02:30,800
‫So hot water everywhere.

27
00:02:34,310 --> 00:02:42,320
‫Good and at the end, 50 hashes this way, you'll notice each iteration.

28
00:02:46,230 --> 00:02:53,880
‫And instead of these hardcoded iPad guys, I'll have our of host.

29
00:02:55,610 --> 00:03:03,470
‫So are is an element of the list, and that means a dictionary in the first iteration.

30
00:03:03,740 --> 00:03:13,280
‫This is our empire of host means of apiaries username equals R of username.

31
00:03:17,410 --> 00:03:19,420
‫And the same for the passport.

32
00:03:26,970 --> 00:03:34,410
‫In this case, all the characters have the same enable password, but I could use different enabled

33
00:03:34,410 --> 00:03:42,030
‫passwords for the routers instead of sending the Cisco string as password.

34
00:03:42,420 --> 00:03:45,930
‫I'll send half of enable password.

35
00:03:46,410 --> 00:03:47,700
‫This is the name of the key.

36
00:03:53,590 --> 00:04:03,190
‫And the last change of the IP address of the Lubezki interface will be the value of the Lubeck IP key

37
00:04:03,370 --> 00:04:13,330
‫of the dictionary, so of ah I lose an F string literal to format to the string like this are of in-between

38
00:04:13,330 --> 00:04:16,480
‫double quotes, lookback, IP

39
00:04:19,360 --> 00:04:20,470
‫and Rexall.

40
00:04:21,310 --> 00:04:22,450
‫I'm writing the script.

41
00:04:27,700 --> 00:04:34,660
‫We are seeing how it's connecting, authenticating and configuring each router sequentially.


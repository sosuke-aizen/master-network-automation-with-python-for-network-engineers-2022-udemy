﻿1
00:00:00,620 --> 00:00:08,150
‫Let's go ahead and see how to connect to networking devices from Python using talibe, the module that

2
00:00:08,150 --> 00:00:16,790
‫implements that in Python is called Tell X Python Standard Library, and that means that's already available.

3
00:00:17,450 --> 00:00:21,770
‫It provides a tailored class that implements the Taylor protocol.

4
00:00:21,950 --> 00:00:24,100
‫So I'm imparting that, Leslie.

5
00:00:28,970 --> 00:00:35,300
‫Optionally, I can declare some variables, for example, host, this is the IP address of their outer.

6
00:00:39,250 --> 00:00:41,560
‫Part by default is 23.

7
00:00:42,990 --> 00:00:49,800
‫It's necessary only if you use another port, the user that will connect to the telnet server.

8
00:00:50,610 --> 00:00:55,230
‫We have two users, my admin and you want lakes use you on.

9
00:00:56,550 --> 00:00:59,700
‫And the user's password, which is Cisco.

10
00:01:01,220 --> 00:01:08,270
‫The first step when working with Bellette is tell telnet object by calling the telnet method of the

11
00:01:08,270 --> 00:01:10,940
‫telnet lipgloss, which is the constructor.

12
00:01:12,190 --> 00:01:24,190
‫So time equals they'll let slip that bell written in upper case and the first argument host equals and

13
00:01:24,190 --> 00:01:28,530
‫the IP address of the device in our case is the host variable.

14
00:01:29,350 --> 00:01:37,520
‫If you don't like to use variables, you can simply right here express and optionally portcullis port

15
00:01:37,540 --> 00:01:38,310
‫by default.

16
00:01:38,320 --> 00:01:39,220
‫It's 23.

17
00:01:42,520 --> 00:01:47,080
‫At this moment, we have a telnet client, the DNA variable.

18
00:01:48,150 --> 00:01:52,470
‫Lexi waxed the floor when connecting to the device using pellet.

19
00:01:53,860 --> 00:02:00,430
‫So in candidate, I'm connecting to the same device using the telnet comment.

20
00:02:04,990 --> 00:02:10,270
‫And it's prompting for the username I'm entering the username you on.

21
00:02:11,280 --> 00:02:17,280
‫Then it's prompting for the password and Dementor, the password, which is Cisco.

22
00:02:18,760 --> 00:02:22,570
‫And I mean, now I can simply send any comment.

23
00:02:23,770 --> 00:02:29,110
‫For example, a stripey interface, brief or show version.

24
00:02:31,850 --> 00:02:38,480
‫Or I can enter the enablement and so on, let's do the same from Python.

25
00:02:41,150 --> 00:02:48,740
‫To post the script when asking for the username, I'll call the read until method of the Telnet client

26
00:02:48,740 --> 00:02:49,340
‫like this.

27
00:02:51,490 --> 00:02:54,550
‫Then the Telnet client that until.

28
00:02:57,330 --> 00:03:02,460
‫And the string username, but is byte, not string.

29
00:03:04,120 --> 00:03:06,970
‫So be from Buycks username.

30
00:03:07,540 --> 00:03:16,510
‫Take care to use an uppercase you letter Callon in the white space, tell that Lip is working only with

31
00:03:16,510 --> 00:03:16,960
‫Buycks.

32
00:03:17,320 --> 00:03:18,550
‫Keep this in mind.

33
00:03:20,040 --> 00:03:26,610
‫After pausing it, I'll send the username into the connection to do that, I'll call the right method

34
00:03:26,610 --> 00:03:29,460
‫of the same object of type Telnet client.

35
00:03:29,880 --> 00:03:31,080
‫So T's and dot the right.

36
00:03:32,150 --> 00:03:38,360
‫The user in our case is the user variable dot encode.

37
00:03:41,530 --> 00:03:48,520
‫Remember I said that it talks with bikes, so I'm not just sending the U.S. there is a string, but

38
00:03:48,520 --> 00:03:50,770
‫as an object of type bikes.

39
00:03:52,820 --> 00:04:00,010
‫And I'll concatenate a backslash in which is a new line or the entire you normally press at the end

40
00:04:00,560 --> 00:04:01,970
‫and of course, is bite's.

41
00:04:03,260 --> 00:04:07,040
‫So be from bikes and pixelation.

42
00:04:11,220 --> 00:04:14,850
‫So, in fact, I'm here, I've entered the username.

43
00:04:16,850 --> 00:04:19,070
‫Then I'll do the same for the past.

44
00:04:20,530 --> 00:04:27,130
‫I'm pausing the script when I read the password from the connection did not read until.

45
00:04:28,730 --> 00:04:31,460
‫Password, Callon Whitespace.

46
00:04:33,120 --> 00:04:35,160
‫And I'm sending it down.

47
00:04:35,250 --> 00:04:35,850
‫That's right.

48
00:04:38,730 --> 00:04:40,620
‫Password that in code.

49
00:04:42,770 --> 00:04:46,580
‫Plus, backslidden is an object of type buycks.

50
00:04:51,150 --> 00:04:53,340
‫At this moment, I'm logged in.

51
00:04:55,160 --> 00:05:02,640
‫Let's send a comment into the Telnet connection, for example, shall I interface with South data,

52
00:05:02,660 --> 00:05:03,050
‫right.

53
00:05:04,220 --> 00:05:11,570
‫The comment is A Buycks object B and the comment, shall I be?

54
00:05:12,510 --> 00:05:22,070
‫Interface, brief and pixelation to read the output from the connection, I'll call the read all method.

55
00:05:22,470 --> 00:05:27,750
‫This can be done after closing the connection so that right.

56
00:05:30,610 --> 00:05:31,240
‫Exit.

57
00:05:32,470 --> 00:05:33,460
‫I'm exiting.

58
00:05:35,170 --> 00:05:43,360
‫And I'm pausing the script for a second so that the device has enough time to execute all comix, so

59
00:05:43,360 --> 00:05:48,320
‫time that sleep of one, don't forget to import the time module.

60
00:05:48,580 --> 00:05:49,710
‫So import time.

61
00:05:52,760 --> 00:05:55,610
‫And the output output equals.

62
00:05:56,770 --> 00:06:01,660
‫The industry, Don, I'm reading all the data from the connections Baffour.

63
00:06:02,710 --> 00:06:10,750
‫Output is an object of type buycks print type of output, and it will display bite's.

64
00:06:12,290 --> 00:06:20,720
‫But normally I want the string, so output equals output data decode, I'm decoding from bytes to string

65
00:06:21,050 --> 00:06:22,310
‫and print output.

66
00:06:23,500 --> 00:06:25,300
‫You'll see the output spring.

67
00:06:26,620 --> 00:06:27,700
‫I'm writing the script.

68
00:06:32,540 --> 00:06:33,520
‫Interrelating.

69
00:06:36,400 --> 00:06:39,970
‫I think it takes too long, so I'm going to stop the script.

70
00:06:40,860 --> 00:06:43,030
‫There is an issue and I'll find it.

71
00:06:46,540 --> 00:06:53,880
‫Let's try the following before extending the show comment, I'd like to send the terminal length a zero

72
00:06:53,890 --> 00:06:54,490
‫comment.

73
00:06:55,240 --> 00:07:03,710
‫When we execute a comment that returns a lot of output, the Cisco iOS will display more pages to display

74
00:07:03,710 --> 00:07:04,390
‫it entirely.

75
00:07:04,390 --> 00:07:11,020
‫Without pausing, we must execute the terminal at length comment, which has an option that affects

76
00:07:11,020 --> 00:07:13,720
‫the number of lines of output to display.

77
00:07:14,640 --> 00:07:19,470
‫And if you set that number to zero, it will not pause while displaying the output.

78
00:07:19,890 --> 00:07:24,900
‫So before sending the show comment, I write down that right.

79
00:07:25,950 --> 00:07:28,890
‫And the comment is a series of bytes.

80
00:07:30,250 --> 00:07:31,900
‫Their criminal length.

81
00:07:32,900 --> 00:07:35,240
‫The zero and pixelation.

82
00:07:36,270 --> 00:07:40,350
‫This is just for the Lauter to display the output entirely.

83
00:07:40,950 --> 00:07:42,180
‫I'm writing the script again.

84
00:07:44,380 --> 00:07:45,600
‫And it's working.

85
00:07:48,230 --> 00:07:55,640
‫You see, if that's the type of the output variable is bitts, and then we see the cumming's.

86
00:07:56,800 --> 00:07:57,940
‫And the output.

87
00:07:59,990 --> 00:08:06,040
‫Now, if you want to send more comments, you call the right method once for each comment.

88
00:08:06,620 --> 00:08:13,250
‫For example, I'm entering the enablement data right enable.

89
00:08:14,590 --> 00:08:15,820
‫And pixelation.

90
00:08:16,800 --> 00:08:25,380
‫After sending the Nable comment, I must send the password, the password, so Tianna right, and the

91
00:08:25,380 --> 00:08:28,230
‫password is a series of bytes.

92
00:08:29,290 --> 00:08:31,360
‫So B and Cisco.

93
00:08:32,540 --> 00:08:35,990
‫This isn't a comment, Eakes, the Nable passata.

94
00:08:39,820 --> 00:08:47,860
‫We send it as any other comment, and finally I'm sending the Suranne comment down.

95
00:08:48,880 --> 00:08:49,630
‫That's right.

96
00:08:51,410 --> 00:08:52,340
‫So, Ron.

97
00:08:53,890 --> 00:08:55,030
‫And by running the script.

98
00:08:58,360 --> 00:09:00,940
‫And we see the expected output.

99
00:09:01,270 --> 00:09:03,970
‫This is the output of the Suranne command.

100
00:09:05,720 --> 00:09:13,360
‫One last thing, when you want to send restring a comment like a series of bikes, you can use the B

101
00:09:13,400 --> 00:09:20,210
‫letter before the string like these, or you can do in the following way the not the right.

102
00:09:21,760 --> 00:09:32,590
‫The comment is a stirring show, IP interface with an explosion and then back in code, this method

103
00:09:32,830 --> 00:09:37,490
‫will include the string to an object of type bytes.

104
00:09:37,870 --> 00:09:40,720
‫So these two lines do the same.

105
00:09:41,970 --> 00:09:43,380
‫I'll comment about this one.

106
00:09:43,650 --> 00:09:44,580
‫It's the same.

107
00:09:47,380 --> 00:09:48,730
‫OK, sorry, X-Rite, right?

108
00:09:50,400 --> 00:09:51,690
‫I've misspelled it.

109
00:09:55,100 --> 00:10:02,780
‫OK, that's all you've just learned, how to connect to networking devices and execute Cumming's using

110
00:10:02,780 --> 00:10:04,130
‫tell from Python.

111
00:10:05,030 --> 00:10:12,770
‫We've used a Cisco router because it was easily available, if you have another platform instead of

112
00:10:12,770 --> 00:10:14,970
‫it, even a Linux server.

113
00:10:15,380 --> 00:10:19,220
‫The flow will be similar because telnet is a standard protocol.

114
00:10:20,170 --> 00:10:26,080
‫We'll take a short break, and in the next lecture, I'll show you how to configure multiple devices

115
00:10:26,170 --> 00:10:27,130
‫using Tallaght.


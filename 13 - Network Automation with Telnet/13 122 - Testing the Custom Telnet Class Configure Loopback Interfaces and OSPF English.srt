﻿1
00:00:00,300 --> 00:00:08,640
‫In this lecture will test the device class we've just created will use this topology with three characters

2
00:00:09,030 --> 00:00:14,920
‫and will configure a Lubeck interface and then the routing protocol on each other.

3
00:00:15,480 --> 00:00:17,730
‫Let's start with the first router.

4
00:00:19,170 --> 00:00:29,400
‫I'm creating a new object of type device called Routon out there, unequals, the name of the class

5
00:00:29,700 --> 00:00:33,300
‫device and the constructor arguments.

6
00:00:34,530 --> 00:00:44,880
‫Self is the object itself, and there are four other arguments, so host equals and the IP address.

7
00:00:47,110 --> 00:00:54,340
‫User name equals yuan and password equals Cisco.

8
00:00:57,320 --> 00:01:06,450
‫The last argument will remain at its default value, and I'm connecting to the device, so are out there

9
00:01:06,460 --> 00:01:08,000
‫one that connect?

10
00:01:08,210 --> 00:01:10,250
‫I'm calling the connect the method.

11
00:01:11,490 --> 00:01:16,320
‫Then I'll authenticate her out there on that authenticate.

12
00:01:18,020 --> 00:01:27,080
‫Perfect at this moment, I'm authenticated to the device and I can send Comix Lexie up terminal, what's

13
00:01:27,080 --> 00:01:31,410
‫the sequence of documents to configure the Lubeck and SPF?

14
00:01:32,490 --> 00:01:35,460
‫First, I'm connecting to the Lautaro using Belnap.

15
00:01:40,030 --> 00:01:47,860
‫First, I am executing the enable command to enter the enablement so enable and the best, I'll do the

16
00:01:47,860 --> 00:01:53,230
‫same in the Python script about the one that sent the method.

17
00:01:54,470 --> 00:02:03,980
‫And the comment enabled it's not necessary to add a backslash in because it's automatically added in

18
00:02:03,980 --> 00:02:05,000
‫the ethics body.

19
00:02:06,620 --> 00:02:15,380
‫And out there on that stand in the past, that enabled passata, Cisco, then I'll execute Consti to

20
00:02:15,380 --> 00:02:17,650
‫enter the global configuration mode.

21
00:02:18,800 --> 00:02:27,890
‫And Interface Lubeck in the name of the Lubezki interface, for example, zero, this will create the

22
00:02:27,890 --> 00:02:28,550
‫interface.

23
00:02:29,090 --> 00:02:35,840
‫Being in the interface configuration mode, I can set its IP address IP address one.

24
00:02:35,840 --> 00:02:37,280
‫That one.

25
00:02:37,490 --> 00:02:42,100
‫And a host Misk 30 to mask.

26
00:02:43,000 --> 00:02:45,590
‫This is how you configure a Lubeck interface.

27
00:02:46,010 --> 00:02:54,950
‫This interface is a virtual one that's always up and it's necessary for many protocols, including routing

28
00:02:54,950 --> 00:02:55,640
‫protocols.

29
00:02:57,480 --> 00:03:03,690
‫Let's send these comments also from Python about out one that sent.

30
00:03:06,570 --> 00:03:13,230
‫Kind of are out there, one that said, OK, send, not send.

31
00:03:17,200 --> 00:03:28,800
‫Interface back zero, router one that send IP address on that on that one, that one and the mask,

32
00:03:30,460 --> 00:03:30,900
‫OK.

33
00:03:32,370 --> 00:03:42,300
‫Then I'll type exit to enter the global configuration mode again, and I'm enabling OEP so her there

34
00:03:42,570 --> 00:03:45,600
‫o spf in the process idea.

35
00:03:46,750 --> 00:03:58,270
‫One net that zero zero that the zero zero zero zero zero zero, that the zero, it will start SPF on

36
00:03:58,270 --> 00:04:02,650
‫all connected networks and area zero, the backbone area.

37
00:04:03,640 --> 00:04:14,590
‫Then I'm typing end to enter the enablement terminal length zero, and I'm executing a show, comment

38
00:04:14,890 --> 00:04:24,010
‫show, IP interface, no show IP protocols, I want to see that SBF was enabled.

39
00:04:25,420 --> 00:04:34,570
‫OK, I'll execute the same sequence of comings from Python, but first I'll disable, SBF and remove

40
00:04:34,570 --> 00:04:35,760
‫the Lubeck interface.

41
00:04:36,430 --> 00:04:38,470
‫I want to configure that from Python.

42
00:04:50,030 --> 00:04:56,500
‫Oh, SPF is not enabled anymore and there is no Lubezki interface either.

43
00:05:00,830 --> 00:05:08,270
‫So out there, one that send exit route around that sent her out there.

44
00:05:08,330 --> 00:05:20,090
‫Oh, SBF, one there, one that sent net zero, that zero zero zero and there you zero.

45
00:05:20,930 --> 00:05:25,640
‫These are the comments that I've also executed in the terminal.

46
00:05:37,220 --> 00:05:38,570
‫And the show coming up.

47
00:05:45,270 --> 00:05:46,860
‫And finally exit.

48
00:05:50,910 --> 00:05:59,960
‫Before sending all these comments, I want to see the output so output equals out there on that show,

49
00:06:00,120 --> 00:06:01,920
‫I'm calling the show method.

50
00:06:03,240 --> 00:06:04,620
‫And print output.

51
00:06:06,260 --> 00:06:07,430
‫And I'm writing the script.

52
00:06:10,250 --> 00:06:12,130
‫Extending the cumming's.

53
00:06:17,920 --> 00:06:21,950
‫OK, there is an error, I'll find it in a second.

54
00:06:22,660 --> 00:06:30,660
‫So if there is an error in the show method, of course, to get the output we should call read all not

55
00:06:30,670 --> 00:06:31,210
‫good until.

56
00:06:32,460 --> 00:06:36,000
‫Sorry, I'm running the script again.

57
00:06:38,500 --> 00:06:45,430
‫We noticed how extending the comments to the device, they are getting executed and we see the output.

58
00:06:47,020 --> 00:06:53,830
‫Both the Lubeck interface and the OSP routing protocol were successfully configured.

59
00:06:54,870 --> 00:07:00,930
‫Just compare to the previous script how easily and straightforward it is.

60
00:07:03,440 --> 00:07:11,660
‫There are no backslash N bytes, objects or other complicated things will take a short break and in

61
00:07:11,660 --> 00:07:17,840
‫the next lecture, I'll show you how to push the configuration to all the doubters in our topology.


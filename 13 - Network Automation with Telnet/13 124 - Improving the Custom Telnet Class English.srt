﻿1
00:00:00,870 --> 00:00:07,940
‫I want to add a small improvement to our class, usually we send more comments to the remote devices,

2
00:00:08,790 --> 00:00:13,280
‫just take a look at this example we've done in a previous lecture.

3
00:00:14,040 --> 00:00:18,690
‫The script connects to that out there and sends a series of comments.

4
00:00:19,530 --> 00:00:28,320
‫What if instead of sending comment after comment by calling the send method, we define a new method

5
00:00:28,590 --> 00:00:30,450
‫which takes is argument.

6
00:00:30,450 --> 00:00:36,870
‫A list of Cumming's uses a follow up to iterate over the comings and thinks of them to that out there

7
00:00:37,020 --> 00:00:38,080
‫to be executed.

8
00:00:38,760 --> 00:00:40,180
‫Wouldn't it be easier?

9
00:00:40,920 --> 00:00:42,500
‫So let's define that.

10
00:00:42,500 --> 00:00:51,390
‫The new method I'm defining a new method called Sent from List, D.F. sent from list.

11
00:00:51,990 --> 00:00:58,680
‫It has a list of commands his argument and thinks each in from the list to the device.

12
00:00:58,860 --> 00:01:00,990
‫So Comix, this is a list.

13
00:01:02,070 --> 00:01:10,380
‫I'm iterating over the list and calling the scent method from above for each comment in the list.

14
00:01:11,920 --> 00:01:23,740
‫So far, ACMD incomings the mattocks argument Callon and inside of a group I'm calling the method sent

15
00:01:24,730 --> 00:01:30,580
‫itself that sent this is how you call a method of an object of study.

16
00:01:31,540 --> 00:01:39,130
‫OK, ACMD is the command argument of the method and the last argument will be left to default.

17
00:01:40,510 --> 00:01:48,980
‫That's all now, instead of sending comment after comment, I'll create a list with all the comments

18
00:01:49,240 --> 00:01:50,460
‫and called the new method.

19
00:01:51,310 --> 00:01:52,390
‫So let's create a list.

20
00:01:55,950 --> 00:02:02,790
‫So Cummings equalised and all these Cummings as elements of the list.

21
00:02:26,190 --> 00:02:33,660
‫OK, these are all the comments I'm removing these lines and I'm calling the new method.

22
00:02:35,170 --> 00:02:44,470
‫So we're out there that sent from list and to the list, this argument, and that's all, it's much

23
00:02:44,470 --> 00:02:45,120
‫more easier.

24
00:02:45,400 --> 00:02:46,440
‫I'm writing the script.

25
00:02:49,460 --> 00:02:51,070
‫We see the same result.

26
00:02:55,650 --> 00:02:59,160
‫It does seem just easier for the user.

27
00:03:03,730 --> 00:03:12,820
‫This is all the user has to do to configure a Lubeck interface and SBF on a device, he's just using

28
00:03:12,820 --> 00:03:13,490
‫the class.

29
00:03:13,960 --> 00:03:15,200
‫No more details.

30
00:03:15,880 --> 00:03:17,290
‫Thank you, Voxel.

